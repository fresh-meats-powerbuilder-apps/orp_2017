HA$PBExportHeader$w_marketing_buffer_inq.srw
forward
global type w_marketing_buffer_inq from w_netwise_response
end type
type dw_division from u_division within w_marketing_buffer_inq
end type
type dw_product from u_product_code_short within w_marketing_buffer_inq
end type
type dw_week_end from datawindow within w_marketing_buffer_inq
end type
end forward

global type w_marketing_buffer_inq from w_netwise_response
int X=170
int Y=261
int Width=1879
int Height=545
long BackColor=79741120
dw_division dw_division
dw_product dw_product
dw_week_end dw_week_end
end type
global w_marketing_buffer_inq w_marketing_buffer_inq

type variables
boolean			ib_valid_to_close

w_marketing_buffer		iw_parent

Integer			ii_PARange
end variables

forward prototypes
public subroutine wf_setproductcode ()
end prototypes

public subroutine wf_setproductcode ();//dw_product.SetItem(1,"fab_product_code", "")
//dw_product.SetItem(1,"product_short_descr", "")

end subroutine

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_week_end

String		ls_division, &
				ls_product, &
				ls_week_end, &
				ls_string

This.SetRedraw(False)

dw_week_end.GetChild("week_end_date", ldwc_week_end)
ldwc_week_end.Reset()
ls_string = iw_parent.wf_set_week_end()
ldwc_week_end.ImportString(ls_string)

ls_division = iw_parent.wf_get_division()
ls_product = iw_parent.wf_get_product()
ls_week_end = iw_parent.wf_get_week_end()

If Not iw_frame.iu_string.nf_IsEmpty(ls_division) Then 
	dw_division.Reset()
	dw_division.ImportString(ls_division)
End If

If Not iw_frame.iu_string.nf_IsEmpty(ls_product) Then 
	dw_product.uf_setproductcode(Left(ls_product, Pos(ls_product, '~t') - 1))
End If

If Not iw_frame.iu_string.nf_IsEmpty(ls_week_end) Then 
	dw_week_end.SetItem(1, "week_end_date", ls_week_end)	
Else
	dw_week_end.SetItem(1, "week_end_date", "          ")
End If

This.SetRedraw(True)
end event

on open;call w_netwise_response::open;String				ls_plant, &
						ls_product

iw_parent = Message.PowerObjectParm

If Not IsValid(iw_parent) Then Close(This)

This.Title = iw_parent.Title + " Inquire"


end on

on w_marketing_buffer_inq.create
int iCurrent
call w_netwise_response::create
this.dw_division=create dw_division
this.dw_product=create dw_product
this.dw_week_end=create dw_week_end
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_division
this.Control[iCurrent+2]=dw_product
this.Control[iCurrent+3]=dw_week_end
end on

on w_marketing_buffer_inq.destroy
call w_netwise_response::destroy
destroy(this.dw_division)
destroy(this.dw_product)
destroy(this.dw_week_end)
end on

on close;call w_netwise_response::close;If Not ib_valid_to_close Then Message.StringParm = " "
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_division, &
			ls_product, &
			ls_product_descr, &
			ls_week_end, &
			ls_string, &
			ls_tab

ls_tab = "~t"

If dw_division.AcceptText() = -1 or &
		dw_product.AcceptText() = -1 Then Return

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End if

ls_product = dw_product.uf_getproductcode()
If iw_frame.iu_string.nf_IsEmpty(ls_product) Then
	ls_product_descr = ""
Else
	ls_product_descr = dw_product.uf_getproductdesc()
End if

ls_week_end = dw_week_end.GetItemString(1, "week_end_date")

ls_string = ls_division + ls_tab + dw_division.uf_get_division_descr() + &
				ls_tab + ls_product + ls_tab + ls_product_descr + ls_tab + &
				ls_week_end

ib_valid_to_close = True
CloseWithReturn(This, ls_string)
end event

type cb_base_help from w_netwise_response`cb_base_help within w_marketing_buffer_inq
int X=1463
int Y=321
int TabOrder=90
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_marketing_buffer_inq
int X=1171
int Y=321
int TabOrder=80
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_marketing_buffer_inq
int X=878
int Y=321
int TabOrder=70
end type

type dw_division from u_division within w_marketing_buffer_inq
int X=234
int Y=25
int TabOrder=100
end type

event itemchanged;Long	ll_ret

ll_ret = Super::Event itemchanged(row, dwo, data)

If ll_ret = 1 then return 1

wf_setproductcode()

Return ll_ret
end event

type dw_product from u_product_code_short within w_marketing_buffer_inq
int X=197
int Y=113
int TabOrder=20
end type

event getfocus;call super::getfocus;iw_frame.SetMicroHelp("Product Code is Optional")
end event

on constructor;call u_product_code_short::constructor;insertrow(0)
uf_set_division(dw_division)
end on

type dw_week_end from datawindow within w_marketing_buffer_inq
int Y=213
int Width=1047
int Height=93
int TabOrder=25
string DataObject="d_marketing_buffer_week_end"
boolean Border=false
boolean LiveScroll=true
end type

event getfocus;iw_frame.SetMicroHelp("Week End Date is Optional")
end event

on constructor;This.InsertRow(0)
end on

