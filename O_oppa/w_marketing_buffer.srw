HA$PBExportHeader$w_marketing_buffer.srw
$PBExportComments$Marketing Buffer
forward
global type w_marketing_buffer from w_netwise_sheet
end type
type dw_marketing_buffer from u_netwise_dw within w_marketing_buffer
end type
type dw_marketing_buffer_header from u_netwise_dw within w_marketing_buffer
end type
end forward

global type w_marketing_buffer from w_netwise_sheet
integer width = 3086
integer height = 1520
string title = "Marketing Buffer"
long backcolor = 12632256
long il_borderpaddingwidth = 100
dw_marketing_buffer dw_marketing_buffer
dw_marketing_buffer_header dw_marketing_buffer_header
end type
global w_marketing_buffer w_marketing_buffer

type variables
u_pas203		iu_pas203
u_ws_pas_share iu_ws_pas_share

s_error		istr_error_info

Integer		ii_rec_count, &
		ii_number_of_dates,&
		ii_lastrow

String		is_update_string,&
		is_data

Boolean		ib_updating 
end variables

forward prototypes
public function string wf_get_product ()
public function string wf_get_division ()
public function string wf_get_week_end ()
public function boolean wf_retrieve ()
public function string wf_set_week_end ()
public subroutine wf_filenew ()
public function boolean wf_update ()
public subroutine wf_delete ()
public function boolean wf_check_required ()
public function boolean wf_validate (integer al_row)
public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_division)
public function boolean wf_deleterow ()
public function boolean wf_addrow ()
public subroutine wf_find_duplicate (long al_row, string as_data, string as_check_field, ref string as_return)
end prototypes

public function string wf_get_product ();String			ls_product

ls_product = dw_marketing_buffer_header.GetItemString &
						(1, "fab_product_code") + "~t" + &
					dw_marketing_buffer_header.GetItemString &
					(1, "product_short_descr") 

Return(ls_product)
end function

public function string wf_get_division ();String			ls_division

ls_division = dw_marketing_buffer_header.GetItemString &
						(1, "division_code") + "~t" + &
					dw_marketing_buffer_header.GetItemString &
					(1, "division_descr") 

Return(ls_division)
end function

public function string wf_get_week_end ();String			ls_week_end

ls_week_end = String(dw_marketing_buffer_header.GetItemString &
				(1, "week_end_date"))

Return(ls_week_end)
end function

public function boolean wf_retrieve ();Int			li_ret

Char			lc_fab_product_descr[30], &
				lc_fab_group[2]

String		ls_string, &
				ls_market_data, &
				ls_product_code, &
				ls_division, &
				ls_week_end				

Long			ll_rec_count


Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

OpenWithParm(w_marketing_buffer_inq, This)
ls_string = Message.StringParm
If iw_frame.iu_string.nf_IsEmpty(ls_string) Then Return False

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

dw_marketing_buffer_header.Reset()
dw_marketing_buffer.Reset()

dw_marketing_buffer_header.ImportString(ls_string) 

ls_division = dw_marketing_buffer_header.GetItemString(1,"division_code")

ls_product_code = dw_marketing_buffer_header.GetItemString &
						(1,"fab_product_code")
If IsNull(ls_product_code) Then
	ls_product_code = ""
End If
	
ls_week_end = dw_marketing_buffer_header.GetItemString(1,"week_end_date")
If iw_frame.iu_string.nf_IsEmpty(ls_week_end) Then
	ls_week_end = ""
End If

istr_error_info.se_event_name = "wf_retrieve"
If Not iu_ws_pas_share.nf_pasp43fr(istr_error_info, &
											ls_division + "~t" + ls_product_code + &
											"~t" + ls_week_end + "~r~n", &
											ls_market_data) Then Return False

SetRedraw (False)

ll_rec_count = dw_marketing_buffer.ImportString(ls_market_data)

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

dw_marketing_buffer.ResetUpdate()

// The first two fields are protected. Move focus to 1rst unprotected.
dw_marketing_buffer.Sort()
dw_marketing_buffer.GroupCalc()

dw_marketing_buffer.SetColumn("buffer_pct")
SetRedraw(True)

Return True
end function

public function string wf_set_week_end ();DataWindowChild			ldwc_temp

String						ls_temp

dw_marketing_buffer_header.GetChild("week_end_date", ldwc_temp)
ls_temp = ldwc_temp.Describe("DataWindow.Data")

Return(ls_temp)
end function

public subroutine wf_filenew ();DataWindowChild			ldwc_week_end

Integer						li_count, &
								li_sequence

Long							ll_row,ll_temp

String						ls_string


If iw_frame.iu_string.nf_IsEmpty(dw_marketing_buffer_header.GetItemString &
		(1, "division_code")) Then
	iw_frame.SetMicroHelp("Please reinquire and enter a Division before adding a row.")
	Return 
End If

ll_row = dw_marketing_buffer.RowCount() + 1
dw_marketing_buffer.SetRedraw(False)

dw_marketing_buffer_header.GetChild("week_end_date", ldwc_week_end)
ls_string = ldwc_week_end.Describe("DataWindow.Data")
ll_temp = dw_marketing_buffer.ImportString(ls_string, 2, 0, 0, 0, 4) 

If ll_row = 1 Then
	li_sequence = 1
Else	
	li_sequence = dw_marketing_buffer.GetitemNumber(ll_row - 1, &
			"sequence") + 1
End If

li_count = ll_row
For li_count = li_count to dw_marketing_buffer.RowCount()
	dw_marketing_buffer.SetItem(li_count, "fab_product_code", " ")
	dw_marketing_buffer.SetItem(li_count, "product_state", "1")
	//12-12 jac
	dw_marketing_buffer.SetItem(li_count, "product_status", "G")
	//
	dw_marketing_buffer.SetItem(li_count, "product_short_descr", " ")
	dw_marketing_buffer.SetItem(li_count, "sequence", li_sequence)
	dw_marketing_buffer.SetItem(li_count, "avail_for_sale", " ")
Next 

dw_marketing_buffer.GroupCalc()
dw_marketing_buffer.ScrollToRow(ll_row)
dw_marketing_buffer.SetColumn( "fab_product_code" )
dw_marketing_buffer.SetFocus()
dw_marketing_buffer.SetRedraw(True)

Return
end subroutine

public function boolean wf_update ();int				li_count

long				ll_Row, &
					ll_modrows, &
					ll_delrows

Char				lc_status_ind					

string			ls_division, &
					ls_product

IF dw_marketing_buffer.AcceptText() = -1 Then Return False

ls_division = dw_marketing_buffer_header.GetItemString(1, "division_code")

If iw_frame.iu_string.nf_IsEmpty(ls_division) Then 
	iw_frame.SetMicroHelp("Please enter a division before updating")
	Return False
End If

ll_modrows = dw_marketing_buffer.ModifiedCount()
ll_delrows = dw_marketing_buffer.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then Return False

ib_updating = True
If Not This.wf_check_required() Then 
	ib_updating = False
	Return False
End If
is_update_string = ""
ii_rec_count = 0

For li_count = 1 To ll_delrows 

	lc_status_ind = 'D'

	If Not This.wf_update_modify(li_count, lc_status_ind, ls_division) Then 
		ib_updating = False
		Return False
	End If

Next

ll_row = 0

For li_count = 1 To ll_modrows

	ll_Row = dw_marketing_buffer.GetNextModified(ll_Row, Primary!) 

	If Not wf_validate(ll_row) Then 
		ib_updating = False
		Return False
	End If

	Choose Case dw_marketing_buffer.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			lc_status_ind = 'A'
		CASE DataModified!
			lc_status_ind = 'M'
	END CHOOSE	

	If Not This.wf_update_modify(ll_row, lc_status_ind, ls_division) Then 
		ib_updating = False
		Return False
	End If

Next


//dw_marketing_buffer.SetRedraw(False)
If ii_rec_count > 0 Then
	IF Not iu_ws_pas_share.nf_pasp44fr(istr_error_info, ls_division, &
												is_update_string) Then 
		ib_updating = False
		dw_marketing_buffer.SetRedraw(True)
		Return False
	End If
End If

ib_updating = False
iw_frame.SetMicroHelp("Modification Successful")
dw_marketing_buffer.ResetUpdate()
dw_marketing_buffer.SetColumn("buffer_pct")
dw_marketing_buffer.SetRedraw(True)

Return( True )
end function

public subroutine wf_delete ();Integer				li_count

Long					ll_last_group_row, &
						ll_row, &
						ll_end_row
dwItemStatus		ldwis_status

ll_row = dw_marketing_buffer.GetRow()

If ll_row < 1 Then Return

Do While ll_row <> dw_marketing_buffer.FindGroupChange(ll_row, 1) 
	ll_row -- 
Loop

ll_end_row = dw_marketing_buffer.FindGroupChange(ll_row + 1, 1)
If ll_end_row = 0 Then
	ll_end_row = dw_marketing_buffer.RowCount()	
End If

ldwis_status = dw_marketing_buffer.GetitemStatus(ll_row, 0, primary!)
If IsDate(dw_marketing_buffer_header.GetItemString(1, "week_end_date")) &
		And (dw_marketing_buffer.GetitemStatus &
				(ll_row, 0, primary!) = DataModified! &
		Or dw_marketing_buffer.GetitemStatus &
				(ll_row, 0, primary!) = NotModified!) Then
	MessageBox("Deleting a Product", "All dates for a product " + &
			"must be deleted.  Inquire on the product again " + &
			"and select a blank date, then " + &
			"delete the product.")
	Return	
End If	

If MessageBox("Delete", "This will delete all the rows for product " + &
		Trim(dw_marketing_buffer.GetItemString(ll_row, "fab_product_code")) + &
		".  Are you sure you want to do this?", Question!, YesNo!) &
		= 2 Then Return

dw_marketing_buffer.SetRedraw(False)
dw_marketing_buffer.ScrollToRow(ll_row)

For li_count = ll_row to ll_end_row
	dw_marketing_buffer.DeleteRow(0)
Next

dw_marketing_buffer.SetRedraw(True)

end subroutine

public function boolean wf_check_required ();Long			ll_row

Integer		li_colnbr

String		ls_colname, &
				ls_textname

dw_marketing_buffer.SetRedraw(False)

dw_marketing_buffer.uf_required(true)

li_colnbr = 0

ll_Row = 1
DO WHILE ll_Row <> 0
	// If there's an error, exit
	IF dw_marketing_buffer.FindRequired(Primary!,	&
			ll_Row, li_ColNbr, ls_ColName, FALSE ) < 0 &
			And ls_ColName <> "sequence" THEN 
		uf_required ( dw_marketing_buffer, false )
		li_colnbr = dw_marketing_buffer.GetColumn ( )
		dw_marketing_buffer.Setcolumn("buffer_pct")
		dw_marketing_buffer.Setcolumn(li_colnbr)
		dw_marketing_buffer.SetRedraw(True)
		Return( False )
	END IF

	// If a row was found, save the row and column
	IF ll_Row <> 0 THEN
		// Get the text of that column's label.
		ls_TextName = ls_colname + "_t.Text"
		ls_colname = dw_marketing_buffer.Describe(ls_TextName)
		// Tell the user which column to fill in.
		MessageBox(ls_colname,  "Please enter a value for '"  &
			+ ls_colname + "', row " + String(ll_Row) + ".")
		// Make the problem column current.
		uf_required ( dw_marketing_buffer, false )
		dw_marketing_buffer.SetColumn(li_ColNbr)
		dw_marketing_buffer.ScrollToRow(ll_Row)
		dw_marketing_buffer.SetFocus()
		dw_marketing_buffer.SetRedraw(True)
		Return( False )
	END IF
	// If no row was found, drop out of the loop
LOOP

// There is a bug with the edit mask in PB 4.  Resets Focus.  rem

uf_required ( dw_marketing_buffer, false )

li_colnbr = dw_marketing_buffer.GetColumn ( )
dw_marketing_buffer.Setcolumn("buffer_pct")
dw_marketing_buffer.Setcolumn(li_colnbr)
dw_marketing_buffer.SetRedraw(True)

Return (True)
end function

public function boolean wf_validate (integer al_row);If iw_frame.iu_string.nf_IsEmpty(dw_marketing_buffer.GetItemString &
		(al_row, "fab_product_code")) Then 
	dw_marketing_buffer.ScrollToRow(al_row)
	dw_marketing_buffer.SetColumn("fab_product_code")
	dw_marketing_buffer.TriggerEvent (Itemerror!)
	Return False
End If

If dw_marketing_buffer.GetItemDecimal(al_row,"buffer_pct") < &
		dw_marketing_buffer.GetItemDecimal(al_row,"stop_pct") Then
	dw_marketing_buffer.ScrollToRow(al_row)
	dw_marketing_buffer.SetColumn("buffer_pct")
	dw_marketing_buffer.TriggerEvent (Itemerror!)
	Return False
End If

If dw_marketing_buffer.GetItemDecimal(al_row,"stop_pct") > &
		dw_marketing_buffer.GetItemDecimal(al_row,"buffer_pct") Then
	dw_marketing_buffer.ScrollToRow(al_row)
	dw_marketing_buffer.SetColumn("stop_pct")
	dw_marketing_buffer.TriggerEvent (Itemerror!)
	Return False
End If

Return True
end function

public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_division);Char							lc_status_ind

DWBuffer						ldwb_buffer


IF ac_status_ind = "D" THEN
	ldwb_buffer = delete!
ELSE
	ldwb_buffer = primary!
End If
//01-13 jac added product-status
is_update_string += dw_marketing_buffer.GetItemString &
	(al_row, "fab_product_code",ldwb_buffer, False) + "~t" + &
	dw_marketing_buffer.GetItemString(al_row, "product_state",ldwb_buffer, False) + "~t" + &	
	String(dw_marketing_buffer.GetItemDate &
	(al_row, "week_end_date",ldwb_buffer, False), "yyyy-mm-dd") + "~t" + &
	String(dw_marketing_buffer.GetItemDecimal &
	(al_row,"buffer_pct",ldwb_buffer, False)) + "~t" + &
	String(dw_marketing_buffer.GetItemDecimal &
	(al_row,"stop_pct",ldwb_buffer, False)) + "~t" + &
	String(dw_marketing_buffer.GetItemString &
	(al_row,"notify_alias",ldwb_buffer, False)) + "~t" + &
	String(dw_marketing_buffer.GetItemString &
	(al_row,"product_status",ldwb_buffer, False)) + "~t" + &
	ac_status_ind + "~r~n"
	
	
ii_rec_count ++

If ii_rec_count = 100 Then
	IF Not iu_ws_pas_share.nf_pasp44fr( &
			istr_error_info, &
			as_division, &
			is_update_string) THEN Return False
	ii_rec_count = 0
	is_update_string = ""
END IF

Return True
end function

public function boolean wf_deleterow ();wf_delete()
Return true
end function

public function boolean wf_addrow ();//messagebox('datawindow data', "null" )
wf_filenew()

Return True
end function

public subroutine wf_find_duplicate (long al_row, string as_data, string as_check_field, ref string as_return);Long		ll_result, &
			ll_row_count

String	ls_product_code,ls_product_state, ls_product_status, ls_find


ll_row_count = dw_marketing_buffer.RowCount()
//If al_row < 1 or al_row > ll_row_count Then return ' '

If as_check_field = 'product_state' Then
	ls_product_code = dw_marketing_buffer.getitemstring(al_row,'fab_product_code')//ll_GetText()
	ls_product_state = as_data
//01-13 jac
	ls_product_status = dw_marketing_buffer.getitemstring(al_row,'product_status')
//
Else
	If as_check_field = 'product_code' Then
	ls_product_code = as_data
	ls_product_state = dw_marketing_buffer.getitemstring(al_row,'product_state')
	//01-13 jac
	ls_product_status = dw_marketing_buffer.getitemstring(al_row,'product_status')
   end if 
//Else
	If as_check_field = 'product_status' Then
		ls_product_status = as_data
		ls_product_code = dw_marketing_buffer.getitemstring(al_row,'fab_product_code')//ll_GetText()
	   ls_product_state = dw_marketing_buffer.getitemstring(al_row,'product_state')
	end if 
	//
End If

//ll_result = dw_marketing_buffer.Find("Trim(fab_product_code) = Trim('" + ls_product_code + "')", 1, al_row - 1)
//ls_find = "Trim(fab_product_code) = Trim('" + ls_product_code + "'" + " and product_state = '" + ls_product_state +"')" 

ll_result = dw_marketing_buffer.Find("Trim(fab_product_code) = Trim('" + ls_product_code + "')" + " and product_state = '" + ls_product_state +"'" + " and product_status = '" + ls_product_status +"'", 1, al_row - 1)
If ll_result = 0 Then
	ll_result = dw_marketing_buffer.Find("Trim(fab_product_code) = Trim('" + ls_product_code + "')" + " and product_state = '" + ls_product_state +"'" + " and product_status = '" + ls_product_status +"'", al_row + 1, ll_row_count)
	If ll_result <> 0 Then
		as_return = "Product of " + ls_product_code + " and State of " + ls_product_state + " and Status of " + ls_product_status + " already exists."
		Return //as_return
	End If
Else
	as_return = "Product of " + ls_product_code + " and State of " + ls_product_state + " and Status of " + ls_product_status + " already exists."
	Return //as_return
End If

SetNull(as_return) 

Return //as_return

//If ll_result <> 0 Then return ll_result

//If al_row = ll_row_count Then return 0
//return dw_marketing_buffer.Find("Trim(fab_product_code) = Trim('" + ls_product_code + "')", &
	//					al_row + 1, ll_row_count)


end subroutine

event ue_postopen;call super::ue_postopen;Integer					li_count, &
							li_PARange, &
							li_rc

DataWindowChild		ldwc_week_end

Date						ldt_week_end[]

String					ls_PARange, &
							ls_week_end


iu_pas203 = create u_pas203
iu_ws_pas_share = Create u_ws_pas_share
If Message.ReturnValue = -1 Then
	Close(This)
	Return
End If

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "marketbuf"
istr_error_info.se_user_id				= sqlca.userid

// get the number of valid days for PA
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if

ldt_week_end[1] = RelativeDate(Today(), 1 - DayNumber( &
			Today())) 
ls_week_end = "          " + "~r~n"
ls_week_end += String(ldt_week_end[1], "mm/dd/yyyy")
//dmk add 1 to pa range before divide, same as PAS315YC
ii_number_of_dates = ((Integer(ls_PARange) + 1) / 7) + 2
For li_count = 2 to ii_number_of_dates
	ldt_week_end[li_count] = RelativeDate(ldt_week_end[li_count - 1], 7)
	ls_week_end += "~r~n" + String(ldt_week_end[li_count], "mm/dd/yyyy")
Next

li_rc	=	dw_marketing_buffer_header.GetChild( "week_end_date", ldwc_week_end )
ldwc_week_end.Reset()
li_rc = ldwc_week_end.ImportString(ls_week_end)


//ls_state = dw_marketing_buffer.GetChild("product_state", ldwc_week_end)
//dw_marketing_buffer.ImportString(ls_grade)


This.PostEvent('ue_query')
end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_ws_pas_share ) THEN
	DESTROY iu_ws_pas_share
END IF
end event

event ue_addrow;DataWindowChild			ldwc_week_end

Integer						li_count, &
								li_sequence

Long							ll_row

String						ls_string


If iw_frame.iu_string.nf_IsEmpty(dw_marketing_buffer_header.GetItemString &
		(1, "division_code")) Then
	iw_frame.SetMicroHelp("Please reinquire and enter a Division before adding a row.")
	Return 
End If

ll_row = dw_marketing_buffer.RowCount() + 1
dw_marketing_buffer.SetRedraw(False)

dw_marketing_buffer_header.GetChild("week_end_date", ldwc_week_end)
ls_string = ldwc_week_end.Describe("DataWindow.Data")
dw_marketing_buffer.ImportString(ls_string, 2, 0, 0, 0, 4) 

If ll_row = 1 Then
	li_sequence = 1
Else	
	li_sequence = dw_marketing_buffer.GetitemNumber(ll_row - 1, &
			"sequence") + 1
End If

li_count = ll_row
For li_count = li_count to dw_marketing_buffer.RowCount()
	dw_marketing_buffer.SetItem(li_count, "fab_product_code", " ")
	dw_marketing_buffer.SetItem(li_count, "product_state", "1")
//12-12 jac
		dw_marketing_buffer.SetItem(li_count, "product_status", "G")
//
	dw_marketing_buffer.SetItem(li_count, "product_short_descr", " ")
	dw_marketing_buffer.SetItem(li_count, "sequence", li_sequence)
	dw_marketing_buffer.SetItem(li_count, "sold_pct", 0)
	dw_marketing_buffer.SetItem(li_count, "avail_for_sale", " ")
Next 

dw_marketing_buffer.GroupCalc()
dw_marketing_buffer.ScrollToRow(ll_row)
dw_marketing_buffer.SetColumn( "fab_product_code" )
dw_marketing_buffer.SetFocus()
dw_marketing_buffer.SetRedraw(True)

Return 0
end event

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
end on

on w_marketing_buffer.create
int iCurrent
call super::create
this.dw_marketing_buffer=create dw_marketing_buffer
this.dw_marketing_buffer_header=create dw_marketing_buffer_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_marketing_buffer
this.Control[iCurrent+2]=this.dw_marketing_buffer_header
end on

on w_marketing_buffer.destroy
call super::destroy
destroy(this.dw_marketing_buffer)
destroy(this.dw_marketing_buffer_header)
end on

event resize;call super::resize;integer li_x                     = 51
integer li_y                     = 355
//
//dw_marketing_buffer.width  = width - (50 + li_x)
//dw_marketing_buffer.height = height - (125 + li_y)
//

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_marketing_buffer.width  = width - (li_x)
dw_marketing_buffer.height = height - (li_y)


end event

event ue_deleterow;Integer				li_count

Long					ll_last_group_row, &
						ll_row, &
						ll_end_row
dwItemStatus		ldwis_status

ll_row = dw_marketing_buffer.GetRow()

If ll_row < 1 Then Return

Do While ll_row <> dw_marketing_buffer.FindGroupChange(ll_row, 1) 
	ll_row -- 
Loop

ll_end_row = dw_marketing_buffer.FindGroupChange(ll_row + 1, 1)
If ll_end_row = 0 Then
	ll_end_row = dw_marketing_buffer.RowCount()	
End If

ldwis_status = dw_marketing_buffer.GetitemStatus(ll_row, 0, primary!)
If IsDate(dw_marketing_buffer_header.GetItemString(1, "week_end_date")) &
		And (dw_marketing_buffer.GetitemStatus &
				(ll_row, 0, primary!) = DataModified! &
		Or dw_marketing_buffer.GetitemStatus &
				(ll_row, 0, primary!) = NotModified!) Then
	MessageBox("Deleting a Product", "All dates for a product " + &
			"must be deleted.  Inquire on the product again " + &
			"and select a blank date, then " + &
			"delete the product.")
	Return	
End If	

If MessageBox("Delete", "This will delete all the rows for product " + &
		Trim(dw_marketing_buffer.GetItemString(ll_row, "fab_product_code")) + &
		".  Are you sure you want to do this?", Question!, YesNo!) &
		= 2 Then Return

dw_marketing_buffer.SetRedraw(False)
dw_marketing_buffer.ScrollToRow(ll_row)

For li_count = ll_row to ll_end_row
	dw_marketing_buffer.DeleteRow(0)
Next

dw_marketing_buffer.SetRedraw(True)

end event

event ue_fileprint;call super::ue_fileprint;//dw_marketing_buffer.print()
String						ls_mod, &
								ls_temp, &
								ls_week_end_date

DataStore					lds_print


lds_print = create u_print_datastore
lds_print.DataObject = 'd_marketing_buffer_report'


dw_marketing_buffer.sharedata(lds_print)

ls_temp = lds_print.modify(ls_mod)

lds_print.print()





end event

event ue_get_data;Long	ll_window_x,ll_window_y,ll_datawindow_x,ll_datawindow_y


CHOOSE CASE as_value
	CASE 'data'
		message.stringparm = is_data
	Case 'xy_values'
		ll_window_x = This.PointerX()
		ll_window_y = This.PointerY()
		ll_datawindow_x = dw_marketing_buffer.PointerX() + &
				Long(dw_marketing_buffer.object.product_state.width)
		ll_datawindow_y = dw_marketing_buffer.PointerY()
		message.StringParm = String(ll_datawindow_x + (ll_window_x - ll_window_x)) + '~t' + &
				String(ll_datawindow_y + (ll_window_y - ll_datawindow_y)) + '~t'

END CHOOSE

end event

type dw_marketing_buffer from u_netwise_dw within w_marketing_buffer
integer y = 188
integer width = 3017
integer height = 1180
integer taborder = 10
string dataobject = "d_marketing_buffer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;Boolean	lb_ret

Integer	li_count

long		ll_row, & 
			ll_tab1, &
			ll_tab2, &
			ll_last_group_row, &
			ll_last_row, &
			ll_first_group_row, &
			ll_find_row

Decimal	ldc_value, &
			ldc_SoldPct, &
			ldc_BufPct

String	ls_colnam, &
			ls_text, &
			ls_division, &
			ls_output, &
			ls_product_short_descr, ls_return

ll_row = GetRow()
ls_text = GetText()
ldc_value = Dec(ls_text)

Choose Case GetColumnName()

	Case	"product_state"
		wf_find_duplicate(This.GetRow(),data,'product_state',ls_return)
		If not isnull(ls_return) Then 
		//01-13 jac	MessageBox("Duplicate Product and Product State", ls_return)
			MessageBox("Duplicate Product, Product State and Status", ls_return)
			This.SelectText ( 1, 100 )
			return 1
		End if
		This.SetRedraw(False)

		ll_first_group_row = ll_row
		Do While ll_first_group_row <> FindGroupChange &
				(ll_first_group_row, 1)
			ll_first_group_row --
		Loop
 
		ll_last_group_row = FindGroupChange(ll_row + 1, 1)
		If ll_last_group_row = 0 Then
			ll_last_group_row = dw_marketing_buffer.RowCount()
		Else
			ll_last_group_row -= 1
		End If
		li_count = 1
		For li_count = li_count to ii_number_of_dates - 1
			ll_row ++
			IF ll_row > ll_last_group_row Then &
						ll_row = ll_first_group_row
			This.SetItem(ll_row, "product_state", ls_text)
		Next

		This.SetRedraw(True)
	//12-12 jac
		Case	"product_status"
		wf_find_duplicate(This.GetRow(),data,'product_status',ls_return)
		If not isnull(ls_return) Then 
		//01-13 jac	MessageBox("Duplicate Product and Product Status", ls_return)
			MessageBox("Duplicate Product, Product State and Status", ls_return)
			This.SelectText ( 1, 100 )
			return 1
		End if
		This.SetRedraw(False)

		ll_first_group_row = ll_row
		Do While ll_first_group_row <> FindGroupChange &
				(ll_first_group_row, 1)
			ll_first_group_row --
		Loop
 
		ll_last_group_row = FindGroupChange(ll_row + 1, 1)
		If ll_last_group_row = 0 Then
			ll_last_group_row = dw_marketing_buffer.RowCount()
		Else
			ll_last_group_row -= 1
		End If
		li_count = 1
		For li_count = li_count to ii_number_of_dates - 1
			ll_row ++
			IF ll_row > ll_last_group_row Then &
						ll_row = ll_first_group_row
			This.SetItem(ll_row, "product_status", ls_text)
		Next

		This.SetRedraw(True)
  //
	Case	"fab_product_code"
		If iw_frame.iu_string.nf_IsEmpty(ls_text) then
			Return 2
		End If	
		wf_find_duplicate(This.GetRow(),data,'product_code',ls_return)
		If not isnull(ls_return) Then			
		//12-12 jac	MessageBox("Duplicate Product and Product State", ls_return)
			MessageBox("Duplicate Product, Product State and Status", ls_return)
			//"Product and State of " + ls_text + 	" already exists.")
			This.SelectText ( 1, 100 )
			return 1
		End if

		This.SetRedraw(False)

		istr_error_info.se_event_name = "Itemchanged for dw_marketing_buffer"
		istr_error_info.se_procedure_name = "pasp45fr"
		istr_error_info.se_message = Space(70)
		istr_error_info.SE_WINDOW_NAME = 'MARKBUFF'
		
												
			lb_ret = iu_ws_pas_share.nf_pasp45fr(istr_error_info, &
												ls_text, &
												ls_output)										
												
												
												
		If not lb_ret then 
			This.SetFocus()
			This.SelectText(1,len(GetText()))
			This.SetRedraw(True)
			return 1
		End if			
		ls_division = dw_marketing_buffer_header.GetItemString( &
				1, "division_code")
	
		ll_tab1 = pos(ls_output, "~t", 1) + 1
		ll_tab2 = pos(ls_output, "~t", ll_tab1 +1) 
// What the H*** is this?????		
//		If Right(ls_output, 2) = "~r~n" Then
//			ll_tab2 = len(ls_output) - ll_tab1 - 1
//		Else
//			ll_tab2 = len(ls_output) - ll_tab1 + 1
//		End If
		If Not Left(ls_output, 2) = ls_division Then
			iw_frame.SetMicroHelp("Invalid Product for Division " + &
					ls_division)
			SelectText(1, Len(ls_text))
			This.SetRedraw(True)
			Return 1
		End IF

		ls_product_short_descr = Mid(ls_output, ll_tab1, ll_tab2 - ll_tab1)
		This.SetItem(ll_row, "product_short_descr", ls_product_short_descr)

		ll_first_group_row = ll_row
		Do While ll_first_group_row <> FindGroupChange &
				(ll_first_group_row, 1)
			ll_first_group_row --
		Loop
 
		ll_last_group_row = FindGroupChange(ll_row + 1, 1)
		If ll_last_group_row = 0 Then
			ll_last_group_row = dw_marketing_buffer.RowCount()
		Else
			ll_last_group_row -= 1
		End If
		li_count = 1
		For li_count = li_count to ii_number_of_dates - 1
			ll_row ++
			IF ll_row > ll_last_group_row Then &
						ll_row = ll_first_group_row
			This.SetItem(ll_row, "fab_product_code", ls_text)
			This.SetItem(ll_row, "product_short_descr", &
					ls_product_short_descr)
		Next
		This.SetRedraw(True)
			
	CASE "buffer_pct"
		If ldc_value < 0 or ldc_value > 100.00 Then
			SelectText(1, Len(ls_text))
			Return 1
		End If
		If GetItemStatus(ll_row, "stop_pct", primary!) = &
				DataModified! Then
			If ldc_value < This.GetItemDecimal(ll_row, "stop_pct") Then
				SelectText(1, Len(ls_text))
				Return 1
			End If
		End If

	CASE "stop_pct"
		If ldc_value < 0 or ldc_value > 100.00 Then
			SelectText(1, Len(ls_text))
			Return 1
		End If
		If GetItemStatus(ll_row, "buffer_pct", primary!) = &
				DataModified! Then
			// This is a workaround to a bug in PB 5.0.1 w/ GetItemDecimal
			If IsNull(This.GetItemNumber(ll_row,"buffer_pct")) Then
				ldc_BufPct = 0
			Else
				ldc_BufPct = This.GetItemDecimal(ll_row, "buffer_pct")
			End If
			If ldc_value > ldc_BufPct Then
				SelectText(1, Len(ls_text))
				Return 1
			End If
		End If
		// This is a workaround to a bug in PB 5.0.1 w/ GetItemDecimal
		If IsNull(This.GetItemNumber(row, 'sold_pct')) Then
			ldc_SoldPct = 0
		Else
			ldc_SoldPct = This.GetItemDecimal(row, 'sold_pct')
		End if
		If ldc_value + ldc_SoldPct >= 100.00 Then
			This.SetItem(row, 'avail_for_sale', 'N')
		Else
			This.SetItem(row, 'avail_for_sale', 'Y')
		End if

	Case "notify_alias"
		ll_first_group_row = ll_row
		Do While ll_first_group_row <> FindGroupChange &
				(ll_first_group_row, 1)
			ll_first_group_row --
		Loop
 
		ll_last_group_row = FindGroupChange(ll_row + 1, 1)
		If ll_last_group_row = 0 Then
			ll_last_group_row = dw_marketing_buffer.RowCount()
		Else
			ll_last_group_row -= 1
		End If

		Choose Case ll_row
			Case ll_first_group_row
				ll_find_row = dw_marketing_buffer.Find( &
					"Not (IsNull(notify_alias) or (Len(Trim(notify_alias))=0))", &
					ll_row + 1, ll_last_group_row)

			Case (ll_first_group_row + 1) to (ll_last_group_row - 1)
				ll_find_row = dw_marketing_buffer.Find( &
					"Not (IsNull(notify_alias) or (Len(Trim(notify_alias))=0))", &
					ll_row - 1, ll_first_group_row)
				If ll_find_row = 0 Then ll_find_row = &
						dw_marketing_buffer.Find( &
					"Not (IsNull(notify_alias) or (Len(Trim(notify_alias))=0))", &
					ll_row + 1, ll_last_group_row)

			Case ll_last_group_row
				ll_find_row = dw_marketing_buffer.Find( &
				"Not (IsNull(notify_alias) or (Len(Trim(notify_alias))=0))", &
						ll_row - 1, ll_first_group_row)
		End Choose

		If ll_find_row = 0 Then 

			li_count = 1
			For li_count = li_count to ii_number_of_dates - 1
				ll_row ++
				IF ll_row > ll_last_group_row Then &
						ll_row = ll_first_group_row
				This.SetItem(ll_row, "notify_alias", ls_text)
			Next
		Else
			This.SetItem(ll_row, "notify_alias", ls_text)
		End IF
END CHOOSE

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end event

event itemerror;call super::itemerror;String		ls_text, &
				ls_error_msg, &
				ls_error_ttl
Long			ll_row
Decimal		ldc_value

ls_text = GetText()
ll_row = This.GetRow()

If dw_marketing_buffer.GetItemStatus(ll_row, &
			 0, Primary!) = New! Then Return

CHOOSE CASE GetColumnName()
	Case "fab_product_code"
		If ib_updating Then
			ls_error_msg = "Please enter a Product code"
			ls_error_ttl = "Product Code"
		Else
			Return 1
		End If
	Case "product_state"
		If ib_updating Then
			ls_error_msg = "Please enter a Product State"
			ls_error_ttl = "Product State"
		Else
			Return 1
		End If
	//12-12 jac
		Case "product_status"
		If ib_updating Then
			ls_error_msg = "Please enter a Product Status"
			ls_error_ttl = "Product Status"
		Else
			Return 1
		End If
	//
	CASE "buffer_pct"
		ldc_value = Dec(ls_text)
		If ldc_value < 0 or ldc_value > 100.00 Then
			ls_error_msg = "Buffer Percent must be" + &
				" between 0 and 100"
		Else
			If ldc_value < This.GetItemDecimal(ll_row, "stop_pct") Then
				ls_error_msg = "Buffer Percent must be" + &
						" greater than or equal to Stop Percent"
			End If
			ls_error_ttl = "Buffer Percent"
		End If

	CASE "stop_pct"
		ldc_value = Dec(ls_text)
		If ldc_value < 0 or ldc_value > 100 Then
			ls_error_msg = "Stop Percent must be" + &
				" between 0 and 100"
			ls_error_ttl = "Stop Percent"
		Else
			If ldc_value > This.GetItemDecimal(ll_row, "buffer_pct") Then
				ls_error_msg = "Stop Percent must be" + &
						" less than or equal to Buffer Percent"
				ls_error_ttl = "Stop Percent"
			End If
		End If

END CHOOSE

If ib_updating Then
	MessageBox(ls_error_ttl, ls_error_msg + ".")
	ib_updating = False
Else
	iw_frame.SetMicroHelp(ls_error_msg) 
End if
Return 1


end event

on itemfocuschanged;call u_netwise_dw::itemfocuschanged;SelectText(1, Len(GetText()))
end on

event constructor;call super::constructor;DataWindowChild		ldwc_state


is_selection = '1'
ib_updateable = True
ib_updating = False

This.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

//12-12 jac
This.GetChild('product_status', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRODSTAT")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()
//
end event

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_code


Choose Case dwo.name
		
	Case 'product_state'
			If ii_LastRow = row Then return
		  ls_code = This.GetItemString(row, 'product_state')
		  
		  is_data = ""
  SELECT tutltypes.type_desc  
    INTO :is_data  
    FROM tutltypes  
   WHERE (tutltypes.type_code = :ls_code)	AND  
         (tutltypes.record_type = 'PRDSTATE');

		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
		This.SetFocus()
		ii_LastRow = row
		If iw_frame.iu_string.nf_IsEmpty(is_data) Then Return
		OpenWithParm(w_order_detail_tooltip, Parent, Parent)
//01-13 jac		
		Case 'product_status'
			If ii_LastRow = row Then return
		  ls_code = This.GetItemString(row, 'product_status')
		  
		  is_data = ""
  SELECT tutltypes.type_desc  
    INTO :is_data  
    FROM tutltypes  
   WHERE (tutltypes.type_code = :ls_code)	AND  
         (tutltypes.record_type = 'PRODSTAT');

		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
		This.SetFocus()
		ii_LastRow = row
		If iw_frame.iu_string.nf_IsEmpty(is_data) Then Return
		OpenWithParm(w_order_detail_tooltip, Parent, Parent)
//
	Case Else
		ii_LastRow = 0
		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
End Choose

end event

type dw_marketing_buffer_header from u_netwise_dw within w_marketing_buffer
integer width = 2414
integer height = 192
integer taborder = 0
string dataobject = "d_marketing_buffer_header"
boolean border = false
end type

on constructor;call u_netwise_dw::constructor;This.insertrow(0)

ib_updateable = False
end on

