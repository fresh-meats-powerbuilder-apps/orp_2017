HA$PBExportHeader$w_sold_by_product_group.srw
forward
global type w_sold_by_product_group from w_netwise_sheet
end type
type dw_product_productgroup from u_product_productgroup within w_sold_by_product_group
end type
type dw_loads_boxes from u_loads_boxes within w_sold_by_product_group
end type
type dw_sold_by_detail from datawindow within w_sold_by_product_group
end type
type dw_sold_by_prod_group_info from datawindow within w_sold_by_product_group
end type
type dw_product_code_short from u_product_code_short within w_sold_by_product_group
end type
end forward

global type w_sold_by_product_group from w_netwise_sheet
integer x = 27
integer y = 192
integer width = 2894
integer height = 1556
string title = "Sold By Product/Product Group"
long backcolor = 67108864
long il_borderpaddingwidth = 100
dw_product_productgroup dw_product_productgroup
dw_loads_boxes dw_loads_boxes
dw_sold_by_detail dw_sold_by_detail
dw_sold_by_prod_group_info dw_sold_by_prod_group_info
dw_product_code_short dw_product_code_short
end type
global w_sold_by_product_group w_sold_by_product_group

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
u_orp204	                iu_orp204
u_ws_orp4				  iu_ws_orp4
s_error		istr_error_info

Long		il_color

Long		il_SelectedColor

Long		il_SelectedTextColor

Boolean		ib_inquire, &
		ib_reinquire, &
                                 ib_first_time_thru

Integer		ii_max_page_number

string		is_inquire_window_name, &
                                is_productgroup, &
                                is_product_group_id, &
                                is_product_group_desc, &
                                is_product_owner, &
                                is_product_code, &
                                is_product_desc

Datastore		ids_report

w_netwise_response	iw_inquirewindow, &
			iw_parentwindow
end variables

forward prototypes
public subroutine wf_format ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_format ();string	ls_temp

if dw_loads_boxes.GetItemString(1,"boxes_loads") = 'B' Then
	ls_temp = dw_sold_by_detail.Modify("dailysum1.Format='##,##0' " + &
			"dailysum2.Format='##,##0' " + &
			"dailysum3.Format='##,##0' " + &
			"dailysum4.Format='##,##0' " + &
			"dailysum5.Format='##,##0' " + &
			"dailysum6.Format='##,##0' " + &
			"dailysum7.Format='##,##0' " + &
			"dailysum8.Format='##,##0' " + &
			"dailysum9.Format='##,##0' " + &
			"dailysum10.Format='##,##0' " + &
			"dailysum11.Format='##,##0' " + &
			"dailysum12.Format='##,##0' " + &
			"dailysum13.Format='##,##0' " + &
			"dailysum14.Format='##,##0' " + &
			"dailysum15.Format='##,##0' " + &
			"dailysum16.Format='##,##0' " + &
			"dailysum17.Format='##,##0' " + &
			"dailysum18.Format='##,##0' " + &
			"dailysum19.Format='##,##0' " + &
			"dailysum20.Format='##,##0' ") 
Else
	ls_temp = dw_sold_by_detail.Modify("dailysum1.Format='##0.00' " + &
			"dailysum2.Format='##0.00' " + &			
			"dailysum3.Format='##0.00' " + &			
			"dailysum4.Format='##0.00' " + &			
			"dailysum5.Format='##0.00' " + &			
			"dailysum6.Format='##0.00' " + &			
			"dailysum7.Format='##0.00' " + &			
			"dailysum8.Format='##0.00' " + &			
			"dailysum9.Format='##0.00' " + &			
			"dailysum10.Format='##0.00' " + &			
			"dailysum11.Format='##0.00' " + &			
			"dailysum12.Format='##0.00' " + &			
			"dailysum13.Format='##0.00' " + &			
			"dailysum14.Format='##0.00' " + &			
			"dailysum15.Format='##0.00' " + &			
			"dailysum16.Format='##0.00' " + &			
			"dailysum17.Format='##0.00' " + &			
			"dailysum18.Format='##0.00' " + &			
			"dailysum19.Format='##0.00' " + &			
			"dailysum20.Format='##0.00' ")
	End If
end subroutine

public function boolean wf_retrieve ();long		   ll_rec_count, &
				ll_row

string		ls_productgroup, &  
            ls_productgroup_desc, &
				ls_product, &
				ls_product_desc, & 
				ls_loads_boxes, &
				ls_product_productgroup, &
				ls_temp, &
				ls_weekly_data, &
				ls_header, &
				ls_plant, &
				ls_plant_desc
 
u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

IF message.StringParm = 'Abort' Then Return False
	
SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.... Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_orpo88fr"
istr_error_info.se_message = Space(71)

ls_product_productgroup = dw_product_productgroup.uf_get_product_productgroup()
 
ls_loads_boxes = dw_loads_boxes.uf_get_loads_boxes()
ls_product = dw_product_code_short.uf_getproductcode()

ls_header = ls_product_productgroup + '~t' + &
				ls_loads_boxes + '~t' + &
				ls_product + '~t' + &
				is_product_group_id + '~r~n' 

dw_sold_by_detail.reset()

If iu_ws_orp4.nf_ORPO88FR(istr_error_info, & 
										ls_header, &
										ls_weekly_data) < 0 Then
										This.SetRedraw(True) 
										Return False
End If										

//ls_weekly_data = '004~t100.0~t200.0~t300.0~t400.0~t500.0~t600.0~t700.0~t800.0~t900.0~t1.0~t' + &
//							          '100.0~t200.0~t300.0~t400.0~t500.0~t600.0~t700.0~t800.0~t900.0~t1.0~r~n' + &
//					  '011~t100.0~t200.0~t300.0~t400.0~t500.0~t600.0~t700.0~t800.0~t900.0~t2.0~t' + &
//							          '100.0~t200.0~t300.0~t400.0~t500.0~t600.0~t700.0~t800.0~t900.0~t2.0~r~n' + &
//					  '026~t100.0~t200.0~t300.0~t400.0~t500.0~t600.0~t700.0~t800.0~t900.0~t3.0~t' + &
//							          '100.0~t200.0~t300.0~t400.0~t500.0~t600.0~t700.0~t800.0~t900.0~t3.0~r~n'
// 
If Not lu_string.nf_IsEmpty(ls_weekly_data) Then
	ll_rec_count = dw_sold_by_detail.ImportString(ls_weekly_Data)
	If ll_rec_count > 0 Then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

FOR ll_row = 1 TO ll_rec_count
	ls_plant = dw_sold_by_detail.getitemstring(ll_row, 'plant')
	select location_name
	into :ls_plant_desc
	from locations
	where location_code = :ls_plant;
	dw_sold_by_detail.Setitem(ll_row, 'plant_name', ls_plant_desc)
NEXT 

wf_format()

//// Must do because of the reinquire to set the scroll bar
dw_sold_by_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_sold_by_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_sold_by_detail.Object.DataWindow.HorizontalScrollSplit = '1093'
dw_sold_by_detail.Object.DataWindow.HorizontalScrollPosition2 =  '1093'

This.SetRedraw(True) 

dw_sold_by_detail.ResetUpdate()
dw_sold_by_detail.SetFocus()
 
Return True

end function

on w_sold_by_product_group.create
int iCurrent
call super::create
this.dw_product_productgroup=create dw_product_productgroup
this.dw_loads_boxes=create dw_loads_boxes
this.dw_sold_by_detail=create dw_sold_by_detail
this.dw_sold_by_prod_group_info=create dw_sold_by_prod_group_info
this.dw_product_code_short=create dw_product_code_short
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_productgroup
this.Control[iCurrent+2]=this.dw_loads_boxes
this.Control[iCurrent+3]=this.dw_sold_by_detail
this.Control[iCurrent+4]=this.dw_sold_by_prod_group_info
this.Control[iCurrent+5]=this.dw_product_code_short
end on

on w_sold_by_product_group.destroy
call super::destroy
destroy(this.dw_product_productgroup)
destroy(this.dw_loads_boxes)
destroy(this.dw_sold_by_detail)
destroy(this.dw_sold_by_prod_group_info)
destroy(this.dw_product_code_short)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')

iw_frame.im_menu.mf_enable('m_print')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')

end event

event resize;call super::resize;integer li_x		= 57
integer li_y		= 318


//  
//dw_sold_by_detail.width	= newwidth - (30 + li_x)
//dw_sold_by_detail.height	= newheight - (30 + li_y)

// Must do because of the split horizonal bar 
dw_sold_by_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_sold_by_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_sold_by_detail.Object.DataWindow.HorizontalScrollSplit = '1093'
dw_sold_by_detail.Object.DataWindow.HorizontalScrollPosition2 =  '1093'


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight >  li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_sold_by_detail.width  = newwidth - (li_x)
dw_sold_by_detail.height = newheight - (li_y)


end event

event ue_fileprint;call super::ue_fileprint;String						ls_mod, &
								ls_temp, &
								ls_week_end_date, &
								ls_detail_string
Long							ll_rtn, ll_rowcount

DataStore					lds_print


lds_print = create u_print_datastore
lds_print.DataObject = 'd_sold_by_report'

If dw_product_productgroup.uf_get_product_productgroup() = 'P' then
	lds_print.object.product_text.visible = true
	lds_print.object.product_t.visible = true
	lds_print.object.product_desc_t.visible = true
	lds_print.object.dash_t.visible = false
	lds_print.object.productgroup_text.visible = false
	lds_print.object.productgroup_t.visible = false
	lds_print.object.productgroup_descr_t.visible = false
	lds_print.object.product_t.text =  &
							dw_product_code_short.uf_getproductcode()
	lds_print.object.product_desc_t.text = &
							dw_product_code_short.uf_getproductdesc()
Else
	lds_print.object.product_text.visible = false
	lds_print.object.product_t.visible = false
	lds_print.object.product_desc_t.visible = false
	lds_print.object.dash_t.visible = true
	lds_print.object.productgroup_text.visible = true
	lds_print.object.productgroup_t.visible = true
	lds_print.object.productgroup_descr_t.visible = true
	lds_print.object.productgroup_t.text = is_product_owner
	lds_print.object.productgroup_descr_t.text = is_product_group_desc	
End if
//ll_rowcount = dw_sold_by_detail.rowcount()
ls_detail_string = dw_sold_by_detail.object.datawindow.data
ll_rtn = lds_print.importstring(ls_detail_string)
//ll_rowcount = lds_print.rowcount()
//dw_sold_by_detail.sharedata(lds_print)

If dw_loads_boxes.uf_Get_loads_boxes() = 'B' Then
	lds_print.object.report_type_t.text = 'Boxes'	
	ls_mod ="daily1.Format='##,##0' " + &
			"daily2.Format='##,##0' " + &
			"daily3.Format='##,##0' " + &
			"daily4.Format='##,##0' " + &
			"daily5.Format='##,##0' " + &
			"daily6.Format='##,##0' " + &
			"daily7.Format='##,##0' " + &
			"daily8.Format='##,##0' " + &
			"daily9.Format='##,##0' " + &
			"daily10.Format='##,##0' " + &
			"daily11.Format='##,##0' " + &
			"daily12.Format='##,##0' " + &
			"daily13.Format='##,##0' " + &
			"daily14.Format='##,##0' " + &
			"daily15.Format='##,##0' " + &
			"daily16.Format='##,##0' " + &
			"daily17.Format='##,##0' " + &
			"daily18.Format='##,##0' " + &
			"daily19.Format='##,##0' " + &
			"daily20.Format='##,##0' " 
Else
		lds_print.object.report_type_t.text = 'Loads'	
		ls_mod ="daily1.Format='##0.00' " + &
			"daily2.Format='##0.00' " + &			
			"daily3.Format='##0.00' " + &			
			"daily4.Format='##0.00' " + &			
			"daily5.Format='##0.00' " + &			
			"daily6.Format='##0.00' " + &			
			"daily7.Format='##0.00' " + &			
			"daily8.Format='##0.00' " + &			
			"daily9.Format='##0.00' " + &			
			"daily10.Format='##0.00' " + &			
			"daily11.Format='##0.00' " + &			
			"daily12.Format='##0.00' " + &			
			"daily13.Format='##0.00' " + &			
			"daily14.Format='##0.00' " + &			
			"daily15.Format='##0.00' " + &			
			"daily16.Format='##0.00' " + &			
			"daily17.Format='##0.00' " + &			
			"daily18.Format='##0.00' " + &			
			"daily19.Format='##0.00' " + &			
			"daily20.Format='##0.00' "
End If

ls_temp = lds_print.modify(ls_mod)

lds_print.print()

end event

event ue_get_data;string   ls_temp

u_string_functions	lu_string


Choose Case as_value
	Case 'Product'
			message.StringParm = dw_product_code_short.uf_getproductcode()
	Case 'product_group_id'
			message.StringParm = is_product_group_id 
	Case 'product_owner'
		If  ib_first_time_thru Then
			message.StringParm = ' '
		else
			message.StringParm = is_product_owner
		end if 
	Case 'product_productgroup'
		message.StringParm = dw_product_productgroup.uf_get_product_productgroup()
	Case 'loads_boxes'
		message.StringParm = dw_loads_boxes.uf_get_loads_boxes()
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env

dw_product_productgroup.Modify("product_productgroup.Protect=1")
dw_loads_boxes.Modify("boxes_loads.Protect=1")

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

This.PostEvent('ue_query')//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Soldby"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_sold_by_product_group_inq'

ib_first_time_thru = True

iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4

wf_retrieve()
end event

event ue_set_data;string		ls_product_short_descr 


Choose Case as_data_item
	Case 'Product_visible'
		If as_value = 'true' Then
			dw_product_code_short.Visible = True
			dw_sold_by_prod_group_info.Visible = False
		Else
			dw_product_code_short.Visible = False
			dw_sold_by_prod_group_info.Visible = True
		End If
	Case 'Product'
		dw_product_code_short.uf_setproductcode(as_value)
	Case 'prod_group_visible'
		If as_value = 'true' Then
			dw_product_code_short.Visible = False
			dw_sold_by_prod_group_info.Visible = True
		Else
			dw_product_code_short.Visible = True
			dw_sold_by_prod_group_info.Visible = False
		End If
	Case 'loads_boxes'
		dw_loads_boxes.uf_set_loads_boxes(as_value)
	Case 'product_group_desc'
		is_product_group_desc = as_value
		dw_sold_by_prod_group_info.Object.product_group_desc_t.text = is_product_group_desc
	Case 'product_group_id'
		is_product_group_id = as_value
	Case 'product_owner'
		is_product_owner = as_value
		dw_sold_by_prod_group_info.Object.owner_t.Text = is_product_owner
	Case 'product_productgroup'
		dw_product_productgroup.uf_set_product_productgroup(as_value)
End Choose
end event

event close;call super::close;Destroy iu_ws_orp4
end event

type dw_product_productgroup from u_product_productgroup within w_sold_by_product_group
integer x = 32
integer y = 20
integer height = 292
boolean bringtotop = true
end type

event itemchanged;CHOOSE CASE data
	CASE 'P'
		dw_product_code_short.Visible = True
		dw_sold_by_prod_group_info.Visible = False
	CASE 'G'
		dw_product_code_short.Visible = False
      dw_sold_by_prod_group_info.Visible = True
END CHOOSE
end event

type dw_loads_boxes from u_loads_boxes within w_sold_by_product_group
integer x = 585
integer y = 24
integer width = 398
integer height = 288
integer taborder = 20
boolean bringtotop = true
end type

type dw_sold_by_detail from datawindow within w_sold_by_product_group
integer x = 23
integer y = 320
integer width = 2784
integer height = 1068
boolean bringtotop = true
string dataobject = "d_sold_by_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_sold_by_prod_group_info from datawindow within w_sold_by_product_group
boolean visible = false
integer x = 978
integer y = 96
integer width = 1838
integer height = 200
integer taborder = 30
string dataobject = "d_sold_by_prod_group_info"
boolean border = false
boolean livescroll = true
end type

type dw_product_code_short from u_product_code_short within w_sold_by_product_group
integer x = 1230
integer y = 128
integer taborder = 11
boolean bringtotop = true
end type

event constructor;call super::constructor;This.insertrow(0)
This.uf_disable()
end event

