HA$PBExportHeader$w_build_loads_sales_order_defaults.srw
forward
global type w_build_loads_sales_order_defaults from w_netwise_sheet
end type
type ole_hides from olecustomcontrol within w_build_loads_sales_order_defaults
end type
end forward

global type w_build_loads_sales_order_defaults from w_netwise_sheet
int X=5
int Y=4
int Width=1678
int Height=1120
boolean TitleBar=true
string Title="Customer Markup Line Calculations"
long BackColor=79741120
event type boolean ue_save ( )
event type boolean ue_inquire ( )
ole_hides ole_hides
end type
global w_build_loads_sales_order_defaults w_build_loads_sales_order_defaults

type variables
s_error			istr_error_info
string			Is_order_num,&	
			is_title
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

event ue_save;return ole_hides.object.save()

end event

event ue_inquire;boolean blnReturn

setPointer(HourGlass!)
This.Event Trigger CloseQuery()

blnReturn = ole_hides.object.Inquire(True)

ole_hides.SetRedraw ( True )
return  blnReturn
end event

public function boolean wf_retrieve ();boolean blnReturn

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_hides.Inquire"


setPointer(HourGlass!)

blnReturn = ole_hides.object.Inquire(True)

//ole_hides.Resetupdate
return  blnReturn

//if ole_hides.object.DataChanged  AND gw_base_frame.im_base_menu.m_file.m_save.enabled then
//	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
//
//		CASE 1	// Save Changes
//			If ole_hides.object.save(false) = FALSE Then
//				return TRUE
//			End If
//		CASE 2	// Do not save changes
//
//		CASE 3	// Cancel the closing of window
//				return TRUE
//	END CHOOSE
//end if
//
//SetPointer(HourGlass!)
//If Len(trim(is_order_num)) > 0 Then
//// Is_****_**** can then be broken down into the appropriate Variables to Call the ocx controls(in this example all I needed was a Order Number)
//	lbln_Return = ole_hides.object.Inquire(False, is_order_num )
//	// always make sure you clear the Instance Variable for future inquires
//	is_order_num = ''                                                                        
//else	
//	lbln_Return = ole_hides.object.Inquire(true, " ")
//End if
//SetPointer(Arrow!)
//
//return lbln_Return
end function

public function boolean wf_update ();boolean lbln_Return

SetPointer(HourGlass!)

lbln_Return = ole_hides.object.Save()

SetPointer(Arrow!)

return lbln_Return
end function

event activate;call super::activate;boolean	lb_rtn
string	ls_classname, &
			ls_Add_Access,&
			ls_Del_Access,&
			ls_Mod_Access,&
			ls_Inq_Access

// Set Security on ActiveX Control.
ls_classname = UPPER(this.ClassName()) + "_EXT"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
					
IF NOT lb_rtn THEN
	// if the window is not listed in the security at all, then allow no permissions
	ls_Add_Access =  'N'
End if

//Rights to Add New Rows
IF UPPER(ls_Add_Access) = 'N' THEN
	iw_frame.im_menu.mf_Disable('m_addrow')
END IF	

iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

iw_frame.im_menu.mf_Disable('m_generatesales')		
iw_frame.im_menu.mf_Disable('m_complete')		
	
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_new')	
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

iw_frame.im_menu.mf_Enable('m_generatesales')		
iw_frame.im_menu.mf_Enable('m_complete')		


end event

event ue_postopen;call super::ue_postopen;boolean	lb_rtn
string	ls_server_suffix, &
			ls_classname, &
			ls_Add_Access,&
			ls_Del_Access,&
			ls_Mod_Access,&
			ls_Inq_Access

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= This.Title
istr_error_info.se_user_id 		= sqlca.userid

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

if ls_server_suffix = "" then
	ls_server_suffix = " "
end if 


ole_hides.object.UserID = sqlca.userid //User ID
ole_hides.object.Password = sqlca.DBpass //User Password
ole_hides.object.SighOnSystem = ls_server_suffix //Mainframe Region (t,z,p)
ole_hides.object.Application = Message.nf_Get_App_ID() //Application Name
ole_hides.object.Window = This.Title  //WIndow Name


// Set Security on ActiveX Control.
ls_classname = UPPER(this.ClassName()) + "_EXT"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
					
IF NOT lb_rtn THEN
	// if the window is not listed in the security at all, then allow no permissions
	ls_Add_Access =  'N'
	ls_Mod_Access =  'N'
	ls_Inq_Access =  'N'
End if

//Rights to Add New Rows
//IF UPPER(ls_Add_Access) = 'N' THEN
	//ole_hides.object.AddRights = false
//ELSE
//	ole_hides.object.AddRights = true		
//END IF

//Rights to Change Dates
//IF UPPER(ls_Inq_Access) = 'N' THEN
//	ole_hides.object.DateRights = false
//ELSE
//	ole_hides.object.DateRights = true
//END IF

//Rights to change Quanties
//IF UPPER(ls_Mod_Access) = 'N' THEN
//	ole_hides.object.QtyRights = false
//ELSE
//	ole_hides.object.QtyRights = true
//END IF


//Inquire
this.event ue_inquire()
end event

on w_build_loads_sales_order_defaults.create
int iCurrent
call super::create
this.ole_hides=create ole_hides
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_hides
end on

on w_build_loads_sales_order_defaults.destroy
call super::destroy
destroy(this.ole_hides)
end on

event resize;if newheight > 10 then
	ole_hides.Height = newheight 
end if

if newwidth> 10 then
	ole_hides.width = newwidth 
end if

//ole_chg_order_prod_dates.object.refresh
//ole_chg_order_prod_dates.setredraw(true)
//this.setredraw(true)
//wf_auto_hscrollbar()
end event

event closequery;If ole_hides.object.CloseQuery Then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If Not This.Event ue_save() Then
//			Message.ReturnValue = 1	 // Update failed - do not close window
			Return 1
		Else
//			Message.ReturnValue = 0
		End If

	CASE 2	// Do not save changes
//		Message.ReturnValue = 0

	CASE 3	// Cancel the closing of window
//		Message.ReturnValue = 1
		Return 1
	END CHOOSE
End If
Return 0
end event

event ue_addrow;call super::ue_addrow;//ole_chg_order_prod_dates.object.AddRow()
end event

event ue_filenew;//ole_chg_order_prod_dates.object.AddRow()
end event

type ole_hides from olecustomcontrol within w_build_loads_sales_order_defaults
event messageposted ( ref string strinfomessage )
event currentrecordchanged ( )
int Width=1637
int Height=1012
int TabOrder=10
boolean BringToTop=true
long BackColor=79741120
long TextColor=33554432
boolean Border=false
boolean FocusRectangle=false
string BinaryKey="w_build_loads_sales_order_defaults.win"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)
end event

event getfocus;ole_hides.object.SetFocusback()
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
08w_build_loads_sales_order_defaults.bin 
2F00000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000bbd7c8b001c17f6500000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a00000002000000010000000464e54a4c11d55e9b0100cab35a91280200000000bbd7c8b001c17f65bbd7c8b001c17f65000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000074000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
20ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000074000800034757f20b000000200065005f00740078006e00650078007400002500000800034757f20a000000200065005f00740078006e00650079007400001a26000b0008cc07eb2effffffc000700061006c00700063006900740061006f00690003006e005000000053004100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
18w_build_loads_sales_order_defaults.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
