HA$PBExportHeader$w_invt_vs_sales_inq.srw
forward
global type w_invt_vs_sales_inq from w_netwise_response
end type
type dw_division from u_division within w_invt_vs_sales_inq
end type
end forward

global type w_invt_vs_sales_inq from w_netwise_response
int X=151
int Y=461
int Width=1756
int Height=345
long BackColor=12632256
dw_division dw_division
end type
global w_invt_vs_sales_inq w_invt_vs_sales_inq

type variables
Boolean			ib_valid_to_close

w_invt_vs_sales		iw_parent

end variables

event ue_postopen;call super::ue_postopen;String	ls_Division


ls_Division = iw_parent.wf_Get_Division()
If Not iw_frame.iu_string.nf_IsEmpty(ls_Division) Then dw_division.uf_Set_division(ls_Division)
end event

event open;call super::open;String				ls_division


iw_parent = Message.PowerObjectParm


This.Title = iw_parent.Title + " Inquire"

dw_division.SetFocus()


end event

on close;call w_netwise_response::close;If Not ib_valid_to_close Then
	Message.StringParm = ""
End if
end on

on w_invt_vs_sales_inq.create
int iCurrent
call w_netwise_response::create
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_division
end on

on w_invt_vs_sales_inq.destroy
call w_netwise_response::destroy
destroy(this.dw_division)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_division

If dw_division.AcceptText() = -1 Then 
	dw_division.SetFocus()
	return
End if
ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End if
ib_valid_to_close = True
CloseWithReturn(This, ls_division)
end event

type cb_base_help from w_netwise_response`cb_base_help within w_invt_vs_sales_inq
int X=1377
int Y=117
int TabOrder=60
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_invt_vs_sales_inq
int X=1079
int Y=117
int TabOrder=40
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_invt_vs_sales_inq
int X=782
int Y=117
int TabOrder=20
end type

type dw_division from u_division within w_invt_vs_sales_inq
int X=5
int Y=5
int TabOrder=10
end type

