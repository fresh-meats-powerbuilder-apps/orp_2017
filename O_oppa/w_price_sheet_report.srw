HA$PBExportHeader$w_price_sheet_report.srw
forward
global type w_price_sheet_report from w_netwise_sheet
end type
type dw_sales_order_id from u_base_dw_ext within w_price_sheet_report
end type
end forward

global type w_price_sheet_report from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 887
integer height = 300
string title = "Hides Formula Price Sheet Report"
long backcolor = 79741120
dw_sales_order_id dw_sales_order_id
end type
global w_price_sheet_report w_price_sheet_report

type variables
s_error			istr_error_info
u_ws_pas_share       iu_ws_pas_share 
end variables

forward prototypes
public function boolean wf_update ()
end prototypes

public function boolean wf_update ();string order_num, update_string,ls_date,ls_time,ls_userid,ls_op
boolean lb_return

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp72grt"
istr_error_info.se_message = Space(71)

dw_sales_order_id.AcceptText()
order_num = dw_sales_order_id.GetItemString(1,"order_id")
ls_userid = sqlca.userid
ls_date = string(today(),'YYMMDD')
ls_time = string(now(),'HHMMSS')

update_string = '1' + 'O360' + '    ' + ls_userid + ' ' + + 'V' + '01     ' + trim(order_num)

lb_return = iu_ws_pas_share.nf_pasp72gr(update_string,istr_error_info)

If lb_return Then
	iw_Frame.SetMicroHelp("Hides Formula Price Sheet Report has been initiated")
End If

return lb_return


end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
iw_frame.im_menu.mf_Disable('m_generatesales')
iw_frame.im_menu.mf_Disable('m_complete')

end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	
iw_frame.im_menu.mf_Enable('m_generatesales')
iw_frame.im_menu.mf_Enable('m_complete')

end event

event ue_postopen;call super::ue_postopen;
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_initiate_mainframe_reports"
istr_error_info.se_user_id 		= sqlca.userid

iu_ws_pas_share = Create u_ws_pas_share

					 
											

end event

on w_price_sheet_report.create
int iCurrent
call super::create
this.dw_sales_order_id=create dw_sales_order_id
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_order_id
end on

on w_price_sheet_report.destroy
call super::destroy
destroy(this.dw_sales_order_id)
end on

event resize;//if newwidth > 2694 then
//	ole_init_mainframe_reports.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_init_mainframe_reports.Height = newheight - 25
//end if
//
end event

event closequery;call super::closequery;Destroy( iu_ws_pas_share)

DESTROY iu_ws_pas_share
end event

type dw_sales_order_id from u_base_dw_ext within w_price_sheet_report
integer x = 18
integer y = 12
integer width = 759
integer height = 160
integer taborder = 20
string dataobject = "d_sales_order_id"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

