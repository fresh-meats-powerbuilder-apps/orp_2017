HA$PBExportHeader$w_pas_daily_summary.srw
forward
global type w_pas_daily_summary from w_netwise_sheet
end type
type dw_product_status_window from datawindow within w_pas_daily_summary
end type
type dw_daily_summary_header from u_base_dw within w_pas_daily_summary
end type
type dw_daily_summary_detail from u_netwise_dw within w_pas_daily_summary
end type
end forward

global type w_pas_daily_summary from w_netwise_sheet
integer width = 2944
integer height = 1380
string title = "Daily Summary Report"
long backcolor = 12632256
long il_borderpaddingwidth = 100
dw_product_status_window dw_product_status_window
dw_daily_summary_header dw_daily_summary_header
dw_daily_summary_detail dw_daily_summary_detail
end type
global w_pas_daily_summary w_pas_daily_summary

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
u_pas203		iu_pas203
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Long		il_color

Long		il_SelectedColor

Long		il_SelectedTextColor

Boolean		ib_reinquire

String		is_pork_sales_option = "B"

Integer		ii_max_page_number


//1-13 jac
long il_selected_status_max = 8

String         is_selected_status_array
//
end variables

forward prototypes
public function boolean wf_retrieve ()
public function string wf_get_plant ()
public function string wf_get_division ()
public function datetime wf_get_update_date ()
public subroutine wf_filter ()
public function string wf_get_boxes_or_loads ()
public subroutine wf_set_format ()
public function string wf_get_selected_status ()
end prototypes

public function boolean wf_retrieve ();String		ls_string, &
				ls_daily_sum_data, &
				ls_date_time_stamp, &
				ls_input, &
				ls_temp, & 
				ls_product_status, &
				ls_string2

DateTime		ldt_datetime

Long			ll_rec_count, &
            ll_count, &
				ll_pos, &
            ll_counter, &
            ll_counter_max


Call w_base_sheet::closequery

If Not ib_reinquire Then
	OpenWithParm(w_pas_daily_summary_inq, This)
	ls_string = Message.StringParm
	If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False

	dw_daily_summary_header.Reset()

	is_pork_sales_option = Right(ls_string,1)
	If dw_daily_summary_header.ImportString(Left(ls_string,iw_frame.iu_string.nf_nPos(ls_string,"~t",1,4)-1)) < 1 Then Return False
	dw_daily_summary_header.SetItem(1,"boxes_or_loads",Left(Right(ls_string,3),1))	
	
//1-13 jac
	ll_pos = 0
	ll_counter = 1
	ll_counter_max = 4
	for ll_counter = 1 to ll_counter_max
		ll_pos = Pos(ls_string,"~t", ll_pos + 1)
	//	ll_counter = ll_counter +1 
	next

	ls_string2 = Mid(ls_string,ll_pos + 1,iw_frame.iu_string.nf_nPos(ls_string,"~t",ll_pos + 1,16))

	dw_product_status_window.ImportString(ls_string2)
	dw_product_status_window.AcceptText()
//
Else
	ib_reinquire = False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

istr_error_info.se_event_name = "wf_retrieve"

ls_input = dw_daily_summary_header.GetItemString(1, "plant_code") + "~t" + &
				dw_daily_summary_header.GetItemString(1, "division_code") + "~t" + &
				dw_daily_summary_header.GetItemString(1, "boxes_or_loads") + "~t" + &
				is_pork_sales_option + "~t" 
				
//1-13 jac
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
	ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) then
		ls_product_status = " "
	end if
   ls_input += ls_product_status + "~t"
next
//

SetRedraw (False)
dw_daily_summary_detail.Reset()
//
													
If Not iu_ws_pas_share.nf_pasp40fr(istr_error_info, &
													ls_input, &
													ls_date_time_stamp, &
													ls_daily_sum_Data) Then 													
													
													
													
	SetRedraw(True)
	Return False
End If


ll_rec_count = dw_daily_summary_detail.ImportString(ls_daily_sum_data)
ldt_datetime = DateTime(Date(String(mid(ls_date_time_stamp, 1,10))), &
					Time(String(mid(ls_date_time_stamp, 12,5))))

dw_daily_summary_header.SetItem(1,"last_updated", ldt_datetime)
dw_daily_summary_detail.SetItem(1,"daily_sum1_t", ldt_datetime)

dw_daily_summary_detail.SetItem(1, "back_color", il_SelectedColor)
dw_daily_summary_detail.SetItem(1, "text_color", il_SelectedTextColor)

ls_temp = dw_daily_summary_header.GetItemString(1,"boxes_or_loads")

wf_set_format()

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

// Must do because of the reinquire
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'
//1-13 jac
//dw_daily_summary_detail.Object.DataWindow.HorizontalScrollSplit = '646'
//dw_daily_summary_detail.Object.DataWindow.HorizontalScrollPosition2 =  '646'
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollSplit = '846'
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollPosition2 =  '846'

SetRedraw(True)


Return (True)
end function

public function string wf_get_plant ();String			ls_plant

ls_plant = dw_daily_summary_header.GetItemString(1, "plant_code") + "~t" + &
					dw_daily_summary_header.GetItemString(1, "plant_descr") 

Return(ls_plant)
end function

public function string wf_get_division ();String			ls_division

ls_division = dw_daily_summary_header.GetItemString(1, "division_code") + "~t" + &
					dw_daily_summary_header.GetItemString(1, "division_descr") 

Return(ls_division)
end function

public function datetime wf_get_update_date ();DateTime			ldt_date

ldt_date = dw_daily_summary_header.GetItemDateTime(1,"last_updated")

Return(ldt_date)

end function

public subroutine wf_filter ();Integer		li_counter

Long			ll_nbrrows

String		ls_filter, &
				ls_row_message

dw_daily_summary_detail.SetRedraw(False)
If dw_daily_summary_header.GetText() = "Y" Then

	ls_filter = "daily_sum1 < 0 or daily_sum2 < 0 or daily_sum3 < 0 or " + &
					"daily_sum4 < 0 or daily_sum5 < 0 or daily_sum6 < 0 or " + &
					"daily_sum7 < 0 or daily_sum8 < 0 or daily_sum9 < 0 or " + &	
					"daily_sum10 < 0 or daily_sum11 < 0 or daily_sum12 < 0 or " + &
					"daily_sum13 < 0 or daily_sum14 < 0 or daily_sum15 < 0 or " + &
					"daily_sum16 < 0 or daily_sum17 < 0 or daily_sum18 < 0 or " + &
					"daily_sum19 < 0 or daily_sum20 < 0 or daily_sum21 < 0 " 
	ls_row_message = " Rows with negative values"
Else
	ls_filter = ""
	ls_row_message = " Rows Total"
End IF

dw_daily_summary_detail.SetFilter(ls_filter)	
dw_daily_summary_detail.Filter()
ll_nbrrows = dw_daily_summary_detail.RowCount()

// Set the highlighting again.
dw_daily_summary_detail.SetItem(1,"back_color", il_SelectedColor)
dw_daily_summary_detail.SetItem(1,"text_color", il_SelectedTextColor)
dw_daily_summary_detail.SetItem(1,"daily_sum1_t", &
		dw_daily_summary_header.GetItemDateTime(1, "last_updated"))

iw_frame.SetMicroHelp(String(ll_nbrrows) + ls_row_message)
dw_daily_summary_detail.SetRedraw(True)

Return
end subroutine

public function string wf_get_boxes_or_loads ();String			ls_boxes_or_loads

ls_boxes_or_loads = dw_daily_summary_header.GetItemString(1, "boxes_or_loads")

Return(ls_boxes_or_loads)
end function

public subroutine wf_set_format ();if dw_daily_summary_header.GetItemString(1,"boxes_or_loads") = 'B' Then
	dw_daily_summary_detail.Modify("daily_sum1.EditMask.Mask='###,##0' " + &
			"daily_sum2.EditMask.Mask='###,##0' " + &
			"daily_sum3.EditMask.Mask='###,##0' " + &
			"daily_sum4.EditMask.Mask='###,##0' " + &
			"daily_sum5.EditMask.Mask='###,##0' " + &
			"daily_sum6.EditMask.Mask='###,##0' " + &
			"daily_sum7.EditMask.Mask='###,##0' " + &
			"daily_sum8.EditMask.Mask='###,##0' " + &
			"daily_sum9.EditMask.Mask='###,##0' " + &
			"daily_sum10.EditMask.Mask='###,##0' " + &
			"daily_sum11.EditMask.Mask='###,##0' " + &
			"daily_sum12.EditMask.Mask='###,##0' " + &
			"daily_sum13.EditMask.Mask='###,##0' " + &
			"daily_sum14.EditMask.Mask='###,##0' " + &
			"daily_sum15.EditMask.Mask='###,##0' " + &
			"daily_sum16.EditMask.Mask='###,##0' " + &
			"daily_sum17.EditMask.Mask='###,##0' " + &
			"daily_sum18.EditMask.Mask='###,##0' " + &
			"daily_sum19.EditMask.Mask='###,##0' " + &
			"daily_sum20.EditMask.Mask='###,##0' " + &
			"daily_sum21.EditMask.Mask='###,##0' " + &
			"daily_sum_total_1.Format='###,##0' " + &
			"daily_sum_total_2.Format='###,##0' " + &
			"daily_sum_total_3.Format='###,##0' " + &
			"daily_sum_total_4.Format='###,##0' " + &
			"daily_sum_total_5.Format='###,##0' " + &
			"daily_sum_total_6.Format='###,##0' " + &
			"daily_sum_total_7.Format='###,##0' " + &
			"daily_sum_total_8.Format='###,##0' " + &
			"daily_sum_total_9.Format='###,##0' " + &
			"daily_sum_total_10.Format='###,##0' " + &
			"daily_sum_total_11.Format='###,##0' " + &
			"daily_sum_total_12.Format='###,##0' " + &
			"daily_sum_total_13.Format='###,##0' " + &
			"daily_sum_total_14.Format='###,##0' " + &
			"daily_sum_total_15.Format='###,##0' " + &
			"daily_sum_total_16.Format='###,##0' " + &
			"daily_sum_total_17.Format='###,##0' " + &
			"daily_sum_total_18.Format='###,##0' " + &
			"daily_sum_total_19.Format='###,##0' " + &
			"daily_sum_total_20.Format='###,##0' " + &
			"daily_sum_total_21.Format='###,##0' ")
Else
		dw_daily_summary_detail.Modify("daily_sum1.EditMask.Mask='###,##0.0' " + &
			"daily_sum2.EditMask.Mask='###,##0.0' " + &			
			"daily_sum3.EditMask.Mask='###,##0.0' " + &			
			"daily_sum4.EditMask.Mask='###,##0.0' " + &			
			"daily_sum5.EditMask.Mask='###,##0.0' " + &			
			"daily_sum6.EditMask.Mask='###,##0.0' " + &			
			"daily_sum7.EditMask.Mask='###,##0.0' " + &			
			"daily_sum8.EditMask.Mask='###,##0.0' " + &			
			"daily_sum9.EditMask.Mask='###,##0.0' " + &			
			"daily_sum10.EditMask.Mask='###,##0.0' " + &			
			"daily_sum11.EditMask.Mask='###,##0.0' " + &			
			"daily_sum12.EditMask.Mask='###,##0.0' " + &			
			"daily_sum13.EditMask.Mask='###,##0.0' " + &			
			"daily_sum14.EditMask.Mask='###,##0.0' " + &			
			"daily_sum15.EditMask.Mask='###,##0.0' " + &			
			"daily_sum16.EditMask.Mask='###,##0.0' " + &			
			"daily_sum17.EditMask.Mask='###,##0.0' " + &			
			"daily_sum18.EditMask.Mask='###,##0.0' " + &			
			"daily_sum19.EditMask.Mask='###,##0.0' " + &			
			"daily_sum20.EditMask.Mask='###,##0.0' " + &			
			"daily_sum21.EditMask.Mask='###,##0.0' " + &
			"daily_sum_total_1.Format='###,##0.0' " + &
			"daily_sum_total_2.Format='###,##0.0' " + &			
			"daily_sum_total_3.Format='###,##0.0' " + &			
			"daily_sum_total_4.Format='###,##0.0' " + &			
			"daily_sum_total_5.Format='###,##0.0' " + &			
			"daily_sum_total_6.Format='###,##0.0' " + &			
			"daily_sum_total_7.Format='###,##0.0' " + &			
			"daily_sum_total_8.Format='###,##0.0' " + &			
			"daily_sum_total_9.Format='###,##0.0' " + &			
			"daily_sum_total_10.Format='###,##0.0' " + &			
			"daily_sum_total_11.Format='###,##0.0' " + &			
			"daily_sum_total_12.Format='###,##0.0' " + &			
			"daily_sum_total_13.Format='###,##0.0' " + &			
			"daily_sum_total_14.Format='###,##0.0' " + &			
			"daily_sum_total_15.Format='###,##0.0' " + &			
			"daily_sum_total_16.Format='###,##0.0' " + &			
			"daily_sum_total_17.Format='###,##0.0' " + &			
			"daily_sum_total_18.Format='###,##0.0' " + &			
			"daily_sum_total_19.Format='###,##0.0' " + &			
			"daily_sum_total_20.Format='###,##0.0' " + &			
			"daily_sum_total_21.Format='###,##0.0' ")			
End If
end subroutine

public function string wf_get_selected_status ();//1-13 jac
long  ll_count
string ls_product_status, & 
		ls_stat_desc, &
       ls_input 

is_selected_status_array = ''
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if ll_count = 1 then 
		If isnull(ls_product_status) Then
		   ls_product_status = 'ALL'
	   end if
	else 
	   If isnull(ls_product_status) Then
			ls_product_status = " "
		end if
	end if 
	is_selected_status_array += ls_product_status + "~t"
   ls_stat_desc = dw_product_status_window.GetItemString(1, "status_desc" + string(ll_count))
   ls_stat_desc = TRIM(ls_stat_desc)
	if ll_count = 1 then 
		If isnull(ls_stat_desc) Then
		   ls_stat_desc = 'ALL'
	   end if
	else 
		If isnull(ls_stat_desc) Then
			ls_stat_desc = " "
		end if
	end if 
	is_selected_status_array += ls_stat_desc + "~t"
next


Return is_selected_status_array
//



end function

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')


end event

event resize;call super::resize;//constant integer li_x		= 1
//constant integer li_y		= 193

 integer li_x		= 15
 integer li_y		= 193

//dw_daily_summary_detail.width	= newwidth - (30 + li_x)
//dw_daily_summary_detail.height	= newheight - (30 + li_y)



if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_daily_summary_detail.width  = newwidth - (li_x)
dw_daily_summary_detail.height = newheight - (li_y)

// Must do because of the split horizonal bar 
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'
//1-13 jac
//dw_daily_summary_detail.Object.DataWindow.HorizontalScrollSplit = '646'
//dw_daily_summary_detail.Object.DataWindow.HorizontalScrollPosition2 =  '646'
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollSplit = '846'
dw_daily_summary_detail.Object.DataWindow.HorizontalScrollPosition2 =  '846'


end event

event ue_postopen;call super::ue_postopen;Environment	le_env

iu_pas203 = create u_pas203
iu_ws_pas_share = Create u_ws_pas_share

If Message.ReturnValue = -1 Then Close(This)

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "dailysum"
istr_error_info.se_user_id				= sqlca.userid

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

ib_reinquire = FALSE

This.PostEvent('ue_query')
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF


Destroy iu_ws_pas_share
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')

iw_frame.im_menu.mf_enable('m_print')


end event

on w_pas_daily_summary.create
int iCurrent
call super::create
this.dw_product_status_window=create dw_product_status_window
this.dw_daily_summary_header=create dw_daily_summary_header
this.dw_daily_summary_detail=create dw_daily_summary_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_status_window
this.Control[iCurrent+2]=this.dw_daily_summary_header
this.Control[iCurrent+3]=this.dw_daily_summary_detail
end on

on w_pas_daily_summary.destroy
call super::destroy
destroy(this.dw_product_status_window)
destroy(this.dw_daily_summary_header)
destroy(this.dw_daily_summary_detail)
end on

event ue_fileprint;call super::ue_fileprint;datawindowchild			ldwc_detail

String						ls_mod, &
								ls_string, ls_temp

DataStore					lds_print


IF Not ib_print_ok Then Return

lds_print = create u_print_datastore
lds_print.DataObject = 'd_pas_daily_summary_report'

// Removed modify statement to fix errors			
lds_print.Object.plant_code_t.text = dw_daily_summary_header.GetItemString(1,'plant_code')
lds_print.Object.plant_descr_t.text = dw_daily_summary_header.GetItemString(1,'plant_descr')
lds_print.Object.division_code_t.text = dw_daily_summary_header.GetItemString(1,'division_code')
lds_print.Object.division_descr_t.text = dw_daily_summary_header.GetItemString(1,'division_descr')
lds_print.Object.last_updated_t.text = String(dw_daily_summary_header.GetItemDateTime(1,'last_updated'))


lds_print.GetChild("dw_detail", ldwc_detail)
//dw_daily_summary_detail.sharedata(ldwc_detail)
ls_string = dw_daily_summary_detail.object.datawindow.data
ldwc_detail.ImportString(ls_string)
		
ls_mod = "product_descr.Border='0' " + &
			"product_state.Border='0' " + &
			"daily_sum1.Border='0' daily_sum2.Border='0' daily_sum3." + &
			"Border='0' daily_sum4.Border='0' daily_sum5.Border='0' " + &
			"daily_sum6.Border='0' daily_sum7.Border='0' daily_sum8." + &	
			"Border='0' daily_sum9.Border='0' daily_sum10.Border='0' " + &
			"daily_sum11.Border='0' daily_sum12.Border='0' daily_sum13." + &
			"Border='0' daily_sum14.Border='0' daily_sum15.Border='0' " + &
			"daily_sum16.Border='0' daily_sum17.Border='0' daily_sum18." + &
			"Border='0' daily_sum19.Border='0' daily_sum20.Border='0'" + &
			" daily_sum21.Border='0' " + &
			"daily_sum1.Background.Color='0 ~t If( daily_sum1  >= 0, " + &
			"16777215, 12632256)' daily_sum2.Background.Color='0 ~t " + &
			"If( daily_sum2 >= 0, 16777215, 12632256)' daily_sum3." + &
			"Background.Color='0 ~t If( daily_sum3  >= 0, 16777215, " + &
			"12632256)' daily_sum4.Background.Color='0 ~t If( daily_sum4 " + &
			">= 0, 16777215, 12632256)' daily_sum5.Background.Color='0 " + &
			"~t If( daily_sum5  >= 0, 16777215, 12632256)' " + &
			"daily_sum6.Background.Color='0 ~t If( daily_sum6  >= 0, " + &
			"16777215, 12632256)' daily_sum7.Background.Color='0 ~t " + &
			"If( daily_sum7  >= 0, 16777215, 12632256)' daily_sum8." + &	
			"Background.Color='0 ~t If( daily_sum8  >= 0, 16777215, " + &
			"12632256)' daily_sum9.Background.Color='0 ~t If( daily_sum9 " + &
			">= 0, 16777215, 12632256)' daily_sum10.Background.Color='0 " + &
			"~t If( daily_sum10  >= 0, 16777215, 12632256)' " + &
			"daily_sum11.Background.Color='0 ~t If( daily_sum11  >= 0, " + &
			"16777215, 12632256)' daily_sum12.Background.Color='0 ~t " + &
			"If( daily_sum12  >= 0, 16777215, 12632256)' daily_sum13." + &
			"Background.Color='0 ~t If( daily_sum13  >= 0, 16777215, " + &
			"12632256)' daily_sum14.Background.Color='0 ~t If( daily_sum14 " + &
			">= 0, 16777215, 12632256)' daily_sum15.Background.Color='0 " + &
			"~t If( daily_sum15  >= 0, 16777215, 12632256)' " + &
			"daily_sum16.Background.Color='0 ~t If( daily_sum16  >= 0, " + &
			"16777215, 12632256)' daily_sum17.Background.Color='0 ~t " + &
			"If( daily_sum17  >= 0, 16777215, 12632256)' daily_sum18." + &
			"Background.Color='0 ~t If( daily_sum18  >= 0, 16777215, " + &
			"12632256)' daily_sum19.Background.Color='0 ~t If( daily_sum19 " + &
			">= 0, 16777215, 12632256)' daily_sum20.Background.Color='0 " + &
			"~t If( daily_sum20  >= 0, 16777215, 12632256)'" + &
			" daily_sum21.Background.Color='0 ~t If( daily_sum21  >= 0, " + &
			"16777215, 12632256)' " + &
			"daily_sum1.Color='0' daily_sum2.Color='0' daily_sum3." + &
			"Color='0' daily_sum4.Color='0' daily_sum5.Color='0' " + &
			"daily_sum6.Color='0' daily_sum7.Color='0' daily_sum8." + &	
			"Color='0' daily_sum9.Color='0' daily_sum10.Color='0' " + &
			"daily_sum11.Color='0' daily_sum12.Color='0' daily_sum13." + &
			"Color='0' daily_sum14.Color='0' daily_sum15.Color='0' " + &
			"daily_sum16.Color='0' daily_sum17.Color='0' daily_sum18." + &
			"Color='0' daily_sum19.Color='0' daily_sum20.Color='0'" + &
			" daily_sum21.Color='0' " + &
			"daily_sum_total_1.Border='0' daily_sum_total_2.Border='0' daily_sum_total_3." + &
			"Border='0' daily_sum_total_4.Border='0' daily_sum_total_5.Border='0' " + &
			"daily_sum_total_6.Border='0' daily_sum_total_7.Border='0' daily_sum_total_8." + &	
			"Border='0' daily_sum_total_9.Border='0' daily_sum_total_10.Border='0' " + &
			"daily_sum_total_11.Border='0' daily_sum_total_12.Border='0' daily_sum_total_13." + &
			"Border='0' daily_sum_total_14.Border='0' daily_sum_total_15.Border='0' " + &
			"daily_sum_total_16.Border='0' daily_sum_total_17.Border='0' daily_sum_total_18." + &
			"Border='0' daily_sum_total_19.Border='0' daily_sum_total_20.Border='0'" + &
			" daily_sum_total_21.Border='0' " + &
			"daily_sum_total_1.Background.Color='0 ~t If( daily_sum_total_1  >= 0, " + &
			"16777215, 12632256)' daily_sum_total_2.Background.Color='0 ~t " + &
			"If( daily_sum_total_2 >= 0, 16777215, 12632256)' daily_sum_total_3." + &
			"Background.Color='0 ~t If( daily_sum_total_3  >= 0, 16777215, " + &
			"12632256)' daily_sum_total_4.Background.Color='0 ~t If( daily_sum_total_4 " + &
			">= 0, 16777215, 12632256)' daily_sum_total_5.Background.Color='0 " + &
			"~t If( daily_sum_total_5  >= 0, 16777215, 12632256)' " + &
			"daily_sum_total_6.Background.Color='0 ~t If( daily_sum_total_6  >= 0, " + &
			"16777215, 12632256)' daily_sum_total_7.Background.Color='0 ~t " + &
			"If( daily_sum_total_7  >= 0, 16777215, 12632256)' daily_sum_total_8." + &	
			"Background.Color='0 ~t If( daily_sum_total_8  >= 0, 16777215, " + &
			"12632256)' daily_sum_total_9.Background.Color='0 ~t If( daily_sum_total_9 " + &
			">= 0, 16777215, 12632256)' daily_sum_total_10.Background.Color='0 " + &
			"~t If( daily_sum_total_10  >= 0, 16777215, 12632256)' " + &
			"daily_sum_total_11.Background.Color='0 ~t If( daily_sum_total_11  >= 0, " + &
			"16777215, 12632256)' daily_sum_total_12.Background.Color='0 ~t " + &
			"If( daily_sum_total_12  >= 0, 16777215, 12632256)' daily_sum_total_13." + &
			"Background.Color='0 ~t If( daily_sum_total_13  >= 0, 16777215, " + &
			"12632256)' daily_sum_total_14.Background.Color='0 ~t If( daily_sum_total_14 " + &
			">= 0, 16777215, 12632256)' daily_sum_total_15.Background.Color='0 " + &
			"~t If( daily_sum_total_15  >= 0, 16777215, 12632256)' " + &
			"daily_sum_total_16.Background.Color='0 ~t If( daily_sum_total_16  >= 0, " + &
			"16777215, 12632256)' daily_sum_total_17.Background.Color='0 ~t " + &
			"If( daily_sum_total_17  >= 0, 16777215, 12632256)' daily_sum_total_18." + &
			"Background.Color='0 ~t If( daily_sum_total_18  >= 0, 16777215, " + &
			"12632256)' daily_sum_total_19.Background.Color='0 ~t If( daily_sum_total_19 " + &
			">= 0, 16777215, 12632256)' daily_sum_total_20.Background.Color='0 " + &
			"~t If( daily_sum_total_20  >= 0, 16777215, 12632256)'" + &
			" daily_sum_total_21.Background.Color='0 ~t If( daily_sum_total_21  >= 0, " + &
			"16777215, 12632256)' " + &
			"daily_sum_total_1.Color='0' daily_sum_total_2.Color='0' daily_sum_total_3." + &
			"Color='0' daily_sum_total_4.Color='0' daily_sum_total_5.Color='0' " + &
			"daily_sum_total_6.Color='0' daily_sum_total_7.Color='0' daily_sum_total_8." + &	
			"Color='0' daily_sum_total_9.Color='0' daily_sum_total_10.Color='0' " + &
			"daily_sum_total_11.Color='0' daily_sum_total_12.Color='0' daily_sum_total_13." + &
			"Color='0' daily_sum_total_14.Color='0' daily_sum_total_15.Color='0' " + &
			"daily_sum_total_16.Color='0' daily_sum_total_17.Color='0' daily_sum_total_18." + &
			"Color='0' daily_sum_total_19.Color='0' daily_sum_total_20.Color='0'" + &
			" daily_sum_total_21.Color='0' " 

ls_temp = ldwc_detail.Modify(ls_mod)

if dw_daily_summary_header.GetItemString(1,"boxes_or_loads") = 'B' Then
	ldwc_detail.Modify("daily_sum1.EditMask.Mask='###,##0' " + &
			"daily_sum2.EditMask.Mask='###,##0' " + &
			"daily_sum3.EditMask.Mask='###,##0' " + &
			"daily_sum4.EditMask.Mask='###,##0' " + &
			"daily_sum5.EditMask.Mask='###,##0' " + &
			"daily_sum6.EditMask.Mask='###,##0' " + &
			"daily_sum7.EditMask.Mask='###,##0' " + &
			"daily_sum8.EditMask.Mask='###,##0' " + &
			"daily_sum9.EditMask.Mask='###,##0' " + &
			"daily_sum10.EditMask.Mask='###,##0' " + &
			"daily_sum11.EditMask.Mask='###,##0' " + &
			"daily_sum12.EditMask.Mask='###,##0' " + &
			"daily_sum13.EditMask.Mask='###,##0' " + &
			"daily_sum14.EditMask.Mask='###,##0' " + &
			"daily_sum15.EditMask.Mask='###,##0' " + &
			"daily_sum16.EditMask.Mask='###,##0' " + &
			"daily_sum17.EditMask.Mask='###,##0' " + &
			"daily_sum18.EditMask.Mask='###,##0' " + &
			"daily_sum19.EditMask.Mask='###,##0' " + &
			"daily_sum20.EditMask.Mask='###,##0' " + &
			"daily_sum21.EditMask.Mask='###,##0' " + &
			"daily_sum_total_1.Format='###,##0' " + &
			"daily_sum_total_2.Format='###,##0' " + &
			"daily_sum_total_3.Format='###,##0' " + &
			"daily_sum_total_4.Format='###,##0' " + &
			"daily_sum_total_5.Format='###,##0' " + &
			"daily_sum_total_6.Format='###,##0' " + &
			"daily_sum_total_7.Format='###,##0' " + &
			"daily_sum_total_8.Format='###,##0' " + &
			"daily_sum_total_9.Format='###,##0' " + &
			"daily_sum_total_10.Format='###,##0' " + &
			"daily_sum_total_11.Format='###,##0' " + &
			"daily_sum_total_12.Format='###,##0' " + &
			"daily_sum_total_13.Format='###,##0' " + &
			"daily_sum_total_14.Format='###,##0' " + &
			"daily_sum_total_15.Format='###,##0' " + &
			"daily_sum_total_16.Format='###,##0' " + &
			"daily_sum_total_17.Format='###,##0' " + &
			"daily_sum_total_18.Format='###,##0' " + &
			"daily_sum_total_19.Format='###,##0' " + &
			"daily_sum_total_20.Format='###,##0' " + &
			"daily_sum_total_21.Format='###,##0' ")
Else
		ldwc_detail.Modify("daily_sum1.EditMask.Mask='###,##0.0' " + &
			"daily_sum2.EditMask.Mask='###,##0.0' " + &			
			"daily_sum3.EditMask.Mask='###,##0.0' " + &			
			"daily_sum4.EditMask.Mask='###,##0.0' " + &			
			"daily_sum5.EditMask.Mask='###,##0.0' " + &			
			"daily_sum6.EditMask.Mask='###,##0.0' " + &			
			"daily_sum7.EditMask.Mask='###,##0.0' " + &			
			"daily_sum8.EditMask.Mask='###,##0.0' " + &			
			"daily_sum9.EditMask.Mask='###,##0.0' " + &			
			"daily_sum10.EditMask.Mask='###,##0.0' " + &			
			"daily_sum11.EditMask.Mask='###,##0.0' " + &			
			"daily_sum12.EditMask.Mask='###,##0.0' " + &			
			"daily_sum13.EditMask.Mask='###,##0.0' " + &			
			"daily_sum14.EditMask.Mask='###,##0.0' " + &			
			"daily_sum15.EditMask.Mask='###,##0.0' " + &			
			"daily_sum16.EditMask.Mask='###,##0.0' " + &			
			"daily_sum17.EditMask.Mask='###,##0.0' " + &			
			"daily_sum18.EditMask.Mask='###,##0.0' " + &			
			"daily_sum19.EditMask.Mask='###,##0.0' " + &			
			"daily_sum20.EditMask.Mask='###,##0.0' " + &			
			"daily_sum21.EditMask.Mask='###,##0.0' " + &
			"daily_sum_total_1.Format='###,##0.0' " + &
			"daily_sum_total_2.Format='###,##0.0' " + &			
			"daily_sum_total_3.Format='###,##0.0' " + &			
			"daily_sum_total_4.Format='###,##0.0' " + &			
			"daily_sum_total_5.Format='###,##0.0' " + &			
			"daily_sum_total_6.Format='###,##0.0' " + &			
			"daily_sum_total_7.Format='###,##0.0' " + &			
			"daily_sum_total_8.Format='###,##0.0' " + &			
			"daily_sum_total_9.Format='###,##0.0' " + &			
			"daily_sum_total_10.Format='###,##0.0' " + &			
			"daily_sum_total_11.Format='###,##0.0' " + &			
			"daily_sum_total_12.Format='###,##0.0' " + &			
			"daily_sum_total_13.Format='###,##0.0' " + &			
			"daily_sum_total_14.Format='###,##0.0' " + &			
			"daily_sum_total_15.Format='###,##0.0' " + &			
			"daily_sum_total_16.Format='###,##0.0' " + &			
			"daily_sum_total_17.Format='###,##0.0' " + &			
			"daily_sum_total_18.Format='###,##0.0' " + &			
			"daily_sum_total_19.Format='###,##0.0' " + &			
			"daily_sum_total_20.Format='###,##0.0' " + &			
			"daily_sum_total_21.Format='###,##0.0' ")			
End If


lds_print.print()




end event

type dw_product_status_window from datawindow within w_pas_daily_summary
boolean visible = false
integer x = 2551
integer y = 96
integer width = 699
integer height = 76
string title = "none"
string dataobject = "d_product_status_2"
boolean border = false
boolean livescroll = true
end type

event constructor;//ib_updateable = False
InsertRow(0)
end event

type dw_daily_summary_header from u_base_dw within w_pas_daily_summary
integer y = 8
integer width = 2862
integer height = 168
integer taborder = 10
string dataobject = "d_pas_daily_summary_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;Integer		li_counter

IF This.GetColumnName() = "neg_selection" Then
	wf_filter()
End If
If dwo.Name = "boxes_or_loads" Then
	ib_reinquire = True
	Parent.PostEvent('ue_query')
End If
end event

on constructor;call u_base_dw::constructor;InsertRow(0)
ib_updateable = False
This.SetItem(1,"neg_selection", "N")
end on

type dw_daily_summary_detail from u_netwise_dw within w_pas_daily_summary
integer y = 184
integer width = 2880
integer height = 1068
integer taborder = 20
string dataobject = "d_pas_daily_summary_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

on constructor;call u_netwise_dw::constructor;ib_updateable = False

end on

