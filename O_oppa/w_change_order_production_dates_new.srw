HA$PBExportHeader$w_change_order_production_dates_new.srw
forward
global type w_change_order_production_dates_new from w_netwise_sheet
end type
type dw_detail from u_base_dw_ext within w_change_order_production_dates_new
end type
type dw_header from u_base_dw_ext within w_change_order_production_dates_new
end type
end forward

global type w_change_order_production_dates_new from w_netwise_sheet
integer width = 3671
integer height = 1428
string title = "Change Order"
long backcolor = 67108864
event ue_emailexpconfirmation ( )
event ue_emailorderconfirmation ( )
event ue_emailshippingorder ( )
event ue_faxexpconfirmation ( )
event ue_faxorderconfirmation ( )
event ue_faxshippingorder ( )
event ue_printexpconfirmation ( )
event ue_printvieworderconfirmation ( )
event ue_printviewshippingorder ( )
event ue_sendshippingorder ( )
dw_detail dw_detail
dw_header dw_header
end type
global w_change_order_production_dates_new w_change_order_production_dates_new

type variables
u_pas201		iu_pas201
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Boolean		ib_open_inq_window

string		is_inquire_window_name, &
				is_order_number, &
				is_order_line, &
				is_include_exclude_string, &
				is_email_fax_ind, &
				is_report_type 
				
				
string	is_classname, &
			is_Add_Access,&
			is_Del_Access,&
			is_Mod_Access,&
			is_Inq_Access				

w_netwise_response	iw_inquirewindow, &
			iw_parentwindow
end variables

forward prototypes
public subroutine wf_apply_ie_plants (string as_plant_string, long as_detail_row_number)
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_inquire ()
end prototypes

event ue_emailexpconfirmation();String	ls_return_string

is_email_fax_ind = 'E'

is_report_type = '28'

OpenWithParm(w_fax_email_response, This)
end event

event ue_emailorderconfirmation();String	ls_return_string

is_email_fax_ind = 'E'

is_report_type = '25'

OpenWithParm(w_fax_email_response, This)
end event

event ue_emailshippingorder();String	ls_return_string

is_email_fax_ind = 'E'

is_report_type = '22'

OpenWithParm(w_fax_email_response, This)
end event

event ue_faxexpconfirmation();String	ls_return_string

is_email_fax_ind = 'F'

is_report_type = '29'

OpenWithParm(w_fax_email_response, This)
end event

event ue_faxorderconfirmation();String	ls_return_string

is_email_fax_ind = 'F'

is_report_type = '26'

OpenWithParm(w_fax_email_response, This)
end event

event ue_faxshippingorder();String	ls_return_string

is_email_fax_ind = 'F'

is_report_type = '23'

OpenWithParm(w_fax_email_response, This)
end event

event ue_printexpconfirmation();String	ls_input
Integer 	li_rtn

ls_input = 	'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'27' + '~t' + is_order_number + '~t' 
 
istr_error_info.se_event_name = "ue_printexpconfirmation"			  

SetPointer(HourGlass!)

//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
												
SetPointer(Arrow!)
end event

event ue_printvieworderconfirmation();String	ls_input
Integer 	li_rtn

ls_input = 	'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'24' + '~t' + is_order_number + '~t' 
 
istr_error_info.se_event_name = "ue_printvieworderconfirmation"			  

SetPointer(HourGlass!)

//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
												
SetPointer(Arrow!)
end event

event ue_printviewshippingorder();String	ls_input
Integer 	li_rtn

ls_input = 	'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'21' + '~t' + is_order_number + '~t' 
 
istr_error_info.se_event_name = "ue_printviewshippingorder"			  

SetPointer(HourGlass!)

//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
												
SetPointer(Arrow!)

end event

event ue_sendshippingorder();String	ls_input
Integer 	li_rtn

ls_input = 	'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
				'20' + '~t' + is_order_number + '~t' 
 
istr_error_info.se_event_name = "ue_sendloadlist"			  

SetPointer(HourGlass!)

li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
												
SetPointer(Arrow!)
end event

public subroutine wf_apply_ie_plants (string as_plant_string, long as_detail_row_number);DataStore	lds_ie_plant

DataWindowChild	ldwc_temp

Long			ll_plant_row_count, &
				ll_sub, &
				ll_found_row, &
				ll_start_pos, &
				ll_new_row
				
String		ls_find_line_number, &
				ls_find_string, &
				ls_plant
				
lds_ie_plant =	Create DataStore
lds_ie_plant.DataObject = 'd_change_order_plants'
ll_plant_row_count = lds_ie_plant.importstring(as_plant_string)

ls_find_line_number = dw_detail.GetItemString(as_detail_row_number, "line_number")
ls_find_string = "line_number = '" 
ls_find_string += ls_find_line_number + "'"
ll_start_pos = 1

dw_detail.GetChild("include_exclude_ind", ldwc_temp)

ldwc_temp.Reset();

ll_found_row = lds_ie_plant.Find(ls_find_string, ll_start_pos, ll_plant_row_count)

If ll_found_row > 0 Then
	Do
		ll_new_row = ldwc_temp.InsertRow(0)
		ldwc_temp.SetItem(ll_new_row, "line_number", ls_find_line_number)
		ls_plant = lds_ie_plant.GetItemString(ll_found_row, "plant_code")
		ldwc_temp.SetItem(ll_new_row, "plant_code", ls_plant) 
		ll_start_pos = ll_found_row + 1
		ll_found_row = lds_ie_plant.Find(ls_find_string, ll_start_pos, ll_plant_row_count)
		
	Loop While ((ll_found_row > 0) AND (ll_start_pos <= ll_plant_row_count)) 
End If		
	
	





end subroutine

public function boolean wf_retrieve ();
boolean	lb_rtn, lb_ret

string	ls_classname

u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

// Set Security on ActiveX Control.
ls_classname = mid(UPPER(this.ClassName()),1,31) + "_EXT"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					is_Add_Access, is_Del_Access, is_Mod_Access, is_Inq_Access)

If ib_open_inq_window Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
End If

lb_ret = wf_inquire()

Return lb_ret
end function

public function boolean wf_addrow ();Integer li_last_row

li_last_row = dw_detail.InsertRow(0)
dw_detail.SetItem(li_last_row, "line_number", '    ')

If dw_header.GetItemString(1, "default_qty_wt_ind") = 'Q' Then
	dw_detail.SetItem(li_last_row, "qty_wt_protect", 'Y')  //Protect qty wt
Else	
	dw_detail.SetItem(li_last_row, "qty_wt_protect", 'N')  //Unprotect qty wt
End if

dw_detail.SetItem(li_last_row, "scheduled_qty", 0)
dw_detail.SetItem(li_last_row, "weight", 0)
dw_detail.SetItem(li_last_row, "qty_wt_ind", "Q")
dw_detail.SetITem(li_last_row, "age_code", 'A')
dw_detail.SetITem(li_last_row, "override_ind", ' ')
dw_detail.SetITem(li_last_row, "updates_allowed_ind", 'Y')
dw_detail.SetRow(li_last_row)
dw_detail.SetColumn("product_code")
dw_detail.ScrollToRow(li_last_row)


Return True
end function

public function boolean wf_update ();long				ll_modrows, &
					ll_row

String			ls_status_ind, &
					ls_update_string, &
					ls_header_string

Integer			li_count, &
					li_ret
					
Date				ld_order_delivery_date					

IF dw_detail.AcceptText() = -1 Then Return False

ll_modrows = dw_detail.ModifiedCount()

IF ll_modrows <= 0 Then Return False

ls_update_string = ''

ld_order_delivery_date = dw_header.GetItemDate(1, "delivery_date")

For li_count = 1 To ll_modrows

	ll_row = dw_detail.GetNextModified(ll_Row, Primary!) 

	Choose Case dw_detail.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			ls_status_ind = 'I'
		CASE DataModified!
			ls_status_ind = 'U'
	END CHOOSE	
	
	If (dw_detail.GetItemString(ll_row, "product_code") >= '          ') Then

		ls_update_string += string(ll_row) + '~t' 
		ls_update_string += ls_status_ind + '~t' 
		ls_update_string += dw_detail.GetItemString(ll_row, "line_number") + '~t' 
		ls_update_string += dw_detail.GetItemString(ll_row, "product_code") + '~t' 		
		
		ls_update_string += dw_detail.GetItemString(ll_row, "qty_wt_ind") + '~t'
		
		If mid(dw_detail.GetItemString(ll_row, "qty_wt_ind"),1,1) = 'Q' Then 
			ls_update_string += string(dw_detail.GetItemNumber(ll_row, "scheduled_qty")) + '~t'
		Else		
			ls_update_string += string(dw_detail.GetItemNumber(ll_row, "weight")) + '~t'
		End If
		
		If isnull(dw_detail.GetItemDate(ll_row, "from_production_date")) Then
			ls_update_string += '0000-00-00' + '~t'
		Else
			If dw_detail.GetItemDate(ll_row, "from_production_date") > ld_order_delivery_date Then
				iw_frame.SetMicroHelp("From Production Date must be a valid date and cannot be greater than the delivery Date")
				Return False
			Else	
				ls_update_string += string(dw_detail.GetItemDate(ll_row, "from_production_date"), "yyyy-mm-dd") + '~t'
			End IF	
		End If
		
		If isnull(dw_detail.GetItemDate(ll_row, "to_production_date")) Then
			ls_update_string += '0000-00-00' + '~t'
		Else
			If dw_detail.GetItemDate(ll_row, "to_production_date") > ld_order_delivery_date Then
				iw_frame.SetMicroHelp("To Production Date must be a valid date and cannot be greater than the delivery Date")
				Return False
			Else	
				ls_update_string += string(dw_detail.GetItemDate(ll_row, "to_production_date"), "yyyy-mm-dd") + '~t'
			End IF	
	
		End If
		
		If isnull(dw_detail.GetItemDate(ll_row, "from_production_date")) or isnull(dw_detail.GetItemDate(ll_row, "to_production_date")) Then
	//		do nothing	 
		Else
			If dw_detail.GetItemDate(ll_row, "to_production_date") < dw_detail.GetItemDate(ll_row, "from_production_date") Then
				iw_frame.SetMicroHelp("To production date must be greater than from production date")
				Return False
			End If
		End If
		
		If isnull(dw_detail.GetItemDate(ll_row, "from_production_date")) or isnull(dw_detail.GetItemDate(ll_row, "to_production_date")) Then
	//		do nothing	 
		Else
			If DaysAfter(dw_detail.GetItemDate(ll_row, "from_production_date"), dw_detail.GetItemDate(ll_row, "to_production_date")) > 9999 Then
				iw_frame.SetMicroHelp("To production date is more than 9999 days after from production date")
				Return False
			End If
		End If
		
		If dw_detail.GetItemString(ll_row, "qty_wt_ind") = 'W' Then
			if dw_header.GetItemString(1, "default_qty_wt_ind") <> 'W' Then
				iw_frame.SetMicroHelp("Qty/Wt indicator must be Q unless order is export/indirect or transfer")
				Return False
			End If
		End If
	
		If dw_detail.GetItemString(ll_row, "override_ind") = ' ' Then
			If isnull(dw_detail.GetItemString(ll_row, "age_code")) or (dw_detail.GetItemString(ll_row, "age_code") <= ' ') Then
				iw_frame.SetMicroHelp("Age code must be entered")
				Return False
			Else	
				ls_update_string += dw_detail.GetItemString(ll_row, "age_code") + '~t'
			End If
		Else	
			If isnull(dw_detail.GetItemDate(ll_row, "from_production_date")) or isnull(dw_detail.GetItemDate(ll_row, "to_production_date")) Then
				iw_frame.SetMicroHelp("From and to production dates are required if override date is checked")
				Return False
			Else	
				ls_update_string += dw_detail.GetItemString(ll_row, "age_code") + '~t'
			End If
		End If
	
		ls_update_string += dw_detail.GetItemString(ll_row, "override_ind") + '~t'
		
		ls_update_string += dw_detail.GetItemString(ll_row, "age_calc_type") + "~r~n"
	End If		

Next

ls_header_string  = dw_header.Describe( "DataWindow.Data")

SetPointer(HourGlass!)

li_ret = iu_ws_pas_share.nf_pasp77gr(istr_error_info, & 
										ls_header_string, &
										ls_update_string)	

SetPointer(HourGlass!)
If li_ret = 0 Then
	wf_inquire()
	iw_frame.SetMicroHelp("Modification Successful")
	dw_detail.ResetUpdate()
	
Else
	iw_frame.SetMicroHelp(istr_error_info.se_message)
End If



Return True

end function

public function boolean wf_inquire ();String	ls_header_string, &
			ls_detail_string, &
			ls_find_string
			
Integer	li_ret

Long		ll_count, &
			ll_start_pos, &
			ll_found_row


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 


istr_error_info.se_event_name = "wf_inquire"
istr_error_info.se_procedure_name = "nf_pasp76gr"
istr_error_info.se_message = Space(71)

ls_header_string = is_order_number + '~t'
										
li_ret = iu_ws_pas_share.nf_pasp76gr(istr_error_info, & 
										ls_header_string, &
										ls_detail_string, &
										is_include_exclude_string)											
										
dw_header.Reset()
ll_count = dw_header.ImportString(ls_header_string)
If ll_count = -1 Then
	dw_header.InsertRow(0)
	dw_header.SetItem(1, "order_number", is_order_number)
	iw_frame.SetMicroHelp("Order not found")
	dw_detail.ResetUpdate()
	dw_header.ResetUpdate()
	This.SetRedraw(True)
	Return True
End If

dw_header.ResetUpdate()

dw_detail.Reset()
dw_detail.ImportString(ls_detail_string)
dw_detail.ResetUpdate()
dw_detail.SetFocus()

If (ib_open_inq_window) = False and (is_order_line > '') Then
	ls_find_string = "line_number = '" 
	ls_find_string += is_order_line + "'"
	ll_start_pos = 1
	ll_found_row = dw_detail.Find(ls_find_string, ll_start_pos, dw_detail.RowCount() + 1)
	If ll_found_row > 0 Then
			dw_detail.ScrollToRow(ll_found_row)
			dw_detail.SetRow(ll_found_row)
	End IF
End IF

ib_open_inq_window = True

If dw_header.GetItemDate(1, "ship_date") < today() then
	iw_frame.SetMicroHelp("The order can not be changed -- Ship date is less than today's date")
	This.SetRedraw(True)
	SetPointer(Arrow!)
	Return True
End If 

IF UPPER(is_Mod_Access) <>  'Y' THEN
	dw_detail.Object.scheduled_qty.Protect = 1
	dw_detail.Object.scheduled_qty.Background.Color = 78682240	
//	dw_detail.Object.override_ind.Protect = 1
//	dw_detail.Object.override_ind.Background.Color = 78682240	
//	dw_detail.Object.age_code.Protect = 1
//	dw_detail.Object.age_code.Background.Color = 78682240
	dw_detail.Object.include_exclude_ind.Protect = 1
	dw_detail.Object.include_exclude_ind.Background.Color = 78682240	
	dw_detail.Object.weight.Protect = 1
	dw_detail.Object.weight.Background.Color = 78682240	
	dw_detail.Object.age_calc_type.Protect = 1
	dw_detail.Object.age_calc_type.Background.Color = 78682240
END IF	

This.SetRedraw(True)

If ll_count > 0 Then
	iw_frame.SetMicroHelp("Inquire Successful")
End If

Return True
end function

on w_change_order_production_dates_new.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_change_order_production_dates_new.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event open;call super::open;Environment	le_env

String ls_temp 

ls_temp = Message.StringParm	

If Len(ls_temp) > 0 and lower(ls_temp) <> 'w_change_order_production_dates_new' and lower(ls_temp) <> 'w_change_order_production_dates_new,'  Then
	is_order_number = mid(ls_temp,1,5)
//6th char is a tab	
	is_order_line = mid(ls_temp,7,4)
	ib_open_inq_window = False
Else
	ib_open_inq_window = True
	is_order_line = ''
End If

iu_pas201 = Create u_pas201


This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "change_production_dates"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_change_order_production_dates_inq'

iu_pas201 = Create u_pas201
iu_ws_pas_share = Create u_ws_pas_share


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'order_number'
		If isnull(dw_header.GetItemString(1,"order_number")) Then
			Message.StringParm = ''
		Else			
			Message.StringParm = dw_header.GetItemString(1,"order_number")
		End if
	Case "email_fax_ind"
		message.StringParm = is_email_fax_ind
	Case "report_type"
		message.StringParm = is_report_type			
End choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'order_number'
		dw_header.SetItem(1, "order_number", as_value)
		is_order_Number = as_value
		
End Choose
end event

event activate;call super::activate;boolean	lb_rtn
string	ls_classname

// Set Security on ActiveX Control.
ls_classname = mid(UPPER(this.ClassName()),1,31) + "_EXT"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					is_Add_Access, is_Del_Access, is_Mod_Access, is_Inq_Access)
					
IF NOT lb_rtn THEN
	// if the window is not listed in the security at all, then allow no permissions
	is_Add_Access =  'N'
End if

//Rights to Add New Rows
IF UPPER(is_Add_Access) = 'N' THEN
	iw_frame.im_menu.mf_Disable('m_addrow')
END IF	

iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

iw_frame.im_menu.mf_Disable('m_generatesales')		
iw_frame.im_menu.mf_Disable('m_complete')	

//If UPPER(is_Mod_Access) = 'Y' THEN
//	iw_frame.im_menu.mf_Enable('m_save')
//Else
//	iw_frame.im_menu.mf_Disable('m_save')
//End If	

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_new')	
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

iw_frame.im_menu.mf_Disable('m_generatesales')		
iw_frame.im_menu.mf_Disable('m_complete')		

end event

event ue_filesave;call super::ue_filesave;wf_update()
end event

event ue_postopen;call super::ue_postopen;IF message.nf_get_app_id() <> 'PAS'	Then 
	ib_open_inq_window = True
End If

wf_retrieve()
end event

event close;call super::close;destroy iu_ws_pas_share
end event

type dw_detail from u_base_dw_ext within w_change_order_production_dates_new
integer y = 416
integer width = 3611
integer height = 896
integer taborder = 20
string dataobject = "d_change_order_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;is_selection = '1'

DataWindowChild	ldw_DDDWChild

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild("qty_wt_ind", ldw_DDDWChild)
ldw_DDDWChild.SetTransObject(SQLCA)
ldw_DDDWChild.Retrieve()


This.GetChild("age_calc_type", ldw_DDDWChild)
ldw_DDDWChild.SetTransObject(SQLCA)
ldw_DDDWChild.Retrieve()

end event

event clicked;call super::clicked;If Row > 0 Then
	Choose Case dwo.name
		Case 'include_exclude_ind'	
			wf_apply_ie_plants(is_include_exclude_string, row)					
	END Choose
End If			
end event

event rbuttondown;call super::rbuttondown;IF message.nf_get_app_id() <> 'PAS' Then Return 

m_change_order_popup	lm_popup

lm_popup = Create m_change_order_popup

lm_popup.m_changeorderprint.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup
end event

event itemfocuschanged;call super::itemfocuschanged;If (dwo.name = "from_production_date") or (dwo.name = "to_production_date") Then
	This.SelectText(1,0)
End If

end event

type dw_header from u_base_dw_ext within w_change_order_production_dates_new
integer width = 2999
integer height = 416
integer taborder = 10
string dataobject = "d_change_order_hdr"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)

end event

event rbuttondown;call super::rbuttondown;IF message.nf_get_app_id() <> 'PAS' Then Return 

m_change_order_popup	lm_popup

lm_popup = Create m_change_order_popup

lm_popup.m_changeorderprint.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup
end event

