HA$PBExportHeader$w_pas_sales_inv_position_inq.srw
forward
global type w_pas_sales_inv_position_inq from w_netwise_response
end type
type dw_plant from u_plant within w_pas_sales_inv_position_inq
end type
type dw_division from u_division within w_pas_sales_inv_position_inq
end type
type gb_1 from groupbox within w_pas_sales_inv_position_inq
end type
type rb_boxes from radiobutton within w_pas_sales_inv_position_inq
end type
type rb_loads from radiobutton within w_pas_sales_inv_position_inq
end type
type cbx_showall from checkbox within w_pas_sales_inv_position_inq
end type
type cbx_pork from checkbox within w_pas_sales_inv_position_inq
end type
type dw_product_status from datawindow within w_pas_sales_inv_position_inq
end type
end forward

global type w_pas_sales_inv_position_inq from w_netwise_response
integer width = 1847
integer height = 820
long backcolor = 12632256
event ue_ncclicked pbm_ncmbuttondown
dw_plant dw_plant
dw_division dw_division
gb_1 gb_1
rb_boxes rb_boxes
rb_loads rb_loads
cbx_showall cbx_showall
cbx_pork cbx_pork
dw_product_status dw_product_status
end type
global w_pas_sales_inv_position_inq w_pas_sales_inv_position_inq

type variables
Boolean			ib_valid_to_close, &
					 ib_open, &
					 ib_modified = false

//1-13 jac 

String         is_selected_status_array, & 
               is_columnname, &
					is_product_status_table

Integer		ii_RowsSelected,&
		ii_x, ii_y, &
		ii_dddw_x, ii_dddw_y

Long		il_RowBelowMouse, &
 		il_xpos, &
		il_ypos, &
		il_row, &
		il_old_row, &
		il_selected_row, &
		il_selected_status_max = 8, &
		il_horz_scroll_pos
		
		
		datastore		ids_selected_status
//

w_pas_sales_inv_position	iw_parent
end variables

forward prototypes
public subroutine wf_get_selected_status (datawindow adw_datawindow)
public subroutine wf_set_selected_status ()
end prototypes

event ue_ncclicked;if ib_open then close(w_prod_status_popup)
end event

public subroutine wf_get_selected_status (datawindow adw_datawindow);//1-13 jac

long							ll_count
string						ls_prod_status_string, ls_stat_desc
integer						li_windowx, li_windowy, li_height, li_height_H
Application					la_Application

//this function will get the list of product status values that exist in the window that were previously
//selected from the popup (dropdown) window
la_Application = GetApplication()
is_selected_status_array = ""
for ll_count = 1 to il_selected_status_max
    ls_prod_status_string = adw_datawindow.getitemstring(1, "product_status" + string(ll_count))
	 ls_stat_desc = adw_datawindow.getitemstring(1, "status_desc" + string(ll_count))
//	
   if ll_count = 1 then
	   if isnull(ls_prod_status_string) then
		  ls_prod_status_string = 'ALL'
		  ls_stat_desc = 'ALL'
	   END IF
	ELSE 
	   if isnull(ls_prod_status_string) then
	   	ls_prod_status_string = ' '
	   	ls_stat_desc = ' '
	   end if
	end if
	is_selected_status_array += ls_prod_status_string + "~t"
	 
	is_selected_status_array += ls_stat_desc + "~t"
	
next

if isnull(is_selected_status_array) Then
	//do nothing
else
   is_selected_status_array = is_selected_status_array + "~r~n"
end if

// find opening x/y positions for "Multi select drop down"
la_Application = GetApplication()
Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose

//x = across and y = down positions
ii_dddw_x = integer(dw_product_status.Object.product_status1.X) + 35
ii_dddw_y = integer(dw_product_status.Object.product_status1.y) + 180 

end subroutine

public subroutine wf_set_selected_status ();//1-13 jac

DataStore			lds_selected_status
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt
string				ls_product_status, ls_stat_desc

//this function will take the status fields from the popup window and push them to the main datawindow

lds_selected_status = Create DataStore
lds_selected_status.DataObject = 'd_product_status_2'
ll_rowcount = lds_selected_status.importstring(is_selected_status_array)

if ib_modified then
	for ll_rowcnt = 1 to il_selected_status_max
		ls_product_status = lds_selected_status.getitemstring(1, "product_status"+ string(ll_rowcnt))
		dw_product_status.setitem(1, "product_status"+ string(ll_rowcnt), ls_product_status)
		ls_stat_desc = lds_selected_status.getitemstring(1, "status_desc"+ string(ll_rowcnt))
		dw_product_status.setitem(1, "status_desc"+ string(ll_rowcnt), ls_stat_desc)
	next

end if

dw_product_status.accepttext()
ib_open = false
//
end subroutine

event ue_postopen;call super::ue_postopen;String		ls_plant, &
				ls_division, &
				ls_boxes_or_loads, &
				ls_product_status
				


SetRedraw(False)

ls_plant = iw_parent.wf_get_plant()
ls_division = iw_parent.dw_division.uf_get_division() + "~t" + &
					iw_parent.dw_division.uf_get_division_descr()
ls_boxes_or_loads = iw_parent.wf_get_boxes_or_loads()
if ls_boxes_or_loads = 'L' Then
	rb_loads.Checked = True
Else
	rb_boxes.Checked = True
End If

cbx_pork.Checked = (iw_parent.is_pork_sales_option = "P")

//1-13 jac
ls_product_status = iw_parent.wf_get_selected_status()
is_selected_status_array = iw_parent.wf_get_selected_status()
//

If Not iw_frame.iu_string.nf_IsEmpty(ls_plant) Then 
	dw_plant.Reset()
	dw_plant.ImportString(ls_plant)
End If

If Not iw_frame.iu_string.nf_IsEmpty(ls_division) Then 
	dw_division.Reset()
	dw_division.ImportString(ls_division)
End If
//1-13 jac
if is_selected_status_array = ' ' Then
   ls_product_status = 'ALL'
   dw_product_status.SetItem(1,'status_desc1',ls_product_status)
   dw_product_status.InsertRow(0)
else 
	ls_product_status = is_selected_status_array
	dw_product_status.ImportString(is_selected_status_array)
	dw_product_Status.Accepttext()
end if 
//

SetRedraw(True)
end event

on open;call w_netwise_response::open;iw_parent = Message.PowerObjectParm

This.Title = iw_parent.Title + " Inquire"

dw_plant.SetFocus()
end on

on close;call w_netwise_response::close;If Not ib_valid_to_close Then Message.StringParm = ""
end on

on w_pas_sales_inv_position_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_division=create dw_division
this.gb_1=create gb_1
this.rb_boxes=create rb_boxes
this.rb_loads=create rb_loads
this.cbx_showall=create cbx_showall
this.cbx_pork=create cbx_pork
this.dw_product_status=create dw_product_status
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.gb_1
this.Control[iCurrent+4]=this.rb_boxes
this.Control[iCurrent+5]=this.rb_loads
this.Control[iCurrent+6]=this.cbx_showall
this.Control[iCurrent+7]=this.cbx_pork
this.Control[iCurrent+8]=this.dw_product_status
end on

on w_pas_sales_inv_position_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_division)
destroy(this.gb_1)
destroy(this.rb_boxes)
destroy(this.rb_loads)
destroy(this.cbx_showall)
destroy(this.cbx_pork)
destroy(this.dw_product_status)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant, &
			ls_division, &
			ls_string, &
			ls_product_status, &
			ls_stat_desc
long		ll_rowcount, ll_count

			
If dw_plant.AcceptText() = -1 or dw_division.AcceptText() = -1 &
	Then Return

ls_plant = dw_plant.nf_get_plant_code()
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	Return
End if

ls_string = ls_plant + "~t" + dw_plant.uf_get_plant_descr() + "~t"
//1-13 jac
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) Then
		ls_product_status = " "
	end if
  ls_string += ls_product_status + "~t"
  ls_stat_desc = dw_product_status.GetItemString(1, "status_desc" + string(ll_count))
  ls_stat_desc = TRIM( ls_stat_desc)
  if isnull( ls_stat_desc) Then
  	 ls_stat_desc = " "
  end if
  ls_string +=  ls_stat_desc + "~t"
next
dw_product_status.AcceptText()

dw_product_status.ShareData(iw_parent.dw_product_status_window)
//
iw_parent.dw_division.uf_set_division(ls_division)

If rb_boxes.Checked Then
	ls_string += "~t" + "B"
Else
	ls_string += "~t" + "L"
End If

If cbx_showall.Checked Then
	ls_string += "~t" + "L"
Else
	ls_string += "~t" + "S"
End If

If cbx_pork.Checked Then
	ls_string += "~t" + "P"
Else
	ls_string += "~t" + "B"
End If

ib_valid_to_close = True
CloseWithReturn(This, ls_string)
end event

event clicked;call super::clicked;//1-13 jac
if ib_open then close(w_prod_status_popup)
//
end event

event ue_get_data;call super::ue_get_data;//1-13  jac            
STRING ls_return_value

Choose Case as_value
   case "product_status"
       ls_return_value = ''
   case "selected"
       ls_return_value = is_selected_status_array
   case "dddwxpos"
       ls_return_value = string(ii_dddw_x)
   case "dddwypos"
      ls_return_value = string(ii_dddw_y)
end choose
                                
return ls_return_value
//

end event

event ue_set_data;call super::ue_set_data;//1-13 jac

choose case as_data_item
	case "selected"
		is_selected_status_array = as_value
		////test
//messagebox("ue_set_data", as_value)
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
end choose
//
end event

type cb_base_help from w_netwise_response`cb_base_help within w_pas_sales_inv_position_inq
integer x = 1207
integer y = 548
integer taborder = 110
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_pas_sales_inv_position_inq
integer x = 905
integer y = 548
integer taborder = 100
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_pas_sales_inv_position_inq
integer x = 603
integer y = 548
integer taborder = 90
end type

event cb_base_ok::getfocus;call super::getfocus;if ib_open then close(w_prod_status_popup)
end event

type dw_plant from u_plant within w_pas_sales_inv_position_inq
integer x = 37
integer width = 1582
integer taborder = 10
end type

type dw_division from u_division within w_pas_sales_inv_position_inq
integer y = 88
integer taborder = 20
end type

type gb_1 from groupbox within w_pas_sales_inv_position_inq
integer x = 46
integer y = 396
integer width = 352
integer height = 216
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Display"
end type

type rb_boxes from radiobutton within w_pas_sales_inv_position_inq
integer x = 110
integer y = 448
integer width = 233
integer height = 76
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Boxes"
boolean lefttext = true
end type

type rb_loads from radiobutton within w_pas_sales_inv_position_inq
integer x = 110
integer y = 516
integer width = 233
integer height = 76
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Loads"
boolean lefttext = true
end type

type cbx_showall from checkbox within w_pas_sales_inv_position_inq
integer x = 539
integer y = 344
integer width = 576
integer height = 76
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Show All Age Codes"
boolean checked = true
boolean lefttext = true
end type

type cbx_pork from checkbox within w_pas_sales_inv_position_inq
integer x = 539
integer y = 420
integer width = 576
integer height = 76
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Pork Sales Option"
boolean lefttext = true
end type

type dw_product_status from datawindow within w_pas_sales_inv_position_inq
integer x = 55
integer y = 176
integer width = 1033
integer height = 96
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_product_status_2"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;is_columnname = dwo.name

choose case dwo.name
	case 'status_desc1'
		ib_open = true
		wf_get_selected_status(this)
		OpenWithParm( w_prod_status_popup, parent)
end choose
end event

