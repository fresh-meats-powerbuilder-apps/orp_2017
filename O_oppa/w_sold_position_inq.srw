HA$PBExportHeader$w_sold_position_inq.srw
$PBExportComments$Sold Position Inquire Window
forward
global type w_sold_position_inq from w_netwise_response
end type
type dw_division from u_division within w_sold_position_inq
end type
type dw_plant_type from u_plant_type within w_sold_position_inq
end type
type dw_country_1 from u_country_1 within w_sold_position_inq
end type
end forward

global type w_sold_position_inq from w_netwise_response
int Width=1701
int Height=516
boolean TitleBar=true
string Title=""
long BackColor=12632256
dw_division dw_division
dw_plant_type dw_plant_type
dw_country_1 dw_country_1
end type
global w_sold_position_inq w_sold_position_inq

type variables
Boolean			ib_ok_to_close, &
			ib_inquire

w_weekly_pa		iw_parentwindow


end variables

on w_sold_position_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_plant_type=create dw_plant_type
this.dw_country_1=create dw_country_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_plant_type
this.Control[iCurrent+3]=this.dw_country_1
end on

on w_sold_position_inq.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_plant_type)
destroy(this.dw_country_1)
end on

event open;call super::open;If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
		This.Title = iw_parentwindow.Title + " Inquire"
	End If
End If


end event

event ue_base_cancel;Close(This)
end event

event ue_base_ok; String		ls_plant, &
				ls_division, &
				ls_string, &
				ls_country


u_string_functions		lu_strings

If dw_division.AcceptText() = -1 &
   or dw_plant_type.AcceptText() = -1 &
	    or dw_country_1.AcceptText() = -1 & 	
   Then return 
	
ls_division = dw_division.uf_get_division()
If lu_strings.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End If

ls_plant = dw_plant_type.uf_get_plant_type()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant type is a required field")
	dw_plant_type.SetFocus()
	Return
End if

ls_country = dw_country_1.uf_get_country()
If lu_strings.nf_IsEmpty(ls_country) Then
	iw_frame.SetMicroHelp("Country is a required field")
	dw_country_1.SetFocus()
	Return
End if

iw_parentwindow.Event ue_set_data('plant_type',ls_plant)
iw_parentwindow.Event ue_set_data('division',ls_division)
iw_parentwindow.Event ue_set_data('country',ls_country)

ib_ok_to_close = True

Close(This)
            

end event

event ue_postopen;iw_parentwindow.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)

iw_parentwindow.Event ue_get_data('plant_type')
dw_plant_type.uf_set_plant_type(Message.StringParm)

iw_parentwindow.Event ue_get_data('country')
dw_country_1.uf_set_country(Message.StringParm)

dw_division.SetFocus()



end event

event close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF

end event

type cb_base_help from w_netwise_response`cb_base_help within w_sold_position_inq
int X=1385
int Y=292
int TabOrder=60
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_sold_position_inq
int X=1033
int Y=292
int TabOrder=50
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_sold_position_inq
int X=681
int Y=292
int TabOrder=40
end type

type dw_division from u_division within w_sold_position_inq
int X=69
int Y=0
int TabOrder=10
end type

type dw_plant_type from u_plant_type within w_sold_position_inq
int X=0
int Y=176
int TabOrder=30
boolean BringToTop=true
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_country_1 from u_country_1 within w_sold_position_inq
int X=78
int Y=84
int TabOrder=20
boolean BringToTop=true
end type

event constructor;call super::constructor;//This.InsertRow(0)
//dw_country_1.uf_Enable(True)

end event

