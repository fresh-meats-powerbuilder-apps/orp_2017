HA$PBExportHeader$w_weekly_pa_inq.srw
$PBExportComments$Inquire Window for Weekly Pa Extended
forward
global type w_weekly_pa_inq from w_netwise_response
end type
type dw_plant from u_plant within w_weekly_pa_inq
end type
type dw_division from u_division within w_weekly_pa_inq
end type
type dw_week_end_date from u_week_end_date within w_weekly_pa_inq
end type
type dw_loads_boxes from u_loads_boxes within w_weekly_pa_inq
end type
type dw_product_status from datawindow within w_weekly_pa_inq
end type
end forward

global type w_weekly_pa_inq from w_netwise_response
integer width = 1861
integer height = 904
string title = ""
long backcolor = 12632256
event ue_ncclicked pbm_nclbuttondown
dw_plant dw_plant
dw_division dw_division
dw_week_end_date dw_week_end_date
dw_loads_boxes dw_loads_boxes
dw_product_status dw_product_status
end type
global w_weekly_pa_inq w_weekly_pa_inq

type variables
Boolean			ib_ok_to_close, &
			      ib_inquire, &
			      ib_open, &
					ib_modified = false

w_weekly_pa		iw_parentwindow

//2-13 jac
String         is_selected_status_array, & 
               is_columnname, &
					is_product_status_table
					
Integer		ii_RowsSelected,&
		ii_x, ii_y, &
		ii_dddw_x, ii_dddw_y

Long		il_RowBelowMouse, &
 		il_xpos, &
		il_ypos, &
		il_row, &
		il_old_row, &
		il_selected_row, &
		il_selected_status_max = 8, &
		il_horz_scroll_pos
		
		
		datastore		ids_selected_status

//
end variables

forward prototypes
public subroutine wf_get_selected_status (datawindow adw_datawindow)
public subroutine wf_set_selected_status ()
end prototypes

event ue_ncclicked;if ib_open then close(w_prod_status_popup)
end event

public subroutine wf_get_selected_status (datawindow adw_datawindow);long							ll_count
string						ls_prod_status_string, ls_stat_desc
integer						li_windowx, li_windowy, li_height, li_height_H
Application					la_Application

//this function will get the list of product status values that exist in the window that were previously
//selected from the popup (dropdown) window
la_Application = GetApplication()
is_selected_status_array = ""
for ll_count = 1 to il_selected_status_max
    ls_prod_status_string = adw_datawindow.getitemstring(1, "product_status" + string(ll_count))
	 ls_stat_desc = adw_datawindow.getitemstring(1, "status_desc" + string(ll_count))
//	
   if ll_count = 1 then
	   if isnull(ls_prod_status_string) then
		  ls_prod_status_string = 'ALL'
		  ls_stat_desc = 'ALL'
	   END IF		
	else
   	if isnull(ls_prod_status_string) then
	   	ls_prod_status_string = ' '
		   ls_stat_desc = ' '
	   end if
	end if
	is_selected_status_array += ls_prod_status_string + "~t"
	 
	is_selected_status_array += ls_stat_desc + "~t"
	
next

if isnull(is_selected_status_array) Then
	//do nothing
else
   is_selected_status_array = is_selected_status_array + "~r~n"
end if

// find opening x/y positions for "Multi select drop down"
la_Application = GetApplication()
Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose

//x = across and y = down positions
ii_dddw_x = integer(dw_product_status.Object.product_status1.X) + 35
ii_dddw_y = integer(dw_product_status.Object.product_status1.y) + 180 
end subroutine

public subroutine wf_set_selected_status ();DataStore			lds_selected_status
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt
string				ls_product_status, ls_stat_desc

//this function will take the status fields from the popup window and push them to the main datawindow

lds_selected_status = Create DataStore
lds_selected_status.DataObject = 'd_product_status_2'
ll_rowcount = lds_selected_status.importstring(is_selected_status_array)

if ib_modified then
	for ll_rowcnt = 1 to il_selected_status_max
		ls_product_status = lds_selected_status.getitemstring(1, "product_status"+ string(ll_rowcnt))
		dw_product_status.setitem(1, "product_status"+ string(ll_rowcnt), ls_product_status)
		ls_stat_desc = lds_selected_status.getitemstring(1, "status_desc"+ string(ll_rowcnt))
		dw_product_status.setitem(1, "status_desc"+ string(ll_rowcnt), ls_stat_desc)
	next

end if

dw_product_status.accepttext()
ib_open = false


end subroutine

on w_weekly_pa_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_division=create dw_division
this.dw_week_end_date=create dw_week_end_date
this.dw_loads_boxes=create dw_loads_boxes
this.dw_product_status=create dw_product_status
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_week_end_date
this.Control[iCurrent+4]=this.dw_loads_boxes
this.Control[iCurrent+5]=this.dw_product_status
end on

on w_weekly_pa_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_division)
destroy(this.dw_week_end_date)
destroy(this.dw_loads_boxes)
destroy(this.dw_product_status)
end on

event open;call super::open;


If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
		This.Title = iw_parentwindow.Title + " Inquire"
	End If
End If

dw_plant.SetFocus()
end event

event ue_base_cancel;Close(This)
end event

event ue_base_ok; String		ls_plant, &
 				ls_plant_desc, &
				ls_division, &
				ls_division_desc, &
				ls_week_end_date, &
				ls_loads_boxes, &
				ls_product_status, &
				ls_string, &
				ls_stat_desc
//date			ldt_week_end_date
integer     ll_count

u_string_functions		lu_strings

If dw_division.AcceptText() = -1 &
   or dw_plant.AcceptText() = -1 &
	or dw_week_end_date.AcceptText() = -1 &
	or dw_loads_boxes.AcceptText() = -1 &  	
   Then return 
	
ls_division = dw_division.uf_get_division()
If lu_strings.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field!")
	dw_division.SetFocus()
	return
End If

ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field!")
	dw_plant.SetFocus()
	Return
End if

ls_week_end_date = string(dw_week_end_date.uf_get_week_end_date())
//If lu_strings.nf_IsEmpty(ls_week_end_date) Then
//	iw_frame.SetMicroHelp("Week End Date is a required field!")
//	dw_week_end_date.SetFocus()
//	Return
//End if

ls_loads_boxes = dw_loads_boxes.uf_get_loads_boxes()
//If lu_strings.nf_IsEmpty(ls_loads_boxes) Then
//	iw_frame.SetMicroHelp("Loads or Boxes is a required field!")
//	dw_loads_boxes.SetFocus()
//	Return
//End if

// 1-13 jac 
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) Then
		ls_product_status = " "
	end if
//	if ls_product_status = 'ALL' then
//		ls_product_status = 'A'
//	end if 
   ls_string += ls_product_status + "~t"
   ls_stat_desc = dw_product_status.GetItemString(1, "status_desc" + string(ll_count))
   ls_stat_desc = TRIM( ls_stat_desc)
   if isnull( ls_stat_desc) Then
  	   ls_stat_desc = " "
   end if
   ls_string +=  ls_stat_desc + "~t"
next

dw_product_status.AcceptText()
dw_product_status.ShareData(iw_parentwindow.dw_product_status_window)
//

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('division',ls_division)
iw_parentwindow.Event ue_set_data('loads_boxes',ls_loads_boxes)
iw_parentwindow.Event ue_set_data('week_end_date',ls_week_end_date)

ib_ok_to_close = True
//1-13 jac
//Close(This)
CloseWithReturn(This, ls_string)

            

end event

event ue_postopen;String  ls_product_status

iw_parentwindow.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)

iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('week_end_date')
dw_week_end_date.uf_set_week_end_date(date(Message.StringParm))

iw_parentwindow.Event ue_get_data('loads_boxes')
dw_loads_boxes.uf_set_loads_boxes(Message.StringParm)

//1-13 jac
ls_product_status = iw_parentwindow.wf_get_selected_status()
is_selected_status_array = iw_parentwindow.wf_get_selected_status()

if is_selected_status_array = ' ' Then
   ls_product_status = 'ALL'
   dw_product_status.SetItem(1,'status_desc1',ls_product_status)
   dw_product_status.InsertRow(0)
else 
	ls_product_status = is_selected_status_array
	dw_product_status.ImportString(is_selected_status_array)
	dw_product_Status.Accepttext()
end if 
//
end event

event close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF

end event

event ue_set_data;call super::ue_set_data;//1-13 jac

choose case as_data_item
	case "selected"
		is_selected_status_array = as_value
		////test
//messagebox("ue_set_data", as_value)
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
end choose
//
end event

event ue_get_data;call super::ue_get_data;//1-13  jac	
STRING	ls_return_value

Choose Case as_value
   case "product_status"
		ls_return_value = ''
	case "selected"
		ls_return_value = is_selected_status_array
	case "dddwxpos"
		ls_return_value = string(ii_dddw_x)
	case "dddwypos"
		ls_return_value = string(ii_dddw_y)
end choose
		
return ls_return_value
//
end event

event clicked;call super::clicked;//1-13 jac
if ib_open then close(w_prod_status_popup)
//
end event

type cb_base_help from w_netwise_response`cb_base_help within w_weekly_pa_inq
integer x = 1541
integer y = 620
integer taborder = 80
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_weekly_pa_inq
integer x = 1189
integer y = 620
integer taborder = 70
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_weekly_pa_inq
integer x = 837
integer y = 620
integer taborder = 60
end type

event cb_base_ok::getfocus;call super::getfocus;if ib_open then close(w_prod_status_popup)
end event

type dw_plant from u_plant within w_weekly_pa_inq
integer x = 251
integer width = 1582
integer taborder = 10
end type

type dw_division from u_division within w_weekly_pa_inq
integer x = 219
integer y = 88
integer taborder = 20
end type

type dw_week_end_date from u_week_end_date within w_weekly_pa_inq
event ue_post_constructor ( )
integer y = 272
integer width = 923
integer height = 92
integer taborder = 40
end type

event ue_post_constructor;call super::ue_post_constructor;This.InsertRow(0)
This.uf_enable(True)

end event

event constructor;call super::constructor;This.PostEvent('ue_post_constructor')
This.uf_set_week_end_type('W')
end event

type dw_loads_boxes from u_loads_boxes within w_weekly_pa_inq
integer x = 178
integer y = 436
integer width = 407
integer height = 296
integer taborder = 50
boolean bringtotop = true
end type

type dw_product_status from datawindow within w_weekly_pa_inq
integer x = 55
integer y = 176
integer width = 1102
integer height = 96
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_product_status_2"
boolean border = false
boolean livescroll = true
end type

event clicked;is_columnname = dwo.name

choose case dwo.name
	case 'status_desc1'
		ib_open = true
		wf_get_selected_status(this)
		OpenWithParm( w_prod_status_popup, parent)
end choose
end event

