HA$PBExportHeader$w_box_inventory_target.srw
$PBExportComments$Boxed inventory Report
forward
global type w_box_inventory_target from w_netwise_sheet
end type
type st_8 from statictext within w_box_inventory_target
end type
type st_7 from statictext within w_box_inventory_target
end type
type st_6 from statictext within w_box_inventory_target
end type
type st_5 from statictext within w_box_inventory_target
end type
type st_4 from statictext within w_box_inventory_target
end type
type st_3 from statictext within w_box_inventory_target
end type
type sle_division_list from singlelineedit within w_box_inventory_target
end type
type st_divisions from statictext within w_box_inventory_target
end type
type rb_eight_days from radiobutton within w_box_inventory_target
end type
type rb_seven_days from radiobutton within w_box_inventory_target
end type
type rb_ready_to_ship_date from radiobutton within w_box_inventory_target
end type
type rb_total_on_hand from radiobutton within w_box_inventory_target
end type
type rb_weight from radiobutton within w_box_inventory_target
end type
type rb_quantity from radiobutton within w_box_inventory_target
end type
type dw_uom_code from u_uom_targ_code within w_box_inventory_target
end type
type st_1 from statictext within w_box_inventory_target
end type
type st_start_date from statictext within w_box_inventory_target
end type
type st_groups from statictext within w_box_inventory_target
end type
type cbx_combined from checkbox within w_box_inventory_target
end type
type cbx_uncombined from checkbox within w_box_inventory_target
end type
type cbx_gpo from checkbox within w_box_inventory_target
end type
type cbx_reservation from checkbox within w_box_inventory_target
end type
type dw_weekly_summary from u_netwise_dw within w_box_inventory_target
end type
type st_boxessold from statictext within w_box_inventory_target
end type
type st_2 from statictext within w_box_inventory_target
end type
type sle_last_update from singlelineedit within w_box_inventory_target
end type
type ole_1 from olecustomcontrol within w_box_inventory_target
end type
type gb_display_uom from groupbox within w_box_inventory_target
end type
type gb_inv_position from groupbox within w_box_inventory_target
end type
type gb_number_of_days from groupbox within w_box_inventory_target
end type
end forward

global type w_box_inventory_target from w_netwise_sheet
integer width = 2939
integer height = 1776
string title = "Boxed Inventory Target Report"
long backcolor = 67108864
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
sle_division_list sle_division_list
st_divisions st_divisions
rb_eight_days rb_eight_days
rb_seven_days rb_seven_days
rb_ready_to_ship_date rb_ready_to_ship_date
rb_total_on_hand rb_total_on_hand
rb_weight rb_weight
rb_quantity rb_quantity
dw_uom_code dw_uom_code
st_1 st_1
st_start_date st_start_date
st_groups st_groups
cbx_combined cbx_combined
cbx_uncombined cbx_uncombined
cbx_gpo cbx_gpo
cbx_reservation cbx_reservation
dw_weekly_summary dw_weekly_summary
st_boxessold st_boxessold
st_2 st_2
sle_last_update sle_last_update
ole_1 ole_1
gb_display_uom gb_display_uom
gb_inv_position gb_inv_position
gb_number_of_days gb_number_of_days
end type
global w_box_inventory_target w_box_inventory_target

type variables
Boolean		ib_m_Print_Status

//u_pas203		iu_pas203
//
u_ws_pas_share  iu_ws_pas_share

s_error		istr_error_info

Int		ii_column_width

String 		is_groupid

Date		idt_start_date


end variables

forward prototypes
public function string wf_transpose (ref datastore adw_source)
public function integer wf_change_loads_required_calc ()
public function string wf_convert_date (date adt_date)
public subroutine wf_set_reserv_calcs ()
public subroutine wf_set_sales_target_calcs ()
public subroutine wf_set_invent_target_calcs ()
public subroutine wf_set_invt_var_calcs ()
public subroutine wf_set_sold_from_invt_calcs ()
public subroutine wf_set_total_calcs ()
public subroutine wf_set_invt_calcs ()
public subroutine wf_set_production_calcs ()
public subroutine wf_set_sales_calcs1 ()
public subroutine wf_set_sales_calcs2 ()
public subroutine wf_set_sales_calcs3 ()
public function boolean wf_retrieve ()
end prototypes

public function string wf_transpose (ref datastore adw_source);DataStore	lds_2, &
				lds_3, &
				lds_4

String		ls_temp = "", &
				ls_syntax_begin = 'release 5; datawindow(units=0 timer_interval=0 processing=0 ) table(', &
				ls_syntax4, &
				ls_syntax3, &
				ls_syntax2

Integer		li_counter, &
				li_row_count, &
				li_col_count

// Here we create three temporary dynamic DataStores to hold the data after transposition
lds_2 = Create DataStore
lds_3 = Create DataStore
lds_4 = Create DataStore
ls_syntax4 = ls_syntax_begin
ls_syntax3 = ls_syntax_begin
ls_syntax2 = ls_syntax_begin
For li_counter = 1 to 15
	ls_syntax4 += 'column=(type=char(1) name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
	ls_syntax3 += 'column=(type=char(3) name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
	ls_syntax2 += 'column=(type=number name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
Next
ls_syntax4 += ')'
ls_syntax3 += ')'
ls_syntax2 += ')'
lds_4.Create(ls_syntax4)
lds_3.Create(ls_syntax3)
lds_2.Create(ls_syntax2)

lds_3.InsertRow(0)
lds_3.InsertRow(0)
li_row_count = adw_source.RowCount()
For li_counter = 1 to li_row_count
	lds_3.SetItem(1,li_counter,adw_source.GetItemString(li_counter,1))
	lds_3.SetItem(2,li_counter,adw_source.GetItemString(li_counter,2))
Next

For li_col_count = 1 to 14*8 + 3
	lds_2.InsertRow(0)
	For li_counter = 1 to li_row_count
		lds_2.SetItem(li_col_count,li_counter,adw_source.GetItemNumber(li_counter,li_col_count + 2))
	Next
Next

lds_4.InsertRow(0)
For li_counter = 1 to li_row_count
	lds_4.SetItem(1,li_counter,adw_source.GetItemString(li_counter,14*8+6))
Next

ls_temp = lds_3.Object.DataWindow.Data + "~r~n"
ls_temp += lds_2.Object.DataWindow.Data + "~r~n"
ls_temp += lds_4.Object.DataWindow.Data + "~r~n"
  
Destroy lds_2
Destroy lds_3
Destroy lds_4

Return ls_temp
end function

public function integer wf_change_loads_required_calc ();String			ls_average_box, &
					ls_uom
					

ls_uom = dw_uom_code.uf_get_uom_code()

SELECT tutltypes.TYPE_SHORT_DESC  
    INTO :ls_average_box  
    FROM tutltypes  
   WHERE ( tutltypes.RECORD_TYPE = "AVAILLDS" ) AND  
         ( tutltypes.TYPE_CODE = :ls_uom );  
			
			

ole_1.object.EntryRC(7,3,"=IF(C11 > 0,IF(C11 > C164, (C11 - C164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,4,"=IF(D11 > 0,IF(D11 > D164, (D11 - D164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,5,"=IF(E11 > 0,IF(E11 > E164, (E11 - E164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,6,"=IF(F11 > 0,IF(F11 > F164, (F11 - F164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,7,"=IF(G11 > 0,IF(G11 > G164, (G11 - G164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,8,"=IF(H11 > 0,IF(H11 > H164, (H11 - H164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,9,"=IF(I11 > 0,IF(I11 > I164, (I11 - I164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,10,"=IF(J11 > 0,IF(J11 > J164, (J11 - J164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,11,"=IF(K11 > 0,IF(K11 > K164, (K11 - K164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,12,"=IF(L11 > 0,IF(L11 > L164, (L11 - L164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,13,"=IF(M11 > 0,IF(M11 > M164, (M11 - M164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,14,"=IF(N11 > 0,IF(N11 > N164, (N11 - N164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,15,"=IF(O11 > 0,IF(O11 > O164, (O11 - O164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,16,"=IF(P11 > 0,IF(P11 > P164, (P11 - P164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,17,"=IF(Q11 > 0,IF(Q11 > Q164, (Q11 - Q164) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(7,19,"=IF(S11 > 0,IF(S11 > S164, (S11 - S164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,20,"=IF(T11 > 0,IF(T11 > T164, (T11 - T164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,21,"=IF(U11 > 0,IF(U11 > U164, (U11 - U164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,22,"=IF(V11 > 0,IF(V11 > V164, (V11 - V164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,23,"=IF(W11 > 0,IF(W11 > W164, (W11 - W164) /" +  &
		ls_average_box + ", 0),0)")
		
ole_1.object.EntryRC(15,3,"=IF(C19 > 0,IF(C19 > C178, (C19 - C178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,4,"=IF(D19 > 0,IF(D19 > D178, (D19 - D178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,5,"=IF(E19 > 0,IF(E19 > E178, (E19 - E178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,6,"=IF(F19 > 0,IF(F19 > F178, (F19 - F178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,7,"=IF(G19 > 0,IF(G19 > G178, (G19 - G178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,8,"=IF(H19 > 0,IF(H19 > H178, (H19 - H178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,9,"=IF(I19 > 0,IF(I19 > I178, (I19 - I178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,10,"=IF(J19 > 0,IF(J19 > J178, (J19 - J178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,11,"=IF(K19 > 0,IF(K19 > K178, (K19 - K178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,12,"=IF(L19 > 0,IF(L19 > L178, (L19 - L178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,13,"=IF(M19 > 0,IF(M19 > M178, (M19 - M178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,14,"=IF(N19 > 0,IF(N19 > N178, (N19 - N178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,15,"=IF(O19 > 0,IF(O19 > O178, (O19 - O178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,16,"=IF(P19 > 0,IF(P19 > P178, (P19 - P178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,17,"=IF(Q19 > 0,IF(Q19 > Q178, (Q19 - Q178) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(15,19,"=IF(S19 > 0,IF(S19 > S178, (S19 - S178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,20,"=IF(T19 > 0,IF(T19 > T178, (T19 - T178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,21,"=IF(U19 > 0,IF(U19 > U178, (U19 - U178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,22,"=IF(V19 > 0,IF(V19 > V178, (V19 - V178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,23,"=IF(W19 > 0,IF(W19 > W178, (W19 - W178) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(23,3,"=IF(C27 > 0,IF(C27 > C192, (C27 - C192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,4,"=IF(D27 > 0,IF(D27 > D192, (D27 - D192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,5,"=IF(E27 > 0,IF(E27 > E192, (E27 - E192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,6,"=IF(F27 > 0,IF(F27 > F192, (F27 - F192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,7,"=IF(G27 > 0,IF(G27 > G192, (G27 - G192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,8,"=IF(H27 > 0,IF(H27 > H192, (H27 - H192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,9,"=IF(I27 > 0,IF(I27 > I192, (I27 - I192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,10,"=IF(J27 > 0,IF(J27 > J192, (J27 - J192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,11,"=IF(K27 > 0,IF(K27 > K192, (K27 - K192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,12,"=IF(L27 > 0,IF(L27 > L192, (L27 - L192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,13,"=IF(M27 > 0,IF(M27 > M192, (M27 - M192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,14,"=IF(N27 > 0,IF(N27 > N192, (N27 - N192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,15,"=IF(O27 > 0,IF(O27 > O192, (O27 - O192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,16,"=IF(P27 > 0,IF(P27 > P192, (P27 - P192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,17,"=IF(Q27 > 0,IF(Q27 > Q192, (Q27 - Q192) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(23,19,"=IF(S27 > 0,IF(S27 > S192, (S27 - S192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,20,"=IF(T27 > 0,IF(T27 > T192, (T27 - T192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,21,"=IF(U27 > 0,IF(U27 > U192, (U27 - U192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,22,"=IF(V27 > 0,IF(V27 > V192, (V27 - V192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,23,"=IF(W27 > 0,IF(W27 > W192, (W27 - W192) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(31,3,"=IF(C32 > 0,IF(C32 > C206, (C32 - C206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,4,"=IF(D32 > 0,IF(D32 > D206, (D32 - D206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,5,"=IF(E32 > 0,IF(E32 > E206, (E32 - E206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,6,"=IF(F32 > 0,IF(F32 > F206, (F32 - F206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,7,"=IF(G32 > 0,IF(G32 > G206, (G32 - G206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,8,"=IF(H32 > 0,IF(H32 > H206, (H32 - H206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,9,"=IF(I32 > 0,IF(I32 > I206, (I32 - I206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,10,"=IF(J32 > 0,IF(J32 > J206, (J32 - J206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,11,"=IF(K32 > 0,IF(K32 > K206, (K32 - K206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,12,"=IF(L32 > 0,IF(L32 > L206, (L32 - L206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,13,"=IF(M32 > 0,IF(M32 > M206, (M32 - M206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,14,"=IF(N32 > 0,IF(N32 > N206, (N32 - N206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,15,"=IF(O32 > 0,IF(O32 > O206, (O32 - O206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,16,"=IF(P32 > 0,IF(P32 > P206, (P32 - P206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,17,"=IF(Q32 > 0,IF(Q32 > Q206, (Q32 - Q206) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(31,19,"=IF(S32 > 0,IF(S32 > S206, (S32 - S206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,20,"=IF(T32 > 0,IF(T32 > T206, (T32 - T206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,21,"=IF(U32 > 0,IF(U32 > U206, (U32 - U206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,22,"=IF(V32 > 0,IF(V32 > V206, (V32 - V206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,23,"=IF(W32 > 0,IF(W32 > W206, (W32 - W206) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(36,3,"=IF(C37 > 0,IF(C37 > C220, (C37 - C220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,4,"=IF(D37 > 0,IF(D37 > D220, (D37 - D220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,5,"=IF(E37 > 0,IF(E37 > E220, (E37 - E220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,6,"=IF(F37 > 0,IF(F37 > F220, (F37 - F220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,7,"=IF(G37 > 0,IF(G37 > G220, (G37 - G220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,8,"=IF(H37 > 0,IF(H37 > H220, (H37 - H220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,9,"=IF(I37 > 0,IF(I37 > I220, (I37 - I220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,10,"=IF(J37 > 0,IF(J37 > J220, (J37 - J220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,11,"=IF(K37 > 0,IF(K37 > K220, (K37 - K220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,12,"=IF(L37 > 0,IF(L37 > L220, (L37 - L220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,13,"=IF(M37 > 0,IF(M37 > M220, (M37 - M220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,14,"=IF(N37 > 0,IF(N37 > N220, (N37 - N220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,15,"=IF(O37 > 0,IF(O37 > O220, (O37 - O220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,16,"=IF(P37 > 0,IF(P37 > P220, (P37 - P220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,17,"=IF(Q37 > 0,IF(Q37 > Q220, (Q37 - Q220) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(36,19,"=IF(S37 > 0,IF(S37 > S220, (S37 - S220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,20,"=IF(T37 > 0,IF(T37 > T220, (T37 - T220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,21,"=IF(U37 > 0,IF(U37 > U220, (U37 - U220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,22,"=IF(V37 > 0,IF(V37 > V220, (V37 - V220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,23,"=IF(W37 > 0,IF(W37 > W220, (W37 - W220) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(41,3,"=IF(C42 > 0,IF(C42 > C234, (C42 - C234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,4,"=IF(D42 > 0,IF(D42 > D234, (D42 - D234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,5,"=IF(E42 > 0,IF(E42 > E234, (E42 - E234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,6,"=IF(F42 > 0,IF(F42 > F234, (F42 - F234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,7,"=IF(G42 > 0,IF(G42 > G234, (G42 - G234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,8,"=IF(H42 > 0,IF(H42 > H234, (H42 - H234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,9,"=IF(I42 > 0,IF(I42 > I234, (I42 - I234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,10,"=IF(J42 > 0,IF(J42 > J234, (J42 - J234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,11,"=IF(K42 > 0,IF(K42 > K234, (K42 - K234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,12,"=IF(L42 > 0,IF(L42 > L234, (L42 - L234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,13,"=IF(M42 > 0,IF(M42 > M234, (M42 - M234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,14,"=IF(N42 > 0,IF(N42 > N234, (N42 - N234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,15,"=IF(O42 > 0,IF(O42 > O234, (O42 - O234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,16,"=IF(P42 > 0,IF(P42 > P234, (P42 - P234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,17,"=IF(Q42 > 0,IF(Q42 > Q234, (Q42 - Q234) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(41,19,"=IF(S42 > 0,IF(S42 > S234, (S42 - S234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,20,"=IF(T42 > 0,IF(T42 > T234, (T42 - T234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,21,"=IF(U42 > 0,IF(U42 > U234, (U42 - U234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,22,"=IF(V42 > 0,IF(V42 > V234, (V42 - V234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,23,"=IF(W42 > 0,IF(W42 > W234, (W42 - W234) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(46,3,"=IF(C47 > 0,IF(C47 > C248, (C47 - C248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,4,"=IF(D47 > 0,IF(D47 > D248, (D47 - D248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,5,"=IF(E47 > 0,IF(E47 > E248, (E47 - E248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,6,"=IF(F47 > 0,IF(F47 > F248, (F47 - F248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,7,"=IF(G47 > 0,IF(G47 > G248, (G47 - G248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,8,"=IF(H47 > 0,IF(H47 > H248, (H47 - H248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,9,"=IF(I47 > 0,IF(I47 > I248, (I47 - I248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,10,"=IF(J47 > 0,IF(J47 > J248, (J47 - J248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,11,"=IF(K47 > 0,IF(K47 > K248, (K47 - K248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,12,"=IF(L47 > 0,IF(L47 > L248, (L47 - L248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,13,"=IF(M47 > 0,IF(M47 > M248, (M47 - M248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,14,"=IF(N47 > 0,IF(N47 > N248, (N47 - N248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,15,"=IF(O47 > 0,IF(O47 > O248, (O47 - O248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,16,"=IF(P47 > 0,IF(P47 > P248, (P47 - P248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,17,"=IF(Q47 > 0,IF(Q47 > Q248, (Q47 - Q248) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(46,19,"=IF(S47 > 0,IF(S47 > S248, (S47 - S248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,20,"=IF(T47 > 0,IF(T47 > T248, (T47 - T248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,21,"=IF(U47 > 0,IF(U47 > U248, (U47 - U248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,22,"=IF(V47 > 0,IF(V47 > V248, (V47 - V248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,23,"=IF(W47 > 0,IF(W47 > W248, (W47 - W248) /" +  &
		ls_average_box + ", 0),0)")


//Need to calculate the projected beginning inventory to use in the last
//days calculation.
ole_1.object.EntryRC(52,3,"=C47+C48-C49-C50")
ole_1.object.EntryRC(52,4,"=D47+D48-D49-D50")
ole_1.object.EntryRC(52,5,"=E47+E48-E49-E50")
ole_1.object.EntryRC(52,6,"=F47+F48-F49-F50")
ole_1.object.EntryRC(52,7,"=G47+G48-G49-G50")
ole_1.object.EntryRC(52,8,"=H47+H48-H49-H50")
ole_1.object.EntryRC(52,9,"=I47+I48-I49-I50")
ole_1.object.EntryRC(52,10,"=J47+J48-J49-J50")
ole_1.object.EntryRC(52,11,"=K47+K48-K49-K50")
ole_1.object.EntryRC(52,12,"=L47+L48-L49-L50")
ole_1.object.EntryRC(52,13,"=M47+M48-M49-M50")
ole_1.object.EntryRC(52,14,"=N47+N48-N49-N50")
ole_1.object.EntryRC(52,15,"=O47+O48-O49-O50")
ole_1.object.EntryRC(52,16,"=P47+P48-P49-P50")
ole_1.object.EntryRC(52,17,"=Q47+Q48-Q49-Q50")

ole_1.object.EntryRC(52,19,"=S47+S48-S49-S50")
ole_1.object.EntryRC(52,20,"=T47+T48-T49-T50")
ole_1.object.EntryRC(52,21,"=U47+U48-U49-U50")
ole_1.object.EntryRC(52,22,"=V47+V48-V49-V50")
ole_1.object.EntryRC(52,23,"=W47+W48-W49-W50")


ole_1.object.EntryRC(51,3,"=IF(C52 > 0,IF(C52 > C262, (C52 - C262) /" + &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,4,"=IF(D52 > 0,IF(D52 > D262, (D52 - D262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,5,"=IF(E52 > 0,IF(E52 > E262, (E52 - E262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,6,"=IF(F52 > 0,IF(F52 > F262, (F52 - F262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,7,"=IF(G52 > 0,IF(G52 > G262, (G52 - G262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,8,"=IF(H52 > 0,IF(H52 > H262, (H52 - H262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,9,"=IF(I52 > 0,IF(I52 > I262, (I52 - I262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,10,"=IF(J52 > 0,IF(J52 > J262, (J52 - J262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,11,"=IF(K52 > 0,IF(K52 > K262, (K52 - K262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,12,"=IF(L52 > 0,IF(L52 > L262, (L52 - L262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,13,"=IF(M52 > 0,IF(M52 > M262, (M52 - M262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,14,"=IF(N52 > 0,IF(N52 > N262, (N52 - N262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,15,"=IF(O52 > 0,IF(O52 > O262, (O52 - O262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,16,"=IF(P52 > 0,IF(P52 > P262, (P52 - P262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,17,"=IF(Q52 > 0,IF(Q52 > Q262, (Q52 - Q262) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(51,19,"=IF(S52 > 0,IF(S52 > S262, (S52 - S262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,20,"=IF(T52 > 0,IF(T52 > T262, (T52 - T262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,21,"=IF(U52 > 0,IF(U52 > U262, (U52 - U262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,22,"=IF(V52 > 0,IF(V52 > V262, (V52 - V262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,23,"=IF(W52 > 0,IF(W52 > W262, (W52 - W262) /" +  &
		ls_average_box + ", 0),0)")


Return 0
end function

public function string wf_convert_date (date adt_date);Integer	li_month
String	ls_month, &
			ls_day

li_month = Month(adt_date)
ls_day	= String(Day(adt_date))

Choose Case li_month
	Case 1
		ls_month = 'Jan'
	Case 2
		ls_month = 'Feb'
	Case 3
		ls_month = 'Mar'
	Case 4
		ls_month = 'Apr'
	Case 5
		ls_month = 'May'
	Case 6
		ls_month = 'Jun'
	Case 7
		ls_month = 'Jul'
	Case 8
		ls_month = 'Aug'
	Case 9
		ls_month = 'Sep'
	Case 10
		ls_month = 'Oct'
	Case 11
		ls_month = 'Nov'
	Case 12
		ls_month = 'Dec'
End Choose

Return ls_month + ' ' + ls_day


end function

public subroutine wf_set_reserv_calcs ();String	ls_string

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C162,0)'
ole_1.object.EntryRC(6,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C176,0)'
ole_1.object.EntryRC(13,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C190,0)'
ole_1.object.EntryRC(20,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C204,0)'
ole_1.object.EntryRC(27,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C218,0)'
ole_1.object.EntryRC(34,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C232,0)'
ole_1.object.EntryRC(41,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C246,0)'
ole_1.object.EntryRC(48,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C260,0)'
ole_1.object.EntryRC(55,3,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D162,0)'
ole_1.object.EntryRC(6,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D176,0)'
ole_1.object.EntryRC(13,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D190,0)'
ole_1.object.EntryRC(20,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D204,0)'
ole_1.object.EntryRC(27,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D218,0)'
ole_1.object.EntryRC(34,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D232,0)'
ole_1.object.EntryRC(41,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D246,0)'
ole_1.object.EntryRC(48,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D260,0)'
ole_1.object.EntryRC(55,4,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E162,0)'
ole_1.object.EntryRC(6,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E176,0)'
ole_1.object.EntryRC(13,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E190,0)'
ole_1.object.EntryRC(20,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E204,0)'
ole_1.object.EntryRC(27,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E218,0)'
ole_1.object.EntryRC(34,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E232,0)'
ole_1.object.EntryRC(41,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E246,0)'
ole_1.object.EntryRC(48,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E260,0)'
ole_1.object.EntryRC(55,5,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F162,0)'
ole_1.object.EntryRC(6,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F176,0)'
ole_1.object.EntryRC(13,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F190,0)'
ole_1.object.EntryRC(20,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F204,0)'
ole_1.object.EntryRC(27,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F218,0)'
ole_1.object.EntryRC(34,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F232,0)'
ole_1.object.EntryRC(41,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F246,0)'
ole_1.object.EntryRC(48,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F260,0)'
ole_1.object.EntryRC(55,6,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G162,0)'
ole_1.object.EntryRC(6,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G176,0)'
ole_1.object.EntryRC(13,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G190,0)'
ole_1.object.EntryRC(20,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G204,0)'
ole_1.object.EntryRC(27,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G218,0)'
ole_1.object.EntryRC(34,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G232,0)'
ole_1.object.EntryRC(41,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G246,0)'
ole_1.object.EntryRC(48,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G260,0)'
ole_1.object.EntryRC(55,7,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H162,0)'
ole_1.object.EntryRC(6,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H176,0)'
ole_1.object.EntryRC(13,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H190,0)'
ole_1.object.EntryRC(20,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H204,0)'
ole_1.object.EntryRC(27,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H218,0)'
ole_1.object.EntryRC(34,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H232,0)'
ole_1.object.EntryRC(41,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H246,0)'
ole_1.object.EntryRC(48,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H260,0)'
ole_1.object.EntryRC(55,8,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I162,0)'
ole_1.object.EntryRC(6,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I176,0)'
ole_1.object.EntryRC(13,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I190,0)'
ole_1.object.EntryRC(20,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I204,0)'
ole_1.object.EntryRC(27,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I218,0)'
ole_1.object.EntryRC(34,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I232,0)'
ole_1.object.EntryRC(41,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I246,0)'
ole_1.object.EntryRC(48,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I260,0)'
ole_1.object.EntryRC(55,9,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J162,0)'
ole_1.object.EntryRC(6,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J176,0)'
ole_1.object.EntryRC(13,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J190,0)'
ole_1.object.EntryRC(20,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J204,0)'
ole_1.object.EntryRC(27,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J218,0)'
ole_1.object.EntryRC(34,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J232,0)'
ole_1.object.EntryRC(41,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J246,0)'
ole_1.object.EntryRC(48,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J260,0)'
ole_1.object.EntryRC(55,10,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K162,0)'
ole_1.object.EntryRC(6,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K176,0)'
ole_1.object.EntryRC(13,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K190,0)'
ole_1.object.EntryRC(20,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K204,0)'
ole_1.object.EntryRC(27,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K218,0)'
ole_1.object.EntryRC(34,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K232,0)'
ole_1.object.EntryRC(41,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K246,0)'
ole_1.object.EntryRC(48,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K260,0)'
ole_1.object.EntryRC(55,11,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L162,0)'
ole_1.object.EntryRC(6,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L176,0)'
ole_1.object.EntryRC(13,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L190,0)'
ole_1.object.EntryRC(20,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L204,0)'
ole_1.object.EntryRC(27,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L218,0)'
ole_1.object.EntryRC(34,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L232,0)'
ole_1.object.EntryRC(41,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L246,0)'
ole_1.object.EntryRC(48,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L260,0)'
ole_1.object.EntryRC(55,12,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M162,0)'
ole_1.object.EntryRC(6,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M176,0)'
ole_1.object.EntryRC(13,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M190,0)'
ole_1.object.EntryRC(20,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M204,0)'
ole_1.object.EntryRC(27,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M218,0)'
ole_1.object.EntryRC(34,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M232,0)'
ole_1.object.EntryRC(41,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M246,0)'
ole_1.object.EntryRC(48,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M260,0)'
ole_1.object.EntryRC(55,13,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N162,0)'
ole_1.object.EntryRC(6,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N176,0)'
ole_1.object.EntryRC(13,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N190,0)'
ole_1.object.EntryRC(20,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N204,0)'
ole_1.object.EntryRC(27,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N218,0)'
ole_1.object.EntryRC(34,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N232,0)'
ole_1.object.EntryRC(41,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N246,0)'
ole_1.object.EntryRC(48,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N260,0)'
ole_1.object.EntryRC(55,14,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O162,0)'
ole_1.object.EntryRC(6,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O176,0)'
ole_1.object.EntryRC(13,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O190,0)'
ole_1.object.EntryRC(20,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O204,0)'
ole_1.object.EntryRC(27,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O218,0)'
ole_1.object.EntryRC(34,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O232,0)'
ole_1.object.EntryRC(41,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O246,0)'
ole_1.object.EntryRC(48,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O260,0)'
ole_1.object.EntryRC(55,15,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P162,0)'
ole_1.object.EntryRC(6,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P176,0)'
ole_1.object.EntryRC(13,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P190,0)'
ole_1.object.EntryRC(20,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P204,0)'
ole_1.object.EntryRC(27,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P218,0)'
ole_1.object.EntryRC(34,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P232,0)'
ole_1.object.EntryRC(41,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P246,0)'
ole_1.object.EntryRC(48,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P260,0)'
ole_1.object.EntryRC(55,16,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q162,0)'
ole_1.object.EntryRC(6,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q176,0)'
ole_1.object.EntryRC(13,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q190,0)'
ole_1.object.EntryRC(20,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q204,0)'
ole_1.object.EntryRC(27,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q218,0)'
ole_1.object.EntryRC(34,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q232,0)'
ole_1.object.EntryRC(41,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q246,0)'
ole_1.object.EntryRC(48,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q260,0)'
ole_1.object.EntryRC(55,17,ls_string)


end subroutine

public subroutine wf_set_sales_target_calcs ();ole_1.object.EntryRC(7,3,"=C164")
ole_1.object.EntryRC(14,3,"=C178")
ole_1.object.EntryRC(21,3,"=C192")
ole_1.object.EntryRC(28,3,"=C206")
ole_1.object.EntryRC(35,3,"=C220")
ole_1.object.EntryRC(42,3,"=C234")
ole_1.object.EntryRC(49,3,"=C248")
ole_1.object.EntryRC(56,3,"=C262")

ole_1.object.EntryRC(7,4,"=D164")
ole_1.object.EntryRC(14,4,"=D178")
ole_1.object.EntryRC(21,4,"=D192")
ole_1.object.EntryRC(28,4,"=D206")
ole_1.object.EntryRC(35,4,"=D220")
ole_1.object.EntryRC(42,4,"=D234")
ole_1.object.EntryRC(49,4,"=D248")
ole_1.object.EntryRC(56,4,"=D262")

ole_1.object.EntryRC(7,5,"=E164")
ole_1.object.EntryRC(14,5,"=E178")
ole_1.object.EntryRC(21,5,"=E192")
ole_1.object.EntryRC(28,5,"=E206")
ole_1.object.EntryRC(35,5,"=E220")
ole_1.object.EntryRC(42,5,"=E234")
ole_1.object.EntryRC(49,5,"=E248")
ole_1.object.EntryRC(56,5,"=E262")

ole_1.object.EntryRC(7,6,"=F164")
ole_1.object.EntryRC(14,6,"=F178")
ole_1.object.EntryRC(21,6,"=F192")
ole_1.object.EntryRC(28,6,"=F206")
ole_1.object.EntryRC(35,6,"=F220")
ole_1.object.EntryRC(42,6,"=F234")
ole_1.object.EntryRC(49,6,"=F248")
ole_1.object.EntryRC(56,6,"=F262")

ole_1.object.EntryRC(7,7,"=G164")
ole_1.object.EntryRC(14,7,"=G178")
ole_1.object.EntryRC(21,7,"=G192")
ole_1.object.EntryRC(28,7,"=G206")
ole_1.object.EntryRC(35,7,"=G220")
ole_1.object.EntryRC(42,7,"=G234")
ole_1.object.EntryRC(49,7,"=G248")
ole_1.object.EntryRC(56,7,"=G262")

ole_1.object.EntryRC(7,8,"=H164")
ole_1.object.EntryRC(14,8,"=H178")
ole_1.object.EntryRC(21,8,"=H192")
ole_1.object.EntryRC(28,8,"=H206")
ole_1.object.EntryRC(35,8,"=H220")
ole_1.object.EntryRC(42,8,"=H234")
ole_1.object.EntryRC(49,8,"=H248")
ole_1.object.EntryRC(56,8,"=H262")

ole_1.object.EntryRC(7,9,"=I164")
ole_1.object.EntryRC(14,9,"=I178")
ole_1.object.EntryRC(21,9,"=I192")
ole_1.object.EntryRC(28,9,"=I206")
ole_1.object.EntryRC(35,9,"=I220")
ole_1.object.EntryRC(42,9,"=I234")
ole_1.object.EntryRC(49,9,"=I248")
ole_1.object.EntryRC(56,9,"=I262")

ole_1.object.EntryRC(7,10,"=J164")
ole_1.object.EntryRC(14,10,"=J178")
ole_1.object.EntryRC(21,10,"=J192")
ole_1.object.EntryRC(28,10,"=J206")
ole_1.object.EntryRC(35,10,"=J220")
ole_1.object.EntryRC(42,10,"=J234")
ole_1.object.EntryRC(49,10,"=J248")
ole_1.object.EntryRC(56,10,"=J262")

ole_1.object.EntryRC(7,11,"=K164")
ole_1.object.EntryRC(14,11,"=K178")
ole_1.object.EntryRC(21,11,"=K192")
ole_1.object.EntryRC(28,11,"=K206")
ole_1.object.EntryRC(35,11,"=K220")
ole_1.object.EntryRC(42,11,"=K234")
ole_1.object.EntryRC(49,11,"=K248")
ole_1.object.EntryRC(56,11,"=K262")

ole_1.object.EntryRC(7,12,"=L164")
ole_1.object.EntryRC(14,12,"=L178")
ole_1.object.EntryRC(21,12,"=L192")
ole_1.object.EntryRC(28,12,"=L206")
ole_1.object.EntryRC(35,12,"=L220")
ole_1.object.EntryRC(42,12,"=L234")
ole_1.object.EntryRC(49,12,"=L248")
ole_1.object.EntryRC(56,12,"=L262")

ole_1.object.EntryRC(7,13,"=M164")
ole_1.object.EntryRC(14,13,"=M178")
ole_1.object.EntryRC(21,13,"=M192")
ole_1.object.EntryRC(28,13,"=M206")
ole_1.object.EntryRC(35,13,"=M220")
ole_1.object.EntryRC(42,13,"=M234")
ole_1.object.EntryRC(49,13,"=M248")
ole_1.object.EntryRC(56,13,"=M262")

ole_1.object.EntryRC(7,14,"=N164")
ole_1.object.EntryRC(14,14,"=N178")
ole_1.object.EntryRC(21,14,"=N192")
ole_1.object.EntryRC(28,14,"=N206")
ole_1.object.EntryRC(35,14,"=N220")
ole_1.object.EntryRC(42,14,"=N234")
ole_1.object.EntryRC(49,14,"=N248")
ole_1.object.EntryRC(56,14,"=N262")

ole_1.object.EntryRC(7,15,"=O164")
ole_1.object.EntryRC(14,15,"=O178")
ole_1.object.EntryRC(21,15,"=O192")
ole_1.object.EntryRC(28,15,"=O206")
ole_1.object.EntryRC(35,15,"=O220")
ole_1.object.EntryRC(42,15,"=O234")
ole_1.object.EntryRC(49,15,"=O248")
ole_1.object.EntryRC(56,15,"=O262")

ole_1.object.EntryRC(7,16,"=P164")
ole_1.object.EntryRC(14,16,"=P178")
ole_1.object.EntryRC(21,16,"=P192")
ole_1.object.EntryRC(28,16,"=P206")
ole_1.object.EntryRC(35,16,"=P220")
ole_1.object.EntryRC(42,16,"=P234")
ole_1.object.EntryRC(49,16,"=P248")
ole_1.object.EntryRC(56,16,"=P262")

ole_1.object.EntryRC(7,17,"=Q164")
ole_1.object.EntryRC(14,17,"=Q178")
ole_1.object.EntryRC(21,17,"=Q192")
ole_1.object.EntryRC(28,17,"=Q206")
ole_1.object.EntryRC(35,17,"=Q220")
ole_1.object.EntryRC(42,17,"=Q234")
ole_1.object.EntryRC(49,17,"=Q248")
ole_1.object.EntryRC(56,17,"=Q262")



end subroutine

public subroutine wf_set_invent_target_calcs ();ole_1.object.ENTRYRC(8,3,"=C165")
ole_1.object.ENTRYRC(15,3,"=C179")
ole_1.object.ENTRYRC(22,3,"=C193")
ole_1.object.ENTRYRC(29,3,"=C207")
ole_1.object.ENTRYRC(36,3,"=C221")
ole_1.object.ENTRYRC(43,3,"=C235")
ole_1.object.ENTRYRC(50,3,"=C249")
ole_1.object.ENTRYRC(57,3,"=C263")

ole_1.object.ENTRYRC(8,4,"=D165")
ole_1.object.ENTRYRC(15,4,"=D179")
ole_1.object.ENTRYRC(22,4,"=D193")
ole_1.object.ENTRYRC(29,4,"=D207")
ole_1.object.ENTRYRC(36,4,"=D221")
ole_1.object.ENTRYRC(43,4,"=D235")
ole_1.object.ENTRYRC(50,4,"=D249")
ole_1.object.ENTRYRC(57,4,"=D263")

ole_1.object.ENTRYRC(8,5,"=E165")
ole_1.object.ENTRYRC(15,5,"=E179")
ole_1.object.ENTRYRC(22,5,"=E193")
ole_1.object.ENTRYRC(29,5,"=E207")
ole_1.object.ENTRYRC(36,5,"=E221")
ole_1.object.ENTRYRC(43,5,"=E235")
ole_1.object.ENTRYRC(50,5,"=E249")
ole_1.object.ENTRYRC(57,5,"=E263")

ole_1.object.ENTRYRC(8,6,"=F165")
ole_1.object.ENTRYRC(15,6,"=F179")
ole_1.object.ENTRYRC(22,6,"=F193")
ole_1.object.ENTRYRC(29,6,"=F207")
ole_1.object.ENTRYRC(36,6,"=F221")
ole_1.object.ENTRYRC(43,6,"=F235")
ole_1.object.ENTRYRC(50,6,"=F249")
ole_1.object.ENTRYRC(57,6,"=F263")

ole_1.object.ENTRYRC(8,7,"=G165")
ole_1.object.ENTRYRC(15,7,"=G179")
ole_1.object.ENTRYRC(22,7,"=G193")
ole_1.object.ENTRYRC(29,7,"=G207")
ole_1.object.ENTRYRC(36,7,"=G221")
ole_1.object.ENTRYRC(43,7,"=G235")
ole_1.object.ENTRYRC(50,7,"=G249")
ole_1.object.ENTRYRC(57,7,"=G263")

ole_1.object.ENTRYRC(8,8,"=H165")
ole_1.object.ENTRYRC(15,8,"=H179")
ole_1.object.ENTRYRC(22,8,"=H193")
ole_1.object.ENTRYRC(29,8,"=H207")
ole_1.object.ENTRYRC(36,8,"=H221")
ole_1.object.ENTRYRC(43,8,"=H235")
ole_1.object.ENTRYRC(50,8,"=H249")
ole_1.object.ENTRYRC(57,8,"=H263")

ole_1.object.ENTRYRC(8,9,"=I165")
ole_1.object.ENTRYRC(15,9,"=I179")
ole_1.object.ENTRYRC(22,9,"=I193")
ole_1.object.ENTRYRC(29,9,"=I207")
ole_1.object.ENTRYRC(36,9,"=I221")
ole_1.object.ENTRYRC(43,9,"=I235")
ole_1.object.ENTRYRC(50,9,"=I249")
ole_1.object.ENTRYRC(57,9,"=I263")

ole_1.object.ENTRYRC(8,10,"=J165")
ole_1.object.ENTRYRC(15,10,"=J179")
ole_1.object.ENTRYRC(22,10,"=J193")
ole_1.object.ENTRYRC(29,10,"=J207")
ole_1.object.ENTRYRC(36,10,"=J221")
ole_1.object.ENTRYRC(43,10,"=J235")
ole_1.object.ENTRYRC(50,10,"=J249")
ole_1.object.ENTRYRC(57,10,"=J263")

ole_1.object.ENTRYRC(8,11,"=K165")
ole_1.object.ENTRYRC(15,11,"=K179")
ole_1.object.ENTRYRC(22,11,"=K193")
ole_1.object.ENTRYRC(29,11,"=K207")
ole_1.object.ENTRYRC(36,11,"=K221")
ole_1.object.ENTRYRC(43,11,"=K235")
ole_1.object.ENTRYRC(50,11,"=K249")
ole_1.object.ENTRYRC(57,11,"=K263")

ole_1.object.ENTRYRC(8,12,"=L165")
ole_1.object.ENTRYRC(15,12,"=L179")
ole_1.object.ENTRYRC(22,12,"=L193")
ole_1.object.ENTRYRC(29,12,"=L207")
ole_1.object.ENTRYRC(36,12,"=L221")
ole_1.object.ENTRYRC(43,12,"=L235")
ole_1.object.ENTRYRC(50,12,"=L249")
ole_1.object.ENTRYRC(57,12,"=L263")

ole_1.object.ENTRYRC(8,13,"=M165")
ole_1.object.ENTRYRC(15,13,"=M179")
ole_1.object.ENTRYRC(22,13,"=M193")
ole_1.object.ENTRYRC(29,13,"=M207")
ole_1.object.ENTRYRC(36,13,"=M221")
ole_1.object.ENTRYRC(43,13,"=M235")
ole_1.object.ENTRYRC(50,13,"=M249")
ole_1.object.ENTRYRC(57,13,"=M263")

ole_1.object.ENTRYRC(8,14,"=N165")
ole_1.object.ENTRYRC(15,14,"=N179")
ole_1.object.ENTRYRC(22,14,"=N193")
ole_1.object.ENTRYRC(29,14,"=N207")
ole_1.object.ENTRYRC(36,14,"=N221")
ole_1.object.ENTRYRC(43,14,"=N235")
ole_1.object.ENTRYRC(50,14,"=N249")
ole_1.object.ENTRYRC(57,14,"=N263")

ole_1.object.ENTRYRC(8,15,"=O165")
ole_1.object.ENTRYRC(15,15,"=O179")
ole_1.object.ENTRYRC(22,15,"=O193")
ole_1.object.ENTRYRC(29,15,"=O207")
ole_1.object.ENTRYRC(36,15,"=O221")
ole_1.object.ENTRYRC(43,15,"=O235")
ole_1.object.ENTRYRC(50,15,"=O249")
ole_1.object.ENTRYRC(57,15,"=O263")

ole_1.object.ENTRYRC(8,16,"=P165")
ole_1.object.ENTRYRC(15,16,"=P179")
ole_1.object.ENTRYRC(22,16,"=P193")
ole_1.object.ENTRYRC(29,16,"=P207")
ole_1.object.ENTRYRC(36,16,"=P221")
ole_1.object.ENTRYRC(43,16,"=P235")
ole_1.object.ENTRYRC(50,16,"=P249")
ole_1.object.ENTRYRC(57,16,"=P263")

ole_1.object.ENTRYRC(8,17,"=Q165")
ole_1.object.ENTRYRC(15,17,"=Q179")
ole_1.object.ENTRYRC(22,17,"=Q193")
ole_1.object.ENTRYRC(29,17,"=Q207")
ole_1.object.ENTRYRC(36,17,"=Q221")
ole_1.object.ENTRYRC(43,17,"=Q235")
ole_1.object.ENTRYRC(50,17,"=Q249")
ole_1.object.ENTRYRC(57,17,"=Q263")



end subroutine

public subroutine wf_set_invt_var_calcs ();		
ole_1.object.EntryRC(9,3,"=C3-C8")
ole_1.object.EntryRC(16,3,"=C10-C15")
ole_1.object.EntryRC(23,3,"=C17-C22")
ole_1.object.EntryRC(30,3,"=C24-C29")
ole_1.object.EntryRC(37,3,"=C31-C36")
ole_1.object.EntryRC(44,3,"=C38-C43")
ole_1.object.EntryRC(51,3,"=C45-C50")
ole_1.object.EntryRC(58,3,"=C52-C57")

ole_1.object.EntryRC(9,4,"=D3-D8")
ole_1.object.EntryRC(16,4,"=D10-D15")
ole_1.object.EntryRC(23,4,"=D17-D22")
ole_1.object.EntryRC(30,4,"=D24-D29")
ole_1.object.EntryRC(37,4,"=D31-D36")
ole_1.object.EntryRC(44,4,"=D38-D43")
ole_1.object.EntryRC(51,4,"=D45-D50")
ole_1.object.EntryRC(58,4,"=D52-D57")

ole_1.object.EntryRC(9,5,"=E3-E8")
ole_1.object.EntryRC(16,5,"=E10-E15")
ole_1.object.EntryRC(23,5,"=E17-E22")
ole_1.object.EntryRC(30,5,"=E24-E29")
ole_1.object.EntryRC(37,5,"=E31-E36")
ole_1.object.EntryRC(44,5,"=E38-E43")
ole_1.object.EntryRC(51,5,"=E45-E50")
ole_1.object.EntryRC(58,5,"=E52-E57")

ole_1.object.EntryRC(9,6,"=F3-F8")
ole_1.object.EntryRC(16,6,"=F10-F15")
ole_1.object.EntryRC(23,6,"=F17-F22")
ole_1.object.EntryRC(30,6,"=F24-F29")
ole_1.object.EntryRC(37,6,"=F31-F36")
ole_1.object.EntryRC(44,6,"=F38-F43")
ole_1.object.EntryRC(51,6,"=F45-F50")
ole_1.object.EntryRC(58,6,"=F52-F57")

ole_1.object.EntryRC(9,7,"=G3-G8")
ole_1.object.EntryRC(16,7,"=G10-G15")
ole_1.object.EntryRC(23,7,"=G17-G22")
ole_1.object.EntryRC(30,7,"=G24-G29")
ole_1.object.EntryRC(37,7,"=G31-G36")
ole_1.object.EntryRC(44,7,"=G38-G43")
ole_1.object.EntryRC(51,7,"=G45-G50")
ole_1.object.EntryRC(58,7,"=G52-G57")

ole_1.object.EntryRC(9,8,"=H3-H8")
ole_1.object.EntryRC(16,8,"=H10-H15")
ole_1.object.EntryRC(23,8,"=H17-H22")
ole_1.object.EntryRC(30,8,"=H24-H29")
ole_1.object.EntryRC(37,8,"=H31-H36")
ole_1.object.EntryRC(44,8,"=H38-H43")
ole_1.object.EntryRC(51,8,"=H45-H50")
ole_1.object.EntryRC(58,8,"=H52-H57")

ole_1.object.EntryRC(9,9,"=I3-I8")
ole_1.object.EntryRC(16,9,"=I10-I15")
ole_1.object.EntryRC(23,9,"=I17-I22")
ole_1.object.EntryRC(30,9,"=I24-I29")
ole_1.object.EntryRC(37,9,"=I31-I36")
ole_1.object.EntryRC(44,9,"=I38-I43")
ole_1.object.EntryRC(51,9,"=I45-I50")
ole_1.object.EntryRC(58,9,"=I52-I57")

ole_1.object.EntryRC(9,10,"=J3-J8")
ole_1.object.EntryRC(16,10,"=J10-J15")
ole_1.object.EntryRC(23,10,"=J17-J22")
ole_1.object.EntryRC(30,10,"=J24-J29")
ole_1.object.EntryRC(37,10,"=J31-J36")
ole_1.object.EntryRC(44,10,"=J38-J43")
ole_1.object.EntryRC(51,10,"=J45-J50")
ole_1.object.EntryRC(58,10,"=J52-J57")

ole_1.object.EntryRC(9,11,"=K3-K8")
ole_1.object.EntryRC(16,11,"=K10-K15")
ole_1.object.EntryRC(23,11,"=K17-K22")
ole_1.object.EntryRC(30,11,"=K24-K29")
ole_1.object.EntryRC(37,11,"=K31-K36")
ole_1.object.EntryRC(44,11,"=K38-K43")
ole_1.object.EntryRC(51,11,"=K45-K50")
ole_1.object.EntryRC(58,11,"=K52-K57")

ole_1.object.EntryRC(9,12,"=L3-L8")
ole_1.object.EntryRC(16,12,"=L10-L15")
ole_1.object.EntryRC(23,12,"=L17-L22")
ole_1.object.EntryRC(30,12,"=L24-L29")
ole_1.object.EntryRC(37,12,"=L31-L36")
ole_1.object.EntryRC(44,12,"=L38-L43")
ole_1.object.EntryRC(51,12,"=L45-L50")
ole_1.object.EntryRC(58,12,"=L52-L57")

ole_1.object.EntryRC(9,13,"=M3-M8")
ole_1.object.EntryRC(16,13,"=M10-M15")
ole_1.object.EntryRC(23,13,"=M17-M22")
ole_1.object.EntryRC(30,13,"=M24-M29")
ole_1.object.EntryRC(37,13,"=M31-M36")
ole_1.object.EntryRC(44,13,"=M38-M43")
ole_1.object.EntryRC(51,13,"=M45-M50")
ole_1.object.EntryRC(58,13,"=M52-M57")

ole_1.object.EntryRC(9,14,"=N3-N8")
ole_1.object.EntryRC(16,14,"=N10-N15")
ole_1.object.EntryRC(23,14,"=N17-N22")
ole_1.object.EntryRC(30,14,"=N24-N29")
ole_1.object.EntryRC(37,14,"=N31-N36")
ole_1.object.EntryRC(44,14,"=N38-N43")
ole_1.object.EntryRC(51,14,"=N45-N50")
ole_1.object.EntryRC(58,14,"=N52-N57")

ole_1.object.EntryRC(9,15,"=O3-O8")
ole_1.object.EntryRC(16,15,"=O10-O15")
ole_1.object.EntryRC(23,15,"=O17-O22")
ole_1.object.EntryRC(30,15,"=O24-O29")
ole_1.object.EntryRC(37,15,"=O31-O36")
ole_1.object.EntryRC(44,15,"=O38-O43")
ole_1.object.EntryRC(51,15,"=O45-O50")
ole_1.object.EntryRC(58,15,"=O52-O57")

ole_1.object.EntryRC(9,16,"=P3-P8")
ole_1.object.EntryRC(16,16,"=P10-P15")
ole_1.object.EntryRC(23,16,"=P17-P22")
ole_1.object.EntryRC(30,16,"=P24-P29")
ole_1.object.EntryRC(37,16,"=P31-P36")
ole_1.object.EntryRC(44,16,"=P38-P43")
ole_1.object.EntryRC(51,16,"=P45-P50")
ole_1.object.EntryRC(58,16,"=P52-P57")

ole_1.object.EntryRC(9,17,"=Q3-Q8")
ole_1.object.EntryRC(16,17,"=Q10-Q15")
ole_1.object.EntryRC(23,17,"=Q17-Q22")
ole_1.object.EntryRC(30,17,"=Q24-Q29")
ole_1.object.EntryRC(37,17,"=Q31-Q36")
ole_1.object.EntryRC(44,17,"=Q38-Q43")
ole_1.object.EntryRC(51,17,"=Q45-Q50")
ole_1.object.EntryRC(58,17,"=Q52-Q57")

end subroutine

public subroutine wf_set_sold_from_invt_calcs ();String	ls_string

ole_1.object.EntryRC(6,3,"=C167")
ole_1.object.EntryRC(13,3,"=C181")
ole_1.object.EntryRC(20,3,"=C195")
ole_1.object.EntryRC(27,3,"=C209")
ole_1.object.EntryRC(34,3,"=C223")
ole_1.object.EntryRC(41,3,"=C237")
ole_1.object.EntryRC(48,3,"=C251")
ole_1.object.EntryRC(55,3,"=C265")

ole_1.object.EntryRC(6,4,"=D167")
ole_1.object.EntryRC(13,4,"=D181")
ole_1.object.EntryRC(20,4,"=D195")
ole_1.object.EntryRC(27,4,"=D209")
ole_1.object.EntryRC(34,4,"=D223")
ole_1.object.EntryRC(41,4,"=D237")
ole_1.object.EntryRC(48,4,"=D251")
ole_1.object.EntryRC(55,4,"=D265")

ole_1.object.EntryRC(6,5,"=E167")
ole_1.object.EntryRC(13,5,"=E181")
ole_1.object.EntryRC(20,5,"=E195")
ole_1.object.EntryRC(27,5,"=E209")
ole_1.object.EntryRC(34,5,"=E223")
ole_1.object.EntryRC(41,5,"=E237")
ole_1.object.EntryRC(48,5,"=E251")
ole_1.object.EntryRC(55,5,"=E265")

ole_1.object.EntryRC(6,6,"=F167")
ole_1.object.EntryRC(13,6,"=F181")
ole_1.object.EntryRC(20,6,"=F195")
ole_1.object.EntryRC(27,6,"=F209")
ole_1.object.EntryRC(34,6,"=F223")
ole_1.object.EntryRC(41,6,"=F237")
ole_1.object.EntryRC(48,6,"=F251")
ole_1.object.EntryRC(55,6,"=F265")

ole_1.object.EntryRC(6,7,"=G167")
ole_1.object.EntryRC(13,7,"=G181")
ole_1.object.EntryRC(20,7,"=G195")
ole_1.object.EntryRC(27,7,"=G209")
ole_1.object.EntryRC(34,7,"=G223")
ole_1.object.EntryRC(41,7,"=G237")
ole_1.object.EntryRC(48,7,"=G251")
ole_1.object.EntryRC(55,7,"=G265")

ole_1.object.EntryRC(6,8,"=H167")
ole_1.object.EntryRC(13,8,"=H181")
ole_1.object.EntryRC(20,8,"=H195")
ole_1.object.EntryRC(27,8,"=H209")
ole_1.object.EntryRC(34,8,"=H223")
ole_1.object.EntryRC(41,8,"=H237")
ole_1.object.EntryRC(48,8,"=H251")
ole_1.object.EntryRC(55,8,"=H265")

ole_1.object.EntryRC(6,9,"=I167")
ole_1.object.EntryRC(13,9,"=I181")
ole_1.object.EntryRC(20,9,"=I195")
ole_1.object.EntryRC(27,9,"=I209")
ole_1.object.EntryRC(34,9,"=I223")
ole_1.object.EntryRC(41,9,"=I237")
ole_1.object.EntryRC(48,9,"=I251")
ole_1.object.EntryRC(55,9,"=I265")

ole_1.object.EntryRC(6,10,"=J167")
ole_1.object.EntryRC(13,10,"=J181")
ole_1.object.EntryRC(20,10,"=J195")
ole_1.object.EntryRC(27,10,"=J209")
ole_1.object.EntryRC(34,10,"=J223")
ole_1.object.EntryRC(41,10,"=J237")
ole_1.object.EntryRC(48,10,"=J251")
ole_1.object.EntryRC(55,10,"=J265")

ole_1.object.EntryRC(6,11,"=K167")
ole_1.object.EntryRC(13,11,"=K181")
ole_1.object.EntryRC(20,11,"=K195")
ole_1.object.EntryRC(27,11,"=K209")
ole_1.object.EntryRC(34,11,"=K223")
ole_1.object.EntryRC(41,11,"=K237")
ole_1.object.EntryRC(48,11,"=K251")
ole_1.object.EntryRC(55,11,"=K265")

ole_1.object.EntryRC(6,12,"=L167")
ole_1.object.EntryRC(13,12,"=L181")
ole_1.object.EntryRC(20,12,"=L195")
ole_1.object.EntryRC(27,12,"=L209")
ole_1.object.EntryRC(34,12,"=L223")
ole_1.object.EntryRC(41,12,"=L237")
ole_1.object.EntryRC(48,12,"=L251")
ole_1.object.EntryRC(55,12,"=L265")

ole_1.object.EntryRC(6,13,"=M167")
ole_1.object.EntryRC(13,13,"=M181")
ole_1.object.EntryRC(20,13,"=M195")
ole_1.object.EntryRC(27,13,"=M209")
ole_1.object.EntryRC(34,13,"=M223")
ole_1.object.EntryRC(41,13,"=M237")
ole_1.object.EntryRC(48,13,"=M251")
ole_1.object.EntryRC(55,13,"=M265")

ole_1.object.EntryRC(6,14,"=N167")
ole_1.object.EntryRC(13,14,"=N181")
ole_1.object.EntryRC(20,14,"=N195")
ole_1.object.EntryRC(27,14,"=N209")
ole_1.object.EntryRC(34,14,"=N223")
ole_1.object.EntryRC(41,14,"=N237")
ole_1.object.EntryRC(48,14,"=N251")
ole_1.object.EntryRC(55,14,"=N265")

ole_1.object.EntryRC(6,15,"=O167")
ole_1.object.EntryRC(13,15,"=O181")
ole_1.object.EntryRC(20,15,"=O195")
ole_1.object.EntryRC(27,15,"=O209")
ole_1.object.EntryRC(34,15,"=O223")
ole_1.object.EntryRC(41,15,"=O237")
ole_1.object.EntryRC(48,15,"=O251")
ole_1.object.EntryRC(55,15,"=O265")

ole_1.object.EntryRC(6,16,"=P167")
ole_1.object.EntryRC(13,16,"=P181")
ole_1.object.EntryRC(20,16,"=P195")
ole_1.object.EntryRC(27,16,"=P209")
ole_1.object.EntryRC(34,16,"=P223")
ole_1.object.EntryRC(41,16,"=P237")
ole_1.object.EntryRC(48,16,"=P251")
ole_1.object.EntryRC(55,16,"=P265")

ole_1.object.EntryRC(6,17,"=Q167")
ole_1.object.EntryRC(13,17,"=Q181")
ole_1.object.EntryRC(20,17,"=Q195")
ole_1.object.EntryRC(27,17,"=Q209")
ole_1.object.EntryRC(34,17,"=Q223")
ole_1.object.EntryRC(41,17,"=Q237")
ole_1.object.EntryRC(48,17,"=Q251")
ole_1.object.EntryRC(55,17,"=Q265")
end subroutine

public subroutine wf_set_total_calcs ();		
ole_1.object.EntryRC(3,18,"=C3+D3+E3+F3+G3+H3+I3+J3+K3+L3+M3+N3+O3+P3+Q3")
ole_1.object.EntryRC(4,18,"=C4+D4+E4+F4+G4+H4+I4+J4+K4+L4+M4+N4+O4+P4+Q4")
ole_1.object.EntryRC(5,18,"=C5+D5+E5+F5+G5+H5+I5+J5+K5+L5+M5+N5+O5+P5+Q5")
//ole_1.object.EntryRC(6,18,"=C6+D6+E6+F6+G6+H6+I6+J6+K6+L6+M6+N6+O6+P6+Q6")
ole_1.object.EntryRC(7,18,"=C7+D7+E7+F7+G7+H7+I7+J7+K7+L7+M7+N7+O7+P7+Q7")
ole_1.object.EntryRC(8,18,"=C8+D8+E8+F8+G8+H8+I8+J8+K8+L8+M8+N8+O8+P8+Q8")
ole_1.object.EntryRC(9,18,"=C9+D9+E9+F9+G9+H9+I9+J9+K9+L9+M9+N9+O9+P9+Q9")

ole_1.object.EntryRC(10,18,"=C10+D10+E10+F10+G10+H10+I10+J10+K10+L10+M10+N10+O10+P10+Q10")
ole_1.object.EntryRC(11,18,"=C11+D11+E11+F11+G11+H11+I11+J11+K11+L11+M11+N11+O11+P11+Q11")
ole_1.object.EntryRC(12,18,"=C12+D12+E12+F12+G12+H12+I12+J12+K12+L12+M12+N12+O12+P12+Q12")
//ole_1.object.EntryRC(13,18,"=C13+D13+E13+F13+G13+H13+I13+J13+K13+L13+M13+N13+O13+P13+Q13")
ole_1.object.EntryRC(14,18,"=C14+D14+E14+F14+G14+H14+I14+J14+K14+L14+M14+N14+O14+P14+Q14")
ole_1.object.EntryRC(15,18,"=C15+D15+E15+F15+G15+H15+I15+J15+K15+L15+M15+N15+O15+P15+Q15")
ole_1.object.EntryRC(16,18,"=C16+D16+E16+F16+G16+H16+I16+J16+K16+L16+M16+N16+O16+P16+Q16")

ole_1.object.EntryRC(17,18,"=C17+D17+E17+F17+G17+H17+I17+J17+K17+L17+M17+N17+O17+P17+Q17")
ole_1.object.EntryRC(18,18,"=C18+D18+E18+F18+G18+H18+I18+J18+K18+L18+M18+N18+O18+P18+Q18")
ole_1.object.EntryRC(19,18,"=C19+D19+E19+F19+G19+H19+I19+J19+K19+L19+M19+N19+O19+P19+Q19")
//ole_1.object.EntryRC(20,18,"=C20+D20+E20+F20+G20+H20+I20+J20+K20+L20+M20+N20+O20+P20+Q20")
ole_1.object.EntryRC(21,18,"=C21+D21+E21+F21+G21+H21+I21+J21+K21+L21+M21+N21+O21+P21+Q21")
ole_1.object.EntryRC(22,18,"=C22+D22+E22+F22+G22+H22+I22+J22+K22+L22+M22+N22+O22+P22+Q22")
ole_1.object.EntryRC(23,18,"=C23+D23+E23+F23+G23+H23+I23+J23+K23+L23+M23+N23+O23+P23+Q23")

ole_1.object.EntryRC(24,18,"=C24+D24+E24+F24+G24+H24+I24+J24+K24+L24+M24+N24+O24+P24+Q24")
ole_1.object.EntryRC(25,18,"=C25+D25+E25+F25+G25+H25+I25+J25+K25+L25+M25+N25+O25+P25+Q25")
ole_1.object.EntryRC(26,18,"=C26+D26+E26+F26+G26+H26+I26+J26+K26+L26+M26+N26+O26+P26+Q26")
//ole_1.object.EntryRC(27,18,"=C27+D27+E27+F27+G27+H27+I27+J27+K27+L27+M27+N27+O27+P27+Q27")
ole_1.object.EntryRC(28,18,"=C28+D28+E28+F28+G28+H28+I28+J28+K28+L28+M28+N28+O28+P28+Q28")
ole_1.object.EntryRC(29,18,"=C29+D29+E29+F29+G29+H29+I29+J29+K29+L29+M29+N29+O29+P29+Q29")
ole_1.object.EntryRC(30,18,"=C30+D30+E30+F30+G30+H30+I30+J30+K30+L30+M30+N30+O30+P30+Q30")

ole_1.object.EntryRC(31,18,"=C31+D31+E31+F31+G31+H31+I31+J31+K31+L31+M31+N31+O31+P31+Q31")
ole_1.object.EntryRC(32,18,"=C32+D32+E32+F32+G32+H32+I32+J32+K32+L32+M32+N32+O32+P32+Q32")
ole_1.object.EntryRC(33,18,"=C33+D33+E33+F33+G33+H33+I33+J33+K33+L33+M33+N33+O33+P33+Q33")
//ole_1.object.EntryRC(34,18,"=C34+D34+E34+F34+G34+H34+I34+J34+K34+L34+M34+N34+O34+P34+Q34")
ole_1.object.EntryRC(35,18,"=C35+D35+E35+F35+G35+H35+I35+J35+K35+L35+M35+N35+O35+P35+Q35")
ole_1.object.EntryRC(36,18,"=C36+D36+E36+F36+G36+H36+I36+J36+K36+L36+M36+N36+O36+P36+Q36")
ole_1.object.EntryRC(37,18,"=C37+D37+E37+F37+G37+H37+I37+J37+K37+L37+M37+N37+O37+P37+Q37")

ole_1.object.EntryRC(38,18,"=C38+D38+E38+F38+G38+H38+I38+J38+K38+L38+M38+N38+O38+P38+Q38")
ole_1.object.EntryRC(39,18,"=C39+D39+E39+F39+G39+H39+I39+J39+K39+L39+M39+N39+O39+P39+Q39")
ole_1.object.EntryRC(40,18,"=C40+D40+E40+F40+G40+H40+I40+J40+K40+L40+M40+N40+O40+P40+Q40")
//ole_1.object.EntryRC(41,18,"=C41+D41+E41+F41+G41+H41+I41+J41+K41+L41+M41+N41+O41+P41+Q41")
ole_1.object.EntryRC(42,18,"=C42+D42+E42+F42+G42+H42+I42+J42+K42+L42+M42+N42+O42+P42+Q42")
ole_1.object.EntryRC(43,18,"=C43+D43+E43+F43+G43+H43+I43+J43+K43+L43+M43+N43+O43+P43+Q43")
ole_1.object.EntryRC(44,18,"=C44+D44+E44+F44+G44+H44+I44+J44+K44+L44+M44+N44+O44+P44+Q44")

ole_1.object.EntryRC(45,18,"=C45+D45+E45+F45+G45+H45+I45+J45+K45+L45+M45+N45+O45+P45+Q45")
ole_1.object.EntryRC(46,18,"=C46+D46+E46+F46+G46+H46+I46+J46+K46+L46+M46+N46+O46+P46+Q46")
ole_1.object.EntryRC(47,18,"=C47+D47+E47+F47+G47+H47+I47+J47+K47+L47+M47+N47+O47+P47+Q47")
//ole_1.object.EntryRC(48,18,"=C48+D48+E48+F48+G48+H48+I48+J48+K48+L48+M48+N48+O48+P48+Q48")
ole_1.object.EntryRC(49,18,"=C49+D49+E49+F49+G49+H49+I49+J49+K49+L49+M49+N49+O49+P49+Q49")
ole_1.object.EntryRC(50,18,"=C50+D50+E50+F50+G50+H50+I50+J50+K50+L50+M50+N50+O50+P50+Q50")
ole_1.object.EntryRC(51,18,"=C51+D51+E51+F51+G51+H51+I51+J51+K51+L51+M51+N51+O51+P51+Q51")

ole_1.object.EntryRC(52,18,"=C52+D52+E52+F52+G52+H52+I52+J52+K52+L52+M52+N52+O52+P52+Q52")
ole_1.object.EntryRC(53,18,"=C53+D53+E53+F53+G53+H53+I53+J53+K53+L53+M53+N53+O53+P53+Q53")
ole_1.object.EntryRC(54,18,"=C54+D54+E54+F54+G54+H54+I54+J54+K54+L54+M54+N54+O54+P54+Q54")
//ole_1.object.EntryRC(55,18,"=C55+D55+E55+F55+G55+H55+I55+J55+K55+L55+M55+N55+O55+P55+Q55")
ole_1.object.EntryRC(56,18,"=C56+D56+E56+F56+G56+H56+I56+J56+K56+L56+M56+N56+O56+P56+Q56")
ole_1.object.EntryRC(57,18,"=C57+D57+E57+F57+G57+H57+I57+J57+K57+L57+M57+N57+O57+P57+Q57")
ole_1.object.EntryRC(58,18,"=C58+D58+E58+F58+G58+H58+I58+J58+K58+L58+M58+N58+O58+P58+Q58")



end subroutine

public subroutine wf_set_invt_calcs ();		
if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,3,"=C152")
else
	ole_1.object.EntryRC(3,3,"=C153")
end if
ole_1.object.EntryRC(10,3,"=C3+C4-C5")
ole_1.object.EntryRC(17,3,"=C10+C11-C12")
ole_1.object.EntryRC(24,3,"=C17+C18-C19")
ole_1.object.EntryRC(31,3,"=C24+C25-C26")
ole_1.object.EntryRC(38,3,"=C31+C32-C33")
ole_1.object.EntryRC(45,3,"=C38+C39-C40")
ole_1.object.EntryRC(52,3,"=C45+C46-C47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,4,"=D152")
else
	ole_1.object.EntryRC(3,4,"=D153")
end if
ole_1.object.EntryRC(10,4,"=D3+D4-D5")
ole_1.object.EntryRC(17,4,"=D10+D11-D12")
ole_1.object.EntryRC(24,4,"=D17+D18-D19")
ole_1.object.EntryRC(31,4,"=D24+D25-D26")
ole_1.object.EntryRC(38,4,"=D31+D32-D33")
ole_1.object.EntryRC(45,4,"=D38+D39-D40")
ole_1.object.EntryRC(52,4,"=D45+D46-D47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,5,"=E152")
else
	ole_1.object.EntryRC(3,5,"=E153")
end if
ole_1.object.EntryRC(10,5,"=E3+E4-E5")
ole_1.object.EntryRC(17,5,"=E10+E11-E12")
ole_1.object.EntryRC(24,5,"=E17+E18-E19")
ole_1.object.EntryRC(31,5,"=E24+E25-E26")
ole_1.object.EntryRC(38,5,"=E31+E32-E33")
ole_1.object.EntryRC(45,5,"=E38+E39-E40")
ole_1.object.EntryRC(52,5,"=E45+E46-E47")
				
if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,6,"=F152")
else
	ole_1.object.EntryRC(3,6,"=F153")
end if
ole_1.object.EntryRC(10,6,"=F3+F4-F5")
ole_1.object.EntryRC(17,6,"=F10+F11-F12")
ole_1.object.EntryRC(24,6,"=F17+F18-F19")
ole_1.object.EntryRC(31,6,"=F24+F25-F26")
ole_1.object.EntryRC(38,6,"=F31+F32-F33")
ole_1.object.EntryRC(45,6,"=F38+F39-F40")
ole_1.object.EntryRC(52,6,"=F45+F46-F47")

if rb_quantity.Checked Then			
	ole_1.object.EntryRC(3,7,"=G152")
else
	ole_1.object.EntryRC(3,7,"=G153")
end if
ole_1.object.EntryRC(10,7,"=G3+G4-G5")
ole_1.object.EntryRC(17,7,"=G10+G11-G12")
ole_1.object.EntryRC(24,7,"=G17+G18-G19")
ole_1.object.EntryRC(31,7,"=G24+G25-G26")
ole_1.object.EntryRC(38,7,"=G31+G32-G33")
ole_1.object.EntryRC(45,7,"=G38+G39-G40")
ole_1.object.EntryRC(52,7,"=G45+G46-G47")

if rb_quantity.Checked Then			
	ole_1.object.EntryRC(3,8,"=H152")
else
	ole_1.object.EntryRC(3,8,"=H153")
end if
ole_1.object.EntryRC(10,8,"=H3+H4-H5")
ole_1.object.EntryRC(17,8,"=H10+H11-H12")
ole_1.object.EntryRC(24,8,"=H17+H18-H19")
ole_1.object.EntryRC(31,8,"=H24+H25-H26")
ole_1.object.EntryRC(38,8,"=H31+H32-H33")
ole_1.object.EntryRC(45,8,"=H38+H39-H40")
ole_1.object.EntryRC(52,8,"=H45+H46-H47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,9,"=I152")
else
	ole_1.object.EntryRC(3,9,"=I153")
end if
ole_1.object.EntryRC(10,9,"=I3+I4-I5")
ole_1.object.EntryRC(17,9,"=I10+I11-I12")
ole_1.object.EntryRC(24,9,"=I17+I18-I19")
ole_1.object.EntryRC(31,9,"=I24+I25-I26")
ole_1.object.EntryRC(38,9,"=I31+I32-I33")
ole_1.object.EntryRC(45,9,"=I38+I39-I40")
ole_1.object.EntryRC(52,9,"=I45+I46-I47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,10,"=J152")
else
	ole_1.object.EntryRC(3,10,"=J153")
end if
ole_1.object.EntryRC(10,10,"=J3+J4-J5")
ole_1.object.EntryRC(17,10,"=J10+J11-J12")
ole_1.object.EntryRC(24,10,"=J17+J18-J19")
ole_1.object.EntryRC(31,10,"=J24+J25-J26")
ole_1.object.EntryRC(38,10,"=J31+J32-J33")
ole_1.object.EntryRC(45,10,"=J38+J39-J40")
ole_1.object.EntryRC(52,10,"=J45+J46-J47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,11,"=K152")
else
	ole_1.object.EntryRC(3,11,"=K153")
end if
ole_1.object.EntryRC(10,11,"=K3+K4-K5")
ole_1.object.EntryRC(17,11,"=K10+K11-K12")
ole_1.object.EntryRC(24,11,"=K17+K18-K19")
ole_1.object.EntryRC(31,11,"=K24+K25-K26")
ole_1.object.EntryRC(38,11,"=K31+K32-K33")
ole_1.object.EntryRC(45,11,"=K38+K39-K40")
ole_1.object.EntryRC(52,11,"=K45+K46-K47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,12,"=L152")
else
	ole_1.object.EntryRC(3,12,"=L153")
end if
ole_1.object.EntryRC(10,12,"=L3+L4-L5")
ole_1.object.EntryRC(17,12,"=L10+L11-L12")
ole_1.object.EntryRC(24,12,"=L17+L18-L19")
ole_1.object.EntryRC(31,12,"=L24+L25-L26")
ole_1.object.EntryRC(38,12,"=L31+L32-L33")
ole_1.object.EntryRC(45,12,"=L38+L39-L40")
ole_1.object.EntryRC(52,12,"=L45+L46-L47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,13,"=M152")
else
	ole_1.object.EntryRC(3,13,"=M153")
end if
ole_1.object.EntryRC(10,13,"=M3+M4-M5")
ole_1.object.EntryRC(17,13,"=M10+M11-M12")
ole_1.object.EntryRC(24,13,"=M17+M18-M19")
ole_1.object.EntryRC(31,13,"=M24+M25-M26")
ole_1.object.EntryRC(38,13,"=M31+M32-M33")
ole_1.object.EntryRC(45,13,"=M38+M39-M40")
ole_1.object.EntryRC(52,13,"=M45+M46-M47")

if rb_quantity.Checked Then			
	ole_1.object.EntryRC(3,14,"=N152")
else
	ole_1.object.EntryRC(3,14,"=N153")
end if
ole_1.object.EntryRC(10,14,"=N3+N4-N5")
ole_1.object.EntryRC(17,14,"=N10+N11-N12")
ole_1.object.EntryRC(24,14,"=N17+N18-N19")
ole_1.object.EntryRC(31,14,"=N24+N25-N26")
ole_1.object.EntryRC(38,14,"=N31+N32-N33")
ole_1.object.EntryRC(45,14,"=N38+N39-N40")
ole_1.object.EntryRC(52,14,"=N45+N46-N47")

if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,15,"=O152")
else
	ole_1.object.EntryRC(3,15,"=O153")
end if
ole_1.object.EntryRC(10,15,"=O3+O4-O5")
ole_1.object.EntryRC(17,15,"=O10+O11-O12")
ole_1.object.EntryRC(24,15,"=O17+O18-O19")
ole_1.object.EntryRC(31,15,"=O24+O25-O26")
ole_1.object.EntryRC(38,15,"=O31+O32-O33")
ole_1.object.EntryRC(45,15,"=O38+O39-O40")
ole_1.object.EntryRC(52,15,"=O45+O46-O47")

if rb_quantity.Checked Then			
	ole_1.object.EntryRC(3,16,"=P152")
else
	ole_1.object.EntryRC(3,16,"=P153")
end if
ole_1.object.EntryRC(10,16,"=P3+P4-P5")
ole_1.object.EntryRC(17,16,"=P10+P11-P12")
ole_1.object.EntryRC(24,16,"=P17+P18-P19")
ole_1.object.EntryRC(31,16,"=P24+P25-P26")
ole_1.object.EntryRC(38,16,"=P31+P32-P33")
ole_1.object.EntryRC(45,16,"=P38+P39-P40")
ole_1.object.EntryRC(52,16,"=P45+P46-P47")
	
if rb_quantity.Checked Then		
	ole_1.object.EntryRC(3,17,"=Q152")
else
	ole_1.object.EntryRC(3,17,"=Q153")
end if	
ole_1.object.EntryRC(3,17,"=Q152")
ole_1.object.EntryRC(10,17,"=Q3+Q4-Q5")
ole_1.object.EntryRC(17,17,"=Q10+Q11-Q12")
ole_1.object.EntryRC(24,17,"=Q17+Q18-Q19")
ole_1.object.EntryRC(31,17,"=Q24+Q25-Q26")
ole_1.object.EntryRC(38,17,"=Q31+Q32-Q33")
ole_1.object.EntryRC(45,17,"=Q38+Q39-Q40")
ole_1.object.EntryRC(52,17,"=Q45+Q46-Q47")

	

end subroutine

public subroutine wf_set_production_calcs ();String	ls_uom_code

ls_uom_code = dw_uom_code.uf_get_uom_code()
		
if rb_quantity.Checked Then	

	ole_1.object.EntryRC(4,3,"=C154")
	ole_1.object.EntryRC(11,3,"=C168")
	ole_1.object.EntryRC(18,3,"=C182")
	ole_1.object.EntryRC(25,3,"=C196")
	ole_1.object.EntryRC(32,3,"=C210")
	ole_1.object.EntryRC(39,3,"=C224")
	ole_1.object.EntryRC(46,3,"=C238")
	ole_1.object.EntryRC(53,3,"=C252")
	
	ole_1.object.EntryRC(4,4,"=D154")
	ole_1.object.EntryRC(11,4,"=D168")
	ole_1.object.EntryRC(18,4,"=D182")
	ole_1.object.EntryRC(25,4,"=D196")
	ole_1.object.EntryRC(32,4,"=D210")
	ole_1.object.EntryRC(39,4,"=D224")
	ole_1.object.EntryRC(46,4,"=D238")
	ole_1.object.EntryRC(53,4,"=D252")
	
	ole_1.object.EntryRC(4,5,"=E154")
	ole_1.object.EntryRC(11,5,"=E168")
	ole_1.object.EntryRC(18,5,"=E182")
	ole_1.object.EntryRC(25,5,"=E196")
	ole_1.object.EntryRC(32,5,"=E210")
	ole_1.object.EntryRC(39,5,"=E224")
	ole_1.object.EntryRC(46,5,"=E238")
	ole_1.object.EntryRC(53,5,"=E252")
	
	ole_1.object.EntryRC(4,6,"=F154")
	ole_1.object.EntryRC(11,6,"=F168")
	ole_1.object.EntryRC(18,6,"=F182")
	ole_1.object.EntryRC(25,6,"=F196")
	ole_1.object.EntryRC(32,6,"=F210")
	ole_1.object.EntryRC(39,6,"=F224")
	ole_1.object.EntryRC(46,6,"=F238")
	ole_1.object.EntryRC(53,6,"=F252")
	
	ole_1.object.EntryRC(4,7,"=G154")
	ole_1.object.EntryRC(11,7,"=G168")
	ole_1.object.EntryRC(18,7,"=G182")
	ole_1.object.EntryRC(25,7,"=G196")
	ole_1.object.EntryRC(32,7,"=G210")
	ole_1.object.EntryRC(39,7,"=G224")
	ole_1.object.EntryRC(46,7,"=G238")
	ole_1.object.EntryRC(53,7,"=G252")
	
	ole_1.object.EntryRC(4,8,"=H154")
	ole_1.object.EntryRC(11,8,"=H168")
	ole_1.object.EntryRC(18,8,"=H182")
	ole_1.object.EntryRC(25,8,"=H196")
	ole_1.object.EntryRC(32,8,"=H210")
	ole_1.object.EntryRC(39,8,"=H224")
	ole_1.object.EntryRC(46,8,"=H238")
	ole_1.object.EntryRC(53,8,"=H252")
	
	ole_1.object.EntryRC(4,9,"=I154")
	ole_1.object.EntryRC(11,9,"=I168")
	ole_1.object.EntryRC(18,9,"=I182")
	ole_1.object.EntryRC(25,9,"=I196")
	ole_1.object.EntryRC(32,9,"=I210")
	ole_1.object.EntryRC(39,9,"=I224")
	ole_1.object.EntryRC(46,9,"=I238")
	ole_1.object.EntryRC(53,9,"=I252")
	
	ole_1.object.EntryRC(4,10,"=J154")
	ole_1.object.EntryRC(11,10,"=J168")
	ole_1.object.EntryRC(18,10,"=J182")
	ole_1.object.EntryRC(25,10,"=J196")
	ole_1.object.EntryRC(32,10,"=J210")
	ole_1.object.EntryRC(39,10,"=J224")
	ole_1.object.EntryRC(46,10,"=J238")
	ole_1.object.EntryRC(53,10,"=J252")
	
	ole_1.object.EntryRC(4,11,"=K154")
	ole_1.object.EntryRC(11,11,"=K168")
	ole_1.object.EntryRC(18,11,"=K182")
	ole_1.object.EntryRC(25,11,"=K196")
	ole_1.object.EntryRC(32,11,"=K210")
	ole_1.object.EntryRC(39,11,"=K224")
	ole_1.object.EntryRC(46,11,"=K238")
	ole_1.object.EntryRC(53,11,"=K252")
	
	ole_1.object.EntryRC(4,12,"=L154")
	ole_1.object.EntryRC(11,12,"=L168")
	ole_1.object.EntryRC(18,12,"=L182")
	ole_1.object.EntryRC(25,12,"=L196")
	ole_1.object.EntryRC(32,12,"=L210")
	ole_1.object.EntryRC(39,12,"=L224")
	ole_1.object.EntryRC(46,12,"=L238")
	ole_1.object.EntryRC(53,12,"=L252")
	
	ole_1.object.EntryRC(4,13,"=M154")
	ole_1.object.EntryRC(11,13,"=M168")
	ole_1.object.EntryRC(18,13,"=M182")
	ole_1.object.EntryRC(25,13,"=M196")
	ole_1.object.EntryRC(32,13,"=M210")
	ole_1.object.EntryRC(39,13,"=M224")
	ole_1.object.EntryRC(46,13,"=M238")
	ole_1.object.EntryRC(53,13,"=M252")
	
	ole_1.object.EntryRC(4,14,"=N154")
	ole_1.object.EntryRC(11,14,"=N168")
	ole_1.object.EntryRC(18,14,"=N182")
	ole_1.object.EntryRC(25,14,"=N196")
	ole_1.object.EntryRC(32,14,"=N210")
	ole_1.object.EntryRC(39,14,"=N224")
	ole_1.object.EntryRC(46,14,"=N238")
	ole_1.object.EntryRC(53,14,"=N252")
	
	ole_1.object.EntryRC(4,15,"=O154")
	ole_1.object.EntryRC(11,15,"=O168")
	ole_1.object.EntryRC(18,15,"=O182")
	ole_1.object.EntryRC(25,15,"=O196")
	ole_1.object.EntryRC(32,15,"=O210")
	ole_1.object.EntryRC(39,15,"=O224")
	ole_1.object.EntryRC(46,15,"=O238")
	ole_1.object.EntryRC(53,15,"=O252")
	
	ole_1.object.EntryRC(4,16,"=P154")
	ole_1.object.EntryRC(11,16,"=P168")
	ole_1.object.EntryRC(18,16,"=P182")
	ole_1.object.EntryRC(25,16,"=P196")
	ole_1.object.EntryRC(32,16,"=P210")
	ole_1.object.EntryRC(39,16,"=P224")
	ole_1.object.EntryRC(46,16,"=P238")
	ole_1.object.EntryRC(53,16,"=P252")
	
	ole_1.object.EntryRC(4,17,"=Q154")
	ole_1.object.EntryRC(11,17,"=Q168")
	ole_1.object.EntryRC(18,17,"=Q182")
	ole_1.object.EntryRC(25,17,"=Q196")
	ole_1.object.EntryRC(32,17,"=Q210")
	ole_1.object.EntryRC(39,17,"=Q224")
	ole_1.object.EntryRC(46,17,"=Q238")
	ole_1.object.EntryRC(53,17,"=Q252")

else
	
	ole_1.object.EntryRC(4,3,"=C155")
	ole_1.object.EntryRC(11,3,"=C169")
	ole_1.object.EntryRC(18,3,"=C183")
	ole_1.object.EntryRC(25,3,"=C197")
	ole_1.object.EntryRC(32,3,"=C211")
	ole_1.object.EntryRC(39,3,"=C225")
	ole_1.object.EntryRC(46,3,"=C239")
	ole_1.object.EntryRC(53,3,"=C253")
	
	ole_1.object.EntryRC(4,4,"=D155")
	ole_1.object.EntryRC(11,4,"=D169")
	ole_1.object.EntryRC(18,4,"=D183")
	ole_1.object.EntryRC(25,4,"=D197")
	ole_1.object.EntryRC(32,4,"=D211")
	ole_1.object.EntryRC(39,4,"=D225")
	ole_1.object.EntryRC(46,4,"=D239")
	ole_1.object.EntryRC(53,4,"=D253")
	
	ole_1.object.EntryRC(4,5,"=E155")
	ole_1.object.EntryRC(11,5,"=E169")
	ole_1.object.EntryRC(18,5,"=E183")
	ole_1.object.EntryRC(25,5,"=E197")
	ole_1.object.EntryRC(32,5,"=E211")
	ole_1.object.EntryRC(39,5,"=E225")
	ole_1.object.EntryRC(46,5,"=E239")
	ole_1.object.EntryRC(53,5,"=E253")
	
	ole_1.object.EntryRC(4,6,"=F155")
	ole_1.object.EntryRC(11,6,"=F169")
	ole_1.object.EntryRC(18,6,"=F183")
	ole_1.object.EntryRC(25,6,"=F197")
	ole_1.object.EntryRC(32,6,"=F211")
	ole_1.object.EntryRC(39,6,"=F225")
	ole_1.object.EntryRC(46,6,"=F239")
	ole_1.object.EntryRC(53,6,"=F253")
	
	ole_1.object.EntryRC(4,7,"=G155")
	ole_1.object.EntryRC(11,7,"=G169")
	ole_1.object.EntryRC(18,7,"=G183")
	ole_1.object.EntryRC(25,7,"=G197")
	ole_1.object.EntryRC(32,7,"=G211")
	ole_1.object.EntryRC(39,7,"=G225")
	ole_1.object.EntryRC(46,7,"=G239")
	ole_1.object.EntryRC(53,7,"=G253")
	
	ole_1.object.EntryRC(4,8,"=H155")
	ole_1.object.EntryRC(11,8,"=H169")
	ole_1.object.EntryRC(18,8,"=H183")
	ole_1.object.EntryRC(25,8,"=H197")
	ole_1.object.EntryRC(32,8,"=H211")
	ole_1.object.EntryRC(39,8,"=H225")
	ole_1.object.EntryRC(46,8,"=H239")
	ole_1.object.EntryRC(53,8,"=H253")
	
	ole_1.object.EntryRC(4,9,"=I155")
	ole_1.object.EntryRC(11,9,"=I169")
	ole_1.object.EntryRC(18,9,"=I183")
	ole_1.object.EntryRC(25,9,"=I197")
	ole_1.object.EntryRC(32,9,"=I211")
	ole_1.object.EntryRC(39,9,"=I225")
	ole_1.object.EntryRC(46,9,"=I239")
	ole_1.object.EntryRC(53,9,"=I253")
	
	ole_1.object.EntryRC(4,10,"=J155")
	ole_1.object.EntryRC(11,10,"=J169")
	ole_1.object.EntryRC(18,10,"=J183")
	ole_1.object.EntryRC(25,10,"=J197")
	ole_1.object.EntryRC(32,10,"=J211")
	ole_1.object.EntryRC(39,10,"=J225")
	ole_1.object.EntryRC(46,10,"=J239")
	ole_1.object.EntryRC(53,10,"=J253")
	
	ole_1.object.EntryRC(4,11,"=K155")
	ole_1.object.EntryRC(11,11,"=K169")
	ole_1.object.EntryRC(18,11,"=K183")
	ole_1.object.EntryRC(25,11,"=K197")
	ole_1.object.EntryRC(32,11,"=K211")
	ole_1.object.EntryRC(39,11,"=K225")
	ole_1.object.EntryRC(46,11,"=K239")
	ole_1.object.EntryRC(53,11,"=K253")
	
	ole_1.object.EntryRC(4,12,"=L155")
	ole_1.object.EntryRC(11,12,"=L169")
	ole_1.object.EntryRC(18,12,"=L183")
	ole_1.object.EntryRC(25,12,"=L197")
	ole_1.object.EntryRC(32,12,"=L211")
	ole_1.object.EntryRC(39,12,"=L225")
	ole_1.object.EntryRC(46,12,"=L239")
	ole_1.object.EntryRC(53,12,"=L253")
	
	ole_1.object.EntryRC(4,13,"=M155")
	ole_1.object.EntryRC(11,13,"=M169")
	ole_1.object.EntryRC(18,13,"=M183")
	ole_1.object.EntryRC(25,13,"=M197")
	ole_1.object.EntryRC(32,13,"=M211")
	ole_1.object.EntryRC(39,13,"=M225")
	ole_1.object.EntryRC(46,13,"=M239")
	ole_1.object.EntryRC(53,13,"=M253")
	
	ole_1.object.EntryRC(4,14,"=N155")
	ole_1.object.EntryRC(11,14,"=N169")
	ole_1.object.EntryRC(18,14,"=N183")
	ole_1.object.EntryRC(25,14,"=N197")
	ole_1.object.EntryRC(32,14,"=N211")
	ole_1.object.EntryRC(39,14,"=N225")
	ole_1.object.EntryRC(46,14,"=N239")
	ole_1.object.EntryRC(53,14,"=N253")
	
	ole_1.object.EntryRC(4,15,"=O155")
	ole_1.object.EntryRC(11,15,"=O169")
	ole_1.object.EntryRC(18,15,"=O183")
	ole_1.object.EntryRC(25,15,"=O197")
	ole_1.object.EntryRC(32,15,"=O211")
	ole_1.object.EntryRC(39,15,"=O225")
	ole_1.object.EntryRC(46,15,"=O239")
	ole_1.object.EntryRC(53,15,"=O253")
	
	ole_1.object.EntryRC(4,16,"=P155")
	ole_1.object.EntryRC(11,16,"=P169")
	ole_1.object.EntryRC(18,16,"=P183")
	ole_1.object.EntryRC(25,16,"=P197")
	ole_1.object.EntryRC(32,16,"=P211")
	ole_1.object.EntryRC(39,16,"=P225")
	ole_1.object.EntryRC(46,16,"=P239")
	ole_1.object.EntryRC(53,16,"=P253")
	
	ole_1.object.EntryRC(4,17,"=Q155")
	ole_1.object.EntryRC(11,17,"=Q169")
	ole_1.object.EntryRC(18,17,"=Q183")
	ole_1.object.EntryRC(25,17,"=Q197")
	ole_1.object.EntryRC(32,17,"=Q211")
	ole_1.object.EntryRC(39,17,"=Q225")
	ole_1.object.EntryRC(46,17,"=Q239")
	ole_1.object.EntryRC(53,17,"=Q253")	
	
end if
end subroutine

public subroutine wf_set_sales_calcs1 ();String	ls_string
		
if rb_quantity.Checked Then	

	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C162,0)'
	ole_1.object.EntryRC(5,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C176,0)'
	ole_1.object.EntryRC(12,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C190,0)'
	ole_1.object.EntryRC(19,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C204,0)'
	ole_1.object.EntryRC(26,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C218,0)'
	ole_1.object.EntryRC(33,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C232,0)'
	ole_1.object.EntryRC(40,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C246,0)'
	ole_1.object.EntryRC(47,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C260,0)'
	ole_1.object.EntryRC(54,3,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D162,0)'
	ole_1.object.EntryRC(5,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D176,0)'
	ole_1.object.EntryRC(12,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D190,0)'
	ole_1.object.EntryRC(19,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D204,0)'
	ole_1.object.EntryRC(26,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D218,0)'
	ole_1.object.EntryRC(33,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D232,0)'
	ole_1.object.EntryRC(40,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D246,0)'
	ole_1.object.EntryRC(47,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D260,0)'
	ole_1.object.EntryRC(54,4,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E162,0)'
	ole_1.object.EntryRC(5,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E176,0)'
	ole_1.object.EntryRC(12,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E190,0)'
	ole_1.object.EntryRC(19,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E204,0)'
	ole_1.object.EntryRC(26,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E218,0)'
	ole_1.object.EntryRC(33,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E232,0)'
	ole_1.object.EntryRC(40,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E246,0)'
	ole_1.object.EntryRC(47,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E260,0)'
	ole_1.object.EntryRC(54,5,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F162,0)'
	ole_1.object.EntryRC(5,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F176,0)'
	ole_1.object.EntryRC(12,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F190,0)'
	ole_1.object.EntryRC(19,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F204,0)'
	ole_1.object.EntryRC(26,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F218,0)'
	ole_1.object.EntryRC(33,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F232,0)'
	ole_1.object.EntryRC(40,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F246,0)'
	ole_1.object.EntryRC(47,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F260,0)'
	ole_1.object.EntryRC(54,6,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G162,0)'
	ole_1.object.EntryRC(5,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G176,0)'
	ole_1.object.EntryRC(12,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G190,0)'
	ole_1.object.EntryRC(19,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G204,0)'
	ole_1.object.EntryRC(26,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G218,0)'
	ole_1.object.EntryRC(33,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G232,0)'
	ole_1.object.EntryRC(40,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G246,0)'
	ole_1.object.EntryRC(47,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G260,0)'
	ole_1.object.EntryRC(54,7,ls_string)
	
else
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C163,0)'
	ole_1.object.EntryRC(5,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C177,0)'
	ole_1.object.EntryRC(12,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C191,0)'
	ole_1.object.EntryRC(19,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C205,0)'
	ole_1.object.EntryRC(26,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C219,0)'
	ole_1.object.EntryRC(33,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C233,0)'
	ole_1.object.EntryRC(40,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C247,0)'
	ole_1.object.EntryRC(47,3,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C261,0)'
	ole_1.object.EntryRC(54,3,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D163,0)'
	ole_1.object.EntryRC(5,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D177,0)'
	ole_1.object.EntryRC(12,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D191,0)'
	ole_1.object.EntryRC(19,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D205,0)'
	ole_1.object.EntryRC(26,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D219,0)'
	ole_1.object.EntryRC(33,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D233,0)'
	ole_1.object.EntryRC(40,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D247,0)'
	ole_1.object.EntryRC(47,4,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D261,0)'
	ole_1.object.EntryRC(54,4,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E163,0)'
	ole_1.object.EntryRC(5,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E177,0)'
	ole_1.object.EntryRC(12,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E191,0)'
	ole_1.object.EntryRC(19,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E205,0)'
	ole_1.object.EntryRC(26,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E219,0)'
	ole_1.object.EntryRC(33,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E233,0)'
	ole_1.object.EntryRC(40,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E247,0)'
	ole_1.object.EntryRC(47,5,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E261,0)'
	ole_1.object.EntryRC(54,5,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F163,0)'
	ole_1.object.EntryRC(5,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F177,0)'
	ole_1.object.EntryRC(12,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F191,0)'
	ole_1.object.EntryRC(19,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F205,0)'
	ole_1.object.EntryRC(26,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F219,0)'
	ole_1.object.EntryRC(33,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F233,0)'
	ole_1.object.EntryRC(40,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F247,0)'
	ole_1.object.EntryRC(47,6,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F261,0)'
	ole_1.object.EntryRC(54,6,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G163,0)'
	ole_1.object.EntryRC(5,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G177,0)'
	ole_1.object.EntryRC(12,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G191,0)'
	ole_1.object.EntryRC(19,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G205,0)'
	ole_1.object.EntryRC(26,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G219,0)'
	ole_1.object.EntryRC(33,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G233,0)'
	ole_1.object.EntryRC(40,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G247,0)'
	ole_1.object.EntryRC(47,7,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G261,0)'
	ole_1.object.EntryRC(54,7,ls_string)	
	
end if

end subroutine

public subroutine wf_set_sales_calcs2 ();
String	ls_string
		
if rb_quantity.Checked Then	

	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H162,0)'
	ole_1.object.EntryRC(5,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H176,0)'
	ole_1.object.EntryRC(12,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H190,0)'
	ole_1.object.EntryRC(19,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H204,0)'
	ole_1.object.EntryRC(26,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H218,0)'
	ole_1.object.EntryRC(33,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H232,0)'
	ole_1.object.EntryRC(40,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H246,0)'
	ole_1.object.EntryRC(47,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H260,0)'
	ole_1.object.EntryRC(54,8,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I162,0)'
	ole_1.object.EntryRC(5,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I176,0)'
	ole_1.object.EntryRC(12,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I190,0)'
	ole_1.object.EntryRC(19,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I204,0)'
	ole_1.object.EntryRC(26,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I218,0)'
	ole_1.object.EntryRC(33,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I232,0)'
	ole_1.object.EntryRC(40,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I246,0)'
	ole_1.object.EntryRC(47,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I260,0)'
	ole_1.object.EntryRC(54,9,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J162,0)'
	ole_1.object.EntryRC(5,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J176,0)'
	ole_1.object.EntryRC(12,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J190,0)'
	ole_1.object.EntryRC(19,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J204,0)'
	ole_1.object.EntryRC(26,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J218,0)'
	ole_1.object.EntryRC(33,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J232,0)'
	ole_1.object.EntryRC(40,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J246,0)'
	ole_1.object.EntryRC(47,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J260,0)'
	ole_1.object.EntryRC(54,10,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K162,0)'
	ole_1.object.EntryRC(5,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K176,0)'
	ole_1.object.EntryRC(12,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K190,0)'
	ole_1.object.EntryRC(19,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K204,0)'
	ole_1.object.EntryRC(26,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K218,0)'
	ole_1.object.EntryRC(33,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K232,0)'
	ole_1.object.EntryRC(40,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K246,0)'
	ole_1.object.EntryRC(47,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K260,0)'
	ole_1.object.EntryRC(54,11,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L162,0)'
	ole_1.object.EntryRC(5,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L176,0)'
	ole_1.object.EntryRC(12,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L190,0)'
	ole_1.object.EntryRC(19,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L204,0)'
	ole_1.object.EntryRC(26,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L218,0)'
	ole_1.object.EntryRC(33,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L232,0)'
	ole_1.object.EntryRC(40,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L246,0)'
	ole_1.object.EntryRC(47,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L260,0)'
	ole_1.object.EntryRC(54,12,ls_string)
	
else

	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H163,0)'
	ole_1.object.EntryRC(5,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H177,0)'
	ole_1.object.EntryRC(12,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H191,0)'
	ole_1.object.EntryRC(19,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H205,0)'
	ole_1.object.EntryRC(26,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H219,0)'
	ole_1.object.EntryRC(33,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H233,0)'
	ole_1.object.EntryRC(40,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H247,0)'
	ole_1.object.EntryRC(47,8,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H261,0)'
	ole_1.object.EntryRC(54,8,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I163,0)'
	ole_1.object.EntryRC(5,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I177,0)'
	ole_1.object.EntryRC(12,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I191,0)'
	ole_1.object.EntryRC(19,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I205,0)'
	ole_1.object.EntryRC(26,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I219,0)'
	ole_1.object.EntryRC(33,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I233,0)'
	ole_1.object.EntryRC(40,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I247,0)'
	ole_1.object.EntryRC(47,9,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I261,0)'
	ole_1.object.EntryRC(54,9,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J163,0)'
	ole_1.object.EntryRC(5,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J177,0)'
	ole_1.object.EntryRC(12,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J191,0)'
	ole_1.object.EntryRC(19,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J205,0)'
	ole_1.object.EntryRC(26,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J219,0)'
	ole_1.object.EntryRC(33,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J233,0)'
	ole_1.object.EntryRC(40,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J247,0)'
	ole_1.object.EntryRC(47,10,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J261,0)'
	ole_1.object.EntryRC(54,10,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K163,0)'
	ole_1.object.EntryRC(5,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K177,0)'
	ole_1.object.EntryRC(12,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K191,0)'
	ole_1.object.EntryRC(19,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K205,0)'
	ole_1.object.EntryRC(26,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K219,0)'
	ole_1.object.EntryRC(33,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K233,0)'
	ole_1.object.EntryRC(40,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K247,0)'
	ole_1.object.EntryRC(47,11,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K261,0)'
	ole_1.object.EntryRC(54,11,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L163,0)'
	ole_1.object.EntryRC(5,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L177,0)'
	ole_1.object.EntryRC(12,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L191,0)'
	ole_1.object.EntryRC(19,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L205,0)'
	ole_1.object.EntryRC(26,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L219,0)'
	ole_1.object.EntryRC(33,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L233,0)'
	ole_1.object.EntryRC(40,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L247,0)'
	ole_1.object.EntryRC(47,12,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L255,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L261,0)'
	ole_1.object.EntryRC(54,12,ls_string)
	
end if	


end subroutine

public subroutine wf_set_sales_calcs3 ();
String	ls_string
		
if rb_quantity.Checked Then	

	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M162,0)'
	ole_1.object.EntryRC(5,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M176,0)'
	ole_1.object.EntryRC(12,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M190,0)'
	ole_1.object.EntryRC(19,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M204,0)'
	ole_1.object.EntryRC(26,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M218,0)'
	ole_1.object.EntryRC(33,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M232,0)'
	ole_1.object.EntryRC(40,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M246,0)'
	ole_1.object.EntryRC(47,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M260,0)'
	ole_1.object.EntryRC(54,13,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N162,0)'
	ole_1.object.EntryRC(5,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N176,0)'
	ole_1.object.EntryRC(12,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N190,0)'
	ole_1.object.EntryRC(19,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N204,0)'
	ole_1.object.EntryRC(26,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N218,0)'
	ole_1.object.EntryRC(33,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N232,0)'
	ole_1.object.EntryRC(40,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N246,0)'
	ole_1.object.EntryRC(47,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N260,0)'
	ole_1.object.EntryRC(54,14,ls_string)
	
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O162,0)'
	ole_1.object.EntryRC(5,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O176,0)'
	ole_1.object.EntryRC(12,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O190,0)'
	ole_1.object.EntryRC(19,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O204,0)'
	ole_1.object.EntryRC(26,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O218,0)'
	ole_1.object.EntryRC(33,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O232,0)'
	ole_1.object.EntryRC(40,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O246,0)'
	ole_1.object.EntryRC(47,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O260,0)'
	ole_1.object.EntryRC(54,15,ls_string)
	
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P162,0)'
	ole_1.object.EntryRC(5,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P176,0)'
	ole_1.object.EntryRC(12,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P190,0)'
	ole_1.object.EntryRC(19,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P204,0)'
	ole_1.object.EntryRC(26,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P218,0)'
	ole_1.object.EntryRC(33,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P232,0)'
	ole_1.object.EntryRC(40,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P246,0)'
	ole_1.object.EntryRC(47,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P260,0)'
	ole_1.object.EntryRC(54,16,ls_string)
	
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q160,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q162,0)'
	ole_1.object.EntryRC(5,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q174,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q176,0)'
	ole_1.object.EntryRC(12,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q188,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q190,0)'
	ole_1.object.EntryRC(19,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q202,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q204,0)'
	ole_1.object.EntryRC(26,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q216,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q218,0)'
	ole_1.object.EntryRC(33,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q230,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q232,0)'
	ole_1.object.EntryRC(40,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q244,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q246,0)'
	ole_1.object.EntryRC(47,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q258,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q260,0)'
	ole_1.object.EntryRC(54,17,ls_string)

else
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M163,0)'
	ole_1.object.EntryRC(5,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M177,0)'
	ole_1.object.EntryRC(12,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M191,0)'
	ole_1.object.EntryRC(19,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M205,0)'
	ole_1.object.EntryRC(26,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M219,0)'
	ole_1.object.EntryRC(33,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M233,0)'
	ole_1.object.EntryRC(40,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M247,0)'
	ole_1.object.EntryRC(47,13,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M261,0)'
	ole_1.object.EntryRC(54,13,ls_string)
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N163,0)'
	ole_1.object.EntryRC(5,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N177,0)'
	ole_1.object.EntryRC(12,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N191,0)'
	ole_1.object.EntryRC(19,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N205,0)'
	ole_1.object.EntryRC(26,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N219,0)'
	ole_1.object.EntryRC(33,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N233,0)'
	ole_1.object.EntryRC(40,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N247,0)'
	ole_1.object.EntryRC(47,14,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N261,0)'
	ole_1.object.EntryRC(54,14,ls_string)
	
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O163,0)'
	ole_1.object.EntryRC(5,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O177,0)'
	ole_1.object.EntryRC(12,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O191,0)'
	ole_1.object.EntryRC(19,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O205,0)'
	ole_1.object.EntryRC(26,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O219,0)'
	ole_1.object.EntryRC(33,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O233,0)'
	ole_1.object.EntryRC(40,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O247,0)'
	ole_1.object.EntryRC(47,15,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O261,0)'
	ole_1.object.EntryRC(54,15,ls_string)
	
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P163,0)'
	ole_1.object.EntryRC(5,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P177,0)'
	ole_1.object.EntryRC(12,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P191,0)'
	ole_1.object.EntryRC(19,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P205,0)'
	ole_1.object.EntryRC(26,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P219,0)'
	ole_1.object.EntryRC(33,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P233,0)'
	ole_1.object.EntryRC(40,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P247,0)'
	ole_1.object.EntryRC(47,16,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P261,0)'
	ole_1.object.EntryRC(54,16,ls_string)
	
	
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q161,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q163,0)'
	ole_1.object.EntryRC(5,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q175,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q177,0)'
	ole_1.object.EntryRC(12,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q189,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q191,0)'
	ole_1.object.EntryRC(19,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q203,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q205,0)'
	ole_1.object.EntryRC(26,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q217,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q219,0)'
	ole_1.object.EntryRC(33,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q231,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q233,0)'
	ole_1.object.EntryRC(40,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q245,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q247,0)'
	ole_1.object.EntryRC(47,17,ls_string)
	ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q259,0)'
	ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q261,0)'
	ole_1.object.EntryRC(54,17,ls_string)
	
end if
	
		
	

end subroutine

public function boolean wf_retrieve ();Int			li_ret, &
				li_counter, &
				li_date_offset, &
				li_row

Long			ll_row, &
				ll_col, &
				ll_plant_count, &
				ll_temp

String		ls_input_string, &
				ls_Plant_Data, &
				ls_weekly_data, &
				ls_last_update, &
				ls_date, &
				ls_time, &
				ls_plant, &
				ls_complex, &
				ls_inquire, ls_string
DataStore	lds_plant

//If Not IsValid(iu_pas203) Then
//	iu_pas203 = Create u_pas203
//End if

If Not IsValid(iu_ws_pas_share) Then
	iu_ws_pas_share = Create u_ws_pas_share
End if

OpenWithParm(w_box_inventory_inq_new, This)
ls_inquire = message.StringParm

IF not ls_inquire = "True" Then
	Return False
End If

li_date_offset = 0
For li_row = 3 to 52 step 7
	ole_1.object.EntryRC(li_row+0,1,wf_convert_date(RelativeDate(idt_start_date, li_date_offset)))
	li_date_offset ++
	ole_1.object.EntryRC(li_row+0,2,"Inventory")
	ole_1.object.EntryRC(li_row+1,2,"Production")
	ole_1.object.EntryRC(li_row+2,2,"Sales")
	ole_1.object.EntryRC(li_row+3,2,"Sold from Invt")
	ole_1.object.EntryRC(li_row+4,2,"Sales Target")
	ole_1.object.EntryRC(li_row+5,2,"Inventory Target")
	ole_1.object.EntryRC(li_row+6,2,"Inventory Variance")
Next

wf_set_invt_calcs()
wf_set_production_calcs()
//had to use 3 functions for sales calcs because of script size exceeded message
wf_set_sales_calcs1()
wf_set_sales_calcs2()
wf_set_sales_calcs3()
//wf_set_reserv_calcs()
wf_set_sold_from_invt_calcs()
wf_set_sales_target_calcs()
wf_set_invent_target_calcs()
wf_set_invt_var_calcs()
wf_set_total_calcs()

ole_1.Object.Recalc()

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")
// change inquire parms ibdkdld 12/15/02
//s_input_string = dw_division.uf_get_division()
ls_input_string = sle_division_list.Text 
ls_input_string += "~t" + dw_uom_code.uf_get_uom_code()
ls_input_string += "~t" + is_groupid + "~t"
ls_input_string += String(Date(st_start_date.Text),"yyyy-mm-dd")  + "~t"

If rb_quantity.Checked = True Then
	ls_input_string += "Q"  + "~t"
Else
	ls_input_string += "W" + "~t"
End If
	
If rb_total_on_hand.Checked = True Then
	ls_input_string += "T"  + "~t"
Else
	ls_input_string += "S" + "~t"
End If	

If rb_seven_days.Checked = True Then
	ls_input_string += "7"  + "~t"
Else
	ls_input_string += "8" + "~t"
End If

ole_1.Object.ClearRange(150, 1, 555, 35, 1)

dw_weekly_summary.Reset()
dw_weekly_summary.InsertRow(0)


//If Not iu_pas203.nf_pasp04dr_inq_box_inventory_target(istr_error_info, &
//									ls_input_string, &
//									ls_last_update, &
//									ls_Plant_Data, &
//									ls_Weekly_data) Then return False


If Not iu_ws_pas_share.nf_pasp04hr(istr_error_info, &
									ls_input_string, &
									ls_last_update, &
									ls_Plant_Data, &
									ls_Weekly_data) Then return False

//This.SetRedraw(False)

//This is to set the formulas for loads required.  We are unable to change
// the actual spreadsheet.

//wf_change_loads_required_calc()



iw_frame.iu_string.nf_parseLeftRight(ls_last_update, ' ', ls_date, ls_time)
sle_last_update.Text = String(Date(ls_date), 'mm/dd/yyyy') + ' ' + &
								String(Time(ls_time), 'hh:mm am/pm')

// make sure all columns are visible
ole_1.Object.SetColWidth(3, 17, ii_column_width, False)
ole_1.Object.SetColWidth(19,23, ii_column_width, False)

// Put the plant data into a local DataStore to separate foreign from domestic
lds_plant = Create DataStore
lds_plant.DataObject = 'd_invt_vs_sales'

lds_plant.reset()

lds_plant.ImportString(ls_Plant_Data)
li_ret = lds_plant.SetFilter("domestic_or_foreign = 'D'") 
li_ret = lds_plant.Filter()

ls_Plant_Data = lds_plant.Describe("DataWindow.Data")

ls_Plant_data = wf_transpose(lds_plant)

ole_1.Object.SetTabbedText(149,3, ref ll_row, ref ll_col, False, ls_Plant_Data)
ll_plant_count = lds_plant.RowCount()
// Set the plants in the column headings
For li_counter = 1 to ll_plant_count
	ls_plant = Space(32)
	ls_plant = ole_1.Object.EntryRC(149, 2 + li_counter)
	ls_complex = ole_1.Object.EntryRC(150, 2 + li_counter)
	ls_plant = String(Integer(ls_plant), "000") + " " + ls_complex
	ole_1.Object.ColText(li_counter + 2, ls_plant)
Next

If ll_plant_count < 15 Then
	//the first two columns are headings
	// Also, don't hide the last plant
	ole_1.Object.SetColWidth(ll_plant_count + 3, 17, 0, False)
End if
//
li_ret = lds_plant.SetFilter("domestic_or_foreign = 'F'")
li_ret = lds_plant.Filter()
ls_Plant_Data = lds_plant.Describe("DataWindow.Data")
ls_Plant_data = wf_transpose(lds_plant)

ole_1.Object.SetTabbedText(149,19, ref ll_row, ref ll_col, False, ls_Plant_Data)
ll_plant_count = lds_plant.RowCount()
// Set the plants in the column headings
For li_counter = 1 to ll_plant_count
	ls_plant = Space(32)
	ls_plant = ole_1.Object.EntryRC(149,li_counter + 18)
	ls_complex = ole_1.Object.EntryRC(150, li_counter + 18)
	ls_plant = String(Integer(ls_plant), "000") + " " + ls_complex
	ole_1.Object.ColText(li_counter + 18, ls_plant)
Next
If ll_plant_count < 5 Then
	//the first two columns are headings
	// Also, don't hide the last plant
	ole_1.Object.SetColWidth(ll_plant_count + 19,23,0,False)
End if

ole_1.Object.MaxCol = 18 + ll_plant_count

If rb_eight_days.Checked = True Then
	ole_1.Object.MaxRow = 58
Else
	ole_1.Object.MaxRow = 51
End If

If rb_quantity.Checked = True Then
	st_boxessold.Text = "Boxes Sold"
Else
	st_boxessold.Text = "Weight Sold"
End If

dw_weekly_summary.Reset()
dw_weekly_summary.ImportString(ls_Weekly_Data)


SetPointer(Arrow!)
iw_frame.SetMicroHelp('Ready')

lds_plant.setfilter("")
lds_plant.filter()

This.SetRedraw(True)
ole_1.SetFocus()

return true





end function

event resize;call super::resize; integer li_x		= 100
 integer li_y		= 600

if il_BorderPaddingWidth > li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

//ole_1.width = width - (90 + li_x)
ole_1.width = width - (li_x)

// ibdkdld changed height adjustment 12/12/02 from 125 to 170
//ole_1.height = height - dw_weekly_summary.Height - (170 + li_y)
ole_1.height = height - dw_weekly_summary.Height - (li_y)

dw_weekly_summary.Y = This.Height - dw_weekly_summary.Height - 110
//dw_weekly_summary.Y = This.Height - dw_weekly_summary.Height - il_BorderPaddingHeight
st_boxessold.Y = dw_weekly_summary.Y



end event

event close;call super::close;//If IsValid(iu_pas203) Then
//	Destroy(iu_pas203)
//End If

If IsValid(iu_ws_pas_share) Then
	Destroy(iu_ws_pas_share)
End If
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

//For orp menus.
iw_frame.im_menu.mf_enable('m_complete')
iw_frame.im_menu.mf_enable('m_generatesales')

If Not ib_m_print_status Then iw_frame.im_menu.mf_Disable('m_print')

end event

event ue_postopen;call super::ue_postopen;Int	li_fileType
String	ls_temp

//iu_pas203 = Create u_pas203
//If Message.ReturnValue = -1 Then Close(This)

iu_ws_pas_share = Create u_ws_pas_share
If Message.ReturnValue = -1 Then Close(This)

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = 'Invt/Sales'
istr_error_info.se_user_id = sqlca.userid

ls_temp = iw_frame.is_workingdir

ole_1.object.Read(iw_frame.is_workingdir + "pinvtarg.vts", ref li_fileType)

ole_1.object.ShowTabs = 0
ole_1.Object.ShowRowHeading = 0
ole_1.Object.ShowHScrollBar = 1
ole_1.Object.ShowVScrollBar = 1
ole_1.Object.AllowDesigner = False
ole_1.Object.AllowInCellEditing = False
ole_1.Object.AllowSelections = False
ole_1.Object.AllowDelete = False
ole_1.Object.AllowEditHeaders = False
ole_1.Object.AllowFormulas = False

//ole_1.object.ShowTabs = 1
//ole_1.Object.ShowRowHeading = 1
//ole_1.Object.ShowHScrollBar = 1
//ole_1.Object.ShowVScrollBar = 1
//ole_1.Object.AllowDesigner = True
//ole_1.Object.AllowInCellEditing = True
//ole_1.Object.AllowSelections = True
//ole_1.Object.AllowDelete = True
//ole_1.Object.AllowEditHeaders = True
//ole_1.Object.AllowFormulas = True

//  Don't use column width from boxed invt target spreadsheet, just hard code to same as boxed invt. report 
//ii_column_width = ole_1.Object.ColWidth(3)
ii_column_width = 2048

ole_1.Object.ColText(1,"")
ole_1.Object.ColText(2,"")
ole_1.Object.ColText(18,"Total")

This.PostEvent("ue_query")
end event

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
end on

on w_box_inventory_target.create
int iCurrent
call super::create
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.sle_division_list=create sle_division_list
this.st_divisions=create st_divisions
this.rb_eight_days=create rb_eight_days
this.rb_seven_days=create rb_seven_days
this.rb_ready_to_ship_date=create rb_ready_to_ship_date
this.rb_total_on_hand=create rb_total_on_hand
this.rb_weight=create rb_weight
this.rb_quantity=create rb_quantity
this.dw_uom_code=create dw_uom_code
this.st_1=create st_1
this.st_start_date=create st_start_date
this.st_groups=create st_groups
this.cbx_combined=create cbx_combined
this.cbx_uncombined=create cbx_uncombined
this.cbx_gpo=create cbx_gpo
this.cbx_reservation=create cbx_reservation
this.dw_weekly_summary=create dw_weekly_summary
this.st_boxessold=create st_boxessold
this.st_2=create st_2
this.sle_last_update=create sle_last_update
this.ole_1=create ole_1
this.gb_display_uom=create gb_display_uom
this.gb_inv_position=create gb_inv_position
this.gb_number_of_days=create gb_number_of_days
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_8
this.Control[iCurrent+2]=this.st_7
this.Control[iCurrent+3]=this.st_6
this.Control[iCurrent+4]=this.st_5
this.Control[iCurrent+5]=this.st_4
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.sle_division_list
this.Control[iCurrent+8]=this.st_divisions
this.Control[iCurrent+9]=this.rb_eight_days
this.Control[iCurrent+10]=this.rb_seven_days
this.Control[iCurrent+11]=this.rb_ready_to_ship_date
this.Control[iCurrent+12]=this.rb_total_on_hand
this.Control[iCurrent+13]=this.rb_weight
this.Control[iCurrent+14]=this.rb_quantity
this.Control[iCurrent+15]=this.dw_uom_code
this.Control[iCurrent+16]=this.st_1
this.Control[iCurrent+17]=this.st_start_date
this.Control[iCurrent+18]=this.st_groups
this.Control[iCurrent+19]=this.cbx_combined
this.Control[iCurrent+20]=this.cbx_uncombined
this.Control[iCurrent+21]=this.cbx_gpo
this.Control[iCurrent+22]=this.cbx_reservation
this.Control[iCurrent+23]=this.dw_weekly_summary
this.Control[iCurrent+24]=this.st_boxessold
this.Control[iCurrent+25]=this.st_2
this.Control[iCurrent+26]=this.sle_last_update
this.Control[iCurrent+27]=this.ole_1
this.Control[iCurrent+28]=this.gb_display_uom
this.Control[iCurrent+29]=this.gb_inv_position
this.Control[iCurrent+30]=this.gb_number_of_days
end on

on w_box_inventory_target.destroy
call super::destroy
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.sle_division_list)
destroy(this.st_divisions)
destroy(this.rb_eight_days)
destroy(this.rb_seven_days)
destroy(this.rb_ready_to_ship_date)
destroy(this.rb_total_on_hand)
destroy(this.rb_weight)
destroy(this.rb_quantity)
destroy(this.dw_uom_code)
destroy(this.st_1)
destroy(this.st_start_date)
destroy(this.st_groups)
destroy(this.cbx_combined)
destroy(this.cbx_uncombined)
destroy(this.cbx_gpo)
destroy(this.cbx_reservation)
destroy(this.dw_weekly_summary)
destroy(this.st_boxessold)
destroy(this.st_2)
destroy(this.sle_last_update)
destroy(this.ole_1)
destroy(this.gb_display_uom)
destroy(this.gb_inv_position)
destroy(this.gb_number_of_days)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

//For orp menus.
iw_frame.im_menu.mf_Disable('m_complete')
iw_frame.im_menu.mf_Disable('m_generatesales')

ib_m_print_status = iw_frame.im_menu.mf_Enabled('m_print')
iw_frame.im_menu.mf_Enable('m_print')


end event

event ue_fileprint;call super::ue_fileprint;String	ls_header, &
			ls_footer
			
Integer li_length

// ibdkdld change header data 12/12/02
ls_header = "&RLast Update: " + trim(sle_last_update.Text) + &
				"&C" + trim(This.Title) + "~r~n" + &
				"Divs:" + trim(sle_division_list.Text) + "~r~n" + &
				"UOM Code - " + trim(dw_uom_code.uf_Get_description()) + "~r~n" + &
				trim(st_groups.text) + "~r~n"  
//				"UOM Code - " + dw_uom_code.uf_Get_description() + "   " + &
//				left(st_groups.text, 15) + "~r~n" + & 
//				mid(st_groups.text, 16) + "~r~n"  

If cbx_combined.Checked Then 		ls_header += "Comb."
If cbx_uncombined.Checked Then 	ls_header += " Uncomb."
If cbx_gpo.Checked Then 			ls_header += " GPO"
If cbx_reservation.Checked Then 	ls_header += " Reserv." + "~r~n"  

If rb_quantity.Checked Then
	ls_header += "Disp UOM: Qty"  
Else
	ls_header += "Disp UOM: Wt" 
ENd If

If rb_total_on_hand.Checked Then
	ls_header += "  Inv Pos: Total on Hand" + "~r~n"  
Else
	ls_header += "  Inv Pos: Ready to Ship Date" + "~r~n"  
ENd If

li_length = len(ls_header)

if rb_quantity.Checked Then
	ls_footer = "&l" + String(Today(), "mmm dd") + "-" + &
					String(RelativeDate(Today(), 6), "mmm dd") + "     " + &
					String(dw_weekly_summary.GetItemNumber(1, "week1")) + &
					"~r~n" + String(RelativeDate(Today(), 23 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 29 - DayNumber(Today()) ), "mmm dd")  + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week4")) + &
					"&cBoxes Sold~r~n" + &
					String(RelativeDate(Today(), 9 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 15 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week2")) + &
					"~r~n" + String(RelativeDate(Today(), 30 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 36 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week5")) + &
					"&r" + String(RelativeDate(Today(), 16 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 22 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week3")) + &
					"~r~n" + String(RelativeDate(Today(), 37 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 43 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week6"))
Else
		ls_footer = "&l" + String(Today(), "mmm dd") + "-" + &
					String(RelativeDate(Today(), 6), "mmm dd") + "     " + &
					String(dw_weekly_summary.GetItemNumber(1, "week1")) + &
					"~r~n" + String(RelativeDate(Today(), 23 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 29 - DayNumber(Today()) ), "mmm dd")  + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week4")) + &
					"&cWeight Sold~r~n" + &
					String(RelativeDate(Today(), 9 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 15 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week2")) + &
					"~r~n" + String(RelativeDate(Today(), 30 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 36 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week5")) + &
					"&r" + String(RelativeDate(Today(), 16 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 22 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week3")) + &
					"~r~n" + String(RelativeDate(Today(), 37 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 43 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week6"))
End If
					
ole_1.Object.PrintTopMargin = 1.50
ole_1.Object.PrintHeader = ls_header
ole_1.Object.PrintFooter = ls_footer
ole_1.Object.PrintColHeading = True

If rb_eight_days.Checked Then
	ole_1.Object.PrintArea = "$A$3:$W$58"
Else
	ole_1.Object.PrintArea = "$A$3:$W$51"
End If

ole_1.Object.FilePrint(False)


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case "division_list"
		sle_division_list.Text = as_value
	Case "UOM Code"
		dw_uom_code.uf_set_uom_code(as_value)
// add 3 ibdkdld 12/09/02
	Case 'groupid'
		is_groupID = as_value
	Case 'group_system'
		st_groups.text = "SYSTEM - " + as_value
	Case 'group_description'
		st_groups.text += "   GROUP - " + as_value
	Case 'start_date'
		st_start_date.text = String(Date(as_value), "mm/dd/yyyy")		
		idt_start_date = Date(as_value)
	Case 'display_uom'
		If as_value = 'Q' then
			rb_quantity.Checked = True
		Else
			rb_weight.Checked = True
		End If
	Case 'inv_position'
		If as_value = 'T' then
			rb_total_on_hand.Checked = True
		Else
			rb_ready_to_ship_date.Checked = True
		End If
	Case 'number_of_days'
		If as_value = '7' then
			rb_seven_days.Checked = True
		Else
			rb_eight_days.Checked = True
		End If		
End Choose
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case	"number_of_days"
		If rb_eight_days.Checked Then
			message.StringParm = "8"
		Else
			If rb_seven_days.Checked Then
				message.StringParm = "7"
			Else
				message.Stringparm = ''
			End If
		End If
	Case	"display_uom"
		If rb_weight.Checked Then
			message.StringParm = "W"
		Else
			If rb_quantity.Checked Then
				message.StringParm = "Q"
			Else
				message.StringParm = ''
			End If
		End If
	Case	"inv_position"
		If rb_total_on_hand.Checked Then
			message.StringParm = "T"
		Else
			If rb_ready_to_ship_date.Checked Then
				message.StringParm = "S"
			Else
				message.StringParm = ''
			End If
		End If		
	Case	"start_date"
		If st_start_date.text = '' Then
			message.StringParm = String(Today(),"mm/dd/yyyy")
		Else
			message.StringParm = st_start_date.Text
		End If		
	Case	"division_list"
		If sle_division_list.Text = '' Then
			message.StringParm = ''
		Else
			message.StringParm = sle_division_list.Text + ',' // pass extra comma at end
		End if	
	
End Choose	
end event

type st_8 from statictext within w_box_inventory_target
integer x = 1746
integer y = 368
integer width = 206
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "8"
boolean focusrectangle = false
end type

type st_7 from statictext within w_box_inventory_target
integer x = 1746
integer y = 308
integer width = 178
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "7"
boolean focusrectangle = false
end type

type st_6 from statictext within w_box_inventory_target
integer x = 987
integer y = 360
integer width = 475
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ready to Ship Date"
boolean focusrectangle = false
end type

type st_5 from statictext within w_box_inventory_target
integer x = 987
integer y = 304
integer width = 466
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Total On Hand"
boolean focusrectangle = false
end type

type st_4 from statictext within w_box_inventory_target
integer x = 206
integer y = 356
integer width = 562
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Weight in Thousands"
boolean focusrectangle = false
end type

type st_3 from statictext within w_box_inventory_target
integer x = 201
integer y = 300
integer width = 343
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Quantity"
boolean focusrectangle = false
end type

type sle_division_list from singlelineedit within w_box_inventory_target
integer x = 293
integer width = 1339
integer height = 76
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
end type

type st_divisions from statictext within w_box_inventory_target
integer x = 37
integer width = 247
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Divisions:"
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type rb_eight_days from radiobutton within w_box_inventory_target
integer x = 1669
integer y = 364
integer width = 96
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_seven_days from radiobutton within w_box_inventory_target
integer x = 1669
integer y = 300
integer width = 101
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_ready_to_ship_date from radiobutton within w_box_inventory_target
integer x = 905
integer y = 360
integer width = 101
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_total_on_hand from radiobutton within w_box_inventory_target
integer x = 905
integer y = 296
integer width = 110
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_weight from radiobutton within w_box_inventory_target
integer x = 128
integer y = 348
integer width = 110
integer height = 72
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_quantity from radiobutton within w_box_inventory_target
integer x = 128
integer y = 292
integer width = 101
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type dw_uom_code from u_uom_targ_code within w_box_inventory_target
integer y = 84
integer width = 786
integer height = 92
integer taborder = 30
end type

event constructor;call super::constructor;This.uf_enable(false)
end event

type st_1 from statictext within w_box_inventory_target
integer x = 2190
integer y = 184
integer width = 274
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Start Date:"
boolean focusrectangle = false
end type

type st_start_date from statictext within w_box_inventory_target
integer x = 2469
integer y = 184
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_groups from statictext within w_box_inventory_target
integer x = 23
integer y = 176
integer width = 2130
integer height = 68
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Courier New"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cbx_combined from checkbox within w_box_inventory_target
integer x = 1733
integer y = 8
integer width = 347
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Combined"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(80, 1, 'Y')
	dw_weekly_summary.SetItem(1, "comb_cbx", 'Y')
Else
	ole_1.Object.TextRC(80, 1, 'N')
	dw_weekly_summary.SetItem(1, "comb_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type cbx_uncombined from checkbox within w_box_inventory_target
integer x = 2139
integer y = 8
integer width = 507
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Uncombined"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(79, 1, 'Y')
	dw_weekly_summary.SetItem(1, "uncomb_cbx", 'Y')
Else
	ole_1.Object.TextRC(79, 1, 'N')
	dw_weekly_summary.SetItem(1, "uncomb_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type cbx_gpo from checkbox within w_box_inventory_target
integer x = 1733
integer y = 92
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "GPO"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(81, 1, 'Y')
	dw_weekly_summary.SetItem(1, "gpo_cbx", 'Y')
Else
	ole_1.Object.TextRC(81, 1, 'N')
	dw_weekly_summary.SetItem(1, "gpo_cbx", 'N')
End if
ole_1.Object.Recalc()
end event

type cbx_reservation from checkbox within w_box_inventory_target
integer x = 2139
integer y = 92
integer width = 475
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Reservation"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(82, 1, 'Y')
	dw_weekly_summary.SetItem(1, "reserv_cbx", 'Y')
Else
	ole_1.Object.TextRC(82, 1, 'N')
	dw_weekly_summary.SetItem(1, "reserv_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type dw_weekly_summary from u_netwise_dw within w_box_inventory_target
integer x = 320
integer y = 1464
integer width = 2437
integer height = 188
integer taborder = 10
string dataobject = "d_invt_vs_sales_summary"
boolean border = false
end type

on constructor;call u_netwise_dw::constructor;ib_updateable = False

This.InsertRow(0)
end on

type st_boxessold from statictext within w_box_inventory_target
integer y = 1460
integer width = 357
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Boxes Sold :"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_box_inventory_target
integer x = 750
integer y = 112
integer width = 315
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Last Update"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_last_update from singlelineedit within w_box_inventory_target
integer x = 1083
integer y = 100
integer width = 581
integer height = 72
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string pointer = "Arrow!"
long textcolor = 33554432
long backcolor = 67108864
boolean autohscroll = false
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type ole_1 from olecustomcontrol within w_box_inventory_target
event click ( long nrow,  long ncol )
event dblclick ( long nrow,  long ncol )
event canceledit ( )
event selchange ( )
event startedit ( ref string editstring,  ref integer ab_cancel )
event endedit ( ref string editstring,  ref integer ab_cancel )
event startrecalc ( )
event endrecalc ( )
event topleftchanged ( )
event objclick ( ref string objname,  long objid )
event objdblclick ( ref string objname,  long objid )
event rclick ( long nrow,  long ncol )
event rdblclick ( long nrow,  long ncol )
event objvaluechanged ( ref string objname,  long objid )
event modified ( )
event mousedown ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mouseup ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mousemove ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event objgotfocus ( ref string objname,  long objid )
event objlostfocus ( ref string objname,  long objid )
event validationfailed ( ref string pentry,  long nsheet,  long nrow,  long ncol,  ref string pshowmessage,  ref integer paction )
event keypress ( ref integer keyascii )
event keydown ( ref integer keycode,  integer shift )
event keyup ( ref integer keycode,  integer shift )
integer x = 27
integer y = 444
integer width = 2825
integer height = 948
integer taborder = 31
boolean bringtotop = true
long backcolor = 67108864
string binarykey = "w_box_inventory_target.win"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
end type

type gb_display_uom from groupbox within w_box_inventory_target
integer x = 32
integer y = 236
integer width = 754
integer height = 196
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Display UOM"
end type

type gb_inv_position from groupbox within w_box_inventory_target
integer x = 818
integer y = 240
integer width = 658
integer height = 196
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Inv Position"
end type

type gb_number_of_days from groupbox within w_box_inventory_target
integer x = 1591
integer y = 240
integer width = 457
integer height = 200
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Number of Days"
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
09w_box_inventory_target.bin 
2500001800e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000007fffffffe00000004000000050000000600000008fffffffe000000090000000afffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000e993d0a001d3f9e20000000300000c800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000540000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000020000067900000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004042badc511ce5e58415210b60100485300000000e993d0a001d3f9e2e993d0a001d3f9e200000000000000000000000000000001fffffffe000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f000000100000001100000012000000130000001400000015000000160000001700000018000000190000001a0000001bfffffffe0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002f0000003000000031fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
20ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f004300790070006900720068006700200074006300280020002900390031003500390056002000730069006100750020006c006f00430070006d006e006f006e0065007300740020002c006e0049002e006300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fffe00020106042badc511ce5e58415210b60100485300000001fb8f0821101b01640008ed8413c72e2b000000300000064900000008000001000000004800000101000000500000010200000058000001030000006000000104000000680000010500000070000001060000007c00000000000005c000000003000100000000000300003fdf000000030000187f00000003000000600000000200000001000000080000000100000000000000410000053b00090000ee000505000000040014ef000000000000000000ffffff00ffffffff3dfff3ff00000012000000000000380001000000310258000000c814907fff00000000010500000061697241c814316cff0000000002bc7f0000000072410500316c61690200c814907fff00000000010500000061697241c814316cff0002000002bc7f0000000072410500316c61690000c814907fff00000000010500000061697241051a1e6c24221700232c2322295f302322285c3b2c2322245c302323061f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c201e295c221d00072c2322242e302323295f303022285c3b2c2322242e302323295c30300008251e2224222223232c2330302e305b3b295f5d6465522422285c232c2322302e30231e295c3032002a352422285f23202a223023232c5f3b295f22242228285c202a23232c233b295c302422285f22202a22295f222d40285f3b2c1e295f5f29002923202a283023232c5f3b295f5c202a28232c2328295c30232a285f3b222d22205f3b295f295f4028002c3d1e22285f3a202a222423232c2330302e305f3b295f22242228285c202a23232c2330302e305f3b295c222422282d22202a5f3f3f22285f3b291e295f4031002b34202a285f23232c2330302e305f3b295f5c202a28232c2328302e30233b295c30202a285f3f222d223b295f3f5f40285f321f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c251e295c222200332c2322242e302323295f303065525b3b285c5d64232224223023232c5c30302e0005ed2900000000000003ec0014e000f5000000c00020ff0000002000000000e00000000000011420fff5000020c0c400000000000000000114e000f5000000c0c420ff0000002000000000e00000000000021420fff5000020c0c400000000000000000214e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e000000000000014200001000020c00000000000000000000514e000f5003300c0c820ff0000002000000000e00000003200051420fff5000020c0c800000000000000000514e000f5000c00c0c820ff0000002000000000e00000000a00051420fff5000020c0c800000000000000000514e000f5000d00c0c820ff0000002000000000e00000000000041412fff0000020c04800000000000000000009850068530600317465650500090a010d001000640c0010000011d2f1a9fc3f50624d2a00015f012b000000012500018c00ff81000100031404c11541260261500708262065670000835026000084000000003fe8000000000027e80000000000283f0000000000293ff000000000a13ff0006400012201000100060001000000000000000000e00000000000003fe00000000000013f000000000000000018f70000ccff009200ffffff00c0c0c03fff00ff0000000000000000000010f200000000000000000000000015f6ffff150015004000ff00030f1d060000000000010000000000000a3e0000000002360000000064a0000099006400000a092600000008000000000000000100010600000009006c6966006d616e6501030065000c0000735f00006b636f74706f727001040073000c00006f620000726564726c7974730101006500090000655f00006e65747802007874090000015f000000657478650079746e00000105000000086e70706100656d6100000100000000097265765f6e6f697300000000000000000001000000003fdf0000187f00000060000100010101010101010101010101010101010100053b000900000000050500000004ee14ef00000000000000000000ffff0000fffffffffff3ffff0000123d0000000000380000000000000258000100c814317fff000000000190000000006972410514316c61000000c802bc7fff00000000410500006c61697200c814317fff0002
2800000190000000006972410514316c61000200c802bc7fff00000000410500006c61697200c814317fff00000000019000000000697241051a1e6c61221700052c2322245f302323285c3b29232224223023232c1f1e295c221c00062c2322245f302323525b3b295c5d6465006f00430074006e006e00650073007400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000001c00000568000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002224222823232c231e295c301d000720232224223023232c5f30302e285c3b29232224223023232c5c30302e08251e2924222200232c2322302e30233b295f306465525b22285c5d2c2322242e302323295c3030002a351e22285f32202a222423232c233b295f302422285f5c202a22232c2328295c302322285f3b202a22245f222d22285f3b291e295f402900292c202a285f23232c233b295f30202a285f2c23285c5c302323285f3b292d22202a3b295f225f40285f2c3d1e29285f3a002a222422232c2320302e30233b295f302422285f5c202a22232c2328302e30233b295c302422285f22202a223f3f222d5f3b295f295f4028002b341e2a285f31232c2320302e30233b295f30202a285f2c23285c2e302323295c30302a285f3b222d2220295f3f3f40285f3b1f1e295f221c00322c2322245f302323525b3b295c5d64652224222823232c231e295c3022003325232224223023232c5f30302e525b3b295c5d64652224222823232c2330302e3005ed295c000000000003ec0014e00000000000000020fff5000020c00000000000000000000114e0fff5000020c0c420000000000000000014e0000000000001c420fff5000020c00000000000000000000214e0fff5000020c0c420000000000000000014e0000000000002c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e00001000020c00020000000000000000014e0000000330005c820fff5000020c00000000000000000000514e0fff5003220c0c820000000000000000014e00000000c0005c820fff5000020c00000000000000000000514e0fff5000a20c0c820000000000000000014e00000000d0005c820fff5000020c00000000000000000000414e0fff0000020c04812000000000000000009850000530600007465656800090a310d001005640c000100001100f1a9fc1050624dd200015f3f2b00002a012500018c00ff00000100011404c181412602035007081520656761008350260000840000000026e80000000000273f0000000000283fe800000000293ff000000000003ff00000000122a1000100640001000100000006000000000000000000003fe00000000000013fe00000000000000000f7000000ff009218ffffffccc0c0c000ff00ff000000003f000000000010f200000000000000000000000000f6ffff000015001500ff00150f1d06400000000301000000000000003e0000000002360a00000000a0000000006400640a09269901ff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
19w_box_inventory_target.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
