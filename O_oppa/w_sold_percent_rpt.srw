HA$PBExportHeader$w_sold_percent_rpt.srw
forward
global type w_sold_percent_rpt from w_netwise_sheet
end type
type dw_detail from u_netwise_dw within w_sold_percent_rpt
end type
type dw_division from u_division within w_sold_percent_rpt
end type
type dw_plant from u_plant within w_sold_percent_rpt
end type
type dw_header from u_netwise_dw within w_sold_percent_rpt
end type
end forward

global type w_sold_percent_rpt from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 2523
integer height = 1824
string title = "Sold Percent Report"
long backcolor = 12632256
long il_borderpaddingwidth = 100
dw_detail dw_detail
dw_division dw_division
dw_plant dw_plant
dw_header dw_header
end type
global w_sold_percent_rpt w_sold_percent_rpt

type variables
u_pas203		iu_pas203
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Boolean		ib_reinquire
end variables
forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();String	ls_string, &
			ls_input, &
			ls_output, &
			ls_filter, &
			ls_exclude, &
			ls_sold_percent, &
			ls_filter_percent

Integer	li_ret, &
			li_counter
Long		ll_rowcount


If dw_header.AcceptText() < 1 Then Return False

Call w_base_sheet::closequery

If Not ib_reinquire Then
	OpenWithParm(w_sold_percent_rpt_inq, This)
	ls_string = Message.StringParm
	If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False
Else
	ib_reinquire = False
End If

SetPointer(HourGlass!)

This.SetRedraw(False)

If dw_header.AcceptText() < 1 Then Return False

ls_input  = dw_division.uf_get_division() + "~t"
ls_input  += dw_plant.uf_get_plant_code() + "~r~n"

dw_detail.Reset()

istr_error_info.se_event_name = "wf_retrieve"

li_ret = iu_ws_pas_share.nf_pasp65fr(istr_error_info, &
														ls_input, &
														ls_output)
														
														


If li_ret <> 0 Then
	This.SetRedraw(True)
	return False
End If

ll_rowcount = dw_detail.ImportString(ls_output)

If ll_rowcount > 0 Then 
	ls_filter_percent = String(dw_header.GetItemDecimal(1, 'filter_percent'))
	If Double(ls_filter_percent) > 0 Then
		dw_detail.SetFilter('percent_sold >= ' + ls_filter_percent)
		dw_detail.Filter()
	End If
	dw_detail.Sort()
	iw_frame.SetMicroHelp(String(ll_rowcount) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

dw_detail.ResetUpdate()

This.SetRedraw(True)
return true
end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_generatesales')

iw_frame.im_menu.mf_Enable('m_print')

end event

event close;call super::close;If IsValid(iu_pas203) Then
	Destroy iu_pas203
End If


Destroy iu_ws_pas_share
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division.uf_get_division()
	Case 'plant'
		Message.StringParm = dw_plant.uf_get_plant_code()
End Choose


end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_sold_percent_rpt"
istr_error_info.se_user_id 		= sqlca.userid


iu_pas203 = Create u_pas203
iu_ws_pas_share = Create u_ws_pas_share

ib_reinquire = FALSE

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division.uf_set_division(as_value)
	case 'plant'
		dw_plant.uf_set_plant_code(as_value)
End Choose

end event

on w_sold_percent_rpt.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_division=create dw_division
this.dw_plant=create dw_plant
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_plant
this.Control[iCurrent+4]=this.dw_header
end on

on w_sold_percent_rpt.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_division)
destroy(this.dw_plant)
destroy(this.dw_header)
end on

event ue_fileprint;call super::ue_fileprint;DataWindowChild	ldwc_header, &
						ldwc_detail
						
DataStore			lds_print

String				ls_plant_code, &
						ls_plant_descr, &
						ls_division_code, &
						ls_division_descr
						
						
IF Not ib_print_ok Then Return

lds_print = Create u_print_datastore
lds_print.DataObject = 'd_sold_percent_rpt_prt'


lds_print.GetChild('dw_report_header', ldwc_header)
dw_header.ShareData(ldwc_header)
lds_print.GetChild('dw_report_detail', ldwc_detail)
dw_detail.ShareData(ldwc_detail)

ldwc_header.Modify("filter_percent.Border = 0")

ldwc_detail.Modify("product.Border = 0 " + &
							"week_end_date.Border = 0 " + &
							"forecast.Border = 0 " + &
							"sold.Border = 0 " + &
							"percent_sold.Border = 0 " + &
							"buffer.Border = 0 " + &
							"product_state.Border = 0 " + &
							"buffer_pct.Border = 0")


ls_plant_code = dw_plant.uf_get_plant_code()
ls_plant_descr = dw_plant.uf_get_plant_descr()
ls_division_code = dw_division.uf_get_division()
ls_division_descr = dw_division.uf_get_division_descr()

lds_print.object.plant_code_t.Text = ls_plant_code
lds_print.object.plant_descr_t.Text = ls_plant_descr
lds_print.object.division_code_t.Text = ls_division_code
lds_print.object.division_descr_t.Text = ls_division_descr

lds_print.Print()

return
end event

event resize;call super::resize;// constant integer li_x		= 46
// constant integer li_y		= 192
//
integer li_x = 46
integer li_y = 222
  
//dw_detail.width	= newwidth - (30 + li_x)
//dw_detail.height	= newheight - (30 + li_y)



if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_detail.width  = newwidth - (li_x)
dw_detail.height = newheight - (li_y)


end event

type dw_detail from u_netwise_dw within w_sold_percent_rpt
integer x = 46
integer y = 192
integer width = 2281
integer height = 1460
integer taborder = 30
string dataobject = "d_sold_percent_rpt"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;ib_updateable = False
end event

type dw_division from u_division within w_sold_percent_rpt
integer x = 23
integer y = 12
integer width = 1614
integer height = 80
integer taborder = 40
end type

event constructor;call super::constructor;ib_updateable = False
This.Disable()

end event

type dw_plant from u_plant within w_sold_percent_rpt
integer x = 55
integer y = 96
integer width = 1577
integer height = 92
integer taborder = 20
end type

event constructor;call super::constructor;ib_updateable = False
This.Disable()

end event

type dw_header from u_netwise_dw within w_sold_percent_rpt
integer x = 1646
integer y = 16
integer width = 736
integer height = 124
integer taborder = 10
string dataobject = "d_sold_percent_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;Choose Case dwo.name
	Case 'filter_percent'
		If double(data) < 0.00 or double(data) > 1.00 Then
			iw_frame.SetMicrohelp('Filter Percent must be between 0 and 100%')
			Return 1
		End IF
End Choose

dw_detail.SetRedraw(False)
dw_detail.SetFilter('percent_sold >= ' + data)
dw_detail.Filter()
dw_detail.Sort()
dw_detail.SetRedraw(True)

end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;InsertRow(0)
end event

