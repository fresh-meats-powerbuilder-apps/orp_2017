HA$PBExportHeader$w_invt_vs_sales.srw
$PBExportComments$Boxed inventory Report
forward
global type w_invt_vs_sales from w_netwise_sheet
end type
type dw_division from u_division within w_invt_vs_sales
end type
type cbx_combined from checkbox within w_invt_vs_sales
end type
type cbx_uncombined from checkbox within w_invt_vs_sales
end type
type cbx_gpo from checkbox within w_invt_vs_sales
end type
type cbx_reservation from checkbox within w_invt_vs_sales
end type
type dw_weekly_summary from u_netwise_dw within w_invt_vs_sales
end type
type st_boxessold from statictext within w_invt_vs_sales
end type
type st_2 from statictext within w_invt_vs_sales
end type
type sle_last_update from singlelineedit within w_invt_vs_sales
end type
type ole_1 from olecustomcontrol within w_invt_vs_sales
end type
end forward

global type w_invt_vs_sales from w_netwise_sheet
integer width = 2907
integer height = 1488
string title = "old Boxed Inventory Report "
long backcolor = 12632256
dw_division dw_division
cbx_combined cbx_combined
cbx_uncombined cbx_uncombined
cbx_gpo cbx_gpo
cbx_reservation cbx_reservation
dw_weekly_summary dw_weekly_summary
st_boxessold st_boxessold
st_2 st_2
sle_last_update sle_last_update
ole_1 ole_1
end type
global w_invt_vs_sales w_invt_vs_sales

type variables
Boolean		ib_m_Print_Status

u_pas203		iu_pas203

s_error		istr_error_info

Int		ii_column_width
end variables

forward prototypes
public function boolean wf_retrieve ()
public function string wf_get_division ()
public function string wf_transpose (ref datastore adw_source)
end prototypes

public function boolean wf_retrieve ();Int			li_ret, &
				li_counter

Long			ll_row, &
				ll_col, &
				ll_plant_count, &
				ll_temp

String		ls_Division, &
				ls_Plant_Data, &
				ls_weekly_data, &
				ls_last_update, &
				ls_date, &
				ls_time, &
				ls_plant, &
				ls_complex

DataStore	lds_plant

If Not IsValid(iu_pas203) Then
	iu_pas203 = Create u_pas203
End if

OpenWithParm(w_invt_vs_sales_inq, This)

ls_division = Message.StringParm
If iw_frame.iu_string.nf_isEmpty(ls_division) Then return false

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

dw_division.uf_set_division(ls_division)

ole_1.Object.ClearRange(150, 1, 555, 35, 1)

dw_weekly_summary.Reset()
dw_weekly_summary.InsertRow(0)


If Not iu_pas203.nf_pasp22br_inq_box_inventory(istr_error_info, &
									ls_Division, &
									ls_last_update, &
									ls_Plant_Data, &
									ls_Weekly_data) Then return False

//This.SetRedraw(False)

iw_frame.iu_string.nf_parseLeftRight(ls_last_update, ' ', ls_date, ls_time)
sle_last_update.Text = String(Date(ls_date), 'mm/dd/yyyy') + ' ' + &
								String(Time(ls_time), 'hh:mm am/pm')

// make sure all columns are visible
ole_1.Object.SetColWidth(3, 17, ii_column_width, False)
ole_1.Object.SetColWidth(19,23, ii_column_width, False)

// Put the plant data into a local DataStore to separate foreign from domestic
lds_plant = Create DataStore
lds_plant.DataObject = 'd_invt_vs_sales'
lds_plant.ImportString(ls_Plant_Data)
li_ret = lds_plant.SetFilter("domestic_or_foreign = 'D'") 
li_ret = lds_plant.Filter()

//ls_Plant_Data = lds_plant.Describe("DataWindow.Data")

ls_Plant_data = wf_transpose(lds_plant)

ole_1.Object.SetTabbedText(149,3, ref ll_row, ref ll_col, False, ls_Plant_Data)
ll_plant_count = lds_plant.RowCount()
// Set the plants in the column headings
For li_counter = 1 to ll_plant_count
	ls_plant = Space(32)
	ls_plant = ole_1.Object.EntryRC(149, 2 + li_counter)
	ls_complex = ole_1.Object.EntryRC(150, 2 + li_counter)
	ls_plant = String(Integer(ls_plant), "000") + " " + ls_complex
	ole_1.Object.ColText(li_counter + 2, ls_plant)
Next

If ll_plant_count < 15 Then
	//the first two columns are headings
	// Also, don't hide the last plant
	ole_1.Object.SetColWidth(ll_plant_count + 3, 17, 0, False)
End if

li_ret = lds_plant.SetFilter("domestic_or_foreign = 'F'")
li_ret = lds_plant.Filter()
//ls_Plant_Data = lds_plant.Describe("DataWindow.Data")
ls_Plant_data = wf_transpose(lds_plant)

ole_1.Object.SetTabbedText(149,19, ref ll_row, ref ll_col, False, ls_Plant_Data)
ll_plant_count = lds_plant.RowCount()
// Set the plants in the column headings
For li_counter = 1 to ll_plant_count
	ls_plant = Space(32)
	ls_plant = ole_1.Object.EntryRC(149,li_counter + 18)
	ls_complex = ole_1.Object.EntryRC(150, li_counter + 18)
	ls_plant = String(Integer(ls_plant), "000") + " " + ls_complex
	ole_1.Object.ColText(li_counter + 18, ls_plant)
Next
If ll_plant_count < 5 Then
	//the first two columns are headings
	// Also, don't hide the last plant
	ole_1.Object.SetColWidth(ll_plant_count + 19,23,0,False)
End if

ole_1.Object.MaxCol = 18 + ll_plant_count
ole_1.Object.MaxRow = 51


dw_weekly_summary.Reset()
dw_weekly_summary.ImportString(ls_Weekly_Data)

Destroy lds_plant
This.SetRedraw(True)
return true





end function

public function string wf_get_division ();If dw_division.RowCount() > 0 Then
	return dw_division.uf_get_division()
End if
return ""
end function

public function string wf_transpose (ref datastore adw_source);DataStore	lds_2, &
				lds_3, &
				lds_4

String		ls_temp = "", &
				ls_syntax_begin = 'release 5; datawindow(units=0 timer_interval=0 processing=0 ) table(', &
				ls_syntax4, &
				ls_syntax3, &
				ls_syntax2

Integer		li_counter, &
				li_row_count, &
				li_col_count

// Here we create three temporary dynamic DataStores to hold the data after transposition
lds_2 = Create DataStore
lds_3 = Create DataStore
lds_4 = Create DataStore
ls_syntax4 = ls_syntax_begin
ls_syntax3 = ls_syntax_begin
ls_syntax2 = ls_syntax_begin
For li_counter = 1 to 15
	ls_syntax4 += 'column=(type=char(1) name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
	ls_syntax3 += 'column=(type=char(3) name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
	ls_syntax2 += 'column=(type=number name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
Next
ls_syntax4 += ')'
ls_syntax3 += ')'
ls_syntax2 += ')'
lds_4.Create(ls_syntax4)
lds_3.Create(ls_syntax3)
lds_2.Create(ls_syntax2)

lds_3.InsertRow(0)
lds_3.InsertRow(0)
li_row_count = adw_source.RowCount()
For li_counter = 1 to li_row_count
	lds_3.SetItem(1,li_counter,adw_source.GetItemString(li_counter,1))
	lds_3.SetItem(2,li_counter,adw_source.GetItemString(li_counter,2))
Next

For li_col_count = 1 to 14*8 + 3
	lds_2.InsertRow(0)
	For li_counter = 1 to li_row_count
		lds_2.SetItem(li_col_count,li_counter,adw_source.GetItemNumber(li_counter,li_col_count + 2))
	Next
Next

lds_4.InsertRow(0)
For li_counter = 1 to li_row_count
	lds_4.SetItem(1,li_counter,adw_source.GetItemString(li_counter,14*8+6))
Next

ls_temp = lds_3.Object.DataWindow.Data + "~r~n"
ls_temp += lds_2.Object.DataWindow.Data + "~r~n"
ls_temp += lds_4.Object.DataWindow.Data + "~r~n"
  
Destroy lds_2
Destroy lds_3
Destroy lds_4

Return ls_temp
end function

event resize;call super::resize;constant integer li_x		= 10
constant integer li_y		= 201

ole_1.width = width - (90 + li_x)
ole_1.height = height - dw_weekly_summary.Height - (125 + li_y)

dw_weekly_summary.Y = This.Height - dw_weekly_summary.Height - 110
st_boxessold.Y = dw_weekly_summary.Y
end event

on close;call w_netwise_sheet::close;If IsValid(iu_pas203) Then
	Destroy(iu_pas203)
End If
end on

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

//For orp menus.
iw_frame.im_menu.mf_enable('m_complete')
iw_frame.im_menu.mf_enable('m_generatesales')

If Not ib_m_print_status Then iw_frame.im_menu.mf_Disable('m_print')

end event

event ue_postopen;call super::ue_postopen;Int	li_fileType

iu_pas203 = Create u_pas203
If Message.ReturnValue = -1 Then Close(This)

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = 'Invt/Sales'
istr_error_info.se_user_id = sqlca.userid

ole_1.object.Read(iw_frame.is_workingdir + "pinvvsls.vts", ref li_fileType)
ole_1.object.ShowTabs = 0
ole_1.Object.ShowRowHeading = 0
ole_1.Object.ShowHScrollBar = 1
ole_1.Object.ShowVScrollBar = 1
ole_1.Object.AllowDesigner = False
ole_1.Object.AllowInCellEditing = False
ole_1.Object.AllowSelections = False
ole_1.Object.AllowDelete = False
ole_1.Object.AllowEditHeaders = False
ole_1.Object.AllowFormulas = False

ii_column_width = ole_1.Object.ColWidth(3)
ole_1.Object.ColText(1,"")
ole_1.Object.ColText(2,"")
ole_1.Object.ColText(18,"Total")

This.PostEvent("ue_query")
end event

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
end on

on w_invt_vs_sales.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.cbx_combined=create cbx_combined
this.cbx_uncombined=create cbx_uncombined
this.cbx_gpo=create cbx_gpo
this.cbx_reservation=create cbx_reservation
this.dw_weekly_summary=create dw_weekly_summary
this.st_boxessold=create st_boxessold
this.st_2=create st_2
this.sle_last_update=create sle_last_update
this.ole_1=create ole_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.cbx_combined
this.Control[iCurrent+3]=this.cbx_uncombined
this.Control[iCurrent+4]=this.cbx_gpo
this.Control[iCurrent+5]=this.cbx_reservation
this.Control[iCurrent+6]=this.dw_weekly_summary
this.Control[iCurrent+7]=this.st_boxessold
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.sle_last_update
this.Control[iCurrent+10]=this.ole_1
end on

on w_invt_vs_sales.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.cbx_combined)
destroy(this.cbx_uncombined)
destroy(this.cbx_gpo)
destroy(this.cbx_reservation)
destroy(this.dw_weekly_summary)
destroy(this.st_boxessold)
destroy(this.st_2)
destroy(this.sle_last_update)
destroy(this.ole_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

//For orp menus.
iw_frame.im_menu.mf_Disable('m_complete')
iw_frame.im_menu.mf_Disable('m_generatesales')

ib_m_print_status = iw_frame.im_menu.mf_Enabled('m_print')
iw_frame.im_menu.mf_Enable('m_print')


end event

event ue_fileprint;call super::ue_fileprint;String	ls_header, &
			ls_footer


ls_header = "&RLast Update: " + sle_last_update.Text + &
				"&C" + This.Title + "~r~n" + &
				"Division " + dw_division.uf_Get_Division() + " " + &
				dw_division.uf_Get_Division_Descr() + "~r~n"
If cbx_combined.Checked Then 		ls_header += " Combined"
If cbx_uncombined.Checked Then 	ls_header += " Uncombined"
If cbx_gpo.Checked Then 			ls_header += " GPO"
If cbx_reservation.Checked Then 	ls_header += " Reservation"

ls_footer = "&l" + String(Today(), "mmm dd") + "-" + &
				String(RelativeDate(Today(), 6), "mmm dd") + "     " + &
				String(dw_weekly_summary.GetItemNumber(1, "week1")) + &
				"~r~n" + String(RelativeDate(Today(), 23 - DayNumber(Today()) ), "mmm dd") + &
				"-" + String(RelativeDate(Today(), 29 - DayNumber(Today()) ), "mmm dd")  + &
				"     " + String(dw_weekly_summary.GetItemNumber(1, "week4")) + &
				"&cBoxes Sold~r~n" + &
				String(RelativeDate(Today(), 9 - DayNumber(Today()) ), "mmm dd") + &
				"-" + String(RelativeDate(Today(), 15 - DayNumber(Today()) ), "mmm dd") + &
				"     " + String(dw_weekly_summary.GetItemNumber(1, "week2")) + &
				"~r~n" + String(RelativeDate(Today(), 30 - DayNumber(Today()) ), "mmm dd") + &
				"-" + String(RelativeDate(Today(), 36 - DayNumber(Today()) ), "mmm dd") + &
				"     " + String(dw_weekly_summary.GetItemNumber(1, "week5")) + &
				"&r" + String(RelativeDate(Today(), 16 - DayNumber(Today()) ), "mmm dd") + &
				"-" + String(RelativeDate(Today(), 22 - DayNumber(Today()) ), "mmm dd") + &
				"     " + String(dw_weekly_summary.GetItemNumber(1, "week3")) + &
				"~r~n" + String(RelativeDate(Today(), 37 - DayNumber(Today()) ), "mmm dd") + &
				"-" + String(RelativeDate(Today(), 43 - DayNumber(Today()) ), "mmm dd") + &
				"     " + String(dw_weekly_summary.GetItemNumber(1, "week6"))

ole_1.Object.PrintTopMargin = 1.25
ole_1.Object.PrintHeader = ls_header
ole_1.Object.PrintFooter = ls_footer
ole_1.Object.PrintColHeading = True
ole_1.Object.PrintArea = "$A$3:$W$51"
ole_1.Object.FilePrint(False)


end event

type dw_division from u_division within w_invt_vs_sales
integer x = 69
integer y = 16
integer taborder = 20
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()
end event

type cbx_combined from checkbox within w_invt_vs_sales
integer x = 1733
integer y = 8
integer width = 347
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Combined"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(80, 1, 'Y')
	dw_weekly_summary.SetItem(1, "comb_cbx", 'Y')
Else
	ole_1.Object.TextRC(80, 1, 'N')
	dw_weekly_summary.SetItem(1, "comb_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type cbx_uncombined from checkbox within w_invt_vs_sales
integer x = 2139
integer y = 8
integer width = 507
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Uncombined"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(79, 1, 'Y')
	dw_weekly_summary.SetItem(1, "uncomb_cbx", 'Y')
Else
	ole_1.Object.TextRC(79, 1, 'N')
	dw_weekly_summary.SetItem(1, "uncomb_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type cbx_gpo from checkbox within w_invt_vs_sales
integer x = 1733
integer y = 92
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "GPO"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(81, 1, 'Y')
	dw_weekly_summary.SetItem(1, "gpo_cbx", 'Y')
Else
	ole_1.Object.TextRC(81, 1, 'N')
	dw_weekly_summary.SetItem(1, "gpo_cbx", 'N')
End if
ole_1.Object.Recalc()
end event

type cbx_reservation from checkbox within w_invt_vs_sales
integer x = 2139
integer y = 92
integer width = 475
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Reservation"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(82, 1, 'Y')
	dw_weekly_summary.SetItem(1, "reserv_cbx", 'Y')
Else
	ole_1.Object.TextRC(82, 1, 'N')
	dw_weekly_summary.SetItem(1, "reserv_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type dw_weekly_summary from u_netwise_dw within w_invt_vs_sales
integer x = 320
integer y = 1212
integer width = 2427
integer height = 164
integer taborder = 10
string dataobject = "d_invt_vs_sales_summary"
boolean border = false
end type

on constructor;call u_netwise_dw::constructor;ib_updateable = False

This.InsertRow(0)
end on

type st_boxessold from statictext within w_invt_vs_sales
integer y = 1216
integer width = 357
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Boxes Sold :"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_invt_vs_sales
integer x = 201
integer y = 112
integer width = 315
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Last Update"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_last_update from singlelineedit within w_invt_vs_sales
integer x = 535
integer y = 108
integer width = 581
integer height = 72
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string pointer = "Arrow!"
long textcolor = 33554432
long backcolor = 12632256
boolean autohscroll = false
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type ole_1 from olecustomcontrol within w_invt_vs_sales
event click ( long nrow,  long ncol )
event dblclick ( long nrow,  long ncol )
event canceledit ( )
event selchange ( )
event startedit ( ref string editstring,  ref integer ab_cancel )
event endedit ( ref string editstring,  ref integer ab_cancel )
event startrecalc ( )
event endrecalc ( )
event topleftchanged ( )
event objclick ( ref string objname,  long objid )
event objdblclick ( ref string objname,  long objid )
event rclick ( long nrow,  long ncol )
event rdblclick ( long nrow,  long ncol )
event objvaluechanged ( ref string objname,  long objid )
event modified ( )
event mousedown ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mouseup ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mousemove ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event objgotfocus ( ref string objname,  long objid )
event objlostfocus ( ref string objname,  long objid )
event validationfailed ( ref string pentry,  long nsheet,  long nrow,  long ncol,  ref string pshowmessage,  ref integer paction )
event keypress ( ref integer keyascii )
event keydown ( ref integer keycode,  integer shift )
event keyup ( ref integer keycode,  integer shift )
integer x = 27
integer y = 196
integer width = 2825
integer height = 984
integer taborder = 31
boolean bringtotop = true
long backcolor = 0
string binarykey = "w_invt_vs_sales.win"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Dw_invt_vs_sales.bin 
2C00001800e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000007fffffffe00000004000000050000000600000008fffffffe000000090000000afffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000002675471001c2ad0f0000000300000c800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000540000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000020000067900000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004042badc511ce5e58415210b601004853000000002675471001c2ad0f2675471001c2ad0f00000000000000000000000000000001fffffffe000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f000000100000001100000012000000130000001400000015000000160000001700000018000000190000001a0000001bfffffffe0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002f0000003000000031fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
2Bffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f004300790070006900720068006700200074006300280020002900390031003500390056002000730069006100750020006c006f00430070006d006e006f006e0065007300740020002c006e0049002e006300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fffe00020005042badc511ce5e58415210b60100485300000001fb8f0821101b01640008ed8413c72e2b000000300000064900000008000001000000004800000101000000500000010200000058000001030000006000000104000000680000010500000070000001060000007c00000000000005c000000003000100000000000300003fdf000000030000196d00000003000000600000000200000001000000080000000100000000000000410000053b00090000ee000505000000040014ef000000000000000000ffffff00ffffffff3dfff3ff00000012000000000000380001000000310258000000c814907fff00000000010500000061697241c814316cff0000000002bc7f0000000072410500316c61690200c814907fff00000000010500000061697241c814316cff0002000002bc7f0000000072410500316c61690000c814907fff00000000010500000061697241051a1e6c24221700232c2322295f302322285c3b2c2322245c302323061f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c201e295c221d00072c2322242e302323295f303022285c3b2c2322242e302323295c30300008251e2224222223232c2330302e305b3b295f5d6465522422285c232c2322302e30231e295c3032002a352422285f23202a223023232c5f3b295f22242228285c202a23232c233b295c302422285f22202a22295f222d40285f3b2c1e295f5f29002923202a283023232c5f3b295f5c202a28232c2328295c30232a285f3b222d22205f3b295f295f4028002c3d1e22285f3a202a222423232c2330302e305f3b295f22242228285c202a23232c2330302e305f3b295c222422282d22202a5f3f3f22285f3b291e295f4031002b34202a285f23232c2330302e305f3b295f5c202a28232c2328302e30233b295c30202a285f3f222d223b295f3f5f40285f321f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c251e295c222200332c2322242e302323295f303065525b3b285c5d64232224223023232c5c30302e0005ed2900000000000003ec0014e000f5000000c00020ff0000002000000000e00000000000011420fff5000020c0c400000000000000000114e000f5000000c0c420ff0000002000000000e00000000000021420fff5000020c0c400000000000000000214e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e000000000000014200001000020c00000000000000000000514e000f5003300c0c820ff0000002000000000e00000003200051420fff5000020c0c800000000000000000514e000f5000c00c0c820ff0000002000000000e00000000a00051420fff5000020c0c800000000000000000514e000f5000d00c0c820ff0000002000000000e00000000000041412fff0000020c04800000000000000000009850068530600317465650500090a010d001000640c0010000011d2f1a9fc3f50624d2a00015f012b000000012500018c00ff81000100031404c11541260261500708262065670000835026000084000000003fe8000000000027e80000000000283f0000000000293ff000000000a13ff0006400012201000100060001000000000000000000e00000000000003fe00000000000013f000000000000000018f70000ccff009200ffffff00c0c0c03fff00ff0000000000000000000010f200000000000000000000000015f6ffff150015004000ff00030f1d060000000000010000000000000a3e0000000002360000000064a0000099006400000a092600000008000000000000000100010600000009006c6966006d616e6501030065000c0000735f00006b636f74706f727001040073000c00006f620000726564726c7974730101006500090000655f00006e65747802007874090000015f000000657478650079746e00000105000000086e70706100656d6100000100000000097265765f6e6f697300000000000000000001000000003fdf0000196d00000060000100010101010101010101010101010101010100053b000900000000050500000004ee14ef00000000000000000000ffff0000fffffffffff3ffff0000123d0000000000380000000000000258000100c814317fff000000000190000000006972410514316c61000000c802bc7fff00000000410500006c61697200c814317fff0002
2800000190000000006972410514316c61000200c802bc7fff00000000410500006c61697200c814317fff00000000019000000000697241051a1e6c61221700052c2322245f302323285c3b29232224223023232c1f1e295c221c00062c2322245f302323525b3b295c5d6465006f00430074006e006e00650073007400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000001c00000568000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002224222823232c231e295c301d000720232224223023232c5f30302e285c3b29232224223023232c5c30302e08251e2924222200232c2322302e30233b295f306465525b22285c5d2c2322242e302323295c3030002a351e22285f32202a222423232c233b295f302422285f5c202a22232c2328295c302322285f3b202a22245f222d22285f3b291e295f402900292c202a285f23232c233b295f30202a285f2c23285c5c302323285f3b292d22202a3b295f225f40285f2c3d1e29285f3a002a222422232c2320302e30233b295f302422285f5c202a22232c2328302e30233b295c302422285f22202a223f3f222d5f3b295f295f4028002b341e2a285f31232c2320302e30233b295f30202a285f2c23285c2e302323295c30302a285f3b222d2220295f3f3f40285f3b1f1e295f221c00322c2322245f302323525b3b295c5d64652224222823232c231e295c3022003325232224223023232c5f30302e525b3b295c5d64652224222823232c2330302e3005ed295c000000000003ec0014e00000000000000020fff5000020c00000000000000000000114e0fff5000020c0c420000000000000000014e0000000000001c420fff5000020c00000000000000000000214e0fff5000020c0c420000000000000000014e0000000000002c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e00001000020c00020000000000000000014e0000000330005c820fff5000020c00000000000000000000514e0fff5003220c0c820000000000000000014e00000000c0005c820fff5000020c00000000000000000000514e0fff5000a20c0c820000000000000000014e00000000d0005c820fff5000020c00000000000000000000414e0fff0000020c04812000000000000000009850000530600007465656800090a310d001005640c000100001100f1a9fc1050624dd200015f3f2b00002a012500018c00ff00000100011404c181412602035007081520656761008350260000840000000026e80000000000273f0000000000283fe800000000293ff000000000003ff00000000122a1000100640001000100000006000000000000000000003fe00000000000013fe00000000000000000f7000000ff009218ffffffccc0c0c000ff00ff000000003f000000000010f200000000000000000000000000f6ffff000015001500ff00150f1d06400000000301000000000000003e0000000002360a00000000a0000000006400640a09269901ff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Dw_invt_vs_sales.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
