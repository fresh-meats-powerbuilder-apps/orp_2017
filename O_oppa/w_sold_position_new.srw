HA$PBExportHeader$w_sold_position_new.srw
$PBExportComments$Sold Postion Window
forward
global type w_sold_position_new from w_netwise_sheet
end type
type dw_sales_options_new from datawindow within w_sold_position_new
end type
type dw_division from u_division within w_sold_position_new
end type
type dw_sold_position_detail from u_netwise_dw within w_sold_position_new
end type
type dw_plant_type from u_plant_type within w_sold_position_new
end type
type dw_country_1 from u_country_1 within w_sold_position_new
end type
end forward

global type w_sold_position_new from w_netwise_sheet
integer x = 0
integer y = 0
integer width = 2912
integer height = 1896
string title = "Sold Position - NEW"
long backcolor = 12632256
dw_sales_options_new dw_sales_options_new
dw_division dw_division
dw_sold_position_detail dw_sold_position_detail
dw_plant_type dw_plant_type
dw_country_1 dw_country_1
end type
global w_sold_position_new w_sold_position_new

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
u_pas201		iu_pas201
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Long		il_color

Long		il_SelectedColor

Long		il_SelectedTextColor

Boolean		ib_inquire, &
		ib_reinquire

Integer		ii_max_page_number

string		is_inquire_window_name

Datastore		ids_report

w_netwise_response	iw_inquirewindow, &
			iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_filter ()
end prototypes

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row

string		ls_division, &  
            ls_division_desc, &
				ls_country, &
				ls_plant_type, &
				ls_weekly_data, &
				ls_header


u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp01hr"
istr_error_info.se_message = Space(71)

ls_division = dw_division.uf_get_division()
ls_division_desc = dw_division.uf_get_division_descr()

ls_country = dw_country_1.uf_get_country()

ls_plant_type = dw_plant_type.uf_get_plant_type()

ls_header = ls_division + '~t' + &
				ls_plant_type + '~t' + &
				ls_country + '~r~n' 

dw_sold_position_detail.reset()

										
If iu_ws_pas_share.nf_pasp01hr(istr_error_info, & 
										ls_header, &
										ls_weekly_data) < 0 Then
										This.SetRedraw(True) 
										Return False										
										
										
End If														

 
//ls_weekly_data = 'T~tTarget~t1245~t1345~t1415~t1455~t1480~t688~t811~t936~t1039~t1147~t220~t180~t1115~r~n' + &
//						'C~t08/01/1998~t10~t20~t30~t40~t50~t60~t70~t80~t90~t100~t220~t180~t100~r~n' + &
//						'U~t08/01/1998~t10~t20~t30~t40~t50~t60~t70~t80~t90~t100~t220~t180~t100~r~n' + &
//						'R~t08/01/1998~t10~t20~t30~t40~t50~t60~t70~t80~t90~t100~t220~t180~t100~r~n' + &
//						'G~t08/01/1998~t10~t20~t30~t40~t50~t60~t70~t80~t90~t100~t220~t180~t100~r~n' + &
//						'C~t08/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t100~r~n' + &
//						'U~t08/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t100~r~n' + &
//						'R~t08/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t100~r~n' + &
//						'G~t08/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t100~r~n' + &
//						'C~t08/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/24/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/24/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/24/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/24/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/25/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/25/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/25/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/25/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/26/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/26/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/26/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/26/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/27/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/27/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/27/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/27/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/28/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/28/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/28/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/28/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/29/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/29/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/29/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/29/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/30/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/30/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/30/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/30/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t08/31/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t08/31/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t08/31/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t08/31/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/01/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/01/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/01/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/01/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/02/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/03/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/04/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/05/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/06/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/07/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/08/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/09/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/10/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/11/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/12/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/13/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/14/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/15/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/16/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/17/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/18/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/19/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/20/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/21/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/22/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'C~t09/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'U~t09/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'R~t09/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' + &
//						'G~t09/23/1998~t1~t2~t3~t4~t5~t6~t7~t8~t9~t10~t220~t180~t200~r~n' 
			  
//MessageBox('Return Data',string(ls_weekly_data))

If Not lu_string.nf_IsEmpty(ls_weekly_data) Then
	ll_rec_count = dw_sold_position_detail.ImportString(ls_weekly_Data) / 4
	If ll_rec_count > 0 Then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

              
FOR ll_row = 1 TO ll_rec_count * 4 + 1
	dw_sold_position_detail.SetItem ( ll_row, "back_color", il_selectedcolor)
	dw_sold_position_detail.SetItem ( ll_row, "text_color", il_selectedtextcolor)
NEXT 

Post wf_filter()

This.SetRedraw(True) 

dw_sold_position_detail.ResetUpdate()
dw_sold_position_detail.SetFocus()
 
Return True

end function

public subroutine wf_filter ();Integer		li_counter

String		ls_filter, &
				ls_row_message
Boolean		lb_first				

iw_frame.SetMicroHelp('Wait... filtering data')

dw_sold_position_detail.SetRedraw(False)

dw_sold_position_detail.SetFilter("")

//new JAC
//If dw_sales_options_new.GetItemString(1,'fresh_ind') = 'T'  Then
//     if dw_sales_options_new.GetItemString(1,'frozen_ind') = 'T' Then
// //       MessageBox(" ", "Fresh or Frozen Only, Not Both can be chosen", StopSign!)
//		  iw_Frame.SetMicroHelp("Please enter Fresh or Frozen Only, Both cannot be chosen")
////		This.SetRedraw(False)
////		dw_rmt_detail.ScrollToRow(al_row)
////		dw_rmt_detail.SetColumn(ls_qty_column)
////		dw_rmt_detail.SetFocus()
//		This.SetRedraw(True)
//		Return 
//     end if 
//End If

If dw_sales_options_new.GetItemString(1,'fresh_ind') = 'F'  Then
     if dw_sales_options_new.GetItemString(1,'frozen_ind') = 'F' Then
		  iw_Frame.SetMicroHelp("Either Fresh or Frozen are required")
		This.SetRedraw(True)
		Return 
     end if 
End If       

//If dw_sales_options_new.GetItemString(1,'boxes_ind') = 'T'  Then
  //   if dw_sales_options_new.GetItemString(1,'combos_ind') = 'T' Then
    //    MessageBox(" ", "Boxes or Combos Only, Not Both can be chosen", StopSign!)
		//  iw_Frame.SetMicroHelp("Please enter Boxes or Combos Only, Both cannot be chosen")
        //This.SetRedraw(True)
		 // Return      
   //  end if 
//End If 

If dw_sales_options_new.GetItemString(1,'boxes_ind') = 'F'  Then
     if dw_sales_options_new.GetItemString(1,'combos_ind') = 'F' Then
		  iw_Frame.SetMicroHelp("Either Boxes or Combos are required")
        This.SetRedraw(True)
		  Return      
     end if 
End If 

//// Build Filter
//If dw_sales_options_new.GetItemString(1,'frozen_ind') = 'F'  Then
//	ls_filter = "sales_options_ind <> '2'"  
//	lb_first = True
//End If
//
//If dw_sales_options_new.GetItemString(1,'fresh_ind') = 'F' Then
//	If lb_first Then
//		ls_filter += " and sales_options_ind <> '1'"
//	Else
//		ls_filter += "sales_options_ind <> '1'"
//		lb_first = True
//	End If
//End If					 
//					 
//If dw_sales_options_new.GetItemString(1,'combos_ind') = 'F'  Then
//   If lb_first Then
//		ls_filter += " and sales_options_ind <> 'C'"  
//	Else
//		ls_filter += "sales_options_ind <> 'C'"
//		lb_first = True
//	End If
//End If
//
//If dw_sales_options_new.GetItemString(1,'boxes_ind') = 'F'  Then
//	   If lb_first Then
//			ls_filter += " and sales_options_ind <> 'B'"  
//		else
//   		ls_filter += "sales_options_ind <> 'B'" 
//				lb_first = True
//		End If
//End If
//new jac 7-13
//If dw_sales_options_new.GetItemString(1,'fresh_ind') = 'T'  Then
//	if dw_sales_options_new.GetItemString(1,'boxes_ind') = 'T'Then
//		 ls_filter = "sales_options_ind = '1'" 
//	else 
//		 ls_filter = "sales_options_ind = '3'"
//	End if	
//   lb_first = True	
//else	
// //  If lb_first Then
// //     ls_filter += " and sales_options_ind <> '2' and sales_options_ind <> '4'"
////	else
////			ls_filter += "sales_options_ind <> '2' and sales_options_ind <> '4'"
////			lb_first = True
////		end if
//	if dw_sales_options_new.GetItemString(1,'boxes_ind') = 'T'Then
//		 ls_filter = "sales_options_ind = '2'" 
//	else 
//		 ls_filter = "sales_options_ind = '4'"
//	End if	
////	else
 //   If dw_sales_options_new.GetItemString(1,'boxes_ind') = 'F' Then
 //      If lb_first Then
	//       ls_filter = " and sales_options_ind <> '1' and sales_options_ind <> '2'"
//		else 
//			 ls_filter = "sales_options_ind <> '1' and sales_options_ind <> '2'"
//		end if
//    Else
//       If dw_sales_options_new.GetItemString(1,'combos_ind') = 'F' Then
//          If lb_first Then
//	          ls_filter = " and sales_options_ind <> '3' and sales_options_ind <> '4'"
//			 else
//				 ls_filter = "sales_options_ind <> '3' and sales_options_ind <> '4'"
//			 end if
//		 end if 
//	  end if
//	 end if 
//End If                  
If dw_sales_options_new.GetItemString(1,'fresh_ind') = 'F'  Then
   ls_filter = "sales_options_ind <> '1' and sales_options_ind <> '3'"
	if dw_sales_options_new.GetItemString(1,'boxes_ind') = 'F'Then
		 ls_filter += " and sales_options_ind <> '2'"
	End if
	if dw_sales_options_new.GetItemString(1,'combos_ind') = 'F'Then
		 ls_filter += " and sales_options_ind <> '4'"
	End if
Else
    If dw_sales_options_new.GetItemString(1,'frozen_ind') = 'F' Then
       ls_filter = "sales_options_ind <> '2' and sales_options_ind <> '4'" 
	    if dw_sales_options_new.GetItemString(1,'boxes_ind') = 'F'Then
	   	 ls_filter += " and sales_options_ind <> '1'"
   	 End if	
		 if dw_sales_options_new.GetItemString(1,'combos_ind') = 'F'Then
		    ls_filter += " and sales_options_ind <> '3'"
	    End if
    Else                                                                                                                                                     
     If dw_sales_options_new.GetItemString(1,'boxes_ind') = 'F' Then
      	ls_filter = "sales_options_ind <> '1' and sales_options_ind <> '2'"
     Else
       If dw_sales_options_new.GetItemString(1,'combos_ind') = 'F' Then
	      ls_filter = "sales_options_ind <> '3' and sales_options_ind <> '4'"
       End If                  
     End If                                                                    
    End If                                                                    
End If                                                                    




dw_sold_position_detail.SetFilter(ls_filter)	
dw_sold_position_detail.Filter()
dw_sold_position_detail.Sort()
dw_sold_position_detail.GroupCalc()

// Set the highlighting again.
dw_sold_position_detail.SetItem(1,"back_color", il_SelectedColor)
dw_sold_position_detail.SetItem(1,"text_color", il_SelectedTextColor)

iw_frame.SetMicroHelp('Ready')
dw_sold_position_detail.SetRedraw(True)

Return
end subroutine

on w_sold_position_new.create
int iCurrent
call super::create
this.dw_sales_options_new=create dw_sales_options_new
this.dw_division=create dw_division
this.dw_sold_position_detail=create dw_sold_position_detail
this.dw_plant_type=create dw_plant_type
this.dw_country_1=create dw_country_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_options_new
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_sold_position_detail
this.Control[iCurrent+4]=this.dw_plant_type
this.Control[iCurrent+5]=this.dw_country_1
end on

on w_sold_position_new.destroy
call super::destroy
destroy(this.dw_sales_options_new)
destroy(this.dw_division)
destroy(this.dw_sold_position_detail)
destroy(this.dw_plant_type)
destroy(this.dw_country_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')

iw_frame.im_menu.mf_enable('m_print')


end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')




end event

event resize;call super::resize;constant integer li_x		= 20
constant integer li_y		= 340
  
dw_sold_position_detail.width	= newwidth - li_x
dw_sold_position_detail.height	= newheight - li_y

// Must do because of the split horizonal bar 
dw_sold_position_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_sold_position_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_sold_position_detail.Object.DataWindow.HorizontalScrollSplit = '89'
dw_sold_position_detail.Object.DataWindow.HorizontalScrollPosition2 =  '89'


end event

event ue_fileprint;call super::ue_fileprint;String						ls_mod, &
								ls_temp, &
								ls_week_end_date

DataStore					lds_print


lds_print = create u_print_datastore
lds_print.DataObject = 'd_sold_position_report_new'

//Set the text to visible if the options are checked for the sales options
If dw_sales_options_new.GetItemString(1,'frozen_ind') = 'T' Then
	lds_print.object.frozen_t.visible = 1
End If

If dw_sales_options_new.GetItemString(1,'fresh_ind') = 'T' Then
	lds_print.object.fresh_t.visible = 1
End If

If dw_sales_options_new.GetItemString(1,'boxes_ind') = 'T' Then
	lds_print.object.boxes_t.visible = 1
End If

If dw_sales_options_new.GetItemString(1,'combos_ind') = 'T' Then
	lds_print.object.combos_t.visible = 1
End If

//Build a string to place on the printed report.
lds_print.object.inquire_t.text = dw_country_1.uf_get_country_desc() &
	+ ' - ' &
	+ dw_plant_type.uf_get_desc()&
	+ ' - '&
	+ 'DIVISION '&
	+ dw_division.uf_get_division()

dw_sold_position_detail.sharedata(lds_print)

ls_temp = lds_print.modify(ls_mod)

lds_print.print()




end event

event ue_get_data;Choose Case as_value
	Case 'division'
		message.StringParm = dw_division.uf_get_division()
	Case 'plant_type'
		message.StringParm = dw_plant_type.uf_get_plant_type()
 	Case 'country'
		message.StringParm = dw_country_1.uf_get_country()
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env


GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255
	il_SelectedTextColor = 0
End if

//ib_reinquire = FALSE

This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "soldpos"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_sold_position_inq_new'

iu_pas201 = Create u_pas201
iu_ws_pas_share = Create u_ws_pas_share

wf_retrieve()

end event

event ue_set_data;u_conversion_functions		lu_conv


Choose Case as_data_item
	Case 'division'
		dw_division.uf_set_division(as_value)
	Case 'plant_type'
		dw_plant_type.uf_set_plant_type(as_value)
	Case 'country'
		dw_country_1.uf_set_country(as_value)
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)
End Choose
end event

event close;call super::close;Destroy iu_ws_pas_share
end event

type dw_sales_options_new from datawindow within w_sold_position_new
integer x = 1673
integer width = 1184
integer height = 472
integer taborder = 20
string title = "none"
string dataobject = "d_sales_options_new"
boolean border = false
boolean livescroll = true
end type

event constructor;This.InsertRow(0)
end event

event itemchanged;Parent.Post wf_filter()
end event

type dw_division from u_division within w_sold_position_new
integer x = 69
integer y = 24
integer taborder = 10
boolean enabled = false
end type

event constructor;call super::constructor;This.disable()
end event

type dw_sold_position_detail from u_netwise_dw within w_sold_position_new
integer x = 9
integer y = 328
integer width = 2843
integer height = 1432
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_sold_position_detail_new"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = False
end event

type dw_plant_type from u_plant_type within w_sold_position_new
integer y = 204
integer height = 88
integer taborder = 20
boolean bringtotop = true
boolean enabled = false
end type

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(FALSE)
end event

type dw_country_1 from u_country_1 within w_sold_position_new
integer x = 82
integer y = 112
integer taborder = 30
boolean bringtotop = true
boolean enabled = false
boolean livescroll = false
end type

event constructor;call super::constructor;This.uf_Enable(FALSE)
end event

