HA$PBExportHeader$w_change_order_production_dates_inq.srw
forward
global type w_change_order_production_dates_inq from w_netwise_response
end type
type st_1 from statictext within w_change_order_production_dates_inq
end type
type sle_1 from u_netwise_singlelineedit within w_change_order_production_dates_inq
end type
end forward

global type w_change_order_production_dates_inq from w_netwise_response
integer x = 1075
integer y = 485
integer width = 1531
integer height = 388
string title = "Change Order Inquire"
long backcolor = 67108864
st_1 st_1
sle_1 sle_1
end type
global w_change_order_production_dates_inq w_change_order_production_dates_inq

type variables
Boolean			ib_ok_to_close, &
			ib_inquire

w_weekly_pa		iw_parentwindow
end variables

on w_change_order_production_dates_inq.create
int iCurrent
call super::create
this.st_1=create st_1
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.sle_1
end on

on w_change_order_production_dates_inq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.sle_1)
end on

event open;call super::open;If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
		This.Title = iw_parentwindow.Title + " Inquire"
	End If
End If
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('order_number')
sle_1.text = Message.StringParm

sle_1.SetFocus()


end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;u_string_functions		lu_strings

If lu_strings.nf_IsEmpty(sle_1.text) Then
	iw_frame.SetMicroHelp("Order Number is a required field")
	sle_1.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('order_number', sle_1.text)

ib_ok_to_close = True

Close(This)
            

end event

type cb_base_help from w_netwise_response`cb_base_help within w_change_order_production_dates_inq
boolean visible = false
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_change_order_production_dates_inq
integer x = 1211
integer y = 152
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_change_order_production_dates_inq
integer x = 1211
integer y = 28
end type

type st_1 from statictext within w_change_order_production_dates_inq
integer x = 183
integer y = 32
integer width = 343
integer height = 84
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Order Number:"
boolean focusrectangle = false
end type

type sle_1 from u_netwise_singlelineedit within w_change_order_production_dates_inq
integer x = 549
integer width = 320
integer height = 88
integer taborder = 11
boolean bringtotop = true
integer textsize = -8
string facename = "MS Sans Serif"
textcase textcase = upper!
integer limit = 7
end type

