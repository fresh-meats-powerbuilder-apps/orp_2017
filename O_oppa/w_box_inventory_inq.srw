HA$PBExportHeader$w_box_inventory_inq.srw
forward
global type w_box_inventory_inq from w_netwise_response
end type
type dw_division from u_division within w_box_inventory_inq
end type
type dw_uom_code from u_uom_code within w_box_inventory_inq
end type
end forward

global type w_box_inventory_inq from w_netwise_response
integer x = 151
integer y = 460
integer width = 1755
integer height = 456
long backcolor = 67108864
dw_division dw_division
dw_uom_code dw_uom_code
end type
global w_box_inventory_inq w_box_inventory_inq

type variables
Boolean			ib_valid_to_close

w_invt_vs_sales		iw_parent

end variables

event open;call super::open;String				ls_division


iw_parent = Message.PowerObjectParm


This.Title = iw_parent.Title + " Inquire"

dw_division.SetFocus()


end event

on close;call w_netwise_response::close;If Not ib_valid_to_close Then
	Message.StringParm = ""
End if
end on

on w_box_inventory_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_uom_code=create dw_uom_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_uom_code
end on

on w_box_inventory_inq.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_uom_code)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok();call super::ue_base_ok;String					ls_division, &
							ls_uom_code
		
u_String_Functions		lu_string_functions

If dw_division.AcceptText() = -1 Then 
	dw_division.SetFocus()
	return
End if
If dw_uom_code.AcceptText() = -1 Then
	dw_uom_code.SetFocus()
	Return
End IF

ls_division = dw_division.uf_get_division()
If lu_string_functions.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End if

ls_uom_code = dw_uom_code.uf_get_uom_code()
If lu_string_functions.nf_IsEmpty(ls_uom_code) Then
	iw_frame.SetMicroHelp("UOM Code is a required field")
	dw_uom_code.SetFocus()
	return
End if

iw_parent.event ue_set_data("Division Code", ls_division)
iw_parent.event ue_set_data("UOM Code", ls_uom_code)

ib_valid_to_close = True
CloseWithReturn(This, "True")
end event

type cb_base_help from w_netwise_response`cb_base_help within w_box_inventory_inq
integer x = 1381
integer y = 232
integer taborder = 50
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_box_inventory_inq
integer x = 1083
integer y = 232
integer taborder = 40
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_box_inventory_inq
integer x = 786
integer y = 232
integer taborder = 30
end type

type dw_division from u_division within w_box_inventory_inq
integer x = 78
integer y = 4
integer taborder = 10
end type

type dw_uom_code from u_uom_code within w_box_inventory_inq
integer y = 92
integer width = 814
integer height = 104
integer taborder = 20
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(true)
end event

