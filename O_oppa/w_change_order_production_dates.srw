HA$PBExportHeader$w_change_order_production_dates.srw
$PBExportComments$Change Order Production Dates
forward
global type w_change_order_production_dates from w_netwise_sheet
end type
type ole_chg_order_prod_dates from olecustomcontrol within w_change_order_production_dates
end type
end forward

global type w_change_order_production_dates from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 2875
integer height = 1656
string title = "Change Order"
long backcolor = 79741120
ole_chg_order_prod_dates ole_chg_order_prod_dates
end type
global w_change_order_production_dates w_change_order_production_dates

type variables
s_error		istr_error_info
string		Is_order_num
boolean		ib_OLE_Error 
end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_update ();boolean lbln_Return

if ib_OLE_Error then return TRUE

SetPointer(HourGlass!)

lbln_Return = ole_chg_order_prod_dates.object.Save(true)

SetPointer(Arrow!)

return lbln_Return
end function

public function boolean wf_retrieve ();boolean lbln_Return

if ib_OLE_Error then return TRUE

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_chg_order_prod_dates.Inquire"


if ole_chg_order_prod_dates.object.DataChanged  AND gw_base_frame.im_base_menu.m_file.m_save.enabled then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)

		CASE 1	// Save Changes
			If ole_chg_order_prod_dates.object.save(false) = FALSE Then
				return TRUE
			End If
		CASE 2	// Do not save changes

		CASE 3	// Cancel the closing of window
				return TRUE
	END CHOOSE
end if

SetPointer(HourGlass!)
If Len(trim(is_order_num)) > 0 Then
// Is_****_**** can then be broken down into the appropriate Variables to Call the ocx controls(in this example all I needed was a Order Number)
	lbln_Return = ole_chg_order_prod_dates.object.Inquire(False, is_order_num )
	// always make sure you clear the Instance Variable for future inquires
	is_order_num = ''                                                                        
else	
	lbln_Return = ole_chg_order_prod_dates.object.Inquire(true, " ")
End if
SetPointer(Arrow!)

return lbln_Return
end function

event activate;call super::activate;boolean	lb_rtn
string	ls_classname, &
			ls_Add_Access,&
			ls_Del_Access,&
			ls_Mod_Access,&
			ls_Inq_Access

// Set Security on ActiveX Control.
ls_classname = UPPER(this.ClassName()) + "_EXT"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
					
IF NOT lb_rtn THEN
	// if the window is not listed in the security at all, then allow no permissions
	ls_Add_Access =  'N'
End if

//Rights to Add New Rows
IF UPPER(ls_Add_Access) = 'N' THEN
	iw_frame.im_menu.mf_Disable('m_addrow')
END IF	

iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

iw_frame.im_menu.mf_Disable('m_generatesales')		
iw_frame.im_menu.mf_Disable('m_complete')		



ole_chg_order_prod_dates.object.SetFocus()
	
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_new')	
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

iw_frame.im_menu.mf_Disable('m_generatesales')		
iw_frame.im_menu.mf_Disable('m_complete')		

end event

event ue_postopen;call super::ue_postopen;boolean	lb_rtn
string	ls_server_suffix, &
			ls_classname, &
			ls_Add_Access,&
			ls_Del_Access,&
			ls_Mod_Access,&
			ls_Inq_Access

if ib_OLE_Error then return

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= This.Title
istr_error_info.se_user_id 		= sqlca.userid

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

if ls_server_suffix = "" then
	ls_server_suffix = " "
end if 


ole_chg_order_prod_dates.object.UserID = sqlca.userid //User ID
ole_chg_order_prod_dates.object.Password = sqlca.DBpass //User Password
ole_chg_order_prod_dates.object.SighOnSystem = ls_server_suffix //Mainframe Region (t,z,p)
ole_chg_order_prod_dates.object.Application = Message.nf_Get_App_ID() //Application Name
ole_chg_order_prod_dates.object.Window = This.Title  //WIndow Name


// Set Security on ActiveX Control.
ls_classname = UPPER(this.ClassName()) + "_EXT"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
					
IF NOT lb_rtn THEN
	// if the window is not listed in the security at all, then allow no permissions
	ls_Add_Access =  'N'
	ls_Mod_Access =  'N'
	ls_Inq_Access =  'N'
End if

//Rights to Add New Rows
IF UPPER(ls_Add_Access) = 'N' THEN
	ole_chg_order_prod_dates.object.AddRights = false
ELSE
	ole_chg_order_prod_dates.object.AddRights = true		
END IF

//Rights to Change Dates
IF UPPER(ls_Inq_Access) = 'N' THEN
	ole_chg_order_prod_dates.object.DateRights = false
ELSE
	ole_chg_order_prod_dates.object.DateRights = true
END IF

//Rights to change Quanties
IF UPPER(ls_Mod_Access) = 'N' THEN
	ole_chg_order_prod_dates.object.QtyRights = false
ELSE
	ole_chg_order_prod_dates.object.QtyRights = true
END IF

if this.width  > 10 then
	ole_chg_order_prod_dates.Width = this.width - 50
end if

if this.height > 10 then
	ole_chg_order_prod_dates.Height = this.height - 150
end if


//Inquire
wf_retrieve()
end event

on w_change_order_production_dates.create
int iCurrent
call super::create
this.ole_chg_order_prod_dates=create ole_chg_order_prod_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_chg_order_prod_dates
end on

on w_change_order_production_dates.destroy
call super::destroy
destroy(this.ole_chg_order_prod_dates)
end on

event resize;if newheight > 10 then
	ole_chg_order_prod_dates.Height = newheight 
end if

if newwidth> 10 then
	ole_chg_order_prod_dates.width = newwidth 
end if

//ole_chg_order_prod_dates.object.refresh
//ole_chg_order_prod_dates.setredraw(true)
//this.setredraw(true)
//wf_auto_hscrollbar()
end event

event closequery;integer intPromptOnSave

//gets the settings values

gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(intPromptOnSave)
IF gw_base_frame.ib_exit and intPromptOnSave = 0 THEN RETURN

// Do a security check for save and do nothing if the user does not have priviliges
IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN 

if ole_chg_order_prod_dates.object.DataChanged = true then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)

		CASE 1	// Save Changes
			If ole_chg_order_prod_dates.object.save(false) = FALSE Then
				Message.ReturnValue = 1	 // Update failed - do not close window
			End If
		CASE 2	// Do not save changes

		CASE 3	// Cancel the closing of window
			Message.ReturnValue = 1
	END CHOOSE
end if

Return
end event

event open;call super::open;String ls_temp 

ls_temp = Message.StringParm	

If Len(ls_temp) > 0 and lower(ls_temp) <> 'w_change_order_production_dates' and lower(ls_temp) <> 'w_change_order_production_dates,'  Then
	is_order_num = ls_temp
End If

end event

event ue_addrow;call super::ue_addrow;ole_chg_order_prod_dates.object.AddRow()
end event

event ue_filenew;//ole_chg_order_prod_dates.object.AddRow()
end event

type ole_chg_order_prod_dates from olecustomcontrol within w_change_order_production_dates
event messageposted ( ref string strinfomessage )
integer width = 2853
integer height = 1564
integer taborder = 20
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
boolean focusrectangle = false
string binarykey = "w_change_order_production_dates.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)
end event

event getfocus;if ib_OLE_Error then return 
ole_chg_order_prod_dates.object.SetFocus()
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

if ib_OLE_Error  then 
	action = ExceptionIgnore! 
	return
end if

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

if ib_OLE_Error  then 
	action = ExceptionIgnore! 
	return
end if

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
04w_change_order_production_dates.bin 
2100000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff000000030000000000000000000000000000000000000000000000000000000096bdfce001c1eba100000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a0000000200000001000000044ec9906511d51e500100e59fdb9028020000000096bdfce001c1eba196bdfce001c1eba1000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
2Cffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e0065007800740000407e000800034757f20affffffe00065005f00740078006e00650079007400002869000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
14w_change_order_production_dates.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
