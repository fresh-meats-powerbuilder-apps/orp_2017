HA$PBExportHeader$w_prod_status_popup.srw
forward
global type w_prod_status_popup from w_netwise_popup
end type
type dw_product_status_window from datawindow within w_prod_status_popup
end type
end forward

global type w_prod_status_popup from w_netwise_popup
integer y = 485
integer width = 713
integer height = 372
boolean titlebar = false
string title = ""
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = child!
long backcolor = 1090519039
event ue_keydown pbm_dwnkey
dw_product_status_window dw_product_status_window
end type
global w_prod_status_popup w_prod_status_popup

type variables
string		is_division, &
		is_selected_status_array, &
		is_prod_status_table, &
		is_header, &
		is_modified
boolean		ib_header = false, &
		ib_Modified = false, &
		ib_updated
window		iw_parent
datastore		ids_selected_status
long		il_selected_count, &
		il_selected_max = 10
end variables

event ue_keydown;if KeyDown (keytab!) then
	close(this)
end if 
end event

on w_prod_status_popup.create
int iCurrent
call super::create
this.dw_product_status_window=create dw_product_status_window
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_status_window
end on

on w_prod_status_popup.destroy
call super::destroy
destroy(this.dw_product_status_window)
end on

event ue_postopen;Integer					li_xpos, li_ypos
String 					ls_prod_status, &
							ls_Filter_String, &						
							ls_find
u_string_functions	lu_string_function
long						ll_rowcount, ll_count, ll_row, ll_rowcnt, ll_count_max


ids_selected_status = Create DataStore
ids_selected_status.DataObject = 'd_product_status_2'
ll_rowcount = ids_selected_status.importstring(is_selected_status_array)
//test
//messagebox ("ue_postopen", is_selected_status_array)

ll_rowcnt = dw_product_status_window.rowcount()
il_selected_count = 0
ll_count_max = 5
if ll_rowcount > 0 then
	for ll_count = 1 to ll_count_max 
		ls_prod_status = ids_selected_status.getitemstring(1, "product_status" + string(ll_count))
		ls_find = "type_code = '" + ls_prod_status + "'"
		ll_row = dw_product_status_window.find(ls_find,0,ll_rowcnt)
		if ll_row > 0 and il_selected_count < il_selected_max then 
			dw_product_status_window.selectrow(ll_row, true)
			il_selected_count ++
		ELSE
			IF ll_row = 0 then
				ids_selected_status.setitem(1, "product_status" + string(ll_count), '   ')
				ids_selected_status.setitem(1, "status_desc" + string(ll_count), '   ')
				ib_modified = true
			end if
		end if
	next
end if
end event

event open;call super::open;integer 	li_xpos, li_ypos
string  ls_update

iw_parent = Message.PowerObjectParm
ib_updated = true
is_selected_status_array = iw_parent.DYNAMIC event ue_get_data("selected")

is_prod_status_table = iw_parent.DYNAMIC event ue_get_data("product_status")

li_xpos = integer(iw_parent.DYNAMIC event ue_get_data("dddwxpos"))

li_ypos = integer(iw_parent.DYNAMIC event ue_get_data("dddwypos"))

this.move(li_xpos, li_ypos)
dw_product_status_window.SetTransObject(SQLCA)
dw_product_status_window.Retrieve()
//This.SetFocus


end event

event closequery;long	ll_row, ll_count1, ll_count2

//
if ib_modified then is_selected_status_array = ""

ll_row = dw_product_status_window.GetSelectedRow(0)
do while ll_row > 0 and ib_modified
	////test
//messagebox("closequery", "I stopped here")
	is_selected_status_array += dw_product_status_window.getitemstring( ll_row, 'type_code') + "~t" 
	is_selected_status_array += dw_product_status_window.getitemstring( ll_row, 'type_short_desc') + "~t" 
	ll_row = dw_product_status_window.GetSelectedRow(ll_row)
loop
is_selected_status_array += "~r~n"
iw_parent.dynamic event ue_set_data("selected",is_selected_status_array)

if ib_modified then
	is_modified = "true"
else
	is_modified = "false"
end if
iw_parent.dynamic event ue_set_data("modified",is_modified)

if ib_modified then iw_parent.dynamic wf_set_selected_status()

close(this)
end event

event close;close(this)
end event

type dw_product_status_window from datawindow within w_prod_status_popup
event ue_keydown pbm_dwnkey
integer y = 32
integer width = 1170
integer height = 672
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_prod_status_window"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_keydown;if KeyDown (keytab!) then
	close(parent)
end if 
end event

event clicked;
IF Row > 0  THEN

	if this.isselected(row) then
		This.SelectRow(row, false)
		il_selected_count --
		ib_modified = true
	else
		////test
//      messagebox("clicked > 0 ", "il_selected_count = ")
		if il_selected_count < il_selected_max then
			This.SelectRow( row, true)
			il_selected_count ++
			ib_modified = true
		end if
	end if
END IF
end event

event losefocus;//close(parent)
end event

event constructor;InsertRow(0)
end event

