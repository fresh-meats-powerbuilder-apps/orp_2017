HA$PBExportHeader$w_new_graph.srw
forward
global type w_new_graph from w_base_sheet
end type
type tab_1 from tab within w_new_graph
end type
type tabpage_type from userobject within tab_1
end type
type dw_graph_type from datawindow within tabpage_type
end type
type tabpage_category from userobject within tab_1
end type
type dw_category_axis from datawindow within tabpage_category
end type
type tabpage_values from userobject within tab_1
end type
type dw_value_axis from datawindow within tabpage_values
end type
type tabpage_series from userobject within tab_1
end type
type dw_series_axis from datawindow within tabpage_series
end type
type tabpage_data from userobject within tab_1
end type
type dw_graph_data from datawindow within tabpage_data
end type
type tabpage_title from userobject within tab_1
end type
type dw_graph_name from datawindow within tabpage_title
end type
type tabpage_1 from userobject within tab_1
end type
type dw_text from datawindow within tabpage_1
end type
type dw_graph from datawindow within w_new_graph
end type
type st_popup from statictext within w_new_graph
end type
type tabpage_type from userobject within tab_1
dw_graph_type dw_graph_type
end type
type tabpage_category from userobject within tab_1
dw_category_axis dw_category_axis
end type
type tabpage_values from userobject within tab_1
dw_value_axis dw_value_axis
end type
type tabpage_series from userobject within tab_1
dw_series_axis dw_series_axis
end type
type tabpage_data from userobject within tab_1
dw_graph_data dw_graph_data
end type
type tabpage_title from userobject within tab_1
dw_graph_name dw_graph_name
end type
type tabpage_1 from userobject within tab_1
dw_text dw_text
end type
type tab_1 from tab within w_new_graph
tabpage_type tabpage_type
tabpage_category tabpage_category
tabpage_values tabpage_values
tabpage_series tabpage_series
tabpage_data tabpage_data
tabpage_title tabpage_title
tabpage_1 tabpage_1
end type
end forward

global type w_new_graph from w_base_sheet
int X=1
int Y=1
int Width=2926
int Height=1633
boolean TitleBar=true
string Title=""
long BackColor=12632256
WindowState WindowState=maximized!
tab_1 tab_1
dw_graph dw_graph
st_popup st_popup
end type
global w_new_graph w_new_graph

type variables
Integer	ii_win_width, & 
	ii_win_height, &
	ii_win_frame_w, &
	ii_win_frame_h, &
	ii_dw_x, &
	ii_dw_width, &
	ii_dw_height, &
	ii_dw_y

boolean	ib_exec
string	is_graphtype, &
	is_objecttext = 'gr_1.Title.Dispattr', &
	is_currentobject = 'Title'

end variables

forward prototypes
public function integer wf_size_it ()
public function integer wf_resize_it (double ai_sizefactor)
public function integer wf_initialize_data (ref datawindow adw_data)
public function integer wf_initialize_name (ref datawindow adw_data)
public subroutine wf_enable_tabpages (string as_graphtype)
public subroutine wf_print ()
public subroutine wf_initialize_axis (datawindow adw_target, string as_axis)
public subroutine wf_printpreview ()
public function integer wf_initialize_text (ref datawindow adw_text, string as_textobject)
end prototypes

public function integer wf_size_it ();////////////////////////////////////////////////////////////////////////////////////////////////////
// function: wf_size_it
////////////////////////////////////////////////////////////////////////////////////////////////////

// save the original sizes of the w_base_sheet and the objects on the w_base_sheet
		
ii_win_width  = this.width
ii_win_height = this.height
ii_win_frame_w = this.width - this.WorkSpaceWidth()
ii_win_frame_h = this.height - this.WorkSpaceHeight()

	
	// everything has a x,y,width and height
	ii_dw_x 			= dw_graph.x 
	ii_dw_width 	= dw_graph.width 
	ii_dw_y 			= dw_graph.y
	ii_dw_height 	= dw_graph.height 
Return 1
end function

public function integer wf_resize_it (double ai_sizefactor);
////////////////////////////////////////////////////////////////////////////////////////////////////
// function: wf_resize_it
////////////////////////////////////////////////////////////////////////////////////////////////////


// loop through off of the objects captured in the wf_size_it function and resize them

SetRedraw(False)
ib_exec = false // keep the function from being called recursively
this.hide()

// resize the w_base_sheet
this.width = ((  ii_win_width - ii_win_frame_w) * ai_sizefactor) + ii_win_frame_w

this.height = ((  ii_win_height - ii_win_frame_h) * ai_sizefactor) + ii_win_frame_h

// for each control in the list, resize it and it's textsize (as applicable)
	dw_graph.x		 	= ii_dw_x * ai_sizefactor
	dw_graph.width   	= ii_dw_width  * ai_sizefactor
	dw_graph.y		 	= ii_dw_y* ai_sizefactor
	dw_graph.height  	= ii_dw_height * ai_sizefactor 
	

//	dw_graph.Modify("Datawindow.Zoom=" + String(int(ai_sizefactor*100)))

this.Show()
ib_exec = true
SetRedraw(True)
return 1

end function

public function integer wf_initialize_data (ref datawindow adw_data);int 		li_index,  &
			li_col_count
string 	ls_colnames[], &
			ls_rc, &
			ls_series, &
			ls_category, &
			ls_values, &
			ls_string, &
			ls_value

adw_data.SetRedraw(False)
f_dw_getcolnames ( dw_graph, ls_colnames[] )
li_col_count = UpperBound ( ls_colnames[] )

ls_category	=	"category.Values='" + ls_colnames[1] + "~~t" + ls_colnames[1]
ls_series	=	"Series_Value.Values='" + ls_colnames[1] + "~~t" + ls_colnames[1]
For li_index	 =	2 TO li_col_count
	ls_category	+=	"/" + ls_colnames[li_index] + "~~t" + ls_colnames[li_index] 
	ls_series	+=	"/" + ls_colnames[li_index] + "~~t" + ls_colnames[li_index] 
NEXT

ls_category	+=	"'"
ls_series	+= "'"

FOR li_index = 1 TO li_col_count
	ls_rc = dw_graph.Describe(ls_colnames[li_index]+'.Coltype')
	IF ls_rc = 	'number' or Left(ls_rc, 1)	=	'd' or ls_rc =	'real' or &
		ls_rc	=	'long' or ls_rc	=	'ulong' THEN 
		ls_string	=	'Sum(' + ls_colnames[li_index] + ' for graph)'
	ELSE
		ls_string	=	'Count(' + ls_colnames[li_index] + ' for graph)'
	END IF
	ls_value	+= ls_colnames[li_index] + "~~t" + ls_colnames[li_index] + &
					'/' + ls_string + "~~t" + ls_string	+ '/'
NEXT
ls_value	=	"Value.Values='" + Left(ls_value, Len(ls_value) - 1 ) + "'"	
adw_data.Modify(ls_category )
adw_data.Modify(ls_series)
adw_data.Modify(ls_Value)
ls_category = dw_graph.Describe("gr_1.Category")
ls_category = f_global_replace ( ls_category, '~~', '' )
ls_category = f_global_replace ( ls_category, '""', '"' )
adw_data.SetItem(1, 'category', ls_category)
ls_values = dw_graph.Describe("gr_1.Values")
ls_values = f_global_replace ( ls_values, '~~', '' )
ls_values = f_global_replace ( ls_values, '""', '"' )
adw_data.SetItem(1, 'Value', ls_values )
ls_series = dw_graph.Describe("gr_1.Series")
IF ls_series = '?' THEN ls_series = ""
ls_series = f_global_replace ( ls_series, '~~', '' )
ls_series = f_global_replace ( ls_series, '""', '"' )
IF trim(ls_series) = "" THEN
	adw_data.SetItem(1, 'Series', 0)
ELSE
	adw_data.SetItem(1, 'Series', 1)
	adw_data.SetItem(1, 'Series_value', ls_series)
END IF
adw_data.SetRedraw(True)
Return	1
end function

public function integer wf_initialize_name (ref datawindow adw_data);SetRedraw(False)
adw_data.SetItem(1, 'title', dw_graph.object.gr_1.title)
adw_data.SetItem(1, 'category_sort', long(dw_graph.Object.gr_1.category.sort))
adw_data.SetItem(1, 'series_sort', Long(dw_graph.Object.gr_1.series.sort))
adw_data.SetItem(1, 'legend_location', Long(dw_graph.Object.gr_1.legend))
adw_data.SetItem(1, 'overlap', Long(dw_graph.Object.gr_1.overlappercent ))
adw_data.SetItem(1, 'spacing', Long(dw_graph.Object.gr_1.spacing ))
SetRedraw(True)
return 	1

end function

public subroutine wf_enable_tabpages (string as_graphtype);is_graphtype	=	as_graphtype
IF as_graphtype	=	'03' or as_graphtype	=	'08' or &
	as_graphtype	=	'15' or as_graphtype	=	'16' THEN 
	tab_1.tabpage_category.enabled	=	True
	tab_1.tabpage_values.enabled		=	True
	tab_1.tabpage_series.enabled		=	True
ELSEIF as_graphtype	=	'13' or as_graphtype	=	'17' THEN
	tab_1.tabpage_category.enabled	=	False
	tab_1.tabpage_values.enabled		=	False
	tab_1.tabpage_series.enabled		=	False
ELSE
	tab_1.tabpage_category.enabled	=	True
	tab_1.tabpage_values.enabled		=	True
	tab_1.tabpage_series.enabled		=	False
END IF

end subroutine

public subroutine wf_print ();dw_graph.TriggerEvent ('printstart')
end subroutine

public subroutine wf_initialize_axis (datawindow adw_target, string as_axis);string		ls_majortic, &
				ls_minortic

adw_target.SetRedraw(False)
as_axis	=	'gr_1.' + as_axis
ls_majortic	=	dw_graph.Describe(as_axis + '.MajorTic')
ls_minortic	=	dw_graph.Describe(as_axis + '.MinorTic')
adw_target.object.label[1]						= dw_graph.Describe(as_axis + '.label')
adw_target.object.majordivisions[1]			= long(dw_graph.Describe(as_axis + '.MajorDivisions'))
adw_target.object.minordivisions[1]			= long(dw_graph.Describe(as_axis + '.MinorDivisions'))
adw_target.object.displayeverynlabels[1]	= long(dw_graph.Describe(as_axis + '.DisplayEveryNLabels'))
adw_target.Object.format[1]					= dw_graph.Describe(as_axis + '.Dispattr.Format')

CHOOSE CASE	ls_majortic
	CASE	'1'
		adw_target.object.cb_majortic.FileName = 'TIC_NO.BMP'
	CASE	'2'
		adw_target.object.cb_majortic.FileName = 'TIC_UP.BMP'
	CASE	'3'
		adw_target.object.cb_majortic.FileName = 'TIC_DN.BMP'
	CASE  '4'
		adw_target.object.cb_majortic.FileName = 'TIC_CNTR.BMP'
END CHOOSE

CHOOSE CASE	ls_minortic
	CASE	'1'
		adw_target.object.cb_minortic.FileName = 'TIC_NO.BMP'
	CASE	'2'
		adw_target.object.cb_minortic.FileName = 'TIC_UP.BMP'
	CASE	'3'
		adw_target.object.cb_minortic.FileName = 'TIC_DN.BMP'
	CASE  '4'
		adw_target.object.cb_minortic.FileName = 'TIC_CNTR.BMP'
END CHOOSE
adw_target.SetRedraw(true)
end subroutine

public subroutine wf_printpreview ();dw_graph.TriggerEvent ('ue_preview')
end subroutine

public function integer wf_initialize_text (ref datawindow adw_text, string as_textobject);string	ls_weight, &
			ls_italic, &
			ls_style, &
			ls_height, &
			ls_face
int		li_autosize, &
			li_underline
			
SetRedraw(False)			
li_autosize	=	Integer(dw_graph.Describe(as_textobject + '.AutoSize'))
ls_height	=	dw_graph.Describe(as_textobject + '.Font.Height')
li_underline=	Integer(dw_graph.Describe(as_textobject  + '.Font.Underline'))
ls_face		=	dw_graph.Describe(as_textobject +  '.Font.Face')
ls_italic	=	dw_graph.Describe(as_textobject +  '.Font.Italic')
ls_weight	=	dw_graph.Describe(as_textobject +  '.Font.Weight')

IF ls_weight	=	'400' THEN
	IF ls_italic	=	'0' THEN
		ls_style	=	'Normal'
	ELSE
		ls_style	=	'Italic'
	END IF
ELSE
	IF ls_italic	=	'0' THEN
		ls_style	=	'Bold'
	ELSE
		ls_style	=	'BoldItalic'		
	END IF
END IF
adw_text.SetItem(1, 'font_style', ls_style)
adw_text.SetItem(1, 'text_object', is_currentobject)
adw_text.SetItem(1, 'font_size', ls_height)
adw_text.SetItem(1, 'autosize', li_autosize)
adw_text.SetItem(1, 'underline', li_Underline)
adw_text.SetItem(1, 'font', ls_face)
SetRedraw(True)
return 1
end function

on w_new_graph.create
int iCurrent
call w_base_sheet::create
this.tab_1=create tab_1
this.dw_graph=create dw_graph
this.st_popup=create st_popup
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=tab_1
this.Control[iCurrent+2]=dw_graph
this.Control[iCurrent+3]=st_popup
end on

on w_new_graph.destroy
call w_base_sheet::destroy
destroy(this.tab_1)
destroy(this.dw_graph)
destroy(this.st_popup)
end on

event open;call super::open;int				li_rc
datawindow		ldw_graph
string			ls_dataobject_name, ls_rc

ldw_graph	=	message.Powerobjectparm

ls_dataobject_name	=	ldw_graph.dataobject
dw_graph.dataobject = ls_dataobject_name + "_graph"
li_rc = dw_graph.Importstring( ldw_graph.Describe("Datawindow.Data") )
IF li_rc = -1 THEN MessageBox( "Data Import", "Importing Data Failed" )
ls_rc	=	dw_graph.Describe("gr_1.Series")

li_rc = dw_graph.Sort()
li_rc = dw_graph.GroupCalc ( )

This.Title	=	dw_graph.object.gr_1.title
//set focus on the graphtype datawindow
tab_1.tabpage_type.dw_graph_type.TriggerEvent(getfocus!)

// save off the size data
li_rc = wf_size_it()
ib_exec = true

end event

event resize;double ratiow, ratio, ratioh
int rc

// recalculate the new ratios and then use the minimum
if ib_exec then  // Check to see if wf_resize_it is already running.
	ratioh = this.height /ii_win_height
	ratiow = this.width / ii_win_width
	ratio = min (ratioh, ratiow)
	rc = wf_resize_it(ratio)
end if

//dw_graph.Resize( (WorkSpaceWidth() - 50 ), ( WorkSpaceHeight()) )
end event

event activate;
gw_base_frame.im_base_Menu.M_File.M_New.Enabled = FALSE
gw_base_frame.im_base_Menu.M_file.m_Delete.Enabled = FALSE
gw_base_frame.im_base_Menu.M_file.M_Inquire.Enabled = FALSE
gw_base_frame.im_base_Menu.M_file.M_Save.Enabled = FALSE


end event

event deactivate;gw_base_frame.im_base_Menu.M_File.M_New.Enabled = TRUE
gw_base_frame.im_base_Menu.M_file.m_Delete.Enabled = TRUE
gw_base_frame.im_base_Menu.M_file.M_Inquire.Enabled = TRUE
gw_base_frame.im_base_Menu.M_file.M_Save.Enabled = TRUE
end event

event ue_postopen;integer		li_resources
				
u_sdkCalls	lu_sdkCalls

//Display warning box if low on system resources.
lu_sdkCalls = Create u_sdkCalls
li_resources = lu_sdkCalls.nf_GetFreeResourcePercent()
Destroy lu_SdkCalls
IF li_resources < 20 THEN
	MessageBox('Warning', 'Low on System Resources' + ' (' + String(li_resources) + '%' + ')' + ', Please Close one or more applications')
END IF

// This opens the window in the same size  and pos if the last saved 
// option is set to true in the settings window
IF gw_base_frame.iu_base_data.is_always_openas	=	'last saved' THEN
		wf_set_winpos ( )
		wf_set_winsize ( )
END IF
end event

type tab_1 from tab within w_new_graph
event selectionchanged pbm_tcnselchanged
int X=5
int Y=5
int Width=2894
int Height=333
int TabOrder=10
boolean FixedWidth=true
boolean FocusOnButtonDown=true
boolean RaggedRight=true
boolean BoldSelectedText=true
Alignment Alignment=Center!
int SelectedTab=1
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_type tabpage_type
tabpage_category tabpage_category
tabpage_values tabpage_values
tabpage_series tabpage_series
tabpage_data tabpage_data
tabpage_title tabpage_title
tabpage_1 tabpage_1
end type

event selectionchanged;CHOOSE CASE	newindex
	CASE	1
		tabpage_type.dw_graph_type.SetFocus()
	CASE	2
		wf_initialize_axis(tabpage_category.dw_category_axis, 'category')
		tabpage_category.dw_category_axis.SetFocus()
	CASE	3
		wf_initialize_axis(tabpage_values.dw_value_axis, 'values')
		tabpage_values.dw_value_axis.SetFocus()
	CASE	4
		wf_initialize_axis(tabpage_series.dw_series_axis, 'series')
		tabpage_series.dw_series_axis.SetFocus()
	CASE	5
		wf_initialize_data(tabpage_data.dw_graph_data)
		tabpage_data.dw_graph_data.SetFocus()
	CASE	6
		wf_initialize_name(tabpage_title.dw_graph_name)
		tabpage_title.dw_graph_name.SetFocus()
	CASE	7
		wf_initialize_text(tabpage_1.dw_text, is_objecttext)
		tabpage_title.dw_graph_name.SetFocus()
END CHOOSE
end event

on tab_1.create
this.tabpage_type=create tabpage_type
this.tabpage_category=create tabpage_category
this.tabpage_values=create tabpage_values
this.tabpage_series=create tabpage_series
this.tabpage_data=create tabpage_data
this.tabpage_title=create tabpage_title
this.tabpage_1=create tabpage_1
this.Control[]={ this.tabpage_type,&
this.tabpage_category,&
this.tabpage_values,&
this.tabpage_series,&
this.tabpage_data,&
this.tabpage_title,&
this.tabpage_1}
end on

on tab_1.destroy
destroy(this.tabpage_type)
destroy(this.tabpage_category)
destroy(this.tabpage_values)
destroy(this.tabpage_series)
destroy(this.tabpage_data)
destroy(this.tabpage_title)
destroy(this.tabpage_1)
end on

type tabpage_type from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Type"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_graph_type dw_graph_type
end type

on tabpage_type.create
this.dw_graph_type=create dw_graph_type
this.Control[]={ this.dw_graph_type}
end on

on tabpage_type.destroy
destroy(this.dw_graph_type)
end on

type dw_graph_type from datawindow within tabpage_type
event clicked pbm_dwnlbuttonclk
int Y=21
int Width=2839
int Height=165
int TabOrder=2
string DataObject="d_graph_type"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;string	ls_graphtype

SetRedraw(FALSE)

IF ISNUMBER(RIGHT(String(dwo.Name), 2)) THEN
	ls_graphtype	=	Trim(RIGHT(String(dwo.Name), 2))
	wf_enable_tabpages(ls_graphtype)
END IF
			
IF Row = 1 AND ISNUMBER(RIGHT(String(dwo.Name), 2)) THEN
	SetItem(1, 'c_Position', INTEGER(RIGHT(String(dwo.Name), 2)))
	dw_graph.object.gr_1.GraphType = INTEGER(RIGHT(String(dwo.Name), 2))
END IF
SetRedraw(TRUE)


end event

event getfocus;string	ls_graphtype

ls_graphtype	=	string(dw_graph.object.gr_1.GraphType)
SetItem(1, 'c_Position', long(ls_graphtype) )
wf_enable_tabpages(ls_GraphType)
end event

type tabpage_category from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Category"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_category_axis dw_category_axis
end type

on tabpage_category.create
this.dw_category_axis=create dw_category_axis
this.Control[]={ this.dw_category_axis}
end on

on tabpage_category.destroy
destroy(this.dw_category_axis)
end on

type dw_category_axis from datawindow within tabpage_category
event clicked pbm_dwnlbuttonclk
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
int X=14
int Width=2771
int Height=185
int TabOrder=1
string DataObject="d_axis"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'CB_MAJORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				dw_graph.Object.gr_1.Category.MajorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				dw_graph.Object.gr_1.Category.MajorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				dw_graph.Object.gr_1.Category.MajorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				dw_graph.Object.gr_1.Category.MajorTic = 2
		END CHOOSE
		dwo.Border = 6
	CASE 'CB_MINORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				dw_graph.Object.gr_1.Category.MinorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				dw_graph.Object.gr_1.Category.MinorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				dw_graph.Object.gr_1.Category.MinorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				dw_graph.Object.gr_1.Category.MinorTic = 2
		END CHOOSE
		dwo.Border = 6
END CHOOSE
end event

event constructor;InsertRow(0)
end event

event itemchanged;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'LABEL'
		dw_graph.Object.gr_1.Category.Label = data
	CASE 'MAJORDIVISIONS'
		IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Major Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Category.MajorDivisions = INTEGER(data)
	CASE 'MINORDIVISIONS'
		IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Minor Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Category.MinorDivisions = INTEGER(data)
	CASE 'DISPLAYEVERYNLABELS'
		IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Display labels must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Category.DisplayEveryNLabels 	= INTEGER(data)
	CASE	'FORMAT'
		dw_graph.Object.gr_1.Category.Dispattr.Format		=	data
END CHOOSE

end event

event getfocus;IF is_graphtype	=	'14' THEN
	This.Object.majordivisions.protect					=	0
	This.Object.minordivisions.protect					=	0
	This.Object.majordivisions.background.color		=	16777215
	This.Object.minordivisions.background.color		=	16777215
ELSE
	This.Object.majordivisions.protect					=	1
	This.Object.minordivisions.protect					=	1
	This.Object.majordivisions.background.color		=	12632256
	This.Object.minordivisions.background.color		=	12632256
END IF
end event

event itemerror;RETURN 1
end event

type tabpage_values from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Values"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_value_axis dw_value_axis
end type

on tabpage_values.create
this.dw_value_axis=create dw_value_axis
this.Control[]={ this.dw_value_axis}
end on

on tabpage_values.destroy
destroy(this.dw_value_axis)
end on

type dw_value_axis from datawindow within tabpage_values
event clicked pbm_dwnlbuttonclk
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
int Y=5
int Width=2839
int Height=189
int TabOrder=1
string DataObject="d_axis"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'CB_MAJORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				dw_graph.Object.gr_1.Values.MajorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				dw_graph.Object.gr_1.Values.MajorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				dw_graph.Object.gr_1.Values.MajorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				dw_graph.Object.gr_1.Values.MajorTic = 2
		END CHOOSE
		dwo.Border = 6
	CASE 'CB_MINORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				dw_graph.Object.gr_1.Values.MinorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				dw_graph.Object.gr_1.Values.MinorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				dw_graph.Object.gr_1.Values.MinorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				dw_graph.Object.gr_1.Values.MinorTic = 2
		END CHOOSE
		dwo.Border = 6
END CHOOSE
end event

event constructor;InsertRow(0)
end event

event itemchanged;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'LABEL'
		dw_graph.Object.gr_1.Values.Label = GetText()
	CASE 'MAJORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Major Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Values.MajorDivisions = INTEGER(GetText())
	CASE 'MINORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Minor Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Values.MinorDivisions = INTEGER(GetText())
	CASE 'DISPLAYEVERYNLABELS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Display labels must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Values.DisplayEveryNLabels = INTEGER(GetText())
	CASE	'FORMAT'
		dw_graph.Object.gr_1.Values.Dispattr.Format		=	data
END CHOOSE
end event

event itemerror;RETURN 1
end event

type tabpage_series from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Series"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_series_axis dw_series_axis
end type

on tabpage_series.create
this.dw_series_axis=create dw_series_axis
this.Control[]={ this.dw_series_axis}
end on

on tabpage_series.destroy
destroy(this.dw_series_axis)
end on

type dw_series_axis from datawindow within tabpage_series
event clicked pbm_dwnlbuttonclk
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
int Width=2757
int Height=189
int TabOrder=1
string DataObject="d_axis"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'CB_MAJORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				dw_graph.Object.gr_1.Series.MajorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				dw_graph.Object.gr_1.Series.MajorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				dw_graph.Object.gr_1.Series.MajorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				dw_graph.Object.gr_1.Series.MajorTic = 2
		END CHOOSE
		dwo.Border = 6
	CASE 'CB_MINORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				dw_graph.Object.gr_1.Series.MinorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				dw_graph.Object.gr_1.Series.MinorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				dw_graph.Object.gr_1.Series.MinorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				dw_graph.Object.gr_1.Series.MinorTic = 2
		END CHOOSE
		dwo.Border = 6
END CHOOSE
end event

event constructor;InsertRow(0)
end event

event itemchanged;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'LABEL'
		dw_graph.Object.gr_1.Series.Label = GetText()
	CASE 'MAJORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Major Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Series.MajorDivisions = INTEGER(GetText())
	CASE 'MINORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Minor Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Series.MinorDivisions = INTEGER(GetText())
	CASE 'DISPLAYEVERYNLABELS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Display labels must be greater than or equal to zero')
			RETURN	1
		END IF
		dw_graph.Object.gr_1.Series.DisplayEveryNLabels = INTEGER(GetText())
END CHOOSE
end event

event itemerror;RETURN 1
end event

type tabpage_data from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Data"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_graph_data dw_graph_data
end type

on tabpage_data.create
this.dw_graph_data=create dw_graph_data
this.Control[]={ this.dw_graph_data}
end on

on tabpage_data.destroy
destroy(this.dw_graph_data)
end on

type dw_graph_data from datawindow within tabpage_data
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
int Y=5
int Width=2849
int Height=193
int TabOrder=11
string DataObject="d_graph_data"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event itemchanged;string 	ls_rc, &
			ls_mod_string, &
			ls_data

CHOOSE CASE String(dwo.Name)
	CASE	'category'
		ls_data			=	String(data)
		ls_data		 	= 	f_global_replace ( ls_data, "'", "~~~'" )
		ls_mod_string 	= 	"gr_1.Category='"+ls_data+"'"
		ls_rc 			= dw_graph.Modify(ls_mod_string)
		IF ls_rc <> "" THEN 
			MessageBox( "Invalid Category", "Category entered is not a Valid Column", stopsign! )
			This.SelectText(1, Len(ls_data))
			Return 1
		END IF
	CASE	'value'
		ls_data 			=	String(data)
		ls_data 			= 	f_global_replace ( ls_data, "'", "~~~'" )
		ls_mod_string 	= "gr_1.Values='"+ls_data+"'"
		ls_rc 			= dw_graph.Modify(ls_mod_string)
		IF ls_rc <> "" THEN 
			MessageBox( "Invalid Values", "Value entered is not a Valid Column", stopsign!  )
			This.SelectText(1, Len(ls_data))
			Return 1
		END IF
	CASE	'series'
		IF Integer(data)	=	0 THEN
			This.setitem(1, 'series_value', '')
			ls_data 			=	''
			ls_rc 			= dw_graph.Modify("gr_1.series = '~"noseries ~" ' ")
			This.SetRedraw(True)
		END IF
	CASE	'series_value'
		ls_data 			=	String(data)
		ls_data 			=	f_global_replace ( ls_data, "'", "~~~'" )
		ls_mod_string 	= 	"gr_1.Series = '" + ls_data + " '"
		ls_rc 			= dw_graph.Modify(ls_mod_string)
		IF ls_rc <> "" THEN 
			MessageBox( "Graph Series", "The Series entered is not Valid Column or a Combination of Columns", stopsign! )
			This.SelectText(1, Len(ls_data))
			Return 1
		END IF
END CHOOSE
end event

event itemerror;RETURN 1
end event

type tabpage_title from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Title"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_graph_name dw_graph_name
end type

on tabpage_title.create
this.dw_graph_name=create dw_graph_name
this.Control[]={ this.dw_graph_name}
end on

on tabpage_title.destroy
destroy(this.dw_graph_name)
end on

type dw_graph_name from datawindow within tabpage_title
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
int Width=2858
int Height=193
int TabOrder=1
string DataObject="d_graph_name"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event itemchanged;CHOOSE CASE	String(dwo.name)
	CASE	'category_sort'
		dw_graph.Object.gr_1.category.sort 	= long(data)
	CASE	'series_sort'
		dw_graph.Object.gr_1.series.sort 	= long(data)
	CASE	'title'
		dw_graph.Object.gr_1.title 			= string(data)
	CASE	'legend_location'
		dw_graph.Object.gr_1.legend 			= long(data)
	CASE	'overlap'
		dw_graph.Object.gr_1.overlappercent = long(data)
	CASE	'spacing'
		dw_graph.Object.gr_1.spacing			= long(data)
END CHOOSE

end event

event getfocus;This.SetRedraw(False)
IF is_graphtype	=	'01' or is_graphtype	=	'12' or &
	is_graphtype	=	'13' or is_graphtype	=	'14' THEN
	This.Object.overlap.protect				=	1
	This.Object.spacing.protect				=	1
	This.Object.overlap.background.color	=	12632256
	This.Object.spacing.background.color	=	12632256
ELSEIF is_graphtype	=	'02' or is_graphtype	=	'07' THEN
	This.Object.overlap.protect				=	0
	This.Object.spacing.protect				=	0
	This.Object.overlap.background.color	=	16777215
	This.Object.spacing.background.color	=	16777215
ELSE
	This.Object.overlap.protect				=	1
	This.Object.overlap.background.color	=	12632256
	This.Object.spacing.protect				=	0
	This.Object.spacing.background.color	=	16777215
END IF
This.SetRedraw(True)
end event

type tabpage_1 from userobject within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
long BackColor=12632256
string Text="Text"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_text dw_text
end type

on tabpage_1.create
this.dw_text=create dw_text
this.Control[]={ this.dw_text}
end on

on tabpage_1.destroy
destroy(this.dw_text)
end on

type dw_text from datawindow within tabpage_1
int Y=9
int Width=2853
int Height=201
int TabOrder=3
boolean BringToTop=true
string DataObject="d_graph_text"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event itemchanged;This.SetRedraw(False)
CHOOSE CASE UPPER(String(dwo.Name))
	CASE	'TEXT_OBJECT'
		is_currentobject	=	data
		CHOOSE CASE	data
			CASE	'CategoryAxisLabel'
				is_objecttext	=	'gr_1.Category.LabelDispattr'
			CASE	'ValueAxisLabel'
				is_objecttext	=	'gr_1.Values.LabelDispattr'
			CASE	'SeriesAxisLabel'
				is_objecttext	=	'gr_1.Series.LabelDispattr'
			CASE ELSE
				is_objecttext	=	'gr_1.' + data + '.Dispattr'
		END CHOOSE
		wf_initialize_text(dw_text, is_objecttext)
	CASE	'FONT'
		dw_graph.Modify(is_objecttext + '.Font.Face="' + data + '"')
	CASE	'FONT_SIZE'
		dw_graph.Modify(is_objecttext + '.Font.Height=' + data)
	CASE	'FONT_STYLE'
		CHOOSE CASE Upper(data)
			CASE	'NORMAL'
				dw_graph.Modify(is_objecttext + '.Font.Italic=0')
				dw_graph.Modify(is_objecttext + '.Font.Weight=400')
			CASE	'BOLD'
				dw_graph.Modify(is_objecttext + '.Font.Italic=0')
				dw_graph.Modify(is_objecttext + '.Font.Weight=700')
			CASE	'ITALIC'
				dw_graph.Modify(is_objecttext + '.Font.Weight=400')
				dw_graph.Modify(is_objecttext + '.Font.Italic=1')
			CASE	'BOLDITALIC'
				dw_graph.Modify(is_objecttext + '.Font.Weight=700')
				dw_graph.Modify(is_objecttext + '.Font.Italic=1')
		END CHOOSE
	CASE	'UNDERLINE'
		dw_graph.Modify(is_objecttext + '.Font.Underline=' + String(data))
	CASE	'AUTOSIZE'
		dw_graph.Modify(is_objecttext + '.AutoSize=' + String(data))
END CHOOSE
This.SetRedraw(True)
end event

type dw_graph from datawindow within w_new_graph
event ue_preview ( )
event ue_rbuttonup pbm_rbuttonup
int Y=329
int Width=2894
int Height=1217
string DataObject="d_graph"
boolean Resizable=true
boolean LiveScroll=true
end type

event ue_rbuttonup;//hide the popup box again
st_popup.visible = false
end event

event printstart;long ll_job


String	ls_job
SetPointer(Hourglass!)

ls_job	=	dw_graph.Object.gr_1.Category.LabelDispattr.Font.Height
ll_job	=	Long(dw_graph.Object.gr_1.Category.LabelDispattr.Font.Height)
ll_job	=	Long(dw_graph.Object.gr_1.Category.Dispattr.Font.Height)
ls_job	=	ls_job
gw_base_frame.setmicrohelp("Printing, Please Wait")
//This.Object.
ll_job = printopen()
//IF	String(This.Object.DataWindow.Print.Orientation)	=	'2' THEN
dw_graph.print(ll_job, 500, PrintY(ll_Job)+500, 9500,7000)
//dw_graph.print()
//ELSE
//	dw_graph.print(ll_job, 500, PrintY(ll_Job)+2000, 7000,5000)
//END IF
printclose(ll_job)
gw_base_frame.setmicrohelp("Ready")

end event

event rbuttondown;// Clicked script for dw_headcount
// This will cause a static text box to appear next to the pointer where the user
// is using right mouse button down. The acutal value for the data item will
// be displayed in the text box.

grObjectType	ClickedObject
string			ls_grgraphname="gr_1"
int				li_series, li_category

/************************************************************
	Find out where the user clicked in the graph
 ***********************************************************/
ClickedObject = this.ObjectAtPointer (ls_grgraphname, li_series, &
						li_category)

/************************************************************
	If user clicked on data or category, find out which one
 ***********************************************************/
If ClickedObject = TypeData! Then
	st_popup.text = string(this.GetData(ls_grgraphname, li_series, li_category))		
	st_popup.x = parent.PointerX()
	st_popup.y = parent.PointerY() - 65
	st_popup.visible = true
End If

end event

type st_popup from statictext within w_new_graph
int X=851
int Y=557
int Width=476
int Height=77
boolean Visible=false
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-10
int Weight=700
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

