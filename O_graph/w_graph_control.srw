HA$PBExportHeader$w_graph_control.srw
forward
global type w_graph_control from w_base_popup
end type
type uo_control from u_graph_control within w_graph_control
end type
end forward

global type w_graph_control from w_base_popup
int Width=2907
int Height=396
WindowType WindowType=popup!
boolean TitleBar=true
string Title="Graph Control"
long BackColor=1090519039
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
uo_control uo_control
end type
global w_graph_control w_graph_control

forward prototypes
public subroutine wf_setgraph (DataWindow adw_graph, string as_name)
end prototypes

public subroutine wf_setgraph (DataWindow adw_graph, string as_name);uo_control.uf_SetGraph(adw_Graph, as_Name)
end subroutine

on w_graph_control.create
int iCurrent
call super::create
this.uo_control=create uo_control
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_control
end on

on w_graph_control.destroy
call super::destroy
destroy(this.uo_control)
end on

type uo_control from u_graph_control within w_graph_control
int X=0
int Y=0
int TabOrder=10
boolean BringToTop=true
end type

on uo_control.destroy
call u_graph_control::destroy
end on

