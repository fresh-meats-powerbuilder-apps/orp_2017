HA$PBExportHeader$w_edi_review_orders.srw
forward
global type w_edi_review_orders from w_base_sheet_ext
end type
type rb_deselect from radiobutton within w_edi_review_orders
end type
type rb_select from radiobutton within w_edi_review_orders
end type
type cb_sort from commandbutton within w_edi_review_orders
end type
type dw_line_choice from u_base_dw_ext within w_edi_review_orders
end type
type dw_detail from u_base_dw_ext within w_edi_review_orders
end type
type dw_header from u_base_dw_ext within w_edi_review_orders
end type
type gb_1 from groupbox within w_edi_review_orders
end type
end forward

global type w_edi_review_orders from w_base_sheet_ext
integer width = 3762
integer height = 2296
string title = "Review EDI Orders Against Sales Orders"
boolean minbox = false
boolean maxbox = false
rb_deselect rb_deselect
rb_select rb_select
cb_sort cb_sort
dw_line_choice dw_line_choice
dw_detail dw_detail
dw_header dw_header
gb_1 gb_1
end type
global w_edi_review_orders w_edi_review_orders

type variables
s_error		istr_error_info


u_orp001		iu_orp001
u_ws_orp1		iu_ws_orp1

long		il_selected_rows

String				is_customer_inq, &
		is_update_string, &
		is_columnname, &
		is_colname

Boolean  		ib_modified = false, &
		ib_updating , &
		ib_reinquire, &
		ib_retrieve, &
		ib_updateable

end variables

forward prototypes
public function boolean wf_update_modify (long al_row, character ac_status_ind)
public function boolean wf_unstring_output (string as_output_string)
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function boolean wf_validate (long al_row)
end prototypes

public function boolean wf_update_modify (long al_row, character ac_status_ind);dwbuffer	ldwb_buffer
string	ls_start_date, &
			ls_end_date, ls_product_code, ls_default_ind, &
			ls_cust_prod_code, ls_dept_code, ls_upc_code, ls_prev_cust_prod
			
Long		ll_rtn, &
			ll_nbrrows
			
ll_nbrrows = dw_detail.RowCount()

if ac_status_ind = 'D' then
	ldwb_buffer = delete!
else
	ldwb_buffer = primary!
end if

//ls_start_date = String(dw_detail.GetItemdate(al_row, "start_date", ldwb_buffer, false),'yyyy-mm-dd')
//ls_end_date = String(dw_detail.GetItemdate(al_row, "end_date", ldwb_buffer, false),'yyyy-mm-dd')
//ls_product_code = trim(dw_detail.getitemstring (al_row, "product_code",  ldwb_buffer, false))
//ls_default_ind = dw_detail.getitemstring (al_row, "default_ind",  ldwb_buffer, false)
//ls_cust_prod_code = trim(dw_detail.getitemstring (al_row, "cust_prod_code",  ldwb_buffer, false))
//ls_dept_code = dw_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false)
//ls_upc_code = dw_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false)
//ls_prev_cust_prod = dw_detail.getitemstring (al_row, "prev_cust_prod",  ldwb_buffer, false)
//is_update_string += &
//	trim(dw_detail.getitemstring (al_row, "product_code",  ldwb_buffer, false)) + "~t" + &
//	dw_detail.getitemstring (al_row, "default_ind",  ldwb_buffer, false) + "~t" + &
//	trim(dw_detail.getitemstring (al_row, "cust_prod_code",  ldwb_buffer, false)) + "~t" + &
//	dw_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false) + "~t" + &
//	dw_detail.getitemstring (al_row, "upc_code",  ldwb_buffer, false) + "~t" + &
//	String(dw_detail.GetItemdate(al_row, "start_date", ldwb_buffer, false),'yyyy-mm-dd') + "~t" + &
//	String(dw_detail.GetItemdate(al_row, "end_date", ldwb_buffer, false),'yyyy-mm-dd') + "~t" + &
//	dw_detail.getitemstring (al_row, "prev_cust_prod",  ldwb_buffer, false) + "~t" + &
//	ac_status_ind + "~r~n"
//	
return true

end function

public function boolean wf_unstring_output (string as_output_string);Date					ldt_start_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows, &
						ll_sequence_number
						

String				ls_cust_product, &
						ls_fm_product, &
						ls_customer_id, &
						ls_searchstring, &
						ls_system, &
						ls_start_date, &
						ls_output_string, &
						ls_sequence_number
integer				li_sequence_number
						

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_detail.RowCount()

dw_detail.SelectRow(0,False)

////find the row that matches the fm product, cust product and start date
//
ll_nbrrows = dw_detail.RowCount()
ls_sequence_number = string(lu_string_functions.nf_gettoken(as_output_string, '~t'))
ls_system = lu_string_functions.nf_gettoken(as_output_string, '~t')
//ls_fm_product = lu_string_functions.nf_gettoken(ls_output_string, '~t')
//ls_cust_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
//ls_start_date = lu_string_functions.nf_gettoken(as_output_string, '~t')
//
							
ls_SearchString = 	"sequence_number = " + ls_sequence_number + &
									" and system = '" + ls_system + "'"
//	
ll_rtn = dw_detail.Find  &
					( ls_SearchString, 1, ll_nbrrows)
//		
If ll_rtn > 0 Then
	dw_detail.SetRedraw(False)
	dw_detail.ScrollToRow(ll_rtn)
	dw_detail.SetRow(ll_rtn)
	dw_detail.SelectRow(ll_rtn, True)
	dw_detail.SetRedraw(True)
End If
//
ls_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public function boolean wf_update ();String	ls_input, &
			ls_update_string, &
			ls_output_string_in, &
			ls_output_string, &
			ls_header_string, &
			ls_rpc_detail, &
			ls_MicroHelp, &
			ls_accept_change, &
			ls_update_flag


Integer	li_rtn, &
			li_count
			
Long		ll_rtn
			
			
			
Long		ll_rowcount, &
			ll_pos, &
			ll_col, &
			ll_row, &
			ll_modrows, &
			ll_6th_row
			

char		lc_status_ind

Boolean	lb_error_found

u_string_functions	lu_string_functions

dw_detail.SetReDraw(False)

if dw_detail.AcceptText() = -1 then
	Return False
end if

SetPointer(HourGlass!)
dw_detail.SetRedraw(false)

is_update_string = ''
ll_rowcount = dw_detail.rowcount()
ll_modrows = dw_detail.modifiedcount()


ll_row = 0

for li_count = 1 to ll_rowcount
//
	ls_accept_change = dw_detail.GetItemString(li_count,"accept_change")
	IF ls_accept_change = 'Y' THEN 
		If Not wf_validate(li_count) Then 
			dw_detail.SetRedraw(True) 
			Return False
		end if	
	END IF
next
//
//for li_count = 1 to ll_modrows
////
//	ll_Row = dw_detail.GetNextModified(ll_Row, Primary!)
//	IF ll_Row > 0 THEN 
//		If Not wf_validate(ll_row) Then 
//			dw_detail.SetRedraw(True) 
//			Return False
//		end if	
//	END IF
//next


ll_row = 0
//
for li_count = 1 to ll_rowcount
//
	ls_accept_change = dw_detail.GetItemString(li_count,"accept_change")
	IF ls_accept_change = 'Y' THEN 
			dw_detail.setitem(li_count, "update_flag", 'U')
		else
			dw_detail.setitem(li_count, "update_flag", ' ')
	end if
next
ls_update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_detail)

if IsNull(ls_update_string) or Len(trim(ls_update_string)) = 0 then
	iw_frame.SetMicroHelp("No rows selected for Update")
	dw_detail.SetRedraw(True) 
	return false
end if
	

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_orp201.nf_orpo14br_review_edi_orders_upd"
istr_error_info.se_message = Space(71)

//loop to handle mass updates
DO 

	ll_6th_Row = lu_string_functions.nf_nPos( ls_Update_string, "~r~n",1,6)
	IF ll_6th_Row > 0 Then
		ls_RPC_Detail = Left( ls_Update_string, ll_6th_Row + 1)
		ls_Update_string = Mid( ls_Update_string, ll_6th_Row + 2)
	ELSE
		ls_Rpc_detail = ls_Update_string
		ls_Update_string = ''
	END IF

//	li_rtn =  iu_orp001.nf_orpo14br_review_edi_orders_upd (istr_error_info, & 
	//										ls_RPC_detail, &
		//									ls_output_string ) 
											
		li_rtn =  iu_ws_orp1.nf_orpo14fr(istr_error_info, & 
											ls_RPC_detail, &
											ls_output_string ) 											

	if li_rtn <> 0 then
		if ls_output_string > '' Then
			wf_unstring_output(ls_output_string)
		End If
		dw_detail.SetRedraw(True)
		SetPointer(Arrow!)																		
		return false
	end if
//	if li_rtn <> 0 Then 
//		Return False 
//	End if
//

Loop While Not lu_string_functions.nf_IsEmpty( ls_Update_string)



dw_detail.ResetUpdate()
ib_ReInquire = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")

dw_detail.SetFocus()
dw_detail.SetReDraw(True)


Return True

end function

public function boolean wf_retrieve ();Boolean	lb_show_message
String		ls_input, &
				ls_output_string, &
				ls_row_option, &
				ls_filter, &
				ls_customer_type, &
				ls_customer_id, &
				ls_div_code, &
				ls_sales_order_id, &
				ls_date_option, &
				ls_from_date, &
				ls_to_date
				
				
Integer		li_rtn

Long		ll_value


TriggerEvent("closequery")

ls_input = Message.StringParm

If Not ib_ReInquire Then
	OpenWithParm(w_edi_review_orders_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_customer_type = dw_header.GetItemString( 1, "customer_type")
If IsNull(ls_customer_type) Then
	ls_customer_type = ""
End If

ls_customer_id = dw_header.GetItemString( 1, "customer_id")
If IsNull(ls_customer_id) Then
	ls_customer_id = ""
End If

ls_div_code = dw_header.GetItemString( 1, "div_code")
If IsNull(ls_div_code) or ls_div_code = '  ' Then
	ls_div_code = 'AL'
End If

ls_sales_order_id = dw_header.GetItemString( 1, "sales_order_id")
If IsNull(ls_sales_order_id) Then
	ls_sales_order_id = ""
End If

ls_date_option = dw_header.GetItemString( 1, "date_option")
If IsNull(ls_date_option) Then
	ls_date_option = ""
End If

ls_from_date = string(dw_header.GetItemdate( 1, "from_date"), 'yyyy-mm-dd')
If IsNull(ls_from_date) Then
	ls_from_date = ""
End If

ls_to_date = string(dw_header.GetItemDate( 1, "to_date"), 'yyyy-mm-dd')
If IsNull(ls_to_date) Then
	ls_to_date = ""
End If

ls_input =  dw_header.GetItemString( 1, "inquire_option") + '~t' + &
    			ls_customer_type + '~t' + &
				ls_customer_id + '~t' + & 
				ls_div_code + '~t' + &
				ls_sales_order_id + '~t' + &
				dw_line_choice.GetItemString( 1, "choice") + '~t' + &
				ls_date_option + '~t' + &
				ls_from_date + '~t' + &
				ls_to_date + '~r~n'
//			  
istr_error_info.se_function_name = "wf_retrieve"
//
dw_detail.Reset()
//li_rtn = iu_orp001.nf_orpo13br_review_edi_orders_inq (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp1.nf_orpo13fr(istr_error_info, ls_input, ls_output_string)
ib_retrieve = True
dw_detail.ImportString(ls_output_string)
dw_detail.ResetUpdate()
//
//if li_rtn = 0 Then
//	ls_row_option = dw_detail_choice.GetItemString(1, "choice")
//	If ls_row_option = 'C' Then
//		ls_filter = "(end_date > RelativeDate(Today(), -1)) and (product_code > ' ')" 
//		dw_detail.SetFilter(ls_filter)
//		dw_detail.Filter()
//	Else
//		dw_detail.SetFilter("")
//		dw_detail.Filter()
//	End if
//End if

ll_value = dw_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

if li_rtn = 0 Then
	ls_row_option = dw_line_choice.GetItemString( 1, "choice")
	If ls_row_option = 'U' Then
		dw_detail.SetFilter("")
		dw_detail.Filter()
		ls_filter = "(match_found_ind = 'N')" 
		dw_detail.SetFilter(ls_filter)
		dw_detail.Filter()
	Else
		If ls_row_option = 'I' Then
			dw_detail.SetFilter("")
			dw_detail.Filter()
			ls_filter = "(invoice_status = 'W')" 
			dw_detail.SetFilter(ls_filter)
			dw_detail.Filter()
		else	
			dw_detail.SetFilter("")
			dw_detail.Filter()
		End if
	end if
	This.dw_detail.Event ue_set_hspscroll()
end if
dw_detail.ResetUpdate()
//dw_detail_choice.ResetUpdate()

IF ll_value > 0 THEN
	dw_detail.SetFocus()
	dw_detail.ScrollToRow(1)
//	dw_detail.SetColumn( "product_code" )
	dw_detail.TriggerEvent("RowFocusChanged")
END IF

//dw_detail.SetSort("product_code, start_date, end_date, default_ind")
//dw_detail.Sort()
dw_detail.ResetUpdate()
dw_detail.SetReDraw(True)
This.SetRedraw(True)
//dw_detail.insertrow(0)

//ll_value = ll_value + 1
//dw_detail.SetItem(ll_value, "product_code", ' ')
//dw_detail.SetColumn("product_code")
//dw_detail.SetFocus()
//dw_detail.SetItem(ll_value, "start_date", today())
//dw_detail.ResetUpdate()
//dw_detail.uf_changerowstatus(ll_value, New!)
//ib_retrieve = False

Return TRUE
end function

public function boolean wf_validate (long al_row);int			li_rc, &
				li_seconds,li_temp

long			ll_source_row, &
				ll_RowCount, &
				ll_find_row, &
				ll_sequence_number, &
				ll_rtn, &
				ll_nbrrows, &
				ll_current_row

string		ls_temp, &
				ls_sequence_number, &
				ls_new_sequence_number, &
				ls_sequence, &
				ls_searchstring, &
				ls_ordered_product, &
				ls_fm_product, &
				ls_system, &
				ls_accept_change, &
				ls_sales_order_id, &
				ls_orp_po_num, & 
				ls_edi_po_num
				
Boolean		lb_changed
				
ll_nbrrows = dw_detail.RowCount()
ll_current_row = al_row

ls_system = dw_detail.getitemstring(al_row, "system")
ls_sequence_number = string(dw_detail.getItemNumber(al_row, "sequence_number"))
ls_ordered_product = trim(dw_detail.getitemstring(al_row, "ordered_product"))
ls_accept_change = dw_detail.getitemstring(al_row, "accept_change")
ls_orp_po_num = trim(dw_detail.getitemstring(al_row, "po_number"))

if ls_system = 'ORP' then  
	if ls_accept_change = 'Y' then
		ls_system = 'EDI'
		ls_SearchString = 	"sequence_number = " + ls_sequence_number + &
								" and system = '" + ls_system + "'"
//	
		CHOOSE CASE al_row 
		CASE 1
		ll_rtn = dw_detail.Find  &
					( ls_SearchString, al_row + 1, ll_nbrrows)
		CASE 2 to (ll_nbrrows - 1)
			ll_rtn = dw_detail.Find ( ls_SearchString, al_row - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_detail.Find  &
				(ls_SearchString, al_row + 1, ll_nbrrows)
		CASE ll_nbrrows 
			ll_rtn = dw_detail.Find ( ls_SearchString, al_row - 1, 1)
		END CHOOSE
		If ll_rtn > 0 Then
			ls_new_sequence_number = string(dw_detail.getItemNumber(ll_rtn, "sequence_number"))
			ls_sales_order_id = trim(dw_detail.getitemstring(ll_rtn, "sales_order_id"))
			ls_fm_product = trim(dw_detail.getitemstring(ll_rtn, "fm_product"))
	   	ls_edi_po_num = trim(dw_detail.getitemstring(ll_rtn, "po_number"))
			if ls_fm_product > ' ' then
				if ls_fm_product = ls_ordered_product  or ls_ordered_product =  ' ' &
					or IsNull(ls_ordered_product) or ls_ordered_product = "" then
						dw_detail.setitem(ll_rtn, "accept_change", 'Y')
				else	
					iw_frame.SetMicroHelp(" Ordered Product must match FM product on EDI customer order")
					dw_detail.SetRedraw(False)
					dw_detail.ScrollToRow(al_row)
					dw_detail.SetColumn("sequence_number")
					dw_detail.SetRow(al_row)
					dw_detail.SelectRow(ll_rtn, True)
					dw_detail.SelectRow(al_row, True)
					dw_detail.SetRedraw(True)
				Return False
				End If
			end if
		
			if ls_orp_po_num = ls_edi_po_num then
		//do nothing
			else	
				iw_frame.SetMicroHelp(" ORP Purchase Order Numbers must match EDI Purchase Order Number")
				dw_detail.SetRedraw(False)
				dw_detail.ScrollToRow(al_row)
				dw_detail.SetColumn("sequence_number")
				dw_detail.SetRow(al_row)
				dw_detail.SelectRow(ll_rtn, True)
				dw_detail.SelectRow(al_row, True)
				dw_detail.SetRedraw(True)
				Return False
			End If
		
		else
			dw_detail.setitem(ll_current_row, "sequence_number", 0)
		end if	
	end if	
end if	
dw_detail.SelectRow(0, False)
	
Return True
end function

on w_edi_review_orders.create
int iCurrent
call super::create
this.rb_deselect=create rb_deselect
this.rb_select=create rb_select
this.cb_sort=create cb_sort
this.dw_line_choice=create dw_line_choice
this.dw_detail=create dw_detail
this.dw_header=create dw_header
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_deselect
this.Control[iCurrent+2]=this.rb_select
this.Control[iCurrent+3]=this.cb_sort
this.Control[iCurrent+4]=this.dw_line_choice
this.Control[iCurrent+5]=this.dw_detail
this.Control[iCurrent+6]=this.dw_header
this.Control[iCurrent+7]=this.gb_1
end on

on w_edi_review_orders.destroy
call super::destroy
destroy(this.rb_deselect)
destroy(this.rb_select)
destroy(this.cb_sort)
destroy(this.dw_line_choice)
destroy(this.dw_detail)
destroy(this.dw_header)
destroy(this.gb_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_delete")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")

end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
DESTROY iu_ws_orp1

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")
end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")
end event

event ue_query;call super::ue_query;IF wf_Retrieve() = false Then
		This.SetRedraw( TRUE)
		CLOSE(THIS)
		RETURN
END IF
end event

event ue_set_data;call super::ue_set_data;choose case as_data_item
	case "customer_type"
		dw_header.setItem(1, 'customer_type', as_value)
	case "customer_id"
		dw_header.setItem(1, 'customer_id', as_value)
	case "customer_name"
		dw_header.setItem(1, 'customer_name', as_value)
	case "div_code"
		if as_value = 'AL' then
		   as_value = '  '
		end if
		dw_header.setItem(1, 'div_code', as_value)	
	case "date_option"
		dw_header.setItem(1, 'date_option', as_value)	
	Case 'from_date'
		dw_header.SetItem(1, 'from_date', Date(as_value))
	Case 'to_date'
		dw_header.SetItem(1, 'to_date', Date(as_value))
	Case 'sales_order_id'
		dw_header.SetItem(1, 'sales_order_id', as_value)
	case "line_option"
		dw_line_choice.SetItem(1, 'choice', as_value)
//		dw_header.setItem(1, 'line_option', as_value)	
	case "inquire_option"
		dw_header.setItem(1, 'inquire_option', as_value)	
//	case "customer_name"
//		dw_header.setItem(1, 'customer_name', as_value)
end choose
end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp1 = Create u_ws_orp1

String	ls_ret, &
			ls_message, &
			ls_customer_id, &
			ls_customer_name
		
u_string_functions		lu_string_functions	

dw_detail.InsertRow(0)
			

//is_initial_reference = Message.StringParm

//ls_ret = dw_detail_header.Modify('excp_customer_id.visible = 0 ' + &
//									'excp_customer_name.visible = 0 ' + &
//										'excp_customer_id_t.visible = 0')
//

//iw_this = this




end event

event ue_get_data;call super::ue_get_data;
Choose Case as_value
	Case 'customer_type'
		message.StringParm = dw_header.getitemstring (1, "customer_type")
	Case 'customer_id'
		message.StringParm = dw_header.getitemstring (1, "customer_id")
	Case 'customer_name'
		message.StringParm = dw_header.getitemstring (1, "customer_name")
	Case 'div_code'
		message.StringParm = dw_header.getitemstring (1, "div_code")	
	Case 'date_option'
		message.StringParm = dw_header.getitemstring (1, "date_option")	
	Case 'from_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'from_date'), 'mm/dd/yyyy')
	Case 'to_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'to_date'), 'mm/dd/yyyy')	
	Case 'sales_order_id'
		message.StringParm = dw_header.getitemstring (1, "sales_order_id")	
	Case 'inquire_option'
		message.StringParm = dw_header.getitemstring (1, "inquire_option")	
	Case 'line_option'
//		message.StringParm = dw_header.getitemstring (1, "line_option")	
		message.StringParm = dw_line_choice.getitemstring (1, "choice")	
//	Case 'customer_name'
//		message.StringParm = dw_header.getitemstring (1, "customer_name")	
End choose
end event

type rb_deselect from radiobutton within w_edi_review_orders
integer x = 1902
integer y = 544
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Deselect"
boolean checked = true
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_detail.SetItem(li_Counter, 'accept_change', 'N')
Next	
end event

type rb_select from radiobutton within w_edi_review_orders
integer x = 1902
integer y = 448
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Select All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

String ls_customer

u_string_functions	lu_string

ll_RowCount = dw_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	ls_customer = dw_detail.GetItemString(li_Counter, 'shipto_customer_id')
	if Not lu_string.nf_IsEmpty(ls_customer) then
		dw_detail.SetItem(li_Counter, 'accept_change', 'Y')
	else
		dw_detail.SetItem(li_Counter, 'accept_change', 'N')
	end if
Next	
end event

type cb_sort from commandbutton within w_edi_review_orders
integer x = 1170
integer y = 384
integer width = 343
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Sort"
end type

event clicked;dw_detail.SetReDraw(False)
dw_detail.SelectRow(0, False)
dw_detail.SetSort("sequence_number A, system D")
dw_detail.Sort()
dw_detail.SetReDraw(True)
end event

type dw_line_choice from u_base_dw_ext within w_edi_review_orders
integer x = 110
integer y = 384
integer width = 768
integer height = 320
integer taborder = 10
string dataobject = "d_edi_order_line_choice"
boolean border = false
end type

event constructor;call super::constructor;dw_line_choice.InsertRow(0)

end event

event itemchanged;call super::itemchanged;String		ls_row_option, &
				ls_filter

dw_detail.SetReDraw(False)
ls_row_option = data
If ls_row_option = 'U' Then
	dw_detail.SetFilter("")
	dw_detail.Filter()
	ls_filter = "(match_found_ind = 'N')" 
	dw_detail.SetFilter(ls_filter)
	dw_detail.Filter()
Else
	If ls_row_option = 'I' Then
		dw_detail.SetFilter("")
		dw_detail.Filter()
		ls_filter = "(invoice_status = 'W')" 
		dw_detail.SetFilter(ls_filter)
		dw_detail.Filter()
	else	
		dw_detail.SetFilter("")
		dw_detail.Filter()
	End if
end if

dw_detail.SetSort("sequence_number A, system D")
dw_detail.Sort()
dw_detail.SetReDraw(True)
end event

type dw_detail from u_base_dw_ext within w_edi_review_orders
event ue_set_hspscroll ( )
integer x = 37
integer y = 832
integer width = 3639
integer height = 1344
integer taborder = 40
string dataobject = "d_edi_orders_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_set_hspscroll();Int	li_position, &
		li_position2


if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	li_position = Integer(This.Object.system.X) + &
						Integer(This.Object.system.Width) + 15
	li_position2 = Integer(This.Object.system.X) + &
						Integer(This.Object.system.Width) + 15
	
	This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
	This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
end if
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus
int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row, &
				ll_boxes, ll_wgt, &
				ll_prod_avg_wgt, & 
			 	ll_sequence_number, &
				ll_avg_wgt, ll_prev_avg_wgt, &
				ll_nbrrows, &
				ll_rtn

string		ls_GetText, &
				ls_tem, ls_temp, &
				ls_sequence_number, &
				ls_sequence, &
				ls_searchstring, &
				ls_system
Boolean		lb_changed
				
Date			ldt_temp	
//
is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0
ll_nbrrows = dw_detail.RowCount()
//CHOOSE CASE is_ColName
//	CASE "sequence_number"
//		If Not IsNumber(ls_GetText) Then
//			iw_frame.SetMicroHelp("Sequnce Number must be a number")
//			this.setfocus( )
//			this.setcolumn("sequence_number") 
//			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
//			return 1
//		End if
//		if Integer(ls_GetText) < 0 then
//			iw_frame.SetMicroHelp("Sequnce Number must be a greater than zero")
//			this.setfocus( )
//			this.setcolumn("sequence_number") 
//			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
//			return 1
//		End if
//END CHOOSE
//
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
			This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF
////
parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//
return 0
end event

event constructor;call super::constructor;ib_updateable = True
end event

type dw_header from u_base_dw_ext within w_edi_review_orders
integer x = 37
integer width = 3255
integer height = 384
integer taborder = 0
string dataobject = "d_edi_orders_header"
boolean border = false
end type

event constructor;call super::constructor;dw_header.InsertRow(0)
end event

type gb_1 from groupbox within w_edi_review_orders
integer x = 1865
integer y = 384
integer width = 402
integer height = 256
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
end type

