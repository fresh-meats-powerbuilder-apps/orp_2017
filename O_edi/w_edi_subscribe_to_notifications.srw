HA$PBExportHeader$w_edi_subscribe_to_notifications.srw
forward
global type w_edi_subscribe_to_notifications from w_base_sheet_ext
end type
type dw_notifications_detail from u_base_dw_ext within w_edi_subscribe_to_notifications
end type
type dw_scroll_to_salesperson from u_base_dw_ext within w_edi_subscribe_to_notifications
end type
type dw_header from u_base_dw_ext within w_edi_subscribe_to_notifications
end type
end forward

global type w_edi_subscribe_to_notifications from w_base_sheet_ext
integer width = 3104
integer height = 1896
string title = "Subscribe to Notifications"
dw_notifications_detail dw_notifications_detail
dw_scroll_to_salesperson dw_scroll_to_salesperson
dw_header dw_header
end type
global w_edi_subscribe_to_notifications w_edi_subscribe_to_notifications

type variables
boolean inq_flag, error_found

s_error	istr_error_info


u_ws_orp1 		iu_ws_orp1

long		il_selected_rows, &
 			il_product_seq

INT		ii_row_count

String	    is_customer_inq, &
			is_update_string, &
			is_columnname, &
			is_colname

Boolean  ib_modified = false, &
			ib_updating , &
			ib_reinquire, &
			ib_retrieve
			
DataStore ids_sales_person









	



end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
public subroutine wf_delete ()
public function boolean wf_update_modify (long al_row, character ac_status_ind)
public function boolean wf_unstring_output (string as_output_string)
end prototypes

public function boolean wf_retrieve ();
Boolean	lb_show_message
String		ls_input, &
				ls_output_string, &
				ls_row_option, &
				ls_filter, &
				ls_notification_id, &
				ls_notification_desc, &
				ls_salesperson_id
				
Integer		li_rtn

Long		ll_value


TriggerEvent("closequery")

ls_input = Message.StringParm

//format for header
	
If Not ib_ReInquire Then
	OpenWithParm(w_edi_subscribe_to_notifications_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_notification_id = dw_header.GetItemString( 1, "notification_id")
If IsNull(ls_notification_id) Then
	ls_notification_id = ""
End If


// format for detail
// call to RPC needed to populate the dw_notifications_detail

ls_input =  'D' + '~t' + &
				'ORP32' + '~t' + &
    			ls_notification_id + '~r~n'
//			  
istr_error_info.se_function_name = "wf_retrieve"
//
dw_notifications_detail.Reset()
li_rtn = iu_ws_orp1.nf_orpo09fr(istr_error_info, ls_input, ls_output_string)
ib_retrieve = True
dw_notifications_detail.ImportString(ls_output_string)
dw_notifications_detail.ResetUpdate()

ll_value = dw_notifications_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if
SetMicroHelp(String(ll_value) + " rows retrieved")
IF ll_value > 0 THEN
	dw_notifications_detail.SetSort("salesperson_id")
	dw_notifications_detail.Sort()	
END IF


ll_value = dw_notifications_detail.RowCount()
if ll_value > 0 then
	ls_salesperson_id = dw_notifications_detail.getitemstring(ll_value, "salesperson_id")
	if ls_salesperson_id > ' ' then
		wf_addrow()
	end if
else
	wf_addrow()
end if


dw_notifications_detail.ScrollToRow(ll_value + 1)
dw_notifications_detail.SetColumn( "salesperson_id" )
dw_notifications_detail.TriggerEvent("RowFocusChanged")

dw_notifications_detail.ResetUpdate()
dw_notifications_detail.SetReDraw(True)
This.SetRedraw(True)


// Salesperson ID
dw_scroll_to_salesperson.InsertRow(1)

Return TRUE


end function

public function boolean wf_addrow ();Long		ll_value
String	ls_doc_id

u_project_functions	lu_project_functions

If not lu_project_functions.nf_issalesmgr() Then
	return True
end if

ll_value = dw_notifications_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

ls_doc_id = dw_header.GetItemString(1, "notification_id")
dw_notifications_detail.insertrow(0)
ll_value = ll_value + 1

dw_notifications_detail.SetItem(ll_value, "parm_id", ls_doc_id)
dw_notifications_detail.SetItem(ll_value, "parm_system", 'ORP32')
dw_notifications_detail.SetItem(ll_value, "salesperson_id", ' ')
dw_notifications_detail.SetItem(ll_value, "salesperson_name", ' ')
dw_notifications_detail.SetItem(ll_value, "subscriber_id", ' ')
dw_notifications_detail.SetItem(ll_value, "subscriber_name", ' ')
dw_notifications_detail.SetItem(ll_value, "update_flag", 'A')


//
dw_notifications_detail.SetColumn("salesperson_id")
dw_notifications_detail.uf_changerowstatus(ll_value, New!)

Return TRUE
end function

public function boolean wf_update ();
String	ls_input, &
			ls_output_string_in, &
			ls_output_string, &
			ls_header_string, &
			ls_Updateflag, &
			ls_output_string_returned, &
			ls_MicroHelp, &
  			ls_salesperson_id, &
			ls_update_string  


Integer	li_rtn, &
			li_count
			
Long		ll_rtn
			
			
			
Long		ll_rowcount, &
			ll_pos, &
			ll_col, &
			ll_row, &
			ll_modrows, &
			ll_delrows

char		lc_status_ind

Boolean	lb_error_found

u_string_functions	lu_string_functions

if dw_notifications_detail.AcceptText() = -1 then
	Return False
end if

SetPointer(HourGlass!)
This.SetRedraw(false)

is_update_string = ''

ll_modrows = dw_notifications_detail.modifiedcount()
ll_delrows = dw_notifications_detail.deletedcount()

		  
// get all deleted records from buffer
For li_count = 1 to ll_delrows

	lc_status_ind = 'D'
	
	if not this.wf_update_modify(li_count, lc_status_ind) then
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
Next


// get all new & modified rows from buffer
ll_row = 0

for li_count = 1 to ll_modrows

	ll_Row = dw_notifications_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then 
			This.SetRedraw(True) 
			Return False
		end if	

	END IF
	
	lc_status_ind = 'A'
		
	if not this.wf_update_modify(ll_row, lc_status_ind) then
		ib_updating = false
		return false
	end if	
next


istr_error_info.se_function_name = "wf_update"


//  mainframe call
// call orpo10fr

	li_rtn =  iu_ws_orp1.nf_orpo10fr(istr_error_info, &
														is_update_string) 		
														

// message does Not match the salesperson location 
// If IsNull( istr_error_info.se_Message ) then
//If LenA(TRIM( istr_error_info.se_Message)) > 0  then


If li_rtn = 2  then
	MessageBox("Error", istr_error_info.se_message)
	This.SetRedraw(True)
	SetPointer(Arrow!)	
	return false
end if	
	
	
//if li_rtn <> 0 then
	//This.SetRedraw(True)
	//SetPointer(Arrow!)																		
	//return false
//end if

dw_notifications_detail.ResetUpdate()
ib_ReInquire = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")

dw_notifications_detail.SetFocus()
dw_notifications_detail.SetReDraw(True)


Return True


end function

public function boolean wf_validate (long al_row);int			li_rc				

long			ll_RowCount, &
				ll_find_row, &
				ll_sequence_number, &
				ll_rtn, &
				ll_nbrrows, &
				ll_current_row, &
				ll_delv_date_range

string		ls_temp, &
				ls_doc_id, &
				ls_salesperson_id, &
				ls_searchstring, &
				ls_subscriber_id 
				
Boolean		lb_changed
				
ll_nbrrows = dw_notifications_detail.RowCount()
ll_current_row = al_row

ls_salesperson_id = dw_notifications_detail.getitemstring(al_row, "salesperson_id")

if ls_salesperson_id = ' ' then
	iw_frame.SetMicroHelp("Salesperson ID cannot be blank")
	dw_notifications_detail.SetRedraw(False)
	dw_notifications_detail.ScrollToRow(al_row)
	dw_notifications_detail.SetColumn("salesperson_id")
	dw_notifications_detail.SetRow(al_row)
	dw_notifications_detail.SelectRow(ll_rtn, True)
	dw_notifications_detail.SelectRow(al_row, True)
     dw_notifications_detail.SetRedraw(True)
 Return False
End If


ls_subscriber_id= dw_notifications_detail.getitemstring(al_row, "subscriber_id")

if ls_subscriber_id = ' ' then
		//if isNull(ls_tsr) then
		iw_frame.SetMicroHelp(" USER ID cannot be blank")
		dw_notifications_detail.SetRedraw(False)
		dw_notifications_detail.ScrollToRow(al_row)
		dw_notifications_detail.SetColumn("subscriber_id")
	     dw_notifications_detail.SetRow(al_row)
		dw_notifications_detail.SelectRow(al_row, True)
	     dw_notifications_detail.SetRedraw(True)
   Return False	
 End if



dw_notifications_detail.SelectRow(0, False)
	
Return True
end function

public subroutine wf_delete ();long	ll_deleted_row, ll_delrows

ll_deleted_row = dw_notifications_detail.getselectedrow(0)


DO WHILE ll_deleted_row > 0
	dw_notifications_detail.deleterow(ll_deleted_row)
	ll_deleted_row = dw_notifications_detail.getselectedrow(ll_deleted_row)
	
LOOP

ll_delrows = dw_notifications_detail.deletedcount()




end subroutine

public function boolean wf_update_modify (long al_row, character ac_status_ind);dwbuffer	ldwb_buffer

string	ls_start_date, &
			ls_end_date, ls_product_code, ls_default_ind, &
			ls_cust_prod_code, ls_dept_code, ls_upc_code, ls_prev_cust_prod, &
			ls_parm_system, ls_parm_id, &
			ls_salesperson_id, ls_salesperson_name ,&
			ls_subscriber_id, ls_subscriber_name, ls_update_flag
			
			
			
Long		ll_rtn, &
			ll_nbrrows
			
ll_nbrrows = dw_notifications_detail.RowCount()

if ac_status_ind = 'D' then
	ldwb_buffer = delete!
else
	ldwb_buffer = primary!
end if
ls_parm_system = dw_notifications_detail.getitemstring(al_row, "parm_system",  ldwb_buffer, false)
ls_parm_id = dw_notifications_detail.getitemstring(al_row, "parm_id",  ldwb_buffer, false)
ls_salesperson_id = dw_notifications_detail.getitemstring(al_row, "salesperson_id",  ldwb_buffer, false)
ls_salesperson_name = dw_notifications_detail.getitemstring(al_row, "salesperson_name",  ldwb_buffer, false)
ls_subscriber_id = dw_notifications_detail.getitemstring(al_row, "subscriber_id",  ldwb_buffer, false)
ls_subscriber_name = dw_notifications_detail.getitemstring(al_row, "subscriber_name",  ldwb_buffer, false)
ls_update_flag = dw_notifications_detail.getitemstring(al_row, "update_flag",  ldwb_buffer, false)


is_update_string += &
	trim(dw_notifications_detail.getitemstring (al_row, "parm_system",  ldwb_buffer, false)) + "~t" + &
	trim(dw_notifications_detail.getitemstring (al_row, "parm_id",  ldwb_buffer, false)) + "~t" + &
	trim(dw_notifications_detail.getitemstring (al_row, "salesperson_id",  ldwb_buffer, false))+ "~t" + &
	trim(dw_notifications_detail.getitemstring (al_row, "salesperson_name",  ldwb_buffer, false)) + "~t" + &
	trim(dw_notifications_detail.getitemstring (al_row, "subscriber_id",  ldwb_buffer, false)) + "~t" + &
	trim(dw_notifications_detail.getitemstring (al_row, "subscriber_name",  ldwb_buffer, false)) + "~t" + &
	ac_status_ind + "~r~n"	
	
	
return true


end function

public function boolean wf_unstring_output (string as_output_string);//Date					ldt_start_date, &
//						ldt_temp
//
//Long					ll_rtn, &
//						ll_nbrrows
//						
//
//String				ls_cust_product, &
//						ls_fm_product, &
//						ls_customer_id, &
//						ls_searchstring, &
//						ls_temp, &
//						ls_start_date
//						
//
//u_string_functions	lu_string_functions
//					
//ll_nbrrows = dw_notifications_detail.RowCount()
//
//dw_notifications_detail.SelectRow(0,False)
//
////find the row that matches the fm product, cust product and start date
//
//ll_nbrrows = dw_notifications_detail.RowCount()
////ls_customer_id = lu_string_functions.nf_gettoken(as_output_string, '~t')
////ls_fm_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
////ls_cust_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
////ls_start_date = lu_string_functions.nf_gettoken(as_output_string, '~t')
//
// ls_
//
//
//ls_SearchString = 	"customer_id = '"+ ls_customer_id +&
//							"' and product_code = '"+ ls_fm_product +&
//							"' and cust_prod_code = '" + ls_cust_product + "'" +&
//							" and start_date = date('" + ls_start_date+ "')" 
//	
//ll_rtn = dw_edi_excp_detail.Find  &
//					( ls_SearchString, 1, ll_nbrrows)
//		
//If ll_rtn > 0 Then
//	dw_edi_excp_detail.SetRedraw(False)
//	dw_edi_excp_detail.ScrollToRow(ll_rtn)
//	dw_edi_excp_detail.SetRow(ll_rtn)
//	dw_edi_excp_detail.SelectRow(ll_rtn, True)
//	dw_edi_excp_detail.SetRedraw(True)
//End If
//
//as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

on w_edi_subscribe_to_notifications.create
int iCurrent
call super::create
this.dw_notifications_detail=create dw_notifications_detail
this.dw_scroll_to_salesperson=create dw_scroll_to_salesperson
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_notifications_detail
this.Control[iCurrent+2]=this.dw_scroll_to_salesperson
this.Control[iCurrent+3]=this.dw_header
end on

on w_edi_subscribe_to_notifications.destroy
call super::destroy
destroy(this.dw_notifications_detail)
destroy(this.dw_scroll_to_salesperson)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;wf_retrieve()

end event

event open;call super::open;iu_ws_orp1 = Create u_ws_orp1

dw_notifications_detail.InsertRow(0)
end event

event close;call super::close;Destroy iu_ws_orp1
end event

event ue_set_data;call super::ue_set_data;choose case as_data_item
	case "notification_id"
		dw_header.setItem(1, 'notification_id', as_value)
      case "doc_desc"
		dw_header.setItem(1, 'description', as_value)
end choose
end event

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_deleterow")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")


u_project_functions	lu_project_functions

If lu_project_functions.nf_issalesmgr() Then
	iw_frame.im_menu.mf_enable("m_save")
	iw_frame.im_menu.mf_enable("m_delete")
Else
	iw_frame.im_menu.mf_disable("m_save")
	iw_frame.im_menu.mf_disable("m_delete")
End IF
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")
end event

type dw_notifications_detail from u_base_dw_ext within w_edi_subscribe_to_notifications
integer y = 384
integer width = 2450
integer height = 1152
integer taborder = 30
string dataobject = "d_edi_subscribe_to_notifications"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;is_selection = '2'
ib_updateable = true
ib_NewRowOnChange = False
end event

event itemchanged;call super::itemchanged;//dwItemStatus	le_RowStatus
//int			li_rc, &
//				li_seconds,li_temp
//
//long			ll_char_row, &
//				ll_source_row, &
//				ll_plant_row, &
//				ll_RowCount, &
//				ll_find_row, &
//				ll_boxes, ll_wgt, &
//				ll_prod_avg_wgt, & 
//			 	ll_sequence_number, &
//				ll_avg_wgt, ll_prev_avg_wgt, &
//				ll_nbrrows, &
//				ll_rtn
//
//string		ls_GetText, &
//				ls_tem, ls_temp, &
//				ls_searchstring, &
//				ls_customer_name, &
//				
//				ls_salesperson_id, &
//				
//				ls_customer_type
//				
//Boolean		lb_changed
//				
//Date			ldt_temp	
//

// is_ColName = GetColumnName()
//ls_GetText = data
//ll_source_row	= GetRow()
//il_ChangedRow = 0
//ll_nbrrows = dw_notifications_detail.RowCount()


//CHOOSE CASE is_ColName
//	CASE "costumer_id"
//		ls_customer_type = This.GetItemString(row, "customer_type")
//		CHOOSE CASE ls_customer_type
//			CASE 'S'
//				If ls_GetText > '  ' then
//					SELECT customers.short_name
//						INTO :ls_customer_name
//						FROM customers
//						WHERE customers.customer_id = :ls_GetText
//						USING SQLCA ;
//					IF isnull(ls_customer_name) THEN
//						ls_customer_name = ' '
//					END IF 
//					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
//				End if
//			CASE 'B'
//				If ls_GetText > '  ' then
//					SELECT billtocust.name
//						INTO :ls_customer_name
//						FROM billtocust
//						WHERE billtocust.bill_to_id = :ls_GetText
//						USING SQLCA ;
//					IF isnull(ls_customer_name) THEN
//						ls_customer_name = ' '
//					END IF 
//					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
//				End if	
//			CASE 'C'
//				If ls_GetText > '  ' then
//					SELECT corpcust.name
//						INTO :ls_customer_name
//						FROM corpcust
//						WHERE corpcust.corp_id = :ls_GetText
//						USING SQLCA ;
//					IF isnull(ls_customer_name) THEN
//						ls_customer_name = ' '
//					END IF 
//					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
//				End if				
//				
//		END CHOOSE		
//		
//	CASE "customer_type"
//		ls_customer_type = data
//		ls_customer_id = This.GetItemString(row, "customer_id")
//		
//		CHOOSE CASE ls_customer_type
//			CASE 'S'
//				If ls_GetText > '  ' then
//					SELECT customers.short_name
//						INTO :ls_customer_name
//						FROM customers
//						WHERE customers.customer_id = :ls_customer_id
//						USING SQLCA ;
//					IF isnull(ls_customer_name) THEN
//						ls_customer_name = ' '
//					END IF 
//					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
//				End if
//			CASE 'B'
//				If ls_GetText > '  ' then
//					SELECT billtocust.name
//						INTO :ls_customer_name
//						FROM billtocust
//						WHERE billtocust.bill_to_id = :ls_customer_id
//						USING SQLCA ;
//					IF isnull(ls_customer_name) THEN
//						ls_customer_name = ' '
//					END IF 
//					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
//				End if	
//			CASE 'C'
//				If ls_GetText > '  ' then
//					SELECT corpcust.name
//						INTO :ls_customer_name
//						FROM corpcust
//						WHERE corpcust.corp_id = :ls_customer_id
//						USING SQLCA ;
//					IF isnull(ls_customer_name) THEN
//						ls_customer_name = ' '
//					END IF 
//					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
//				End if				
//				
//		END CHOOSE				
//END CHOOSE

//		   
//			iw_frame.SetMicroHelp("Sequnce Number must be a number")
//			this.setfocus( )
//			this.setcolumn("sequence_number") 
//			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
//			return 1
//		End if
//		if Integer(ls_GetText) < 0 then
//			iw_frame.SetMicroHelp("Sequnce Number must be a greater than zero")
//			this.setfocus( )
//			this.setcolumn("sequence_number") 
//			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
//			return 1
//		End if
//END CHOOSE
//


//// Update the Update_Flag column so the RPC will know how to update the row
//IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
//	IF Long(This.Describe("update_flag.id")) > 0 THEN
//		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
//			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
//			CHOOSE CASE le_RowStatus
//			CASE NewModified!, New!
//			This.SetItem(ll_source_row, "update_flag", "A")
//			CASE DataModified!, NotModified!
//				This.SetItem(ll_source_row, "update_flag", "U")
//			END CHOOSE		
//		End if
//	END IF
//END IF
//////
//parent.PostEvent("ue_postitemchanged")
//iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//dw_notifications_detail.AcceptText()
//ll_rowcount = dw_notifications_detail.RowCount()
//ls_customer_id = dw_notifications_detail.getitemstring(ll_rowcount, "customer_id")
//if ls_salesperson_id > ' ' then
//	wf_addrow()
//end if
//
////
//return 0
end event

event itemfocuschanged;call super::itemfocuschanged;if ib_retrieve Then Return 1
is_columnname = dwo.name

if is_columnname = 'salesperson_id' then
	This.SetText(Trim(this.GetText()))
	This.SelectText(1,len(trim(this.GetText())))
end if

end event

event ue_postconstructor;call super::ue_postconstructor;//DataWindowChild ldwc_temp
//
//Long		ll_value
//String	ls_customer_id, &
//      	ls_Location
//
//
//
This.InsertRow(0)
//
//This.GetChild('customer_id', ldwc_temp)
//iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)
//
//
//
//ids_sales_person = CREATE DATASTORE
//ids_sales_person.DataObject = 'd_sales_people'
//ls_Location = message.is_smanlocation
//ids_sales_person.SetTransObject(SQLCA)
//ids_sales_person.Retrieve( ls_Location, ls_Location)
//
//This.GetChild('tsr', ldwc_temp)
//ids_sales_person.ShareData( ldwc_temp)


end event

type dw_scroll_to_salesperson from u_base_dw_ext within w_edi_subscribe_to_notifications
integer x = 37
integer y = 256
integer width = 2450
integer height = 96
integer taborder = 20
string dataobject = "d_edi_subscribe_to_notifications_header"
boolean border = false
end type

event editchanged;call super::editchanged;Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
Integer 	li_counter


ll_row_count = dw_notifications_detail.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return 

Do While li_counter <= ll_Row_count 
	dw_notifications_detail.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop

if data > ' ' then
else
	return
end if

ll_row = dw_notifications_detail.GetSelectedRow(row)
if ll_row > 0 then
	dw_notifications_detail.SelectRow(ll_row, False)
end if
ll_row = dw_notifications_detail.Find("salesperson_id >= '" + data + "'",1,dw_notifications_detail.RowCount()+1)

If ll_row > 0 Then 
	dw_notifications_detail.SetRedraw(False)
	dw_notifications_detail.ScrollToRow(ll_row)
	dw_notifications_detail.SelectRow(ll_row, True)
	dw_notifications_detail.SetRow(ll_row + 1)
	dw_notifications_detail.SetRedraw(True)
End If

ll_first_row = Long(dw_notifications_detail.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_notifications_detail.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_notifications_detail.SetRedraw(False)
	dw_notifications_detail.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_notifications_detail.ScrollToRow(ll_row)
	dw_notifications_detail.SetRow(ll_row + 1)
	dw_notifications_detail.SetRedraw(True)
End If
end event

type dw_header from u_base_dw_ext within w_edi_subscribe_to_notifications
integer x = 37
integer width = 2414
integer height = 224
integer taborder = 10
string dataobject = "d_edi_subscribe_to_notifications_detail_header"
boolean border = false
end type

event constructor;call super::constructor;dw_header.InsertRow(0)
end event

