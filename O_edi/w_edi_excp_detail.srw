HA$PBExportHeader$w_edi_excp_detail.srw
$PBExportComments$Edi Customer Detail Prodcut Update
forward
global type w_edi_excp_detail from w_netwise_sheet
end type
type dw_edi_prod_detail_choice from u_base_dw_ext within w_edi_excp_detail
end type
type dw_edi_prod_detail_header from u_base_dw_ext within w_edi_excp_detail
end type
type dw_edi_excp_detail from u_base_dw_ext within w_edi_excp_detail
end type
end forward

global type w_edi_excp_detail from w_netwise_sheet
integer width = 2816
integer height = 1652
string title = "EDI Product Cross Reference Customer Exception Detail"
boolean maxbox = false
long backcolor = 12632256
event ue_getcustomerid pbm_custom63
event ue_systemcommand pbm_syscommand
event ue_open_order_confirmation ( )
event ue_getdata ( string as_stringvalue )
event ue_pa_summary ( )
dw_edi_prod_detail_choice dw_edi_prod_detail_choice
dw_edi_prod_detail_header dw_edi_prod_detail_header
dw_edi_excp_detail dw_edi_excp_detail
end type
global w_edi_excp_detail w_edi_excp_detail

type variables
s_error		istr_error_info

u_orp003		iu_orp003
u_orp001		iu_orp001
u_ws_orp1		iu_ws_orp1

long		il_selected_rows

String		is_MicroHelp, &
		is_customer_inq, &
		is_update_string, &
		is_columnname

Boolean  ib_modified = false, &
		ib_updating , &
		ib_reinquire, &
		ib_retrieve
	
end variables

forward prototypes
public subroutine wf_delete ()
public subroutine wf_rightclick ()
public function boolean wf_addrow ()
public function boolean wf_unstring_output (string as_output_string)
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function boolean wf_validate (long al_row)
public function boolean wf_update_modify (long al_row, character ac_status_ind)
end prototypes

on ue_getcustomerid;call w_netwise_sheet::ue_getcustomerid;Message.StringParm = ''
end on

public subroutine wf_delete ();long	ll_deleted_row

ll_deleted_row = dw_edi_excp_detail.getselectedrow(0)


DO WHILE ll_deleted_row > 0
	dw_edi_excp_detail.deleterow(ll_deleted_row)
	ll_deleted_row = dw_edi_excp_detail.getselectedrow(ll_deleted_row)
LOOP


end subroutine

public subroutine wf_rightclick ();Constant Date ld_date = Today() 

m_edi_prod_popup NewMenu

u_project_functions lu_project_functions

if lu_project_functions.nf_isscheduler() then return

NewMenu = CREATE m_edi_prod_popup
NewMenu.m_file.m_customerexceptions.Visible = FALSE

NewMenu.M_file.m_edi_cross_ref_report.enabled = TRUE

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu
end subroutine

public function boolean wf_addrow ();Long		ll_value


ll_value = dw_edi_excp_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

dw_edi_excp_detail.insertrow(0)
ll_value = ll_value + 1
dw_edi_excp_detail.SetItem(ll_value, "customer_id", ' ')
dw_edi_excp_detail.SetColumn("customer_id")
dw_edi_excp_detail.SetFocus()
dw_edi_excp_detail.SetItem(ll_value, "start_date", today())
dw_edi_excp_detail.uf_changerowstatus(ll_value, New!)

Return TRUE
end function

public function boolean wf_unstring_output (string as_output_string);Date					ldt_start_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows
						

String				ls_cust_product, &
						ls_fm_product, &
						ls_customer_id, &
						ls_searchstring, &
						ls_temp, &
						ls_start_date
						

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_edi_excp_detail.RowCount()

dw_edi_excp_detail.SelectRow(0,False)

//find the row that matches the fm product, cust product and start date

ll_nbrrows = dw_edi_excp_detail.RowCount()
ls_customer_id = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_fm_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_cust_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_start_date = lu_string_functions.nf_gettoken(as_output_string, '~t')

ls_SearchString = 	"customer_id = '"+ ls_customer_id +&
							"' and product_code = '"+ ls_fm_product +&
							"' and cust_prod_code = '" + ls_cust_product + "'" +&
							" and start_date = date('" + ls_start_date+ "')" 
	
ll_rtn = dw_edi_excp_detail.Find  &
					( ls_SearchString, 1, ll_nbrrows)
		
If ll_rtn > 0 Then
	dw_edi_excp_detail.SetRedraw(False)
	dw_edi_excp_detail.ScrollToRow(ll_rtn)
	dw_edi_excp_detail.SetRow(ll_rtn)
	dw_edi_excp_detail.SelectRow(ll_rtn, True)
	dw_edi_excp_detail.SetRedraw(True)
End If

as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public function boolean wf_update ();String	ls_input, &
			ls_output_string_in, &
			ls_output_string, &
			ls_header_string, &
			ls_Updateflag, &
			ls_output_string_returned, &
			ls_MicroHelp, &
  			ls_customer_id
//	//		ls_business_rules, &

//			ls_product_code, &
//			ls_default_ind, &
//			ls_cust_prod_code, &
//			ls_dept_code, &
//			ls_upc_code, &
// 			ls_searchstring, &
//			ls_cust_prod

//Date		ldt_start_date, &
//			ldt_end_date, &
//			ldt_found_start_date, &
//			ldt_found_end_date
//			
Integer	li_rtn, &
			li_count
			
Long		ll_rtn
			
			
			
Long		ll_rowcount, &
			ll_pos, &
			ll_col, &
			ll_row, &
			ll_modrows, &
			ll_delrows

char		lc_status_ind

Boolean	lb_error_found

u_string_functions	lu_string_functions

if dw_edi_excp_detail.AcceptText() = -1 then
	Return False
end if

SetPointer(HourGlass!)
This.SetRedraw(false)

is_update_string = ''

ll_modrows = dw_edi_excp_detail.modifiedcount()
ll_delrows = dw_edi_excp_detail.deletedcount()

ls_header_string = 'X' + '~t' + &
			  dw_edi_prod_detail_header.GetItemString( 1, "customer_id") + '~r~n'
			  
// get all deleted records from buffer
For li_count = 1 to ll_delrows

	lc_status_ind = 'D'
	
	if not this.wf_update_modify(li_count, lc_status_ind) then
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if

Next

// get all new & modified rows from buffer
ll_row = 0

for li_count = 1 to ll_modrows

	ll_Row = dw_edi_excp_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then 
			This.SetRedraw(True) 
			Return False
		end if	
//	ELSE
//		ll_Row = ll_modRows + 1
	END IF
	
//LOOP
	choose case dw_edi_excp_detail.getitemstatus(ll_row, 0, primary!)
		case newmodified!
			lc_status_ind = 'A'
		case datamodified!
			lc_status_ind = 'M'
	end choose
	
	if not this.wf_update_modify(ll_row, lc_status_ind) then
		ib_updating = false
		return false
	end if
//end if	
next

ls_output_string_in = is_update_string
//ls_output_string_in = lu_string_functions.nf_BuildUpdateString( dw_edi_excp_detail)
istr_error_info.se_function_name = "wf_update"


//  mainframe call
//li_rtn = iu_orp001.nf_orpo08br_edi_cust_prod_det_upd (istr_error_info, ls_output_string_in, ls_output_string, ls_header_string)
li_rtn = iu_ws_orp1.nf_orpo08fr(istr_error_info, ls_output_string_in, ls_output_string, ls_header_string)																			
if li_rtn <> 0 then
	if ls_output_string > '' Then
		wf_unstring_output(ls_output_string)
	End If
	This.SetRedraw(True)
	SetPointer(Arrow!)																		
	return false
end if

dw_edi_excp_detail.ResetUpdate()
ib_ReInquire = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")
//dw_edi_excp_detail.ResetUpdate()
dw_edi_excp_detail.SetColumn("product_code")

ls_customer_id = dw_edi_prod_detail_header.GetItemString( 1, "customer_id")
SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)

dw_edi_excp_detail.SetFocus()
dw_edi_excp_detail.SetReDraw(True)

//SetPointer(Arrow!)																		
//dw_edi_excp_detail.SelectRow(0,False)
//
Return True

end function

public function boolean wf_retrieve ();Boolean	lb_show_message
String		ls_input, &
				ls_output_string, &
				ls_row_option, &
				ls_filter
				
Integer		li_rtn

Long		ll_value


TriggerEvent("closequery")

ls_input = Message.StringParm

If Not ib_ReInquire Then
	OpenWithParm(w_edi_excp_detail_inq, This)
	ls_input = Message.StringParm
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if

IF Message.StringParm = "Cancel" then return False

If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
This.SetRedraw(False)

ls_input = 'X' + '~t' + &
			  dw_edi_prod_detail_header.GetItemString( 1, "customer_id") + '~t' + &
			  dw_edi_prod_detail_header.GetItemString( 1, "excp_customer_id") + '~r~n'
istr_error_info.se_function_name = "wf_retrieve"

dw_edi_excp_detail.Reset()
//li_rtn = iu_orp001.nf_orpo07br_edi_cust_prod_det_inq (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp1.nf_orpo07fr(istr_error_info, ls_input, ls_output_string)

ib_retrieve = True
dw_edi_excp_detail.ImportString(ls_output_string)
dw_edi_excp_detail.ResetUpdate()


if li_rtn = 0 Then
	ls_row_option = dw_edi_prod_detail_choice.GetItemString(1, "choice")
	If ls_row_option = 'C' Then
		ls_filter = "(end_date > RelativeDate(Today(), -1)) and (product_code > ' ')" 
		dw_edi_excp_detail.SetFilter(ls_filter)
		dw_edi_excp_detail.Filter()
	Else
		dw_edi_excp_detail.SetFilter("")
		dw_edi_excp_detail.Filter()
	End if
End if

ll_value = dw_edi_excp_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

dw_edi_excp_detail.ResetUpdate()
dw_edi_prod_detail_choice.ResetUpdate()

IF ll_value > 0 THEN
	dw_edi_excp_detail.SetFocus()
	dw_edi_excp_detail.ScrollToRow(1)
	dw_edi_excp_detail.SetColumn( "product_code" )
	dw_edi_excp_detail.TriggerEvent("RowFocusChanged")
END IF

dw_edi_excp_detail.SetSort("customer_id, product_code, start_date, end_date")
dw_edi_excp_detail.Sort()
dw_edi_prod_detail_choice.ResetUpdate()
dw_edi_excp_detail.ResetUpdate()
dw_edi_excp_detail.SetReDraw(True)
This.SetRedraw(True)
dw_edi_excp_detail.insertrow(0)
ll_value = ll_value + 1
dw_edi_excp_detail.SetItem(ll_value, "customer_id", ' ')
dw_edi_excp_detail.SetColumn("customer_id")
dw_edi_excp_detail.SetFocus()
dw_edi_excp_detail.SetItem(ll_value, "start_date", today())
dw_edi_excp_detail.ResetUpdate()
dw_edi_excp_detail.uf_changerowstatus(ll_value, New!)
ib_retrieve = False
Return TRUE
end function

public function boolean wf_validate (long al_row);string	ls_customer_id, &
			ls_product_code, &
			ls_default_ind, &
			ls_cust_prod_code, &
			ls_dept_code, &
			ls_upc_code, &
			ls_prev_cust_prod, &
			ls_prev_start_date, &
			ls_searchstring
		
Date		ldt_start_date, &
			ldt_prev_start_date, &
			ldt_end_date
			
			
Long		ll_rtn
			
			
Long		ll_modrows

ll_modrows = dw_edi_excp_detail.modifiedcount()

ls_product_code = dw_edi_excp_detail.getitemstring(al_row, "product_code")
if iw_frame.iu_string.nf_isempty(ls_product_code) then
	iw_frame.setmicrohelp("Product Code is a required field.")
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetColumn("product_code")
	ib_updating = false
	This.SetRedraw(True)
	return false
end if
	
ls_customer_id = dw_edi_excp_detail.getitemstring(al_row, "customer_id")
if iw_frame.iu_string.nf_isempty(ls_product_code) then
	iw_frame.setmicrohelp("Customer ID is a required field.")
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetColumn("customer_id")
	ib_updating = false
	This.SetRedraw(True)
	return false
end if
	
ls_prev_cust_prod = dw_edi_excp_detail.getitemstring(al_row, "prev_cust_prod")
if isNull(ls_prev_cust_prod) then
	dw_edi_excp_detail.setitem(al_row, "prev_cust_prod", ' ')		
end if
	
ldt_prev_start_date = dw_edi_excp_detail.getItemDate(al_row, "start_date")
if isnull(ldt_start_date) then
	dw_edi_excp_detail.setitem(al_row, "prev_start_date", '01/01/0001')	
end if
	
	
ls_cust_prod_code = dw_edi_excp_detail.getitemstring(al_row, "cust_prod_code")
if iw_frame.iu_string.nf_isempty(ls_cust_prod_code) then
	iw_frame.setmicrohelp("Customer Product Code is a required field.")
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetColumn("cust_prod_code")
	ib_updating = false
	This.SetRedraw(True)
	return false
end if
	
ls_dept_code = dw_edi_excp_detail.getitemstring(al_row, "dept_code")
if isNull(ls_dept_code) then
	dw_edi_excp_detail.setitem(al_row, "dept_code", ' ')
end if

ls_upc_code = dw_edi_excp_detail.getitemstring(al_row, "upc_code")
if isNull(ls_upc_code) then
	dw_edi_excp_detail.setitem(al_row, "upc_code", ' ')
end if

ldt_start_date = dw_edi_excp_detail.getItemDate(al_row, "start_date")
if isnull(ldt_start_date) then
	iw_frame.setmicrohelp("Start Date is a required field.")
	This.SetRedraw(False)
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetColumn("start_date")
	dw_edi_excp_detail.SetFocus()
	ib_updating = false
	This.SetRedraw(True)
	return false		
end if
	 
choose case dw_edi_excp_detail.getitemstatus(al_row, 0, primary!)
	case newmodified!
		if ldt_start_date < today() then
			iw_frame.setmicrohelp("Start Date must be equal to or greater than current date.")
			This.SetRedraw(False)
			dw_edi_excp_detail.ScrollToRow(al_row)
			dw_edi_excp_detail.SelectRow(al_row, True)
			dw_edi_excp_detail.SetColumn("start_date")
			dw_edi_excp_detail.SetFocus()
			ib_updating = false
			This.SetRedraw(True)
			return false		
		end if
end choose
	
ldt_end_date = dw_edi_excp_detail.getItemDate(al_row, "end_date")
if isnull(ldt_end_date) then
iw_frame.setmicrohelp("End Date is a required field.")
	This.SetRedraw(False)
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetColumn("end_date")
	dw_edi_excp_detail.SetFocus()
	ib_updating = false
	This.SetRedraw(True)
	return false
end if
	
if ldt_start_date > ldt_end_date then
	iw_frame.setmicrohelp("Start Date must be less than End Date.")
	This.SetRedraw(False)
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetColumn("start_date")
	dw_edi_excp_detail.SetFocus()
	ib_updating = false
	This.SetRedraw(True)
	return false
end if
	
ls_SearchString	= "customer_id = '"+ ls_customer_id +&
						"' and cust_prod_code = '" + ls_cust_prod_code + "'"
							
// Find a matching row excluding the current row.
ll_rtn = iw_frame.iu_string.nf_compare_dates(al_row, ls_SearchString, dw_edi_excp_detail) 

If ll_rtn > 0 Then
	iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
					" same Customer, Customer Product Code and overlapping dates.")					
	dw_edi_excp_detail.SetRedraw(False)
	dw_edi_excp_detail.SelectRow(0, False)
	dw_edi_excp_detail.ScrollToRow(al_row)
	dw_edi_excp_detail.SetColumn("start_date")
	dw_edi_excp_detail.SetRow(al_row)
	dw_edi_excp_detail.SelectRow(ll_rtn, True)
	dw_edi_excp_detail.SelectRow(al_row, True)
	dw_edi_excp_detail.SetRedraw(True)
	Return False
End If
	
Return True
end function

public function boolean wf_update_modify (long al_row, character ac_status_ind);dwbuffer	ldwb_buffer
string	ls_start_date, &
			ls_end_date, ls_product_code, ls_default_ind, &
			ls_cust_prod_code, ls_dept_code, ls_upc_code, ls_prev_cust_prod, &
			ls_customer_id, ls_prev_start_date
			
Long		ll_rtn, &
			ll_nbrrows
			
ll_nbrrows = dw_edi_excp_detail.RowCount()

if ac_status_ind = 'D' then
	ldwb_buffer = delete!
else
	ldwb_buffer = primary!
end if
ls_customer_id = dw_edi_excp_detail.getitemstring(al_row, "customer_id",  ldwb_buffer, false)
ls_start_date = String(dw_edi_excp_detail.GetItemdate(al_row, "start_date"), 'yyyy-mm-dd')
ls_end_date = String(dw_edi_excp_detail.GetItemdate(al_row, "end_date"), 'yyyy-mm-dd')
ls_product_code = trim(dw_edi_excp_detail.getitemstring (al_row, "product_code",  ldwb_buffer, false))
ls_cust_prod_code = trim(dw_edi_excp_detail.getitemstring (al_row, "cust_prod_code",  ldwb_buffer, false))
ls_dept_code = dw_edi_excp_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false)
ls_upc_code = dw_edi_excp_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false)
ls_prev_cust_prod = dw_edi_excp_detail.getitemstring (al_row, "prev_cust_prod",  ldwb_buffer, false)
ls_prev_start_date = String(dw_edi_excp_detail.GetItemdate(al_row, "prev_start_date"), 'yyyy-mm-dd')
is_update_string += &
	dw_edi_excp_detail.getitemstring (al_row, "customer_id",  ldwb_buffer, false) + "~t" + &
	trim(dw_edi_excp_detail.getitemstring (al_row, "product_code",  ldwb_buffer, false)) + "~t" + &
	trim(dw_edi_excp_detail.getitemstring (al_row, "cust_prod_code",  ldwb_buffer, false))+ "~t" + &
	dw_edi_excp_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false) + "~t" + &
	dw_edi_excp_detail.getitemstring (al_row, "upc_code",  ldwb_buffer, false) + "~t" + &
	String(dw_edi_excp_detail.GetItemDate( al_row,"start_date",  ldwb_buffer, false),"YYYY-MM-DD") + "~t" + &
	String(dw_edi_excp_detail.GetItemDate( al_row,"end_date",  ldwb_buffer, false),"YYYY-MM-DD") + "~t" + &
	dw_edi_excp_detail.getitemstring (al_row, "prev_cust_prod",  ldwb_buffer, false) + "~t" + &
	String(dw_edi_excp_detail.GetItemDate( al_row,"prev_start_date",  ldwb_buffer, false),"YYYY-MM-DD") + "~t" + &
	ac_status_ind + "~r~n"	
return true


end function

on w_edi_excp_detail.create
int iCurrent
call super::create
this.dw_edi_prod_detail_choice=create dw_edi_prod_detail_choice
this.dw_edi_prod_detail_header=create dw_edi_prod_detail_header
this.dw_edi_excp_detail=create dw_edi_excp_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_edi_prod_detail_choice
this.Control[iCurrent+2]=this.dw_edi_prod_detail_header
this.Control[iCurrent+3]=this.dw_edi_excp_detail
end on

on w_edi_excp_detail.destroy
call super::destroy
destroy(this.dw_edi_prod_detail_choice)
destroy(this.dw_edi_prod_detail_header)
destroy(this.dw_edi_excp_detail)
end on

event ue_query;call super::ue_query;IF wf_Retrieve() = false Then
		This.SetRedraw( TRUE)
		CLOSE(THIS)
		RETURN
END IF
end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")

end event

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_delete")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")

end event

event deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")

end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
destroy iu_ws_orp1
end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp1 = CREATE u_ws_orp1

String	ls_ret, &
			ls_message, &
			ls_customer_name, &
			ls_customer_id
			
u_string_functions		lu_string_functions		 
			
ls_message = message.StringParm
if message.StringParm > ' ' then
		ib_reinquire = True
		Event ue_set_data('customer_id', ls_message)
		Event ue_set_data('excp_customer_id', ' ')
		lu_string_functions.nf_parseleftright (ls_message, "~t", ls_customer_id, ls_customer_name)
 		dw_edi_prod_detail_header.SetItem(1,"customer_id", ls_customer_id)
	   dw_edi_prod_detail_header.SetItem(1,"customer_name", ls_customer_name)
	else
		ib_reinquire = False
end if



end event

event ue_set_data;choose case as_data_item
	case "customer_id"
		dw_edi_prod_detail_header.setItem(1, 'customer_id', as_value)
	case "customer_name"
		dw_edi_prod_detail_header.setItem(1, 'customer_name', as_value)
	case "excp_customer_id"
		dw_edi_prod_detail_header.setItem(1, 'excp_customer_id', as_value)
	case "excp_customer_name"
		dw_edi_prod_detail_header.setItem(1, 'excp_customer_name', as_value)	
end choose
end event

event ue_get_data;Choose Case as_value
	Case 'customer_id'
		message.StringParm = dw_edi_prod_detail_header.getitemstring (1, "customer_id")
	Case 'customer_name'
		message.StringParm = dw_edi_prod_detail_header.getitemstring (1, "customer_name")	
	Case 'excp_customer_id'
		message.StringParm = dw_edi_prod_detail_header.getitemstring (1, "excp_customer_id")	
	Case 'excp_customer_name'
		message.StringParm = dw_edi_prod_detail_header.getitemstring (1, "excp_customer_name")		
End choose

end event

type dw_edi_prod_detail_choice from u_base_dw_ext within w_edi_excp_detail
integer x = 1600
integer y = 12
integer width = 626
integer height = 220
integer taborder = 30
string dataobject = "d_edi_prod_detail_choice"
boolean border = false
end type

event itemchanged;call super::itemchanged;String		ls_row_option, &
				ls_filter
long			ll_value
ib_retrieve = true
dw_edi_excp_detail.SetReDraw(False)
ls_row_option = data
If ls_row_option = 'C' Then
	dw_edi_excp_detail.SetFilter("")
	dw_edi_excp_detail.Filter()
	ls_filter = "(end_date > RelativeDate(Today(), -1)) and (product_code > ' ')" 
	dw_edi_excp_detail.SetFilter(ls_filter)
	dw_edi_excp_detail.Filter()
Else
	dw_edi_excp_detail.SetFilter("")
	dw_edi_excp_detail.Filter()
	ls_filter = "(product_code > ' ')" 
	dw_edi_excp_detail.SetFilter(ls_filter)
	dw_edi_excp_detail.Filter()
End if

dw_edi_excp_detail.SELECTROW(dw_edi_excp_detail.GETROW(),false)

dw_edi_excp_detail.SetSort("customer_id, product_code, start_date, end_date")
dw_edi_excp_detail.Sort()
ll_value = dw_edi_excp_detail.RowCount()
dw_edi_excp_detail.insertrow(0)
ll_value = ll_value + 1
dw_edi_excp_detail.SetItem(ll_value, "customer_id", ' ')
dw_edi_excp_detail.SetColumn("customer_Id")
dw_edi_excp_detail.SetItem(ll_value, "start_date", today())
dw_edi_excp_detail.uf_changerowstatus(ll_value, New!)
dw_edi_excp_detail.SetReDraw(True)
ib_retrieve = false
end event

type dw_edi_prod_detail_header from u_base_dw_ext within w_edi_excp_detail
integer x = 23
integer width = 1577
integer height = 360
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_edi_prod_detail_header"
boolean border = false
end type

event constructor;call super::constructor;dw_edi_prod_detail_header.InsertRow(0)
dw_edi_prod_detail_choice.InsertRow(0)

end event

type dw_edi_excp_detail from u_base_dw_ext within w_edi_excp_detail
integer x = 18
integer y = 352
integer width = 2761
integer height = 1140
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_edi_excp_detail"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;long							ll_value
DataWindowChild ldwc_temp


is_selection = '2'
ib_updateable = True

//ll_value = dw_edi_excp_detail.RowCount()
//dw_edi_excp_detail.InsertRow(0)
//dw_edi_excp_detail.SetColumn("customer_id")
//dw_edi_excp_detail.SetFocus()
//ll_value = ll_value + 1
//dw_edi_excp_detail.SetItem(ll_value, "customer_id", ' ')
//dw_edi_excp_detail.SetItem(ll_value, "start_date", today())
////dw_edi_excp_detail.ResetUpdate()
//dw_edi_excp_detail.uf_changerowstatus(ll_value, New!)
//
//
ib_NewRowOnChange = False
//
This.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)
//
end event

event itemchanged;call super::itemchanged;datawindowchild	ldwc_division_code, &
						ldwc_locations

string				ls_temp, &
						ls_customer_id

long					ll_rowfound, &
						ll_rowcount

//Choose Case dwo.name
//
//
//	case "product_code"
//		if iw_frame.iu_string.nf_isempty(data) then
//			iw_frame.setmicrohelp("FM Product Code is a required field.")
//			return 1
//		end if
//
//	case "cust_prod_code"
//		if iw_frame.iu_string.nf_isempty(data) then
//			iw_frame.setmicrohelp("Customer Product Code is a required field.")
//			return 1
//		end if
//	Case "start_date" 
//		If IsNull(data) Then
//			iw_frame.SetMicroHelp("Start Date is not a valid date")
//			Return 1
//		End If
//end choose

ll_rowcount = dw_edi_excp_detail.RowCount()
ls_customer_id = dw_edi_excp_detail.getitemstring(ll_rowcount, "customer_id")
if ls_customer_id > ' ' then
	wf_addrow()
end if
	


end event

event itemerror;call super::itemerror;return 1
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event itemfocuschanged;call super::itemfocuschanged;
if ib_retrieve Then Return 1
is_columnname = dwo.name

if is_columnname = 'cust_prod_code' then
	This.SetText(Trim(this.GetText()))
	This.SelectText(1,len(trim(this.GetText())))
end if


end event

