HA$PBExportHeader$w_edi_transmission_by_customer_inq.srw
forward
global type w_edi_transmission_by_customer_inq from w_base_response_ext
end type
type dw_edi_trans_by_cust_inq from u_base_dw_ext within w_edi_transmission_by_customer_inq
end type
end forward

global type w_edi_transmission_by_customer_inq from w_base_response_ext
integer width = 2272
integer height = 544
string title = "EDI Transmission by Customer Inquire"
long backcolor = 12632256
dw_edi_trans_by_cust_inq dw_edi_trans_by_cust_inq
end type
global w_edi_transmission_by_customer_inq w_edi_transmission_by_customer_inq

type variables
w_base_sheet	iw_parent
end variables

on w_edi_transmission_by_customer_inq.create
int iCurrent
call super::create
this.dw_edi_trans_by_cust_inq=create dw_edi_trans_by_cust_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_edi_trans_by_cust_inq
end on

on w_edi_transmission_by_customer_inq.destroy
call super::destroy
destroy(this.dw_edi_trans_by_cust_inq)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_Customer, &
						ldwc_temp
						
String				ls_customer_id, &
						ls_customer_type, &
						ls_transmission_type, &
						ls_from_date, &
						ls_to_date
						
u_string_functions	lu_string_functions						

dw_edi_trans_by_cust_inq.GetChild( "customer_id", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_edi_trans_by_cust_inq.GetChild( "transmission_type", ldwc_temp)

IF ldwc_temp.RowCount()	= 0 THEN 
	ldwc_temp.SetTransObject(SQLCA)
	ldwc_temp.Retrieve()
end if

iw_parent.Event ue_get_data('customer_id')
ls_customer_id = Message.StringParm
If Not lu_string_functions.nf_IsEmpty(ls_customer_id) Then
	If ls_customer_id <> 'ALL' Then
		dw_edi_trans_by_cust_inq.SetItem(1, "customer_id", ls_customer_id)
	End If
End If

iw_parent.Event ue_get_data('from_date')
ls_from_date = Message.StringParm
If Not lu_string_functions.nf_IsEmpty(ls_from_date) Then 
	dw_edi_trans_by_cust_inq.SetItem(1, "from_date", Date(ls_from_date))
Else
	dw_edi_trans_by_cust_inq.SetItem(1, "from_date", Today())
End If

iw_parent.Event ue_get_data('to_date')
ls_to_date = Message.StringParm
If Not lu_string_functions.nf_IsEmpty(ls_to_date) Then 
	dw_edi_trans_by_cust_inq.SetItem(1, "to_date", Date(ls_to_date))
Else
	dw_edi_trans_by_cust_inq.SetItem(1, "to_date", Today())
End If

iw_parent.Event ue_get_data('transmission_type')
ls_transmission_type = Message.StringParm
If Not lu_string_functions.nf_IsEmpty(ls_transmission_type) Then 
	dw_edi_trans_by_cust_inq.SetItem(1, "transmission_type", ls_transmission_type)
End If

dw_edi_trans_by_cust_inq.SetColumn("customer_id")

end event

event open;call super::open;iw_parent = Message.PowerObjectParm
end event

event ue_base_ok;call super::ue_base_ok;u_string_functions	lu_string_functions

dw_edi_trans_by_cust_inq.AcceptText()

If dw_edi_trans_by_cust_inq.GetItemString(1,"customer_id") > '       ' Then
	iw_parent.Event ue_set_data("customer_id", dw_edi_trans_by_cust_inq.GetItemString(1,"customer_id"))
	iw_parent.Event ue_set_data("customer_type", "S")
Else
	iw_parent.Event ue_set_data("customer_id", "       ")
	iw_parent.Event ue_set_data("customer_type", " ")
	iw_parent.Event ue_set_data("customer_name", "ALL")
End If


iw_parent.Event ue_Set_Data("from_date", String(dw_edi_trans_by_cust_inq.GetItemDate(1, "from_date"),'mm-dd-yyyy'))
iw_parent.Event ue_Set_Data("to_date", String(dw_edi_trans_by_cust_inq.GetItemDate(1, "to_date"),'mm-dd-yyyy'))

If lu_string_functions.nf_IsEmpty(dw_edi_trans_by_cust_inq.GetItemString(1,"transmission_type")) Then
	MessageBox("Transmission Type Error", "A Transmission Type is required")
	Return
End If
		
iw_parent.Event ue_set_data("transmission_type", dw_edi_trans_by_cust_inq.GetItemString(1,"transmission_type")) 

CloseWithReturn(This, "OK")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_edi_transmission_by_customer_inq
integer x = 2414
integer y = 257
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_edi_transmission_by_customer_inq
integer x = 1944
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_edi_transmission_by_customer_inq
integer x = 1944
end type

type cb_browse from w_base_response_ext`cb_browse within w_edi_transmission_by_customer_inq
integer x = 2417
end type

type dw_edi_trans_by_cust_inq from u_base_dw_ext within w_edi_transmission_by_customer_inq
integer width = 1901
integer height = 416
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_edi_transmission_by_cust_inq"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)


end event

