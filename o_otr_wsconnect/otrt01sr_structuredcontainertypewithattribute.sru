HA$PBExportHeader$otrt01sr_structuredcontainertypewithattribute.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_structuredContainerTypeWithAttribute from nonvisualobject
    end type
end forward

global type otrt01sr_structuredContainerTypeWithAttribute from nonvisualobject
end type

type variables
    boolean structuredContainer
end variables

on otrt01sr_structuredContainerTypeWithAttribute.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_structuredContainerTypeWithAttribute.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

