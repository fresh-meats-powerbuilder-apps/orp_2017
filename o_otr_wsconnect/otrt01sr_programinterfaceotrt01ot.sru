HA$PBExportHeader$otrt01sr_programinterfaceotrt01ot.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterfaceOtrt01ot from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterfaceOtrt01ot from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on otrt01sr_ProgramInterfaceOtrt01ot.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterfaceOtrt01ot.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

