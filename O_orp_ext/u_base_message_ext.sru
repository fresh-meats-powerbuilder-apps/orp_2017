HA$PBExportHeader$u_base_message_ext.sru
$PBExportComments$Extension layer Message Object - inherited from u_Netwise_message.
forward
global type u_base_message_ext from u_netwise_message
end type
type str_commhandles from structure within u_base_message_ext
end type
type str_loadtable_data from structure within u_base_message_ext
end type
end forward

type str_commhandles from structure
    string dll_name
    string server_name
    integer commhandle
end type

type str_loadtable_data from structure
    string table_name
    integer total_rows
    integer current_rows
    integer sei_rec_width
end type

global type u_base_message_ext from u_netwise_message
event ue_getsmancode pbm_custom74
event ue_getprojectdata pbm_custom73
event ue_getlocation pbm_custom72
end type
global u_base_message_ext u_base_message_ext

type prototypes

end prototypes

type variables
// close w_open_so
Boolean ib_ok,&
             ib_netwise_error

// Netwise Encryption Key
string	is_EncryptionKey = "NETWISE"

// error code identifiers
Boolean	ib_paresolve_ind

// individual user identifiers
string	is_salesperson_code,&
	is_smandivision,&
	is_smanlocation	

// Used for message Handling for Weight overriding
Char ic_weight_override
Double id_Load_weight

Protected:
// Holds User Name and Password
string	is_UserName, &
	is_UserPassword,&
              is_Shared_sales_id
	
CHAR 	ic_Load_Status

Private:	
String	is_Shared_Sales_Status, &
	is_WIndow_Name,&
	is_Instruction_Type
	

private:
// Holds information for loading tables_window
str_loadtable_data    istr_loadtable_data

// Holds information on the CommHandles
str_CommHandles	istr_CommHandles[]
end variables

forward prototypes
public function string nf_getuserpassword ()
public subroutine nf_setuserpassword (string as_userpassword)
public subroutine nf_update_user_info (string args_sales_person_code, string args_division, string args_location)
public function String nf_getlocation ()
public function string nf_getsmancode ()
public function string nf_getuserid ()
end prototypes

event ue_getsmancode;call super::ue_getsmancode;message.stringparm = is_salesperson_code
end event

on ue_getprojectdata;call u_netwise_message::ue_getprojectdata;//This.PowerObjectParm = iw_projectdata
return
end on

event ue_getlocation;call super::ue_getlocation;message.stringparm = is_smanlocation
end event

public function string nf_getuserpassword ();return is_userpassword
end function

public subroutine nf_setuserpassword (string as_userpassword);is_userpassword = as_userpassword
end subroutine

public subroutine nf_update_user_info (string args_sales_person_code, string args_division, string args_location);is_salesperson_code = args_sales_person_code
is_smandivision = args_division
is_smanlocation = args_location
end subroutine

public function String nf_getlocation ();Return is_smanlocation
end function

public function string nf_getsmancode ();Return is_salesperson_code
end function

public function string nf_getuserid ();Return SQLCA.UserID
end function

on u_base_message_ext.create
call super::create
end on

on u_base_message_ext.destroy
call super::destroy
end on

