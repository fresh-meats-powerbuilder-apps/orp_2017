HA$PBExportHeader$u_base_dw_ext.sru
$PBExportComments$Extension layer Datawindow control - inherited from u_netwise_dw.
forward
global type u_base_dw_ext from u_netwise_dw
end type
end forward

global type u_base_dw_ext from u_netwise_dw
event type long ue_populatedddw ( unsignedinteger wparm,  long lparm )
event type long ue_itemchanged ( unsignedinteger wparm,  long lparm )
event ue_dwndropdown pbm_dwndropdown
event type integer ue_setbusinessrule ( integer row,  string name,  string value )
end type
global u_base_dw_ext u_base_dw_ext

type variables
LONG		il_CurrentRow,&
		il_ClickedRow, &
		il_ChangedRow

INTEGER	ii_CurrentColumnID, &
		ii_ClickedColumnID, &
		ii_ChangedColumnID

STRING		is_CurrentColumnName, &
		is_ClickedColumnName, &
		is_ChangedColumnName

DATAWINDOWCHILD	idw_DDDWChild

BOOLEAN	ib_Changes, &
		ib_SelectText = TRUE, &
		ib_NewRowOnChange


end variables

forward prototypes
public function Long nf_filterunique (string as_KeyColumn, Character ac_SortOrder)
end prototypes

public function Long nf_filterunique (string as_KeyColumn, Character ac_SortOrder);LONG		ll_Row, &
			ll_Rows

ANY		la_Cur, &
			la_Prev

This.Filter()
This.SetSort(as_KeyColumn + " " + ac_SortOrder)
This.Sort()

ll_Rows = This.RowCount()

CHOOSE CASE UPPER(LEFT(This.Describe(as_KeyColumn + ".ColType"), 5))
CASE	"CHAR("
	la_Prev = This.GetItemString(ll_Row, as_KeyColumn)
	FOR ll_Row = 2 TO ll_Rows
		la_Cur = This.GetItemString(ll_Row, as_KeyColumn)
		IF la_Cur = la_Prev THEN This.RowsMove(ll_Row, ll_Row, Primary!, This, 999999, Filter!)
		la_Prev = la_Cur
	NEXT

CASE 	"TIME ", "TIMES"
	la_Prev = This.GetItemTime(ll_Row, as_KeyColumn)
	FOR ll_Row = 2 TO ll_Rows
		la_Cur = This.GetItemTime(ll_Row, as_KeyColumn)
		IF la_Cur = la_Prev THEN This.RowsMove(ll_Row, ll_Row, Primary!, This, 999999, Filter!)
		la_Prev = la_Cur
	NEXT

CASE	"DATE "
	la_Prev = This.GetItemDate(ll_Row, as_KeyColumn)
	FOR ll_Row = 2 TO ll_Rows
		la_Cur = This.GetItemDate(ll_Row, as_KeyColumn)
		IF la_Cur = la_Prev THEN This.RowsMove(ll_Row, ll_Row, Primary!, This, 999999, Filter!)
		la_Prev = la_Cur
	NEXT

CASE	"DATET"
	la_Prev = This.GetItemDateTime(ll_Row, as_KeyColumn)
	FOR ll_Row = 2 TO ll_Rows
		la_Cur = This.GetItemDateTime(ll_Row, as_KeyColumn)
		IF la_Cur = la_Prev THEN This.RowsMove(ll_Row, ll_Row, Primary!, This, 999999, Filter!)
		la_Prev = la_Cur
	NEXT

CASE	"DECIM", "NUMBE"
	la_Prev = This.GetItemNumber(ll_Row, as_KeyColumn)
	FOR ll_Row = 2 TO ll_Rows
		la_Cur = This.GetItemNumber(ll_Row, as_KeyColumn)
		IF la_Cur = la_Prev THEN This.RowsMove(ll_Row, ll_Row, Primary!, This, 999999, Filter!)
		la_Prev = la_Cur
	NEXT

END CHOOSE
	
RETURN	This.FilteredCount()

end function

event doubleclicked;Date 		ld_TempDate
DateTime	ld_TempDateTime
String	ls_Data,&
			ls_Temp,&
			ls_Date

long			ll_ClickedColumn
string		ls_ColumnType, &
				ls_protected, &
				ls_ColName
				
str_parms	lstr_parms

Integer	li_ReturnCode

boolean	lb_product

Application	lu_App

u_AbstractParameterStack	lu_Stack
u_AbstractErrorContext		lu_ErrorContext
u_AbstractClassFactory		lu_ClassFactory


// If the double clicked field is a date or datetime, open w_Calendar
// to allow date editing.



// Make sure a valid row/column was clicked
IF String(dwo.Type) = "column" THEN
	ls_ColName = String(dwo.Name)
	ll_ClickedColumn = Long(dwo.ID)
END IF
IF Row < 1 OR ll_ClickedColumn < 1 THEN Return

If This.Describe("DataWindow.ReadOnly") = 'yes' Then return

// Get the column type of the clicked field
ls_ColumnType = Lower(This.Describe("#" + String( &
						ll_ClickedColumn) + ".ColType"))

ls_protected = This.Describe("#" + String(ll_ClickedColumn) + ".Protect")

If Not IsNumber(ls_protected) Then
	// then it is an expression, evaluate it
	// first char is default value, then tab, then expression
	// the whole thing is surrounded by quotes
	ls_protected = This.Describe('Evaluate("' + Mid(ls_protected, &
									Pos(ls_protected, '~t') + 1, &
									Len(ls_protected) - Pos(ls_protected, '~t') - 1) + &
									'", ' + String(Row) + ')')
End if

// Only display the clock if the column has edit capablilities

lb_product = false
IF POS(UPPER(String(dwo.Name)),"PRODUC") > 0 Then
	// bypass so double click on protected product code brings up product code list window
	lb_product = true
else
	If This.Describe("#" + String(ll_ClickedColumn) + ".Edit.DisplayOnly") = "yes" OR &
	     This.Describe("#" + String(ll_ClickedColumn) + ".DDDW.AllowEdit") = "no" OR &
		  This.Describe("#" + String(ll_ClickedColumn) + ".DDLB.AllowEdit") = "no" OR &
		  This.Describe("#" + String(ll_ClickedColumn) + ".TabSequence") = "0" OR &
		  ls_protected = '1' Then
		Return
	End If
End if		

// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = gw_base_Frame.PointerX() + gw_base_Frame.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = gw_base_Frame.PointerY() + gw_base_Frame.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

Choose Case ls_ColumnType
	Case "date", "datetime"
		Choose Case String(dwo.ColType)
			Case "date"
				ld_TempDate = This.GetItemDate(row, String(dwo.Name))
				IF IsNull(ld_TempDate) Then ld_TempDate = Today()
				If Year(ld_TempDate) = 1900 Then ld_TempDate = Today()
				ls_Date = String(ld_TempDate,"mm/yy/yyyy")
			Case "datetime"
				ld_TempDateTime = This.GetItemDateTime(row, String(dwo.Name))
				IF IsNUll(ld_TempDateTime) Then 
					ls_Date = String(Today(),"mm/dd/yyyy")
				Else
					if (Year(Date(ld_TempDateTime)) = 1900) or (Year(Date(ld_TempDateTime)) = 1000) Then 
						ls_Date = String(Today(),"mm/dd/yyyy")
					else
						ls_Date = String(String(ld_TempDateTime, "mm/dd/yyyy"))
					end if
				END IF
		End Choose
		IF NOT(ISDate(ls_Date)) Then ls_Date =String( Today(), "mm/dd/yyyy")
		lu_App = GetApplication()	
		IF lu_App.Dynamic af_GetClassFactory(lu_ClassFactory) <> 0 then return
		lu_ClassFactory.uf_GetObject('u_ErrorContext', lu_ErrorContext)
		lu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,lu_ErrorContext)

		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",lu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_GPO_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)

		// Open the calendar window
		lu_ClassFactory.uf_GetResponseWindow("w_orp_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",lu_ErrorContext)


		// Was it successful?  If so, get the date passed back and set the column.
		If lu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
		//	This.SetItem(row, String(dwo.Name), Date(ls_Date))
			this.settext(ls_date)
			this.accepttext()
		END IF	
	Case Else
		if lb_product then
			// ibdkdld 11/27/02 load the pallet info when needed the first time 
			if not isvalid(iw_frame.iu_project_functions.ids_product_pallet_info) Then
				iw_frame.iu_project_functions.nf_get_product_pallet_info()
				iw_frame.setmicrohelp( "Ready")
			End if		
			// ibdkdld If the object wasn't created don't open the w_choose_products window have the user try again
			if isvalid(iw_frame.iu_project_functions.ids_product_pallet_info) Then
			// ibdkdld 11/27/02 end
				IF POS(UPPER(String(dwo.Name)),"PRODUC") > 0 Then
					ls_Temp = this.Describe(dwo.name+".Protect")
					ls_Temp = Mid(ls_Temp,Pos(ls_Temp,"~t")+1,Len(ls_temp) - 1)
					ls_Data = 'evaluate("' + ls_Temp + ","+String(row)+')'
					ls_Data = This.Describe(ls_Data)
					If ls_Data = '1'   Then 
						ls_temp = "product_code_text" + "~t" + this.GetItemString(row, ls_ColName)
						OpenWithParm(w_choose_products,ls_temp)
						Return
					ELSE
						ls_temp = "product_code_text" + "~t" + this.GetText()
						OpenWithParm(w_choose_products,ls_temp)
						IF Message.StringParm <> 'CANCEL' Then
							ls_Data = Message.StringParm
							This.SetText(ls_Data)
							This.AcceptText()
						END IF
					End if	
				END IF
		end if
	END IF
End Choose




end event

event itemchanged;call super::itemchanged;dwitemstatus	le_RowStatus

Long 				ll_Count  
string			ls_UpdateFlag, &
					ls_business_rules,&
					ls_Product_code


ib_changes = TRUE
ii_ChangedColumnID = ii_CurrentColumnID
is_ChangedColumnName = is_CurrentColumnName
il_ChangedRow = il_CurrentRow

IF IsValid(DWO) Then
	IF POS(UPPER(String(dwo.Name)),"PRODUCT_STATE") > 0 Then
		//bypass checking when product state is changed.
	ELSE   
	IF POS(UPPER(String(dwo.Name)),"PRODUC") > 0 Then
		//You must look for all 10 places in the database
		ls_product_Code = Data + Space(10 - len(Data))
		Select Count(*) 
		into :ll_count
		from sku_products
		WHERE sku_products.sku_product_code = :ls_Product_code
		USING SQLCA;
	

		IF ll_Count < 1 AND SQLCA.SQLCode = 0 then 
			iw_Frame.SetMicroHelp("Invalid/Inactive product code")
			This.Event Post ue_SetBusinessRule(row,String(dwo.name),'E')
			Return 0
		ELSE
			This.Event Post ue_SetBusinessRule(row,String(dwo.name),'M')
			iw_Frame.SetMicroHelp("Ready")
		END IF
	END IF
END IF
END IF




// Update the Update_Flag column so the RPC will know how to update the row
IF il_CurrentRow > 0 AND il_CurrentRow <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		ls_UpdateFlag = Trim(This.GetItemString(il_CurrentRow, "update_flag"))
		CHOOSE CASE TRUE
			CASE ISNULL(ls_UpdateFlag), TRIM(ls_UpdateFlag) = ""
				le_RowStatus = This.GetItemStatus(il_CurrentRow, 0, Primary!)
				CHOOSE CASE le_RowStatus
				CASE NewModified!, New!
					This.SetItem(il_CurrentRow, "update_flag", "A")
				CASE DataModified!, NotModified!
					This.SetItem(il_CurrentRow, "update_flag", "U")
					IF Long(This.Describe("business_rules.id")) > 0 THEN
						ls_business_rules	=	This.GetItemString(row, 'business_rules')
						ls_business_rules	=	Left(ls_business_rules, Long(dwo.id) - 1 ) + &
							'M' + Right(ls_business_rules, Len(ls_business_rules) - Long(dwo.id))
						This.SetItem(row, 'business_rules', ls_business_rules)
					END IF
				CASE Else
					string ls_null
					SetNull(ls_null)
					This.SetItem(il_CurrentRow, "update_flag", ls_null)
				END CHOOSE
			CASE ls_UpdateFlag = 'U'
				IF Long(This.Describe("business_rules.id")) > 0 THEN
						ls_business_rules	=	This.GetItemString(row, 'business_rules')
						ls_business_rules	=	Left(ls_business_rules, Long(dwo.id) - 1) + &
									'M' + Right(ls_business_rules, Len(ls_business_rules) - Long(dwo.id))
						This.SetItem(row, 'business_rules', ls_business_rules)
					END IF
		END CHOOSE
	END IF
END IF

// If the ib_NewRowOnChange boolean is true and the current row
// is the last row in the datawindow insert a new row at the end
IF This.ib_NewRowOnChange AND This.il_CurrentRow = This.RowCount() THEN
	This.InsertRow(0)
END IF

end event

event itemfocuschanged;call super::itemfocuschanged;STRING	ls_Mask, ls_text, ls_EditStyle, ls_ColumnType
INTEGER	li_start
 
is_CurrentColumnName = This.GetColumnName()
ii_CurrentColumnID = This.GetColumn()
il_CurrentRow = This.GetRow()

CHOOSE CASE This.Describe(is_CurrentColumnName + ".DDDW.DataColumn")
CASE "?", "!"
CASE ELSE
	This.GetChild(is_CurrentColumnName, idw_DDDWChild)
END CHOOSE

IF ib_Changes THEN 
	ib_Changes = FALSE
	This.PostEvent("ue_ItemChanged")
END IF

ls_EditStyle = This.Describe(is_CurrentColumnName + ".EditMask.Spin")
ls_ColumnType = This.Describe(is_CurrentColumnName + ".ColType")

IF ib_SelectText THEN
	CHOOSE CASE UPPER(ls_EditStyle + "," + ls_ColumnType)
	CASE "YES,DATE"
		ls_Mask = This.Describe(is_CurrentColumnName + ".EditMask.Mask")
		ls_Text = STRING(This.GetItemDate(il_CurrentRow, is_CurrentColumnName), ls_mask)
		CHOOSE CASE MID(ls_Mask, POS(ls_Mask, "d", 1) - 1, 1)
		CASE "m", "y"
			This.SelectText(1, 100)
		CASE ELSE
		 	li_Start = POS(ls_Text, MID(ls_Mask, POS(ls_Mask, "d", 1) - 1, 1), 1) + 1
			IF POS(ls_Mask, MID(ls_Mask, POS(ls_Mask, "d", 1) - 1, 1), 1) <> POS(ls_Mask, "d", 1) - 1 THEN
			 	li_Start = POS(ls_Text, MID(ls_Mask, POS(ls_Mask, "d", 1) - 1, 1), li_Start + 1) + 1
			END IF
			IF POS(ls_Mask, "d", POS(ls_Mask, "d", 1) + 1) > 0 THEN
				This.SelectText(li_Start, 2)
			ELSE
				This.SelectText(li_Start, 1)
			END IF
		END CHOOSE
	CASE ELSE  
		This.SelectText(1, 100)
	END CHOOSE
END IF



end event

event rowfocuschanged;call super::rowfocuschanged;il_CurrentRow = This.GetRow()
end event

event ue_keydown;String	ls_Data,&
			ls_Temp,&
			ls_Date


long			ll_ClickedColumn
string		ls_ColumnType, &
				ls_protected, &
				ls_ColName
				
str_parms	lstr_parms

Integer	li_ReturnCode

Application	lu_App

u_AbstractParameterStack	lu_Stack
u_AbstractErrorContext		lu_ErrorContext
u_AbstractClassFactory		lu_ClassFactory

INTEGER		li_Column, &
				li_ColumnCount


long			ll_CurrentRow, &
				ll_CurrentColumn
				



CHOOSE CASE TRUE
CASE	KeyDown(KeyEnter!)
	IF NOT ib_firstcolumnonnextrow THEN RETURN
	IF il_dwrow < This.RowCount() THEN
		li_ColumnCount = INTEGER(This.Describe("DataWindow.Column.Count"))
		FOR li_Column = 1 TO li_ColumnCount
			IF This.Describe("#" + STRING(li_Column) + ".TabSequence") = "10" THEN
				This.SetColumn(li_Column)
			END IF
		NEXT
	END IF
CASE	Keydown(KeyControl!) and Keydown(KeyDownArrow!)
	IF This.Describe("DataWindow.ReadOnly") = 'yes' THEN RETURN 
	
	// Get the current row and column that has focus
			ll_CurrentRow = This.GetRow()
			ll_CurrentColumn = This.GetColumn()
			
	// Get the current column name
			is_ColumnName = GetColumnName()

	// Get the column type of the clicked field
			ls_ColumnType = Lower(This.Describe("#" + String( &
												ll_CurrentColumn) + ".ColType"))

	// Get the X and Y coordinates of the place that was clicked
		lstr_parms.integer_arg[1] = long(This.Describe(is_ColumnName + &
									".X")) + iw_parent.X + 200
	CHOOSE CASE	lstr_parms.integer_arg[1]
		CASE IS > 2253
			lstr_parms.integer_arg[1] = 2253
		CASE is < 113
			lstr_parms.integer_arg[1] = 113
	END CHOOSE

	lstr_parms.integer_arg[2] = long(This.Describe(is_ColumnName + &
								".Y")) + iw_parent.Y - 400
	CHOOSE CASE	lstr_parms.integer_arg[2]
		CASE IS > 1053
			lstr_parms.integer_arg[2] = 1053
		CASE is < 0
			lstr_parms.integer_arg[2] = 0
	END CHOOSE
	
	IF ls_ColumnType = "date" or &
		ls_ColumnType = "datetime" THEN
			Choose Case ls_ColumnType
				Case "date"
					ls_Date = String(This.GetItemDate(This.GetRow(), ll_CurrentColumn), "mm/dd/yyyy")
				Case "datetime"
					ls_Date = String(This.GetItemDateTime(This.Getrow(), ll_CurrentColumn), "mm/dd/yyyy") 
			End Choose
		if Year(Date(ls_date)) = 1900 Then ls_Date = String(Today(),"mm/dd/yyyy")	
		lu_App = GetApplication()	
		IF lu_App.Dynamic af_GetClassFactory(lu_ClassFactory) <> 0 then return
		lu_ClassFactory.uf_GetObject('u_ErrorContext', lu_ErrorContext)
		lu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,lu_ErrorContext)

		// Set up the parameters for the calendar window
		lu_Stack.uf_Push("u_AbstractClassFactory",lu_ClassFactory)
		lu_stack.uf_Push("String", ls_Date)
		lu_Stack.uf_Push("String", "d_GPO_calendar")
		lu_Stack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)

		// Open the calendar window
		lu_ClassFactory.uf_GetResponseWindow("w_orp_calendar",lu_Stack)
		lu_Stack = Message.PowerObjectParm
		If Not Isvalid( lu_stack ) Then Return
		lu_Stack.uf_Pop("u_AbstractErrorContext",lu_ErrorContext)


		// Was it successful?  If so, get the date passed back and set the column.
		If lu_ErrorContext.uf_IsSuccessful() Then
			lu_Stack.uf_Pop("String",ls_Date)
			If Len(Trim(ls_Date)) = 0 Then Return
		//	This.SetItem(row, String(dwo.Name), Date(ls_Date))
			this.settext(ls_date)
			this.accepttext()
		END IF	
	END IF
CASE ELSE
END CHOOSE

end event

event ue_postconstructor;return
end event

on u_base_dw_ext.create
call super::create
end on

on u_base_dw_ext.destroy
call super::destroy
end on

