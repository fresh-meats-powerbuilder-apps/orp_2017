HA$PBExportHeader$w_base_frame_ext.srw
$PBExportComments$Project-level frame window - inherited from w_base_frame.
forward
global type w_base_frame_ext from w_netwise_frame
end type
end forward

global type w_base_frame_ext from w_netwise_frame
string title = "Order Processing"
string menuname = "m_base_menu_ext"
long backcolor = 1090519039
end type
global w_base_frame_ext w_base_frame_ext

type variables
// Menu
m_base_menu_ext  		im_menu

// Active Sheet
w_base_sheet 		iw_Active_Sheet

u_project_functions	iu_project_functions

end variables

forward prototypes
public function boolean wf_windowisvalid (ref window aw_window, string as_window_name)
public subroutine wf_opensheet (string as_window_name, string as_stringparm)
end prototypes

public function boolean wf_windowisvalid (ref window aw_window, string as_window_name);// Allow multiple instances of detail to be open
Choose Case Lower(as_window_name) 
Case "w_sales_order_detail", "w_customer_order", "w_reservations", "w_pa_inquiry"
	Return FALSE
End Choose

aw_window = iw_frame.GetFirstSheet()



DO While IsValid( aw_window) 
  IF Upper(Trim(aw_window.ClassName())) = Upper(Trim(as_window_name)) Then
		
      Return TRUE
  End IF   
   aw_window = iw_frame.GetNextSheet( aw_window)
Loop

Return FALSE


end function

public subroutine wf_opensheet (string as_window_name, string as_stringparm);Int li_rtn
Window lw_Open

IF wf_WindowIsValid( lw_open, as_window_name) THEN
   Message.StringParm = as_StringParm
   lw_open.BringToTop = TRUE
   lw_open.TriggerEvent( Open!)
ELSE
   IF LEN(TRIM(as_window_name)) > 0 THEN
   	li_rtn = OpenSheetWithParm( lw_open, as_StringParm, as_window_name, iw_frame, 8, iw_frame.im_menu.iao_arrangeopen)
   END IF
END IF  
end subroutine

event ue_postopen;call super::ue_postopen;String ls_userid,ls_Region

IF iw_Frame.ib_login_cancel then Return

This.is_help = iw_frame.is_WorkingDir + "orp.hlp"

IF ProfileInt( This.is_userini, "System Settings", "ReloadChecked", 0) = 0 Then
	im_menu.m_Options.m_reloadstartupdata.Checked = FALSE
ELSE
	im_menu.m_Options.m_reloadstartupdata.Checked = TRUE
END IF


iu_project_functions.nf_loadpersondef()
iu_project_functions.nf_loadcustomers()
iu_project_functions.nf_load_products()
//remove ibdkdld 11/26/02 load the pallet info when needed the first time not on startup 
//iu_project_functions.nf_get_product_pallet_info()

// IBDKDLD 01/02/03
ls_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
If upper(left(ls_userid,4)) = "IBDK" or upper(left(ls_userid,4)) = "QATE" Then
	ls_Region =  ProfileString( gw_netwise_frame.is_ApplicationINI,"Netwise Server Info","ServerSuffix","")
	IF UPPER(left(ls_Region,1)) = "P" Then
		This.title += "            ****************  WARNING YOU ARE CONNECTED TO PRODUCTION  ****************"
	END IF
ENd IF



This.SetMicroHelp("Ready")

end event

on w_base_frame_ext.create
call super::create
if IsValid(this.MenuID) then destroy(this.MenuID)
if this.MenuName = "m_base_menu_ext" then this.MenuID = create m_base_menu_ext
end on

on w_base_frame_ext.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

event close;call super::close;IF im_menu.m_Options.m_reloadstartupdata.Checked Then
   SetProfileString( This.is_userini, "orp", "SalesCode", "")
  	SetProfileString( This.is_userini, "orp", "Division", "")
	SetProfileString( This.is_userini, "orp", "Location", "")
	SetProfileString( This.is_userini, "System Settings", "DefaultDate", "")
	SetProfileString( This.is_userini, "System Settings", "ReloadChecked", "1")
ELSE
	SetProfileString( This.is_userini, "System Settings", "ReloadChecked", "0")
END IF
Disconnect;


end event

event open;call super::open;this.im_menu = this.menuID


end event

