HA$PBExportHeader$w_tooltip_1.srw
forward
global type w_tooltip_1 from window
end type
type dw_description from datawindow within w_tooltip_1
end type
end forward

global type w_tooltip_1 from window
integer x = 672
integer y = 268
integer width = 923
integer height = 112
windowtype windowtype = child!
long backcolor = 15793151
dw_description dw_description
end type
global w_tooltip_1 w_tooltip_1

type variables
w_base_sheet	iw_Parent
end variables

on w_tooltip_1.create
this.dw_description=create dw_description
this.Control[]={this.dw_description}
end on

on w_tooltip_1.destroy
destroy(this.dw_description)
end on

event open;Long						ll_NewX, &
							ll_NewY,&
							ll_Width,&
							ll_Height,&
							ll_Temp
							
String					ls_data,&
							ls_String


u_string_functions	lu_strings

ll_Temp = 15

iw_parent = Message.PowerObjectParm
IF IsValid(iw_parent) Then
	iw_parent.Event ue_get_data("ToolTip")
	ls_String = Message.StringParm 
ELSE
	ls_String = Message.StringParm 
END IF

ll_NewX = iw_frame.PointerX() + 50
ll_NewY = iw_Frame.PointerY() + 50

If ll_NewX + This.Width > iw_Frame.Width Then
	ll_NewX = iw_Frame.Width - This.Width - 5
End if
If ll_NewY + This.Height > iw_Frame.Height Then
	ll_NewY = iw_Frame.Height - This.Height - 100
End if

This.Move(ll_NewX, ll_NewY)
	
ll_height = dw_description.ImportString(ls_String)
ll_height = ll_height * (Long(dw_description.Object.tooltip.Height)+ll_Temp)
ll_width = (dw_description.GetItemNumber(1,"c_max")+1) * 29
iw_frame.setMicroHelp(ls_String)
dw_description.Resize(ll_width,ll_height)
//Window should be a tiny bit larger then the DW
This.Resize(ll_width+5,ll_height)
end event

type dw_description from datawindow within w_tooltip_1
integer y = 8
integer width = 837
integer height = 88
integer taborder = 1
string dataobject = "d_tooltip"
boolean border = false
boolean livescroll = true
end type

event constructor;//This.insertRow(0)
end event

