HA$PBExportHeader$u_project_functions.sru
forward
global type u_project_functions from nonvisualobject
end type
end forward

global type u_project_functions from nonvisualobject autoinstantiate
end type

type variables
DataStore	ids_customers,&
		ids_Billtos,&
		ids_Corps,&
		ids_Products, &
		ids_Product_pallet_info


         u_ws_orp4    	iu_ws_orp4   
		u_ws_orp2       iu_ws_orp2
end variables

forward prototypes
public subroutine nf_getlocations (ref datawindowchild adwc_locations, string as_smancode)
public subroutine nf_gettsrs (ref datawindowchild adwc_tsrs, string as_location)
public subroutine nf_getcustomers (ref datawindowchild adwc_customers, string as_location)
public subroutine nf_getcorps (ref datawindowchild adwc_customers)
public subroutine nf_getbillto (ref datawindowchild adwc_customers)
public function boolean nf_loadpersondef ()
public subroutine nf_getservicecenters (ref datawindowchild adwc_servicecenters, string as_location)
public subroutine nf_getdivisions (ref datawindowchild adwc_divisions)
public subroutine nf_set_s_error (ref s_error astr_s_error)
public subroutine nf_replace_rows (ref datawindow adw_replace_dw, string as_replace_string)
public function integer nf_gen_sales (string as_data)
public function integer nf_gettutltype (ref datawindowchild adw_dddatawindow, string as_recordtype)
public function string nf_buildupdatestring (ref datawindow adw_tobuildfrom)
public subroutine nf_loadcustomers ()
public subroutine nf_list_sales_orders (string as_salesid)
public function integer nf_gpo_week (integer ai_month, long al_year)
public function integer nf_validatecustomer (string as_customer_id, string as_customer_type, ref string as_customer_name, ref string as_customer_city)
public function string nf_getdefault_tsr_for_customer (string as_customer_id)
public function integer nf_load_products ()
public subroutine nf_openorderconfirmation (readonly w_base_sheet_ext aw_parentwindow)
public function integer nf_getgpomonth (date gpodate)
public function integer nf_getgpoweek (date gpodate)
public function integer nf_getgpoyear (date gpodate)
public function string nf_getgpodate (date gpo_date)
public function string nf_get_age_code (string sku_product_code)
public function date nf_get_begin_shipdate (date gpo_date)
public function date nf_get_end_shipdate (date gpo_date)
public function date nf_get_contract_due_date (date gpo_date)
public function boolean nf_isscheduler ()
public subroutine nf_openshortagequeue (readonly w_base_sheet_ext aw_parent_window)
public function integer nf_validatecustomerstate (string as_customer_id, string as_customer_type, ref string as_customer_name, ref string as_customer_city, ref string as_customer_state)
public function boolean nf_plt_based_on_delivery (string as_plant)
public function boolean nf_issvcpresident ()
public subroutine nf_get_product_pallet_info ()
public function boolean nf_get_additional_datal (string as_customer_id, string as_customer_type, string as_division, date ad_delivery_date, ref string as_city, ref string as_state, ref string as_short_name, ref time at_delivery_time, ref string as_type_of_sale, ref string as_sub_ind)
public function string nf_get_age_code (string as_sku_product_code, string as_customer_id, string as_plant_code)
public function string nf_get_age_code (string as_sku_product_code, string as_customer_id, boolean ab_fresh)
public function boolean nf_isschedulermgr ()
public function boolean nf_issalesmgr ()
public subroutine nf_openorderedbolcomparison (w_base_sheet_ext aw_parentwindow)
end prototypes

public subroutine nf_getlocations (ref datawindowchild adwc_locations, string as_smancode);String			ls_first, &
					ls_last
					

If as_smancode = '' Then 
	ls_first = '000'
	ls_last = 'ZZZ'
Else
	ls_first = as_smancode
	ls_last = as_smancode
End IF

adwc_locations.SetTransObject(SQLCA)
adwc_locations.Retrieve(ls_first, ls_last)
end subroutine

public subroutine nf_gettsrs (ref datawindowchild adwc_tsrs, string as_location);String			ls_first, &
					ls_last
					

If as_location = '' Then 
	ls_first = '000'
	ls_last = 'ZZZ'
Else
	ls_first = as_location
	ls_last = as_location
End IF

adwc_tsrs.SetTransObject(SQLCA)
adwc_tsrs.Retrieve(ls_first, ls_last)
end subroutine

public subroutine nf_getcustomers (ref datawindowchild adwc_customers, string as_location);String			ls_first, &
					ls_last
					

If as_location = '' Then 
	ls_first = ' '
	ls_last = 'ZZZ'
Else
	ls_first = as_location
	ls_last = as_location
End IF

adwc_customers.SetTransObject(SQLCA)
adwc_customers.Retrieve(ls_first, ls_last)
end subroutine

public subroutine nf_getcorps (ref datawindowchild adwc_customers);adwc_customers.SetTransObject(SQLCA)
adwc_customers.Retrieve()
end subroutine

public subroutine nf_getbillto (ref datawindowchild adwc_customers);adwc_customers.SetTransObject(SQLCA)
adwc_customers.Retrieve()
end subroutine

public function boolean nf_loadpersondef ();Int 		li_rtn,&
			li_tab_pos1,&	
			li_tab_pos2,&
			li_tab_pos3


String	ls_userid,  &
			ls_sales_person_info_String, &
			ls_date,  &
			ls_salesperson_code,& 
			ls_smandivision,& 
			ls_smanlocation 

u_orp003	lu_orp003

s_error lstr_error_info

iw_frame.SetMicroHelp( "Loading Personal Defaults")
ls_date = ProfileString(iw_frame.is_UserINI, "System Settings","DefaultDate", String(RelativeDate(Today(),  -1), "MM/DD/YY"))

ls_salesperson_code = ProfileString(iw_frame.is_UserINI, "Orp", "SalesCode", "")
ls_smandivision = ProfileString( iw_frame.is_UserINI, "Orp", "Division", "")
ls_smanlocation = ProfileString( iw_frame.is_UserINI, "Orp", "Location", "")


IF (ls_smandivision = "" OR &
	ls_smanlocation = "") OR Date(ls_date) < Today() THEN

	iu_ws_orp2 = Create u_ws_orp2
	
	ls_sales_person_info_string = Space(12) 
	ls_userid = UPPER(Message.nf_getuserid())

	li_rtn = iu_ws_orp2.nf_orpo41fr(ls_userid,ls_sales_person_info_string,  lstr_error_info) 
	
	Destroy iu_ws_orp2
	
	IF Li_rtn > 0 THEN 
		Return FALSE
	ELSE
		IF LEN( TRIM( ls_sales_person_info_string)) > 0 Then
			li_tab_pos1 = POS( ls_sales_person_info_string,"~t")
			li_tab_pos2 = POS( ls_sales_person_info_string,"~t",li_tab_pos1+1)
			li_tab_pos3 = POS( ls_sales_person_info_string,"~t",li_tab_pos2+1)

			ls_salesperson_code = Mid( ls_sales_person_info_string, 1, li_tab_pos1 - 1)
			ls_smandivision = Mid( ls_sales_person_info_string, li_tab_pos1 + 1, li_tab_pos2 - li_tab_pos1 - 1) 
			ls_smanlocation = Mid( ls_sales_person_info_string, li_tab_pos2 + 1, LEN(ls_sales_person_info_string) - li_tab_pos2)
	   		Message.nf_update_User_Info( ls_salesperson_code, ls_smandivision, ls_smanlocation)

			li_rtn = SetProfileString( iw_frame.is_UserINI, "System Settings", "DefaultDate", String(Today(), "MM/DD/YY"))
			li_rtn = SetProfileString( iw_frame.is_UserINI, "Orp", "SalesCode", ls_salesperson_code)
			li_rtn = SetProfileString( iw_frame.is_UserINI, "Orp", "Division", ls_smandivision)
			li_rtn = SetProfileString( iw_frame.is_UserINI, "Orp", "Location", ls_smanlocation)
		ELSE
			iw_frame.SetMicroHelp( "No location - division - salesperson code found for " + ls_userid)
			RETURN FALSE
   	END IF
	END IF
ELSE
	Message.nf_update_User_Info( ls_salesperson_code, ls_smandivision, ls_smanlocation)	
END IF
RETURN TRUE
end function

public subroutine nf_getservicecenters (ref datawindowchild adwc_servicecenters, string as_location);String			ls_first, &
					ls_last
					

If as_location = '' Then 
	ls_first = '000'
	ls_last = 'ZZZ'
Else
	ls_first = as_location
	ls_last = as_location
End IF

adwc_servicecenters.SetTransObject(SQLCA)
adwc_servicecenters.Retrieve(ls_first, ls_last)
end subroutine

public subroutine nf_getdivisions (ref datawindowchild adwc_divisions);long		ll_row

adwc_divisions.SetTransObject(SQLCA)
ll_row = adwc_divisions.Retrieve('DIVCODE')

Return
end subroutine

public subroutine nf_set_s_error (ref s_error astr_s_error);astr_s_error.se_app_name = Message.nf_get_app_id()
astr_s_error.se_window_name = iw_Frame.GetActiveSheet().ClassName()
astr_s_error.se_user_id  = SQLCA.UserId
astr_s_error.se_Return_Code = '0'
astr_s_error.se_message  = Space(70)
astr_S_Error.se_RVal = 0 

end subroutine

public subroutine nf_replace_rows (ref datawindow adw_replace_dw, string as_replace_string);//DataStore						lds_datastore
//Integer							li_count
//String							ls_selected
//u_string_functions	lu_string_functions
//
//lds_datastore = CREATE datastore 
//lds_datastore.DataObject = adw_replace_dw.DataObject
//lds_DataStore.Object.Data = adw_replace_dw.Object.Data.Selected
//
//ls_selected = lds_DataStore.Object.DataWindow.Data
//
//IF lu_string_functions.nf_isempty(ls_selected) Then
//	adw_replace_dw.Reset()
//	adw_replace_dw.ImportString(as_replace_string)
//	Return
//ENd IF 
//lds_DataStore.Reset()
//li_count = lds_datastore.ImportString(as_replace_string)
//If li_Count <  1 Then
//		// For some reason the Data did not import.. Put back the rows that were there selected
//		li_count = lds_datastore.ImportString(ls_selected)
//END IF
//adw_replace_dw.Object.Data.Selected = lds_DataStore.Object.Data
//Destroy lds_DataStore
//Return
////////////////////////////////////////////////
DataStore						lds_datastore
Long								ll_arowcount, ll_arow, ll_hold
Integer							li_count
String							ls_selected						
u_string_functions			lu_string_functions

lds_datastore = CREATE datastore 
lds_datastore.DataObject = adw_replace_dw.DataObject
lds_DataStore.Object.Data = adw_replace_dw.Object.Data.Selected

ls_selected = lds_DataStore.Object.DataWindow.Data
//bad if statement you loose all lines that are not selected possibly delete the reset
IF lu_string_functions.nf_isempty(ls_selected) Then
	adw_replace_dw.Reset()
	adw_replace_dw.ImportString(as_replace_string)
	Return
End if 
lds_DataStore.Reset()
li_count = lds_datastore.ImportString(as_replace_string)
If li_Count <  1 Then
		//For some reason the Data did not import.. Put back the rows that were there selected
		li_count = lds_datastore.ImportString(ls_selected)
END IF
////////////////sem
ll_arowcount = lds_DataStore.RowCount()
ll_arow = adw_replace_dw.GetSelectedRow(0)
If ll_arow > 0 Then
	ll_hold = 1
	Do
		If ll_hold > ll_arowcount Then
//			MessageBox("u_project_functions","nf_replace_rows")
			ll_arow = 0
		Else
			adw_replace_dw.RowsDiscard(ll_arow, ll_arow, Primary!)
			lds_DataStore.RowsCopy(ll_hold, ll_hold, Primary!, adw_replace_dw, ll_arow, Primary!)
			adw_replace_dw.SelectRow(ll_arow, True)
			ll_hold += 1
			ll_arow = adw_replace_dw.GetSelectedRow(ll_arow)
		End If
	Loop Until ll_arow  <= 0
End If
////////////////sem
//adw_replace_dw.Object.Data.Selected = lds_DataStore.Object.Data
//Destroy lds_DataStore
Return
end subroutine

public function integer nf_gen_sales (string as_data);//Datastore						lds_datastore
//
//
//lds_datastore = CREATE ds_additional_datastore
//
//OpenWithParm(w_additional_data, as_data)
//
//ls_data = Message.StringParm
//f_ParseLeftRight(ls_data, '~x', is_additional_data, ls_action)
//		
//Choose Case ls_Action
//Case "cb_pa"
//	SetPointer(HourGlass!)
//	OpenSheetWithParm(lw_open, This, "w_pa_inquiry", iw_frame, 8, iw_frame.im_menu.iao_arrangeopen)
//	dw_detail.SetRedraw(True)
//	Return 0
//
//Case "cb_res_browse"
//	SetPointer(HourGlass!)
//	ll_12th_Tab = f_npos(is_Additional_Data, "~t", 1, 12)
//	ll_13th_Tab = Pos(is_Additional_Data, "~t", ll_12th_Tab + 1)
//	ll_14th_Tab = Pos(is_Additional_Data, "~t", ll_13th_Tab + 1)
//	ll_15th_Tab = Pos(is_Additional_Data, "~t", ll_14th_Tab + 1)
//
//	ls_ParmString = "~t~t" + &
//						Mid(is_Additional_Data, ll_13th_Tab + 1, & 
//						(ll_14th_Tab  - ll_13th_Tab) - 1) + "~t" + &
//						Mid(is_Additional_Data, ll_12th_Tab + 1, &
//						(ll_13th_Tab  - ll_12th_Tab) - 1) + "~t" + &
//						dw_header.GetItemString(1, "sales_person_code") + "~t~t" + &
//						Mid(is_Additional_Data, ll_15th_Tab + 1) + "~r~n"
//
//	ll_RowCount = dw_detail.Rowcount()
//	
//	FOR li_counter = 1 TO ll_RowCount
//		If dw_detail.IsSelected(li_counter) Then
//			ls_temp = String(dw_detail.GetItemDecimal(li_counter, "ordered_units"))
//			If IsNull(ls_temp) Then ls_temp = '0.0'
//			ls_ParmString += ls_temp + "~t"
//	
//			ls_temp = dw_detail.GetItemString(li_counter, "sku_product_code")
//			If IsNull(ls_temp) Then ls_temp = ''
//			ls_ParmString += ls_temp + "~t"
//	
//			ls_temp = dw_detail.GetItemString(li_counter, "ordered_age")
//			If IsNull(ls_temp) Then ls_temp = ''
//			ls_ParmString += ls_temp + "~t"
//
//			ls_temp =dw_detail.GetItemString(li_counter, "ordered_units_uom")
//			If IsNull(ls_temp) Then ls_temp = ''
//			ls_ParmString += ls_temp + "~r~n"				
//		End if
//	NEXT
//
//	OpenWithParm(w_res_browse-product, ls_ParmString)
//	ls_ResReductions = Message.StringParm
//
//	IF TRIM(ls_ResReductions) = "ABORT" THEN 
//		RETURN -1
//	End IF
//
//	ls_requested_plant = Right(ls_ResReductions, 3)
//	ls_ResReductions = Left(ls_ResReductions, Len(ls_ResReductions) - 3)
//
//	If Not f_IsEmpty(ls_requested_plant) Then 
//		is_additional_data = Replace(is_additional_data, ll_14th_tab + 1, 3, ls_requested_plant)
//	End If
//
//Case "cb_cancel"
//	return 0
//End Choose
//Choose Case ls_button_ClassName
//	Case "cb_gen_so"		// Gen SO button pressed
//		dw_Header.SetItem(1,"type_of_order", "R")
//		ls_Header = dw_header.Describe("DataWindow.Data")&
//					+"~t"+Mid( ls_MessageStringParm, 1,ll_Pos_x -1)
//		ls_Detail = f_GetSelectedRows( dw_detail)
//		ls_Res_Info = ""
//		SetMicroHelp( String(dw_Detail.RowCount()))
//	
//	CASE Else	// AnyThing Else
//			dw_Detail.SetRow( ll_Current_Row)
//			dw_Detail.ScrolltoRow(ll_Current_Row)
//			Return -1
//END CHOOSE
//
//dw_header.SetItem(1, 'type_of_order', 'C')
//ls_header = dw_header.Describe("DataWindow.Data") + '~t' + is_additional_data
//
//li_ret = iu_orp003.nf_orpo39ar(istr_error_info, &
//							ls_header, &
//							ls_data, &
//							ls_ResReductions)
//
//
Return -1
end function

public function integer nf_gettutltype (ref datawindowchild adw_dddatawindow, string as_recordtype);adw_dddataWindow.SetTransObject( SQLCA)
Return adw_dddataWindow.Retrieve( as_RecordType)
	

end function

public function string nf_buildupdatestring (ref datawindow adw_tobuildfrom);///////////////////////////////////////////////////////////////////////////////////////
//
// Modified Date		Author			Description
//
// 7/18/96				Tim Bornholtz	This will now read the delete buffer also.
//
///////////////////////////////////////////////////////////////////////////////////////


Int		li_counter, &
			lia_SelectedRows[], &
			li_SelectedUpperBound,&
			li_MaskCounter

Long		ll_RowCount, &
			ll_ColumnCount, &
			ll_DeletedCount

String 	ls_update_flag, &
			ls_data, &
			ls_OldMask[]


If Integer(adw_tobuildfrom.Describe("update_flag.id")) < 1 Then 
	return "you don't have an update_flag column"
End if

adw_tobuildfrom.SetRedraw(False)

ll_RowCount = adw_tobuildfrom.RowCount()

For li_Counter = 1 to ll_RowCount
	If adw_ToBuildFrom.IsSelected(li_Counter) Then
		li_SelectedUpperBound ++
		lia_SelectedRows[li_SelectedUpperBound] = li_counter
	End if
Next

For li_counter = 1 to ll_RowCount
	ls_update_flag = adw_ToBuildFrom.GetItemString(li_counter, 'update_flag')
	If ls_update_flag = 'A' or ls_update_flag = 'U' Then
		adw_ToBuildFrom.RowsCopy(li_counter, li_counter, Primary!, adw_ToBuildFrom, 10000, Filter!)
	Else
		adw_ToBuildFrom.RowsMove(li_counter, li_counter, Primary!, adw_ToBuildFrom, 10000, Filter!)
		ll_RowCount --
		li_counter --
	End if
Next

ll_DeletedCount = adw_ToBuildFrom.DeletedCount()
If ll_DeletedCount > 0 Then
	adw_ToBuildFrom.RowsMove( 1, ll_DeletedCount, Delete!, adw_ToBuildFrom, 10000, Primary!)
	// Set update flag to 'D'
	// ll_RowCount is set to the OLD RowCount
	For li_Counter = ll_RowCount + 1 to ll_RowCount + ll_DeletedCount
		adw_ToBuildFrom.SetItem(li_Counter, 'update_flag', 'D')
	Next
End if

ll_ColumnCount = Long(adw_ToBuildFrom.Describe("DataWindow.Column.Count"))
For li_Counter = 1 to ll_ColumnCount
	// Get the column type of the clicked field
	If Left( Lower(adw_ToBuildFrom.Describe("#" + String(li_Counter) + ".ColType")), 4) = 'date' Then
		li_MaskCounter++
		ls_OldMask[li_MaskCounter] = adw_tobuildfrom.Describe("#" + String(li_Counter) + ".EditMask.Mask")
		adw_tobuildfrom.Modify("#" + String(li_Counter) + ".Edit.format = 'yyyy-mm-dd'")
	End if
Next

ls_data = adw_ToBuildFrom.Describe("DataWindow.Data")
li_MaskCounter = 0
For li_Counter = 1 to ll_ColumnCount
	If Left( Lower(adw_ToBuildFrom.Describe("#" + String(li_Counter) + ".ColType")), 4) = 'date' Then
			li_MaskCounter ++
			adw_tobuildfrom.Modify("#" + String(li_Counter) + ".EditMask.Mask ='" +ls_OldMask[li_MaskCounter]+"'" )
	End if
Next

adw_ToBuildFrom.RowsDiscard(1, adw_ToBuildFrom.RowCount(), Primary!)
adw_ToBuildFrom.RowsMove(1, adw_ToBuildFrom.FilteredCount(), Filter!, adw_ToBuildFrom, 1, Primary!)

adw_ToBuildFrom.SelectRow(0, False)
For li_Counter = 1 to li_SelectedUpperBound
	adw_ToBuildFrom.SelectRow(lia_SelectedRows[li_counter], True)
Next
adw_tobuildfrom.SetRedraw(True)

return ls_data
end function

public subroutine nf_loadcustomers ();String	ls_Location

ids_customers = CREATE DATASTORE
ids_customers.DataObject = 'd_customer_data'
ls_Location = message.is_smanlocation
iw_frame.SetMicroHelp( "Loading Customers for location " + ls_Location)
ids_customers.SetTransObject(SQLCA)
ids_customers.Retrieve( ls_Location, ls_Location)


ids_billtos = CREATE DATASTORE
ids_billtos.DataObject = 'd_billto_customer_data'
ids_billtos.SetTransObject( SQLCA)
ids_billtos.Retrieve()

ids_corps = CREATE DATASTORE
ids_corps.DataObject = 'd_corp_customer_data'
ids_corps.SetTransObject( SQLCA)
ids_corps.Retrieve()





end subroutine

public subroutine nf_list_sales_orders (string as_salesid);Window	ls_sheet
w_sales_order_detail lw_Temp
Long		ll_Number_of_Windows

ls_Sheet = iw_frame.GetFirstSheet( )
ll_Number_Of_Windows ++
Do While IsValid( ls_Sheet)
	If ls_Sheet.ClassName()  = 'w_sales_order_detail' Then
		OPenWithPArm( w_list_of_orders, as_SalesID)
		Return
	END IF
	ls_Sheet = iw_frame.GetNextSheet( ls_Sheet)
Loop

// OPen a new window
//iw_frame.wf_OpenSheet("w_sales_order_detail", as_SalesID)
opensheetwithparm(lw_Temp, as_SalesID, "w_sales_order_detail", &
	  					iw_frame,0,iw_frame.im_menu.iao_arrangeOpen)

Return

end subroutine

public function integer nf_gpo_week (integer ai_month, long al_year);Integer		li_week_number

Choose Case ai_month
Case 1
	li_week_number = 4
Case 2
	li_week_number = 4
Case 3
	li_week_number = 5
Case 4
	li_week_number = 4
Case 5
	li_week_number = 4
Case 6
	li_week_number = 5
Case 7
	li_week_number = 4
Case 8
	li_week_number = 4
Case 9
	li_week_number = 5
Case 10
	If DayNumber(date(string(String(al_year - 1) + "-12-31"))) = 6 Then 
		li_week_number = 5 
	Else
		li_week_number = 4
	End If
Case 11
	li_week_number = 5
Case 12
	li_week_number = 4
End Choose

Return li_week_number
end function

public function integer nf_validatecustomer (string as_customer_id, string as_customer_type, ref string as_customer_name, ref string as_customer_city);U_Base_DataStore	lds_Defaults
Int			li_Rtn

Long			ll_Loop			
			
			
String		ls_Header_String,&
				ls_Detail_String,&
				ls_Default_String

S_Error		lstr_Error_Info

u_orp002	lu_orp002

lu_orp002 = Create u_orp002	
iu_ws_orp4 = Create u_ws_orp4

Choose Case as_Customer_Type
	Case 'S'
		SELECT customers.short_name, customers.city
			INTO :as_Customer_Name, :as_Customer_City
			FROM customers
			WHERE customers.customer_id = :as_Customer_ID
			USING SQLCA;
	Case 'B'
		SELECT billtocust.name, billtocust.city
			INTO :as_Customer_Name, :as_Customer_City
			FROM billtocust
			WHERE billtocust.bill_to_id = :as_Customer_ID
			USING SQLCA;

	Case 'C'
		SELECT corpcust.name, corpcust.city
			INTO :as_Customer_Name, :as_Customer_City
			FROM corpcust
			WHERE corpcust.corp_id = :as_Customer_ID
			USING SQLCA;

END CHOOSE

If SQLCA.SQLCode = 100 then
	ls_Header_String = as_Customer_ID+"~t"+as_Customer_Type
	ls_Detail_String 	= Space(60000)
	ls_Default_String = Space(60000)
//	li_Rtn = lu_Orp002.nf_orpo77_validate_customers( lstr_Error_Info, ls_Header_String, ls_Detail_String,ls_Default_String)
	li_Rtn = iu_ws_orp4.nf_orpo77fr( lstr_Error_Info, ls_Header_String, ls_Detail_String,ls_Default_String)
	IF li_Rtn > 0 Then
		lds_Defaults = Create u_base_DataStore
		lds_Defaults.DataObject = 'd_customer_defaults'
		lds_Defaults.Reset()
		li_rtn = lds_Defaults.ImportString( ls_Default_String)
		FOR ll_Loop = 1 to li_rtn
			lds_Defaults.SetItem( ll_Loop, "sales_location", Message.is_Smanlocation)
			lds_Defaults.SetItemStatus(ll_Loop, 0, Primary!, NewModified!)	
		Next
		lds_Defaults.SetTransObject( SQLCA)
		lds_Defaults.Update()
		COMMIT ;
		Choose Case as_Customer_Type
			Case 'S'
				lds_Defaults.DataObject = 'd_customer_data'			
			Case 'B'
				lds_Defaults.DataObject = 'd_billto_customer_data'
			Case 'C'
				lds_Defaults.DataObject = 'd_corp_customer_data'  
		END CHOOSE
		lds_Defaults.Reset()
		li_rtn = lds_Defaults.ImportString( ls_detail_String)
	
		IF as_customer_type = 'S' Then
			FOR ll_Loop = 1 to li_rtn
				lds_Defaults.SetItem( ll_Loop, "location", Message.is_Smanlocation)
				lds_Defaults.SetItemStatus(ll_Loop, 0, Primary!, NewModified!)	
			Next
		END IF
		lds_Defaults.SetTransObject( SQLCA)
		lds_Defaults.Update()
		COMMIT ;
		Destroy lds_Defaults
		Choose Case as_Customer_Type
			Case 'S'
				SELECT customers.short_name, customers.city
				INTO :as_Customer_Name, :as_Customer_City
				FROM customers
				WHERE customers.customer_id = :as_Customer_ID
				USING SQLCA;
			Case 'B'
				SELECT billtocust.name, billtocust.city
				INTO :as_Customer_Name, :as_Customer_City
				FROM billtocust
				WHERE billtocust.bill_to_id = :as_Customer_ID
				USING SQLCA;

			Case 'C'
				SELECT corpcust.name, corpcust.city
				INTO :as_Customer_Name, :as_Customer_City
				FROM corpcust
				WHERE corpcust.corp_id = :as_Customer_ID
				USING SQLCA;

		END CHOOSE
		If SQLCA.SQLCode = 100 then Return -1
	ELSE
		Return -1
	END IF
END IF

Destroy lu_Orp002
Destroy iu_ws_orp4

Return 1
end function

public function string nf_getdefault_tsr_for_customer (string as_customer_id);String	ls_default_TSR,&
			ls_Division,&
			ls_sales_location
			
ls_Division = Message.is_smandivision			
ls_sales_location = Message.is_smanlocation

SELECT customer_defaults.sman_code
	INTO :ls_default_TSR
	FROM customer_defaults
	WHERE customer_defaults.sales_division = :ls_Division
	AND 	customer_defaults.sales_location = :ls_sales_location
	AND	customer_defaults.customer_id   = :as_Customer_id
	USING SQLCA ;

if SQLCA.SQLCode = 100 then
	ls_Division = '11'

	SELECT customer_defaults.sman_code
	INTO :ls_default_TSR
	FROM customer_defaults
	WHERE customer_defaults.sales_division = :ls_Division
	AND	customer_defaults.customer_id   = :as_Customer_id
	USING SQLCA ;
	if SQLCA.SQLCode <> 0 then
		Return Message.is_salesperson_code
	ELSE
		Return ls_default_TSR
	END IF
		
ELSE
	Return ls_default_TSR
END IF
	
end function

public function integer nf_load_products ();Long	ll_RowsRetrieved

ids_products = Create	DataStore
ids_products.DataObject = "d_product_codes"
IF ids_products.SetTransObject( SQLCA) < 1 Then 
	MessageBox("Error", "Error setting transaction Object")
	Return -1
END IF

Select Count(*)
into : ll_RowsRetrieved
From sku_products;

iw_frame.SetMicroHelp("Loading "+String(ll_rowsRetrieved)+" product codes please wait")
ll_RowsRetrieved = ids_products.Retrieve()
IF ll_RowsRetrieved < 0 Then
	MessageBox("Error Retrieving Product Codes","SQLCode: "+String(SQLCA.SQLCode)+"~r~n"+&
					"SQLCAErrText: '"+SQLCA.SQLERRText+"'~r~n"+&
					"SQLDBCode: "+String(SQLCA.SQLDBCODE)+"~r~n"+&
					"SQLNRows: "+String(SQLCA.SQLNRows)+"~r~n"+&
					"SQLReturnData: '"+SQLCA.SQLRETURNDATA+"'")
END IF

Return ll_RowsRetrieved


end function

public subroutine nf_openorderconfirmation (readonly w_base_sheet_ext aw_parentwindow);String	ls_Order_ID

window	lw_OrderConfirmation

aw_ParentWindow.Event ue_Get_Data("Order_ID")

ls_Order_Id = Message.StringParm

OpenSheetWithParm(lw_OrderConfirmation, ls_Order_ID, "w_sales_order_confirmation", &
						iw_frame, 0, iw_frame.im_menu.iao_arrangeOpen)
end subroutine

public function integer nf_getgpomonth (date gpodate);u_String_Functions	lu_StringFunctions
String	ls_Temp_String,&
			ls_Token
			
ls_Temp_String = nf_GetGPODate(gpodate)
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
Return Integer(ls_Token)
end function

public function integer nf_getgpoweek (date gpodate);u_String_Functions	lu_StringFunctions
String	ls_Temp_String,&
			ls_Token
			
ls_Temp_String = nf_GetGPODate(gpodate)
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
Return Integer(ls_Token)
end function

public function integer nf_getgpoyear (date gpodate);u_String_Functions	lu_StringFunctions
String	ls_Temp_String,&
			ls_Token
			
ls_Temp_String = nf_GetGPODate(gpodate)
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
ls_Token = ls_Temp_String
Return Integer(ls_Token)
end function

public function string nf_getgpodate (date gpo_date);Date		ld_MondayOfWeek,&
			ld_MonthStart

Integer	li_DayNumber,&
			li_RelativeMonday,&
			li_DayNumberOf1st,&
			li_DayNumberof1stMonday,&
			li_Number_OF_Weeks

String	ls_1stOfMonth


li_DayNumber = DayNumber(GPO_Date)
li_RelativeMonday = li_DayNumber - 2
ld_MondayofWeek = RelativeDate(gpo_date, -(li_RelativeMonday))

ls_1stOfMonth = String(Month(GPO_Date))+"/01/"+String(year(GPO_Date))
ld_MonthStart = Date(ls_1stOfMonth)
li_DayNumberOf1st = DayNumber(ld_MonthStart)
li_DayNumberof1stMonday = li_DayNumberOf1st - 2
ld_MonthStart = RelativeDate(ld_MonthStart,-(li_DayNumberof1stMonday))

li_Number_OF_Weeks = nf_GPO_Week(Month(gpo_date),Year(gpo_date))

Choose Case True
	Case ld_MonthStart <= gpo_date and RelativeDate(ld_MonthStart,+6) >= gpo_date
		Return "1~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))
	Case	RelativeDate(ld_MonthStart,+7) <= gpo_date and RelativeDate(ld_MonthStart,+13) >= gpo_date 
		Return "2~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))
	Case	RelativeDate(ld_MonthStart,+14) <= gpo_date and RelativeDate(ld_MonthStart,+20) >= gpo_date 
		Return "3~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))	
	Case	RelativeDate(ld_MonthStart,+21) <= gpo_date and RelativeDate(ld_MonthStart,+27) >=gpo_date 
		Return "4~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))	
	Case	RelativeDate(ld_MonthStart,+28) <= gpo_date and RelativeDate(ld_MonthStart,+34) >=gpo_date 
		IF li_Number_OF_Weeks = 5 Then
			Return "5~t"+String(Month(gpo_date))	
		ELSE
			IF Month(gpo_date) = 12 Then
				Return "1~t01"+"~t"+String(Year(gpo_date)+1)
			ELSE
				Return "1~t"+String(Month(gpo_date)+1)+"~t"+String(Year(gpo_date))	
			END IF
		END IF	
End Choose

end function

public function string nf_get_age_code (string sku_product_code);//IF Left(sku_product_code,2) = 'D0' AND &
//		(Mid(sku_product_code,6,2) = 'BH' OR & 
//		 Mid(sku_product_code,6,2) = 'BA' OR &
//		 Mid(sku_product_code,6,2) = 'BP') Then
//	Return ('A')
//ELSE
//	Return ('B')
//END IF
String ls_pc, ls_asc, ls_prod

ls_prod = TRIM(sku_product_code) + space(10 - Len(TRIM(sku_product_code)))
Choose case Left(sku_product_code,1)
	case 'F', 'P', 'B', 'I'
		Return ('C')
	case else
		Select primal_code, age_schedule_code
	Into :ls_pc, :ls_asc
	From sku_products
	Where sku_product_code = :ls_prod
	Using SQLCA;
	
	If SQLCA.SQLCode = 0 Then
		ls_asc = TRIM(ls_asc) + space(3 - Len(TRIM(ls_asc)))
		ls_pc = TRIM(ls_pc) + space(2 - Len(TRIM(ls_pc)))
		If ls_asc = 'GB ' and ls_pc = '00' Then
			Return ('A')
		Else
			Return ('B')
		End If
	ELSE
		Return ('B')
	END IF
end choose
		
//IF Left(sku_product_code,1) = 'F' or  Left(sku_product_code,1) = 'P' or  Left(sku_product_code,1) = 'B' &
//	 or  Left(sku_product_code,1) = 'I' Then
//	Return ('C')
//Else
//	Select primal_code, age_schedule_code
//	Into :ls_pc, :ls_asc
//	From sku_products
//	Where sku_product_code = :ls_prod
//	Using SQLCA;
//	
//	If SQLCA.SQLCode = 0 Then
//		ls_asc = TRIM(ls_asc) + space(3 - Len(TRIM(ls_asc)))
//		ls_pc = TRIM(ls_pc) + space(2 - Len(TRIM(ls_pc)))
//		If ls_asc = 'GB ' and ls_pc = '00' Then
//			Return ('A')
//		Else
//			Return ('B')
//		End If
//	ELSE
//		Return ('B')
//	END IF
//END IF
end function

public function date nf_get_begin_shipdate (date gpo_date);// Gets the begin ShipDate of the Month in the Date Sent
Date		ld_MondayOfWeek,&
			ld_MonthStart

Integer	li_DayNumber,&
			li_RelativeMonday,&
			li_DayNumberOf1st,&
			li_DayNumberof1stMonday,&
			li_Number_OF_Weeks

String	ls_1stOfMonth


li_DayNumber = DayNumber(GPO_Date)
li_RelativeMonday = li_DayNumber - 2
ld_MondayofWeek = RelativeDate(gpo_date, -(li_RelativeMonday))

ls_1stOfMonth = String(Month(GPO_Date))+"/01/"+String(year(GPO_Date))
ld_MonthStart = Date(ls_1stOfMonth)
li_DayNumberOf1st = DayNumber(ld_MonthStart)
li_DayNumberof1stMonday = li_DayNumberOf1st - 2
ld_MonthStart = RelativeDate(ld_MonthStart,-(li_DayNumberof1stMonday))
Return ld_MonthStart

end function

public function date nf_get_end_shipdate (date gpo_date);// End Shipdate is Day Prior to Next Months StartDate
Date		ld_Begin_ShipDate

Integer	li_Number_of_Weeks,&
			li_month,&
			li_Year
			
			
ld_Begin_ShipDate = nf_get_Begin_ShipDate(gpo_date)

li_Month = nf_GetGPOMonth(gpo_Date)
li_year = nf_GetGPOYear(gpo_Date)
li_Number_of_Weeks = nf_Gpo_Week(li_Month,li_Year)

Return RelativeDate(RelativeDate(ld_Begin_ShipDate,7*li_Number_of_Weeks),-1)

end function

public function date nf_get_contract_due_date (date gpo_date);// ******************************************************************************************
// Contract Due Date is the Wednesday after 2 weeks prior of the Start of the Next Month
// For Example if Next months Start Date is on the 28th the Monday  2 weeks Prior would be 
// the 14th So that Next Wednesday is the 16th -
// Contract Due Date for the Next Month is the 16th
// ******************************************************************************************
Date		ld_Begin_ShipDate

Integer	li_Number_of_Weeks,&
			li_month,&
			li_Year
			
			
ld_Begin_ShipDate = nf_get_Begin_ShipDate(gpo_date)

li_Month = nf_GetGPOMonth(gpo_Date)
li_year = nf_GetGPOYear(gpo_Date)
li_Number_of_Weeks = nf_Gpo_Week(li_Month,li_Year)

Return RelativeDate(RelativeDate(ld_Begin_ShipDate,7*(li_Number_of_Weeks - 2)),+2)
end function

public function boolean nf_isscheduler ();IF isValid(iw_frame) then
	IF IsValid(iw_frame.iu_netwise_data) Then
		If iw_frame.iu_netwise_data.is_groupid = '105' Then return True
	ELSE
		MessageBox("ORP","InValid iu_Netwise_Data")
	END IF
ELSE
	MessageBox("ORP","Invalid iw_Frame")
END IF
return False
end function

public subroutine nf_openshortagequeue (readonly w_base_sheet_ext aw_parent_window);String	ls_Order_ID, ls_indicator

Window	lw_shortagequeue

ls_indicator = "S"

//aw_Parent_Window.Event ue_Get_Data("Order_ID")
//ls_Order_Id = Message.StringParm + '~t'
//ls_Order_Id += ls_indicator

OpenWithParm(w_product_shortage_queue, aw_parent_window)

aw_parent_window.Event ue_get_data('retrieve')
//???= Message.StringParm
end subroutine

public function integer nf_validatecustomerstate (string as_customer_id, string as_customer_type, ref string as_customer_name, ref string as_customer_city, ref string as_customer_state);Int			li_Rtn

Long			ll_Loop			
			
			
String		ls_Header_String,&
				ls_Detail_String,&
				ls_Default_String

S_Error		lstr_Error_Info

u_orp002		lu_orp002
lu_orp002 = Create u_orp002	

iu_ws_orp4 = Create u_ws_orp4

U_Base_DataStore	lds_Defaults

Choose Case as_Customer_Type
	Case 'S'
		SELECT customers.short_name, customers.city, customers.state
			INTO :as_Customer_Name, :as_Customer_City, :as_customer_state
			FROM customers
			WHERE customers.customer_id = :as_Customer_ID
			USING SQLCA;
	Case 'B'
		SELECT billtocust.name, billtocust.city, billtocust.state
			INTO :as_Customer_Name, :as_Customer_City, :as_Customer_state
			FROM billtocust
			WHERE billtocust.bill_to_id = :as_Customer_ID
			USING SQLCA;

	Case 'C'
		SELECT corpcust.name, corpcust.city, corpcust.state
			INTO :as_Customer_Name, :as_Customer_City, :as_Customer_state
			FROM corpcust
			WHERE corpcust.corp_id = :as_Customer_ID
			USING SQLCA;

END CHOOSE

If SQLCA.SQLCode = 100 then
	ls_Header_String = as_Customer_ID+"~t"+as_Customer_Type
	ls_Detail_String 	= Space(60000)
	ls_Default_String = Space(60000)
	//li_Rtn = lu_Orp002.nf_orpo77_validate_customers( lstr_Error_Info, ls_Header_String, ls_Detail_String,ls_Default_String)
	li_Rtn = iu_ws_orp4.nf_orpo77fr( lstr_Error_Info, ls_Header_String, ls_Detail_String,ls_Default_String)
	IF li_Rtn > 0 Then
		lds_Defaults = Create u_base_DataStore
		lds_Defaults.DataObject = 'd_customer_defaults'
		lds_Defaults.Reset()
		li_rtn = lds_Defaults.ImportString( ls_Default_String)
		if li_rtn < 0 Then return -1 
		FOR ll_Loop = 1 to li_rtn
			lds_Defaults.SetItem( ll_Loop, "sales_location", Message.is_Smanlocation)
			lds_Defaults.SetItemStatus(ll_Loop, 0, Primary!, NewModified!)	
		Next
		lds_Defaults.SetTransObject( SQLCA)
		lds_Defaults.Update()
		COMMIT ;
		Choose Case as_Customer_Type
			Case 'S'
				lds_Defaults.DataObject = 'd_customer_data'			
			Case 'B'
				lds_Defaults.DataObject = 'd_billto_customer_data'
			Case 'C'
				lds_Defaults.DataObject = 'd_corp_customer_data'  
		END CHOOSE
		lds_Defaults.Reset()
		li_rtn = lds_Defaults.ImportString( ls_detail_String)
	
		IF as_customer_type = 'S' Then
			FOR ll_Loop = 1 to li_rtn
				lds_Defaults.SetItem( ll_Loop, "location", Message.is_Smanlocation)
				lds_Defaults.SetItemStatus(ll_Loop, 0, Primary!, NewModified!)	
			Next
		END IF
		lds_Defaults.SetTransObject( SQLCA)
		lds_Defaults.Update()
		COMMIT ;
		Destroy lds_Defaults
		Choose Case as_Customer_Type
			Case 'S'
				SELECT customers.short_name, customers.city,  customers.state
				INTO :as_Customer_Name, :as_Customer_City, :as_Customer_state
				FROM customers
				WHERE customers.customer_id = :as_Customer_ID
				USING SQLCA;
			Case 'B'
				SELECT billtocust.name, billtocust.city, billtocust.state
				INTO :as_Customer_Name, :as_Customer_City, :as_Customer_state
				FROM billtocust
				WHERE billtocust.bill_to_id = :as_Customer_ID
				USING SQLCA;

			Case 'C'
				SELECT corpcust.name, corpcust.city, corpcust.state
				INTO :as_Customer_Name, :as_Customer_City, :as_Customer_state
				FROM corpcust
				WHERE corpcust.corp_id = :as_Customer_ID
				USING SQLCA;

		END CHOOSE
		If SQLCA.SQLCode = 100 then Return -1
	ELSE
		Return -1
	END IF
END IF

Destroy lu_Orp002
Destroy iu_ws_orp4

Return 1
end function

public function boolean nf_plt_based_on_delivery (string as_plant);String	ls_short_desc

//as_plant = TRIM(as_plant) + space(8 - len(TRIM(as_plant)))

SELECT TUTLTYPES.type_short_desc
	INTO :ls_short_desc
	FROM TUTLTYPES
	WHERE record_type = 'PASLDTYP'
	  AND type_code = :as_plant
	USING SQLCA;

If SQLCA.SQLCode = 0 then
	If mid(ls_short_desc,3,1) = 'L' then
		Return True
	Else
		Return False
	End If
Else
	Return False
End If	
		


end function

public function boolean nf_issvcpresident ();IF isValid(iw_frame) then
	IF IsValid(iw_frame.iu_netwise_data) Then
		If (iw_frame.iu_netwise_data.is_groupid = '104') Then return True
	ELSE
		MessageBox("ORP","InValid iu_Netwise_Data")
	END IF
ELSE
	MessageBox("ORP","Invalid iw_Frame")
END IF
return False
end function

public subroutine nf_get_product_pallet_info ();Int			li_Rtn

Long			ll_length, &
				ll_count

String		ls_Output_string, &
				ls_temp

S_Error		lstr_Error_Info

u_orp204		lu_orp204
lu_orp204 = Create u_orp204	

iu_ws_orp4 = Create u_ws_orp4

ids_product_pallet_info = CREATE DATASTORE
ids_product_pallet_info.DataObject = 'd_sku_prod_pallet_info'

ls_output_String 	= Space(20000)
//dld 11/27/02 iw_frame.SetMicroHelp("Loading box information - please wait")
iw_frame.SetMicroHelp("Loading Product Pallet IEs nformation - please wait")

//li_Rtn = lu_Orp204.nf_orpo90br_get_product_pallet_info( lstr_Error_Info, ls_Output_String)
li_Rtn = iu_ws_orp4.nf_ORPO90FR( lstr_Error_Info, ls_Output_String)


IF li_Rtn = 0 Then
	ids_product_pallet_info.Reset()
	li_rtn = ids_product_pallet_info.ImportString( ls_Output_String)
	
	if li_rtn < 0 then
//dld	11/27/02 MessageBox ("Product pallet info", "Error importing product pallet information")
		MessageBox ("Loading Product Pallet Information", "Error importing product pallet information.  Please try again.")
		//ibdkdld 11/27/02 If there was an error destroy it so it will try to reload again 
		Destroy ids_product_pallet_info
		Return
	end if
	ll_count = ids_product_pallet_info.RowCount()
	if ll_count < 1 then
//dld	11/27/02 MessageBox ("Product pallet info", "Error row count product pallet information")
		MessageBox ("Loading Product Pallet Information", "Error no rows retrieved for product pallet information.  Please try again.")
		//ibdkdld 11/27/02 If there was an error destroy it so it will try to reload again 
		Destroy ids_product_pallet_info
		Return
	end if	
else	
//dld	11/27/02 MessageBox ("Product pallet info", "Error retrieving product pallet information")
	MessageBox ("Loading Product Pallet Information", "Error retrieving product pallet information.  Please try again.")
		//ibdkdld 11/27/02 If there was an error destroy it so it will try to reload again 
		Destroy ids_product_pallet_info
end if	
	

	





end subroutine

public function boolean nf_get_additional_datal (string as_customer_id, string as_customer_type, string as_division, date ad_delivery_date, ref string as_city, ref string as_state, ref string as_short_name, ref time at_delivery_time, ref string as_type_of_sale, ref string as_sub_ind);
Int			li_Rtn, &
				li_day

String		ls_Header_String,&
				ls_Detail_String,&
				ls_Default_String, ls_temp, &
				ls_division

S_Error		lstr_Error_Info

u_orp002		lu_orp002

lu_orp002 = Create u_orp002	
iu_ws_orp4 = Create u_ws_orp4

U_Base_DataStore	lds_Defaults

lds_Defaults = Create u_base_DataStore

SELECT customers.short_name, customers.city, customers.state
	INTO :as_short_name, :as_city, :as_state
	FROM customers
	WHERE customers.customer_id = :as_customer_id
	USING SQLCA;

If SQLCA.SQLCode = 100 then
	ls_Header_String = as_customer_id+ "~t" + as_customer_type
	ls_Detail_String 	= Space(60000)
	ls_Default_String = Space(60000)
	//li_Rtn = lu_Orp002.nf_orpo77_validate_customers( lstr_Error_Info, ls_Header_String, ls_Detail_String,ls_Default_String)
	li_Rtn = iu_ws_orp4.nf_orpo77fr( lstr_Error_Info, ls_Header_String, ls_Detail_String,ls_Default_String)
	IF li_Rtn > 0 Then
		lds_Defaults.DataObject = 'd_customer_data'
		lds_Defaults.Reset()
		li_rtn = lds_Defaults.ImportString( ls_Detail_String)
		if li_rtn > 0 then
			as_city = lds_Defaults.GetItemString(1, "city")
			as_state = lds_Defaults.GetItemString(1, "state")
			as_short_name = lds_Defaults.GetItemString(1, "short_name")
			
//			column type_of_sale no longer excits.
//			as_type_of_sale = lds_Defaults.GetItemSTring(1, "type_of_sale")
		end if
		
		lds_Defaults.DataObject = 'd_customer_defaults'
		lds_Defaults.Reset()
		li_rtn = lds_Defaults.ImportString( ls_Default_String)
		if li_rtn > 0 then
			li_day = DayNumber(ad_delivery_date)		
			as_type_of_sale = lds_Defaults.GetItemString(1, "type_of_sale")
			as_sub_ind = lds_Defaults.GetItemString(1, "sub")
	
			Choose case li_day
				Case 1
					at_delivery_time = lds_Defaults.GetItemTime(1, "sunday_delv_time")
				Case 2
					at_delivery_time = lds_Defaults.GetItemTime(1, "monday_delv_time")
				Case 3
					at_delivery_time = lds_Defaults.GetItemTime(1, "tuesday_delv_time")
				Case 4
					at_delivery_time = lds_Defaults.GetItemTime(1, "wednesday_delv_time")
				Case 5
					at_delivery_time = lds_Defaults.GetItemTime(1, "thursday_delv_time")
				Case 6
					at_delivery_time = lds_Defaults.GetItemTime(1, "friday_delv_time")
				Case 7
					at_delivery_time = lds_Defaults.GetItemTime(1, "saturday_delv_time")	
			End Choose
		end if
	end if
else
	Select customer_defaults.type_of_sale, customer_defaults.sub
	INTO :as_type_of_sale, :as_sub_ind
	FROM customer_defaults
 	WHERE customer_defaults.sales_division = :as_division AND 
			customer_defaults.customer_id = :as_Customer_id
	USING SQLCA ;
	if SQLCA.SQLCode = 100 then
		ls_division = '11'
		Select customer_defaults.type_of_sale
		INTO :as_type_of_sale
		FROM customer_defaults
 		WHERE customer_defaults.sales_division = :ls_division AND 
				customer_defaults.customer_id = :as_Customer_id
		USING SQLCA ;
		if SQLCA.SQLCode = 100 then
			if mid(as_Customer_id,1,3) = 'I59' then
				as_type_of_sale = 'T'
			else
				as_type_of_sale = 'D'
			end if
		end if
	end if		
end if

Destroy lu_Orp002
Destroy lds_Defaults
Destroy iu_ws_orp4

Return True
end function

public function string nf_get_age_code (string as_sku_product_code, string as_customer_id, string as_plant_code);BOOLEAN 	lb_Fresh
STRING	ls_location_type

SELECT 	location_type
INTO		:ls_location_type
FROM     locations
WHERE    (location_code = :as_plant_code)
ORDER BY location_code
Using SQLCA;

If SQLCA.SQLCode = 0 Then
	choose case upper(ls_location_type)
		case 'W', 'X', 'Y', 'T', 'Z'
			lb_Fresh = false
		case else
			lb_Fresh = true
	end choose
	
	return nf_get_age_code(as_sku_product_code, as_customer_id, lb_Fresh)
Else
	return nf_get_age_code(as_sku_product_code)
END IF


end function

public function string nf_get_age_code (string as_sku_product_code, string as_customer_id, boolean ab_fresh);String 	ls_sku_product_code, &
			ls_customer_id, &
			ls_age_code, &
			ls_ProductState, &
			ls_product_12char, &
			ls_crk_age_default, &
			ls_age_schedule_code

INTEGER	li_CFM_TO_DAYS_OLD_DELV, &
			li_SKU_TO_DAYS_OLD_DELV
		
ls_sku_product_code = TRIM(as_sku_product_code) + space(10 - Len(TRIM(as_sku_product_code)))
ls_product_12char = TRIM(as_sku_product_code) + space(12 - Len(TRIM(as_sku_product_code)))

ls_customer_id = TRIM(as_customer_id) + space(10 - Len(TRIM(as_customer_id)))
If ab_fresh Then
	ls_ProductState = '1'
Else
	ls_ProductState = '2'
End IF

SELECT mt_tskuage.AGE_CODE, mt_tskuage.AGE_SCHEDULE_CODE
INTO	 :ls_age_code, :ls_age_schedule_code
FROM 	 sku_products, mt_tskuage, mt_tcfmage
WHERE sku_products.sku_product_code 	= :ls_sku_product_code
AND	( 
			(sku_products.age_schedule_code 	= mt_tcfmage.AGE_SCHEDULE_CODE
			 AND :ls_ProductState					= '1')
		or (sku_products.frz_age_schd_code = mt_tcfmage.AGE_SCHEDULE_CODE
			 AND :ls_ProductState					= '2')		 
		)
AND	mt_tcfmage.CUSTOMER_ID 				= :ls_customer_id
AND 	mt_tcfmage.CUSTOMER_TYPE 			= 'S'
AND	mt_tcfmage.AGE_SCHEDULE_CODE		=  mt_tskuage.AGE_SCHEDULE_CODE 
AND	mt_tcfmage.TO_DAYS_OLD_DELV 		>= mt_tskuage.FROM_DAYS_OLD_DELV
AND	mt_tcfmage.TO_DAYS_OLD_DELV 		<= mt_tskuage.TO_DAYS_OLD_DELV
Using SQLCA;
		 
If SQLCA.SQLCode <> 0 Then
	SELECT TOP 1 
			mt_tcfmage.TO_DAYS_OLD_DELV, 
			mt_tskuage.TO_DAYS_OLD_DELV, 
			mt_tskuage.AGE_CODE
	INTO	:li_CFM_TO_DAYS_OLD_DELV, 
			:li_SKU_TO_DAYS_OLD_DELV,
			:ls_age_code
	FROM 	sku_products, 
			mt_tskuage, 
			mt_tcfmage
	WHERE sku_products.sku_product_code 	= :ls_sku_product_code
	AND	( 
				(sku_products.age_schedule_code 	= mt_tcfmage.AGE_SCHEDULE_CODE
				 AND :ls_ProductState					= '1')
			or (sku_products.frz_age_schd_code = mt_tcfmage.AGE_SCHEDULE_CODE
				 AND :ls_ProductState					= '2')		 
			)
	AND	mt_tcfmage.CUSTOMER_ID 				= :ls_customer_id
	AND 	mt_tcfmage.CUSTOMER_TYPE 			= 'S'
	AND	mt_tcfmage.AGE_SCHEDULE_CODE		=  mt_tskuage.AGE_SCHEDULE_CODE 
	AND 	mt_tskuage.TO_DAYS_OLD_DELV		= (SELECT MAX(mt_tskuage2.TO_DAYS_OLD_DELV)
															FROM mt_tskuage mt_tskuage2 
															WHERE mt_tskuage2.AGE_SCHEDULE_CODE = mt_tcfmage.AGE_SCHEDULE_CODE)
	Using SQLCA;
			
	If SQLCA.SQLCode <> 0 Then
		
		ls_age_code = nf_get_age_code(as_sku_product_code)	
		
	ELSEIF ISNULL(li_CFM_TO_DAYS_OLD_DELV) &
		OR ISNULL(li_SKU_TO_DAYS_OLD_DELV) &
			OR ISNULL(ls_age_code) THEN
			
		ls_age_code = nf_get_age_code(as_sku_product_code)	
		
	ELSEIF li_SKU_TO_DAYS_OLD_DELV > li_CFM_TO_DAYS_OLD_DELV THEN
		
		ls_age_code = nf_get_age_code(as_sku_product_code)	
		
	END IF
END IF

If ab_fresh and ls_age_schedule_code = 'CRK' Then
	Select type_desc
	Into :ls_crk_age_default
	From tutltypes
	Where record_type = 'CRKAGEDF' and type_short_desc = :ls_product_12char
	Using SQLCA;
	
	If SQLCA.SQLCode = 0 Then
		ls_age_code = mid(ls_crk_age_default,1,1)
		
	End If
End If



return ls_age_code

end function

public function boolean nf_isschedulermgr ();string ls_groupid, &
		ls_type_short_desc
		
IF isValid(iw_frame) then
	IF IsValid(iw_frame.iu_netwise_data) Then
		ls_groupid = iw_frame.iu_netwise_data.is_groupid
		
		SELECT tutltypes.type_short_desc  
   	 INTO :ls_type_short_desc  
   	 FROM tutltypes  
   	WHERE (LTRIM (tutltypes.type_code) = :ls_groupid ) AND
			( LTRIM(tutltypes.type_short_desc) = 'SCHEDULER' ) AND  
     	    ( LTRIM(tutltypes.record_type) = 'GRPMGMT' )   ;
   	
		if sqlca.sqlcode = 0 Then return True
	//	If iw_frame.iu_netwise_data.is_groupid = '107' Then return True
	ELSE
		MessageBox("ORP","InValid iu_Netwise_Data")
	END IF
ELSE
	MessageBox("ORP","Invalid iw_Frame")
END IF
return False


		

end function

public function boolean nf_issalesmgr ();string ls_groupid, &
		ls_type_short_desc
		
IF isValid(iw_frame) then
	IF IsValid(iw_frame.iu_netwise_data) Then
		ls_groupid = iw_frame.iu_netwise_data.is_groupid
		
		SELECT tutltypes.type_short_desc  
   	 INTO :ls_type_short_desc  
   	 FROM tutltypes  
		 WHERE (LTRIM (tutltypes.type_code) = :ls_groupid ) AND
			( LTRIM(tutltypes.type_short_desc) IN ('SALESMGT', 'SCHDMGT') ) AND  
     	    ( LTRIM(tutltypes.record_type) = 'GRPMGMT' )   ;
   	   	
		if sqlca.sqlcode = 0 Then return True
		//If (ls_groupid = '102') or (ls_groupid = '108') or (ls_groupid = '111') Then return True
	ELSE
		MessageBox("ORP","InValid iu_Netwise_Data")
	END IF
ELSE
	MessageBox("ORP","Invalid iw_Frame")
END IF
return False
end function

public subroutine nf_openorderedbolcomparison (w_base_sheet_ext aw_parentwindow);String	ls_Order_ID

window	lw_OrderedBOLComparison

aw_ParentWindow.Event ue_Get_Data("Order_ID")

ls_Order_Id = Message.StringParm

OpenSheetWithParm(lw_OrderedBOLComparison, ls_Order_ID, "w_ordered_bol_comparison", &
						iw_frame, 0, iw_frame.im_menu.iao_arrangeOpen)
end subroutine

on u_project_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_project_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;Destroy ids_customers
Destroy ids_billtos
Destroy ids_corps
Destroy ids_products
Destroy ids_product_pallet_info
Destroy iu_ws_orp4
end event

