HA$PBExportHeader$w_fax_email_response.srw
forward
global type w_fax_email_response from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_fax_email_response
end type
end forward

global type w_fax_email_response from w_base_response_ext
integer width = 2533
integer height = 740
long backcolor = 12632256
dw_1 dw_1
end type
global w_fax_email_response w_fax_email_response

type variables
u_pas201		iu_pas201
s_error		istr_error_info
string		is_displayed_order, &
				is_report_type
end variables

on w_fax_email_response.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_fax_email_response.destroy
call super::destroy
destroy(this.dw_1)
end on

event open;call super::open;iu_pas201 = Create u_pas201

//If IsValid(message.powerobjectparm) Then
//	If message.powerobjectparm.TypeOf() = window! Then
//		iw_parentwindow = message.powerobjectparm
//	End If
//End If
end event

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_Pas201
Close(This)
end event

event ue_postopen;call super::ue_postopen;//String	ls_input, ls_output, ls_email_fax_ind, ls_plant_code
//Integer	li_ret
//
//SetPointer(HourGlass!)
//
//iw_frame.SetMicroHelp("Wait.. Inquiring Database")
//
//This.SetRedraw(False) 
//
//istr_error_info.se_event_name = "wf_retrieve"
//istr_error_info.se_procedure_name = "nf_pasp73cr_inq_email_fax_info"
//istr_error_info.se_message = Space(71)
//
//If iw_parentwindow.Title = 'Load List Maintenance' then
//	is_displayed_order = ''
//	iw_parentwindow.Event ue_get_data("plant_code")
//	ls_plant_code = Message.StringParm
//Else
//	iw_parentwindow.Event ue_get_data("order_number")
//	is_displayed_order = Message.StringParm
//	ls_plant_code = ''
//End if 
//
//iw_parentwindow.Event ue_get_data("email_fax_ind")
//ls_email_fax_ind = Message.StringParm
//
//iw_parentwindow.Event ue_get_data("report_type")
//is_report_type = Message.StringParm
//
//If ls_email_fax_ind = 'E' Then
//	This.Title = 'Email Address Inquire'
//	dw_1.object.fax_email_info_t.Text = 'Email Address'
//Else
//	This.Title = 'Fax Number Inquire'
//	dw_1.object.fax_email_info_t.Text = 'Fax Number'
//End If
//
//ls_input = Is_displayed_order + '~t' + ls_plant_code + '~t' + ls_email_fax_ind + '~t'
//
//li_ret = iu_pas201.nf_pasp73cr_inq_fax_email_info(istr_error_info, & 
//										ls_input, &
//										ls_output)
//										
//
//dw_1.SetRedraw(False) 
//dw_1.Reset()
//dw_1.Importstring(ls_output)
//
//If (is_report_type = '22') or (is_report_type = '23') Then
//
//	If dw_1.GetItemString(1, "disp_transmit_ind") = 'Y' Then
//		dw_1.object.transmit_ind.Visible = True
//		dw_1.object.t_1.Text = 'Transmit to MH:'
//	Else
//		dw_1.object.transmit_ind.Visible = False
//		dw_1.object.t_1.Text = ''
//	End If	
//Else
//	dw_1.object.transmit_ind.Visible = False
//	dw_1.object.t_1.Text = ''
//End If
//
//
//dw_1.SetRedraw(True)
//
//SetPointer(Arrow!)
//iw_frame.SetMicroHelp("Ready")
//
//This.SetRedraw(TRue) 
//
//
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;//String	ls_input, &
//			ls_load_list_type, &
//			ls_plant_code, &
//			ls_ship_date, &
//			ls_division_code, &
//			ls_complex_code, &
//			ls_plant_type, &
//			ls_load_list_by_option
//			
//Integer 	li_rtn
//
//if dw_1.accepttext() = -1 then return
//
//ib_ok_to_close = True
//
//If iw_parentwindow.Title = 'Load List Maintenance' Then
//
//	iw_parentwindow.Event ue_get_data("load_list_type")
//	ls_load_list_type = Message.StringParm
//		
//	iw_parentwindow.Event ue_get_data("plant_code")
//	ls_plant_code = Message.StringParm
//
//	iw_parentwindow.Event ue_get_data("ship_date")
//	ls_ship_date = Message.StringParm
//	
//	iw_parentwindow.Event ue_get_data("division_code")
//	ls_division_code = Message.StringParm
//	
//	iw_parentwindow.Event ue_get_data("complex_code")
//	ls_complex_code = Message.StringParm
//	
//	iw_parentwindow.Event ue_get_data("plant_type")
//	ls_plant_type = Message.StringParm
//	
//	iw_parentwindow.Event ue_get_data("load_list_by_option")
//	ls_load_list_by_option = Message.StringParm
//	
//	ls_input = 	ls_load_list_type + '~t' + &
//					ls_plant_code + '~t' + &			
//					ls_ship_date + '~t' + &
//					ls_division_code + '~t' + &
//					ls_complex_code + '~t' + &
//					ls_plant_type + '~t' + &
//					ls_load_list_by_option + '~t' + &
//					'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + &	
//					is_report_type + '~t' + '' + '~t' + dw_1.GetItemString(1,"fax_email_info") + '~t' + dw_1.GetItemString(1,"transmit_ind") + '~t'
//
//Else
//	ls_input = 	'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
//					'' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + '' + '~t' + &
//					is_report_type + '~t' + is_displayed_order + '~t' + dw_1.GetItemString(1,"fax_email_info") + '~t' + dw_1.GetItemString(1,"transmit_ind") + '~t' 
//End if					
// 
//istr_error_info.se_event_name = "ue_sendloadlist"			  
//
//SetPointer(HourGlass!)
//
//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
//												
//SetPointer(Arrow!)	
//
//Close(This)
//
//
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_fax_email_response
integer x = 1120
integer y = 1112
integer taborder = 0
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_fax_email_response
integer x = 1394
integer y = 476
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_fax_email_response
integer x = 466
integer y = 468
end type

type cb_browse from w_base_response_ext`cb_browse within w_fax_email_response
end type

type dw_1 from u_base_dw_ext within w_fax_email_response
integer x = 142
integer y = 44
integer width = 2281
integer height = 388
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_fax_email_info"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(1)
end event

