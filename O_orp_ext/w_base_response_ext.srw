HA$PBExportHeader$w_base_response_ext.srw
$PBExportComments$Extension layer Repsonse window - inherited from w_base_repsonse.
forward
global type w_base_response_ext from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_base_response_ext
end type
end forward

global type w_base_response_ext from w_base_response
integer width = 1742
integer height = 676
boolean controlmenu = false
cb_browse cb_browse
end type
global w_base_response_ext w_base_response_ext

type variables

end variables

on w_base_response_ext.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
end on

on w_base_response_ext.destroy
call super::destroy
destroy(this.cb_browse)
end on

type cb_base_help from w_base_response`cb_base_help within w_base_response_ext
integer taborder = 30
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_base_response_ext
integer taborder = 40
end type

type cb_base_ok from w_base_response`cb_base_ok within w_base_response_ext
integer y = 96
integer taborder = 20
end type

type cb_browse from u_base_commandbutton_ext within w_base_response_ext
boolean visible = false
integer x = 2126
integer y = 396
integer height = 108
integer taborder = 10
string text = "&Browse"
end type

