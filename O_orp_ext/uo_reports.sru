HA$PBExportHeader$uo_reports.sru
forward
global type uo_reports from UserObject
end type
type cb_1 from commandbutton within uo_reports
end type
type cb_next from commandbutton within uo_reports
end type
type cb_previous from commandbutton within uo_reports
end type
type st_1 from statictext within uo_reports
end type
type ddlb_1 from u_base_dropdownlistbox_ext within uo_reports
end type
end forward

global type uo_reports from UserObject
int Width=2359
int Height=101
long BackColor=12632256
cb_1 cb_1
cb_next cb_next
cb_previous cb_previous
st_1 st_1
ddlb_1 ddlb_1
end type
global uo_reports uo_reports

type variables
w_Base_Report	iw_Parent
end variables

on constructor;iw_parent = Parent
end on

on uo_reports.create
this.cb_1=create cb_1
this.cb_next=create cb_next
this.cb_previous=create cb_previous
this.st_1=create st_1
this.ddlb_1=create ddlb_1
this.Control[]={ this.cb_1,&
this.cb_next,&
this.cb_previous,&
this.st_1,&
this.ddlb_1}
end on

on uo_reports.destroy
destroy(this.cb_1)
destroy(this.cb_next)
destroy(this.cb_previous)
destroy(this.st_1)
destroy(this.ddlb_1)
end on

type cb_1 from commandbutton within uo_reports
int X=42
int Y=9
int Width=247
int Height=77
int TabOrder=10
string Text="&Print"
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;iw_parent.dw_Page.Print(TRUE)
end on

type cb_next from commandbutton within uo_reports
int X=1285
int Y=9
int Width=247
int Height=77
int TabOrder=40
string Text="&Next"
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;iw_parent.dw_page.ScrollNextPage()
end on

type cb_previous from commandbutton within uo_reports
int X=974
int Y=9
int Width=270
int Height=77
int TabOrder=30
string Text="&Previous"
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;iw_parent.dw_page.scrollpriorpage()
end on

type st_1 from statictext within uo_reports
int X=366
int Y=9
int Width=247
int Height=73
boolean Enabled=false
string Text="Zoom %:"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_1 from u_base_dropdownlistbox_ext within uo_reports
int X=618
int Y=9
int TabOrder=20
boolean Sorted=false
boolean AllowEdit=true
int Limit=3
int TextSize=-7
string Item[]={"200",&
"150",&
"100",&
"50",&
"30"}
end type

on constructor;call u_base_dropdownlistbox_ext::constructor;This.Text = "100"
iw_parent.dw_Page.Modify("DataWindow.Zoom = " + This.Text)
iw_parent.dw_Page.Resize(INT(3511*INTEGER(This.Text)/100),INT(4034*INTEGER(This.Text)/100))
iw_Parent.TriggerEvent(Resize!)
end on

on modified;call u_base_dropdownlistbox_ext::modified;iw_parent.dw_Page.Modify("DataWindow.Zoom = " + This.Text)
iw_parent.dw_Page.Resize(INT(3511*INTEGER(This.Text)/100),INT(4034*INTEGER(This.Text)/100))
iw_Parent.TriggerEvent(Resize!)
end on

