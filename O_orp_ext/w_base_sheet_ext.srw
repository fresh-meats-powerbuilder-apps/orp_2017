HA$PBExportHeader$w_base_sheet_ext.srw
$PBExportComments$Extension layer Sheet  - inherited from w_netwise_sheet.
forward
global type w_base_sheet_ext from w_netwise_sheet
end type
end forward

global type w_base_sheet_ext from w_netwise_sheet
long BackColor=12632256
event ue_getcustomertype pbm_custom64
event ue_getcustomerid pbm_custom63
event ue_systemcommand pbm_syscommand
event ue_open_order_confirmation ( )
event ue_getdata ( string as_stringvalue )
event ue_pa_summary ( )
end type
global w_base_sheet_ext w_base_sheet_ext

type variables
u_orp003 iu_orp003

String	is_MicroHelp


end variables

on ue_getcustomertype;call w_netwise_sheet::ue_getcustomertype;Message.StringParm = ""
end on

on ue_getcustomerid;call w_netwise_sheet::ue_getcustomerid;Message.StringParm = ''
end on

on ue_systemcommand;call w_netwise_sheet::ue_systemcommand;//// 0xF040
Window	lw_Window

IF Message.WordParm = 61504 OR Message.WordParm = 61520 Then 
	lw_Window = iw_Frame.GetFirstSheet()
	lw_Window = iw_Frame.GetNextSheet( lw_Window)
	IF IsValid( lw_Window) Then Open(w_ctrl_tab_window_switching)
	Message.processed = true
	Message.returnvalue = 0
END IF



end on

event ue_getdata;call super::ue_getdata;Choose Case as_stringValue
	Case "Title"
		Message.StringParm = This.Title
	Case "Customer_Info"
		Message.StringParm = is_customer_info
	Case "iu_orp003"
		Message.PowerObjectParm = iu_orp003
END CHoose
end event

on w_base_sheet_ext.create
call w_netwise_sheet::create
end on

on w_base_sheet_ext.destroy
call w_netwise_sheet::destroy
end on

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return
end event

