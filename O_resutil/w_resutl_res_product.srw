HA$PBExportHeader$w_resutl_res_product.srw
forward
global type w_resutl_res_product from w_netwise_sheet
end type
type dw_header from u_netwise_dw within w_resutl_res_product
end type
type ole_spreadsheet from olecustomcontrol within w_resutl_res_product
end type
end forward

global type w_resutl_res_product from w_netwise_sheet
integer width = 2894
integer height = 1544
string title = "Reservation Utilization By Reservation/Product"
long backcolor = 12632256
event ue_getretrievaldata pbm_custom64
dw_header dw_header
ole_spreadsheet ole_spreadsheet
end type
global w_resutl_res_product w_resutl_res_product

type variables
u_orp001		iu_orp001

u_ws_orp3		iu_ws_orp3

String		is_OpenParm

end variables
forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_print ()
end prototypes

event ue_getretrievaldata;call super::ue_getretrievaldata;Message.StringParm = ''
Message.StringParm = dw_header.Describe("DataWindow.Data")

end event

public function boolean wf_retrieve ();Int						li_ret

long						ll_Tab_pos, ll_right_tab_pos

String				 	ls_modify, &
							ls_customer, &
							ls_tsr, &
							ls_name, &
							ls_city, &
							ls_Open_value, ls_right_value
	
u_string_functions		lu_string_functions	

If lu_string_functions.nf_isempty( is_OpenParm) Then
	ls_modify = "Reservation/Product"
	OpenWithParm( w_res_util_inq_resp,This)
	If Message.StringParm = 'Abort'Then 
		Return False
	ELSE
		ls_modify = Message.StringParm
		//
		ls_Open_value = ls_modify
		ll_Tab_pos = lu_string_functions.nf_Npos( ls_Open_Value, "~t", 1, 5)
		ll_right_tab_pos = lu_string_functions.nf_npos(ls_open_value, "~t", ll_tab_pos, 2)
		ls_right_value = Right(ls_open_value, Len(ls_open_value) - ll_right_tab_pos )
		ls_modify = Left( ls_Open_Value, ll_Tab_pos)
		ls_modify += "N~t" + ls_right_value
		//
	End IF
Else
	ls_modify = is_OpenParm
End IF
is_OpenParm = ""
This.SetRedraw(False)
dw_header.Reset()
li_ret = dw_header.ImportString(ls_modify)
ls_tsr = dw_header.GetItemString(1, "tsr")

IF Not lu_string_functions.nf_isempty(ls_tsr) Then
	Select	salesman.smanname
	Into		:ls_name
	From		salesman	
	Where		salesman.smancode = :ls_tsr AND salesman.smantype = 's'
	Using		SQLCA;
End If
dw_header.object.tsr_name.text = ls_name
ls_customer = dw_header.GetItemString(1, "customer_id")

IF lu_string_functions.nf_isempty(ls_customer) Then
	dw_header.SetItem( 1, "customer_id", "ALL")
	Choose Case dw_header.GetItemString(1, 'customer_type')
		Case 'C'
			dw_header.object.corp_name.text = ''
			dw_header.object.corp_city.text = ''
		Case 'B'
			dw_header.object.billto_name.text = ''
			dw_header.object.billto_city.text = ''
		Case 'S'
			dw_header.object.shipto_id_name.text = ''
			dw_header.object.customer_id_city.text = ''
	End Choose
Else
	Choose Case dw_header.GetItemString(1, 'customer_type')
		Case 'C'
			Select	corpcust.name,
						corpcust.city
			Into		:ls_name,
						:ls_city
			From		corpcust
			Where		corpcust.corp_id = :ls_customer
			Using		SQLCA;
			dw_header.object.corp_name.text = ls_name
			dw_header.object.corp_city.text= ls_city
		Case 'B'
			Select	billtocust.name,
						billtocust.city
			Into		:ls_name,
						:ls_city
			From		billtocust
			Where		billtocust.bill_to_id = :ls_customer
			Using		SQLCA;
			dw_header.object.billto_name.text = ls_name
			dw_header.object.billto_city.text= ls_city
		Case 'S'
			Select	customers.short_name,
						customers.city
			Into		:ls_name,
						:ls_city
			From		customers
			Where		customers.customer_id = :ls_customer
			Using		SQLCA;
			dw_header.object.shipto_id_name.text = ls_name
			dw_header.object.customer_id_city.text = ls_city
	End Choose
End IF
This.SetRedraw(True)
Return This.PostEvent("Ue_Query", 0, ls_modify)
end function

public subroutine wf_print ();ole_spreadsheet.object.FilePrint(True)
Return 
end subroutine

event activate;call super::activate;iw_frame.im_menu.mf_enable('m_Print')
iw_frame.im_menu.mf_Disable('m_Save')
iw_frame.im_menu.mf_Disable('m_generatesales')
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Disable('m_Print')
iw_frame.im_menu.mf_enable('m_Save')
iw_frame.im_menu.mf_Enable('m_generatesales')
end event

event ue_query;call super::ue_query;Boolean					lb_first_time

Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
	
Int						li_rtn
	
Long						ll_first_row,&
							ll_row,&
							ll_col

String					ls_Input_String, &
							ls_out_data,&
							ls_customer_id, &
							ls_name, &
							ls_city

u_string_functions		lu_string_functions	
s_Error					lstr_Error_info

ls_Input_String = String(Message.LongParm, "address")
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ll_first_row = 1
lb_first_time = True
This.SetRedraw( False)
ole_spreadsheet.object.EnableProtection(False)
ole_spreadsheet.object.ClearRange(1, 1, ole_spreadsheet.object.MaxRow, &
		ole_spreadsheet.object.MaxCol, 3)
//Do
//	li_rtn = iu_orp001.nf_orpo60ar_resutil_resproduct( &
//				lstr_error_info, &
//				ls_Input_String, &
//				ls_customer_id, &
//				ls_Out_Data, &
//				ld_task_number, &
//				ld_last_record_number, &
//				ld_max_record_number)
				
	li_rtn = iu_ws_orp3.uf_orpo60fr_resutil_resproduct( &
				lstr_error_info, &
				ls_Input_String, &
				ls_customer_id, &
				ls_Out_Data)
	If lb_first_time Then
		IF Not lu_string_functions.nf_AmIEmpty(ls_customer_id) Then
			dw_header.SetItem(1, "customer_id", ls_customer_id)
			Choose Case dw_header.GetItemString(1, 'customer_type')
				Case 'C'
					Select	corpcust.name,
								corpcust.city
					Into		:ls_name,
								:ls_city
					From		corpcust
					Where		corpcust.corp_id = :ls_customer_id
					Using		SQLCA;
					dw_header.object.corp_name.text = ls_name
					dw_header.object.corp_city.text= ls_city
				Case 'B'
					Select	billtocust.name,
								billtocust.city
					Into		:ls_name,
								:ls_city
					From		billtocust
					Where		billtocust.bill_to_id = :ls_customer_id
					Using		SQLCA;
					dw_header.SetItem(1, 'billto_name', ls_name)
					dw_header.SetItem(1, 'billto_city', ls_city)
				Case 'S'
					Select	customers.short_name,
								customers.city
					Into		:ls_name,
								:ls_city
					From		customers
					Where		customers.customer_id = :ls_customer_id
					Using		SQLCA;
					dw_header.object.shipto_id_name.text = ls_name
					dw_header.object.customer_id_city.text = ls_city
			End Choose
		End IF
		If lu_string_functions.nf_isempty(ls_out_data) Then
			ole_spreadsheet.Visible = False
			This.SetRedraw(True)
			Return
		End IF
		lb_first_time = False
	End IF
	ole_spreadsheet.object.SetTabbedText(ll_first_row, 1, Ref ll_row, Ref ll_col, False, ls_out_data)
	ll_first_row = ll_first_row + ll_row
//Loop While ld_last_record_number <> ld_max_record_number
//Set up the Data
ole_spreadsheet.object.SetTabbedText(2, 1, Ref ll_row, Ref ll_col, False, ls_out_data)
ole_spreadsheet.object.MaxRow = ll_first_row
ole_spreadsheet.object.MaxCol = 7
ole_spreadsheet.object.BackColor = 12632256
//Set up the Column Headings
ole_spreadsheet.object.SetColWidth(1, 2, 3500, False)
ole_spreadsheet.object.SetColWidth(3, 7, 3000, False)
ole_spreadsheet.object.TextRC(1, 1, 'Product')
ole_spreadsheet.object.TextRC(1, 2, 'Product Desc.')
ole_spreadsheet.object.TextRC(1, 3, 'Reserved')
ole_spreadsheet.object.TextRC(1, 4, 'Generated')
ole_spreadsheet.object.TextRC(1, 5, 'Transfered')
ole_spreadsheet.object.TextRC(1, 6, 'Released')
ole_spreadsheet.object.TextRC(1, 7, 'Utilized')
ole_spreadsheet.object.TextRC(2, 1, 'Total')
ole_spreadsheet.object.FixedRow = 1
ole_spreadsheet.object.FixedRows = 1
ole_spreadsheet.object.SetSelection(1, 1, 1, 7)
ole_spreadsheet.object.SetAlignment(3, False, 3, 0)   
//Do Some Formating
ole_spreadsheet.Visible = True
ole_spreadsheet.object.SetSelection(1, 1, ll_first_row, 7)
ole_spreadsheet.object.SetBorder(1,1,1,1,1,0,0,0,0,0,0)
ole_spreadsheet.object.EnableProtection = True
ole_spreadsheet.object.SetSelection(2,1,2,1)
This.SetRedraw( True)
end event

event ue_postopen;call super::ue_postopen;wf_retrieve()

ole_spreadsheet.SetFocus()

end event

event open;call super::open;is_OpenParm = Message.StringParm
IF Not IsValid( iu_orp001) Then iu_orp001 = Create u_orp001
IF Not IsValid( iu_ws_orp3) Then iu_ws_orp3 = Create u_ws_orp3


end event

event close;call super::close;Destroy( iu_orp001)
Destroy( iu_ws_orp3)
end event

on w_resutl_res_product.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.ole_spreadsheet=create ole_spreadsheet
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.ole_spreadsheet
end on

on w_resutl_res_product.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.ole_spreadsheet)
end on

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

end event

event resize;call super::resize;ole_spreadsheet.Resize(this.width - ole_spreadsheet.x - il_BorderPaddingWidth, &
								this.height - ole_spreadsheet.y - il_BorderPaddingHeight)
end event

type dw_header from u_netwise_dw within w_resutl_res_product
integer x = 32
integer width = 2597
integer height = 280
string dataobject = "d_resutil_tsr_cust"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)

This.object.reservation_number_t.visible = 1
This.object.resevation_number.visible = 1


end event

type ole_spreadsheet from olecustomcontrol within w_resutl_res_product
event click ( long nrow,  long ncol )
event dblclick ( long nrow,  long ncol )
event canceledit ( )
event selchange ( )
event startedit ( ref string editstring,  ref integer cancel )
event endedit ( ref string editstring,  ref integer cancel )
event startrecalc ( )
event endrecalc ( )
event topleftchanged ( )
event objclick ( ref string objname,  long objid )
event objdblclick ( ref string objname,  long objid )
event rclick ( long nrow,  long ncol )
event rdblclick ( long nrow,  long ncol )
event objvaluechanged ( ref string objname,  long objid )
event modified ( )
event mousedown ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mouseup ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mousemove ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event objgotfocus ( ref string objname,  long objid )
event objlostfocus ( ref string objname,  long objid )
event validationfailed ( ref string pentry,  long nsheet,  long nrow,  long ncol,  ref string pshowmessage,  ref integer paction )
event keypress ( ref integer keyascii )
event keydown ( ref integer keycode,  integer shift )
event keyup ( ref integer keycode,  integer shift )
boolean visible = false
integer y = 288
integer width = 2843
integer height = 1128
integer taborder = 2
boolean bringtotop = true
long backcolor = 16777215
string binarykey = "w_resutl_res_product.win"
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
end type

event constructor;ole_spreadsheet.object.SheetName(1, 'Product Utilization')
This.object.AllowDesigner = False
This.object.ShowColHeading = False
This.object.ShowRowHeading = False
This.object.ShowVScrollBar = 1
This.object.SetProtection(True, False)
This.object.EnableProtection = True


end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
05w_resutl_res_product.bin 
2600001600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000007fffffffe00000004000000050000000600000008fffffffe00000009fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000085f1e5001d23c740000000300000c000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000540000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000020000063100000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004042badc511ce5e58415210b60100485300000000085f1e5001d23c74085f1e5001d23c7400000000000000000000000000000001fffffffe000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f000000100000001100000012000000130000001400000015000000160000001700000018000000190000001afffffffe0000001c0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002ffffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
27ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f004300790070006900720068006700200074006300280020002900390031003500390056002000730069006100750020006c006f00430070006d006e006f006e0065007300740020002c006e0049002e006300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fffe00020206042badc511ce5e58415210b60100485300000001fb8f0821101b01640008ed8413c72e2b000000300000060100000008000001000000004800000101000000500000010200000058000001030000006000000104000000680000010500000070000001060000007c000000000000057800000003000100000000000300003cab0000000300001b630000000300000060000000020000000100000008000000010000000000000041000004f300090000ee000505000000040014ef000000000000000000ffffff00ffffffff3dfff3ff00000012000000000000380001000000310258000000c814907fff00000000010500000061697241c814316cff0000000002bc7f0000000072410500316c61690200c814907fff00000000010500000061697241c814316cff0002000002bc7f0000000072410500316c61690000c814907fff00000000010500000061697241051a1e6c24221700232c2322295f302322285c3b2c2322245c302323061f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c201e295c221d00072c2322242e302323295f303022285c3b2c2322242e302323295c30300008251e2224222223232c2330302e305b3b295f5d6465522422285c232c2322302e30231e295c3032002a352422285f23202a223023232c5f3b295f22242228285c202a23232c233b295c302422285f22202a22295f222d40285f3b2c1e295f5f29002923202a283023232c5f3b295f5c202a28232c2328295c30232a285f3b222d22205f3b295f295f4028002c3d1e22285f3a202a222423232c2330302e305f3b295f22242228285c202a23232c2330302e305f3b295c222422282d22202a5f3f3f22285f3b291e295f4031002b34202a285f23232c2330302e305f3b295f5c202a28232c2328302e30233b295c30202a285f3f222d223b295f3f5f40285f0005ed2900000000000003ec0014e000f5000000c00020ff0000002000000000e00000000000011420fff5000020c0c400000000000000000114e000f5000000c0c420ff0000002000000000e00000000000021420fff5000020c0c400000000000000000214e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e000000000000014200001000020c00000000000000000000514e000f5000800c0c820ff0000002000000000e00000000600051420fff5000020c0c800000000000000000514e000f5000c00c0c820ff0000002000000000e00000000a00051420fff5000020c0c800000000000000000514e000f5000d00c0c820ff0000002000000000e00000000000041412fff0000020c04800000000000000000009850068530600317465650500090a010d001000640c0010000011d2f1a9fc3f50624d2a00015f012b000000012500018c00ff81000100031404c11541260261500708262065670000835026000084000000003fe8000000000027e80000000000283f0000000000293ff000000000a13ff0006400012201000100060001000000000000000000e00000000000003fe00000000000013f000000000000000018f70000ccff009200ffffff00c0c0c03fff00ff0000000000000000000010f200000000000000000000000015f6ffff150015004000ff00030f1d060000000000010000000000000a3e0000000002360000000064a0000099006400000a092600000008000000000000000100010600000009006c6966006d616e6501030065000c0000735f00006b636f74706f727001040073000c00006f620000726564726c7974730101006500090000655f00006e65747802007874090000015f000000657478650079746e00000105000000086e70706100656d6100000100000000097265765f6e6f6973000000000000000000000000000000000001000000003cab00001b630000006000010001010101010101010101010101010101010004f3000900000000050500000004ee14ef00000000000000000000ffff0000fffffffffff3ffff0000123d0000000000380000000000000258000100c814317fff000000000190000000006972410514316c61000000c802bc7fff00000000410500006c61697200c814317fff000200000190000000006972410514316c61000200c802bc7fff00000000410500006c61697200c814317fff00000000019000000000697241051a1e6c6122170005
202c2322245f302323285c3b29232224223023232c1f1e295c221c00062c2322245f302323525b3b295c5d64652224222823232c231e295c301d000720232224223023232c5f30302e285c3b29232224223023232c5c30302e08251e2924222200232c2322302e30233b295f30006f00430074006e006e00650073007400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000001b00000520000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000006465525b22285c5d2c2322242e302323295c3030002a351e22285f32202a222423232c233b295f302422285f5c202a22232c2328295c302322285f3b202a22245f222d22285f3b291e295f402900292c202a285f23232c233b295f30202a285f2c23285c5c302323285f3b292d22202a3b295f225f40285f2c3d1e29285f3a002a222422232c2320302e30233b295f302422285f5c202a22232c2328302e30233b295c302422285f22202a223f3f222d5f3b295f295f4028002b341e2a285f31232c2320302e30233b295f30202a285f2c23285c2e302323295c30302a285f3b222d2220295f3f3f40285f3b05ed295f000000000003ec0014e00000000000000020fff5000020c00000000000000000000114e0fff5000020c0c420000000000000000014e0000000000001c420fff5000020c00000000000000000000214e0fff5000020c0c420000000000000000014e0000000000002c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e00001000020c00020000000000000000014e0000000080005c820fff5000020c00000000000000000000514e0fff5000620c0c820000000000000000014e00000000c0005c820fff5000020c00000000000000000000514e0fff5000a20c0c820000000000000000014e00000000d0005c820fff5000020c00000000000000000000414e0fff0000020c04812000000000000000009850000530600007465656800090a310d001005640c000100001100f1a9fc1050624dd200015f3f2b00002a012500018c00ff00000100011404c181412602035007081520656761008350260000840000000026e80000000000273f0000000000283fe800000000293ff000000000003ff00000000122a1000100640001000100000006000000000000000000003fe00000000000013fe00000000000000000f7000000ff009218ffffffccc0c0c000ff00ff000000003f000000000010f200000000000000000000000000f6ffff000015001500ff00150f1d06400000000301000000000000003e0000000002360a00000000a0000000006400640a09269901ff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
15w_resutl_res_product.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
