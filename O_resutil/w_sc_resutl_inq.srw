HA$PBExportHeader$w_sc_resutl_inq.srw
forward
global type w_sc_resutl_inq from w_netwise_response
end type
type dw_sc_resutl_inq from u_netwise_dw within w_sc_resutl_inq
end type
end forward

global type w_sc_resutl_inq from w_netwise_response
int X=73
int Y=100
int Height=1028
long BackColor=12632256
boolean ControlMenu=false
dw_sc_resutl_inq dw_sc_resutl_inq
end type
global w_sc_resutl_inq w_sc_resutl_inq

type variables
Window		iw_calling_parent
end variables

forward prototypes
public function string wf_get_to_date (string as_week, integer ai_month, long al_year)
end prototypes

public function string wf_get_to_date (string as_week, integer ai_month, long al_year);Boolean		lb_first_time

Integer		li_month, &
				li_week, &
				li_last_week
				
Long			ll_num_of_weeks

String		ls_rtn_string

lb_first_time = True
li_month = ai_month
If as_week = 'A' Then
	li_week = 0
Else 
	li_week = Integer(as_week)
End If

Do While ll_num_of_weeks <= 14
	If lb_first_time then
		li_last_week = iw_frame.iu_project_functions.nf_gpo_week(li_month, al_year)
		ll_Num_of_weeks += li_last_week - li_week
		lb_first_time = False
	Else
		li_last_week = iw_frame.iu_project_functions.nf_gpo_week(li_month, al_year)
		ll_Num_of_weeks += li_last_week
	End If
	If ll_num_of_weeks <= 14 Then
		If li_month = 12 Then 
			li_month = 1
			al_year ++
		Else
			li_month ++
		End If
	End If
Loop 

ls_rtn_string = String(al_year) + &
					String(li_month, "00") +&
					String(li_last_week - (ll_num_of_weeks - 14))
Return ls_rtn_string
end function

event ue_postopen;Boolean						lb_is_salesmgr

String						ls_modify_string,&
								ls_window_name, &
								ls_salesman_code, &
								ls_salesman

u_string_functions		lu_string_functions
u_project_functions		lu_project_functions

ls_window_name = iw_Calling_Parent.ClassName()
lb_is_salesmgr = False
Choose Case ls_window_name
Case	"w_sc_resutl_tsrres"
	if  iw_frame.iu_project_functions.nf_issalesmgr() or iw_frame.iu_project_functions.nf_isschedulermgr() then
		lb_is_salesmgr = true
	else
		lb_is_salesmgr = false
	end if
//	lb_is_salesmgr = iw_frame.iu_project_functions.nf_issalesmgr()
	If lb_is_salesmgr Then
		dw_sc_resutl_inq.Modify('all_service_centers.Protect = 0 ' + &
										'all_tsrs.Protect = 0 ' + &
										'service_center.Protect = 0 ' + &
										'tsr_code.Protect = 0 ') 
	Else
		dw_sc_resutl_inq.Modify('all_tsrs.Protect = 0 ' + &
										'service_center.Background.Color="12632256" ' + & 
										'tsr_code.Protect = 0 ') 
	End If
Case	"w_sc_resutl_scres"
	if lu_project_functions.nf_issalesmgr() or iw_frame.iu_project_functions.nf_isschedulermgr() then
		dw_sc_resutl_inq.Modify('all_service_centers.Protect = 0 ' + &
									'service_center.Protect = 0 ')
	else
		dw_sc_resutl_inq.Modify('all_service_centers.Protect = 1 ' + &
									'service_center.Protect = 1 ')
	end if
Case	"w_sc_resutl_weekres"
	dw_sc_resutl_inq.Modify('all_min_boxes.Protect = 0 ' + &
									'min_boxes.Protect = 0 ' + &
									'sort_ind.Protect = 0 ' + &
									'to_week.Protect = 0 ' + &
									'to_month.Protect = 0 ' + &
									'to_year.Protect = 0 ')
END Choose
//IF lb_is_salesmgr or ls_window_name = "w_sc_resutl_weekres" Then
IF lb_is_salesmgr Then
	dw_sc_resutl_inq.SetItem( 1, "all_service_centers", "Y")
Else
	dw_sc_resutl_inq.SetItem( 1, "all_service_centers", "N")
	dw_sc_resutl_inq.SetItem( 1, "service_center", message.is_smanlocation)
End If
If ls_window_name = "w_sc_resutl_tsrres" Then
	dw_sc_resutl_inq.SetItem( 1, "all_tsrs", "N")
Else
	dw_sc_resutl_inq.SetItem( 1, "all_tsrs", "Y")
End IF
If ls_window_name = "w_sc_resutl_weekres" Then
	dw_sc_resutl_inq.SetItem( 1, "all_min_boxes", "N")
Else
	dw_sc_resutl_inq.SetItem( 1, "all_min_boxes", "Y")
End IF
dw_sc_resutl_inq.SetItem( 1, "all_products", "N")
Message.StringParm = ''
dw_sc_resutl_inq.SetRedraw(False)
iw_Calling_Parent.TriggerEvent("ue_GetRetrievalData")
ls_modify_String = Message.StringParm
IF Not lu_string_functions.nf_AmIEmpty( ls_modify_String) Then 
	dw_sc_resutl_inq.Reset()
	dw_sc_resutl_inq.ImportString(ls_modify_string)
	ls_salesman_code = dw_sc_resutl_inq.GetItemString(1, 'tsr_code')
Else
	dw_sc_resutl_inq.SetItem( 1, "from_month", Month(Today()))
	dw_sc_resutl_inq.SetItem( 1, "from_year", Year(Today()))
	dw_sc_resutl_inq.SetItem( 1, "from_week", 'A')
	dw_sc_resutl_inq.SetItem( 1, "percent_util",100)
	dw_sc_resutl_inq.SetItem(1, 'division_code', message.is_smandivision)
	IF ls_window_name = "w_sc_resutl_weekres" Then
		dw_sc_resutl_inq.SetItem( 1, "to_month", Month(Today()))
		dw_sc_resutl_inq.SetItem( 1, "to_year", Year(Today()))
		dw_sc_resutl_inq.SetItem( 1, "to_week", 'A')
		dw_sc_resutl_inq.SetItem( 1, "sort_ind", 'S')
	End IF
	message.stringparm = ""
	message.TriggerEvent("ue_getsmancode")
	ls_salesman_code = message.stringparm
	dw_sc_resutl_inq.SetItem( 1, 'tsr_code', ls_salesman_code)
END IF
If Not lu_string_functions.nf_IsEmpty(ls_salesman_code) Then
	SELECT 	salesman.smanname
	INTO 		:ls_salesman
	FROM 		salesman  
	WHERE 	salesman.smancode = :ls_salesman_code AND salesman.smantype = 's';
	dw_sc_resutl_inq.object.tsr_name.text =  ls_salesman
End If
ls_modify_string = ''
IF ls_window_name <> 'w_sc_resutl_weekres' Then
	IF dw_sc_resutl_inq.GetItemString(1, "all_service_centers") = "Y" Then 
		dw_sc_resutl_inq.SetItem(1, "service_center", "")
		ls_modify_string += "service_center.Protect='1' " + &
								"service_center.Background.Color='12632256' " 
	End IF
End IF
IF ls_window_name = 'w_sc_resutl_weekres' Then
	IF dw_sc_resutl_inq.GetItemString(1, "all_min_boxes") = "Y" Then 
		dw_sc_resutl_inq.SetItem(1, "min_boxes", "")
		ls_modify_string += "min_boxes.Protect='1' " + &
								"min_boxes.Background.Color='12632256' " 
	End IF
End IF
IF ls_window_name = 'w_sc_resutl_tsrres' Then
	IF dw_sc_resutl_inq.GetItemString(1, "all_tsrs") = "Y" Then 
		dw_sc_resutl_inq.SetItem(1, "tsr_code", "")
		dw_sc_resutl_inq.object.tsr_name.text = ''
		ls_modify_string += "tsr_code.Protect='1' " + &
								"tsr_code.Background.Color='12632256' " 
	End IF
End IF
IF dw_sc_resutl_inq.GetItemString(1, "all_products") = "Y" Then 
	 dw_sc_resutl_inq.SetItem(1, "product_1", "")
	 dw_sc_resutl_inq.SetItem(1, "product_2", "")
	 dw_sc_resutl_inq.SetItem(1, "product_3", "")
	 dw_sc_resutl_inq.SetItem(1, "product_4", "")
	 dw_sc_resutl_inq.SetItem(1, "product_5", "")
	 dw_sc_resutl_inq.SetItem(1, "product_6", "")
	 dw_sc_resutl_inq.SetItem(1, "product_7", "")
	 dw_sc_resutl_inq.SetItem(1, "product_8", "")
	ls_modify_string += "product_1.Protect='1' " + &
							"product_1.Background.Color='12632256' " + &
							"product_2.Protect='1' "  + &
							"product_2.Background.Color='12632256' " + &
							"product_3.Protect='1' " + &
							"product_3.Background.Color='12632256' " + &
							"product_4.Protect='1' " + &
							"product_4.Background.Color='12632256' " + &
							"product_5.Protect='1' " + &
							"product_5.Background.Color='12632256' " + &
							"product_6.Protect='1' " + &
							"product_6.Background.Color='12632256' " + &
							"product_7.Protect='1' " + &
							"product_7.Background.Color='12632256' " + &
							"product_8.Protect='1' " + &
							"product_8.Background.Color='12632256' "
Else
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_1")) Then
		ls_modify_string += "product_2.Protect='0' "  + &
									"product_2.Background.Color='16777215' "
	End If
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_2")) Then
		ls_modify_string += "product_3.Protect='0' "  + &
									"product_3.Background.Color='16777215' "
	End If
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_3")) Then
		ls_modify_string += "product_4.Protect='0' "  + &
									"product_4.Background.Color='16777215' "
	End If
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_4")) Then
		ls_modify_string += "product_5.Protect='0' "  + &
									"product_5.Background.Color='16777215' "
	End If
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_5")) Then
		ls_modify_string += "product_6.Protect='0' "  + &
									"product_6.Background.Color='16777215' "
	End If
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_6")) Then
		ls_modify_string += "product_7.Protect='0' "  + &
									"product_7.Background.Color='16777215' "
	End If
	If Not lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString(1, "product_7")) Then
		ls_modify_string += "product_8.Protect='0' "  + &
									"product_8.Background.Color='16777215' "
	End If
End If
dw_sc_resutl_inq.Modify( ls_modify_string)	
dw_sc_resutl_inq.SetColumn('division_code')
dw_sc_resutl_inq.SetFocus()
dw_sc_resutl_inq.SetRedraw(True)
end event

event open;call super::open;iw_Calling_Parent = Message.PowerObjectParm

This.Title = iw_Calling_Parent.Title + " Inquire"

cb_base_ok.BringToTop = True
cb_base_cancel.BringToTop = True

end event

on w_sc_resutl_inq.create
int iCurrent
call super::create
this.dw_sc_resutl_inq=create dw_sc_resutl_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sc_resutl_inq
end on

on w_sc_resutl_inq.destroy
call super::destroy
destroy(this.dw_sc_resutl_inq)
end on

event ue_base_ok;call super::ue_base_ok;Integer						li_from_month

Long							ll_from_year

String						ls_from_week, &
								ls_to_date, &
								ls_date, &
								ls_window_name, &
								ls_to_week
								
u_string_functions		lu_string_functions	

If dw_sc_resutl_inq.AcceptText() < 0 Then Return
ls_window_name = iw_calling_parent.ClassName()
If ls_window_name = "w_sc_resutl_weekres" Then
	ls_from_week = dw_sc_resutl_inq.GetItemString(1, 'from_week')
	li_from_month = dw_sc_resutl_inq.GetItemNumber(1, 'from_month')
	ll_from_year = dw_sc_resutl_inq.GetItemNumber(1, 'from_year')
	ls_to_week = String(dw_sc_resutl_inq.GetItemString(1, 'to_week'))
	IF ls_to_week = 'A' then
		ls_to_week = '5' 
	End If
	ls_to_date = String(dw_sc_resutl_inq.GetItemNumber(1, 'to_year')) + &
					String(dw_sc_resutl_inq.GetItemNumber(1, 'to_month'),"00") + &
					ls_to_week
	ls_date = wf_get_to_date(ls_from_week, li_from_month, ll_from_year)
	If ls_to_date > ls_date Then
		MessageBox(	"Date Range", "You defined a Date Range that is more  " + &
			"than 14 weeks.  Please enter a different Range.")
		Return
	END IF
End If
IF dw_sc_resutl_inq.GetItemString( 1, "All_service_centers") = 'N' AND &
	lu_string_functions.nf_isempty( dw_sc_resutl_inq.GetItemString( 1, "service_center")) Then
	MessageBox(	"Input Service Center", "You are required " + &
		"to specify a Service Center or check All Service Centers.")
	Return
END IF
IF dw_sc_resutl_inq.GetItemString( 1, "all_tsrs") = 'N' AND &
		lu_string_functions.nf_isempty(dw_sc_resutl_inq.GetItemString( 1, "tsr_code")) Then
	MessageBox("Input customer", "You are required to specify a " + &
			"TSR or check All TSRs.")
	Return
End if 
IF dw_sc_resutl_inq.GetItemString( 1, "all_min_boxes") = 'N' AND&
		IsNull( dw_sc_resutl_inq.GetItemNumber( 1, "min_boxes")) Then
	MessageBox(	"Input Reservation Number", "You are required to " + &
			"specify a Minimum Number of Boxes or check All Boxes.")
	Return
END IF
IF dw_sc_resutl_inq.GetItemString( 1, "all_Products") = 'N' AND&
		lu_string_functions.nf_isempty( dw_sc_resutl_inq.GetItemString( 1, "product_1")) Then
	MessageBox(	"Input Product Code", "You are required to specify " + &
			"a Product or check All Products")
	Return
END IF    
CloseWithReturn( This, dw_sc_resutl_inq.Describe("DataWindow.Data"))
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, "Abort")
end event

type cb_base_help from w_netwise_response`cb_base_help within w_sc_resutl_inq
int X=1349
int Y=808
int TabOrder=40
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_sc_resutl_inq
int X=1042
int Y=808
int TabOrder=30
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_sc_resutl_inq
int X=727
int Y=808
end type

type dw_sc_resutl_inq from u_netwise_dw within w_sc_resutl_inq
event ue_dwndropdown pbm_dwndropdown
int X=5
int Y=16
int Width=2487
int Height=764
int TabOrder=20
string DataObject="d_sc_res_util_inq"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event ue_dwndropdown;call super::ue_dwndropdown;DataWindowChild		ldwc_temp

Choose Case This.GetColumnName()
	Case 'division_code'
		dw_sc_resutl_inq.GetChild('division_code', ldwc_temp)

		If ldwc_temp.RowCount() < 1 Then &
				iw_frame.iu_project_functions.nf_getdivisions(ldwc_temp)
End choose
end event

event itemchanged;call super::itemchanged;DataWindowChild			ldwc_temp

Integer						li_month

Long							ll_year, &
								ll_percent_util

STRING						ls_GetText, &
								ls_Modify_Value, &
								ls_salesman
								
u_string_functions		lu_string_functions	

ls_GetText = data
Choose Case dwo.name
Case "from_year"
	ll_year = Long(ls_GetText)
	IF ll_year < 1900 or ll_year > 3000 Then
		MessageBox("From Year", "The Year must be a valid Year from 1900 to 3000.")
		Return 1
	Else 
		If ll_year > GetItemNumber(1, 'To_year') Then
			MessageBox("From Year", "From Year must be greater than To Year. ")
			Return 1
		End If	
	End IF
Case "from_month"
	li_month = Integer(ls_GetText)
	IF li_month < 1 or li_month > 12 Then
		MessageBox("From Month", "From Month must be a valid Month from 1 to 12.")
		Return 1
	Else
		IF GetItemNumber(1, 'to_year') = GetItemNumber(1, 'from_year') Then 
			IF li_month > GetItemNumber(1, 'to_month') Then
				MessageBox("From Month", "From Month must be less than To Month.")
				Return 1
			End IF
		End If
	End IF
Case "to_year"
	ll_year = Long(ls_GetText)
	IF ll_year < 1950 or ll_year > 2999 Then
		MessageBox("To Year", "The Year must be a valid Year from 1900 to 3000. ")
		Return 1
	Else 
		If ll_year < GetItemNumber(1, 'from_year') Then
			MessageBox("To Year", "To Year must be greater than From Year. ")
			Return 1
		End IF
	End IF
Case "to_month"
	li_month = Integer(ls_GetText)
	IF li_month < 1 or li_month > 12 Then
		MessageBox("To Month", "To Month must be a valid Month from 1 to 12.")
		Return 1
	Else
		IF GetItemNumber(1, 'to_year') = GetItemNumber(1, 'from_year') Then 
			IF li_month < GetItemNumber(1, 'from_month') Then
				MessageBox("To Month", "To Month must be greater than From Month.")
				Return 1
			End IF
		End If
	End IF
Case "percent_util"
	ll_percent_util = Long(ls_GetText)
	IF ll_percent_util < 0 or ll_percent_util > 100 Then
		MessageBox("Percent Utilized", "The Percent Utilized must be " + &
				"between 1 and 100.")
		Return 1
	End IF
Case "all_tsrs" 
	IF ls_GetText = 'Y' Then
		dw_sc_resutl_inq.SetItem( 1, "tsr_code", "")
		dw_sc_resutl_inq.object.tsr_name.text =  ""
		dw_sc_resutl_inq.object.tsr_code.Protect = 1 
		dw_sc_resutl_inq.object.tsr_code.BackGround.Color = 12632256
	Else
		dw_sc_resutl_inq.SetItem( 1, "tsr_code", "")
		dw_sc_resutl_inq.object.tsr_name.text =  ""
		dw_sc_resutl_inq.object.tsr_code.Protect = 0 
		dw_sc_resutl_inq.object.tsr_code.BackGround.Color = 16777215
	END IF
Case "all_service_centers" 
	IF ls_GetText = 'Y' Then
		dw_sc_resutl_inq.SetItem( 1, "service_center", "")
		dw_sc_resutl_inq.object.service_center.Protect = 1
		dw_sc_resutl_inq.object.service_center.BackGround.Color = 12632256
	Else
		dw_sc_resutl_inq.SetItem( 1, "service_center", "")
		dw_sc_resutl_inq.object.service_center.Protect = 0 
		dw_sc_resutl_inq.object.service_center.BackGround.Color = 16777215
	END IF
Case "all_min_boxes" 
	IF ls_GetText = 'Y' Then
		dw_sc_resutl_inq.SetItem( 1, "min_boxes", 0)
		dw_sc_resutl_inq.object.min_boxes.Protect = 1
		dw_sc_resutl_inq.object.min_boxes.BackGround.Color = 12632256
	Else
		dw_sc_resutl_inq.SetItem( 1, "min_boxes", "")
		dw_sc_resutl_inq.object.min_boxes.Protect = 0
		dw_sc_resutl_inq.object.min_boxes.BackGround.Color = 16777215
	END IF
Case  "all_products" 
	IF ls_GetText = 'Y' Then
		dw_sc_resutl_inq.SetItem( 1, "product_1", "")
		dw_sc_resutl_inq.SetItem( 1, "product_2", "")
		dw_sc_resutl_inq.SetItem( 1, "product_3", "")
		dw_sc_resutl_inq.SetItem( 1, "product_4", "")
		dw_sc_resutl_inq.SetItem( 1, "product_5", "")
		dw_sc_resutl_inq.SetItem( 1, "product_6", "")
		dw_sc_resutl_inq.SetItem( 1, "product_7", "")
		dw_sc_resutl_inq.SetItem( 1, "product_8", "")
		ls_modify_Value = "product_1.Protect = '1' " + &
								"product_1.BackGround.Color = 12632256 " + &
								"product_2.Protect = '1' " + &
								"product_2.BackGround.Color = 12632256 " + &
								"product_3.Protect = '1' " + &
								"product_3.BackGround.Color = 12632256 " + &
								"product_4.Protect = '1' " + &
								"product_4.BackGround.Color = 12632256 " + &
								"product_5.Protect = '1' " + &
								"product_5.BackGround.Color = 12632256 " + &
								"product_6.Protect = '1' " + &
								"product_6.BackGround.Color = 12632256 " + &
								"product_7.Protect = '1' " + &
								"product_7.BackGround.Color = 12632256 " + &
								"product_8.Protect = '1' " + &
								"product_8.BackGround.Color = 12632256 " 								
		dw_sc_resutl_inq.Modify( ls_Modify_Value)
	Else
		dw_sc_resutl_inq.SetItem( 1, "product_1", "")
		dw_sc_resutl_inq.object.product_1.Protect = 0
		dw_sc_resutl_inq.object.product_1.BackGround.Color = 16777215
	END IF
Case 'tsr_code'
	If lu_string_functions.nf_isempty(ls_GetText) Then 
		iw_frame.SetMicroHelp("TSR is a required field.")
		Return 1 
	End IF
	This.GetChild("tsr_code", ldwc_temp)
	If ldwc_temp.Find("smancode = '" + ls_GetText + "'", 1, &
			ldwc_temp.RowCount()) < 1 Then
		iw_frame.SetMicroHelp("Invalid TSR.")
		Return 1
	End IF	
	SELECT 	salesman.smanname
	INTO 		:ls_salesman
	FROM 		salesman  
	WHERE 	salesman.smancode = :ls_gettext AND salesman.smantype = 's';
	dw_sc_resutl_inq.object.tsr_name.text =  ls_salesman
Case 'service_center'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	This.GetChild("service_center", ldwc_temp)
	If ldwc_temp.Find("service_center_code = '" + ls_GetText + "'", 1, &
		ldwc_temp.RowCount()) < 1 Then
		iw_frame.SetMicroHelp("Invalid Service Center.")
		Return 1
	End IF
Case 'division_code'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	This.GetChild("division_code", ldwc_temp)
		If ldwc_temp.Find("type_code = '" + ls_GetText + "'", 1, &
		ldwc_temp.RowCount()) < 1 Then
		iw_frame.SetMicroHelp("Invalid Division.")
		This.SelectText(1, 2)
		Return 1
	Else
		iw_frame.SetMicroHelp("Ready.")
		This.SetColumn('division_code')
	End IF
Case 'product_1'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_2.Background.Color = 16777215 
	dw_sc_resutl_inq.object.product_2.Protect = 0 
Case 'product_2'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_3.Background.Color = 16777215
	dw_sc_resutl_inq.object.product_3.Protect = 0 
Case 'product_3'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_4.Background.Color = 16777215 
	dw_sc_resutl_inq.object.product_4.Protect = 0 
Case 'product_4'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_5.Background.Color = 16777215 
	dw_sc_resutl_inq.object.product_5.Protect = 0 
Case 'product_5'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_6.Background.Color = 16777215
	dw_sc_resutl_inq.object.product_6.Protect = 0
Case 'product_6'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_7.Background.Color = 16777215
	dw_sc_resutl_inq.object.product_7.Protect = 0
Case 'product_7'
	If lu_string_functions.nf_isempty(ls_GetText) Then Return 1 
	dw_sc_resutl_inq.object.product_8.Background.Color = 16777215 
	dw_sc_resutl_inq.object.product_8.Protect = 0 									
End Choose
end event

event constructor;call super::constructor;This.InsertRow(0)











end event

event itemerror;call super::itemerror;Return 1
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_temp

String						ls_location, &
								ls_window_name
								
u_string_functions		lu_string_functions	

ls_window_name = iw_calling_parent.ClassName()
Choose Case ls_window_name
Case	"w_sc_resutl_tsrres"
	dw_sc_resutl_inq.Modify('all_service_centers.visible = 1 ' + &
										'all_tsrs.visible = 1 ' + &
										'service_center.visible = 1 ' + &
										'service_center_t.visible = 1 ' + &
										'tsr_code.visible = 1 ' + &
										'tsr_name.visible = 1 ' + &
										'tsr_code_t.visible = 1 ')
	dw_sc_resutl_inq.GetChild( "tsr_code",ldwc_temp)
	ls_location = message.is_smanlocation
	IF Not iw_frame.iu_project_functions.nf_issalesmgr() Then
		If Not lu_string_functions.nf_isempty(ls_location) Then
			iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, ls_location)
		End If
	Else	
		iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, '')
	End IF										
Case	"w_sc_resutl_scres"
	dw_sc_resutl_inq.Modify('all_service_centers.visible = 1 ' + &
									'service_center.visible = 1 ' + &
									'service_center_t.visible = 1 ' )
Case	"w_sc_resutl_weekres"
	dw_sc_resutl_inq.Modify('all_min_boxes.visible = 1 ' + &
									'min_boxes.visible = 1 ' + &
									'min_boxes_t.visible = 1 ' + &
									'sort_ind.visible = 1 ' + &
									'sort_ind_t.visible = 1 ' + &
									'to_week.visible = 1 ' + &
									'to_week_t.visible = 1 ' + &
									'to_month.visible = 1 ' + &
									'to_month_t.visible = 1 ' + &
									'to_year.visible = 1 ' + &
									'to_year_t.visible = 1 ' + &
									'white_to_rec.visible = 1 ' + &
									'gray_to_rec.visible = 1 ' + &
									'to_date_t.visible = 1 ')
END Choose
// Go Get Service Center
If ls_window_name <> "w_sc_resutl_weekres" Then
	dw_sc_resutl_inq.GetChild('service_center', ldwc_temp)
	iw_frame.iu_project_functions.nf_GetServiceCenters(ldwc_temp, '')
End If
end event

