HA$PBExportHeader$nvuo_windows_installer.sru
$PBExportComments$Object used to install and register all external objects.
forward
global type nvuo_windows_installer from nonvisualobject
end type
end forward

global type nvuo_windows_installer from nonvisualobject
end type
global nvuo_windows_installer nvuo_windows_installer

type variables
public:
string		is_ApplicationPath, &
				is_ApplicationName, &
				is_ApplicationFileName, &
				is_server_path

end variables

forward prototypes
private function boolean nf_check_installedcomponents ()
private subroutine uf_install_com_objects ()
private subroutine uf_uninstall_com_objects ()
private subroutine uf_bat_install ()
private subroutine uf_manual_install ()
public subroutine uf_install ()
end prototypes

private function boolean nf_check_installedcomponents ();String			ls_RegistryString, &
					ls_valuearray[],&
					ls_value
Integer			li_rtn

//added if
ls_value = ProfileString('ibpuser.ini', "PAS",'BypassVBcoreRegCheck', "None") 
If ls_value = 'None'	Then
	SetProfileString ( 'ibpuser.ini', 'PAS', 'BypassVBcoreRegCheck', 'Y' )
End If

ls_value = ProfileString('ibpuser.ini', "PAS",'BypassVBcoreRegCheck', "None") 
IF ls_value = "N" Then
	// Check to see if the OCX is registered for Formula One
	ls_RegistryString = "HKEY_CURRENT_USER\Software\Microsoft\Installer\UpgradeCodes\36B559E05D104D11F9A3040000009282"
	li_rtn = RegistryValues(ls_RegistryString,ls_valuearray)
	IF li_rtn < 0 Then
		MessageBox('Severe Error','Contact Network Services, One or more of the following components may be missing.' + '~r~n' + &
		'~t' + '1. Windows Installer ' + '~r~n'+ &
		'~t' + '2. Mdac_type ' + '~r~n' + &
		'~t' + '3. VB Core Components. '+ '~r~n' + &
		"This will have adverse affects on the operation of some of the windows within this application. ",StopSign!)
		Return False
	END IF
End If

Return True
end function

private subroutine uf_install_com_objects ();/* This function was written to use the ibpocx.vbc file as a way to install VB OCX Controls.
                                                                       Author: David Deal  */     


String			ls_command,ls_temp	
Integer			li_rtn, &
					li_file_num,li_num_bytes, li_count
u_sdkcalls	U_api	
u_api = create u_sdkcalls

li_count = 0
ls_command = ProfileString (is_server_path +'IBPOCX.VBC', 'OPPA', 'Commandinstall','')

//li_file_num = FileOpen ( 'C:\Program Files\Common Files\IBP\install.bat',LineMode!  , Write! , LockReadWrite!, Replace! )
li_file_num = FileOpen ( 'C:\install.bat',LineMode!  , Write! , LockReadWrite!, Replace! )

If li_file_num = -1 Then
	//error occured
	MessageBox('Severe Error','Contact Applications install.bat file Open error.',StopSign!) 
Else
	li_num_bytes = FileWrite ( li_file_num, ls_command)
	If li_file_num = -1 or isnull(li_num_bytes) Then
		MessageBox('Severe Error','Contact Applications install.bat file Write error.',StopSign!) 
	Else
		FileClose ( li_file_num )
		Run ('"C:\install.bat"', Minimized! )
		Do 
			li_rtn = u_api.findwindowa('ConsoleWindowClass','c:\WINNT\System32\cmd.exe')
			li_count = li_count + 1
		LOOP UNTIL li_rtn <> 0 or li_count > 5000
		DO
			li_rtn = u_api.findwindowa('ConsoleWindowClass','c:\WINNT\System32\cmd.exe')
		LOOP UNTIL li_rtn = 0
	End If
End IF
end subroutine

private subroutine uf_uninstall_com_objects ();/* This function was written to use the ibpocx.vbc file as a way to uninstall VB OCX Controls.
                                                                       Author: David Deal  */     

String			ls_work, &
					ls_command, &
					ls_path, &
					ls_installed, &
					ls_registrystring, &
					ls_upgrade_code, &
					ls_package_code,ls_temp
					
Integer			li_rtn, &
					li_commands,li_file_num,li_num_bytes,li_count

Boolean			lb_oppa_here,lb_ok

Long				ll_upper
u_sdkcalls	U_api	
u_api = create u_sdkcalls

li_count = 0
IF Not gw_base_frame.ib_ran Then
	gw_base_frame.ib_ran = True
	ls_work = gw_base_frame.is_workingdir
	ls_Upgrade_Code = ProfileString (is_server_path +'IBPOCX.VBC', 'OPPA', 'IBP_OPPA_INSTALLER Upgrade Code',' ')
	If ls_Upgrade_Code = ' ' Then
		iw_frame.setmicrohelp("Contact Applications Missing IBPOCX.VBC file on Server.") 
//		MessageBox('Severe Error','Contact Applications Missing IBPOCX.VBC file on Server.',StopSign!) 
	Else
		ls_Installed = ""
		ls_RegistryString = "HKEY_CURRENT_USER\Software\Microsoft\Installer\UpgradeCodes\" + ls_Upgrade_Code
		String ls_valuearray[]
		li_rtn = RegistryValues(ls_RegistryString,ls_valuearray)
		IF li_rtn < 0 Then
			//Package Not Installed 
			gw_base_frame.ib_oppa_install = True //run reinstall later
		Else
			ls_package_code = ProfileString(is_server_path +'IBPOCX.VBC','OPPA','IBP_OPPA_INSTALLER PackageCode','')
			ll_upper = UpperBound (ls_valuearray)
			If ll_upper > 1 Then
//				li_file_num = FileOpen ( 'C:\Program Files\Common Files\IBP\Uninstall.bat',LineMode!  , Write! , LockReadWrite!, Replace! )
				li_file_num = FileOpen ( 'C:\Uninstall.bat',LineMode!  , Write! , LockReadWrite!, Replace! )
				If li_file_num = -1 Then
					//error occured
					MessageBox('Severe Error','Contact Applications Uninstall.bat file Open error.',StopSign!) 
				Else
					FOR li_commands = 1 TO ll_upper
						gw_base_frame.ib_oppa_install = True //run reinstall later
						lb_ok = True
						// The following two RegistryGet commands are to build the uninstall string
						ls_RegistryString = "HKEY_CURRENT_USER\Software\Microsoft\Installer\Products\" &
								+ ls_valuearray[li_commands] + '\SourceList\Net'
						li_rtn = RegistryGet(ls_RegistryString,"1", ls_path)
						IF li_rtn < 0 OR (len(ls_path) <= 0) Then
							//should never happen
							MessageBox('Severe Error','Contact Applications OPPA Uninstall.bat, error reading the registry',StopSign!) 
							lb_ok = false
							Exit
						Else
							ls_command = 'msiexec /x "' + ls_path
							ls_RegistryString = "HKEY_CURRENT_USER\Software\Microsoft\Installer\Products\"	&
								+ ls_valuearray[li_commands] + '\SourceList'
							li_rtn = RegistryGet(ls_RegistryString,"PackageName", ls_path)
							IF li_rtn < 0 OR (len(ls_path) <= 0) Then
								//should never happen
								MessageBox('Severe Error','Contact Applications OPPA Installer error reading the registry.',StopSign!) 
								lb_ok = false
								Exit
							Else
								// uninstall the product
								ls_command += ls_path + '" /q'
								li_num_bytes = FileWrite ( li_file_num, ls_command )
								If li_file_num = -1 or isnull(li_num_bytes) Then
									MessageBox('Severe Error','Contact Applications Uninstall.bat file Write error.',StopSign!) 
									lb_ok = false
									Exit
								End If
							End If
						End if	
					Next
					FileClose ( li_file_num )
					If lb_ok Then
						Run ('"C:\Uninstall.bat"', Minimized!)
						Do 
							li_rtn = u_api.findwindowa('ConsoleWindowClass','c:\WINNT\System32\cmd.exe')
						//	Yield ( )
							li_count = li_count + 1
						LOOP UNTIL li_rtn <> 0 or li_count > 5000
						DO
							li_rtn = u_api.findwindowa('ConsoleWindowClass','c:\WINNT\System32\cmd.exe')
							//Yield ( )
						LOOP UNTIL li_rtn = 0
					End If
				End if
			Else
				If ll_upper = 1 Then
					If ls_package_code = ls_valuearray[1] Then 
						gw_base_frame.ib_oppa_install = False
					else
						gw_base_frame.ib_oppa_install = True//run reinstall later
						// The following two RegistryGet commands are to build the uninstall string
						ls_RegistryString = "HKEY_CURRENT_USER\Software\Microsoft\Installer\Products\" &
								+ ls_valuearray[1] + '\SourceList\Net'
						li_rtn = RegistryGet(ls_RegistryString,"1", ls_path)
						IF li_rtn < 0 OR (len(ls_path) <= 0) Then
							//should never happen
							MessageBox('Severe Error','Contact Applications OPPA Uninstall, error reading the registry',StopSign!) 
						Else
							ls_command = 'msiexec /x "' + ls_path
							ls_RegistryString = "HKEY_CURRENT_USER\Software\Microsoft\Installer\Products\"	&
								+ ls_valuearray[1] + '\SourceList'
							li_rtn = RegistryGet(ls_RegistryString,"PackageName", ls_path)
							IF li_rtn < 0 OR (len(ls_path) <= 0) Then
								//should never happen
								MessageBox('Severe Error','Contact Applications OPPA Installer error reading the registry',StopSign!) 
							Else
								// uninstall the product
								ls_command += ls_path + '" /q'
								li_file_num = FileOpen ( 'C:\Uninstall.bat',LineMode!  , Write! , LockReadWrite!, Replace! )
								If li_file_num = -1 Then
									//error occured
									MessageBox('Severe Error','Contact Applications Uninstall.bat file Open error.',StopSign!) 
								Else
									li_num_bytes = FileWrite ( li_file_num, ls_command)
									If li_file_num = -1 or isnull(li_num_bytes) Then
										MessageBox('Severe Error','Contact Applications install.bat file write error.',StopSign!) 
									Else
										FileClose ( li_file_num )
										Run ('"C:\Uninstall.bat"', Minimized!)
										Do 
											li_rtn = u_api.findwindowa('ConsoleWindowClass','c:\WINNT\System32\cmd.exe')
											li_count = li_count + 1
										LOOP UNTIL li_rtn <> 0 or li_count > 5000
										DO
											li_rtn = u_api.findwindowa('ConsoleWindowClass','c:\WINNT\System32\cmd.exe')
										LOOP UNTIL li_rtn = 0
									End If
								End IF
							End If
						End If
					End If
				End IF
			END IF
		End If
	End If
End IF
end subroutine

private subroutine uf_bat_install ();If is_applicationname = "ORP" Or is_applicationname = "PAS" Then
	// Checking to see if the window installer mdac_type and VB core components are installed 
	//If	nf_check_isntalledcomponents() Then
		// Checking to see if the oppa installer is installed 
		uf_uninstall_com_objects()
		IF gw_base_frame.ib_oppa_install Then	
			uf_install_com_objects()	
		End if
	//End If
End if

end subroutine

private subroutine uf_manual_install ();String			ls_RegistryString, &
					ls_Installed, &
					ls_app_path, &
					ls_FilePath, &
					ls_dec_reg_val, &
					ls_subkeylist[]
					
Integer			li_rtn, &
					li_numberofentries

Long				ll_number_of_users

environment		lu_env

//**********************************************************************************************************
// Register COM Objects
//**********************************************************************************************************
If is_applicationname = "PRC" Then
	// Check to see if the OCX is registered for First Impression
	ls_Installed = ""
	li_rtn = RegistryGet("HKEY_CLASSES_ROOT\CLSID\{5A721580-5AF0-11CE-8384-0020AF2337F2}", &
								"", ls_Installed)
	IF li_rtn < 0 OR ls_Installed <> "VCI First Impression Chart" Then
		ls_installed = 'regsvr32 "' + is_applicationpath + 'VCFI32.ocx" /S'
		Run(ls_installed, Minimized!)
	END IF
End if


// Check to see if the OCX is registered for Formula One
ls_Installed = ""
ls_RegistryString = "HKEY_CLASSES_ROOT\CLSID\{042BADC5-5E58-11CE-B610-524153480001}"
li_rtn = RegistryGet(  ls_RegistryString,	"", ls_Installed)
IF li_rtn < 0 OR ls_Installed <> "VCI Formula One Workbook" Then
	ls_installed = 'regsvr32 "' + is_applicationpath + 'VCF132.OCX" /S'
	Run(ls_installed, Minimized!)
END IF


ls_Installed = ""
ls_RegistryString = "HKEY_CLASSES_ROOT\CLSID\{2E2C1DA3-3CDD-11D2-A856-400000002368}\InprocServer32"
li_rtn = RegistryGet(  ls_RegistryString,	"", ls_Installed)
IF li_rtn < 0 OR (len(ls_Installed) <= 0) Then
	ls_installed = 'regsvr32 "' + is_applicationpath + 'ProductGroupMaintenance.dll" /S'
	Run(ls_installed, Minimized!)
END IF

STRING ls_DBMS

ls_DBMS = sqlca.dbms
IF UPPER(ls_DBMS) <> UPPER("MSS Microsoft SQL Server 6.x") Then
	//**********************************************************************************************************
	// Set Path For SQL Anywhere
	//**********************************************************************************************************		
	ls_RegistryString ="HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\" + is_ApplicationFileName
	li_rtn = RegistryGet(ls_RegistryString, "Path", ls_App_Path)
	IF li_rtn < 0 OR  Len(Trim(ls_App_Path)) < 1  Then
		IF	GetEnvironment( lu_env) > 0 THEN
			CHOOSE CASE LU_env.OSType
				CASE WindowsNT!
						Choose Case lu_env.OSMajorRevision	
							CASE 4 	// WinNT 4.x 
								li_rtn = RegistrySet( ls_RegistryString, "", ls_FilePath)
								ls_App_Path="c:\SQLANY50\win32;" + is_ApplicationPath
								li_rtn = RegistrySet(  ls_RegistryString,	"Path", ls_App_Path)
							CASE 5	//Win 2000
								li_rtn = RegistrySet( ls_RegistryString, "", ls_FilePath)
								ls_App_Path="c:\SQLANY50\win32;" + is_ApplicationPath
								li_rtn = RegistrySet(  ls_RegistryString,	"Path", ls_App_Path)								
						END CHOOSE
			END CHOOSE
		END IF
	END IF
	
	//**********************************************************************************************************
	// CHECK ODBC for SQL Anywhere
	//**********************************************************************************************************
	ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers"
	li_rtn = RegistryGet(  ls_RegistryString,	"Sybase SQL Anywhere 5.0", ls_Installed)
	
	IF li_rtn < 0 OR ls_Installed <> "Installed" Then
		RegistrySet(  ls_RegistryString,  "Sybase SQL Anywhere 5.0","Installed")
	END IF
	
	ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\Sybase SQL Anywhere 5.0"
	li_rtn = RegistryGet(  ls_RegistryString,	"DRIVER", ls_Installed)
	IF li_rtn < 0 OR Len(TRim(ls_Installed)) < 1  Then
		RegistrySet( ls_RegistryString, "DRIVER","c:\SQLANY50\win32\WOD50T.DLL")
		RegistrySet( ls_RegistryString, "SETUP" ,"c:\SQLANY50\win32\WOD50T.DLL")
	END IF
	 
	 
	GetEnvironment( lu_env)
	IF LU_env.OSType  =  WindowsNT! Then // This is windows NT
		ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources"
		li_rtn = RegistryGet(  ls_RegistryString,	"pblocaldb", ls_Installed)
		
		IF li_rtn < 0 OR Len(Trim(ls_Installed)) < 1 Then
			RegistrySet(  ls_RegistryString,  "pblocaldb","Sybase SQL Anywhere 5.0")
			ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\Pblocaldb"
			RegistrySet(  ls_RegistryString,  "Start","c:\SQLANY50\win32\Rtdsk50.exe")
			RegistrySet(  ls_RegistryString,  "Driver","c:\sqlany50\win32\WOD50T.DLL")
			RegistrySet(  ls_RegistryString,  "DatabaseFile",is_ApplicationPath+"pblocaldb.db")
			RegistrySet(  ls_RegistryString,  "UID","dba")
			RegistrySet(  ls_RegistryString,  "PWD","sql")
			RegistrySet(  ls_RegistryString,  "AutoStop","yes")
			ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\ODBCAD32.exe"
			RegistrySet( ls_RegistryString,"",'"C:\WINNT\System32\ODBCAD32.exe"')
			ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\RTDSK50.EXE"
			Registryset( ls_RegistryString,"",'"c:\sqlany50\win32\RTDSK50.EXE"')
	
			// increment the shared dll count  by one
			//		ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\SharedDLLs"
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ctl3d32.dll",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ctl3d32.dll",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\DECODBDR.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\DECODBDR.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\DECODBSP.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\DECODBSP.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\DS32GT.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\DS32GT.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\MSVCRT10.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\MSVCRT10.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\MSVCRT20.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\MSVCRT20.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBC16GT.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBC16GT.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBC32.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBC32.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBC32GT.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBC32GT.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCAD32.exe",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBCAD32.exe",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCCP32.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBCCP32.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCCR32.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBCCR32.DLL",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCINST.hlp",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBCINST.hlp",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCINST.cnt",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBCINST.cnt",String(Long(ls_Dec_Reg_Val)+1))
			//		RegistryGet(ls_RegistryString, "C:\WINNT\System32\ODBCINT.DLL",ls_Dec_Reg_Val)
			//   	RegistrySet(ls_RegistryString,"C:\WINNT\System32\ODBCINT.DLL",String(Long(ls_Dec_Reg_Val)+1))
		END IF
	END IF
END IF

//ls_installed = ""
//ls_RegistryString = "HKEY_LOCAL_MACHINE\SOFTWARE\IBP\PAS\EXCEL"
//li_rtn = RegistryGet(  ls_RegistryString,	"IVAPATH", ls_Installed)
////MESSAGEBOX('GET RETURN',STRING(LI_RTN))
//IF li_rtn < 0 OR Len(Trim(ls_Installed)) < 1 Then
//	//\\NTCORP04\AppDev unicode to Find Drive On all machines
//	li_rtn = RegistrySet(  ls_RegistryString,  "PRODPATH", "F:\SOFTWARE\APPEXCEL\dave13.xls")
//	li_rtn = RegistrySet(  ls_RegistryString,  "TESTPATH", "\\NTCORP04\AppDev\Test\PB\dave13.xls")
//	//MESSAGEBOX('SET RETURN',STRING(LI_RTN))
//END IF
end subroutine

public subroutine uf_install ();string ls_WindowsDirectory, ls_WorkingDir
u_sdkcalls	U_api	

IF Not gw_base_frame.ib_ran Then
	u_api = create u_sdkcalls
	
	// added this code to bypass the client install if ran in Terminal Server ** IBDKDLD **
	ls_WindowsDirectory = u_api.nf_getwindowsdirectory() 
	
	IF LEN(ls_WindowsDirectory) >= 10 Then
		IF upper(LEFT(ls_WindowsDirectory,10)) = "H:\WINDOWS" Then Return
	End If

	// Check If running under PowerBuilder debug. ** IBDKEEM ** 07/29/2002 
	IF UPPER(is_ApplicationFileName) = UPPER('pb60.exe') &
		or UPPER(is_ApplicationFileName) = UPPER('pb80.exe') Then 
			Return
	End If
	
	// Don't Run if ServerDir is blank. ** IBDKEEM ** 07/29/2002 	
	IF LEN(is_server_path) > 1 then
		uf_bat_install()
		uf_manual_install()
	End If
	
	gw_base_frame.ib_ran = true
end if

end subroutine

on nvuo_windows_installer.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvuo_windows_installer.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;integer	li_loopcount, &
			li_pos

String	ls_JustFileName, &
			ls_FilePath
			
u_sdkcalls	lu_sdkCalls

//********************************
// Get the exe fullpath and Name
//********************************
lu_sdkcalls = Create u_sdkcalls
ls_FilePath = lu_sdkcalls.nf_getmodulefilename()
Destroy lu_sdkcalls
//Debug Code
//MessageBox ( 'u download data -- nf update registry' , "ls_file_path = API getmodulefilename" + "~r~n" + ls_filePath  )


//********************************
//Get The Path
//Get The Exe name
//********************************
li_LoopCount = 0
Do // Find The Last "\"
	li_LoopCount = POS( ls_FilePath,"\", li_LoopCount + 1)
	IF li_LoopCount > 0 Then li_pos = li_LoopCount
Loop While li_LoopCount <> 0

is_ApplicationName 		= Message.nf_Get_App_ID()
is_ApplicationPath 		= Left( ls_FilePath, li_pos)
is_ApplicationFileName 	= Mid( ls_FilePath, li_pos +1)
is_server_path 			= ProfileString ('IBP002.ini','Netwise Server Info','ServerDir','') 

is_server_path = TRIM(is_server_path)
If LEN(is_server_path) > 0 Then
	IF RIGHT(is_server_path,1) <> "\" Then
		is_server_path = is_server_path + "\"
	End If
End If
	
end event

