HA$PBExportHeader$w_close_mainframe.srw
forward
global type w_close_mainframe from Window
end type
type sle_2 from singlelineedit within w_close_mainframe
end type
type sle_1 from singlelineedit within w_close_mainframe
end type
type p_1 from picture within w_close_mainframe
end type
end forward

global type w_close_mainframe from Window
int X=750
int Y=529
int Width=1541
int Height=585
WindowType WindowType=child!
sle_2 sle_2
sle_1 sle_1
p_1 p_1
end type
global w_close_mainframe w_close_mainframe

on w_close_mainframe.create
this.sle_2=create sle_2
this.sle_1=create sle_1
this.p_1=create p_1
this.Control[]={ this.sle_2,&
this.sle_1,&
this.p_1}
end on

on w_close_mainframe.destroy
destroy(this.sle_2)
destroy(this.sle_1)
destroy(this.p_1)
end on

type sle_2 from singlelineedit within w_close_mainframe
int X=549
int Y=305
int Width=421
int Height=105
int TabOrder=20
boolean Border=false
boolean AutoHScroll=false
string Text="Please Wait"
string Pointer="Arrow!"
long TextColor=8388608
long BackColor=12632256
int TextSize=-12
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_1 from singlelineedit within w_close_mainframe
int X=202
int Y=189
int Width=1153
int Height=89
int TabOrder=10
boolean Border=false
boolean AutoHScroll=false
string Text="Closing Mainframe Connections"
string Pointer="Arrow!"
long TextColor=8388608
long BackColor=12632256
int TextSize=-12
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type p_1 from picture within w_close_mainframe
int Width=1537
int Height=577
string PictureName="splash.bmp"
boolean FocusRectangle=false
boolean OriginalSize=true
end type

