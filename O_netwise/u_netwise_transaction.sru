HA$PBExportHeader$u_netwise_transaction.sru
forward
global type u_netwise_transaction from u_base_transaction
end type
type str_commhandles from structure within u_netwise_transaction
end type
end forward

type str_commhandles from structure
    string dll_name
    string server_name
    int commhandle
end type

global type u_netwise_transaction from u_base_transaction
event type integer ue_reconnect ( )
end type
global u_netwise_transaction u_netwise_transaction

type prototypes

end prototypes

type variables
// Error structure used in all RPC Calls
s_error	istr_error

// Netwise Encryption Key
string	is_EncryptionKey = "NETWISE"

integer	ii_messagebox_rtn

u_wrkbench16_externals	luo_wkb16
u_wrkbench32_externals	luo_wkb32

private:
// Holds information on the CommHandles
str_CommHandles	istr_CommHandles[]
end variables

forward prototypes
public subroutine nf_closecommhandle ()
public function integer nf_checkrpcerror (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle)
public function integer nf_getcommhandle (string as_dllname)
public subroutine nf_getservernames ()
public function string nf_findservername (string as_dll_name)
public subroutine nf_getsqlcafromini (string as_inifile)
public function integer nf_set_s_error (ref s_error astr_error_info, ref string as_app_name, ref string as_window_name, ref string as_function_name, ref string as_event_name, ref string as_procedure_name, ref string as_user_id, ref string as_return_code, ref string as_message)
public function integer nf_get_s_error_values (ref s_error astr_error_info, ref string as_app_name, ref string as_window_name, ref string as_function_name, ref string as_event_name, ref string as_procedure_name, ref string as_user_id, ref string as_return_code, ref string as_message)
public function boolean nf_write_benchmark (time at_starttime, time at_endtime, string as_function_name, string as_other_info)
public function boolean nf_display_message (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle)
public function integer nf_checkrpcerror (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle, boolean ab_visual)
end prototypes

event type integer ue_reconnect();///////////////////////////////////////////////////////////////////////
//
//	Function:	nf_reconnect
//
//	Purpose:		Reconnects from the database.
//
//	Arguments:	(None)
//
//	Returns:		boolean	Any + - Reconnect was successful
//								-1 	- Resconnect was Unsuccessful
//
//	Created: 	IBDKEEM - 11/24/2003
////////////////////////////////////////////////////////////////////////
integer li_FailCount

SetPointer(HourGlass!)
Disconnect Using This;

li_FailCount = 0
DO
	Connect Using This;
	
	IF This.SQLCode >= 0 THEN
		RETURN li_FailCount 
	END IF
	
	li_FailCount += 1
LOOP WHILE li_FailCount < 5


nf_db_error(This, "The connection to the database was lost.")

RETURN -1
end event

public subroutine nf_closecommhandle ();integer	li_rtn, &
			li_CommHandleTotal, &
			li_Index

li_CommHandleTotal = UpperBound(istr_CommHandles)
IF IsValid( luo_wkb32) Then
	For li_Index = 1 to li_CommHandleTotal
		IF (istr_CommHandles[li_index].Dll_Name <> "") AND &
			(istr_CommHandles[li_index].CommHandle > -1) THEN
				li_rtn = luo_wkb32.WBReleaseCommHandle( istr_CommHandles[li_index].CommHandle)
		End If
	Next
	luo_wkb32._nl_reset()
ELSE	
	For li_Index = 1 to li_CommHandleTotal
		IF (istr_CommHandles[li_index].Dll_Name <> "") AND &
			(istr_CommHandles[li_index].CommHandle > -1) THEN
				li_rtn = luo_wkb16.WBReleaseCommHandle( istr_CommHandles[li_index].CommHandle)
		End If
	Next
	luo_wkb16.nl_reset()
END IF	
end subroutine

public function integer nf_checkrpcerror (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle);return This.nf_checkRPCError(ai_returnval, astr_errorinfo, ai_commhandle, True)
end function

public function integer nf_getcommhandle (string as_dllname);integer	li_rtn, &
			li_CommHandleTotal, &
			li_Index, &
			li_TempCommHandle
 

s_error	lstr_ErrorInfo

IF Upper(Left( as_dllname, 3)) = 'ORP' Then	as_dllname = Left( as_dllname, 3)+'2' + Mid(as_dllname, 5)
IF Upper(Left( as_dllname, 3)) = 'CFM' Then	as_dllname = Left( as_dllname, 3)+'2' + Mid(as_dllname, 5)

li_CommHandleTotal = UpperBound(istr_CommHandles)
IF ISVAlid( luo_wkb32) Then
	For li_Index = 1 to li_CommHandleTotal

		If istr_CommHandles[li_index].Dll_Name = as_DllName Then
			If istr_CommHandles[li_index].CommHandle = -1 Then

				li_rtn = luo_wkb32.WBGetCommHandle(li_TempCommHandle)

				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If
					
				li_rtn = luo_wkb32.WBSetSecurity(SQLCA.UserID, &
							SQLCA.DBPass, is_EncryptionKey, &
							li_TempCommHandle)
	
				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If
	
				li_rtn = luo_wkb32.WBSetServerAlias(istr_CommHandles[li_index].server_name, &
							li_TempCommHandle)
	
				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If
				IF li_rtn = 0 THEN 
					istr_CommHandles[li_index].CommHandle = li_TempCommHandle
				END IF

				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If

				// If it made it to this point, a new CommHandle was create
				Return istr_CommHandles[li_index].CommHandle

			Else
				Return istr_CommHandles[li_index].CommHandle
			End If
		End If
	Next
ELSE
	For li_Index = 1 to li_CommHandleTotal

		If istr_CommHandles[li_index].Dll_Name = as_DllName Then
			If istr_CommHandles[li_index].CommHandle = -1 Then

				li_rtn = luo_wkb16.WBGetCommHandle(li_TempCommHandle)

				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If
					
				li_rtn = luo_wkb16.WBSetSecurity(SQLCA.UserID, &
							SQLCA.DBPass, is_EncryptionKey, &
							li_TempCommHandle)
	
				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If
	
				li_rtn = luo_wkb16.WBSetServerAlias(istr_CommHandles[li_index].server_name, &
							li_TempCommHandle)
	
				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If
				IF li_rtn = 0 THEN 
					istr_CommHandles[li_index].CommHandle = li_TempCommHandle
				END IF

				If li_rtn <> 0 Then
					This.nf_CheckRPCError(li_rtn,	lstr_ErrorInfo, &
							li_TempCommHandle)
					Return -1
				End If

				// If it made it to this point, a new CommHandle was create
				Return istr_CommHandles[li_index].CommHandle

			Else
				Return istr_CommHandles[li_index].CommHandle
			End If
		End If
	Next
END IF

// If it makes it to this point, the passed DLL name was not found
MessageBox("nf_GetCommHandle() error", "Cannot find DLL '" + as_DllName + "' in " + &
				gw_netwise_frame.is_WorkingDir + "IBP002.INI")
return -1

end function

public subroutine nf_getservernames ();integer	li_ServerNumber 
string	ls_ServerName

li_ServerNumber = 1

ls_ServerName = &
		ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "nw_server", "server" + &
			String(li_ServerNumber), "Done")

Do While ls_ServerName <> "Done"

	istr_CommHandles[li_ServerNumber].Dll_Name = &
				Mid(ls_ServerName, Pos(ls_ServerName, ".") + 1, 6)
	istr_CommHandles[li_ServerNumber].Server_Name = ls_ServerName
	// 0 is a valid commhandle.  init allof them to -1 which is invalid
	istr_CommHandles[li_ServerNumber].CommHandle = -1
	li_ServerNumber ++
	ls_ServerName = &
			ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "nw_server", "server" + &
				String(li_ServerNumber), "Done")
Loop

end subroutine

public function string nf_findservername (string as_dll_name);integer		li_NbrRows, &
				li_Row

string		ls_ServerName


li_NbrRows = UpperBound(istr_commhandles)

FOR li_Row = 1 TO li_NbrRows
	IF Upper(istr_commhandles[li_Row].dll_name) = Upper(as_dll_name) THEN
		ls_ServerName = istr_commhandles[li_Row].server_name
		EXIT
	END IF
NEXT

Return( ls_ServerName )
end function

public subroutine nf_getsqlcafromini (string as_inifile);string	ls_application, ls_section, ls_message, ls_inifile
	
ls_application = Message.nf_Get_App_ID()
ls_inifile = 'ibp002.ini'

ls_section = ls_application + " DATABASE"

This.dbms =  ProfileString(ls_inifile, ls_section, "dbms", "ODBC")
This.database = ProfileString(ls_inifile, ls_section, "database", "false")
This.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")

if Upper(this.dbms) = Upper("SNC SQL Native Client(OLE DB)") Then
	This.LogPass = ProfileString(ls_inifile, ls_section, "LogPass", "sql")
	This.ServerName = ProfileString(ls_inifile, ls_section, "ServerName", " ")
	This.LogId = ProfileString(ls_inifile, ls_section, "LogId", "dba")
//Else
//	This.dbParm = ProfileString(ls_inifile, ls_section, "dbParm", "false")
End If

//if This.database = 'false' or This.dbParm = 'false' then 
//	This.database = ProfileString(ls_inifile, "DEFAULT DATABASE", "database", "pblocaldb")
//	This.dbParm = ProfileString(ls_inifile, "DEFAULT DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
//end if

ls_message = ProfileString(ls_inifile, ls_section, "message", "false")

if ls_message = 'false' then
else
	messagebox(ls_message,"This is the ini file for the local database.")
end if
																		




end subroutine

public function integer nf_set_s_error (ref s_error astr_error_info, ref string as_app_name, ref string as_window_name, ref string as_function_name, ref string as_event_name, ref string as_procedure_name, ref string as_user_id, ref string as_return_code, ref string as_message);astr_error_info.se_app_name = as_app_name 
astr_error_info.se_window_name = as_window_name
astr_error_info.se_function_name = as_function_name 
astr_error_info.se_event_name = as_event_name
astr_error_info.se_procedure_name = as_procedure_name
astr_error_info.se_user_id = as_user_id
astr_error_info.se_return_code = as_return_code
astr_error_info.se_message = as_message

Return(1)
end function

public function integer nf_get_s_error_values (ref s_error astr_error_info, ref string as_app_name, ref string as_window_name, ref string as_function_name, ref string as_event_name, ref string as_procedure_name, ref string as_user_id, ref string as_return_code, ref string as_message);IF IsNull( astr_error_info.se_app_name) Then
	as_app_name = Space(8)
ELSE
as_app_name = astr_error_info.se_app_name + fill(' ', &
		8 - Len(astr_error_info.se_app_name))
END IF		

IF IsNull( astr_error_info.se_window_name) Then
	as_window_name = Space(8)
ELSE
	as_window_name = astr_error_info.se_window_name + fill(' ', &
			8 - Len(astr_error_info.se_window_name))
END IF			

IF IsNull( astr_error_info.se_function_name) Then
	as_function_name = Space(8)
ELSE
	as_function_name = astr_error_info.se_function_name + fill(' ', &
		8 - Len(astr_error_info.se_function_name))
END IF		
IF IsNull( astr_error_info.se_event_name) Then
	as_event_name = Space(32)
ELSE
	as_event_name = astr_error_info.se_event_name + fill(' ', &
			32 - Len(astr_error_info.se_event_name))
END IF		
IF IsNull( astr_error_info.se_procedure_name ) Then
	as_procedure_name = Space(32)
ELSE
	as_procedure_name = astr_error_info.se_procedure_name + fill(' ', &
			32 - Len(astr_error_info.se_app_name))
END IF		
IF IsNull( astr_error_info.se_user_id) Then
	as_user_id = Space(8)
ELSE
	as_user_id = astr_error_info.se_user_id + fill(' ', &
			8 - Len(astr_error_info.se_app_name))
END IF		
IF IsNull( astr_error_info.se_return_code ) Then
	as_return_code = Space(6)
ELSE
	as_return_code = astr_error_info.se_return_code + fill(' ', &
			6 - Len(astr_error_info.se_return_code))
END IF		

IF IsNull( astr_error_info.se_message ) Then
	as_message= Space(71)
ELSE
	as_message = astr_error_info.se_message + fill(' ', &
			71 - Len(astr_error_info.se_message))
END IF		
Return(1)
end function

public function boolean nf_write_benchmark (time at_starttime, time at_endtime, string as_function_name, string as_other_info);char 			lc_PCIP_Address[100], &
				lc_region

Integer		li_fileNum, &
				li_TimeVal
						
Long			ll_SecondsAfter						

String		ls_FileName, &
				ls_ipaddr, &
				ls_pcip_address, &
				ls_path, &
				ls_ntEventLog

u_ip_functions			lu_ip_address_functions


li_TimeVal = ProfileInt(gw_netwise_frame.is_WorkingDir + "ibp002.ini","BenchMark", Message.nf_Get_App_ID(),0)

ls_ntEventLog = ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "BenchMark", "NTEVENTLOG", "YES")

lc_region = Right(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "nw_server", "server1", ""), 1)
If lc_region = 'z' AND ls_ntEventLog = "YES" Then
	// Log All QA Activity, Are they working today????
	ll_SecondsAfter = SecondsAfter(at_starttime,at_endtime) 
ELSEIf lc_region = 't' AND ls_ntEventLog = "YES" Then
	// Log All QA Activity, Are they working today????
	ll_SecondsAfter = SecondsAfter(at_starttime,at_endtime) 
Else
	IF li_timeVal = 0 Then Return False								
	ll_SecondsAfter = SecondsAfter(at_starttime,at_endtime) 
	IF ll_SecondsAfter < li_TimeVal Then Return False 
End If

Choose Case lc_region
	Case 'p'
		ls_path = ProfileString('ibp002.ini', "BenchMark",'PATH', "f:\software\pb\pblog101\")
//		ls_path = "f:\software\pb\pblog101\"
	Case 't'
//		ls_path = ProfileString('ibp002.ini', "BenchMark",'PATH', "\\pc2164\QA\LOGS\")
		ls_path = ProfileString('ibp002.ini', "BenchMark",'PATH', "j:\test\pb\")
//		ls_path = "j:\test\pb\"
	Case 'z'
		ls_path = ProfileString('ibp002.ini', "BenchMark",'PATH', "\\pc2164\QA\LOGS\")
//		ls_path = "j:\software\pb\qa\pblog101\"
	Case ''
		Return False
End Choose

ls_fileName = ls_path + "BM" + String(Month(Today()), '00') + String(Day(Today()), '00') + ".log"
li_FileNum = FileOpen(ls_FileName,  lineMode!, Write!, LockWrite! , Append! )

If li_FileNum >= 0 then
	ls_ipaddr = lu_ip_address_functions.nf_get_ip_address()
	
	FileWrite(li_FileNum,Message.nf_Get_App_ID() + &
				"~tUser:~t" + SQLCA.Userid + &
				"~tIP Addr:~t" + ls_ipaddr + &
				'~t' + Trim(As_Function_Name) + &
				"~tStart:~t" + String(At_StartTime) + &
				"~tEnd:~t" + String(At_EndTime) + &
				"~tElapsed:~t" + String(ll_SecondsAfter) + &
				"~tOther Info:~t" + as_other_info)
				
	FileClose(li_FileNum)
End If

Return True 

end function

public function boolean nf_display_message (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle);Boolean  lb_retval
			
Int      li_rpc_error_rtn, &
			li_rtn
String	ls_microhelp_str, &
			ls_active_sheet_title

LB_RETVAL = TRUE
 
li_rpc_error_rtn = This.nf_CheckRPCError(ai_rtn,	astr_errorinfo, ai_commhandle)
IF li_rpc_error_rtn >= 0 THEN
	IF ai_rtn = 0 and li_rpc_error_rtn = 0 then
		IF LEN(TRIM( astr_errorinfo.se_Message)) > 0 THEN
			ls_microHelp_str = astr_errorinfo.se_Message
			ls_microHelp_str = Left( ls_microHelp_str,71)
			gw_netwise_frame.SetMicroHelp( Ls_MicroHelp_Str)
			RETURN TRUE
		END IF
	END IF
	IF ai_rtn < 0 THEN
		MessageBox("Error Updating", astr_errorinfo.se_Message)
		lb_retval = FALSE		
  	ELSE
		IF ai_rtn > 0 and ai_rtn < 10 THEN
			// Set Micro Help      
			ls_microHelp_str = astr_errorinfo.se_Message
		   ls_microHelp_str = Left( ls_microHelp_str,71)
			gw_netwise_frame.SetMicroHelp( Ls_MicroHelp_str)
			lb_retval = FALSE
		END IF
	END IF
	IF ai_rtn >= 10 THEN
		If IsValid( gw_netwise_frame) Then
			Window	lw_active_sheet

			lw_active_sheet = gw_netwise_frame.GetActiveSheet()
			If IsValid(lw_active_sheet) Then
				ls_Active_Sheet_Title = lw_active_sheet.Title
			End If
		END IF
		CHOOSE CASE ai_rtn
			CASE	10
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message)
				ii_messagebox_rtn = 1
			CASE	11
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	12
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	13
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	14
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	15
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	20
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, OK!)
				ii_messagebox_rtn = 1
			CASE	21
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	22
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	23
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	24
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	25
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	30
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, OK!)
				ii_messagebox_rtn = 1
			CASE	31
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	32
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	33
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	34
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	35
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	40
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, OK!)
				ii_messagebox_rtn = 1
			CASE	41
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	42
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	43
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	44
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	45
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
		END CHOOSE	
			lb_retval = FALSE
		END IF					
ELSE
	//IF li_rpc_error_rtn = -107 then
		lb_retval = FALSE
END IF



Return lb_retval
end function

public function integer nf_checkrpcerror (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle, boolean ab_visual);// To be called after running an RPC.

integer 					li_result, &
							li_commerror, &
							li_file, &
							li_temp, &
							li_upper_bound
				
long 						ll_neterror, &
							ll_primaryerror, &
							ll_secondaryerror
				
string 					ls_neterrmsg, &
							ls_commerrmsg, &
							ls_server_name

s_rpc_error 			lstr_rpc_error_info

u_string_functions	lu_string

window					lw_RPCError



ls_neterrmsg = space(100)
ls_commerrmsg = space(100)

IF IsValid( luo_wkb32) Then
	li_result = luo_wkb32.WBGetErrorInfo( li_commerror, ls_commerrmsg, ll_neterror,&
				 ls_neterrmsg, ll_primaryerror, ll_secondaryerror, ai_CommHandle)
ELSE				 
	li_result = luo_wkb16.WBGetErrorInfo( li_commerror, ls_commerrmsg, ll_neterror,&
				 ls_neterrmsg, ll_primaryerror, ll_secondaryerror, ai_CommHandle)
END IF

if li_result = 0 then
	if li_commerror = 0 and ai_ReturnVal >= 0 then
		return(ai_ReturnVal)
	else
		lstr_rpc_error_info.se_app_name = astr_ErrorInfo.se_app_name
		lstr_rpc_error_info.se_window_name = astr_ErrorInfo.se_window_name
		lstr_rpc_error_info.se_function_name = astr_ErrorInfo.se_function_name
		lstr_rpc_error_info.se_event_name = astr_ErrorInfo.se_event_name
		lstr_rpc_error_info.se_procedure_name = astr_ErrorInfo.se_procedure_name
		lstr_rpc_error_info.se_user_id = astr_ErrorInfo.se_user_id
		lstr_rpc_error_info.se_return_code = astr_ErrorInfo.se_return_code
		lstr_rpc_error_info.se_message = astr_ErrorInfo.se_message
		if ai_ReturnVal >= 0 then
			ai_ReturnVal = -25
		end if
		lstr_rpc_error_info.se_rval = ai_ReturnVal
		lstr_rpc_error_info.se_commerror = li_commerror
		lstr_rpc_error_info.se_commerrmsg = ls_commerrmsg
		lstr_rpc_error_info.se_neterror = ll_neterror
		if lstr_rpc_error_info.se_neterror <> 0 then
			lstr_rpc_error_info.se_neterrmsg = ls_neterrmsg
		else
			lstr_rpc_error_info.se_neterrmsg = space(100)
		end if
		lstr_rpc_error_info.se_primaryerror = ll_primaryerror
		lstr_rpc_error_info.se_secondaryerror = ll_secondaryerror

		If Not (li_commerror = 0 And ll_primaryerror = 0 And &
				  ll_secondaryerror = 0 And ll_neterror = 0) Then
			// If they all equal zero, don't log
			li_upper_bound = UpperBound(istr_commhandles)
			For li_temp = 1 to li_upper_bound
				If ai_commhandle = istr_commhandles[li_temp].commhandle Then
					ls_server_name = istr_commhandles[li_temp].server_name
					Exit
				End if
			Next

			If lu_string.nf_IsEmpty(ls_server_name) Then
				ls_server_name = "Unknown Server Name"
			End if

			li_file = FileOpen("f:\software\pb\pblog101\pberror.log", &
									LineMode!, Write!, LockWrite!, Append!)
			If li_file >  0 Then
				If Len(Trim(astr_errorinfo.se_user_id)) < 1 Then
					astr_errorInfo.se_user_id = SQLCA.UserID
				End if
				FileWrite(li_file, String(Today(), "mm-dd-yyyy") + '~t' + &
							String(Now(), "hh:mm:ss.ff") + '~t' + astr_errorinfo.se_user_id + &
							'~t' + ls_server_name + '~t' + astr_errorinfo.se_window_name + &
							'~t' + astr_errorinfo.se_procedure_name + '~t' + &
							String(li_commerror) + '~t' + String(ll_primaryerror) + '~t' + &
							String(ll_secondaryerror) + '~t' + String(ll_neterror))
				FileClose(li_file)
			End if
		End if
		
		If ab_visual Then
			//ibdkdld 11/27/02 bypass 107 errors Per Duane Z
			If li_commerror <> 107 Then
				openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
			Else
				Return(-100)
			End If
		End if
		return(ai_ReturnVal)
	end if
else
	return (-100)
end if

return(0)
end function

on u_netwise_transaction.create
call super::create
end on

on u_netwise_transaction.destroy
call super::destroy
end on

event constructor;call super::constructor;Environment	lenv_env
GetEnvironment( lenv_env)

IF lenv_env.OsMajorRevision = 4 OR lenv_env.OsType = WindowsNt! Then
	luo_wkb32 = Create u_wrkbench32_externals	
Else
	luo_wkb16 = Create u_wrkbench16_externals
End if	


end event

