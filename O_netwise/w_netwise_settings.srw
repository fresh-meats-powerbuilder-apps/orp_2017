HA$PBExportHeader$w_netwise_settings.srw
forward
global type w_netwise_settings from w_base_settings
end type
end forward

global type w_netwise_settings from w_base_settings
integer height = 1024
end type
global w_netwise_settings w_netwise_settings

forward prototypes
public subroutine wf_initialize_microhelp (datawindow adw_target)
end prototypes

public subroutine wf_initialize_microhelp (datawindow adw_target);u_base_data		lu_base_data


lu_base_data = gw_netwise_frame.iu_base_data

adw_target.Object.userid[1]				=	lu_base_data.ii_userid
adw_target.Object.microhelp_status[1]	=	lu_base_data.ii_status
adw_target.Object.microhelp_datetime[1]=	lu_base_data.ii_datetime
adw_target.Object.microhelp_color[1]	=	lu_base_data.il_microhelp_color
adw_target.Object.userid_color[1]		=	lu_base_data.il_userid_color
adw_target.Object.status_color[1]		=	lu_base_data.il_status_color
adw_target.Object.datetime_color[1]		=	lu_base_data.il_datetime_color
adw_target.Object.prompt_on_exit[1]		=	lu_base_data.ii_prompt_on_exit
adw_target.Object.prompt_to_save_onexit[1]			=	lu_base_data.ii_prompt_to_save_onexit
adw_target.Object.save_datawindow_layouts[1]			=	lu_base_data.ii_save_dw_layouts
adw_target.Object.display_horizontal_scrollbars[1]	=	lu_base_data.ii_display_hscrollbars
adw_target.Object.display_vertical_scrollbars[1]	=	lu_base_data.ii_display_vscrollbars
adw_target.Object.prompt_on_close[1]					=	lu_base_data.ii_prompt_on_close
adw_target.Object.always_openas[1]						=	lu_base_data.is_always_openas

end subroutine

on w_netwise_settings.create
call super::create
end on

on w_netwise_settings.destroy
call super::destroy
end on

type tab_1 from w_base_settings`tab_1 within w_netwise_settings
string tag = "MicroHelp"
integer height = 724
end type

type tabpage_3 from w_base_settings`tabpage_3 within tab_1
integer height = 596
end type

type dw_window from w_base_settings`dw_window within tabpage_3
integer height = 572
end type

type tabpage_2 from w_base_settings`tabpage_2 within tab_1
integer height = 596
end type

type dw_toolbars from w_base_settings`dw_toolbars within tabpage_2
end type

type cb_reset from w_base_settings`cb_reset within w_netwise_settings
end type

event cb_reset::clicked;Tab_1.tabpage_2.dw_toolbars.Trigger Event ue_reset()
tab_1.tabpage_3.dw_window.Trigger Event ue_reset()
end event

type cb_ok from w_base_settings`cb_ok within w_netwise_settings
end type

