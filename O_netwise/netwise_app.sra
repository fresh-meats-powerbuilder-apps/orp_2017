HA$PBExportHeader$netwise_app.sra
forward
global u_netwise_transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global u_netwise_message message
end forward

global variables
w_base_frame	gw_base_frame
w_netwise_frame	gw_netwise_frame
end variables

global type netwise_app from application
 end type
global netwise_app netwise_app

on netwise_app.create
appname = "netwise_app"
message = create u_netwise_message
sqlca = create u_netwise_transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on netwise_app.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

event open;MessageBox("No", "This application is for regenerating Netwise objects only")
Halt Close
end event

