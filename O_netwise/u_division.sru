HA$PBExportHeader$u_division.sru
forward
global type u_division from u_netwise_dw
end type
end forward

global type u_division from u_netwise_dw
int Width=1595
int Height=88
string DataObject="d_division"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
event ue_accept ( )
end type
global u_division u_division

type variables
DataWindowChild	idwc_child

Boolean		ib_required
end variables

forward prototypes
public function string uf_get_division_descr ()
public function string uf_get_division ()
public subroutine disable ()
public subroutine enable ()
public subroutine uf_set_division (string as_division)
end prototypes

event ue_accept;call super::ue_accept;This.AcceptText()

end event

public function string uf_get_division_descr ();return Trim(This.GetItemString(1, "division_description"))
end function

public function string uf_get_division ();return Trim(This.GetItemString(1, "division_code"))
end function

public subroutine disable ();This.Modify("division_code.Background.Mode = 1 division_code.Protect = 1 " + &
				"division_code.Pointer = 'Arrow!'")

end subroutine

public subroutine enable ();This.Modify("division_code.Background.Mode = 0 division_code.Protect = 0" + &
				"division_code.Pointer = 'Beam!'")

end subroutine

public subroutine uf_set_division (string as_division);Long	ll_row, &
		ll_row_count

String	ls_text, &
			ls_description


This.SetItem(1, "division_code", as_division)

This.GetChild("division_code",idwc_child)

ls_text = 'Trim(type_code) = "' + as_division + '"'
ll_row_count = idwc_child.RowCount()
ll_row = idwc_child.Find(ls_text, 1, ll_row_count)
If ll_row <= 0 Then
	If ib_required OR as_division <> "  " Then
		gw_netwise_Frame.SetMicroHelp(as_division + " is an Invalid Division Code")
		SetFocus()
		This.SelectText(1, Len(ls_text))
	End If
	ls_description = ""
Else
	SetFocus()
	This.SelectText(1, Len(ls_text))
	ls_Description = idwc_child.GetItemString(ll_row, "type_desc")
	gw_netwise_frame.SetMicroHelp("Ready")
End if

This.SetItem(1, "division_description", ls_Description)
end subroutine

event destructor;call super::destructor;SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastDivision", &
			This.GetItemString(1, 'division_code'))

end event

on getfocus;call u_netwise_dw::getfocus;This.SelectText(1, Len(This.GetText()))
end on

event itemchanged;call super::itemchanged;Long	ll_row

String	ls_text, &
			ls_description


ls_text = This.GetText()
If Len(Trim(ls_text)) = 0 Then 
	This.SetItem(1, "division_description", "")	
	return
End if
ll_row = idwc_child.Find('Trim(type_code) = "' + ls_text + '"', 1, idwc_child.RowCount())
If ll_row <= 0 Then
	gw_netwise_Frame.SetMicroHelp(ls_text + " is an Invalid Division Code")
//	ls_description = ""
//	This.SetItem(1, "division_description", ls_Description)
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	return 1
Else
//	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	ls_Description = idwc_child.GetItemString(ll_row, "type_desc")
	gw_netwise_frame.SetMicroHelp("Ready")
End if

This.SetItem(1, "division_description", ls_Description)
end event

event itemerror;call super::itemerror;return 1
end event

on itemfocuschanged;call u_netwise_dw::itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end on

event constructor;call super::constructor;ib_updateable = False
ib_required = True

String	ls_Division

This.GetChild("division_code", idwc_child)

idwc_child.SetTransObject(SQLCA)
idwc_child.Retrieve("DIVCODE")

//idwc_child.Sort()

If This.Describe("division_code.Protect") <> '1' Then
	ls_Division = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastDivision", "  ")
	This.Reset()
	This.SetItem(This.InsertRow(0), 'division_code', ls_Division)
	This.TriggerEvent(ItemChanged!)
End if

end event

event losefocus;call super::losefocus;This.TriggerEvent('ue_Accept')

end event

