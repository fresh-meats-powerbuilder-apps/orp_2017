HA$PBExportHeader$nvuo_vb_profile.sru
$PBExportComments$Used by VB Objects to access user infomation.
forward
global type nvuo_vb_profile from nonvisualobject
end type
end forward

global type nvuo_vb_profile from nonvisualobject
end type
global nvuo_vb_profile nvuo_vb_profile

type variables
u_ole_com        	iu_oleProfile
boolean		ib_bypass
end variables

forward prototypes
public function boolean setvalues ()
end prototypes

public function boolean setvalues ();// ****************************************************************************************************************
// This function sets all the Application variables that VB objects can use
// ****************************************************************************************************************
integer		li_Application
string		ls_userid 
string		ls_password 
string		ls_server_suffix
string		ls_PAS301Server
string		ls_GroupID             
string		ls_System              
string		ls_PBConnectionString 
string		ls_DB2ConnectionString 
string		ls_DB2Prefix

If Isvalid(iu_oleProfile) and not ib_bypass Then	
	if not iu_oleProfile.locked then
		// get the values
		ls_userid = SQLCA.UserID
		ls_password = SQLCA.dbpass
		ls_server_suffix = ProfileString ("Ibp002.ini", "VB Settings", "ServerSuffix", "p" ) 
		ls_PAS301Server = ProfileString ("Ibp002.ini", "VB Settings", "PAS301Server", "dkmvs00.pas301p" )
		ls_PBConnectionString = ProfileString ("Ibp002.ini", "VB Settings", "PBConnectionString", "Provider=SQLOLEDB.1;Password=abc123;Persist Security Info=True;User ID=pblocalowner;Initial Catalog=pblocaldb;Data Source=NTSQL02") 
 		ls_DB2ConnectionString = ProfileString ("Ibp002.ini", "VB Settings", "DB2ConnectionString", "Provider=IBMDADB2.1;Data Source=MVSDB200") 
	 	ls_DB2Prefix = ProfileString ("Ibp002.ini", "VB Settings", "DB2Prefix", "IBP") 
		 
		CHOOSE CASE Upper(Message.nf_Get_App_ID())
			CASE "ORP"
				li_Application = 2
			CASE "PAS"
				li_Application = 1
			CASE ELSE
				li_Application = 0
		END CHOOSE
		
		ls_GroupID = iw_frame.iu_netwise_data.is_groupid
		ls_System = iw_frame.iu_netwise_data.is_system_id	


		// set the values
		if not isnull(ls_userid) then
			iu_oleProfile.UserID						= ls_userid
		end if
		
		if not isnull(ls_password) then
			iu_oleProfile.Password            	= ls_password
		end if

		if not isnull(ls_GroupID) then
			iu_oleProfile.GroupID             	= ls_GroupID
		end if
		
		if not isnull(ls_System) then
			iu_oleProfile.System              	= ls_System
		end if
		
		iu_oleProfile.PAS301Server        	= ls_PAS301Server
		iu_oleProfile.SignOnSystrm        	= ls_server_suffix
		iu_oleProfile.Application 				= li_Application
		iu_oleProfile.PBConnectionString  	= ls_PBConnectionString
		iu_oleProfile.DB2ConnectionString	= ls_DB2ConnectionString
		iu_oleProfile.DB2Prefix					= ls_DB2Prefix
		
		iu_oleProfile.locked = true
	end if
end if

return true
end function

on nvuo_vb_profile.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvuo_vb_profile.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;Destroy iu_oleProfile
end event

event constructor;integer		li_rtn
ib_bypass = false
 
iu_oleProfile = Create u_ole_com
li_rtn = iu_oleProfile.ConnectToNewObject("IBPProfile.Profile")
	
If li_rtn < 0 Then
	// Problem happened, handle this error and forget about retrieving the report list
//	iw_frame.setmicrohelp("Return code from VB Profile Object is " + string(li_rtn)) 
//	ib_bypass = true
End If
end event

