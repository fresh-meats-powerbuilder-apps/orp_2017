HA$PBExportHeader$u_netwise_dw.sru
forward
global type u_netwise_dw from u_base_dw
end type
end forward

global type u_netwise_dw from u_base_dw
end type
global u_netwise_dw u_netwise_dw

type variables
PROTECTED:
BOOLEAN ib_DB_Reconnectable

PRIVATE:
BOOLEAN ib_DBDead
INTEGER ii_retry_count
end variables

forward prototypes
public function integer uf_importstring (string as_string, boolean ab_resetdw)
public function integer uf_sortcolumnsintoarray (string as_stringtoparse, string as_delimiter, str_sortcolumns astr_sortcolumns[])
end prototypes

public function integer uf_importstring (string as_string, boolean ab_resetdw);//////////////////////////////////////////////////////////////////////////
//
//	Function:	uf_ImportString(as_string
//
//	Purpose:		Imports a string into this datawindow and then sets
//					the status of all rows to NotModified!
//
//	Arguments:	ai_String			A tab delimted string to be imported into
//											this datawindow.
//					ab_ResetDW			A Boolean to reset the datawindow before 
//											the ImportString.
//
// Returns:		integer li_rtn
//
//	Author:		T.J. Cox
//
// Documentation: Anil Peggerla		9/07/95
//////////////////////////////////////////////////////////////////////////
integer	li_rtn

// Check to see if we have to reset the datawindow before the import
If ab_ResetDW Then
	This.Reset()
End If

// Import the string and return the return value
li_rtn = This.ImportString(Trim(as_String))

If li_rtn > 0 Then
	This.ResetUpdate()
End If

Return li_rtn

end function

public function integer uf_sortcolumnsintoarray (string as_stringtoparse, string as_delimiter, str_sortcolumns astr_sortcolumns[]);integer	li_delimiter_pos, &
			li_delimiter_len, &
			li_count, &
			li_StartPos = 1
string	ls_Holder


// Find the length of the passed delimiter
li_delimiter_len = Len(as_delimiter)

// Find the position of the first found delimiter
li_delimiter_pos =  Pos(as_StringToParse, as_delimiter, li_StartPos)

If Trim(as_StringToParse) = "" OR as_StringToParse = "?" Then
	Return 0
ElseIf li_delimiter_pos = 0 Then
	astr_sortcolumns[1].column_name = Left(as_StringToParse, Pos(as_StringToParse, " ") - 1)
	astr_sortcolumns[1].sort_order = Right(as_StringToParse, 1)
	Return 1
End If

Do While li_delimiter_pos > 0
	ls_Holder = Mid(as_StringToParse, li_StartPos, (li_delimiter_pos - li_StartPos))
	li_count ++
	astr_sortcolumns[li_count].column_name = Left(ls_holder, Pos(ls_holder, " ") - 1)
	astr_sortcolumns[li_count].sort_order = Right(ls_holder, 1)
	li_StartPos = li_delimiter_pos + li_delimiter_len
	li_delimiter_pos =  Pos(as_StringToParse, as_delimiter, li_StartPos)	
Loop

ls_Holder = Mid(as_StringToParse, li_StartPos, Len(as_StringToParse))

li_count = li_count + 1
astr_sortcolumns[li_count].column_name = Left(ls_holder, Pos(ls_holder, " ") - 1)
astr_sortcolumns[li_count].sort_order = Right(ls_holder, 1)

Beep(1)
Return li_count


end function

event itemfocuschanged;call super::itemfocuschanged;string		ls_tag_text

// Get the current column name
If IsNull(dwo) Then Return

is_ColumnName = String(dwo.Name)

// Set the MicroHelp to the tag values for the columns of the datawindow.
If Len(is_ColumnName) > 0 Then 
	ls_tag_text = Describe(This, is_ColumnName + ".tag")
	If ls_tag_text <> "?" And ls_tag_text <> "!" And Len(ls_tag_text) > 0 Then
		iw_parent.SetMicrohelp(ls_tag_text)
	End If
End If
end event

event constructor;call super::constructor;ib_DBDead = false
ii_retry_count = 0

//This.event ue_retrieve(0,0)
end event

event dberror;call super::dberror;transaction 		lu_transaction
ClassDefinition 	lu_ClassDef

ib_DB_Reconnectable = true

if (sqldbcode = 10005 or sqldbcode = 10025 or sqldbcode = 121 or sqldbcode = 10054 or sqldbcode = -1) &
	and not ib_DBDead &
		and ii_retry_count < 2  &
			and ib_DB_Reconnectable then
			
	ib_DBDead = true
	ii_retry_count += 1
	
	lu_ClassDef = sqlca.classdefinition
	do while IsValid(lu_ClassDef)
		if lu_ClassDef.name = "u_netwise_transaction" then
			sqlca.postevent("ue_reconnect")
			//this.postevent("ue_retrieve",0,0)
			parent.postevent("ue_base_cancel")
			exit
		end if
		
		lu_ClassDef = lu_ClassDef.Ancestor		
	loop
	
	ib_DBDead = false
else
	messagebox("Datawindow error in object '" + this.classname( ) + "'" , &
					"A Database error occurred.~r~n Error Code " + &
					string(sqldbcode) + &
					"~r~n Error Message "  + &
					sqlerrtext)	
end if

return 1 // Do not display default error message.
end event

event ue_retrieve;call super::ue_retrieve;//This is where you put SetTransObject and Retrieve
end event

on u_netwise_dw.create
call super::create
end on

on u_netwise_dw.destroy
call super::destroy
end on

