HA$PBExportHeader$u_netwise_data.sru
forward
global type u_netwise_data from nonvisualobject
end type
end forward

global type u_netwise_data from nonvisualobject
event ue_postconstructor ( )
end type
global u_netwise_data u_netwise_data

type prototypes

Function int HookKB() library "IBPKBHook.dll"
Function int UnHookKB() library "IBPKBHook.dll"
end prototypes

type variables
u_base_datastore	ids_win_access, &		
						ids_menu_class 

String				is_system_id, &
						is_userid,&
						is_groupid

//nvuo_vb_profile   invuo_profile
end variables
forward prototypes
public function string nf_getlocationtype (character ac_plant_code[3])
public function integer nf_getshortdesc (string as_recordtype, string as_typecode, ref string as_typedesc)
public function boolean nf_secure_windows ()
public function string nf_get_group_id (ref s_error astr_error_info, string as_name_string, ref string as_groupid)
public function boolean nf_getaccess (readonly string as_window_class, ref string as_add, ref string as_delete, ref string as_modify, ref string as_inquire)
public subroutine nf_savesqlca_settings (string as_inifile)
public function boolean nf_getaccess (string as_window_class, ref boolean ab_add, ref boolean ab_modify, ref boolean ab_inquire, ref boolean ab_delete)
public function boolean nf_iswindowaccessable (string as_window_name)
public function boolean nf_getmenuplacement (string as_menuid, ref integer ai_level1, ref integer ai_level2, ref integer ai_level3)
end prototypes

event ue_postconstructor();//******************************************************************************
//**EEM** this object is used by the vb windows to get user infomation quickly.
//******************************************************************************

// Removed code forvb profile

//IF not Isvalid (invuo_profile) THEN 
//	invuo_profile = CREATE nvuo_vb_profile
//	invuo_profile.setvalues()
//end if
//******************************************************************************

//******************************************************************************
//**EEM** 06/19/2002 ** 
// This object sets a hook into the applications keyboard event. CTRL+Tab presses
//	are then handled by posting a NEXTWINDOW massage to the Active sheet.
//
// The Destructor of this object (u_netwise_data) must call UnHookKB() 
// or bad things will happen
//******************************************************************************
//** API Call to the IBPKBHook.dll **
HookKB()
//******************************************************************************
end event

public function string nf_getlocationtype (character ac_plant_code[3]);string		ls_location_type

SELECT 	locations.location_type  
INTO		:ls_location_type
FROM 		locations  
WHERE 	locations.location_code = :ac_plant_code;

RETURN ls_location_type
end function

public function integer nf_getshortdesc (string as_recordtype, string as_typecode, ref string as_typedesc);SELECT	tutltypes.type_short_desc  
INTO		:as_typedesc
FROM		tutltypes  
WHERE 	( tutltypes.record_type = :as_recordtype ) AND  
         ( tutltypes.type_code = :as_typecode )   ;

IF IsNull(as_typedesc) or Trim(as_typedesc) = '' THEN RETURN -1
RETURN 0
	

end function

public function boolean nf_secure_windows ();Int 	li_i,&
		li_j,&
		li_k,&
		li_top_items,&
		li_sub_items,&
		li_last_items, &
		li_num_windows, &
  		li_place_1, &
		li_place_2, &
		li_place_3, &
		li_comp_val

Long	ll_rtn_row

String	ls_Import_String,&
			ls_find_string, &
			ls_name_string, &
			ls_groupid
s_error	lstr_error_info

ids_win_access 				= Create u_base_datastore
ids_menu_class					= Create u_base_datastore
ids_menu_class.DataObject	= 'd_menu_class'
ids_win_access.DataObject	= 'd_proj_win_access'

li_top_items = UpperBound(gw_netwise_frame.im_netwise_menu.Item[])

	FOR li_i = 1 TO li_top_items
		ls_Import_String = ls_Import_String+String(li_i)+"~t0~t0~t"+Upper(gw_netwise_frame.im_netwise_menu.Item[li_i].ClassName())+"~r~n"
		li_sub_items = UpperBound(gw_netwise_frame.im_netwise_menu.Item[li_i].Item[])
		FOR li_j = 1 TO li_sub_items
			ls_Import_String = ls_Import_String+String(li_i)+"~t"+String(li_j)+"~t0~t"+Upper(gw_netwise_frame.im_netwise_menu.Item[li_i].Item[li_j].ClassName())+"~r~n"
			li_last_items = UpperBound(gw_netwise_frame.im_netwise_menu.Item[li_i].Item[li_j].Item[])
	      FOR li_k = 1 TO li_last_items
	         ls_Import_String = ls_Import_String+String(li_i)+"~t"+String(li_j)+"~t"+String(li_k)+"~t"+Upper(gw_netwise_frame.im_netwise_menu.Item[li_i].item[li_j].item[li_k].ClassName())+"~r~n"
	      NEXT
	    NEXT
	NEXT

ll_rtn_row = ids_menu_class.ImportString(ls_Import_String)

//retrieve proj specific windows passing groupid and systemid
If is_system_id = "PAS" Then
	is_system_id = "PRD"
End If
ls_name_string	=	SQLCA.UserID + '~t' + is_system_id
nf_get_group_id(lstr_error_info, ls_name_string, ls_groupid)
ids_win_access.SetTransObject(SQLCA)
ids_win_access.Retrieve(is_system_id, ls_groupid)
If is_system_id = "PRD" Then
	is_system_id = "PAS"
End If

li_num_windows = ids_win_access.RowCount()

For li_i = 1 to li_num_Windows
  	Li_comp_val = ids_win_access.GetItemNumber( li_i, "compute_see_window") 
	ls_import_string = trim(ids_win_access.GetItemString( li_i, "Menuid"))
	IF li_comp_val= 0 THEN
		ls_find_string = "menu_class_name = '" + trim(ids_win_access.GetItemString( li_i, "Menuid"))+"'" 
      ll_rtn_row = ids_menu_class.Find( ls_find_String, 1, ids_menu_class.RowCount()) 
		IF ll_rtn_row > 0 THEN
			li_place_1 = ids_menu_class.GetItemNumber( ll_rtn_row, "menu_place1")
			li_place_2 = ids_menu_class.GetItemNumber( ll_rtn_row, "menu_place2")
			li_place_3 = ids_menu_class.GetItemNumber( ll_rtn_row, "menu_place3")
			IF li_place_2 > 0 THEN
				IF li_place_3 > 0 THEN
					gw_netwise_frame.im_netwise_menu.Item[li_place_1].item[li_place_2].item[li_place_3].enabled = FALSE
					gw_netwise_frame.im_netwise_menu.Item[li_place_1].item[li_place_2].item[li_place_3].ToolBarItemVisible = FALSE
				ELSE	
					gw_netwise_frame.im_netwise_menu.Item[li_place_1].item[li_place_2].enabled = FALSE
					gw_netwise_frame.im_netwise_menu.Item[li_place_1].item[li_place_2].ToolBarItemVisible = FALSE
				END IF
			ELSE
				gw_netwise_frame.im_netwise_menu.Item[li_place_1].enabled = FALSE
				gw_netwise_frame.im_netwise_menu.Item[li_place_1].ToolBarItemVisible = FALSE
			END IF
		END IF
   END IF
Next
Return TRUE


end function

public function string nf_get_group_id (ref s_error astr_error_info, string as_name_string, ref string as_groupid);integer	li_rtn

s_error	lstr_ErrorInfo

u_ws_utl		lu_ws_utl

SetPointer( HourGlass!)
lu_ws_utl	=	Create	u_ws_utl
gw_netwise_frame.SetMicroHelp( "Loading Groups")
li_rtn = lu_ws_utl.nf_utlu00er(lstr_ErrorInfo, &
											as_name_string, &
											as_groupid)

If li_rtn <> 0 Then
	MessageBox("Error Downloading", "Problem retrieving " + &
				"GroupID.", StopSign!)
	HALT CLOSE
End if
is_groupid	=	as_groupid
destroy lu_ws_utl
RETURN as_groupid


end function

public function boolean nf_getaccess (readonly string as_window_class, ref string as_add, ref string as_delete, ref string as_modify, ref string as_inquire);
long     ll_foundrow

ll_foundrow = ids_win_access.find("window_name ='"+ UPPER(as_window_class) +"'",1, ids_win_access.RowCount())
IF ll_foundrow < 1 THEN return False
as_add = ids_win_access.GetItemString(ll_foundrow, 'add_auth')
as_delete = ids_win_access.GetItemString(ll_foundrow, 'delete_auth')
as_modify = ids_win_access.GetItemString(ll_foundrow, 'modify_auth') 
as_inquire = ids_win_access.GetItemString(ll_foundrow, 'inquire_auth') 

Return True


end function

public subroutine nf_savesqlca_settings (string as_inifile);SetProfileString(as_inifile, "PBDNLOAD DATABASE", "dbms", "ODBC")
SetProfileString(as_inifile, "PBDNLOAD DATABASE", "database", "pblocaldb")
SetProfileString(as_inifile, "PBDNLOAD DATABASE", "dbParm", "ConnectString='DSN=pblocaldb;UID=dba;PWD=sql'")
																		

end subroutine

public function boolean nf_getaccess (string as_window_class, ref boolean ab_add, ref boolean ab_modify, ref boolean ab_inquire, ref boolean ab_delete);long     ll_foundrow

ll_foundrow = ids_win_access.find("window_name ='"+ UPPER(as_window_class) +"'",1, ids_win_access.RowCount())
IF ll_foundrow < 1 THEN return False
ab_add = ids_win_access.GetItemString(ll_foundrow, 'add_auth') = 'Y'
ab_delete = ids_win_access.GetItemString(ll_foundrow, 'delete_auth') = 'Y'
ab_modify = ids_win_access.GetItemString(ll_foundrow, 'modify_auth') = 'Y'
ab_inquire = ids_win_access.GetItemString(ll_foundrow, 'inquire_auth') = 'Y'

Return True

end function

public function boolean nf_iswindowaccessable (string as_window_name);// as_window_name is a string of a window name.  This will return false only if the 
// window has permissions set to n n n n in the dw_win_access datawindow
If ids_win_access.Find("window_name = '" + Upper(as_window_name) + &
			"' and compute_See_window = 0", 1, ids_win_access.RowCount()) > 0 Then
	// this was found, no permissions for the window exist
	Return False
Else
	If ids_win_access.Find("window_name = '" + Upper(as_window_name) + &
			"' and compute_See_window = 1", 1, ids_win_access.RowCount()) > 0 Then
	// this was found, no permissions for the window exist
		Return True
	Else
		IF ids_win_access.Find("window_name = 'W_MENU_DEFAULTS' and compute_See_window = 0", 1, ids_win_access.RowCount()) > 0 Then 
		   Return FALSE
	  	ELSE
			return True
		END IF
	END IF
End if
end function

public function boolean nf_getmenuplacement (string as_menuid, ref integer ai_level1, ref integer ai_level2, ref integer ai_level3);Long	ll_row

ll_row = ids_menu_class.Find("menu_class_name = '" + Upper(as_menuid) + "'", &
								1, ids_menu_class.RowCount())

If ll_row < 1 Then return False

ai_level1 = ids_menu_class.GetItemNumber(ll_row, 'menu_place1')
ai_level2 = ids_menu_class.GetItemNumber(ll_row, 'menu_place2')
ai_level3 = ids_menu_class.GetItemNumber(ll_row, 'menu_place3')

return true
end function

on u_netwise_data.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_netwise_data.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;u_download_data	lu_download
lu_download		=	Create u_download_data

is_system_id = Message.nf_get_app_id()

If Len(Trim(is_system_id)) = 0 then
	MessageBox("Error", "No system ID, contact Applications Development")
	Halt Close
End if

is_userid		= SQLCA.UserID
nf_secure_windows()

this.postevent("ue_postconstructor")

destroy lu_download
end event

event destructor;nf_savesqlca_settings(gw_netwise_frame.is_userini)
IF Isvalid (ids_menu_class) THEN Destroy	ids_menu_class
IF Isvalid (ids_win_access) THEN Destroy	ids_win_access
//IF Isvalid (invuo_profile) THEN Destroy	invuo_profile


//******************************************************************************
//**EEM** 06/19/2002 ** 
// This object sets a hook into the applications keyboard event. CTRL+Tab presses
//	are then handled by posting a NEXTWINDOW massage to the Active sheet.
//
// The Destructor of this object (u_netwise_data) must call UnHookKB() 
// or bad things will happen
//******************************************************************************
//** API Call to the IBPKBHook.dll **
UnHookKB()
//******************************************************************************
end event

