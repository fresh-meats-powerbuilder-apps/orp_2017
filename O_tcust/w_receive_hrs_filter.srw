HA$PBExportHeader$w_receive_hrs_filter.srw
forward
global type w_receive_hrs_filter from w_base_response_ext
end type
type dw_recv_window_filter from u_base_dw_ext within w_receive_hrs_filter
end type
end forward

global type w_receive_hrs_filter from w_base_response_ext
int X=942
int Y=376
int Width=965
int Height=772
boolean TitleBar=true
string Title="Receiving Hours"
long BackColor=12632256
dw_recv_window_filter dw_recv_window_filter
end type
global w_receive_hrs_filter w_receive_hrs_filter

type variables
Integer		ii_rc

u_receive_hrs	ui_receive_hrs

DataWindowChild   idwc_division
end variables

event open;call super::open;// 01-15-1997 ibdkkam  Updated for PB 5.0.2  Modified to access 
// nvo instance variables instead of structure variables.

//dw_recv_window_filter.InsertRow(0)

ui_receive_hrs = Message.PowerObjectParm

IF ui_receive_hrs.customer_id[1] <> '' THEN 
	dw_recv_window_filter.SetItem( 1, "customer_id", ui_receive_hrs.customer_id)
END IF

IF ui_receive_hrs.division[1] <> '' THEN 
	dw_recv_window_filter.SetItem( 1, "division_code", ui_receive_hrs.division)
END IF



end event

on w_receive_hrs_filter.create
int iCurrent
call super::create
this.dw_recv_window_filter=create dw_recv_window_filter
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_recv_window_filter
end on

on w_receive_hrs_filter.destroy
call super::destroy
destroy(this.dw_recv_window_filter)
end on

event ue_base_cancel;call super::ue_base_cancel;// 01-15-1997 ibdkkam  Updated for PB 5.0.2  Modified to access 
// nvo instance variables instead of structure.

ui_receive_hrs.cancel = TRUE

CloseWithReturn( This, ui_receive_hrs )

Return
 

end event

event ue_base_ok;call super::ue_base_ok;// 01-15-1997 ibdkkam  Updated for PB 5.0.2  Modified to access 
// nvo instance variables instead of structure.

Boolean   lb_parameter_ind

char      lc_customer_id[7], &
	       lc_division[2]

lb_parameter_ind = FALSE

If dw_recv_window_filter.AcceptText() = 1 Then
   
	lc_customer_id	= dw_recv_window_filter.GetItemString( 1, "customer_id" )
   lc_division = dw_recv_window_filter.GetItemString( 1, "division_code" )

   IF lc_customer_id[1] > " " and not IsNull(lc_customer_id[1]) Then
   	lb_parameter_ind = TRUE
   Else
      dw_recv_window_filter.SetFocus()
      dw_recv_window_filter.SetColumn("customer_id")
      dw_recv_window_filter.SelectText(1, 7)
      MessageBox("Invalid Parameters", "Please enter a Customer I.D.")
      Return
   End if
     
   ui_receive_hrs.customer_id = lc_customer_id
   ui_receive_hrs.division  	= lc_division
   ui_receive_hrs.cancel      = FALSE

   CloseWithReturn( This, ui_receive_hrs )

End if



end event

type cb_base_help from w_base_response_ext`cb_base_help within w_receive_hrs_filter
int X=649
int Y=524
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_receive_hrs_filter
int X=338
int Y=524
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_receive_hrs_filter
int X=32
int Y=524
int TabOrder=20
end type

type dw_recv_window_filter from u_base_dw_ext within w_receive_hrs_filter
int X=142
int Y=96
int Width=699
int Height=336
int TabOrder=10
string DataObject="d_receive_filter"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event constructor;call super::constructor;
this.InsertRow(0)
ii_rc = GetChild( 'division_code', idwc_division )

If ii_rc = -1 Then 
	MessageBox("DataWindwoChild Error", "Unable to get Child Handle for division_code.")
End If

idwc_division.SetTransObject(SQLCA)
idwc_division.Retrieve('DIVCODE')

end event

event itemchanged;call super::itemchanged;long ll_FoundRow
   
If Trim(data) = "" Then Return

If dwo.id = "2" Then
	
	ll_FoundRow = idwc_division.Find ( "type_code='"+data+"'", 1, idwc_division.RowCount() )
   If ll_FoundRow < 1 Then  
   	MessageBox( "Division Error", data + " is Not a Valid Division Code." )
      SetColumn("division_code")
      SetFocus()
      Return 1
	End if
	
End if

end event

event itemerror;call super::itemerror;Return 1
end event

