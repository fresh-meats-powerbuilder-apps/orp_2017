HA$PBExportHeader$smas01sr_programinterfacesmas01pg.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterfaceSmas01pg from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterfaceSmas01pg from nonvisualobject
end type

type variables
    smas01sr_ProgramInterfaceSmas01pgSma000sr_program_container sma000sr_program_container
    boolean structuredContainer
end variables

on smas01sr_ProgramInterfaceSmas01pg.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterfaceSmas01pg.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

