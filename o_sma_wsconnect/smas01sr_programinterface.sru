HA$PBExportHeader$smas01sr_programinterface.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterface from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterface from nonvisualobject
end type

type variables
    smas01sr_ProgramInterfaceSmas01ci smas01ci
    smas01sr_ProgramInterfaceSmas01pg smas01pg
    smas01sr_ProgramInterfaceSmas01in smas01in
    smas01sr_ProgramInterfaceSmas01ot smas01ot
    boolean channel
end variables

on smas01sr_ProgramInterface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

