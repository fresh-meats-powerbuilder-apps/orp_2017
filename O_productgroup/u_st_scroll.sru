HA$PBExportHeader$u_st_scroll.sru
$PBExportComments$Vertical Panel V1.3
forward
global type u_st_scroll from statictext
end type
end forward

global type u_st_scroll from statictext
integer width = 18
integer height = 300
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "SizeWE!"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
event mousemove pbm_mousemove
event lbuttondown pbm_lbuttondown
event lbuttonup pbm_lbuttonup
end type
global u_st_scroll u_st_scroll

type variables
Window		iw_Parent
DragObject	idrg_Left
DragObject	idrg_Right
Integer		ii_Gap
Integer		ii_Offset

Integer		ii_Min
Integer		ii_Max

Boolean		ib_Init = FALSE
Boolean		ib_LButtonDown = FALSE

Integer		ii_LastX

Long			il_WindowColor 
end variables

forward prototypes
public subroutine uf_resize ()
public function integer uf_register (window awi_parent, dragobject adrg_left, dragobject adrg_right, integer ai_gap, integer ai_offset)
end prototypes

event mousemove;
Integer li_X

ii_Max = iw_Parent.Width - 200

//IF ib_lbuttondown AND ib_init THEN
IF ib_lbuttondown THEN	
//	li_X = iwi_Parent.PointerX() - ii_offset
	li_X = iw_Parent.PointerX() 	
	IF li_X >= ii_Min AND li_X <= ii_Max THEN this.X = li_X
END IF

Parent.TriggerEvent ("resize")
end event

event lbuttondown;
ib_lbuttondown = TRUE
This.BackColor = 0
//ii_Min = idrg_Left.X
//ii_Max = idrg_Right.X + idrg_Right.Width

end event

event lbuttonup;
BackColor = il_windowcolor
ib_lbuttondown = FALSE

//uf_resize()
Parent.TriggerEvent ("resize")

end event

public subroutine uf_resize ();
/*======================================================================
Parameters	:	(None)
Description	:	Resize the left and right objects that are registered
Returns		:	(None)
Created By	:	Ken Hwe
==========================Revision History==============================
Updated On	Updated By		Description of Modification
5/5/99		Kwan Fu Sit		Loop through user objects to resize them
========================================================================*/
Integer    li_Diff
Integer    li_Index
UserObject luo_Object
DragObject ldrg_Object

// Find the difference between the original x coordinate and the new
// one so we can adjust the left and right panes

li_Diff = ii_LastX - UnitsToPixels( X, XUnitsToPixels! )

IF li_Diff <> 0 THEN
	
	// If the left object is a user object then we must loop thru each
	// of the controls and adjust the width
	
	If idrg_Left.TypeOf() = UserObject! Then 
		
		// Assign the left object to a user object variable so we can 
		// get at the control array
		
		luo_Object = idrg_Left
		
		// Loop thru each of the controls and adjust the width to match the new
		// width
		
		For li_Index = 1 To UpperBound(luo_Object.Control)
			ldrg_Object = luo_Object.Control[li_Index]
			ldrg_Object.Width = PixelsToUnits( UnitsToPixels( ldrg_Object.Width, XUnitsToPixels! ) - li_Diff, XPixelsToUnits! )		
		Next
	End If
		
	// Adjust the left side object's width to match the new width size
	
	idrg_Left.Width = PixelsToUnits( UnitsToPixels( idrg_Left.Width, XUnitsToPixels! ) - li_Diff, XPixelsToUnits! )

	// Check if the right side object is a user object and if so then resize
	// each object within the user object

	If idrg_Right.TypeOf() = UserObject! Then
		
		// Assign the user object to a local variable so we can access
		// the control array
		
		luo_Object = idrg_Right
		
		// Loop thru each control and resize the object to the new width and
		// x  values
		
		For li_Index = 1 To UpperBound(luo_Object.Control)
			ldrg_Object = luo_Object.Control[li_Index]
			ldrg_Object.X = PixelsToUnits( UnitsToPixels( ldrg_Object.X, XUnitsToPixels! ) - &
								 li_Diff, XPixelsToUnits! )
			ldrg_Object.Width = PixelsToUnits( UnitsToPixels( ldrg_Object.Width, XUnitsToPixels! ) + &
									  li_Diff, XPixelsToUnits! )
		Next
	End If
		
	// Resize the right side object
	
	idrg_Right.X = PixelsToUnits( UnitsToPixels( idrg_Right.X, XUnitsToPixels! ) - li_Diff, XPixelsToUnits! )
	idrg_Right.Width = PixelsToUnits( UnitsToPixels( idrg_Right.Width, XUnitsToPixels! ) + li_Diff, XPixelsToUnits! )
	
	// Save the new x position
	
	ii_LastX = UnitsToPixels( X, XUnitsToPixels! )
END IF
end subroutine

public function integer uf_register (window awi_parent, dragobject adrg_left, dragobject adrg_right, integer ai_gap, integer ai_offset);
//Integer li_Right, li_Left, li_I, li_J
//
//// Check objcts are aligned ok.
//IF adrg_Left.X > adrg_Right.X THEN RETURN -1
//
//// Copy the arguments to instance vars
//idrg_Right = adrg_Right
//idrg_Left = adrg_Left
//ii_offset = ai_offset
//IF ai_Gap < 7 THEN ii_Gap = 7 ELSE ii_gap = ai_Gap
//iwi_Parent = awi_Parent
//
//// Say we are registered and ready for use
//ib_Init = TRUE
//
//// Adjust the objects to match the gap.
//li_Left = UnitsToPixels( idrg_Left.X, XUnitsToPixels! ) + &
//	UnitsToPixels( idrg_Left.Width, XUnitsToPixels! )
//li_Right = UnitsToPixels( idrg_Right.X, XUnitsToPixels! )
//
//li_I = li_Right - li_Left
//IF li_I > ii_Gap THEN
//	li_I -= ii_Gap + 1
//	li_J = li_I / 2
//
//	// Should always be > 0 but I'll check anyway
//	IF li_J > 0 THEN
//		// Adjust height to use extra space.
//		idrg_Left.Width = PixelsToUnits( UnitsToPixels( idrg_Left.Width, XUnitsToPixels! ) + li_J, XPixelsToUnits! )
//		li_J = li_I - li_J
//
//		// Might be zero
//		IF li_J > 0 THEN
//			// Adjust Height + Y to fill extra space.
//			idrg_Right.Width = PixelsToUnits( UnitsToPixels( idrg_Right.Width, XUnitsToPixels! ) + li_J, XPixelsToUnits! )
//			idrg_Right.X = PixelsToUnits( UnitsToPixels( idrg_Right.X, XUnitsToPixels! ) - li_J, XPixelsToUnits! )
//		END IF
//	END IF
//ELSEIF li_I < ii_Gap THEN
//	li_I = ii_Gap - li_I
//	li_J = li_I / 2
//
//	// Should always be > 0 but I'll check anyway
//	IF li_J > 0 THEN
//		// Adjust height to use less space.
//		idrg_Left.Width = PixelsToUnits( UnitsToPixels( idrg_Left.Width, XUnitsToPixels! ) - li_J, XPixelsToUnits! )
//		li_J = li_I - li_J
//
//		// Might be zero
//		IF li_J > 0 THEN
//			// Adjust Height + Y to use less space.
//			idrg_Right.Width = PixelsToUnits( UnitsToPixels( idrg_Right.Width, XUnitsToPixels! ) - li_J, XPixelsToUnits! )
//			idrg_Right.X = PixelsToUnits( UnitsToPixels( idrg_Right.X, XUnitsToPixels! ) + li_J, XPixelsToUnits! )
//		END IF
//	END IF
//END IF
//
//// Adjust the size of this object.
//Width = PixelsToUnits( ii_Gap, XPixelsToUnits! )
//ii_LastX = UnitsToPixels( idrg_Left.X, XUnitsToPixels! ) + &
//	UnitsToPixels( idrg_Left.Width, XUnitsToPixels! )
//X = PixelsToUnits( ii_LastX, XPixelsToUnits! )
//
//IF adrg_Left.Y < adrg_Right.Y THEN
//	Y = adrg_Right.Y
//ELSE
//	Y = adrg_Left.Y
//END IF
//
//IF adrg_Left.Height + adrg_Left.Y < adrg_Right.Height + adrg_Right.Y THEN
//	Height = adrg_Left.Height + adrg_Left.Y - Y
//ELSE
//	Height = adrg_Right.Height + adrg_Right.Y - Y
//END IF
//
//this.SetPosition( ToTop! )
//
RETURN 0

end function

event constructor;il_WindowColor = 12632256

ii_Min = 200
iw_Parent = This.GetParent()
ii_Max = iw_Parent.Width - 200


end event

on u_st_scroll.create
end on

on u_st_scroll.destroy
end on

