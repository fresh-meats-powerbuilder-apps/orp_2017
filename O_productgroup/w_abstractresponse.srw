HA$PBExportHeader$w_abstractresponse.srw
forward
global type w_abstractresponse from Window
end type
type cb_help from commandbutton within w_abstractresponse
end type
type cb_cancel from commandbutton within w_abstractresponse
end type
type cb_ok from commandbutton within w_abstractresponse
end type
end forward

global type w_abstractresponse from Window
int X=1056
int Y=484
int Width=3031
int Height=1576
boolean TitleBar=true
string Title="WLS Response"
long BackColor=79741120
boolean ControlMenu=true
WindowType WindowType=response!
event ue_postopen ( )
event ue_close ( )
cb_help cb_help
cb_cancel cb_cancel
cb_ok cb_ok
end type
global w_abstractresponse w_abstractresponse

on w_abstractresponse.create
this.cb_help=create cb_help
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.Control[]={this.cb_help,&
this.cb_cancel,&
this.cb_ok}
end on

on w_abstractresponse.destroy
destroy(this.cb_help)
destroy(this.cb_cancel)
destroy(this.cb_ok)
end on

event open;// Post an event that handles post-opening processing
PostEvent("ue_postopen")

end event

type cb_help from commandbutton within w_abstractresponse
int X=795
int Y=1268
int Width=247
int Height=108
int TabOrder=30
string Text="&Help"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_cancel from commandbutton within w_abstractresponse
int X=425
int Y=1268
int Width=279
int Height=108
int TabOrder=20
string Text="&Cancel"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from commandbutton within w_abstractresponse
int X=91
int Y=1268
int Width=247
int Height=108
int TabOrder=10
string Text="&OK"
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

