HA$PBExportHeader$u_grpmaintenance.sru
forward
global type u_grpmaintenance from nonvisualobject
end type
end forward

global type u_grpmaintenance from nonvisualobject
end type
global u_grpmaintenance u_grpmaintenance

type variables
Private:
u_ProdGrpList		ilv_list
u_ProdGrpTree		itv_tree
w_groupmaintenance	iw_grpmaint
u_olecom			iu_olepfm
long			il_treehandle, &
			il_currenthandle, &
			il_ownerhandle, &
			il_prdgrpind
String			is_updateowner, &
			is_copytext, &
			is_selected_owner, &
			is_currentlabel
Boolean			ib_newgroup

end variables

forward prototypes
public function boolean uf_inquire ()
public subroutine uf_resize (window aw_window)
public function boolean uf_new ()
public subroutine uf_delete ()
public function boolean uf_initialize (listview alv, treeview atv, window aw)
public subroutine uf_tl_populate_grps (string as_owner, long al_handle)
public subroutine uf_tl_selectionchange (long al_handle)
public subroutine uf_tl_newprodgrp ()
public subroutine uf_list_groups (string as_owner)
public function string uf_getproddescr (string as_product)
public subroutine uf_list_products (long al_prodgrp)
public subroutine uf_load_root ()
public function string uf_getstring (string as_type)
public function string uf_getserver ()
public function boolean uf_tl_delete (long al_prodgrp)
public subroutine uf_pointerxy (ref long pointerx, ref long pointery)
public subroutine uf_tl_cpyprodgrp ()
public function boolean uf_initializepfm ()
public subroutine uf_setmicrohelp (string as_input)
public function boolean uf_update_db (string as_option, string as_type, string as_data, string as_products)
public subroutine uf_expandupdateowner (long al_handle)
public function long uf_getownerhandle ()
public subroutine uf_copygroup (string as_description, long al_prdgrpind)
public function boolean uf_deletegroup (long al_prodgrp)
public subroutine uf_setprdgrpind (long al_prdgrpind)
public subroutine uf_setprdnewgrpind (boolean ab_newgrpind)
public function boolean uf_checkfordupdescr (string as_descr)
end prototypes

public function boolean uf_inquire ();Long		ll_handle


SetPointer(HourGlass!)

// reload root level (sales, sched, smacc, etc.)
uf_load_root()
ll_handle = il_ownerhandle
// clear list view
ilv_list.EVENT DYNAMIC ue_clear()

SetPointer(HourGlass!)
// retrieve product groups
iu_olepfm.Retrieve()

uf_expandupdateowner(ll_handle)

SetPointer(Arrow!)

Return True
end function

public subroutine uf_resize (window aw_window);ilv_list.Resize(aw_window.width - ilv_list.x - 50, aw_window.height - ilv_list.y - 110)
itv_tree.Resize(1193, aw_window.height - itv_tree.y - 110)
end subroutine

public function boolean uf_new ();
uf_tl_newprodgrp()

Return True
end function

public subroutine uf_delete ();//ilv_list.EVENT ue_delete()
end subroutine

public function boolean uf_initialize (listview alv, treeview atv, window aw);
ilv_list = alv
itv_tree = atv
iw_grpmaint = aw

IF NOT ilv_list.uf_initialize(atv,This) Then Return False
IF NOT itv_tree.uf_initialize(alv,This) Then Return False

Return True

end function

public subroutine uf_tl_populate_grps (string as_owner, long al_handle);treeviewitem	ltvi_child
Long		 		ll_count, ll_index, ll_pic_idx , ll_handle


Choose Case String(as_owner)
	Case "SALES"
		ll_pic_idx = 5
	Case "SCHED"
		ll_pic_idx = 6
	Case "SMACC"
		ll_pic_idx = 7
	Case Else
		ll_pic_idx = 4
End Choose
ltvi_child.PictureIndex = ll_pic_idx
ltvi_child.SelectedPictureIndex = 2
ltvi_child.Children = False

iu_olepfm.Filter(as_owner)
ll_count = iu_olepfm.Count()

FOR ll_index = 1 to ll_count
	ltvi_child.Label = iu_olepfm.Item(ll_index).description
	ltvi_child.Data = ll_index
	ll_handle = itv_tree.InsertItemLast(al_handle,ltvi_child)
	If ltvi_child.label = is_currentlabel Then
		il_currenthandle = ll_handle
	End if
NEXT

end subroutine

public subroutine uf_tl_selectionchange (long al_handle);treeviewitem	ltvi_item, ltvi_parent
String			ls_owner
Long				ll_parent_handle


SetPointer(HourGlass!)

il_treehandle = al_handle

ilv_list.EVENT DYNAMIC ue_clear()

itv_tree.GetItem(al_handle,ltvi_item)

CHOOSE CASE ltvi_item.Level		
	CASE 1			
		uf_list_groups(ltvi_item.Data)
	CASE 2		
		ll_parent_handle = itv_tree.FindItem (ParentTreeItem!, al_handle)
		itv_tree.GetItem(ll_parent_handle,ltvi_parent)
		ls_owner = ltvi_parent.Data
		//need to filter by parent.data
		iu_olepfm.Filter(ls_owner)
		
		uf_list_products(ltvi_item.Data)
	Case 3
		//ilv_list.EVENT DYNAMIC ue_product_descr(ltvi_item.Data)
END CHOOSE

SetPointer(Arrow!)
end subroutine

public subroutine uf_tl_newprodgrp ();String			ls_rtn


ib_newgroup = True
il_prdgrpind = 0

OpenWithParm(w_groupmaintdef, This)
ls_rtn = Message.StringParm

itv_tree.FindItem(CurrentTreeItem!, il_treehandle)
If ls_rtn = "OK" Then 
	uf_inquire()
End If

end subroutine

public subroutine uf_list_groups (string as_owner);String			ls_label
Long				ll_pic_idx, &
					ll_count, &
					ll_index, &
					ll_handle, &
					ll_listindex
listviewitem 	llvi_product


// Add column headers for the Report view
ilv_list.AddColumn ("Product Group Description", Left!, 1150)
ilv_list.AddColumn ("User Id", Left!, 250)
ilv_list.AddColumn ("Date Updated", Left!, 375)

Choose Case String(as_owner)
	Case "SALES"
		ll_pic_idx = 5
	Case "SCHED"
		ll_pic_idx = 6
	Case "SMACC"
		ll_pic_idx = 7
	Case Else
		ll_pic_idx = 4
End Choose

iu_olepfm.Filter(as_owner)
ll_count = iu_olepfm.Count()

FOR ll_index = 1 TO ll_count
	ls_label = iu_olepfm.Item(ll_index).description + "~t" + &
						iu_olepfm.Item(ll_index).UpdateUserId + "~t" + &
						iu_olepfm.Item(ll_index).UpdateDate
	
	llvi_product.Label = ls_label
	llvi_product.Data = ll_index
	llvi_product.PictureIndex = ll_pic_idx
	
	ll_listindex = ilv_list.AddItem(llvi_product)
	If Trim(is_currentlabel) = Trim(llvi_product.label) Then
//		il_currentidex = ll_listindex
	End If
	
NEXT

ilv_list.uf_setdata(1,0)
end subroutine

public function string uf_getproddescr (string as_product);String		ls_descr


Select long_description
Into :ls_descr
From sku_products
Where sku_product_code = :as_product
Using SQLCA;

If SQLCA.SQLCode <> 0 THEN ls_descr = ""

Return ls_descr
end function

public subroutine uf_list_products (long al_prodgrp);Long 				ll_count, ll_index
String 			ls_label, ls_id, ls_descr, ls_product
listviewitem 	llvi_product


ll_count = iu_olepfm.item(al_prodgrp).count

// Add column headers for the Report view
ilv_list.AddColumn ("SKU Product", Left!, 500)
ilv_list.AddColumn ("SKU Description", Left!, 1500)

FOR ll_index = 1 TO ll_count
	
	ls_product = iu_olepfm.item(al_prodgrp).item(ll_index).ProductCode
	ls_descr = uf_getproddescr(ls_product)
	ls_label = ls_product + "~t" + ls_descr
	
	llvi_product.Label = ls_label
	llvi_product.Data = ll_index
	llvi_product.PictureIndex = 2
	
	ilv_list.AddItem (llvi_product)
	
NEXT

ilv_list.uf_setdata(2,al_prodgrp)

end subroutine

public subroutine uf_load_root ();Long					ll_rowcount, ll_index, ll_handle
TreeViewItem		ltvi_ownergrp
DataStore			lds_root_items
String				ls_data


// deleting tree
ll_handle = itv_tree.FindItem(RootTreeItem!, 0)
Do While ll_handle <> -1
	itv_tree.DeleteItem(ll_handle)
	ll_handle = itv_tree.FindItem(RootTreeItem!, 0)
Loop

lds_root_items =  Create DataStore
lds_root_items.DataObject = "d_ownergroup"
lds_root_items.Reset()
lds_root_items.SetTransObject(SQLCA)
ll_rowcount = lds_root_items.Retrieve()

ltvi_ownergrp.PictureIndex = 1
ltvi_ownergrp.SelectedPictureIndex = 2

//	Cycle through to populate owners
FOR ll_index = 1 TO ll_rowcount
	
	ltvi_ownergrp.Label = String(lds_root_items.object.type_short_desc[ll_index])
	ls_data = String(lds_root_items.object.type_code[ll_index])
	ltvi_ownergrp.Data = ls_data
	ltvi_ownergrp.Children = TRUE
	
	ll_handle = itv_tree.InsertItemSort(0,ltvi_ownergrp)
	
	If ls_data = is_updateowner Then
		il_ownerhandle = ll_handle
	End If
	
NEXT

end subroutine

public function string uf_getstring (string as_type);String		ls_string
Long			ll_index, ll_count

Choose Case as_type
	Case "DESCRIPTION"
		Return iu_olepfm.Item(il_prdgrpind).Description
		
	Case "PRODUCTS"
		ls_string = ""
		If il_prdgrpind > 0 Then
			ll_count = iu_olepfm.item(il_prdgrpind).count
			For ll_index = 1 To ll_count
				ls_string += iu_olepfm.Item(il_prdgrpind).Item(ll_index).ProductCode + "~t~r~n"
			Next
		End If
		Return ls_string
		
	Case "OWNER"
		Return is_updateowner
		
	Case "ADDMOD"
		If ib_newgroup Then
			Return "NEW"
		Else
			Return "MOD"
		End If
		
	Case "COPYTEXT"
		Return is_copytext
		
	Case Else
		Return ""
		
End Choose
end function

public function string uf_getserver ();String	ls_return, ls_filename


//ls_filename = iw_grpmaint.wf_get_working_dir() + "ibp002.ini"
ls_filename = "ibp002.ini"

ls_return = ProfileString (ls_filename, "Netwise Server Info", "ServerPrefix", "dkmvs00.")
ls_return += "pfm001"
ls_return +=ProfileString (ls_filename, "Netwise Server Info", "ServerSuffix", "t")


Return ls_return
end function

public function boolean uf_tl_delete (long al_prodgrp);treeviewitem	ltvi_root
String			ls_label, ls_id, ls_rtn, ls_ownergrp
Integer			li_pic_idx, li_prodsetid
long				ll_handle


ll_handle = itv_tree.FindItem(ParentTreeItem!, il_treehandle)
If ll_handle = -1 Then
	ll_handle = itv_tree.FindItem(CurrentTreeItem!, 0)
End If
itv_tree.GetItem(ll_handle, ltvi_root)

ls_ownergrp = String(ltvi_root.Data)

If ls_ownergrp = is_updateowner Then
	
	iu_olepfm.item(al_prodgrp).Delete
	uf_inquire()
	Return True
	
Else
	
	uf_setmicrohelp("Unable to delete this product group")
	Return False
	
End If

end function

public subroutine uf_pointerxy (ref long pointerx, ref long pointery);iw_grpmaint.wf_pointerposition(pointerx, pointery)
end subroutine

public subroutine uf_tl_cpyprodgrp ();treeviewitem	ltvi_root
String			ls_open, ls_ownergrp
Integer			li_prodsetid, &
					li_index
long				ll_handle


ll_handle = itv_tree.FindItem(ParentTreeItem!, il_currenthandle)
If ll_handle = -1 Then
	ll_handle = itv_tree.FindItem(CurrentTreeItem!, 0)
End If
itv_tree.GetItem(ll_handle,ltvi_root)

ls_ownergrp = String(ltvi_root.Data)

il_prdgrpind = ilv_list.uf_get_index()

If il_prdgrpind <= 0 Then
	uf_setmicrohelp("No Product Group Has Focus Please Select A Product Group")
	//MessageBox("No Product Group Has Focus","Please Select A Product Group")
	Return
End If

//If ls_ownergrp = is_updateowner Then
//	// must specify new name
//	is_copytext = "New Name Is Required"
//	ib_newgroup = True
//Else
//	// can use same name
//	is_copytext = "New Name Is NOT Required"
//	ib_newgroup = False
//End If

OpenWithParm(w_groupmaintcpy, This)
ls_open = Message.StringParm

If Len(Trim(ls_open)) > 0 Then 
	iu_olepfm.item(il_prdgrpind).Copy(ls_open, is_updateowner)
	uf_inquire()
Else
	uf_setmicrohelp("No Product Group Created")
End If
end subroutine

public function boolean uf_initializepfm ();String		ls_compare, ls_4compare, ls_user_id, ls_user_pw, ls_app_name, ls_server
Integer		li_ret

SetPointer(HourGlass!)

iu_olepfm = Create u_olecom

li_ret = iu_olepfm.ConnectToNewObject("ProductGroupMaintenance.ProductGroupList.1")

If li_ret < 0 Then
	MessageBox("Couldn't Connect", "ConnectToNewObject returned: " + String(li_ret))
	SetPointer(Arrow!)
	Return False
End If

ls_app_name = GetApplication().AppName
ls_user_id = iw_grpmaint.wf_get_user_id()
ls_server = uf_getserver()
ls_user_pw = iw_grpmaint.wf_get_user_pw()

iu_olepfm.Initialize(ls_user_id, ls_user_pw, ls_server)

Choose Case ls_app_name
	Case "pas" 
		is_updateowner = "SCHED"
	Case "orp" 
		is_updateowner = "SALES"
	Case "sma"
		is_updateowner = "SMACC"
	Case Else
		is_updateowner = "APPER"
End Choose

SetPointer(Arrow!)
Return True
end function

public subroutine uf_setmicrohelp (string as_input);iw_grpmaint.wf_setmicrohelp(as_input)
end subroutine

public function boolean uf_update_db (string as_option, string as_type, string as_data, string as_products);Boolean		lb_rtn

lb_rtn = True

Choose Case as_option
	Case "MOD"
		Choose Case as_type
			Case "H"
				iu_olepfm.item(il_prdgrpind).description = as_data
			Case "P"
				iu_olepfm.item(il_prdgrpind).AddMany(as_data)
			Case "D"
				iu_olepfm.item(il_prdgrpind).RemoveMany(as_data)
		End Choose
		
	CASE "NEW"
		Choose Case as_type
			Case "H"
				iu_olepfm.Add(as_data, is_updateowner).AddMany(as_products)
				//il_prdgrpind = iu_olepfm.count
			Case "P"
				//iu_olepfm.item(il_prdgrpind).AddMany(as_data)
			Case "D"
				// No Need To Do Anything
		End Choose
		
End Choose

Return lb_rtn
end function

public subroutine uf_expandupdateowner (long al_handle);// set focus on for is_updateowner


itv_tree.SelectItem(al_handle)

itv_tree.ExpandItem(al_handle)

uf_tl_selectionchange(al_handle)
end subroutine

public function long uf_getownerhandle ();Return il_ownerhandle
end function

public subroutine uf_copygroup (string as_description, long al_prdgrpind);SetPointer(hourglass!)
iu_olepfm.item(al_prdgrpind).Copy(as_description, is_updateowner)
uf_inquire()

end subroutine

public function boolean uf_deletegroup (long al_prodgrp);
iu_olepfm.item(al_prodgrp).Delete
uf_inquire()

Return True
	

end function

public subroutine uf_setprdgrpind (long al_prdgrpind);il_prdgrpind = al_prdgrpind
end subroutine

public subroutine uf_setprdnewgrpind (boolean ab_newgrpind);ib_newgroup = ab_newgrpind
end subroutine

public function boolean uf_checkfordupdescr (string as_descr);u_TreeViewFunctions		lu_TreeFunctions

String						ls_return


If Not lu_TreeFunctions.uf_checkfordupdescriptions(as_descr, il_ownerhandle, itv_tree) Then 
	Return False
End If

Return True
end function

on u_grpmaintenance.create
TriggerEvent( this, "constructor" )
end on

on u_grpmaintenance.destroy
TriggerEvent( this, "destructor" )
end on

