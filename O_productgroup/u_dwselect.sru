HA$PBExportHeader$u_dwselect.sru
$PBExportComments$This object will control the single row selection or multiple row selection of a datawindow
forward
global type u_dwselect from nonvisualobject
end type
end forward

global type u_dwselect from nonvisualobject autoinstantiate
end type

type variables
Private:
DataWindow	idw_Data
Integer		ii_SelectionType
Long		il_LastClickedRow, &
		il_selected
end variables

forward prototypes
public function boolean uf_initialize (ref datawindow adw_data, integer ai_selectiontype)
public subroutine uf_select (long al_row)
public subroutine uf_setselectiontype (integer ai_selectiontype)
public function integer uf_getselectiontype ()
public subroutine uf_select (long al_row, long al_max)
end prototypes

public function boolean uf_initialize (ref datawindow adw_data, integer ai_selectiontype);
/* --------------------------------------------------------

<DESC>	Initialize the object
</DESC>

<ARGS>	DataWindow:DataWindow,
			SelectionType:Integer
</ARGS>
			
<USAGE>	Call this function to Initialize the object
			Valid selection types are:
			0 - Do not select any rows
			1 - Select only one row
			2 - Select multiple rows, one at a time
			3 - Select multiple rows with CTRL and SHIFT support for blocks
			4 - Select multiple rows with SHIFT support for blocks
</USAGE>
-------------------------------------------------------- */
idw_Data = adw_Data
ii_SelectionType = ai_SelectionType
return True
end function

public subroutine uf_select (long al_row);
/* --------------------------------------------------------

<DESC>	Select various rows on the DataWindow
</DESC>

<ARGS>	al_row:Long
</ARGS>
			
<USAGE>	Call this function whenever the row changes focus on the datawindow
</USAGE>
-------------------------------------------------------- */
// al_row is the current row
Int	li_idx

Choose Case ii_selectiontype
	Case 0
		Return
	Case 1
		idw_data.SelectRow(0, FALSE)
		idw_data.SelectRow(al_row, True)
	Case 2
		// If the row is currently selected, deselect it, otherwise, select it
		idw_data.SelectRow(al_row, Not idw_data.IsSelected(al_row))
	Case 3
		If KeyDown(KeyShift!) Then
			idw_data.SetRedraw(FALSE)
			idw_data.SelectRow(0, FALSE)

			If il_LastClickedRow > al_row Then
				For li_idx = il_LastClickedRow To al_row STEP -1
					idw_data.SelectRow(li_idx, TRUE)	
				Next
			Elseif il_LastClickedRow = 0 Then
				idw_data.SelectRow(al_row, TRUE)	
			Else
				For li_idx = il_LastClickedRow To al_row
					idw_data.SelectRow(li_idx, TRUE)	
				Next	
			End If
			idw_data.SetRedraw(TRUE)
			Return
		End If
		If Keydown(KeyControl!) Then
			If idw_data.IsSelected(al_row) Then
				idw_data.SelectRow(al_row, FALSE)
			Else
				idw_data.SelectRow(al_row, TRUE)
			End If
		Else
			idw_data.SelectRow(0, FALSE)
			idw_data.SelectRow(al_row, TRUE)
		End If

		il_LastClickedRow = al_row
	Case 4
		If KeyDown(KeyShift!) Then
			idw_data.SetRedraw(FALSE)
			idw_data.SelectRow(0, FALSE)

			If il_LastClickedRow > al_row Then
				For li_idx = il_LastClickedRow To al_row STEP -1
					idw_data.SelectRow(li_idx, TRUE)	
				End For
			Elseif il_LastClickedRow = 0 Then
				idw_data.SelectRow(al_row, TRUE)	
			Else
				For li_idx = il_LastClickedRow To al_row
					idw_data.SelectRow(li_idx, TRUE)	
				Next	
			End If
			idw_data.SetRedraw(TRUE)
			Return
		End If
			If idw_data.IsSelected(al_row) Then
			idw_data.SelectRow(al_row, FALSE)
		Else
			idw_data.SelectRow(al_row, TRUE)
		End If
		il_LastClickedRow = al_row
End Choose
return 
end subroutine

public subroutine uf_setselectiontype (integer ai_selectiontype);
/* --------------------------------------------------------

<DESC>	Set the current selection type
</DESC>

<ARGS>	SelectionType:Integer
</ARGS>
			
<USAGE>	Call this function to change the current selection type
			Valid selection types are:
			0 - Do not select any rows
			1 - Select only one row
			2 - Select multiple rows, one at a time
			3 - Select multiple rows with CTRL and SHIFT support for blocks
			4 - Select multiple rows with SHIFT support for blocks
</USAGE>
-------------------------------------------------------- */
ii_selectiontype = ai_SelectionType
end subroutine

public function integer uf_getselectiontype ();
/* --------------------------------------------------------

<DESC>	Get the current selection type
			Valid selection types are:
			0 - Do not select any rows
			1 - Select only one row
			2 - Select multiple rows, one at a time
			3 - Select multiple rows with CTRL and SHIFT support for blocks
			4 - Select multiple rows with SHIFT support for blocks
</DESC>

<ARGS>	(none)</ARGS>
			
<USAGE>	Call this function to determine the current
			selection type
</USAGE>
-------------------------------------------------------- */
return ii_selectiontype
end function

public subroutine uf_select (long al_row, long al_max);
/* --------------------------------------------------------

<DESC>	Select various rows on the DataWindow
</DESC>

<ARGS>	al_row:Long
<ARGS>	al_max:Long
</ARGS>
			
<USAGE>	Call this function whenever the row changes focus on the datawindow.
<USAGE>	The max is if you have a maximum rows that can be selected.
</USAGE>
-------------------------------------------------------- */
// al_row is the current row
Int	li_idx

Choose Case ii_selectiontype
	Case 0
		Return
	Case 1
		idw_data.SelectRow(0, FALSE)
		idw_data.SelectRow(al_row, True)
	Case 2
		// If the row is currently selected, deselect it, otherwise, select it
		idw_data.SelectRow(al_row, Not idw_data.IsSelected(al_row))
	Case 3
		If KeyDown(KeyShift!) Then
			idw_data.SetRedraw(FALSE)
			idw_data.SelectRow(0, FALSE)
			il_selected = 0

			If il_LastClickedRow > al_row Then
				For li_idx = il_LastClickedRow To al_row STEP -1
					IF il_selected < al_max Then
						idw_data.SelectRow(li_idx, TRUE)	
						il_selected ++
					End If
				Next
			Elseif il_LastClickedRow = 0 Then
				IF il_selected < al_max Then
					idw_data.SelectRow(li_idx, TRUE)	
					il_selected ++
				End If
			Else
				For li_idx = il_LastClickedRow To al_row
					IF il_selected < al_max Then
						idw_data.SelectRow(li_idx, TRUE)	
						il_selected ++
					End If
				Next	
			End If
			idw_data.SetRedraw(TRUE)
			Return
		End If
		If Keydown(KeyControl!) Then
			If idw_data.IsSelected(al_row) Then
				idw_data.SelectRow(al_row, FALSE)
				il_selected --
			Else
				IF il_selected < al_max Then
					idw_data.SelectRow(al_row, TRUE)	
					il_selected ++
				End If
			End If
		Else
			idw_data.SelectRow(0, FALSE)
			idw_data.SelectRow(al_row, TRUE)
			il_selected = 1
		End If

		il_LastClickedRow = al_row
	Case 4
		If KeyDown(KeyShift!) Then
			idw_data.SetRedraw(FALSE)
			idw_data.SelectRow(0, FALSE)
			il_selected = 0

			If il_LastClickedRow > al_row Then
				For li_idx = il_LastClickedRow To al_row STEP -1
					IF il_selected < al_max Then
						idw_data.SelectRow(li_idx, TRUE)	
						il_selected ++
					End If
				End For
			Elseif il_LastClickedRow = 0 Then
				IF il_selected < al_max Then
					idw_data.SelectRow(al_row, TRUE)	
					il_selected ++
				End If
			Else
				For li_idx = il_LastClickedRow To al_row
					IF il_selected < al_max Then
						idw_data.SelectRow(li_idx, TRUE)	
						il_selected ++
					End If
				Next	
			End If
			idw_data.SetRedraw(TRUE)
			Return
		End If
			If idw_data.IsSelected(al_row) Then
			idw_data.SelectRow(al_row, FALSE)
			il_selected --
		Else
			IF il_selected < al_max Then
				idw_data.SelectRow(al_row, TRUE)	
				il_selected ++
			End If
		End If
		il_LastClickedRow = al_row
End Choose
return 
end subroutine

on u_dwselect.create
TriggerEvent( this, "constructor" )
end on

on u_dwselect.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_DWSelect

<OBJECT>	This object is a utility object for selecting
			rows in a DataWindow. 
			The valid selection methods are:
			0 - Do not select any rows
			1 - Select only one row
			2 - Select multiple rows, one at a time
			3 - Select multiple rows with CTRL and SHIFT support for blocks
			4 - Select multiple rows with SHIFT support for blocks
			</OBJECT>
			
<USAGE>	Create this object and call uf_initialize().
 			The first parameter to uf_initialize is the 
			 DataWindow to act upon.  The second parameter
			 is the selection method.</USAGE>

<AUTH>	Tim Bornholtz	</AUTH>

--------------------------------------------------------- */

end event

