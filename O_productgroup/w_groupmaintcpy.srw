HA$PBExportHeader$w_groupmaintcpy.srw
forward
global type w_groupmaintcpy from w_abstractresponse
end type
type sle_1 from singlelineedit within w_groupmaintcpy
end type
type st_2 from statictext within w_groupmaintcpy
end type
type st_1 from statictext within w_groupmaintcpy
end type
end forward

global type w_groupmaintcpy from w_abstractresponse
int X=439
int Y=352
int Width=1115
int Height=660
boolean TitleBar=true
string Title="Creating New Product Group"
sle_1 sle_1
st_2 st_2
st_1 st_1
end type
global w_groupmaintcpy w_groupmaintcpy

type variables
Boolean			save 
String			is_original, is_option

end variables

on w_groupmaintcpy.create
int iCurrent
call super::create
this.sle_1=create sle_1
this.st_2=create st_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.st_1
end on

on w_groupmaintcpy.destroy
call super::destroy
destroy(this.sle_1)
destroy(this.st_2)
destroy(this.st_1)
end on

event open;call super::open;//iu_grpmaint = Message.PowerObjectParm

is_original = Message.StringParm


end event

event ue_postopen;String			ls_text


This.SetRedraw(False)

//If isValid(iu_grpmaint) Then
//	is_original = iu_grpmaint.uf_getstring("DESCRIPTION")
//	is_option = iu_grpmaint.uf_getstring("ADDMOD")
//	ls_text = iu_grpmaint.uf_getstring("COPYTEXT")
//Else
//	MessageBox("Error","invalid parameters")
//End If

sle_1.Text = Trim(is_original)

sle_1.SelectText(1, Len(Trim(is_original)) )
sle_1.SetFocus()

This.SetRedraw(True)
end event

type cb_help from w_abstractresponse`cb_help within w_groupmaintcpy
int X=1125
int Y=396
int Width=238
int TabOrder=0
string Text="Help"
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_groupmaintcpy
int X=722
int Y=384
int TabOrder=30
boolean Cancel=true
end type

event cb_cancel::clicked;CloseWithReturn(Parent, "")
end event

type cb_ok from w_abstractresponse`cb_ok within w_groupmaintcpy
int X=421
int Y=384
int Width=279
int TabOrder=20
boolean Default=true
end type

event cb_ok::clicked;String	ls_return


//IF sle_1.AcceptText() = -1 THEN
//	Return
//END IF

ls_return = sle_1.Text

//If trim(ls_return) = trim(is_original) Then
//	MessageBox("Error","Description must be different than original")
//	Return
//End If

CloseWithReturn(Parent, ls_return)
	
end event

type sle_1 from singlelineedit within w_groupmaintcpy
int X=46
int Y=232
int Width=1010
int Height=88
int TabOrder=10
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
int Limit=40
TextCase TextCase=Upper!
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_groupmaintcpy
int X=46
int Y=168
int Width=311
int Height=56
boolean Enabled=false
boolean BringToTop=true
string Text="Description:"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_groupmaintcpy
int X=46
int Y=48
int Width=997
int Height=84
boolean Enabled=false
boolean BringToTop=true
string Text="Enter Name For New Product Group"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=67108864
int TextSize=-10
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

