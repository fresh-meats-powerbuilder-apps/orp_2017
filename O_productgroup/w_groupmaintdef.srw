HA$PBExportHeader$w_groupmaintdef.srw
forward
global type w_groupmaintdef from w_abstractresponse
end type
type dw_products from u_abstractdw within w_groupmaintdef
end type
type dw_header from u_abstractdw within w_groupmaintdef
end type
type dw_selected_prod from u_abstractdw within w_groupmaintdef
end type
type cb_select_prod from commandbutton within w_groupmaintdef
end type
type cb_remove from commandbutton within w_groupmaintdef
end type
type cb_remove_all from commandbutton within w_groupmaintdef
end type
end forward

global type w_groupmaintdef from w_abstractresponse
int X=14
int Y=40
int Width=2482
boolean TitleBar=true
string Title="Product Group Maintenance Definition"
dw_products dw_products
dw_header dw_header
dw_selected_prod dw_selected_prod
cb_select_prod cb_select_prod
cb_remove cb_remove
cb_remove_all cb_remove_all
end type
global w_groupmaintdef w_groupmaintdef

type variables
Boolean	save 
Long	il_CurrentRowSelected, il_grp_id

String	is_WhatWasTyped, is_owner, is_option
Integer	ii_keysTyped = 2

u_grpmaintenance		iu_grpmaint
end variables

forward prototypes
public function boolean wf_update_header ()
public function boolean wf_update_products ()
public function boolean wf_product_selected (string as_product)
end prototypes

public function boolean wf_update_header ();String	ls_desc, ls_string
Boolean	lb_rtn
Long		ll_idx

ls_desc = trim(dw_header.Object.product_grp_name[1])
If len(ls_desc) > 0 Then
	If is_option = "MOD" Then
		lb_rtn = iu_grpmaint.uf_update_db(is_option,"H",ls_desc, "")
		Return lb_rtn
	End If
Else
	iu_grpmaint.uf_setmicrohelp("No Description Specified")
	Return False
End If
//
ll_idx = dw_selected_prod.GetNextModified(0, Primary!)
If ll_idx > 0 then
	ls_string = dw_selected_prod.Object.sku_product_code[ll_idx]
	ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += "~t" + dw_selected_prod.Object.sku_product_code[ll_idx]
		ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"H", ls_desc, ls_string)
End If

if dw_header.GetItemString(1 , 'product_grp_name') = "NEW PRODUCTS" Then
	ls_string = ""
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"H", ls_desc, ls_string)
End if

Return lb_rtn
end function

public function boolean wf_update_products ();Long			ll_idx, ll_count
String		ls_string
Boolean		lb_rtn

If is_option = "NEW" Then Return True
lb_rtn = True
// Deleteing Products
ll_count = dw_selected_prod.DeletedCount()
If ll_count > 0 Then
	ls_string = dw_selected_prod.GetItemString(1, "sku_product_code", Delete!, False)
	If ll_count > 1 Then
		For ll_idx = 2 To ll_count
			ls_string += "~t" + dw_selected_prod.GetItemString(ll_idx, "sku_product_code", Delete!, False)
		Next
	End If
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"D",ls_string, "")
End If

//////////////////////////////
// Adding - Modify Products //
//////////////////////////////
ll_idx = dw_selected_prod.GetNextModified(0, Primary!)

If ll_idx > 0 Then
	ls_string = dw_selected_prod.Object.sku_product_code[ll_idx]
	
	ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += "~t" + dw_selected_prod.Object.sku_product_code[ll_idx]
		ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint.uf_update_db(is_option,"P",ls_string, "")
End If

Return lb_rtn
// // //
end function

public function boolean wf_product_selected (string as_product);Long		ll_row
String	ls_expesion


ls_expesion = "sku_product_code = '" + as_product + "'"

ll_row = dw_selected_prod.Find(ls_expesion, 1, dw_selected_prod.RowCount())

//If ll_row > 0 or dw_header.GetItemString(1 , 'product_grp_name') <> "NEW PRODUCTS" Then
If ll_row > 0 Then
	Return True
Else
	Return False
End IF
end function

on w_groupmaintdef.create
int iCurrent
call super::create
this.dw_products=create dw_products
this.dw_header=create dw_header
this.dw_selected_prod=create dw_selected_prod
this.cb_select_prod=create cb_select_prod
this.cb_remove=create cb_remove
this.cb_remove_all=create cb_remove_all
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_products
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.dw_selected_prod
this.Control[iCurrent+4]=this.cb_select_prod
this.Control[iCurrent+5]=this.cb_remove
this.Control[iCurrent+6]=this.cb_remove_all
end on

on w_groupmaintdef.destroy
call super::destroy
destroy(this.dw_products)
destroy(this.dw_header)
destroy(this.dw_selected_prod)
destroy(this.cb_select_prod)
destroy(this.cb_remove)
destroy(this.cb_remove_all)
end on

event open;call super::open;
This.SetRedraw(False)

iu_grpmaint = Message.PowerObjectParm

If isValid(iu_grpmaint) Then
	is_owner = iu_grpmaint.uf_getstring("OWNER")
	is_option = iu_grpmaint.uf_getstring("ADDMOD")
Else
	MessageBox("Error","invalid parameters")
End If

dw_products.SetTransObject (SQLCA)
dw_products.Retrieve()


end event

event ue_postopen;String			ls_div, ls_filter, ls_name, ls_products


dw_header.InsertRow(0)
dw_header.SetRow(1)

If is_option = "NEW" Then
	dw_header.SetColumn(3)
Else
	ls_products = iu_grpmaint.uf_getstring("PRODUCTS")	
	dw_selected_prod.ImportString(ls_products)
	
	ls_name = iu_grpmaint.uf_getstring("DESCRIPTION")	
	dw_header.Object.product_grp_name[1] = Trim(ls_name)
	dw_header.SetColumn(5)
End If

dw_header.Object.division_code[1] = "11"
ls_div = dw_header.Object.division_code[1]
ls_filter = "product_division = '" + ls_div + "'"
dw_products.SetFilter(ls_filter)
dw_products.Filter()

dw_selected_prod.ResetUpdate()

dw_header.SetFocus()

This.SetRedraw(True)
end event

type cb_help from w_abstractresponse`cb_help within w_groupmaintdef
int X=2153
int Y=280
int Width=279
int TabOrder=0
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_groupmaintdef
int X=2153
int Y=148
int TabOrder=80
boolean Cancel=true
end type

event clicked;CloseWithReturn(Parent, "CANCEL")
end event

type cb_ok from w_abstractresponse`cb_ok within w_groupmaintdef
int X=2153
int Y=20
int Width=279
int TabOrder=70
boolean Default=true
end type

event cb_ok::clicked;String	ls_header, ls_products
Long		ll_rows


IF dw_selected_prod.AcceptText() = -1 THEN
	SetMicroHelp("error in product list")
	Return
END IF
IF dw_header.AcceptText() = -1 THEN
	iu_grpmaint.uf_setmicrohelp("Error in Group Description")
	Return
END IF

ll_rows = dw_selected_prod.RowCount()

If ll_rows < 2 and dw_header.GetItemString(1 , 'product_grp_name') <> "NEW PRODUCTS" Then 
	iu_grpmaint.uf_setmicrohelp("Must select 2 or more products")
	MessageBox("Group Maintenance","Please select 2 or more products.")
	Return
End If
SetPointer(HourGlass!)

If wf_update_header() Then
	If wf_update_products() Then
		SetPointer(Arrow!)
		CloseWithReturn(Parent, "OK")
	End If
End If
SetPointer(Arrow!)
end event

type dw_products from u_abstractdw within w_groupmaintdef
int X=0
int Y=320
int Width=1385
int Height=1128
int TabOrder=20
boolean BringToTop=true
string DataObject="d_select_product"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event constructor;SetMicroHelp("Loading Product Codes")
SetPointer(HourGlass!)
This.SetTransObject(sqlca)
This.Retrieve()
SetPointer(Arrow!)
SetMicroHelp("Ready")

end event

event clicked;If row > 0 Then
	If This.IsSelected(row) Then
		This.SelectRow(row,False)
	Else
		This.SelectRow(row,True)
	End If
End If
end event

type dw_header from u_abstractdw within w_groupmaintdef
event ue_another_key pbm_dwnkey
int X=0
int Y=0
int Width=1157
int Height=312
boolean BringToTop=true
string DataObject="d_prodgrpdefheader"
BorderStyle BorderStyle=StyleBox!
end type

event ue_another_key;Choose Case key
	Case	KeyBack!
		save = False
		ii_keystyped = Len(This.GetText())+ 1
	Case KeyLeftArrow!   
		ii_keystyped --
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		save = False		
		ii_keystyped = Len(This.GetText())
END choose

end event

event constructor;DataWindowChild	ldwc_div

//This.InsertRow(0)
dw_header.GetChild("division_code", ldwc_div)
ldwc_div.SetTransObject(SQLCA)
ldwc_div.Retrieve('000', 'ZZZ')
end event

event itemchanged;String		ls_filter, ls_div

u_TreeViewFunctions		lu_TreeViewFunctions


Choose Case dwo.name
	Case "division_code"
		ls_div = Trim(Left(data,2))
		ls_filter = "product_division = '" + ls_div + "'"
		dw_products.SetFilter(ls_filter)
		dw_products.Filter()
	Case "product_grp_name"
		this.object.business_rule[row] = "M"
		If Not iu_grpmaint.uf_checkfordupdescr(data) Then 
			MessageBox("Error","You cannot have duplicate Product Group Descriptions.")
			This.SetFocus()
			This.SelectText(1, 1000)
			Return 1
		End If
	Case Else
		
End Choose
end event

event editchanged;Long		ll_row, ll_first_row, ll_last_row
String	ls_FindString			

IF KEYDOWN(KeyBack!) OR KEYDOWN(KeyLeftArrow!) OR KEYDOWN(KeyEnd!) OR  KEYDOWN(KeyHome!) OR  KEYDOWN( KeyRightArrow!)&
							OR KEYDOWN(Keydelete!) Then RETURN

ii_keystyped++

If ii_keystyped > Len(data) Then ii_keystyped = Len(data) + 1

Choose Case String(dwo.Name)
	Case 'product_code'
			save = Not Save			
			IF Not save Then Return
			is_whatwastyped += Right(Data,1)
			ls_FindString = "sku_product_code >= '"+Trim(Left(Data,ii_keystyped))+"'"
	Case "product_decription"
		ls_FindString = "Pos(long_description,'" + Data +"') > 0"
	Case Else
		Return
End Choose
ll_Row = dw_products.Find( ls_FindString, 1, dw_products.RowCount()+1)

If ll_row > 0 Then 
	if String(dwo.Name) =  'product_code' Then
		This.SetText(dw_products.GetItemString(ll_row,"sku_product_code"))
		This.SelectText(ii_keystyped,10)
		IF ll_row + 1 <= dw_products.RowCount() Then
			dw_products.SelectRow( il_CurrentRowSelected, fALSE)
			dw_products.SelectRow( ll_row, TRUE)
			il_CurrentRowSelected = ll_row
			
//			if pos(dw_products.GetItemString( ll_row +1,"sku_product_code"),&
//					Trim(Left(Data,ii_keystyped))) = 0 Then 
//					Beep(1)
//					dw_products.SelectRow(0,FALSE)
//					dw_products.SelectRow( ll_row, TRUE)
//					il_CurrentRowSelected = ll_row
//				ELSE
//					dw_products.SelectRow(0,FALSE)
//					il_CurrentRowSelected = 0
//			END IF
		END IF
	END IF
	dw_products.ScrollToRow(ll_row)
	
	dw_products.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_products.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_products.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_products.SetRedraw(False)
	dw_products.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_products.ScrollToRow(ll_row)
	dw_products.SetRow(ll_row + 1)
	dw_products.SetRedraw(True)
End If

ii_keystyped++


end event

event itemerror;Return 1
end event

event itemfocuschanged;ii_keystyped = 2
end event

type dw_selected_prod from u_abstractdw within w_groupmaintdef
int X=1582
int Y=320
int Width=558
int Height=1128
int TabOrder=60
boolean BringToTop=true
string DataObject="d_productselected"
boolean VScrollBar=true
end type

event clicked;If row > 0 Then
	If this.IsSelected(row) Then
		This.SelectRow(row,False)
	Else
		This.SelectRow(row,True)
	End If	
End If
end event

type cb_select_prod from commandbutton within w_groupmaintdef
int X=1403
int Y=560
int Width=160
int Height=108
int TabOrder=30
boolean BringToTop=true
string Text=">"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Integer		li_row = 0, li_new
String		ls_product



li_row = dw_products.GetSelectedRow(li_row)
If li_row = 0 Then Return
Do
	ls_product = dw_products.Object.sku_product_code[li_row]
	If Not wf_product_selected(ls_product) Then
		li_new = dw_selected_prod.InsertRow(0)
		dw_selected_prod.object.sku_product_code[li_new] = ls_product
	End If
	li_row = dw_products.GetSelectedRow(li_row)
Loop Until li_row = 0

dw_products.SelectRow(0,False)
dw_selected_prod.Sort()

end event

type cb_remove from commandbutton within w_groupmaintdef
int X=1403
int Y=992
int Width=160
int Height=108
int TabOrder=40
boolean BringToTop=true
string Text="<<"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
//////////////////////dw_selected_prod.Reset()

Integer		li_row

w_groupmaintdef.SetRedraw(False)
//li_row = dw_selected_prod.RowCount()

//For li_new = 1 to li_row
////	dw_selected_prod.SelectRow(li_new, True)
//	dw_selected_prod.DeleteRow(li_new)
//Next
Do
	li_row = dw_selected_prod.RowCount()
	If li_row > 0 Then dw_selected_prod.DeleteRow(1)
Loop Until li_row <= 0
//dw_selected_prod.SelectRow(0,False)
w_groupmaintdef.SetRedraw(TRUE)

end event

type cb_remove_all from commandbutton within w_groupmaintdef
int X=1403
int Y=856
int Width=160
int Height=108
int TabOrder=50
boolean BringToTop=true
string Text="<"
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Integer		li_row, li_new

li_row = dw_selected_prod.GetSelectedRow(0)
If li_row = 0 Then Return
Do
	dw_selected_prod.DeleteRow(li_row)
	li_row = dw_selected_prod.GetSelectedRow(0)
Loop Until li_row = 0

dw_selected_prod.SelectRow(0,False)

end event

