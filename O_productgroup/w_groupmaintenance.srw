HA$PBExportHeader$w_groupmaintenance.srw
forward
global type w_groupmaintenance from w_netwise_sheet
end type
type tv_1 from u_prodgrptree within w_groupmaintenance
end type
type lv_1 from u_prodgrplist within w_groupmaintenance
end type
end forward

global type w_groupmaintenance from w_netwise_sheet
integer width = 3022
string title = "Product Group Maintenance (old)"
long backcolor = 12632256
event ue_copygrp ( )
event ue_changelargeicon ( )
event ue_changesmallicon ( )
event ue_changereport ( )
event ue_changelist ( )
event ue_deletegrp ( )
event ue_modifygrp ( )
event ue_arrangelist ( )
event ue_newgrp ( )
tv_1 tv_1
lv_1 lv_1
end type
global w_groupmaintenance w_groupmaintenance

type variables
u_grpmaintenance		iu_grpmaintenance

end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_filenew ()
public subroutine wf_delete ()
public function string wf_get_user_id ()
public function string wf_get_user_pw ()
public function string wf_get_working_dir ()
public subroutine wf_pointerposition (ref long pointerx, ref long pointery)
public subroutine wf_setmicrohelp (string as_input)
end prototypes

event ue_copygrp;GetFocus().TriggerEvent('ue_copygrp')
end event

event ue_changelargeicon;lv_1.View = listviewlargeicon!
end event

event ue_changesmallicon;lv_1.View = listviewsmallicon!
end event

event ue_changereport;lv_1.View = listviewreport!
end event

event ue_changelist;lv_1.View = listviewlist!
end event

event ue_deletegrp;GetFocus().TriggerEvent('ue_deletegrp')
end event

event ue_modifygrp;GetFocus().TriggerEvent('ue_modifygrp')
end event

event ue_arrangelist;lv_1.Arrange()
end event

event ue_newgrp;GetFocus().TriggerEvent('ue_newgrp')
end event

public function boolean wf_retrieve ();Return iu_grpmaintenance.uf_inquire()

end function

public subroutine wf_filenew ();iu_grpmaintenance.uf_new()
end subroutine

public subroutine wf_delete ();iu_grpmaintenance.uf_delete()
end subroutine

public function string wf_get_user_id ();// returns string that is the userid
// this function is different in the new frame work
String ls_user_id

ls_user_id = SQLCA.UserId

Return ls_user_id
end function

public function string wf_get_user_pw ();// returns string that is the User PassWord
// this function is different in the new frame work
String ls_user_pw

ls_user_pw = SQLCA.DBPass

Return ls_user_pw

end function

public function string wf_get_working_dir ();String		ls_userini, ls_workingdir
u_sdkcalls	lu_sdkcalls


// Get Absolute path to ibpuser.ini file
lu_sdkcalls = Create u_sdkcalls
lu_sdkcalls.nf_GetUserIniPath(ls_userini, ls_workingdir)


Return ls_workingdir
end function

public subroutine wf_pointerposition (ref long pointerx, ref long pointery);pointerx = iw_frame.PointerX()
pointery = iw_frame.PointerY()

end subroutine

public subroutine wf_setmicrohelp (string as_input);This.SetMicroHelp(as_input)
end subroutine

on w_groupmaintenance.create
int iCurrent
call super::create
this.tv_1=create tv_1
this.lv_1=create lv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tv_1
this.Control[iCurrent+2]=this.lv_1
end on

on w_groupmaintenance.destroy
call super::destroy
destroy(this.tv_1)
destroy(this.lv_1)
end on

event resize;call super::resize;iu_grpmaintenance.uf_resize(This)
end event

event close;call super::close;//Destroy iu_grpmaintenance
end event

event ue_postopen;call super::ue_postopen;Boolean		lb_rtn

lb_rtn = iu_grpmaintenance.uf_inquire()
end event

event open;call super::open;iu_grpmaintenance = Create u_grpmaintenance
IF NOT iu_grpmaintenance.uf_initialize(lv_1,tv_1,This) Then
	MessageBox("open event", "uf_initialize Failed")
End If
IF NOT iu_grpmaintenance.uf_initializepfm() Then
	MessageBox("Error", "Initialize For PFM Failed")
End If

end event

event activate;call super::activate;//m_netwise_menu lm_menu
//
//lm_menu = This.MenuId
//lm_menu.m_file.m_new.Enabled = False
//lm_menu.m_file.m_delete.Enabled = False

iw_Frame.im_Menu.m_file.m_new.Enabled = True
iw_Frame.im_Menu.m_file.m_delete.Enabled = True


//iw_frame.im_menu.object.m_holding4.m_deleterow.Enabled = false
//iw_frame.im_menu.object.m_holding4.m_addrow.Enabled = false
//
//iw_frame.im_menu.object.m_holding4.m_deleterow.Enabled = false
//iw_frame.im_menu.object.m_holding4.m_addrow.Enabled = false

end event

event deactivate;iw_Frame.im_Menu.m_file.m_new.Enabled = False
iw_Frame.im_Menu.m_file.m_delete.Enabled = False
//iw_frame.im_menu.m_holding4.m_deleterow.Enabled = True
//iw_frame.im_menu.m_holding4.m_addrow.Enabled = True
end event

event ue_deleterow;call super::ue_deleterow;
Long					ll_handle, &
						ll_ownerhandle, &
						ll_ParentHandle					
						


iu_grpmaintenance.uf_delete()


ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
ll_parenthandle = tv_1.FindItem(ParentTreeItem!, ll_handle)
ll_ownerhandle = iu_grpmaintenance.uf_GetOwnerHandle()
if (ll_ParentHandle = ll_ownerhandle) then
	this.TriggerEvent('ue_deletegrp')
else
	messagebox("Informational","Can not delete this item.")
end if
end event

event ue_addrow;call super::ue_addrow;this.TriggerEvent('ue_newgrp')
end event

type tv_1 from u_prodgrptree within w_groupmaintenance
integer width = 1193
integer height = 1324
boolean bringtotop = true
long textcolor = 0
long backcolor = 1073785281
boolean linesatroot = true
string picturename[] = {"Custom039!","Custom050!","line.ico","Custom051!","Op.ico","Pas.ico","sma.ico","Custom066!"}
long picturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

type lv_1 from u_prodgrplist within w_groupmaintenance
integer x = 1207
integer width = 1728
integer height = 1320
integer taborder = 11
boolean bringtotop = true
long textcolor = 0
long backcolor = 1073785281
boolean autoarrange = true
string largepicturename[] = {"line.ico","box.ico","Search!","Help!","Op.ico","Pas.ico","sma.ico"}
long largepicturemaskcolor = 553648127
string smallpicturename[] = {"line.ico","box.ico","Search!","Help!","Op.ico","Pas.ico","sma.ico"}
long smallpicturemaskcolor = 553648127
long statepicturemaskcolor = 553648127
end type

