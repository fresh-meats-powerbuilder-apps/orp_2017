HA$PBExportHeader$w_groupmaintcustomer_new.srw
forward
global type w_groupmaintcustomer_new from w_abstractresponse
end type
type cb_select_cust from commandbutton within w_groupmaintcustomer_new
end type
type cb_remove from commandbutton within w_groupmaintcustomer_new
end type
type cb_remove_all from commandbutton within w_groupmaintcustomer_new
end type
type dw_header from u_base_dw within w_groupmaintcustomer_new
end type
type dw_grouptype from u_base_dw within w_groupmaintcustomer_new
end type
type dw_customers from u_base_dw within w_groupmaintcustomer_new
end type
type dw_selected_cust from u_base_dw within w_groupmaintcustomer_new
end type
type st_comments from statictext within w_groupmaintcustomer_new
end type
type mle_comment from multilineedit within w_groupmaintcustomer_new
end type
end forward

global type w_groupmaintcustomer_new from w_abstractresponse
integer x = 15
integer y = 42
integer width = 3046
integer height = 1872
string title = "Customer Group Maintenance Definition"
cb_select_cust cb_select_cust
cb_remove cb_remove
cb_remove_all cb_remove_all
dw_header dw_header
dw_grouptype dw_grouptype
dw_customers dw_customers
dw_selected_cust dw_selected_cust
st_comments st_comments
mle_comment mle_comment
end type
global w_groupmaintcustomer_new w_groupmaintcustomer_new

type variables
Boolean	save 
Long	il_CurrentRowSelected, il_grp_id

String	is_WhatWasTyped, is_owner, is_option
Integer	ii_keysTyped = 2

u_grpmaintenance_new		iu_grpmaint_new
end variables

forward prototypes
public function boolean wf_update_header ()
public function boolean wf_customer_selected (string as_customer_coder)
public function boolean wf_update_customers ()
public function string wf_build_filter_string (string as_customer_type)
public function string wf_reformat_text (string as_reformat)
end prototypes

public function boolean wf_update_header ();String	ls_desc, ls_string, ls_grouptypecode, ls_comment
Boolean	lb_rtn
Long		ll_idx

ls_desc = trim(dw_header.Object.customer_grp_name[1])
ls_grouptypecode = dw_grouptype.GetItemString(1, "group_type")
ls_comment = wf_reformat_text(mle_comment.Text)

If len(ls_desc) > 0 Then
	If is_option = "MOD" Then
		lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"H",ls_desc, ls_grouptypecode, '', ls_comment)
		Return lb_rtn
	End If
Else
	iu_grpmaint_new.uf_setmicrohelp("No Description Specified")
	Return False
End If
//
ll_idx = dw_selected_cust.GetNextModified(0, Primary!)
If ll_idx > 0 then
	ls_string = dw_selected_cust.Object.customer_code[ll_idx] + "~t" 
	ls_string += dw_selected_cust.Object.customer_type[ll_idx] + "~t" 
	ll_idx = dw_selected_cust.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += dw_selected_cust.Object.customer_code[ll_idx] + "~t" 
		ls_string += dw_selected_cust.Object.customer_type[ll_idx] + "~t" 
		ll_idx = dw_selected_cust.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"H", ls_desc, ls_grouptypecode, ls_string, ls_comment)
End If

if dw_header.GetItemString(1 , 'customer_grp_name') = "NEW CUSTOMERS" Then
	ls_string = ""
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"H", ls_desc, ls_grouptypecode, ls_string, ls_comment)
End if

Return lb_rtn
end function

public function boolean wf_customer_selected (string as_customer_coder);Long		ll_row
String	ls_expesion


ls_expesion = "customer_code = '" + as_customer_coder + "'"

ll_row = dw_selected_cust.Find(ls_expesion, 1, dw_selected_cust.RowCount())

If ll_row > 0 Then
	Return True
Else
	Return False
End IF
end function

public function boolean wf_update_customers ();Long			ll_idx, ll_count
String		ls_string, ls_grouptypecode
Boolean		lb_rtn

If is_option = "NEW" Then Return True
lb_rtn = True
// Deleting Customers
ll_count = dw_selected_cust.DeletedCount()
If ll_count > 0 Then
	ls_string = dw_selected_cust.GetItemString(1, "customer_code", Delete!, False) + "~t"
	ls_string += dw_selected_cust.GetItemString(1, "customer_type", Delete!, False) + "~t"
	If ll_count > 1 Then
		For ll_idx = 2 To ll_count
			ls_string += dw_selected_cust.GetItemString(ll_idx, "customer_code", Delete!, False) + "~t"
			ls_string += dw_selected_cust.GetItemString(ll_idx, "customer_type", Delete!, False) + "~t"
		Next
	End If
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"D", '', '', ls_string, '')
End If

//////////////////////////////
// Adding - Modify Customers //
//////////////////////////////
ll_idx = dw_selected_cust.GetNextModified(0, Primary!)

If ll_idx > 0 Then
	ls_string = dw_selected_cust.Object.customer_code[ll_idx] + "~t"
	ls_string += dw_selected_cust.Object.customer_type[ll_idx] + "~t"
	
	ll_idx = dw_selected_cust.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += dw_selected_cust.Object.customer_code[ll_idx] + "~t"
		ls_string += dw_selected_cust.Object.customer_type[ll_idx] + "~t"
		ll_idx = dw_selected_cust.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"P", '','', ls_string, '')
End If

Return lb_rtn
// // //
end function

public function string wf_build_filter_string (string as_customer_type);String		ls_filter_string

Long		ll_selected_count, ll_sub

ls_filter_string = "customer_type = '" + as_customer_type + "'" 
//ls_filter_string = ''


ll_selected_count = dw_selected_cust.RowCount()

For ll_sub = 1 to ll_selected_count
		ls_filter_string += " AND (NOT  (customer_id = '" + RightTrim(dw_selected_cust.GetItemString(ll_sub, "customer_code")) + "' AND customer_type = '" + &
								dw_selected_cust.GetItemString(ll_sub, "customer_type") + "'))"
Next

Return ls_filter_string
end function

public function string wf_reformat_text (string as_reformat);Long		ll_PlaceHolder, ll_len

String		ls_ReturnString, ls_first_line, ls_second_line, ls_third_line, ls_fourth_line, ls_fifth_line

ll_PLaceHolder = Pos(as_reformat, "~r~n")

If ll_PlaceHolder < 1 then
	ls_ReturnString = as_reformat
else
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_first_line = mid(as_reformat, 1, 40)
		as_reformat = mid(as_reformat, 41)	
	else
		ls_first_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_first_line)
	ls_first_line = ls_first_line + space (40 - ll_len)

	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_second_line = mid(as_reformat, 1, 40)
		as_reformat = mid(as_reformat, 41)	
	else
		ls_second_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if		
	ll_len = len(ls_second_line)
	ls_second_line = ls_second_line + space (40 - ll_len)
		
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_third_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		ls_third_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_third_line)
	ls_third_line = ls_third_line + space (40 - ll_len)
	
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_fourth_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		ls_fourth_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_fourth_line)
	ls_fourth_line = ls_fourth_line + space (40 - ll_len)
	
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_fifth_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		as_reformat = mid(as_reformat, ll_PlaceHolder - 1)
		ls_fifth_line = mid(as_reformat, 1, ll_PlaceHolder + 2)
	end if		
	ll_len = len(ls_fifth_line)
	ls_fifth_line = ls_fifth_line + space (40 - ll_len)
	
	ls_ReturnString = ls_first_line + ls_second_line + ls_third_line + ls_fourth_line + ls_fifth_line
	
end if	

Return ls_ReturnString
	

end function

on w_groupmaintcustomer_new.create
int iCurrent
call super::create
this.cb_select_cust=create cb_select_cust
this.cb_remove=create cb_remove
this.cb_remove_all=create cb_remove_all
this.dw_header=create dw_header
this.dw_grouptype=create dw_grouptype
this.dw_customers=create dw_customers
this.dw_selected_cust=create dw_selected_cust
this.st_comments=create st_comments
this.mle_comment=create mle_comment
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_select_cust
this.Control[iCurrent+2]=this.cb_remove
this.Control[iCurrent+3]=this.cb_remove_all
this.Control[iCurrent+4]=this.dw_header
this.Control[iCurrent+5]=this.dw_grouptype
this.Control[iCurrent+6]=this.dw_customers
this.Control[iCurrent+7]=this.dw_selected_cust
this.Control[iCurrent+8]=this.st_comments
this.Control[iCurrent+9]=this.mle_comment
end on

on w_groupmaintcustomer_new.destroy
call super::destroy
destroy(this.cb_select_cust)
destroy(this.cb_remove)
destroy(this.cb_remove_all)
destroy(this.dw_header)
destroy(this.dw_grouptype)
destroy(this.dw_customers)
destroy(this.dw_selected_cust)
destroy(this.st_comments)
destroy(this.mle_comment)
end on

event open;call super::open;
Long	frameX, &
		frameY, &
		responseX, &
		responseY, &
		parent_windowX, &
		parent_windowY
		
responseX = This.X
responseY = This.Y

frameX = gw_netwise_frame.X
frameY = gw_netwise_frame.Y

window	lw_parentwindow

If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		lw_parentwindow = message.powerobjectparm
		parent_windowX = lw_parentwindow.X
		parent_windowY = lw_parentwindow.Y
	Else	
		parent_windowX = 0
		parent_windowY = 0
	End If
Else
	parent_windowX = 0
	parent_windowY = 0
End If

If responseX < frameX Then
	This.Move (frameX + parent_windowX + 400, frameY + parent_windowY + 600)
End If


This.SetRedraw(False)

iu_grpmaint_new = Message.PowerObjectParm

If isValid(iu_grpmaint_new) Then
	is_owner = iu_grpmaint_new.uf_getstring("OWNER")
	is_option = iu_grpmaint_new.uf_getstring("ADDMOD")
Else
	MessageBox("Error","invalid parameters")
End If

dw_customers.SetTransObject (SQLCA)
dw_customers.Retrieve('S')

end event

event ue_postopen;String			ls_div, ls_filter, ls_name, ls_customers, ls_grouptype, ls_customer_type, ls_groupcomment
Long			ll_ret

dw_header.InsertRow(0)
dw_header.SetRow(1)

If is_option = "NEW" Then
	dw_header.SetColumn(3)
Else
	ls_customers = iu_grpmaint_new.uf_getstring("CUSTOMERS")	
	ll_ret = dw_selected_cust.ImportString(ls_customers)
	
	ls_name = iu_grpmaint_new.uf_getstring("DESCRIPTION")	
	dw_header.Object.customer_grp_name[1] = Trim(ls_name)
	dw_header.SetColumn(5)
	
	ls_grouptype = iu_grpmaint_new.uf_getstring("GROUP_TYPE")	
	dw_grouptype.SetItem(1, "group_type", ls_grouptype)
	
	ls_groupcomment = iu_grpmaint_new.uf_getstring("GROUP_COMMENT")	
	mle_comment.Text = righttrim(ls_groupcomment)	
	
End If

dw_selected_cust.ResetUpdate()

dw_header.SetFocus()

ls_customer_type = dw_header.object.customer_type[1]
ls_filter = wf_build_filter_string(ls_customer_type)
dw_customers.SetFilter(ls_filter)
dw_customers.Filter()

This.SetRedraw(True)
end event

type cb_help from w_abstractresponse`cb_help within w_groupmaintcustomer_new
integer x = 2695
integer y = 282
integer width = 278
integer taborder = 0
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_groupmaintcustomer_new
integer x = 2695
integer y = 147
integer taborder = 100
boolean cancel = true
end type

event clicked;CloseWithReturn(Parent, "CANCEL")
end event

type cb_ok from w_abstractresponse`cb_ok within w_groupmaintcustomer_new
integer x = 2695
integer y = 19
integer width = 278
integer taborder = 90
end type

event cb_ok::clicked;String	ls_header
Long		ll_rows

if isnull(dw_grouptype.GetItemString(1, "group_type")) or (dw_grouptype.GetItemString(1, "group_type") <= "        ") then
	dw_grouptype.SetItem(1, "group_type",  "        ")
end if

IF dw_selected_cust.AcceptText() = -1 THEN
	SetMicroHelp("error in customer list")
	Return
END IF
IF dw_header.AcceptText() = -1 THEN
	iu_grpmaint_new.uf_setmicrohelp("Error in Group Description")
	Return
END IF

ll_rows = dw_selected_cust.RowCount()

//If ll_rows < 2 and dw_header.GetItemString(1 , 'customer_grp_name') <> "NEW CUSTOMERS" Then 
//	iu_grpmaint.uf_setmicrohelp("Must select 2 or more customers")
//	MessageBox("Group Maintenance","Please select 2 or more customers.")
//	Return
//End If

SetPointer(HourGlass!)

If wf_update_header() Then
	If wf_update_customers() Then
		SetPointer(Arrow!)
		CloseWithReturn(Parent, "OK")
	End If
End If
SetPointer(Arrow!)
end event

type cb_select_cust from commandbutton within w_groupmaintcustomer_new
integer x = 1357
integer y = 768
integer width = 161
integer height = 109
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = ">"
end type

event clicked;Integer		li_row = 0, li_new
Long  ll_first_row, ll_last_row
String		ls_customer_code, ls_customer_type, ls_userid, ls_filter

ls_userid = iu_grpmaint_new.uf_getuserid()

ll_first_row = Long(dw_customers.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_customers.object.datawindow.LastRowOnPage)

li_row = dw_customers.GetSelectedRow(li_row)
If li_row = 0 Then Return
Do
	ls_customer_code = dw_customers.Object.customer_id[li_row]
	ls_customer_type = dw_customers.Object.customer_type[li_row]
	If Not wf_customer_selected(ls_customer_code) Then
		li_new = dw_selected_cust.InsertRow(0)
		dw_selected_cust.object.customer_code[li_new] = ls_customer_code
		dw_selected_cust.object.customer_type[li_new] = ls_customer_type
		dw_selected_cust.object.last_update_date[li_new] = today()
		dw_selected_cust.object.last_update_userid[li_new] = ls_userid		
	End If
	li_row = dw_customers.GetSelectedRow(li_row)
Loop Until li_row = 0

This.SetRedraw(False)

dw_customers.SelectRow(0,False)
dw_selected_cust.Sort()

ls_customer_type = dw_header.object.customer_type[1]
ls_filter = wf_build_filter_string(ls_customer_type)
dw_customers.SetFilter(ls_filter)
dw_customers.Filter()

dw_customers.ScrollToRow(ll_last_row)
dw_customers.ScrollToRow(ll_first_row)

This.SetRedraw(True)
end event

type cb_remove from commandbutton within w_groupmaintcustomer_new
integer x = 1357
integer y = 1200
integer width = 161
integer height = 109
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<<"
end type

event clicked;
//////////////////////dw_selected_cust.Reset()

Integer		li_row
Long		ll_first_row, ll_last_row

String		ls_customer_type, ls_filter

ll_first_row = Long(dw_customers.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_customers.object.datawindow.LastRowOnPage)

This.SetRedraw(False)
//li_row = dw_selected_cust.RowCount()

//For li_new = 1 to li_row
////	dw_selected_cust.SelectRow(li_new, True)
//	dw_selected_cust.DeleteRow(li_new)
//Next
Do
	li_row = dw_selected_cust.RowCount()
	If li_row > 0 Then dw_selected_cust.DeleteRow(1)
Loop Until li_row <= 0
//dw_selected_cust.SelectRow(0,False)


ls_customer_type = dw_header.object.customer_type[1]
ls_filter = wf_build_filter_string(ls_customer_type)
dw_customers.SetFilter(ls_filter)
dw_customers.Filter()

dw_customers.ScrollToRow(ll_last_row)
dw_customers.ScrollToRow(ll_first_row)

This.SetRedraw(TRUE)

end event

type cb_remove_all from commandbutton within w_groupmaintcustomer_new
integer x = 1357
integer y = 1066
integer width = 161
integer height = 109
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<"
end type

event clicked;Integer		li_row, li_new
Long  ll_first_row, ll_last_row

String			ls_filter, ls_customer_type

ll_first_row = Long(dw_customers.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_customers.object.datawindow.LastRowOnPage)

li_row = dw_selected_cust.GetSelectedRow(0)
If li_row = 0 Then Return
Do
	dw_selected_cust.DeleteRow(li_row)
	li_row = dw_selected_cust.GetSelectedRow(0)
Loop Until li_row = 0

This.SetRedraw(False)

dw_selected_cust.SelectRow(0,False)

ls_customer_type = dw_header.object.customer_type[1]
ls_filter = wf_build_filter_string(ls_customer_type)
dw_customers.SetFilter(ls_filter)
dw_customers.Filter()


dw_customers.ScrollToRow(ll_last_row)
dw_customers.ScrollToRow(ll_first_row)
This.SetRedraw(True)
end event

type dw_header from u_base_dw within w_groupmaintcustomer_new
event ue_another_key pbm_dwnkey
integer width = 1156
integer height = 333
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_custgrpdefheader"
boolean livescroll = true
end type

event ue_another_key;Choose Case key
	Case	KeyBack!
		save = False
		ii_keystyped = Len(This.GetText())+ 1
	Case KeyLeftArrow!   
		ii_keystyped --
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		save = False		
		ii_keystyped = Len(This.GetText())
END choose
end event

event constructor;call super::constructor;DataWindowChild	ldwc_div

//This.InsertRow(0)

This.SetItem(1, "customer_type", 'S')

end event

event editchanged;call super::editchanged;Long		ll_row, ll_first_row, ll_last_row
String	ls_FindString

if String(dwo.Name) =  'customer_code' then
	data = RightTrim(data)
	if is_whatwastyped = data then
		return
	else
		if len(is_whatwastyped) < len(data) then
			is_whatwastyped = data
		else
			is_whatwastyped = data
			return
		end if
	end if
	ls_FindString = "customer_id >= '"+ data +"'"
	ll_Row = dw_customers.Find( ls_FindString, 1, dw_customers.RowCount()+1)
	If ll_row > 0 Then 
		This.SelectText(len(data)+1,0)
		IF ll_row + 1 <= dw_customers.RowCount() Then
			dw_customers.SelectRow( il_CurrentRowSelected, fALSE)
			dw_customers.SelectRow( ll_row, TRUE)
			il_CurrentRowSelected = ll_row
		END IF
		dw_customers.ScrollToRow(ll_row)
		dw_customers.SetRow(ll_row + 1)
	End If
	ll_first_row = Long(dw_customers.Object.DataWindow.FirstRowOnPage)
	ll_last_row = Long(dw_customers.Object.DataWindow.LastRowOnPage)
	If ll_row > ll_first_row and ll_row <= ll_last_row Then 
		dw_customers.SetRedraw(False)
		dw_customers.ScrollToRow(ll_row + ll_last_row - ll_first_row)
		dw_customers.ScrollToRow(ll_row)
		dw_customers.SetRow(ll_row + 1)
		dw_customers.SetRedraw(True)
	End If
End If

Return
end event

event itemchanged;call super::itemchanged;u_TreeViewFunctions		lu_TreeViewFunctions

String		ls_filter

Choose Case dwo.name
	Case "customer_type"
		dw_customers.Reset()
		dw_customers.Retrieve(data)

		ls_filter = wf_build_filter_string(data)
		dw_customers.SetFilter(ls_filter)
		dw_customers.Filter()		
		
	Case "customer_grp_name"
		this.object.business_rule[row] = "M"
		If Not iu_grpmaint_new.uf_checkfordupdescr(data) Then 
			MessageBox("Error","You cannot have duplicate Customer Group Descriptions.")
			This.SetFocus()
			This.SelectText(1, 1000)
			Return 1
		End If
	Case Else
		
End Choose
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;ii_keystyped = 2
end event

type dw_grouptype from u_base_dw within w_groupmaintcustomer_new
integer y = 419
integer width = 1584
integer height = 90
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_group_type"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_child

Long	ll_row

If This.RowCount() = 0 Then This.InsertRow(0)

dw_grouptype.GetChild('group_type', ldwc_child)

ldwc_child.SetTransObject(sqlca)
ldwc_child.Reset()
ldwc_child.Retrieve('GRPTYPEC')
// add blank row for group type <= spaces
ll_row = ldwc_child.InsertRow(0)
ldwc_child.SetItem(ll_row, "type_code", '        ')
ldwc_child.SetItem(ll_row, "type_desc", 'GroupType not found or No Group Entered')

end event

event constructor;call super::constructor;DataWindowChild	ldwc_child
Long	ll_row

If This.RowCount() = 0 Then This.InsertRow(0)

dw_grouptype.GetChild('group_type', ldwc_child)

ldwc_child.SetTransObject(sqlca)
ldwc_child.Reset()
ldwc_child.Retrieve('GRPTYPEC')
// add blank row for group type <= spaces
ll_row = ldwc_child.InsertRow(0)
ldwc_child.SetItem(ll_row, "type_code", '        ')
ldwc_child.SetItem(ll_row, "type_desc", 'GroupType not found or No Group Entered')
end event

type dw_customers from u_base_dw within w_groupmaintcustomer_new
integer y = 538
integer width = 1287
integer height = 1219
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_select_customer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;SetMicroHelp("Loading Customer Codes")
SetPointer(HourGlass!)
This.SetTransObject(sqlca)
This.Retrieve('S')
SetPointer(Arrow!)
SetMicroHelp("Ready")

is_selection = '3'
end event

event ue_keydown;call super::ue_keydown;Long 		ll_row

ll_row = this.GetRow()

if Key = KeyDownArrow! and ll_row < this.RowCount() then
	ll_row ++
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
End If

if Key = KeyUpArrow! and ll_row > 1 then
	ll_row --
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
end if

Return 0

end event

type dw_selected_cust from u_base_dw within w_groupmaintcustomer_new
integer x = 1584
integer y = 538
integer width = 1408
integer height = 1232
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_customerselected"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;is_selection = '3'
end event

type st_comments from statictext within w_groupmaintcustomer_new
integer x = 1178
integer width = 435
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Group Comments:"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type mle_comment from multilineedit within w_groupmaintcustomer_new
integer x = 1167
integer y = 77
integer width = 1518
integer height = 333
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Fixedsys"
long textcolor = 33554432
textcase textcase = upper!
integer limit = 200
borderstyle borderstyle = stylelowered!
end type

