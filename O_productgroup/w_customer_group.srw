HA$PBExportHeader$w_customer_group.srw
$PBExportComments$Customer Group maint ibdkdld Pas32
forward
global type w_customer_group from w_groups
end type
end forward

global type w_customer_group from w_groups
string title = "Customer Group Maintenance"
end type
global w_customer_group w_customer_group

on w_customer_group.create
call super::create
end on

on w_customer_group.destroy
call super::destroy
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event ue_postopen;call super::ue_postopen;//groupTypes
//1 = Locations
//2 = Customers
//3 = Products
ole_groups.object.GroupType = 2
ole_groups.object.ApplyUserSettings() 

wf_retrieve()

end event

type ole_groups from w_groups`ole_groups within w_customer_group
end type

