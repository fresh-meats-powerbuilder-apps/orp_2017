HA$PBExportHeader$w_groupmaintproduct_new.srw
forward
global type w_groupmaintproduct_new from w_abstractresponse
end type
type cb_select_prod from commandbutton within w_groupmaintproduct_new
end type
type cb_remove from commandbutton within w_groupmaintproduct_new
end type
type cb_remove_all from commandbutton within w_groupmaintproduct_new
end type
type dw_products from u_base_dw within w_groupmaintproduct_new
end type
type dw_header from u_base_dw within w_groupmaintproduct_new
end type
type dw_selected_prod from u_base_dw within w_groupmaintproduct_new
end type
type dw_grouptype from u_base_dw within w_groupmaintproduct_new
end type
type st_comments from statictext within w_groupmaintproduct_new
end type
type mle_comment from multilineedit within w_groupmaintproduct_new
end type
type dw_division from u_division within w_groupmaintproduct_new
end type
type cbx_alldiv from checkbox within w_groupmaintproduct_new
end type
end forward

global type w_groupmaintproduct_new from w_abstractresponse
integer x = 0
integer y = 42
integer height = 1888
string title = "Product Group Maintenance Definition"
cb_select_prod cb_select_prod
cb_remove cb_remove
cb_remove_all cb_remove_all
dw_products dw_products
dw_header dw_header
dw_selected_prod dw_selected_prod
dw_grouptype dw_grouptype
st_comments st_comments
mle_comment mle_comment
dw_division dw_division
cbx_alldiv cbx_alldiv
end type
global w_groupmaintproduct_new w_groupmaintproduct_new

type variables
Boolean	save 
Long	il_CurrentRowSelected, il_grp_id

String	is_WhatWasTyped, is_owner, is_option
Integer	ii_keysTyped = 2

u_grpmaintenance_new		iu_grpmaint_new
end variables

forward prototypes
public function boolean wf_update_header ()
public function boolean wf_update_products ()
public function boolean wf_product_selected (string as_product)
public function string wf_build_filter_string (string as_division)
public function string wf_reformat_text (string as_reformat)
end prototypes

public function boolean wf_update_header ();String	ls_desc, ls_string, ls_dummy, ls_grouptypecode, ls_comment
Boolean	lb_rtn
Long		ll_idx

ls_dummy = space(1)
ls_desc = trim(dw_header.Object.product_grp_name[1])
ls_grouptypecode = dw_grouptype.GetItemString(1, "group_type")
ls_comment = wf_reformat_text(mle_comment.Text)

If len(ls_desc) > 0 Then
	If is_option = "MOD" Then
		lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"H",ls_desc, ls_grouptypecode, "", ls_comment)
		Return lb_rtn
	End If
Else
	iu_grpmaint_new.uf_setmicrohelp("No Description Specified")
	Return False
End If
//
ll_idx = dw_selected_prod.GetNextModified(0, Primary!)
If ll_idx > 0 then
	ls_string = dw_selected_prod.Object.sku_product_code[ll_idx] + '~t' 
	ls_string += ls_dummy + '~t' 
	ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += dw_selected_prod.Object.sku_product_code[ll_idx] + '~t' 
		ls_string += ls_dummy + '~t' 
		ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"H", ls_desc, ls_grouptypecode, ls_string, ls_comment)
End If

if dw_header.GetItemString(1 , 'product_grp_name') = "NEW PRODUCTS" Then
	ls_string = ""
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"H", ls_desc, ls_grouptypecode,  ls_string, ls_comment)
End if

Return lb_rtn
end function

public function boolean wf_update_products ();Long			ll_idx, ll_count
String		ls_string, ls_dummy
Boolean		lb_rtn

ls_dummy = space(1)
If is_option = "NEW" Then Return True
lb_rtn = True
// Deleteing Products
ll_count = dw_selected_prod.DeletedCount()
If ll_count > 0 Then
	ls_string = dw_selected_prod.GetItemString(1, "sku_product_code", Delete!, False) + '~t'
	ls_string += ls_dummy + '~t'
	If ll_count > 1 Then
		For ll_idx = 2 To ll_count
			ls_string += dw_selected_prod.GetItemString(ll_idx, "sku_product_code", Delete!, False) + '~t'
			ls_string += ls_dummy + '~t'
		Next
	End If
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"D",'', '', ls_string, '')
End If

//////////////////////////////
// Adding - Modify Products //
//////////////////////////////
ll_idx = dw_selected_prod.GetNextModified(0, Primary!)

If ll_idx > 0 Then
	ls_string = dw_selected_prod.Object.sku_product_code[ll_idx]  + '~t'
	ls_string += ls_dummy  + '~t'
	
	ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)

	Do while ll_idx > 0
		ls_string += dw_selected_prod.Object.sku_product_code[ll_idx]  + '~t'
		ls_string += ls_dummy  + '~t'
		ll_idx = dw_selected_prod.GetNextModified(ll_idx, Primary!)
	Loop
	
	lb_rtn = iu_grpmaint_new.uf_update_db(is_option,"P", '', '', ls_string, '')
End If

Return lb_rtn
// // //
end function

public function boolean wf_product_selected (string as_product);Long		ll_row
String	ls_expesion


ls_expesion = "sku_product_code = '" + as_product + "'"

ll_row = dw_selected_prod.Find(ls_expesion, 1, dw_selected_prod.RowCount())

//If ll_row > 0 or dw_header.GetItemString(1 , 'product_grp_name') <> "NEW PRODUCTS" Then
If ll_row > 0 Then
	Return True
Else
	Return False
End IF
end function

public function string wf_build_filter_string (string as_division);String		ls_filter_string

Long		ll_selected_count, ll_sub

if as_division <= "*" then 
	ls_filter_string = "1 = 1"
else
	ls_filter_string = "product_division = '" + as_division + "'"
end if

ll_selected_count = dw_selected_prod.RowCount()

For ll_sub = 1 to ll_selected_count
	ls_filter_string += " AND sku_product_code <> '" + dw_selected_prod.GetItemString(ll_sub, "sku_product_code") + "'"
Next

Return ls_filter_string
end function

public function string wf_reformat_text (string as_reformat);Long		ll_PlaceHolder, ll_len

String		ls_ReturnString, ls_first_line, ls_second_line, ls_third_line, ls_fourth_line, ls_fifth_line

ll_PLaceHolder = Pos(as_reformat, "~r~n")

If ll_PlaceHolder < 1 then
	ls_ReturnString = as_reformat
else
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_first_line = mid(as_reformat, 1, 40)
		as_reformat = mid(as_reformat, 41)	
	else
		ls_first_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_first_line)
	ls_first_line = ls_first_line + space (40 - ll_len)

	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_second_line = mid(as_reformat, 1, 40)
		as_reformat = mid(as_reformat, 41)	
	else
		ls_second_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if		
	ll_len = len(ls_second_line)
	ls_second_line = ls_second_line + space (40 - ll_len)
		
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_third_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		ls_third_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_third_line)
	ls_third_line = ls_third_line + space (40 - ll_len)
	
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_fourth_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		ls_fourth_line = mid(as_reformat, 1, ll_PlaceHolder - 1)		
		as_reformat = mid(as_reformat, ll_PlaceHolder + 2)
	end if	
	ll_len = len(ls_fourth_line)
	ls_fourth_line = ls_fourth_line + space (40 - ll_len)
	
	ll_PLaceHolder = Pos(as_reformat, "~r~n")	
	
	if (ll_PlaceHolder > 40) or (ll_PlaceHolder < 1)  then
		ls_fifth_line = mid(as_reformat, 1, 40)		
		as_reformat = mid(as_reformat, 41)	
	else
		as_reformat = mid(as_reformat, ll_PlaceHolder - 1)
		ls_fifth_line = mid(as_reformat, 1, ll_PlaceHolder + 2)
	end if		
	ll_len = len(ls_fifth_line)
	ls_fifth_line = ls_fifth_line + space (40 - ll_len)
	
	ls_ReturnString = ls_first_line + ls_second_line + ls_third_line + ls_fourth_line + ls_fifth_line
	
end if	

Return ls_ReturnString
	
end function

on w_groupmaintproduct_new.create
int iCurrent
call super::create
this.cb_select_prod=create cb_select_prod
this.cb_remove=create cb_remove
this.cb_remove_all=create cb_remove_all
this.dw_products=create dw_products
this.dw_header=create dw_header
this.dw_selected_prod=create dw_selected_prod
this.dw_grouptype=create dw_grouptype
this.st_comments=create st_comments
this.mle_comment=create mle_comment
this.dw_division=create dw_division
this.cbx_alldiv=create cbx_alldiv
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_select_prod
this.Control[iCurrent+2]=this.cb_remove
this.Control[iCurrent+3]=this.cb_remove_all
this.Control[iCurrent+4]=this.dw_products
this.Control[iCurrent+5]=this.dw_header
this.Control[iCurrent+6]=this.dw_selected_prod
this.Control[iCurrent+7]=this.dw_grouptype
this.Control[iCurrent+8]=this.st_comments
this.Control[iCurrent+9]=this.mle_comment
this.Control[iCurrent+10]=this.dw_division
this.Control[iCurrent+11]=this.cbx_alldiv
end on

on w_groupmaintproduct_new.destroy
call super::destroy
destroy(this.cb_select_prod)
destroy(this.cb_remove)
destroy(this.cb_remove_all)
destroy(this.dw_products)
destroy(this.dw_header)
destroy(this.dw_selected_prod)
destroy(this.dw_grouptype)
destroy(this.st_comments)
destroy(this.mle_comment)
destroy(this.dw_division)
destroy(this.cbx_alldiv)
end on

event open;call super::open;Long	frameX, &
		frameY, &
		responseX, &
		responseY, &
		parent_windowX, &
		parent_windowY
		
responseX = This.X
responseY = This.Y

frameX = gw_netwise_frame.X
frameY = gw_netwise_frame.Y

window	lw_parentwindow

If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		lw_parentwindow = message.powerobjectparm
		parent_windowX = lw_parentwindow.X
		parent_windowY = lw_parentwindow.Y
	Else	
		parent_windowX = 0
		parent_windowY = 0
	End If
Else
	parent_windowX = 0
	parent_windowY = 0
End If

If responseX < frameX Then
	This.Move (frameX + parent_windowX + 400, frameY + parent_windowY + 600)
End If

This.SetRedraw(False)

iu_grpmaint_new = Message.PowerObjectParm

If isValid(iu_grpmaint_new) Then
	is_owner = iu_grpmaint_new.uf_getstring("OWNER")
	is_option = iu_grpmaint_new.uf_getstring("ADDMOD")
Else
	MessageBox("Error","invalid parameters")
End If

dw_products.SetTransObject (SQLCA)
dw_products.Retrieve()


end event

event ue_postopen;String			ls_div, ls_filter, ls_name, ls_products, ls_groupcomment,ls_group_type


dw_header.InsertRow(0)
dw_header.SetRow(1)

If is_option = "NEW" Then
	dw_header.SetColumn(3)
Else
	ls_products = iu_grpmaint_new.uf_getstring("PRODUCTS")	
	dw_selected_prod.ImportString(ls_products)
	
	ls_name = iu_grpmaint_new.uf_getstring("DESCRIPTION")	
	dw_header.Object.product_grp_name[1] = Trim(ls_name)
	dw_header.SetColumn(5)
	
	ls_group_type = iu_grpmaint_new.uf_getString("GROUP_TYPE")
	dw_grouptype.SetItem(1, "group_type", ls_group_type)
	
	ls_groupcomment = iu_grpmaint_new.uf_getstring("GROUP_COMMENT")	
	mle_comment.Text = righttrim(ls_groupcomment)	

End If

dw_products.SetSort("sku_product_code, division_code")
dw_products.Sort()


//dw_header.Object.division_code[1] = "11"
//dw_division.Object.division_code[1] = "11"
dw_division.uf_set_division("11")

If cbx_alldiv.checked = True Then
	dw_division.visible = false
	ls_div = '*'
else
	dw_division.visible = true
	ls_div = dw_division.Object.division_code[1]
End if

//ls_div = dw_header.Object.division_code[1]

ls_filter = wf_build_filter_string(ls_div)
dw_products.SetFilter(ls_filter)
dw_products.Filter()

dw_selected_prod.ResetUpdate()

dw_header.SetFocus()

This.SetRedraw(True)
end event

type cb_help from w_abstractresponse`cb_help within w_groupmaintproduct_new
integer x = 2706
integer y = 282
integer width = 278
integer taborder = 0
end type

type cb_cancel from w_abstractresponse`cb_cancel within w_groupmaintproduct_new
integer x = 2706
integer y = 147
integer taborder = 100
boolean cancel = true
end type

event clicked;CloseWithReturn(Parent, "CANCEL")
end event

type cb_ok from w_abstractresponse`cb_ok within w_groupmaintproduct_new
integer x = 2706
integer y = 19
integer width = 278
integer taborder = 90
end type

event cb_ok::clicked;String	ls_header, ls_products
Long		ll_rows

if isnull(dw_grouptype.GetItemString(1, "group_type")) or (dw_grouptype.GetItemString(1, "group_type") <= "        ") then
	dw_grouptype.SetItem(1, "group_type",  "        ")
end if

IF dw_selected_prod.AcceptText() = -1 THEN
	SetMicroHelp("error in product list")
	Return
END IF
IF dw_header.AcceptText() = -1 THEN
	iu_grpmaint_new.uf_setmicrohelp("Error in Group Description")
	Return
END IF

ll_rows = dw_selected_prod.RowCount()

//If ll_rows < 2 and dw_header.GetItemString(1 , 'product_grp_name') <> "NEW PRODUCTS" Then 
//	iu_grpmaint.uf_setmicrohelp("Must select 2 or more products")
//	MessageBox("Group Maintenance","Please select 2 or more products.")
//	Return
//End If

SetPointer(HourGlass!)

If wf_update_header() Then
	If wf_update_products() Then
		SetPointer(Arrow!)
		CloseWithReturn(Parent, "OK")
	End If
End If
SetPointer(Arrow!)
end event

type cb_select_prod from commandbutton within w_groupmaintproduct_new
integer x = 1554
integer y = 813
integer width = 161
integer height = 109
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = ">"
end type

event clicked;Integer		li_row = 0, li_new
Long  ll_first_row, ll_last_row
String		ls_product, ls_userid, ls_filter, ls_div

ls_userid = iu_grpmaint_new.uf_getuserid()

ll_first_row = Long(dw_products.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_products.object.datawindow.LastRowOnPage)

li_row = dw_products.GetSelectedRow(li_row)
If li_row = 0 Then Return
Do
	ls_product = dw_products.Object.sku_product_code[li_row]
	If Not wf_product_selected(ls_product) Then
		li_new = dw_selected_prod.InsertRow(0)
		dw_selected_prod.object.sku_product_code[li_new] = ls_product
		dw_selected_prod.object.last_update_date[li_new] = today()
		dw_selected_prod.object.last_update_userid[li_new] = ls_userid
	End If
	li_row = dw_products.GetSelectedRow(li_row)
Loop Until li_row = 0

This.SetRedraw(False)

dw_selected_prod.Sort()

//ls_div = dw_header.Object.division_code[1]
ls_div = dw_division.Object.division_code[1]
ls_filter = wf_build_filter_string(ls_div)
dw_products.SetFilter(ls_filter)
dw_products.Filter()

dw_products.ScrollToRow(ll_last_row)
dw_products.ScrollToRow(ll_first_row)

This.SetRedraw(True)

end event

type cb_remove from commandbutton within w_groupmaintproduct_new
integer x = 1554
integer y = 1245
integer width = 161
integer height = 109
integer taborder = 70
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<<"
end type

event clicked;
//////////////////////dw_selected_prod.Reset()

Integer		li_row
Long		ll_first_row, ll_last_row

String			ls_filter, ls_div

ll_first_row = Long(dw_products.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_products.object.datawindow.LastRowOnPage)

This.SetRedraw(False)
//li_row = dw_selected_prod.RowCount()

//For li_new = 1 to li_row
////	dw_selected_prod.SelectRow(li_new, True)
//	dw_selected_prod.DeleteRow(li_new)
//Next
Do
	li_row = dw_selected_prod.RowCount()
	If li_row > 0 Then dw_selected_prod.DeleteRow(1)
Loop Until li_row <= 0
//dw_selected_prod.SelectRow(0,False)

//ls_div = dw_header.Object.division_code[1]
ls_div = dw_division.Object.division_code[1]
ls_filter = wf_build_filter_string(ls_div)
dw_products.SetFilter(ls_filter)
dw_products.Filter()

dw_products.ScrollToRow(ll_last_row)
dw_products.ScrollToRow(ll_first_row)

This.SetRedraw(TRUE)


end event

type cb_remove_all from commandbutton within w_groupmaintproduct_new
integer x = 1554
integer y = 1110
integer width = 161
integer height = 109
integer taborder = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<"
end type

event clicked;Integer		li_row, li_new
Long  ll_first_row, ll_last_row

String			ls_filter, ls_div

ll_first_row = Long(dw_products.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_products.object.datawindow.LastRowOnPage)

li_row = dw_selected_prod.GetSelectedRow(0)
If li_row = 0 Then Return
Do
	dw_selected_prod.DeleteRow(li_row)
	li_row = dw_selected_prod.GetSelectedRow(0)
Loop Until li_row = 0

This.SetRedraw(False)

dw_selected_prod.SelectRow(0,False)

//ls_div = dw_header.Object.division_code[1]
ls_div = dw_division.Object.division_code[1]
ls_filter = wf_build_filter_string(ls_div)
dw_products.SetFilter(ls_filter)
dw_products.Filter()


dw_products.ScrollToRow(ll_last_row)
dw_products.ScrollToRow(ll_first_row)

This.SetRedraw(True)
end event

type dw_products from u_base_dw within w_groupmaintproduct_new
integer y = 589
integer width = 1518
integer height = 1152
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_select_product"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;SetMicroHelp("Loading Product Codes")
SetPointer(HourGlass!)
This.SetTransObject(sqlca)
This.Retrieve()
SetPointer(Arrow!)
SetMicroHelp("Ready")

is_selection = '3'
end event

event ue_keydown;call super::ue_keydown;Long 		ll_row

ll_row = this.GetRow()

if Key = KeyDownArrow! and ll_row < this.RowCount() then
	ll_row ++
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
End If

if Key = KeyUpArrow! and ll_row > 1 then
	ll_row --
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
end if

Return 0

end event

type dw_header from u_base_dw within w_groupmaintproduct_new
event ue_another_key pbm_dwnkey
integer y = 6
integer width = 1156
integer height = 397
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_prodgrpdefheader"
boolean livescroll = true
end type

event ue_another_key;Choose Case key
	Case	KeyBack!
		save = False
		ii_keystyped = Len(This.GetText())+ 1
	Case KeyLeftArrow!   
		ii_keystyped --
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		save = False		
		ii_keystyped = Len(This.GetText())
END choose
end event

event constructor;call super::constructor;//DataWindowChild	ldwc_div

//This.InsertRow(0)
//dw_header.GetChild("division_code", ldwc_div)
//ldwc_div.SetTransObject(SQLCA)
//ldwc_div.Retrieve('000', 'ZZZ')
//ldwc_div.insertrow(1)
//ldwc_div.SetItem(1,"type_code","        ")
//ldwc_div.SetItem(1,"type_desc","ALL DIVISIONS")
end event

event editchanged;call super::editchanged;Long		ll_row, ll_first_row, ll_last_row
String	ls_FindString
DataWindowChild ldwc_div

if String(dwo.Name) =  'product_code' then
	data = RightTrim(data)
	if is_whatwastyped = data then
		return
	else
		if len(is_whatwastyped) < len(data) then
			is_whatwastyped = data
		else
			is_whatwastyped = data
			return
		end if
	end if
	ls_FindString = "sku_product_code >= '"+ data +"'"
	ll_Row = dw_products.Find( ls_FindString, 1, dw_products.RowCount()+1)
	If ll_row > 0 Then 
		This.SelectText(len(data)+1,0)
		IF ll_row + 1 <= dw_products.RowCount() Then
			dw_products.SelectRow( il_CurrentRowSelected, fALSE)
			dw_products.SelectRow( ll_row, TRUE)
			il_CurrentRowSelected = ll_row
		END IF
		dw_products.ScrollToRow(ll_row)
		dw_products.SetRow(ll_row + 1)
	End If
	ll_first_row = Long(dw_products.Object.DataWindow.FirstRowOnPage)
	ll_last_row = Long(dw_products.Object.DataWindow.LastRowOnPage)
	If ll_row > ll_first_row and ll_row <= ll_last_row Then 
		dw_products.SetRedraw(False)
		dw_products.ScrollToRow(ll_row + ll_last_row - ll_first_row)
		dw_products.ScrollToRow(ll_row)
		dw_products.SetRow(ll_row + 1)
		dw_products.SetRedraw(True)
	End If
End If

Return
end event

event itemchanged;call super::itemchanged;String		ls_filter, ls_div

u_TreeViewFunctions		lu_TreeViewFunctions


Choose Case dwo.name
//	Case "division_code"
//		ls_div = Trim(Left(data,2))
//		ls_filter = wf_build_filter_string(ls_div)
//		dw_products.SetFilter(ls_filter)
//		dw_products.Filter()
	Case "product_grp_name"
		this.object.business_rule[row] = "M"
		If Not iu_grpmaint_new.uf_checkfordupdescr(data) Then 
			MessageBox("Error","You cannot have duplicate Product Group Descriptions.")
			This.SetFocus()
			This.SelectText(1, 1000)
			Return 1
		End If
	Case Else
		
End Choose
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;ii_keystyped = 2
end event

type dw_selected_prod from u_base_dw within w_groupmaintproduct_new
integer x = 1752
integer y = 589
integer width = 1221
integer height = 1130
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_productselected"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;is_selection = '3'
end event

type dw_grouptype from u_base_dw within w_groupmaintproduct_new
integer y = 509
integer width = 1554
integer height = 83
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_group_type"
boolean livescroll = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_child
Long	ll_row

If This.RowCount() = 0 Then This.InsertRow(0)

dw_grouptype.GetChild('group_type', ldwc_child)

ldwc_child.SetTransObject(sqlca)
ldwc_child.Reset()
ldwc_child.Retrieve('GRPTYPEP')
// add blank row for group type <= spaces
ll_row = ldwc_child.InsertRow(0)
ldwc_child.SetItem(ll_row, "type_code", '        ')
ldwc_child.SetItem(ll_row, "type_desc", 'GroupType not found or No Group Entered')
end event

type st_comments from statictext within w_groupmaintproduct_new
integer x = 1178
integer width = 435
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Group Comments:"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type mle_comment from multilineedit within w_groupmaintproduct_new
integer x = 1167
integer y = 77
integer width = 1518
integer height = 333
integer taborder = 110
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Fixedsys"
long textcolor = 33554432
textcase textcase = upper!
integer limit = 200
borderstyle borderstyle = stylelowered!
end type

type dw_division from u_division within w_groupmaintproduct_new
integer y = 410
integer width = 1554
integer height = 90
integer taborder = 20
boolean bringtotop = true
boolean border = true
end type

event itemchanged;call super::itemchanged;String		ls_filter, ls_div

If Isnull(dwo) then return

Choose Case string(dwo.name)
	Case "division_code"
		ls_div = Trim(data)
		ls_filter = wf_build_filter_string(ls_div)
		dw_products.SetFilter(ls_filter)
		dw_products.Filter()
	Case Else
		
End Choose

end event

type cbx_alldiv from checkbox within w_groupmaintproduct_new
integer x = 18
integer y = 317
integer width = 380
integer height = 61
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "All Divisions"
borderstyle borderstyle = StyleRaised!
end type

event clicked;String ls_div, ls_filter

If This.checked = True Then
	dw_division.visible = false
	ls_div = '*'
else
	dw_division.visible = true
	ls_div = dw_division.Object.division_code[1]
End if

ls_filter = wf_build_filter_string(ls_div)
dw_products.SetFilter(ls_filter)
dw_products.Filter()
end event

