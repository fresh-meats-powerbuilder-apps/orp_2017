HA$PBExportHeader$w_location_group.srw
$PBExportComments$Location Group maint ibdkdld Pas32
forward
global type w_location_group from w_groups
end type
end forward

global type w_location_group from w_groups
string title = "Location Group Maintenance"
end type
global w_location_group w_location_group

on w_location_group.create
call super::create
end on

on w_location_group.destroy
call super::destroy
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event ue_postopen;call super::ue_postopen;//groupTypes
//1 = Locations
//2 = Customers
//3 = Products
ole_groups.object.GroupType = 1
ole_groups.object.ApplyUserSettings() 

wf_retrieve()

end event

type ole_groups from w_groups`ole_groups within w_location_group
end type

