HA$PBExportHeader$w_groupmaintenance_new.srw
forward
global type w_groupmaintenance_new from w_netwise_sheet
end type
type st_scroll from u_st_scroll within w_groupmaintenance_new
end type
type lv_1 from u_prodgrplist within w_groupmaintenance_new
end type
type tv_1 from u_prodgrptree within w_groupmaintenance_new
end type
end forward

global type w_groupmaintenance_new from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 3867
string title = ""
event ue_arrangelist ( )
event ue_changelargeicon ( )
event ue_changelist ( )
event ue_changereport ( )
event ue_changesmallicon ( )
event ue_copygrp ( )
event ue_delete ( )
event ue_deletegrp ( )
event type boolean ue_inquire ( )
event ue_modifygrp ( )
event ue_new ( )
event ue_newgrp ( )
event type boolean ue_save ( )
st_scroll st_scroll
lv_1 lv_1
tv_1 tv_1
end type
global w_groupmaintenance_new w_groupmaintenance_new

type variables
s_error							istr_error_info

String								is_grouptype, &
									is_app_name, &
									is_updateowner
	
long								il_ownerhandle, &
									il_treehandle

u_ws_sma						iu_ws_sma

Datastore						ids_group_list, &
									ids_group_item_list
									
u_grpmaintenance_new		iu_grpmaintenance_new
end variables

forward prototypes
public subroutine wf_pointerposition (ref long pointerx, ref long pointery)
public subroutine wf_setmicrohelp (string as_input)
end prototypes

event ue_arrangelist();lv_1.Arrange()
end event

event ue_changelargeicon();lv_1.View = listviewlargeicon!
end event

event ue_changelist();lv_1.View = listviewlist!
end event

event ue_changereport();lv_1.View = listviewreport!
end event

event ue_changesmallicon();lv_1.View = listviewsmallicon!
end event

event ue_copygrp();GetFocus().TriggerEvent('ue_copygrp')
end event

event ue_delete();iu_grpmaintenance_new.uf_delete()
end event

event ue_deletegrp();GetFocus().TriggerEvent('ue_deletegrp')
end event

event type boolean ue_inquire();Return iu_grpmaintenance_new.uf_inquire()

Return False
end event

event ue_modifygrp();GetFocus().TriggerEvent('ue_modifygrp')
end event

event ue_new();iu_grpmaintenance_new.uf_new()
end event

event ue_newgrp();GetFocus().TriggerEvent('ue_newgrp')
end event

event type boolean ue_save();Return True
end event

public subroutine wf_pointerposition (ref long pointerx, ref long pointery);pointerx = iw_frame.PointerX()
pointery = iw_frame.PointerY()
//pointerx = 900
//pointery = 500
end subroutine

public subroutine wf_setmicrohelp (string as_input);//wf_setmicrohelp(String)
This.SetMicrohelp(as_input)
end subroutine

event open;call super::open;is_grouptype = Message.StringParm

If is_grouptype = 'P' Then
	This.Title = 'Product Group Maintenance'
Else
	If is_grouptype = 'L' Then
		This.Title = 'Location Group Maintenance'
	Else
		This.Title = 'Customer Group Maintenance'
	End If
End If

iu_ws_sma = Create u_ws_sma

iu_grpmaintenance_new = Create u_grpmaintenance_new

is_app_name = 'ORP'
is_updateowner = "SALES"

 iu_grpmaintenance_new.uf_initialize(lv_1, tv_1, This, st_scroll)

//iu_grpmaintenance_new.uf_initializeorp(is_grouptype, is_app_name, is_updateowner)
iu_grpmaintenance_new.uf_initializepas(is_grouptype, is_app_name, is_updateowner)

iu_grpmaintenance_new.uf_inquire()


end event

on w_groupmaintenance_new.create
int iCurrent
call super::create
this.st_scroll=create st_scroll
this.lv_1=create lv_1
this.tv_1=create tv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_scroll
this.Control[iCurrent+2]=this.lv_1
this.Control[iCurrent+3]=this.tv_1
end on

on w_groupmaintenance_new.destroy
call super::destroy
destroy(this.st_scroll)
destroy(this.lv_1)
destroy(this.tv_1)
end on

event close;call super::close;Destroy iu_ws_sma

Destroy iu_grpmaintenance_new
end event

event resize;call super::resize;iu_grpmaintenance_new.uf_resize(This)
end event

type st_scroll from u_st_scroll within w_groupmaintenance_new
integer x = 1193
integer height = 1328
end type

type lv_1 from u_prodgrplist within w_groupmaintenance_new
integer x = 1207
integer width = 2597
integer height = 1324
integer taborder = 20
string largepicturename[] = {"line.ico","box.ico","Search!","Help!","Op.ico","Pas.ico","sma.ico"}
string smallpicturename[] = {"line.ico","box.ico","Search!","Help!","Op.ico","Pas.ico","sma.ico"}
end type

type tv_1 from u_prodgrptree within w_groupmaintenance_new
integer width = 1193
integer height = 1324
boolean linesatroot = true
string picturename[] = {"Custom039!","Custom050!","line.ico","Custom051!","Op.ico","Pas.ico","sma.ico","Custom066!"}
end type

