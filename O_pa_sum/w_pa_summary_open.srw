HA$PBExportHeader$w_pa_summary_open.srw
forward
global type w_pa_summary_open from w_base_response_ext
end type
type dw_main from u_base_dw_ext within w_pa_summary_open
end type
type dw_customer_inq from datawindow within w_pa_summary_open
end type
end forward

global type w_pa_summary_open from w_base_response_ext
integer x = 809
integer y = 520
integer width = 2011
integer height = 808
string title = "Product Availability Summary Inquire"
long backcolor = 79741120
dw_main dw_main
dw_customer_inq dw_customer_inq
end type
global w_pa_summary_open w_pa_summary_open

type variables
u_orp204		iu_orp204
u_ws_orp4       iu_ws_orp4

w_base_sheet_ext	iw_ParentWindow

w_pa-summary      iw_parent



end variables

event open;call super::open;INTEGER	li_Rtn

w_pa-summary		lw_Parent


//added 8-12 jac
DataWindowChild	ldwc_customer

string					ls_customer_id,&
                     ls_customer_name
//

dw_customer_inq.GetChild( "customer_id", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_customer_inq.GetChild( "customer_Name", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)
//


IF IsValid(Message.PowerObjectParm) THEN
	IF Message.PowerObjectParm.TypeOf() = Window! THEN
		iw_Parent = Message.PowerObjectParm
	END IF
END IF

 IF iw_Frame.wf_windowisvalid(lw_Parent, "w_pa-summary" ) THEN
//   li_Rtn = lw_Parent.dw_pa-summary_header.ShareData(dw_main)
	dw_main.SetItem(1, 'seq_num', '00')

ELSE
	li_Rtn = -1
END IF




	
//IF li_Rtn = -1 THEN 
//	CloseWithReturn(This, "CANCEL")
//	Return
//End if

IF NOt IsValid(iu_orp204) Then iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4

This.Event Post ue_postopen(0,0)


end event

event ue_postopen;call super::ue_postopen;//added 8-12 jac
string					ls_customer_id,&
                     ls_customer_name, &
							ls_reinquire_date,& 
							ls_seq_num,& 
							ls_last_seq_num, &
							ls_description,&
							ls_availability_date,&
							ls_date_type_ind, &
							ls_discounted_ind

u_string_functions	lu_string


dw_customer_inq.SetRedraw(True)


if dw_customer_inq.rowcount( ) = 0 Then
	dw_customer_inq.insertrow( 0)
End If

if dw_main.rowcount( ) = 0 Then
	dw_main.insertrow( 0)
End If

iw_Parent.Event ue_get_data('customer_id')
ls_customer_id = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_customer_id) Then 
	dw_customer_inq.SetItem(1, "customer_id", ls_customer_id)
End If

iw_Parent.Event ue_get_data('customer_name')
ls_customer_name = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_customer_name) Then 
	dw_customer_inq.SetItem(1, "customer_name", ls_customer_name)
End If

//  added 9-12-12 jac

iw_Parent.Event ue_get_data('availability_date')
ls_reinquire_date = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_reinquire_date) Then 
	dw_main.SetItem(1, "reinquire_date", Date(ls_reinquire_date))
End If


iw_Parent.Event ue_get_data('seq_num')
ls_seq_num = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_seq_num) Then 
	dw_main.SetItem(1, "seq_num", ls_seq_num)
End If

iw_Parent.Event ue_get_data('last_seq_num')
ls_last_seq_num = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_last_seq_num) Then 
	dw_main.SetItem(1, "last_seq_num", ls_last_seq_num)
End If

iw_Parent.Event ue_get_data('description')
ls_description = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_description) Then 
	dw_main.SetItem(1, "description", ls_description)
End If

iw_Parent.Event ue_get_data('date_type_ind')
ls_date_type_ind = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_date_type_ind) Then 
	dw_main.SetItem(1, "date_type_ind", ls_date_type_ind)
End If

//added 11-12 jac
iw_Parent.Event ue_get_data('discounted_ind')
ls_discounted_ind = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_discounted_ind) Then 
	dw_main.SetItem(1, "discounted_ind", ls_discounted_ind)
End If

//dw_main.SetItem(1, "availability_date", ls_reinquire_date)

if ls_date_type_ind = 'A' then
	   	dw_customer_inq.visible = False
		else 
	   	dw_customer_inq.visible = True
	end if


	dw_main.SetItem(1, 'seq_num', '00')
//

dw_main.SetRedraw(False)
dw_main.SetFocus()
dw_main.SetColumn('seq_num')
dw_main.SetColumn('reinquire_date')
dw_main.SelectText(4, 2)
dw_main.SetRedraw(True)



end event

on w_pa_summary_open.create
int iCurrent
call super::create
this.dw_main=create dw_main
this.dw_customer_inq=create dw_customer_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
this.Control[iCurrent+2]=this.dw_customer_inq
end on

on w_pa_summary_open.destroy
call super::destroy
destroy(this.dw_main)
destroy(this.dw_customer_inq)
end on

event close;call super::close;Destroy(iu_orp204)
end event

event ue_base_ok;call super::ue_base_ok;

string   ls_date_type_ind, &
         ls_customer_id,&
			ls_customer_name,&
			ls_today,&
			ls_reinquire_date,&
			ls_seq_num,&
			ls_last_seq_num,&
			ls_description,&
			ls_discounted_ind
			
char		lc_AvailableDate[10]	


			
			
			
dw_main.AcceptText()
dw_customer_inq.AcceptText()

//dmk
ls_customer_id = dw_customer_inq.GetItemString( 1, "customer_id")
If ls_customer_id > '          '	Then
	SELECT customers.short_name
	INTO :ls_Customer_Name
	FROM customers
	WHERE customers.customer_id = :ls_Customer_ID
	USING SQLCA;
																				
	If SQLCA.SQLCode = 100 then
		 iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
		 Return 
	ELSE
 		dw_customer_inq.setitem(1,'customer_name', ls_Customer_name)
	end if
else 
		dw_customer_inq.setitem(1,'customer_name'," ")
end if
	

ls_date_type_ind = dw_main.GetItemString( 1, "date_type_ind")
//ls_customer_id = dw_customer_inq.GetItemString( 1, "customer_id")
//ls_customer_name = dw_customer_inq.GetItemString( 1, "customer_name")
ls_seq_num = dw_main.GetItemString( 1, "seq_num")
ls_last_seq_num = dw_main.GetItemString( 1, "last_seq_num")
ls_description = dw_main.GetItemString(1, 'description')
//added 11-12 jac
ls_discounted_ind = dw_main.GetItemString( 1, "discounted_ind")
//
lc_AvailableDate = STRING(dw_main.&
		GetItemDate(1, "reinquire_date"), "YYYY/MM/DD")
		


ls_today = STRING(Today(), "YYYY/MM/DD")






iw_parent.event ue_set_data('customer_id',ls_customer_id)
iw_parent.event ue_set_data('customer_name',ls_customer_name)
iw_parent.event ue_set_data('date_type_ind',ls_date_type_ind)
iw_parent.event ue_set_data('seq_num',ls_seq_num)
iw_parent.event ue_set_data('last_seq_num',ls_last_seq_num)
iw_parent.event ue_set_data('availability_date',lc_AvailableDate)
//added 11-12 jac
iw_parent.event ue_set_data('discounted_ind',ls_discounted_ind)
//
IF lc_AvailableDate < ls_today THEN
	MessageBox("WRONG DATE", "Can't Inquire on a Date Prior To Today's Date")
	Return
END IF	


CloseWithReturn(This, 'OK')


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pa_summary_open
boolean visible = false
integer x = 1394
integer y = 328
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pa_summary_open
integer x = 1655
integer y = 292
integer taborder = 50
end type

event cb_base_cancel::clicked;call super::clicked;dw_main.AcceptText()
dw_main.SetItem(1, "seq_num", "00")
dw_main.ShareDataOff()
CloseWithReturn(Parent, "CANCEL")
end event

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pa_summary_open
integer x = 1650
integer y = 132
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_pa_summary_open
integer taborder = 20
end type

type dw_main from u_base_dw_ext within w_pa_summary_open
event getfocus pbm_dwnsetfocus
integer x = 23
integer y = 36
integer width = 1591
integer height = 604
integer taborder = 10
string dataobject = "d_pa-summary_open"
boolean border = false
end type

event getfocus;call super::getfocus;If This.GetColumnName() = 'reinquire_date' Then
	This.SelectText(4,2)
End if









end event

event ue_postconstructor;call super::ue_postconstructor;Char	lc_ProcessOption, &
		lc_AvailableDate[10]			

DataWindowChild	ldwc_file_number, &
						ldwc_file_descr

Integer	li_Rtn

String	ls_description_out, &
			ls_plantdata, &
			ls_detaildata, &
			ls_descr_out, &
			ls_page_descr
			
s_Error	lstr_Error

lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Retriev"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

lc_ProcessOption = "R"
lc_AvailableDate = ""
ls_page_descr = ""
ls_PlantData = ""
ls_DetailData = ""
ls_descr_out = ""

////call rpc 				
li_Rtn = iu_ws_orp4.nf_orpo81fr(lstr_Error, lc_ProcessOption, lc_AvailableDate,ls_page_descr,ls_descr_out,ls_PlantData, ls_DetailData, SQLCA.userid)

This.GetChild('seq_num', ldwc_file_number)
IF NOT ISVAlid(ldwc_file_number)Then
	MessageBox("","seq_number not valid")
	return
END IF

This.GetChild('seq_number_description', ldwc_file_descr)
IF NOT ISVAlid(ldwc_file_descr)Then
	MessageBox("","description not valid")
	return
END IF

ldwc_file_number.ImportString(ls_descr_out)
ldwc_file_number.ShareData(ldwc_file_descr)
end event

event itemchanged;call super::itemchanged;

If dwo.name = 'date_type_ind' Then
	CHOOSE CASE data
      CASE 'A' 
	   	dw_customer_inq.visible = False
    	Case 'D' 
	   	dw_customer_inq.visible = True
	END  CHOOSE
	end if
	
//	If dwo.name = 'discounted_ind' Then
//	CHOOSE CASE data
//      CASE 'Y' 
//	   	dw_customer_inq.visible = False
//    	Case 'N' 
//	   	dw_customer_inq.visible = True
//	END  CHOOSE
//	end if
end event

type dw_customer_inq from datawindow within w_pa_summary_open
integer x = 37
integer y = 332
integer width = 1467
integer height = 124
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_customer_inq_1"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;			
		String		ls_CustomerName,&
						ls_CustomerCity, &
						ls_state, &
						ls_customer_id 
			
		S_Error		lstr_Error_Info
		
	


////IF iw_frame.iu_project_functions.nf_ValidateCustomerState( Data, This.GetItemString(1,"customer_type"),&
////																				ls_CustomerName,ls_CustomerCity,ls_state) < 1 Then
////																				
////			IF lstr_error_info.se_message = "" then iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
////
////	IF iw_frame.iu_project_functions.nf_ValidateCustomerState( Data,'S', &
////																				ls_CustomerName,ls_CustomerCity,ls_state) < 1 Then
//   If dwo.name = 'customer_id' Then
//	   If data > '          ' Then
//      	ls_customer_id = data
//	
//			SELECT customers.short_name
//			INTO :ls_CustomerName
//			FROM customers
//			WHERE customers.customer_id = :ls_Customer_ID
//			USING SQLCA;
//																				
//	//IF lstr_error_info.se_message = "" then iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
//			If SQLCA.SQLCode = 100 then
//				 iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
//	 //         This.Object.customer_name.Text = ''
//			
//			    this.setitem(1,'customer_id',"          ")
////			    This.SelectText(1, Len(data))
//			    This.SetColumn('customer_id')
//				 this.setitem(1,'customer_name',"        ")									
//			    Return 1
//		   ELSE
////		   	if isnull(ls_CustomerName) THEN
////						 this.setitem(1,'customer_name',"")
////					This.Object.customer_name.Text = ''
////				else
////					This.Object.customer_name.Text = ls_CustomerName
//					this.setitem(1,'customer_name', ls_CustomerName)
//	//			end if
//			
//			end if
//		else 
//			this.setitem(1,'customer_name',"")
//		end if
//	end if
end event

