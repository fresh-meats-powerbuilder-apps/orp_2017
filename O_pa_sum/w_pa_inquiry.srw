HA$PBExportHeader$w_pa_inquiry.srw
$PBExportComments$New and inproved window
forward
global type w_pa_inquiry from w_base_sheet_ext
end type
type dw_customer from u_base_dw_ext within w_pa_inquiry
end type
type dw_main from u_base_dw_ext within w_pa_inquiry
end type
end forward

global type w_pa_inquiry from w_base_sheet_ext
integer x = 5
integer y = 32
integer width = 3557
integer height = 1868
string title = "PA Inquiry"
event ue_complete_order ( )
event ue_gensales ( )
event ue_postactivate ( )
event ue_salesorderchanged ( )
event ue_showprocesstype ( )
event ue_get_ds_salesorder ( )
event ue_reinquire ( )
event ue_ncclicked pbm_nclbuttondown
event ue_ncclickedup pbm_nclbuttonup
dw_customer dw_customer
dw_main dw_main
end type
global w_pa_inquiry w_pa_inquiry

type variables
Private:
Boolean		ib_no_gensales, &
		ib_RefreshNeeded, &
		ib_show_process_type,&
		ib_CO_Opened,&
		ib_need_close, &
		ib_header, &
		ib_open, &
		ib_modified, &
		ib_different_cust = true

String		is_CustomerOrderID, &
		is_SalesManCode,&
		is_Product_State, &
		is_plant, &
		is_ship_date, &
		is_orderid, is_message, &
		is_customer = "???", &
		is_inq_new = "INQUIRE", &
		is_description, &
		is_load_instruction, &
		is_additional_data_input, &
		is_selected_plant_array, &
		is_window_ind, &
		is_original_cust = "   ", &
		is_dept_code, &
		is_delivery_date, &
		is_division_code, &
		is_input_string, &
		is_lastcolumn, & 
		is_dest_country, &
		is_col, &
		is_colname, &
		is_save_col, &
		is_tooltip_type, &
		is_store_door, &
		is_deck_code, is_temp_string_data, is_po_num, is_corp_po, is_type_of_order
		
string is_additional_data [10]

s_error		istr_error_info

u_orp001		iu_orp001
u_orp002		iu_orp002 

u_ws_orp3		iu_ws_orp3
u_ws_orp2       iu_ws_orp2
u_ws_orp4       iu_ws_orp4


// This is a handle to the window that opened it if any
w_base_sheet	iw_Parent

DataStore	ids_Additional_Data, &
				ids_salesorders, &
				ids_age_info, &
				ids_multi_return_code, &
				ids_co_header

Double		id_task_number, &
		id_page_number

Char		ic_ind_process_option

// pjm 08/06/2014 use extended tooltip
// w_tooltip_orp	iw_tooltip 
w_tooltip_expanded iw_tooltip
application 	ia_apptool
Window			iw_This

long	il_Row, &
	il_xpos, &
	il_ypos, &
	il_selected_row, &
	il_selected_plant_max = 10, &
	il_horz_scroll_pos1, &
	il_horz_scroll_pos2

integer	ii_dddw_x, &
	ii_dddw_y, &
	ii_pageno=1
end variables

forward prototypes
public function integer wf_error ()
public function integer wf_findincomplete ()
public function boolean wf_next ()
public function boolean wf_previous ()
public subroutine wf_filenew ()
public function boolean wf_del_temp_storage ()
public subroutine wf_create_input_string (ref string as_main1, ref string as_main2, ref string as_main3)
public subroutine wf_get_selected_plants (datawindow adw_datawindow)
public subroutine wf_set_selected_plants ()
public subroutine wf_default_newrow ()
public subroutine wf_set_header ()
public subroutine wf_remove_dup_rows ()
public function boolean wf_importsalesorders ()
public subroutine wf_recalculate_num_of_ltls ()
public function long wf_import_output_string (string as_main1_out, string as_main2_out, string as_main3_out, ref string as_main_info_out)
public subroutine wf_set_ltl_button ()
public function string wf_create_dup_product_string (string as_main1, string as_main2, string as_main3, string as_dup_order)
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function boolean wf_inquire ()
public subroutine wf_tooltip_age_info (string as_column, readonly long row)
public subroutine wf_tooltip (string as_column, readonly long row)
public subroutine wf_tooltip_ltl (string as_column, readonly long row)
public function integer wf_gensales ()
end prototypes

event ue_complete_order;call super::ue_complete_order;String	ls_OrderID, &
			ls_ColumnName

Window	lw_OrderDetail

u_string_functions	lu_string_function

If ClassName(GetFocus()) = 'dw_main' Then
	ls_ColumnName = dw_main.GetColumnName()
	IF Not IsNumber(Right(ls_columnName,1)) Then
		setMicroHelp("Set focus on 'Ordered' to complete an order")
		Return
	End if
	ls_OrderID = dw_main.GetItemString(1, 'order_id_' + Right(ls_ColumnName, 1))
Else
	Return
End if

If lu_string_function.nf_IsEmpty(ls_OrderID) Then return

OpenSheetWithParm(lw_OrderDetail, ls_OrderID, 'w_sales_order_detail', iw_frame, 0, &
			iw_frame.im_menu.iao_ArrangeOpen)
Do 
Loop While Yield()
lw_OrderDetail.PostEvent('ue_complete_order')
end event

event ue_gensales;call super::ue_gensales;wf_gensales()
end event

event ue_postactivate();// This will refresh the sales orders when Sales Order Detail Changes Them
Int	li_ret,&
		li_count

Datawindowchild	ldwc_status, &
						ldwc_gross_weight, &
						ldwc_load_weight, &
						ldwc_num_ltls
String	ls_string
			
dw_main.SetRedraw(False)

ls_string = ids_salesorders.Describe('datawindow.data')

iw_Frame.SetMicroHelp("Refreshing Order Numbers ...")
istr_error_info.se_event_name = 'ue_PostActivate'
//li_ret = iu_orp001.nf_orpo56ar(istr_error_info, ls_string)
li_ret = iu_ws_orp3.uf_orpo56fr(istr_error_info, ls_string)

////this line was added to test Stan Meng's crap
iw_frame.SetMicroHelp(is_message)

ids_salesorders.Reset()
ids_salesorders.importstring(ls_string)
wf_remove_dup_rows()

If li_ret <> 0 Then 
	dw_main.SetRedraw(True)
	return
End If

For li_count = 1 to 10
	dw_main.GetChild('order_id_' + String(li_count) + '_status', ldwc_status)
	dw_main.GetChild('order_id_' + String(li_count) + '_gross' , ldwc_gross_weight)
	dw_main.GetChild('order_id_' + String(li_count) + '_load', ldwc_load_weight)
	dw_main.GetChild('order_id_' + String(li_count) + '_ltls', ldwc_num_ltls)
	If ISValid (ldwc_status) Then &
	   ldwc_status.Reset()
	If IsValid (ldwc_gross_weight) Then &
		ldwc_gross_weight.Reset()
	IF IsValid(ldwc_load_weight) Then &
		ldwc_load_weight.Reset()
	If IsValid(ldwc_num_ltls) Then &
		ldwc_num_ltls.Reset()
Next

This.wf_ImportSalesOrders()
This.wf_set_ltl_button()
dw_main.SetRedraw(True)

Return
end event

event ue_salesorderchanged;call super::ue_salesorderchanged;ib_RefreshNeeded = True
This.TriggerEvent('ue_PostActivate')



end event

event ue_showprocesstype;call super::ue_showprocesstype;IF ib_show_process_type Then 
	Message.StringParm = 'YES'
ELSE
	Message.StringParm = 'NO'
END IF



end event

event ue_reinquire;Boolean		lb_rtn
String		ls_temp
Long			ll_new_row
u_string_functions	lu_string_functions

If lu_string_functions.nf_IsEmpty(dw_customer.GetItemString(1,'customer_ID')) Then 
	wf_retrieve()
else
	dw_main.AcceptText()
	lb_rtn = wf_inquire()

	ll_new_row = dw_main.InsertRow(0)
	wf_default_newrow()
	wf_set_header()
	ls_temp = dw_main.GetItemString(ll_new_row, 'detail_errors')
	If isNull(ls_temp) Then ls_temp = "                    "
	ls_temp = Left(ls_temp, 20) + &
							"  V      V      V      V      V      V      V      V      V      V"
	dw_main.SetItem(ll_new_row, 'detail_errors', ls_temp)
End If
end event

event ue_ncclicked;if ib_open then close(w_locations_popup)



end event

public function integer wf_error ();dw_main.SetRedraw(False)
dw_main.Object.microhelp1.Text = " "
dw_main.Object.microhelp2.Text = " "
dw_main.Object.microhelp3.Text = " "
dw_main.Object.microhelp4.Text = " "
dw_main.Object.microhelp5.Text = " "
dw_main.Object.microhelp6.Text = " "
dw_main.Object.microhelp7.Text = " "
dw_main.Object.microhelp8.Text = " "
dw_main.Object.microhelp9.Text = " "
dw_main.Object.microhelp10.Text = " "
dw_main.SetRedraw(true)
Return 0
end function

public function integer wf_findincomplete ();Int						li_Counter
Long						ll_row
String					ls_Status, ls_order
DataWindowChild		ldwc_status
u_string_functions	lu_string_function

If dw_main.RowCount() > 0 Then

	For li_Counter = 1 to 10
		ls_Order = dw_main.GetItemString(1, 'order_id_' + String(li_Counter))
		If lu_string_function.nf_IsEmpty(ls_Order) Then Continue
		
		dw_main.GetChild('order_id_' + String(li_Counter), ldwc_status)
		ll_Row = ids_salesorders.Find("order_id = '" + ls_Order + "'", 1, 1000)
		
		If ll_Row > 0 Then
			ls_status = ids_salesorders.GetItemString(ll_Row, 'status')
			If Trim(ls_Status) = 'INCOMPLETE' Then Return li_Counter	
		End if
	Next
	
End If

return 0
end function

public function boolean wf_next ();If ib_need_close Then
	dw_main.triggerevent("ue_next")
	Return False
Else
	Return False
End If
end function

public function boolean wf_previous ();If ib_need_close Then
	dw_main.triggerEvent("ue_previous")
	Return false
Else
	Return False
End If

end function

public subroutine wf_filenew ();Boolean		lb_rtn
Long			ll_new_row
String		ls_data_in, ls_Order, ls_temp

u_string_functions	lu_string_functions
			
//This.TriggerEvent(CloseQuery!)
//If Message.returnValue = 1 Then return 
//dw_main.Reset()
is_inq_new = "NEW"
lb_rtn = wf_retrieve()
is_inq_new = "INQUIRE"
//OpenWithParm(w_pa_inquiry_inq, This)
//ls_data_in = Message.StringParm
//If lu_string_functions.nf_IsEmpty(ls_data_in) Then return
//lu_string_functions.nf_ParseLeftRight(ls_data_in, '~x', ls_data_in, ls_Order)
//dw_customer.Reset()
//dw_customer.ImportString(ls_data_in)
//dw_customer.SetItem( 1, "sales_loc", Message.is_SmanLocation)
//IF lu_string_functions.nf_IsEmpty(dw_customer.GetItemString( 1, "trans_mode")) Then
//	dw_Customer.SetItem(1, "trans_mode", 'T')
//END IF
//dw_main.Reset()
//This.SetRedraw(True)
//dw_customer.SetItem( 1, "sales_loc", Message.is_SmanLocation)
//wf_error()
//
//is_customer = dw_customer.Object.customer_id[1]
//ll_new_row = dw_main.InsertRow(0)
//ls_temp = dw_main.GetItemString(ll_new_row, 'detail_errors')
//If isNull(ls_temp) Then ls_temp = "                    "
//ls_temp = Left(ls_temp, 20) + &
//						"  V      V      V      V      V      V      V      V      V      V"
//dw_main.SetItem(ll_new_row, 'detail_errors', ls_temp)
Return
end subroutine

public function boolean wf_del_temp_storage ();Integer	li_ret
String	ls_data_in, ls_main_in, ls_detail_Data, ls_so_info, ls_production_dates, ls_age_info
//s_error	istr_error_info
Char		lc_ind_process_option
Double	ld_task_number_in, ld_page_number_in, ld_task_number_out, ld_page_number_out

If ib_need_close Then
	ls_data_in = dw_customer.Describe("DataWindow.Data")
	ls_main_in = dw_main.Describe("DataWindow.Data")
	lc_ind_process_option = 'D'
	ld_task_number_in	= id_task_number
	
	istr_error_info.se_event_name = 'wf_retrieve'
//	li_ret = iu_orp003.nf_orpo44ar(istr_error_info,&
//											ls_data_in, &
//											ls_main_in, &
//											ic_ind_process_option, &
//											ls_detail_Data, &
//											ls_so_info, &
//											ls_age_info, &
//											ls_production_dates, &
//											ld_task_number_in, &
//											ld_page_number_in, &
//											ld_task_number_out, &
//											ld_page_number_out, &
//											is_window_ind)	
//											
	li_ret = iu_ws_orp2.nf_orpo44fr(ls_data_in, &
											ls_main_in, &
											ic_ind_process_option, &
											ls_detail_Data, &
											ls_so_info, &
											ls_age_info, &
											ls_production_dates, &
											ld_task_number_in, &
											ld_page_number_in, &
											ld_task_number_out, &
											ld_page_number_out, &
											is_window_ind, &
											istr_error_info)	
											
	ib_need_close = False
End If
If li_ret = 0 Then 
	Return True
Else
	Return False
End If
end function

public subroutine wf_create_input_string (ref string as_main1, ref string as_main2, ref string as_main3);long						ll_row_count, ll_find_row, ll_column_count
u_string_functions	lu_string_functions


ll_row_count = dw_main.rowcount()

as_main1 = ""
as_main2 = ""
as_main3 = ""

if ll_row_count = 0 then return

for ll_find_row = 1 to ll_row_count
	if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,"ordered_units"))) then
		as_main1 += '0000000000' + "~t"
	else
		as_main1 += string(dw_main.getitemnumber(ll_find_row,"ordered_units")) + "~t" 
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"sku_product_code")) then
		as_main1 += '          ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"sku_product_code") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"ordered_age")) then
		as_main1 += '  ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"ordered_age") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"ordered_uom")) then
		as_main1 += '  ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"ordered_uom") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,"sales_price"))) then
		as_main1 += '000000000' + "~t"
	else
		as_main1 += string(dw_main.getitemnumber(ll_find_row,"sales_price")) + "~t"
	end if	
	if lu_string_functions.nf_IsEmpty(string(dw_main.getitemdate(ll_find_row,"delivery_date"))) then
		as_main1 += '          ' + "~t"
	else
		as_main1 += string(dw_main.getitemdate(ll_find_row,"delivery_date")) + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"customer_id")) then
		as_main1 += '     ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"customer_id") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"div_po_num")) then
		as_main1 += '                  ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"div_po_num") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"type_of_order")) then
		as_main1 += ' ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"type_of_order") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,"line_number"))) then
		as_main1 += '    ' + "~t"
	else
		as_main1 += string(dw_main.getitemnumber(ll_find_row,"line_number")) + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"division_code")) then
		as_main1 += '  ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"division_code") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"line_status")) then
		as_main1 += ' ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"line_status") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"sales_order_id")) then
		as_main1 += '     ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"sales_order_id") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"corp_po_num")) then
		as_main1 += '                  ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"corp_po_num") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"update_flag")) then
		as_main1 += ' ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"update_flag") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"detail_errors")) then
		as_main1 += '                                                             ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"detail_errors") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"aged_ind")) then
		as_main1 += ' ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"aged_ind") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"price_override")) then
		as_main1 += ' ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"price_override") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"requested_plant")) then
		as_main1 += '   ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"requested_plant") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,"requested_units"))) then
		as_main1 += '          ' + "~t"
	else
		as_main1 += string(dw_main.getitemnumber(ll_find_row,"requested_units")) + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"price_units")) then
		as_main1 += '  ' + "~r~n"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"price_units") + "~t"
	end if
	if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"inc_exl_ind")) then
		as_main1 += '  ' + "~t"
	else
		as_main1 += dw_main.getitemstring(ll_find_row,"inc_exl_ind") + "~t"
	end if
	for ll_column_count = 1 to il_selected_plant_max
		if ll_column_count = il_selected_plant_max then
			if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"plant"+ string(ll_column_count))) then
				as_main1 += ' ' + "~r~n"
			else
				as_main1 += dw_main.getitemstring(ll_find_row,"plant" + string(ll_column_count)) + "~r~n"
			end if
		else
			if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,"plant"+ string(ll_column_count))) then
				as_main1 += '   ' + "~t"
			else
				as_main1 += dw_main.getitemstring(ll_find_row,"plant" + string(ll_column_count)) + "~t"
			end if
		end if
	next
	for ll_column_count = 1 to 10
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("pa_units_" + string(ll_column_count)))) then
			as_main2 += '      ' + "~t"
		else
			as_main2 += dw_main.getitemstring(ll_find_row,string("pa_units_" + string(ll_column_count))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("pa_sub_ind_" + string(ll_column_count)))) then
			as_main2 += ' ' + "~t"
		else
			as_main2 += dw_main.getitemstring(ll_find_row,string("pa_sub_ind_" + string(ll_column_count))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,string("requested_units_" + string(ll_column_count))))) then
			as_main2 += '          ' + "~t"
		else
			as_main2 += string(dw_main.getitemnumber(ll_find_row,string("requested_units_" + string(ll_column_count)))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,string("schedule_units_" + string(ll_column_count))))) then
			as_main2 += '          ' + "~t"
		else
			as_main2 += string(dw_main.getitemnumber(ll_find_row,string("schedule_units_" + string(ll_column_count)))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("short_units_" + string(ll_column_count)))) then
			as_main2 += ' ' + "~t"
		else
			as_main2 += dw_main.getitemstring(ll_find_row,string("short_units_" + string(ll_column_count))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("line_number_" + string(ll_column_count)))) then
			as_main2 += '    ' + "~t"
		else
			as_main2 += dw_main.getitemstring(ll_find_row,string("line_number_" + string(ll_column_count))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("resolve_ind_" + string(ll_column_count)))) then
			if ll_column_count = 10 then
				as_main2 += ' ' + "~r~n"
			else
				as_main2 += ' ' + "~t"
			end if
		else
			if ll_column_count = 10 then
				as_main2 += dw_main.getitemstring(ll_find_row,string("resolve_ind_" + string(ll_column_count))) + "~r~n"
			else
				as_main2 += dw_main.getitemstring(ll_find_row,string("resolve_ind_" + string(ll_column_count))) + "~t"
			end if
		end if
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("plant_" + string(ll_column_count)))) then
			as_main3 += '   ' + "~t"
		else
			as_main3 += dw_main.getitemstring(ll_find_row,string("plant_" + string(ll_column_count))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(string(dw_main.getitemnumber(ll_find_row,string("penalty_" + string(ll_column_count))))) then
			as_main3 += '         ' + "~t"
		else
			as_main3 += string(dw_main.getitemnumber(ll_find_row,string("penalty_" + string(ll_column_count)))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(string(dw_main.getitemdate(ll_find_row,string("ship_date_" + string(ll_column_count))))) then
			as_main3 += '         ' + "~t"
		else
			as_main3 += string(dw_main.getitemdate(ll_find_row,string("ship_date_" + string(ll_column_count)))) + "~t"
		end if
		if lu_string_functions.nf_IsEmpty(dw_main.getitemstring(ll_find_row,string("order_id_" + string(ll_column_count)))) then
			if ll_column_count = 10 then
				as_main3 += '     ' + "~r~n"
			else
				as_main3 += ' ' + "~t"
			end if
			
		else
			if ll_column_count = 10 then
				as_main3 += dw_main.getitemstring(ll_find_row,string("order_id_" + string(ll_column_count))) + "~r~n"
			else
				as_main3 += dw_main.getitemstring(ll_find_row,string("order_id_" + string(ll_column_count))) + "~t"
			end if
		end if
	next
//	messagebox(string(ll_find_row), as_main3)
next
end subroutine

public subroutine wf_get_selected_plants (datawindow adw_datawindow);long				ll_count
string			ls_plant
integer			li_windowx, li_windowy, li_height, li_height_H
Application		la_Application
is_selected_plant_array = ""
for ll_count = 1 to il_selected_plant_max
	ls_plant = adw_datawindow.getitemstring(il_selected_row, "plant" + string(ll_count)) + "~r~n"
	if isnull(ls_plant) then
		is_selected_plant_array += " " + "~r~n"
	else
		is_selected_plant_array += ls_plant
	end if
next
// find opening x/y positions for "Multi select drop down"

la_Application = GetApplication()

Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose
li_windowx = iw_frame.width
li_windowy = iw_frame.height
if ib_Header then
	if (iw_frame.width - (this.x + integer(dw_customer.X) + integer(dw_customer.Object.plant1.X))) > 1179 then
		ii_dddw_x = integer(dw_customer.Object.plant1.X) + 24 + this.x		
	else
		ii_dddw_x = iw_frame.width - 1179 - 30
		if ii_dddw_x < 0 then ii_dddw_x = integer(dw_customer.Object.plant1.X) + 24 + this.x
	end if
	if (iw_frame.height - (this.y + integer(dw_customer.Object.plant1.y) +  &
			integer(dw_customer.Object.plant1.height) + integer(dw_customer.y)) > (676 + 450)) then
		ii_dddw_y = integer(dw_customer.Object.plant1.y) + integer(dw_customer.Object.plant1.height) + 224 + this.y + li_height_H
	else
		ii_dddw_y = integer(dw_customer.Object.plant1.y) + 224 + this.y - 676 + li_height_H
		if ii_dddw_y < 0 then ii_dddw_y = integer(dw_customer.Object.plant1.y) + integer(dw_customer.Object.plant1.height) + &
							224 + this.y + li_height_H
	end if
else
	if ((iw_frame.width - (this.x + integer(dw_main.X) + integer(dw_main.Object.plant1.X))) - integer(dw_main.Object.DataWindow.HorizontalScrollPosition)) > 1179 then
		ii_dddw_x = integer(dw_main.Object.plant1.X) + 22 + this.x - integer(dw_main.Object.DataWindow.HorizontalScrollPosition)
	else
		ii_dddw_x = iw_frame.width - 1179 - 30
		if ii_dddw_x < 0 then ii_dddw_x = integer(dw_main.Object.plant1.X) + 22 + this.x - integer(dw_main.Object.DataWindow.HorizontalScrollPosition)
	end if
	if (iw_frame.height - (this.y + integer(dw_main.y) + ((integer(dw_main.Object.plant1.y) + &
					integer(dw_main.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(dw_main.Object.DataWindow.FirstRowOnPage) - 1)) + 840 + li_height)) > (676 + 200)) then
		ii_dddw_y = this.y + integer(dw_main.y) + ((integer(dw_main.Object.plant1.y) + &
					integer(dw_main.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(dw_main.Object.DataWindow.FirstRowOnPage) - 1))) + 840 + li_height
	else
		ii_dddw_y = (integer(dw_customer.Object.plant1.y) + 525 + this.y + (((integer(dw_main.Object.plant1.y) + &
					integer(dw_main.Object.plant1.height) + 8)) * &
					(il_selected_row - (integer(dw_main.Object.DataWindow.FirstRowOnPage) - 1))))  - 676
		if ii_dddw_y < 0 then ii_dddw_y = this.y + integer(dw_main.y) + ((integer(dw_main.Object.plant1.y) + &
					integer(dw_main.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(dw_main.Object.DataWindow.FirstRowOnPage) - 1))) + 840 + li_height
	end if
end if

////test
//messagebox("pointery", string(li_windowy))
end subroutine

public subroutine wf_set_selected_plants ();DataStore			lds_selected_plants
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt

lds_selected_plants = Create DataStore
lds_selected_plants.DataObject = 'd_plant_code'
ll_rowcount = lds_selected_plants.importstring(is_selected_plant_array)
if ib_header then
	for ll_count = 1 to il_selected_plant_max
		if ll_count <= ll_rowcount then
			dw_customer.setitem(il_selected_row, "plant" + string(ll_count) , &
												lds_selected_plants.getitemstring(ll_count, "plant"))
		else
			dw_customer.setitem(il_selected_row, "plant" + string(ll_count) , " ")
		end if
	next
	dw_main.setcolumn("ordered_units")
	dw_main.setrow(1)
	dw_main.SetFocus()
else
	for ll_count = 1 to il_selected_plant_max
		if ll_count <= ll_rowcount then
			dw_main.setitem(il_selected_row, "plant" + string(ll_count), &
											lds_selected_plants.getitemstring(ll_count, "plant"))
		else
			dw_main.setitem(il_selected_row, "plant" + string(ll_count), " ")
		end if
	next
//	dw_main.setrow(il_selected_row + 1)
//	dw_main.setcolumn("ordered_units")
//	dw_main.SetFocus()	
end if
ib_modified = false
ib_open = false
end subroutine

public subroutine wf_default_newrow ();long	ll_count, ll_rowcount

ll_rowcount = dw_main.rowcount()
for ll_count = 1 to il_selected_plant_max
	dw_main.setitem(ll_rowcount, "plant" + string(ll_count), dw_customer.getitemstring(1, "plant" + string(ll_count)))
	dw_main.setitem(ll_rowcount, "inc_exl_ind", dw_customer.getitemstring(1, "inc_exl_ind"))
next
end subroutine

public subroutine wf_set_header ();long	ll_count


if is_window_ind = '1' then return

dw_customer.setitem(1,"inc_exl_ind", dw_main.getitemstring(1, "inc_exl_ind"))
for ll_count = 1 to il_selected_plant_max
	dw_customer.setitem(1,"plant" + string(ll_count), dw_main.getitemstring(1, "plant" + string(ll_count)))
next
end subroutine

public subroutine wf_remove_dup_rows ();long		ll_rowcount, ll_count
string	ls_order1, ls_order2, ls_plant1, ls_plant2

ids_salesorders.setsort("order_id A, plant A")
ids_salesorders.sort()

ll_rowcount = ids_salesorders.rowcount()
ll_count = 1

do while ll_count < ll_rowcount
	ls_order1 = ids_salesorders.getitemstring(ll_count, "order_id")
	ls_plant1 = ids_salesorders.getitemstring(ll_count, "plant")
	ll_count += 1
	ls_order2 = ids_salesorders.getitemstring(ll_count, "order_id")
	ls_plant2 = ids_salesorders.getitemstring(ll_count, "plant")
	if ls_order1 = ls_order2 and ls_plant1 = ls_plant2 then
		ids_salesorders.DeleteRow(ll_count)
		ll_count = ll_count - 1
		ll_rowcount = ids_salesorders.rowcount()
	end if
loop
end subroutine

public function boolean wf_importsalesorders ();Long					ll_RowCount
Integer				li_count
string 				ls_debug

Datawindowchild	ldwc_status, &
						ldwc_gross_weight, &
						ldwc_load_weight, &
						ldwc_num_ltls

ls_debug = ids_salesorders.describe("DataWindow.Data")
						
For li_count = 1 to 10
	dw_main.GetChild('order_id_' + String(li_count) + '_status', ldwc_status)
  	dw_main.GetChild('order_id_' + String(li_count) + '_gross', ldwc_gross_weight)
	dw_main.GetChild('order_id_' + String(li_count) + '_load', ldwc_load_weight)
	dw_main.GetChild('order_id_' + String(li_count) + '_ltls', ldwc_num_ltls)

	ldwc_status.reset()
	ldwc_gross_weight.reset()
	ldwc_load_weight.reset()
	ldwc_num_ltls.reset()
	
	If ISValid (ldwc_status)       Then 
		ll_RowCount = ldwc_status.ImportString(ids_salesorders.Describe("DataWindow.Data"))
	End If
	If IsValid (ldwc_gross_weight) Then 
		ll_RowCount = ldwc_gross_weight.ImportString(ids_salesorders.Describe("DataWindow.Data"))
	End If
	IF IsValid (ldwc_load_weight)  Then 
		ll_RowCount = ldwc_load_weight.ImportString(ids_salesorders.Describe("DataWindow.Data"))
	End If
	If IsValid (ldwc_num_ltls) 	 Then
		ll_RowCount = ldwc_num_ltls.ImportString(ids_salesorders.Describe("DataWindow.Data"))
	End If
Next	

Return True
end function

public subroutine wf_recalculate_num_of_ltls ();Long		ll_order_ltls_1, ll_order_ltls_2, ll_order_ltls_3, ll_order_ltls_4, ll_order_ltls_5, &
			ll_order_ltls_6, ll_order_ltls_7, ll_order_ltls_8, ll_order_ltls_9, ll_order_ltls_10, &
			ll_order_ltl, &
			ll_so_count, &
			ll_dw_count, &
			ll_count

String	ls_new_so_after, &
			ls_new_order_plt, &
			ls_new_order_ship_date, &
			ls_temp, &
			ls_order_plt_1, ls_order_plt_2, ls_order_plt_3, ls_order_plt_4, ls_order_plt_5, &
			ls_order_plt_6, ls_order_plt_7, ls_order_plt_8, ls_order_plt_9, ls_order_plt_10, &
			ls_order_ship_date_1, ls_order_ship_date_2, ls_order_ship_date_3, ls_order_ship_date_4, ls_order_ship_date_5, &
			ls_order_ship_date_6, ls_order_ship_date_7, ls_order_ship_date_8, ls_order_ship_date_9, ls_order_ship_date_10, &
			ls_order_ltls_1, ls_order_ltls_2, ls_order_ltls_3, ls_order_ltls_4, ls_order_ltls_5, &
			ls_order_ltls_6, ls_order_ltls_7, ls_order_ltls_8, ls_order_ltls_9, ls_order_ltls_10

// Loop #1.
// Table Screen Columns 10 Plants & 10 Ship Dates.
For ll_dw_count = 1 to 10
	Choose Case ll_dw_count
		Case 1
				ls_order_plt_1 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_1 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 2
				ls_order_plt_2 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_2 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 3
				ls_order_plt_3 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_3 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 4
				ls_order_plt_4 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_4 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 5
				ls_order_plt_5 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_5 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 6
				ls_order_plt_6 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_6 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 7
				ls_order_plt_7 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_7 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 8
				ls_order_plt_8 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_8 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 9
				ls_order_plt_9 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_9 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
		Case 10
				ls_order_plt_10 = dw_main.GetItemString(1, String("plant_" + String(ll_dw_count)))
				ls_order_ship_date_10 = String(dw_main.GetItemDate(1, String("ship_date_" + String(ll_dw_count))))
	End Choose
Next

ll_so_count = ids_salesorders.rowcount()
ls_new_so_after = ids_salesorders.describe("DataWindow.Data")

// Loop #2.
// Search for all the new orders which is at the bottom of table & update "TRUE" screen Number Of LTL'S.
For ll_count = 1 to ll_so_count
	ls_temp = ids_salesorders.GetItemString(ll_count, 'number_of_ltls')
	If ls_temp = 'ADD' Then
		ls_new_order_plt = ids_salesorders.GetItemString(ll_count, 'plant')
		ls_new_order_ship_date = String(ids_salesorders.GetItemDate(ll_count, 'ship_date'))
		For ll_dw_count = 1 to 10
			Choose Case ll_dw_count
				Case 1
					If ls_new_order_plt = ls_order_plt_1 And &
						ls_new_order_ship_date = ls_order_ship_date_1 Then
						ll_order_ltls_1 = ll_order_ltls_1 + 1
					End If
				Case 2
					If ls_new_order_plt = ls_order_plt_2 And &
						ls_new_order_ship_date = ls_order_ship_date_2 Then
						ll_order_ltls_2 = ll_order_ltls_2 + 1
					End If
				Case 3
					If ls_new_order_plt = ls_order_plt_3 And &
						ls_new_order_ship_date = ls_order_ship_date_3 Then
						ll_order_ltls_3 = ll_order_ltls_3 + 1
					End If
				Case 4
					If ls_new_order_plt = ls_order_plt_4 And &
						ls_new_order_ship_date = ls_order_ship_date_4 Then
						ll_order_ltls_4 = ll_order_ltls_4 + 1
					End If
				Case 5
					If ls_new_order_plt = ls_order_plt_5 And &
						ls_new_order_ship_date = ls_order_ship_date_5 Then
						ll_order_ltls_5 = ll_order_ltls_5 + 1
					End If
				Case 6
					If ls_new_order_plt = ls_order_plt_6 And &
						ls_new_order_ship_date = ls_order_ship_date_6 Then
						ll_order_ltls_6 = ll_order_ltls_6 + 1
					End If
				Case 7
					If ls_new_order_plt = ls_order_plt_7 And &
						ls_new_order_ship_date = ls_order_ship_date_7 Then
						ll_order_ltls_7 = ll_order_ltls_7 + 1
					End If
				Case 8
					If ls_new_order_plt = ls_order_plt_8 And &
						ls_new_order_ship_date = ls_order_ship_date_8 Then
						ll_order_ltls_8 = ll_order_ltls_8 + 1
					End If
				Case 9
					If ls_new_order_plt = ls_order_plt_9 And &
						ls_new_order_ship_date = ls_order_ship_date_9 Then
						ll_order_ltls_9 = ll_order_ltls_9 + 1
					End If
				Case 10
					If ls_new_order_plt = ls_order_plt_10 And &
						ls_new_order_ship_date = ls_order_ship_date_10 Then
						ll_order_ltls_10 = ll_order_ltls_10 + 1
					End If
			End Choose
		Next
	Else
		ls_new_order_plt = ids_salesorders.GetItemString(ll_count, 'plant')
		ls_new_order_ship_date = String(ids_salesorders.GetItemDate(ll_count, 'ship_date'))
		For ll_dw_count = 1 to 10
			Choose Case ll_dw_count
				Case 1
					If ls_new_order_plt = ls_order_plt_1 And &
						ls_new_order_ship_date = ls_order_ship_date_1 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_1 = Long(ls_temp)
						End If
					End If
				Case 2
					If ls_new_order_plt = ls_order_plt_2 And &
						ls_new_order_ship_date = ls_order_ship_date_2 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_2 = Long(ls_temp)
						End If
					End If
				Case 3
					If ls_new_order_plt = ls_order_plt_3 And &
						ls_new_order_ship_date = ls_order_ship_date_3 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_3 = Long(ls_temp)
						End If
					End If
				Case 4
					If ls_new_order_plt = ls_order_plt_4 And &
						ls_new_order_ship_date = ls_order_ship_date_4 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_4 = Long(ls_temp)
						End If
					End If
				Case 5
					If ls_new_order_plt = ls_order_plt_5 And &
						ls_new_order_ship_date = ls_order_ship_date_5 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_5 = Long(ls_temp)
						End If
					End If
				Case 6
					If ls_new_order_plt = ls_order_plt_6 And &
						ls_new_order_ship_date = ls_order_ship_date_6 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_6 = Long(ls_temp)
						End If
					End If
				Case 7
					If ls_new_order_plt = ls_order_plt_7 And &
						ls_new_order_ship_date = ls_order_ship_date_7 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_7 = Long(ls_temp)
						End If
					End If
				Case 8
					If ls_new_order_plt = ls_order_plt_8 And &
						ls_new_order_ship_date = ls_order_ship_date_8 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_8 = Long(ls_temp)
						End If
					End If
				Case 9
					If ls_new_order_plt = ls_order_plt_9 And &
						ls_new_order_ship_date = ls_order_ship_date_9 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_9 = Long(ls_temp)
						End If
					End If
				Case 10
					If ls_new_order_plt = ls_order_plt_10 And &
						ls_new_order_ship_date = ls_order_ship_date_10 Then
						If Not ls_temp = '   ' Then
							ll_order_ltls_10 = Long(ls_temp)
						End If
					End If
			End Choose
		Next
	End If
Next

// Loop #3. 
// Make all orders the max number of LTL's for the column they fall under.
For ll_count = 1 to ll_so_count
	ls_temp = ids_salesorders.GetItemString(ll_count, 'number_of_ltls')
	If ls_temp > '   ' Then
		ls_new_order_plt = ids_salesorders.GetItemString(ll_count, 'plant')
		ls_new_order_ship_date = String(ids_salesorders.GetItemDate(ll_count, 'ship_date'))
		For ll_dw_count = 1 to 10
			Choose Case ll_dw_count
				Case 1
					If ls_order_plt_1 = ls_new_order_plt And &
						ls_order_ship_date_1 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_1))
					End If
				Case 2
					If ls_order_plt_2 = ls_new_order_plt And &
						ls_order_ship_date_2 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_2))
					End If
				Case 3
					If ls_order_plt_3 = ls_new_order_plt And &
						ls_order_ship_date_3 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_3))
					End If
				Case 4
					If ls_order_plt_4 = ls_new_order_plt And &
						ls_order_ship_date_4 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_4))
					End If
				Case 5
					If ls_order_plt_5 = ls_new_order_plt And &
						ls_order_ship_date_5 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_5))
					End If
				Case 6
					If ls_order_plt_6 = ls_new_order_plt And &
						ls_order_ship_date_6 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_6))
					End If
				Case 7
					If ls_order_plt_7 = ls_new_order_plt And &
						ls_order_ship_date_7 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_7))
					End If
				Case 8
					If ls_order_plt_8 = ls_new_order_plt And &
						ls_order_ship_date_8 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_8))
					End If
				Case 9
					If ls_order_plt_9 = ls_new_order_plt And &
						ls_order_ship_date_9 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_9))
					End If
				Case 10
					If ls_order_plt_10 = ls_new_order_plt And &
						ls_order_ship_date_10 = ls_new_order_ship_date Then
						ids_salesorders.SetItem(ll_count, "number_of_ltls", String(ll_order_ltls_10))
					End If
			End Choose
		Next
	End If
Next

ls_new_so_after = ids_salesorders.describe("DataWindow.Data")

ll_so_count = 0
ll_dw_count = 0
ll_count = 0

end subroutine

public function long wf_import_output_string (string as_main1_out, string as_main2_out, string as_main3_out, ref string as_main_info_out);long						ll_row_count, ll_find_row, ll_column_count, ll_row_formula
u_string_functions	lu_string_functions
datastore				lds_main1, lds_main2, lds_main3
string					ls_temp, ls_sales_order_id, ls_debug

ll_row_count = dw_main.rowcount()
     
lds_main1 = Create DataStore
lds_main1.DataObject = 'd_pa_inquiry_main1'
lds_main1.importstring(as_main1_out)

lds_main2 = Create DataStore
lds_main2.DataObject = 'd_pa_inquiry_main2'
lds_main2.importstring(as_main2_out)

lds_main3 = Create DataStore
lds_main3.DataObject = 'd_pa_inquiry_main3'
lds_main3.importstring(as_main3_out)

as_main_info_out = ""

ll_row_count = lds_main1.rowcount()
for ll_find_row = 1 to ll_row_count
	dw_main.setitem(ll_find_row, "ordered_units", lds_main1.getitemnumber(ll_find_row,"ordered_units"))
	dw_main.setitem(ll_find_row, "sku_product_code", lds_main1.getitemstring(ll_find_row,"sku_product_code"))
	dw_main.setitem(ll_find_row, "ordered_age", lds_main1.getitemstring(ll_find_row,"ordered_age"))
	dw_main.setitem(ll_find_row, "ordered_uom", lds_main1.getitemstring(ll_find_row,"ordered_units_uom"))
	dw_main.setitem(ll_find_row, "sales_price", lds_main1.getitemnumber(ll_find_row,"sales_price"))
	dw_main.setitem(ll_find_row, "delivery_date", lds_main1.getitemdate(ll_find_row,"delivery_date"))
	dw_main.setitem(ll_find_row, "customer_id", lds_main1.getitemstring(ll_find_row,"customer_id"))
	dw_main.setitem(ll_find_row, "div_po_num", lds_main1.getitemstring(ll_find_row,"div_po_num"))
	dw_main.setitem(ll_find_row, "type_of_order", lds_main1.getitemstring(ll_find_row,"type_of_order"))
	dw_main.setitem(ll_find_row, "line_number", lds_main1.getitemnumber(ll_find_row,"line_number"))
	dw_main.setitem(ll_find_row, "division_code", lds_main1.getitemstring(ll_find_row,"division_code"))
	dw_main.setitem(ll_find_row, "line_status", lds_main1.getitemstring(ll_find_row,"line_status"))
	dw_main.setitem(ll_find_row, "sales_order_id", lds_main1.getitemstring(ll_find_row,"sales_order_id"))
	dw_main.setitem(ll_find_row, "corp_po_num", lds_main1.getitemstring(ll_find_row,"corp_po_num"))
	dw_main.setitem(ll_find_row, "update_flag", lds_main1.getitemstring(ll_find_row,"update_flag"))
	dw_main.setitem(ll_find_row, "detail_errors", lds_main1.getitemstring(ll_find_row,"detail_errors"))
	dw_main.setitem(ll_find_row, "aged_ind", lds_main1.getitemstring(ll_find_row,"aged_ind"))
	dw_main.setitem(ll_find_row, "price_override", lds_main1.getitemstring(ll_find_row,"price_override"))
	dw_main.setitem(ll_find_row, "requested_plant", lds_main1.getitemstring(ll_find_row,"requested_plant"))
	dw_main.setitem(ll_find_row, "requested_units", lds_main1.getitemnumber(ll_find_row,"requested_units"))
	dw_main.setitem(ll_find_row, "price_units", lds_main1.getitemstring(ll_find_row,"price_units"))
	dw_main.setitem(ll_find_row, "inc_exl_ind", lds_main1.getitemstring(ll_find_row,"inc_exl_ind"))
	for ll_column_count = 1 to 10
		dw_main.setitem(ll_find_row, "plant" + string(ll_column_count), lds_main1.getitemstring(ll_find_row,"plant" + string(ll_column_count)))
	next
	for ll_column_count = 1 to 10
		ll_row_formula = ((ll_find_row - 1)* 10) + ll_column_count
		ls_temp = lds_main2.getitemstring(ll_row_formula,"pa_units_1")

//		ls_temp = lds_main2.getitemstring(ll_row_formula,"pa_units_" + string(ll_column_count))
		dw_main.setitem(ll_find_row,string("pa_units_" + string(ll_column_count)), ls_temp)
		dw_main.setitem(ll_find_row,string("pa_sub_ind_" + string(ll_column_count)), lds_main2.getitemstring(ll_row_formula,"pa_sub_ind_1"))
		dw_main.setitem(ll_find_row,string("requested_units_" + string(ll_column_count)), lds_main2.getitemnumber(ll_row_formula,"requested_units_1"))
		dw_main.setitem(ll_find_row,string("schedule_units_" + string(ll_column_count)), lds_main2.getitemnumber(ll_row_formula,"schedule_units_1"))
		dw_main.setitem(ll_find_row,string("short_units_" + string(ll_column_count)), lds_main2.getitemstring(ll_row_formula,"short_units_1"))
		dw_main.setitem(ll_find_row,string("line_number_" + string(ll_column_count)), lds_main2.getitemstring(ll_row_formula,"line_number_1"))
		dw_main.setitem(ll_find_row,string("resolve_ind_" + string(ll_column_count)), lds_main2.getitemstring(ll_row_formula,"resolve_ind_1"))
		dw_main.setitem(ll_find_row,string("plant_" + string(ll_column_count)), lds_main3.getitemstring(ll_row_formula,"plant_1"))	
		dw_main.setitem(ll_find_row,string("penalty_" + string(ll_column_count)), lds_main3.getitemnumber(ll_row_formula,"penalty_1"))	
		dw_main.setitem(ll_find_row,string("ship_date_" + string(ll_column_count)), lds_main3.getitemdate(ll_row_formula,"ship_date_1"))	
		dw_main.setitem(ll_find_row,string("order_id_" + string(ll_column_count)), lds_main3.getitemstring(ll_row_formula,"order_id_1"))	
	next	
next

return ll_row_count
end function

public subroutine wf_set_ltl_button ();Long		ll_row_count, &
			ll_ds_row_count, &
			ll_count, &
			ll_ids_count
			
String	ls_sales_order_id, &
			ls_sales_order_plant, &
			ls_ds_order_id, &
			ls_ds_num_ltls, &
			ls_ds_plant, &
			ls_debug

Date		dt_sales_order_ship_date, &
			dt_ds_ship_date
			
u_string_functions	lu_string_functions			

ll_row_count = ids_salesorders.rowcount()
ls_debug = ids_salesorders.describe("DataWindow.Data")

//MessageBox("",ls_debug)

For ll_count = 1 to 10
	ls_sales_order_id = dw_main.GetItemString(1, String("order_id_" + String(ll_count)))
	dt_sales_order_ship_date = dw_main.GetItemDate(1, String("ship_date_" + String(ll_count)))
	ls_sales_order_plant = dw_main.GetItemString(1, String("plant_" + String(ll_count)))

	Choose case ll_count  // Default LTL button to Not Visible 
		Case 1
			dw_main.object.b_so_by_cust1.Visible = False
			dw_main.object.b_so_by_cust1.Text = ""
		Case 2
			dw_main.object.b_so_by_cust2.Visible = False
			dw_main.object.b_so_by_cust2.Text = ""
		Case 3
			dw_main.object.b_so_by_cust3.Visible = False
			dw_main.object.b_so_by_cust3.Text = ""
		Case 4
			dw_main.object.b_so_by_cust4.Visible = False
			dw_main.object.b_so_by_cust4.Text = ""
		Case 5
			dw_main.object.b_so_by_cust5.Visible = False
			dw_main.object.b_so_by_cust5.Text = ""
		Case 6
			dw_main.object.b_so_by_cust6.Visible = False
			dw_main.object.b_so_by_cust6.Text = ""
		Case 7
			dw_main.object.b_so_by_cust7.Visible = False
			dw_main.object.b_so_by_cust7.Text = ""
		Case 8
			dw_main.object.b_so_by_cust8.Visible = False
			dw_main.object.b_so_by_cust8.Text = ""
		Case 9
			dw_main.object.b_so_by_cust9.Visible = False
			dw_main.object.b_so_by_cust9.Text = ""
		Case 10
			dw_main.object.b_so_by_cust10.Visible = False
			dw_main.object.b_so_by_cust10.Text = ""
	End Choose

	For ll_ids_count = 1 to ll_row_count	
		ls_ds_plant = ids_salesorders.GetItemString(ll_ids_count, 'plant')
		dt_ds_ship_date = ids_salesorders.GetItemDate(ll_ids_count, 'ship_date')
		ls_ds_num_ltls = ids_salesorders.GetItemString(ll_ids_count, 'number_of_ltls')
		ls_ds_order_id = ids_salesorders.GetItemString(ll_ids_count, 'order_id')
		
		// if order at top shouldn't be displayed, hide it
		If (ls_ds_order_id = ls_sales_order_id) And (ls_ds_num_ltls > "   ") Then
			dw_main.SetItem(1, String("order_id_" + String(ll_count)),"")
		End If
		
		If (ls_sales_order_plant = ls_ds_plant) And &
			(dt_sales_order_ship_date = dt_ds_ship_date) And &
			isNull(ls_ds_num_ltls) And &
			(lu_string_functions.nf_isempty(dw_main.GetItemSTring(1, String("order_id_" + String(ll_count))))) Then
					dw_main.SetItem(1, String("order_id_" + String(ll_count)),ls_ds_order_id)
					
		End If			
		//Make LTL button visible if ltl count > '   '

		If (ls_sales_order_plant = ls_ds_plant) And &
			(dt_sales_order_ship_date = dt_ds_ship_date) And &
			(ls_ds_num_ltls > "   ") Then
			Choose Case ll_count
				case 1
					dw_main.object.b_so_by_cust1.Visible = TRUE
					dw_main.object.b_so_by_cust1.Text = (ls_ds_num_ltls + " LTL's")
				case 2
					dw_main.object.b_so_by_cust2.Visible = TRUE
					dw_main.object.b_so_by_cust2.Text = (ls_ds_num_ltls + " LTL's")
				case 3
					dw_main.object.b_so_by_cust3.Visible = TRUE
					dw_main.object.b_so_by_cust3.Text = (ls_ds_num_ltls + " LTL's")
				case 4
					dw_main.object.b_so_by_cust4.Visible = TRUE
					dw_main.object.b_so_by_cust4.Text = (ls_ds_num_ltls + " LTL's")
				case 5
					dw_main.object.b_so_by_cust5.Visible = TRUE
					dw_main.object.b_so_by_cust5.Text = (ls_ds_num_ltls + " LTL's")
				case 6
					dw_main.object.b_so_by_cust6.Visible = TRUE
					dw_main.object.b_so_by_cust6.Text = (ls_ds_num_ltls + " LTL's")
				case 7
					dw_main.object.b_so_by_cust7.Visible = TRUE
					dw_main.object.b_so_by_cust7.Text = (ls_ds_num_ltls + " LTL's")
				case 8 
					dw_main.object.b_so_by_cust8.Visible = TRUE
					dw_main.object.b_so_by_cust8.Text = (ls_ds_num_ltls + " LTL's")
				case 9
					dw_main.object.b_so_by_cust9.Visible = TRUE
					dw_main.object.b_so_by_cust9.Text = (ls_ds_num_ltls + " LTL's")
				case 10
					dw_main.object.b_so_by_cust10.Visible = TRUE
					dw_main.object.b_so_by_cust10.Text = (ls_ds_num_ltls + " LTL's")
			End Choose
		
		End If

	Next
Next	


end subroutine

public function string wf_create_dup_product_string (string as_main1, string as_main2, string as_main3, string as_dup_order);DataStore lds_main3

String	ls_return_string, ls_column, ls_temp_string, &
			ls_product, ls_age, ls_price

Integer	li_sub, li_rowcount, li_row, li_ret

Long		ll_temp, ll_1st_pos, ll_2nd_pos

Boolean	lb_end_of_products

u_string_functions	lu_string_functions

lds_main3 = Create DataStore
lds_main3.DataObject = 'd_pa_inquiry_main3'
lds_main3.importstring(as_main3)


If GetItemString(lds_main3,1,"order_id_1") = as_dup_order Then 
	li_sub = 1
ElseIf GetItemString(lds_main3,1,"order_id_2") = as_dup_order Then 
	li_sub = 2
ElseIf GetItemString(lds_main3,1,"order_id_3") = as_dup_order Then 
	li_sub = 3
ElseIf GetItemString(lds_main3,1,"order_id_4") = as_dup_order Then 
	li_sub = 4 
ElseIf GetItemString(lds_main3,1,"order_id_5") = as_dup_order Then 
	li_sub = 5
ElseIf GetItemString(lds_main3,1,"order_id_6") = as_dup_order Then 
	li_sub = 6
ElseIf GetItemString(lds_main3,1,"order_id_7") = as_dup_order Then 
	li_sub = 7 
ElseIf GetItemString(lds_main3,1,"order_id_8") = as_dup_order Then 
	li_sub = 8
ElseIf GetItemString(lds_main3,1,"order_id_9") = as_dup_order Then 
	li_sub = 9
ElseIf GetItemString(lds_main3,1,"order_id_10") = as_dup_order Then 
	li_sub = 10 
End If

li_row = 1
lb_end_of_products = False

as_main1 += '~r~n'
as_main2 += '~r~n'
as_main3 += '~r~n'

DO UNTIL (li_row = 99) or (lb_end_of_products = True)
	
	ls_temp_string = lu_string_functions.nf_gettoken(as_main1,'~r~n')
	ll_1st_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_row - 1) * 32 + 1) + 1
	ll_2nd_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_row - 1) * 32 + 2) - 1
	ls_product = mid(ls_temp_string, ll_1st_pos, ll_2nd_pos - ll_1st_pos + 1)
	if ls_product > '          ' Then
		ll_1st_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_row - 1) * 32 + 2) + 1
		ll_2nd_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_row - 1) * 32 + 3) - 1
		ls_age = mid(ls_temp_string, ll_1st_pos, ll_2nd_pos - ll_1st_pos + 1)

		ll_1st_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_row - 1) * 32 + 4) + 1
		ll_2nd_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_row - 1) * 32 + 5) - 1
		ls_price = mid(ls_temp_string, ll_1st_pos, ll_2nd_pos - ll_1st_pos + 1)
		
		ls_temp_string = lu_string_functions.nf_gettoken(as_main2,'~r~n')
		ll_1st_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_sub - 1) * 7 + 2) + 1
		ll_2nd_pos = lu_string_functions.nf_npos(ls_temp_string, '~t', 1, (li_sub - 1) * 7 + 3) - 1

		ls_return_string += mid(ls_temp_string, ll_1st_pos, ll_2nd_pos - ll_1st_pos + 1) + '~t'
		ls_return_string += ls_product + '~t'
		ls_return_string += ls_age + '~t'
		ls_return_string += ls_price + '~t'
	else
		lb_end_of_products = True
	End If
	
//	ls_column = "requested_units_" + String(li_sub)
//	ls_return_string += String(lds_main2.GetItemNumber(li_row, ls_column)) + '~t'
//	ls_return_string += lds_main1.GetItemString(li_row, "ordered_age") + '~t'
Loop

Return ls_return_string
end function

public function boolean wf_update ();Int		li_ret

Long		ll_RowCount, ll_new_row

String	ls_customer_info_in, ls_temp, &
			ls_main_info_in, ls_main_info_out, &
			ls_main1_in, ls_main2_in, ls_main3_in, &
			ls_main1_out,ls_main2_out,ls_main3_out, ls_tmp, &
			ls_dup_orders, ls_dup_products
			
u_string_functions	lu_string_functions			
			
If dw_main.AcceptText() = -1 Then Return False

ls_customer_info_in = dw_customer.describe("datawindow.data")
//ls_main_info_in = dw_main.describe("datawindow.data")
wf_create_input_string(ls_main1_in,ls_main2_in,ls_main3_in)
ll_RowCount = dw_main.RowCount()
istr_error_info.se_event_name = 'wf_update'

//li_ret = iu_orp001.nf_orpo54ar_product_browse_update(istr_error_info, &
//																		ls_customer_info_in, &
//																		ls_main1_in, &
//																		ls_main2_in, &
//																		ls_main3_in, &
//																		ls_main1_out, &
//																		ls_main2_out, &
//																		ls_main3_out, &
//																		ls_dup_orders, &
//																		ls_dup_products)	

li_ret = iu_ws_orp3.uf_orpo54fr_product_browse_update(istr_error_info, &
																		ls_customer_info_in, &
																		ls_main1_in, &
																		ls_main2_in, &
																		ls_main3_in, &
																		ls_main1_out, &
																		ls_main2_out, &
																		ls_main3_out, &
																		ls_dup_orders, &
																		ls_dup_products)	


is_message = iw_frame.wf_Getmicrohelp()
This.PostEvent('ue_postactivate', 0, iw_frame.wf_GetMicroHelp())
If (li_ret = 0) Or (li_ret = 8) Then
	wf_import_output_string(ls_main1_out, ls_main2_out, ls_main3_out, ls_main_info_out)
// dw_main.Reset()
// dw_main.ImportString(ls_main_info_out)
	
	ll_RowCount = dw_main.RowCount()
//	messagebox("ll_RowCount",string(ll_RowCount))
	ls_tmp = dw_main.object.sku_product_code[ll_RowCount]
//	messagebox("ls_tmp",ls_tmp)	
	If Not lu_string_functions.nf_IsEmpty(ls_tmp)  and ll_rowcount < 98 Then
		ll_new_row = dw_main.InsertRow(0)
		wf_default_newrow()
		ls_temp = dw_main.GetItemString(ll_new_row, 'detail_errors')
		If isNull(ls_temp) Then ls_temp = "                    "
		ls_temp = Left(ls_temp, 20) + &
								"  V      V      V      V      V      V      V      V      V      V"
		dw_main.SetItem(ll_new_row, 'detail_errors', ls_temp)
	End If
		
   Return True
End If
If li_ret = 6 Then
	ls_dup_products = wf_create_dup_product_string(ls_main1_in, ls_main2_in, ls_main3_in, ls_dup_orders)	
	OpenWithParm(w_product_add_to_existing_response, ls_dup_orders + '~t' + ls_dup_orders + '~t' + 'P' + '~t' + ls_dup_products)
	ls_dup_products = Message.StringParm	
	if ls_dup_products <>  "Cancel" Then
//		li_ret = iu_orp001.nf_orpo54ar_product_browse_update(istr_error_info, &
//																			ls_customer_info_in, &
//																			ls_main1_in, &
//																			ls_main2_in, &
//																			ls_main3_in, &
//																			ls_main1_out, &
//																			ls_main2_out, &
//																			ls_main3_out, &
//																			ls_dup_orders, &
//																			ls_dup_products)	
		li_ret = iu_ws_orp3.uf_orpo54fr_product_browse_update(istr_error_info, &
																			ls_customer_info_in, &
																			ls_main1_in, &
																			ls_main2_in, &
																			ls_main3_in, &
																			ls_main1_out, &
																			ls_main2_out, &
																			ls_main3_out, &
																			ls_dup_orders, &
																			ls_dup_products)	
		This.PostEvent('ue_postactivate', 0, iw_frame.wf_GetMicroHelp())																	
		If li_ret = 0  Or li_ret = 8 Then
			wf_import_output_string(ls_main1_out, ls_main2_out, ls_main3_out, ls_main_info_out)
		End If															
	End If

	Return False
End If
Return False

end function

public function boolean wf_retrieve ();Boolean	lb_return
Long		ll_RowCount, ll_rows, ll_new_row, ll_temprow, ll_rowcount2
String	ls_data_in, ls_order, ls_other, ls_temp, ls_customer

u_string_functions	lu_string_functions

If dw_main.RowCount() > 0 Then
	If dw_main.AcceptText() = -1 Then return False		
	This.TriggerEvent(CloseQuery!)
	If Message.returnValue = 1 Then return False
End If
If Not ib_CO_Opened Then
	OpenWithParm(w_pa_inquiry_inq, This)
	ls_data_in = Message.StringParm
	If lu_string_functions.nf_IsEmpty(ls_data_in) Then return False
	lu_string_functions.nf_ParseLeftRight(ls_data_in, '~x', ls_data_in, ls_temp)
	lu_string_functions.nf_ParseLeftRight(ls_temp, '~x', ls_order, ls_other)
	dw_customer.Reset()
	dw_customer.ImportString(ls_data_in)
	dw_customer.SetItem( 1, "sales_loc", Message.is_SmanLocation)
	IF lu_string_functions.nf_IsEmpty(dw_customer.GetItemString( 1, "trans_mode")) Then
		dw_Customer.SetItem(1, "trans_mode", 'T')
	END IF
	If Not lu_string_functions.nf_IsEmpty(ls_order) Then
		dw_main.Reset()
		dw_main.ImportString( ls_order)
		If lu_string_functions.nf_IsEmpty(dw_main.object.sku_product_code[1]) Then
			dw_main.deleterow(1)
		end if
			
		ll_rowcount2 = dw_main.RowCount()
		do 
		ll_temprow = dw_main.find('IsNull(sku_product_code) or len(trim(sku_product_code)) = 0', 1, ll_rowcount2)
		If ll_temprow > 0 Then
				dw_main.deleterow(ll_temprow)
				ll_rowcount2 = dw_main.RowCount()
			End If
			If ll_temprow = ll_rowcount2 Then
				ll_temprow = 0
			End If
		loop while ll_temprow > 0

	Else
		If is_inq_new = "NEW" And ls_other = "OK" Then
			dw_main.Reset()
		End If
	End If
	dw_customer.SetItem( 1, "sales_loc", Message.is_SmanLocation)
	wf_error()
	
	ll_RowCount = dw_main.RowCount()
	ls_customer = dw_customer.Object.customer_id[1]
	This.SetRedraw(True)
	If ll_RowCount > 0 and ls_other = "OK" Then
		If Not lu_string_functions.nf_IsEmpty(dw_main.object.sku_product_code[1]) Then
			lb_return = wf_inquire()
			if lb_return = False Then Return False
			wf_set_header()
		Else
			If is_inq_new = "NEW" Then
				lb_return = wf_del_temp_storage()
			End If
		End If
	Else
		If is_inq_new = "NEW" And ls_other = "OK" Then
			lb_return = wf_del_temp_storage()
		End If
		lb_return = True
	End If
Else 
	ib_CO_Opened = False
	dw_customer.SetItem( 1, "sales_loc", Message.is_SmanLocation)
	wf_error()
	lb_return = wf_inquire()
End If
// // //
ll_RowCount = dw_main.RowCount()
If ll_RowCount > 0 Then
	If lu_string_functions.nf_IsEmpty(dw_main.object.sku_product_code[ll_RowCount]) Then
		Return lb_return
	End If
End If

if dw_main.rowcount() < 98 then

	ll_new_row = dw_main.InsertRow(0)
	wf_default_newrow()
	ls_temp = dw_main.GetItemString(ll_new_row, 'detail_errors')
	If isNull(ls_temp) Then ls_temp = "                    "
	ls_temp = Left(ls_temp, 20) + &
							"  V      V      V      V      V      V      V      V      V      V"
	dw_main.SetItem(ll_new_row, 'detail_errors', ls_temp)	
end if

Return lb_return
end function

public function boolean wf_inquire ();Int		li_ret

Double	ld_task_number_in, &
			ld_page_number_in, &
			ld_task_number_out, &
			ld_page_number_out

Long		ll_RowCount, &
			ll_temp, &
			ll_rows, ll_test

String	ls_data_in, &
			ls_so_info, &
			ls_age_info, &
			ls_Order, &
			ls_main_in,&
			ls_detail_Data, &
			ls_production_dates, &
			ls_text

Char		lc_ind_process_option

u_string_functions	lu_string_functions
u_project_functions	lu_project_functions	

If lu_string_functions.nf_IsEmpty(dw_customer.Object.customer_id[1]) Then
	SetMicroHelp("Inquire and specify a customer")
	Return False
End If
			
This.TriggerEvent(CloseQuery!)
If Message.returnValue = 1 Then return False

This.SetRedraw(False)
dw_customer.Modify("delivery_date.Edit.Format = 'yyyy-mm-dd'")

ls_data_in = dw_customer.Describe("DataWindow.Data")
ls_main_in = dw_main.Describe("DataWindow.Data")

dw_customer.Modify("delivery_date.Edit.Format = 'mm/dd/yyyy'")
If IsValid(iw_Parent) Then
	iw_parent.TriggerEvent("ue_get_data", 0, "CustOrderID")
	is_CustomerOrderID = Message.StringParm
Else
	dw_customer.SetItem(1, 'trans_mode', 'T')
End if

lc_ind_process_option = 'R'
ld_task_number_in	= id_task_number
ld_page_number_in = id_page_number

ls_text = dw_customer.getitemstring(1,'customer_id')
if ls_text = is_original_cust then
	ib_different_cust = false
else
	ib_different_cust = true
	is_original_cust = ls_text
end if
if not(ib_different_cust) and is_window_ind = '0' then
	is_window_ind = '2'
end if

ll_test = len(ls_data_in)
ll_test = len(ls_main_in)

is_window_ind='2'

//call wf_error to reset the error messages
ll_RowCount = dw_main.RowCount()
istr_error_info.se_event_name = 'wf_retrieve'
// pjm 08/08/2014 - added ls_age_info for production dates
//
//li_ret = iu_orp003.nf_orpo44ar(istr_error_info,&
//											ls_data_in, &
//											ls_main_in, &
//											lc_ind_process_option, &
//											ls_detail_Data, &
//											ls_so_info, &
//											ls_age_info, & 
//											ls_production_dates, &
//											ld_task_number_in, &
//											ld_page_number_in, &
//											ld_task_number_out, &
//											ld_page_number_out, &
//											is_window_ind)	


li_ret = iu_ws_orp2.nf_orpo44fr(ls_data_in, &
											ls_main_in, &
											lc_ind_process_option, &
											ls_detail_Data, &
											ls_so_info, &
											ls_age_info, & 
											ls_production_dates, &
											ld_task_number_in, &
											ld_page_number_in, &
											ld_task_number_out, &
											ld_page_number_out, &
											is_window_ind, &
											istr_error_info)	


id_page_number = ld_page_number_out
id_task_number = ld_task_number_out
ib_need_close = True
If (li_ret < 0) OR (li_ret = 1) OR (li_ret = 10) Then 
	This.SetRedraw(True)
	return False
END IF

If li_ret <> 1 Then
	dw_main.Reset()
	ids_salesorders.Reset()
	ll_rows = ids_salesorders.ImportString(ls_so_info)
	
	string ls_debug
	ls_debug = ids_salesorders.describe("DataWindow.Data")
	
	// pjm 08/08/2014 - logic for production dates
	ids_age_info.Reset()
	ll_rows = ids_age_info.ImportString(ls_age_info)
	
	// added to remove duplicate rows coming from the RPC
	wf_remove_dup_rows()
	ll_rows = ids_salesorders.rowcount()
	This.wf_ImportSalesOrders()
	dw_main.ImportString(ls_detail_Data)
	dw_main.ResetUpdate()
	This.wf_set_ltl_button()
End if
// pjm 09/04/2014 for second page
ii_pageno = 1
// end pjm

ls_text = dw_customer.getitemstring(1,'customer_id')

if ls_text = is_original_cust then
	ib_different_cust = ib_different_cust
else
	is_original_cust = ls_text
end if

ls_Text = lu_project_functions.nf_getdefault_tsr_for_customer (ls_text)
dw_customer.setitem( 1, "tsr", ls_Text)

This.SetRedraw(True)
ll_temp = 1105
dw_main.Object.DataWindow.HorizontalScrollSplit = ll_temp
Return True
end function

public subroutine wf_tooltip_age_info (string as_column, readonly long row);// pjm 08/08/2014 added for production dates
String	 ls_temp=""
int   li_ret
Long	ll_ids_rowcount, &
			ll_count, &
			ll_row=0
			
	if IsValid(iw_tooltip) and il_Row = Row and as_column = is_save_col Then Return // pjm 08/06/2014
	
	IF IsValid(iw_tooltip) Then
		close(iw_tooltip)
	END IF

	//	// check if we have any valid row/columns.
	ll_row = ids_age_info.Find("ai_row = " + string(row) + " and ai_col = " + as_column,1,ids_age_info.RowCount())
	If IsNull(ll_row) Or ll_row <= 0 Then return

	ll_ids_rowcount = ids_age_info.RowCount()
	For ll_count = 1 to ll_ids_rowcount
 
			ls_temp = ls_temp + String(ids_age_info.GetItemNumber(ll_count, 'ai_row')) + "~t"
			ls_temp = ls_temp + String(ids_age_info.GetItemNumber(ll_count, 'ai_col')) + '~t'
			ls_temp = ls_temp + ids_age_info.GetItemString(ll_count, 'ai_prod_date') + space(3)
			ls_temp = ls_temp + String(ids_age_info.GetItemNumber(ll_count, 'ai_days_old')) + space(7)
			ls_temp = ls_temp + String(ids_age_info.GetItemNumber(ll_count, 'ai_qty')) + "~r~n"
 
	Next
	is_description = ls_temp
	
	if len(is_description) > 0 then 
		openwithparm(iw_tooltip, iw_this, iw_frame)		
	end if
		
	//il_row = Row

	is_col = as_column
	 
end subroutine

public subroutine wf_tooltip (string as_column, readonly long row);	string		ls_from_date, &
					ls_to_date
	
	if IsValid(iw_tooltip) and il_Row = Row and as_column = is_save_col Then Return // pjm 08/06/2014
	
	IF IsValid(iw_tooltip) Then
		close(iw_tooltip)
	END IF
	
	if row < dw_main.rowcount() then
		ls_from_date = "fromdate" + as_column
		ls_to_date = "todate" + as_column
		// pjm 08/06/2014 changed format for expanded tooltip
		is_description = string(row) + "~t" + as_column + "~t" + dw_main.getitemstring(row,ls_from_date) + " to " + &
								dw_main.getitemstring(row,ls_to_date)
//		is_description = "This is a test"
		if len(is_description) > 0 then 
			is_col = as_column
			openwithparm(iw_tooltip, iw_this, iw_frame)		
		end if
	il_row = Row
	is_col = as_column
	end if
end subroutine

public subroutine wf_tooltip_ltl (string as_column, readonly long row);	
	if IsValid(iw_tooltip) and il_Row = Row and as_column = is_save_col Then Return // pjm 08/06/2014
	
	IF IsValid(iw_tooltip) Then
		close(iw_tooltip)
	END IF
	
	if row < dw_main.rowcount() then
		is_description = string(row) + "~t" + as_column + "~t" + "Click to view LTL's" 
		if len(is_description) > 0 then 
			is_col = as_column
			il_row = Row
			openwithparm(iw_tooltip, iw_this, iw_frame)		
		end if
	il_row = Row
	is_col = as_column
	end if
end subroutine

public function integer wf_gensales ();Int		li_rtn, &
         li_rowcount, &
			li_currentColumnNumber, &
			j,&
			lI_Order_Column[10]
					
Long		ll_RowCount,&
			ll_findrow,&
			ll_Number_Of_Orders,&
			ll_Import, ll_error_count, ll_new_row, ll_count
string ls_empyArray []
String	ls_customer_in, &
			ls_main_info_in, &
			ls_additional_data_in, &
			ls_main_info_out, ls_so_id,& 
			ls_so_info, &
			ls_multi_return_code_out, &
			ls_find_string,&
			ls_Temp, &
			ls_temp_additional, &
			ls_button,&
			ls_product_code,&
			ls_division,&
			ls_sman,&
			ls_customer_id,&
			ls_location, &
			ls_instruction_ind[10], &
			ls_temp2, &
			ls_main1_in, ls_main2_in, ls_main3_in, &
			ls_main1_out,ls_main2_out,ls_main3_out, ls_message, ls_temp_string, ls_protect, ls_temp_add_data_string, ls_additional_data_curr
boolean  lb_ok = true
						
DataStore	lds_new_so_info 
				
Window	lw_detail[10]

S_error	lstr_s_error

u_string_functions	lu_string_functions

lds_new_so_info = Create DataStore
lds_New_so_info.DataObject = 'd_so_choices'
ls_location = Message.is_smanlocation	

dw_main.AcceptText()
dw_Customer.AcceptText()
ll_RowCount = dw_main.RowCount()

This.SetRedraw(False)
ls_empyArray[1] = ""

ls_customer_in = dw_customer.Describe("DataWindow.Data")

lstr_s_error.se_function_name = "wf_GenSales"
lstr_s_error.se_event_name = "Customer Gen"
lstr_s_error.se_procedure_name = "orpo79fr_multi_gen"

//// Loop here getting additional data
For li_currentColumnNumber = 1 to 10
	
	// Find to see if there are quantities  to generate
 	ls_find_string = "requested_units_" + String(li_currentColumnNumber) + " > 0" 
   ll_FindRow = dw_main.Find(ls_Find_string, 1, ll_RowCount)
	IF   ll_FindRow > 0 then
		ll_Number_Of_Orders++
		lI_Order_Column[ll_Number_Of_Orders] = li_currentColumnNumber
		//// Building additional data string
		ls_temp_additional = "P~t" + String(dw_customer.GetItemDate( 1, "delivery_date"), "mm/dd/yyyy") + "~t"
		ls_temp_additional += String(dw_main.GetItemDate( 1, "ship_date_" + String( li_currentColumnNumber)),"mm/dd/yyyy") + "~t"
		ls_temp_additional += dw_main.GetItemString( 1, "plant_" + String( li_currentColumnNumber)) + "~t"
		ls_customer_id = dw_customer.GetItemString( 1, "customer_id")
		ls_temp_additional += ls_customer_id + "~tU~t"
		ls_temp_additional += dw_customer.GetItemString( 1, "trans_mode") + '~t'
		ls_Temp = dw_main.GetItemString(ll_FindRow, 'division_code')
		
		If lu_string_functions.nf_IsEmpty(ls_temp) Then 
			ls_product_code = dw_main.GetItemString(ll_FindRow, &
				'sku_product_code')
			ls_product_code += Space(10 - Len(Trim(ls_product_code)))		

			Select sku_products.product_division
			into :ls_division
			From sku_products
			where sku_products.sku_product_code = :ls_product_code
			using SQLCA;
	
			If lu_string_functions.nf_IsEmpty(ls_Temp) Then ls_Temp = ' '
		End IF
		 ls_temp_additional += ls_Temp + '~t'
		
		//Set div to temp string. 
		//check if it's null and if it's empty set to "            " and append
		ls_temp = dw_main.GetItemString( ll_FindRow, 'div_po_num')
		If lu_string_Functions.nf_IsEmpty(ls_temp) then ls_temp = "                  "
		ls_temp_additional += ls_Temp + "~t"
//		Always retrieve salesman code		
//		IF lu_string_functions.nf_IsEmpty(is_SalesManCode) Then 
			
   		ls_product_code = dw_main.GetItemString(ll_FindRow, &
			'sku_product_code')
			ls_product_code += Space(10 - Len(Trim(ls_product_code)))		

			Select sku_products.product_division
			into :ls_division
			From sku_products
			where sku_products.sku_product_code = :ls_product_code
			using SQLCA;
	
			If lu_string_functions.nf_IsEmpty(ls_Temp) Then 
				ls_division = '11'
			ELSE
				ls_division = ls_Temp
			END IF
			
			Select customer_defaults.sman_code
			into :ls_sman
			from customer_defaults
			where customer_id = :ls_customer_id AND
			      sales_location = :ls_location AND
					sales_division = :ls_division;
					
			if lu_string_functions.nf_IsEmpty(ls_sman) Then 
				is_salesmancode = Message.is_salesperson_code
			Else
				is_salesmancode = ls_sman
			END IF	  
//		End IF
		ls_temp_additional += is_salesmancode + "~t"
		ls_temp = dw_main.GetItemString( ll_FindRow, 'corp_po_num')
		IF lu_string_Functions.nf_IsEmpty(ls_temp) then ls_temp = "                  "
			ls_temp_additional +=ls_Temp
			
		
		if is_additional_data[li_currentColumnNumber] = ls_empyArray[1] then
			ls_additional_data_curr = ""
		else
			ls_additional_data_curr = is_additional_data[li_currentColumnNumber]
		end if
			
		if lu_string_functions.nf_IsEmpty(ls_additional_data_curr) Then 
			is_additional_data_input = ls_temp_additional
		else
			ls_temp_add_data_string = is_additional_data_input
			ls_temp_additional += "~t" + ls_additional_data_curr    //is_temp_string_data
			is_additional_data_input = ls_temp_additional
		end if
		ls_additional_data_curr = ""
			
		
		//is_additional_data_input = ls_temp_additional
		
		
			

		// END Building additional data strin
		//Open Additonal Data
		OpenWithParm( w_additional_data, this)
		ls_temp_additional = is_additional_data_input
		lu_string_functions.nf_parseleftright ( ls_temp_additional,  "~x", ls_temp_additional, ls_button )
		is_additional_data[li_currentColumnNumber] = ls_temp_additional
		is_temp_string_data = is_additional_data[li_currentColumnNumber]
		
//		ls_temp_string =  Mid(is_temp_string_data, 62, 18)
//		lu_string_functions.nf_parseleftright(ls_temp_string, "~t", is_po_num, ls_temp_string)	
//		dw_main.SetItem(ll_FindRow, 'div_po_num', is_po_num)
//		
//		ls_temp_string = Mid(is_temp_string_data, 121, 18)
//		lu_string_functions.nf_parseleftright(ls_temp_string, "~t", is_corp_po, ls_temp_string)
//		dw_main.SetItem(ll_FindRow, 'corp_po_num', is_corp_po)
		
//		ls_temp_string = Mid(is_temp_string_data, 102, 8)
//		//lu_string_functions.nf_parseleftright(ls_temp_string, "~t", is_type_of_order, ls_temp_string)
//		dw_main.SetItem(ll_FindRow, 'type_of_order', ls_temp_string)
		
		
		
		
		
		//dw_main.SetItem(ll_FindRow, 'div_po_num', Mid(is_temp_string_data, 61, 18))
		
		IF ls_temp_additional <> 'Cancel' Then
			ls_instruction_ind[li_currentColumnNumber] = mid(ls_temp_additional,len(ls_temp_additional) - 1,1)
			ls_temp = left(ls_temp_additional, len(ls_temp_additional))
			ls_temp += '~t' + mid(ls_temp_additional,len(ls_temp_additional),1)
			//dmk sr5571
			ls_additional_data_in += ls_temp_additional + "~t" + is_load_instruction + '~t' + is_dept_code + '~t' + is_dest_country + '~t' &
								+ is_store_door + '~t' + is_deck_code + "~r~n"
		ELSE
			EXIT
		END IF
	Else
// sem addition 10-02-97		
	ls_additional_data_in += '~r~n'
	End If
Next

//Checking to see if there is anything to generate
If lu_string_functions.nf_IsEmpty(ls_additional_data_in) Then 
	destroy 	lds_new_so_info
	Return 1
END IF

wf_error()
//ls_main_info_in = dw_main.Describe("DataWindow.Data")
wf_create_input_string(ls_main1_in,ls_main2_in,ls_main3_in)
dw_main.SetRedraw(False)

//messagebox("main1",ls_main1_in)
//messagebox("main2",ls_main2_in)
//messagebox("main3",ls_main3_in)

//li_rtn = iu_orp002.nf_orpo79br_multi_gen(lstr_s_error, &
//										  ls_customer_in, &
//										  ls_main1_in, &
//										  ls_main2_in, &
//										  ls_main3_in, &
//										  ls_additional_data_in, &
//										  ls_main1_out, & 
//										  ls_main2_out, & 
//										  ls_main3_out, & 
//										  ls_so_info, &
//										  ls_multi_return_code_out)

li_rtn = iu_ws_orp4.nf_orpo79fr(lstr_s_error, &
										  ls_customer_in, &
										  ls_main1_in, &
										  ls_main2_in, &
										  ls_main3_in, &
										  ls_additional_data_in, &
										  ls_main1_out, & 
										  ls_main2_out, & 
										  ls_main3_out, & 
										  ls_so_info, &
										  ls_multi_return_code_out)

li_rtn = ids_salesorders.importstring(ls_so_info)

wf_recalculate_num_of_ltls()

// added to remove duplicate rows coming from the RPC
wf_remove_dup_rows()
li_rtn = ids_salesorders.rowcount()
wf_importsalesorders()
//Create datastore to handle the multi return code and messages
ids_multi_return_code = create datastore 
ids_multi_return_code.dataobject = "d_multi_return"

lds_New_so_info.reset()
ll_Import = lds_New_so_info.ImportString(ls_so_info)

//find orders generated to display errors
ls_message = ""
ll_rowcount = ids_multi_return_code.importstring(ls_multi_return_code_out)
For ll_error_count = 1 to ll_RowCount
	dw_main.Modify("microhelp"+string(lI_Order_Column[ll_Error_count])+".Text='"&
						+ids_multi_return_code.getitemString(ll_error_count,"message")+"'")

// commented the if out because Deb T didn't want to see the error codes in message.						
						
//	if ids_multi_return_code.getitemnumber(ll_error_count,"return_code") = 0 then
//		ls_message += left(ids_multi_return_code.getitemstring(ll_error_count,"message"),20)
		ls_message += ids_multi_return_code.getitemstring(ll_error_count,"message")
//	else
//		lb_ok = false
//	end if
Next	

if lb_ok then
	iw_frame.SetMicroHelp(ls_message)
else
	iw_frame.SetMicroHelp(ls_multi_return_code_out)
end if

li_rtn = wf_import_output_string(ls_main1_out, ls_main2_out, ls_main3_out, ls_main_info_out)
//dw_main.Reset()
//li_rtn = dw_main.Importstring(ls_main_info_out)	
//unstring the error indicator for each new additional_data window
For j = 1 to ll_rowcount 
	li_rtn = ids_multi_return_code.getitemnumber(j, "return_code")
	Choose Case li_rtn
		Case 2, 5, 6
		//Set Order for this sales order.
			dw_main.SelectRow(0, False)
			This.SetRedraw(True)
			li_rowcount = dw_main.rowcount()
		//getting sales order id to open sales order detail
			ls_so_id = lds_New_so_info.GetItemString(j,"order_id")
			dw_main.SetItem(1, "order_id_" +String(lI_Order_Column[j]) , ls_so_id)
			
		// Reset update.
			dw_main.ResetUpdate()
			dw_main.SelectRow(0, False)
		
			This.SetRedraw(True)
			if (ls_instruction_ind[j] = 'Y') and (li_rtn = 6) then
				ls_so_id = ls_so_id + '~t' + 'I'
			end if
			OpenSheetWithParm(lw_detail[J], ls_so_id, 'w_sales_order_detail', iw_frame, 0, &
							iw_Frame.im_menu.iao_ArrangeOpen)
			is_additional_data[lI_Order_Column[j]] = ''

			continue
		Case 0
			is_additional_data[lI_Order_Column[j]] = ''
			dw_main.SelectRow(0, False)
			This.SetRedraw(True)
			continue
		Case Else
			dw_main.SelectRow(0, False)
			This.SetRedraw(True)
			
			continue
	End Choose
Next

ll_RowCount = dw_main.RowCount()
ls_temp2 = dw_main.getitemstring(ll_rowcount,"sku_product_code")

// the following line is place there so the window will not GPF It works not ask why. CJR
ll_rowcount = ll_rowcount

If Not lu_string_functions.nf_IsEmpty(ls_temp2) and ll_rowcount < 98 Then
	ll_rowcount = ll_rowcount
	ll_new_row = dw_main.InsertRow(0)
	wf_default_newrow()
	ls_temp = dw_main.GetItemString(ll_new_row, 'detail_errors')
	If isNull(ls_temp) Then ls_temp = "                    "
	ls_temp = Left(ls_temp, 20) + &
							"  V      V      V      V      V      V      V      V      V      V"
	dw_main.SetItem(ll_new_row, 'detail_errors', ls_temp)
End If

//For ll_count = 1 to ll_rowcount
//	ls_protect = dw_main.GetItemString(ll_count, 'detail_errors')
//	If  Mid( ls_protect , 18, 1) = 'E' then
//		dw_customer.Modify("price_override_t.Visible = '1'")
//		dw_customer.Modify("price_override.Visible = '1'")
//	end if
//Next
	
	
dw_main.ResetUpdate()
dw_main.SelectRow(0, False)
This.SetRedraw(True)
dw_main.SetRedraw(True)
This.wf_set_ltl_button()
destroy 	lds_new_so_info
//
//This.PostEvent('ue_reinquire')
//
Return 0
end function

on w_pa_inquiry.create
int iCurrent
call super::create
this.dw_customer=create dw_customer
this.dw_main=create dw_main
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_customer
this.Control[iCurrent+2]=this.dw_main
end on

on w_pa_inquiry.destroy
call super::destroy
destroy(this.dw_customer)
destroy(this.dw_main)
end on

event activate;call super::activate;IF ib_no_gensales Then
	iw_Frame.im_menu.mf_Enable('m_generatesales')
End IF
iw_Frame.im_menu.mf_Enable('m_generatesales')

iw_Frame.im_menu.mf_Enable('m_completeorder')

If IsValid(w_pa_inquiry_dddw) Then Close (w_pa_inquiry_dddw)
This.SetRedraw(True)
dw_customer.SetRedraw(True)
dw_main.SetRedraw(True)
If ib_RefreshNeeded Then
	ib_RefreshNeeded = False
	This.PostEvent('ue_PostActivate', 0, iw_frame.wf_GetMicroHelp())
End if

iw_frame.im_menu.mf_enable("m_previous")
iw_frame.im_menu.mf_enable("m_next")
iw_frame.im_menu.mf_enable("m_new")
end event

event close;call super::close;Int		li_ret

Double	ld_task_number_in, &
			ld_page_number_in, &
			ld_task_number_out, &
			ld_page_number_out

Long		ll_RowCount

String	ls_data_in, &
			ls_so_info, &
			ls_age_info, &
			ls_Order, &
			ls_main_in,&
			ls_detail_Data, &
			ls_production_dates

Char		lc_ind_process_option

If ib_need_close Then
//lb_return = wf_del_temp_storage()
	ls_data_in = dw_customer.Describe("DataWindow.Data")
	ls_main_in = dw_main.Describe("DataWindow.Data")
	
	lc_ind_process_option = 'D'
	ld_task_number_in	= id_task_number
	
	//call wf_error to reset the error messages
	ll_RowCount = dw_main.RowCount()
	istr_error_info.se_event_name = 'wf_retrieve'
	
//	li_ret = iu_orp003.nf_orpo44ar(istr_error_info,&
//											ls_data_in, &
//											ls_main_in, &
//											lc_ind_process_option, &
//											ls_detail_Data, &
//											ls_so_info, &
//											ls_age_info, &
//											ls_production_dates, &
//											ld_task_number_in, &
//											ld_page_number_in, &
//											ld_task_number_out, &
//											ld_page_number_out, &
//											is_window_ind)	
										
	li_ret = iu_ws_orp2.nf_orpo44fr(ls_data_in, &
											ls_main_in, &
											lc_ind_process_option, &
											ls_detail_Data, &
											ls_so_info, &
											ls_age_info, &
											ls_production_dates, &
											ld_task_number_in, &
											ld_page_number_in, &
											ld_task_number_out, &
											ld_page_number_out, &
											is_window_ind, &
											istr_error_info)	
																						
											
End IF
IF IsValid(iw_tooltip) Then close(iw_tooltip)
IF IsValid(iu_orp001) Then Destroy iu_orp001
IF IsValid(iu_orp002) Then Destroy iu_orp002 
IF IsValid(iu_orp003) Then Destroy iu_orp003
IF IsValid(iu_ws_orp3) Then Destroy iu_ws_orp3
IF IsValid(iu_ws_orp2) Then Destroy iu_ws_orp2
IF IsValid(iu_ws_orp4) Then Destroy iu_ws_orp4
Destroy  ids_additional_data
Destroy  ids_salesorders
Destroy 	ids_multi_return_code
// pjm 08/08/2014 added for production dates
Destroy  ids_age_info
end event

event closequery;call super::closequery;// This would not allow the user to close the window when there are orders incomplete

Int	li_ret

li_ret = This.wf_findincomplete()
If li_ret > 0 Then
	If KeyDown(KeyControl!) And KeyDown(KeyAlt!) And KeyDown(KeyShift!) Then 
		Message.returnValue = 0
	Else
		dw_main.SetColumn('order_id_' + String(li_ret))
		MessageBox( "Incomplete Order", "There are outstanding incomplete~r~n" + &
						"orders pending.  You must complete~r~n" + &
						"all outstanding incomplete orders.", Exclamation!)
		dw_main.SetFocus()
		Message.ReturnValue = 1
		Return 1
	End if
	return
End if

end event

event open;call super::open;iw_Parent = Message.PowerObjectParm
ib_need_close = False
If IsValid(iw_Parent) Then
	ib_no_gensales = False
	ib_CO_Opened = True
Else
	ib_CO_Opened = False
	ib_no_gensales = True
End If
iw_Frame.im_menu.mf_Enable('m_generatesalesorder')

end event

event deactivate;iw_Frame.im_menu.mf_Disable('m_generatesalesorder')
iw_Frame.im_menu.mf_Disable('m_completeorder')
iw_Frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Disable("m_previous")
iw_frame.im_menu.mf_Disable("m_next")
iw_Frame.im_menu.mf_Disable('m_new')

IF IsValid(iw_tooltip) Then
	close(iw_tooltip)
END IF

If IsValid(w_pa_inquiry_dddw) Then Close(w_pa_inquiry_dddw)
end event

event ue_postopen;call super::ue_postopen;String	ls_Detail_data, &
			ls_additional_data, &
			ls_headerIncExc, ls_plant
			
u_string_functions	lu_string_function

dw_customer.SetItem(1, 'sales_loc', Message.is_smanlocation)

ids_Additional_Data = Create DataStore
ids_Additional_Data.DataObject = 'd_additional_data_rpc'

ids_salesorders = create Datastore
ids_salesorders.dataobject = 'd_so_choices'

// pjm 08/08/2014 added for production dates
ids_age_info = create Datastore
ids_age_info.dataobject = 'd_age_info'

iu_orp002 = Create u_orp002
If Message.ReturnValue = -1 Then Close(This)

iu_orp003 = Create u_orp003
If Message.ReturnValue = -1 Then Close(This)

iu_orp001 = Create u_orp001
If Message.ReturnValue = -1 Then Close(This)

iu_ws_orp3 = Create u_ws_orp3
If Message.ReturnValue = -1 Then Close(This)

iu_ws_orp2 = Create u_ws_orp2
If Message.ReturnValue = -1 Then Close(This)

iu_ws_orp4 = Create u_ws_orp4
If Message.ReturnValue = -1 Then Close(This)

iw_this	=	This

IF IsValid(iw_Parent) Then
	ib_show_process_type = True
	is_window_ind = '1'
	iw_parent.Event ue_get_data("Additional")
	ls_Additional_Data = Message.StringParm
	ids_Additional_data.Reset()

	IF ids_Additional_Data.ImportString( ls_Additional_Data) > 0 Then
		dw_customer.SetItem(1, 'delivery_date', ids_additional_data.GetItemDate(1,"schd_delv_date"))
		dw_customer.SetItem(1, 'customer_id',  ids_additional_data.GetItemString(1,"customer_id"))
		dw_customer.SetItem(1, 'processing_type', ids_additional_data.GetItemString(1,"product_state"))
	ELSE
		iw_parent.Event ue_get_data ("delivery_date")
		dw_customer.SetItem(1, 'delivery_date', Date(message.StringParm))
		iw_parent.Event ue_get_data ("customer_id")
		dw_customer.SetItem(1, 'customer_id', message.StringParm)
		dw_customer.SetItem(1, 'processing_type', 'Y')
	END IF
//	dw_customer.SetItem(1, 'div_code', '  ')
	iw_parent.Event ue_get_data ("transmode")
	dw_customer.SetItem(1, 'trans_mode', message.stringparm)

	iw_parent.Event ue_get_data("custorderid")
	is_CustomerOrderID = Message.StringParm
	if is_CustomerOrderID > " " Then
		dw_customer.SetItem(1, 'customer_order_id',  is_CustomerOrderId)
	end if

	iw_parent.Event ue_get_data ("salesman")
	is_SalesManCode = Message.StringParm
	dw_customer.SetItem(1, 'tsr', is_SalesManCode)

	iw_parent.Event ue_get_data("price_override")
	dw_customer.SetItem(1, 'price_override', Message.StringParm)

	iw_parent.Event ue_get_data("detail")
	ls_detail_data = Message.StringParm
	If Not lu_string_function.nf_IsEmpty(ls_detail_data) Then
		dw_main.Reset()
		dw_main.ImportString(ls_detail_data)
		dw_main.TriggerEvent("itemchanged")
	End if
	iw_parent.Event ue_get_data("product_state")
	is_Product_State = Message.StringParm
	dw_customer.SetItem(1, "processing_type", is_Product_State)
	
	iw_parent.Event ue_get_data("headerIncExc")
	ls_headerIncExc = Message.StringParm
	is_selected_plant_array = mid(ls_headerIncExc,2)
	ls_headerIncExc = left(ls_headerIncExc,1)
	dw_customer.SetItem(1,"inc_exl_ind",ls_headerIncExc)
	ib_header = true
	il_selected_row = 1
	wf_set_selected_plants()
Else
	is_window_ind = '0'
	//// This window was not opened from another window
	dw_customer.SetItem(1, 'trans_mode', 'T')
	dw_customer.AcceptText()
	//is_SalesManCode = Message.is_salesperson_code
End if

If Message.ReturnValue = -1 Then Close(This)

istr_Error_info.se_app_name = Message.nf_Get_App_ID()
istr_error_info.se_window_name = 'PA Inq'
istr_error_info.se_user_id = sqlca.userID

iw_frame.im_menu.m_file.m_inquire.PostEvent(Clicked!)
end event

event ue_get_data;String  ls_product_code, ls_micro_test_ind, ls_micro_test_product_found
Long	ll_RowCount, ll_Row

CHOOSE CASE Lower(as_value)
	CASE "query"
		  Message.StringParm = "sales_order_id~tMessage.StringParm~r~n"+&
		  							  "plant~tMessage.StringParm~r~n"+&
									  "dw_customer~tMessage.StringParm~r~n"+&
									  "list_sales_orders~tMessage.StringParm"
	CASE	'sales_order_id'
		Message.StringParm = dw_main.GetItemString(1, 'sales_order_id')
	CASE	'plant'
		Message.StringParm = is_plant
	CASE	'shipdate'
		Message.StringParm = is_ship_date
	CASE 'dw_customer'
		Message.StringParm = dw_customer.Describe("DataWindow.Data") + '~x' + &
									  dw_main.Describe("DataWindow.Data")
	Case 'list_sales_orders'
		String	 ls_temp, &
					ls_ds_num_ltls
		Long	ll_ids_rowcount, &
					ll_count
		ls_temp = ""
		ll_ids_rowcount = ids_salesorders.RowCount()
		For ll_count = 1 to ll_ids_rowcount
			ls_ds_num_ltls = ids_salesorders.GetItemString(ll_count, 'number_of_ltls')
			If ls_ds_num_ltls > '   ' Then
			Else
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'order_id') + "~t"
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'plant') + '~t'
				ls_temp = ls_temp + string(ids_salesorders.GetItemDate(ll_count, 'ship_date')) + "~t"
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'gross_weight') + "~t"
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'load_weight') + "~t"
//				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'number_of_ltls') + "~t"
				ls_temp = ls_temp + "~t" 
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'display_mode') + "~t"
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'status') + "~t"
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'po_number') + "~t"
				ls_temp = ls_temp + ids_salesorders.GetItemString(ll_count, 'wgt_queue_status') + "~r~n"
			End If	
		Next
		
		
		message.stringParm = trim(ls_temp)
	Case 'inquire-new'
		message.stringParm = is_inq_new
	case	"tooltip"
		Message.StringParm = is_description
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case "load_instruction"
		Message.StringParm = is_load_instruction
	case "additonal_data_input"
		Message.StringParm = is_additional_data_input
	case "selected"
		Message.StringParm = is_selected_plant_array
	case "header"
		if ib_header then
			Message.StringParm = "true"
		else
			Message.StringParm = 'false'
		end if
	case "dddwxpos"
		message.StringParm = string(ii_dddw_x)
	case "dddwypos"
		message.StringParm = string(ii_dddw_y)
	case "updateheader"
		message.StringParm = "true"
	case "updatedetail"
		message.StringParm = "true"
	case "export_country"
		Message.StringParm = is_dest_country
// pjm 08/07/2014 added for w_tooltip_expanded
	CASE 'currrow'
		Message.StringParm = String(il_row)
		iw_frame.SetMicroHelp("Row: " + message.stringparm)
	CASE 'currcol'
		Message.StringParm = is_col
// pjm end
	case "colname"
		message.Stringparm = is_colname 
	Case "micro_test_product"  //slh SR25459
		ls_micro_test_product_found = "N"
	     ll_RowCount = dw_main.RowCount()
		For ll_Row = 1 to ll_RowCount
				If dw_main.GetItemNumber(ll_row, 'requested_units_1') > 0  &
					or dw_main.GetItemNumber(ll_row, 'requested_units_2') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_3') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_4') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_5') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_6') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_7') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_8') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_9') > 0 &
					or dw_main.GetItemNumber(ll_row, 'requested_units_10') > 0 Then
					
					ls_product_code = dw_main.GetItemString(ll_Row, 'sku_product_code')
						
						SELECT micro_test_ind
						INTO :ls_micro_test_ind
						FROM sku_products 
                           WHERE sku_product_code = :ls_product_code
						USING SQLCA;
					
						If ls_micro_test_ind = "Y" Then
							ls_micro_test_product_found = "Y"
							EXIT
						End If
				End If
		Next
		Message.StringParm = ls_micro_test_product_found		
	case else
		message.StringParm = ""
END CHOOSE




end event

event ue_set_data;String 	ls_Number, ls_order_number
Long		ll_row, ll_rowcount, ll_temp

Choose Case as_data_item
	Case "order_id"
		//is_orderid is the name of the field
		ls_order_number = dw_main.GetItemString(1, is_orderid)
		dw_main.setitem(1, is_orderid, as_value)
		dw_main.setfocus()
		ls_Number = Mid(is_orderid,10)
		dw_main.SetColumn('requested_units_'+ls_Number)
		dw_main.SetRow(1)
		//
		ll_rowcount = dw_main.RowCount()
		If ll_rowcount > 0  and ls_order_number <> as_value Then
			SetNUll(ll_temp)
			For ll_row = 1 to ll_rowcount
				dw_main.setitem(ll_row, 'schedule_units_'+ls_Number, ll_temp)
				dw_main.setitem(ll_row, 'line_number_'+ls_Number, "")
				//dw_main.setitem(ll_row, 'short_units_'+ls_Number, "")
			Next
		End If
		//
	case "load_instruction"
		is_load_instruction = as_value
	case "dept_code"
		is_dept_code = as_value	
	case "additonal_data_input"
		is_additional_data_input = as_value
	case "selected"
		is_selected_plant_array = as_value
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
	case "window_ind"
		if is_window_ind = '1' then
			is_window_ind = as_value
		end if
	case "export_country"
		is_dest_country = as_value
	case "store_door"
  		is_store_door = as_value
	case "deck_code"
  		is_deck_code = as_value		
	case "corp_po_num"
		dw_main.setitem(1, 'corp_po_num', as_value)
	End Choose

end event

event ue_query;call super::ue_query;Int		li_ret

Double	ld_task_number_in, &
			ld_page_number_in, &
			ld_task_number_out, &
			ld_page_number_out

Long		ll_RowCount, &
			ll_temp, &
			ll_rows, ll_new_row, ll_size

String	ls_data_in, &
			ls_so_info, &
			ls_age_info, &
			ls_Order, &
			ls_main_in, ls_MicroHelp, &
			ls_detail_Data, ls_temp, &
			ls_production_dates, ls_text

Char		lc_ind_process_option						

Long ll_error_count
			
u_string_functions	lu_string_functions
u_project_functions	lu_project_functions	

ls_main_in = dw_main.Describe("DataWindow.Data")

ld_task_number_in	= id_task_number
ld_page_number_in = id_page_number

For ll_error_count = 1 to 10
	dw_main.Modify("microhelp"+string(ll_error_Count)+".Text=''")
Next	

ls_MicroHelp = iw_frame.wf_getmicrohelp()
if ls_MicroHelp = "INVALID NEXT OR PREVIOUS ACTION" THEN 
	RETURN
end if

ll_size = len(ls_main_in)
ll_size = len(ls_data_in)


ls_text = dw_customer.getitemstring(1,'customer_id')
if ls_text = is_original_cust then
	ib_different_cust = false
else
	ib_different_cust = true
	is_original_cust = ls_text
end if
if not(ib_different_cust) and is_window_ind = '0' then
	is_window_ind = '2'
end if

//call wf_error to reset the error messages
ll_RowCount = dw_main.RowCount()
istr_error_info.se_event_name = 'wf_retrieve'
//
//li_ret = iu_orp003.nf_orpo44ar(istr_error_info,&
//											ls_data_in, &
//											ls_main_in, &
//											ic_ind_process_option, &
//											ls_detail_Data, &
//											ls_so_info, &
//											ls_age_info, &
//											ls_production_dates, &
//											ld_task_number_in, &
//											ld_page_number_in, &
//											ld_task_number_out, &
//											ld_page_number_out, &
//											is_window_ind)	
											
li_ret = iu_ws_orp2.nf_orpo44fr(ls_data_in, &
											ls_main_in, &
											ic_ind_process_option, &
											ls_detail_Data, &
											ls_so_info, &
											ls_age_info, &
											ls_production_dates, &
											ld_task_number_in, &
											ld_page_number_in, &
											ld_task_number_out, &
											ld_page_number_out, &
											is_window_ind, &
											istr_error_info)	


ls_MicroHelp = iw_frame.wf_getmicrohelp()
if ls_MicroHelp = "INVALID NEXT OR PREVIOUS ACTION" THEN 
	RETURN
end if

											
IF li_ret < 0 OR li_ret = 10 Then 
	This.SetRedraw(True)
	return
END IF

id_page_number = ld_page_number_out
wf_set_header()
IF li_ret <> 1 AND id_page_number > 0 Then
	dw_main.Reset()
	dw_main.ImportString(ls_detail_Data)
	This.wf_ImportSalesOrders()
	dw_main.ResetUpdate()
	This.wf_set_ltl_button()
End IF

ls_text = dw_customer.getitemstring(1,'customer_id')

ls_Text = lu_project_functions.nf_getdefault_tsr_for_customer (ls_text)
dw_customer.setitem( 1, "tsr", ls_Text)

This.SetRedraw(True)
ll_temp = 1105
dw_main.Object.DataWindow.HorizontalScrollSplit = ll_temp
ll_RowCount = dw_main.RowCount()
If Not lu_string_functions.nf_IsEmpty(dw_main.object.sku_product_code[ll_RowCount]) Then
	ll_new_row = dw_main.InsertRow(0)
	wf_default_newrow()
	ls_temp = dw_main.GetItemString(ll_new_row, 'detail_errors')
	If isNull(ls_temp) Then ls_temp = "                    "
	ls_temp = Left(ls_temp, 20) + &
							"  V      V      V      V      V      V      V      V      V      V"
	dw_main.SetItem(ll_new_row, 'detail_errors', ls_temp)
End If
end event

event resize;call super::resize;//dw_main.Resize(this.width - dw_main.x - 50, this.height - dw_main.y - 105)

constant integer li_x		= 0
constant integer li_y		= 0

this.setredraw(false)
  
dw_main.width	= newwidth - (30 + li_x)
dw_main.height	= newheight - (30 + li_y)

if dw_main.width > 6430 then
	dw_main.width = 6430
end if 

// Must do because of the split horizonal bar 
dw_main.Object.DataWindow.HorizontalScrollSplit = '0'
dw_main.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_main.Object.DataWindow.HorizontalScrollSplit = '1105'
dw_main.Object.DataWindow.HorizontalScrollPosition2 =  '1105'

this.setredraw(true)
end event

event mouseup;if il_horz_scroll_pos1 > 340 THEN
	dw_main.Object.DataWindow.HorizontalScrollPosition = 340
END IF
if il_horz_scroll_pos2 < 1460 THEN
	dw_main.Object.DataWindow.HorizontalScrollPosition2 = 1460
END IF
end event

event clicked;if ib_open then close(w_locations_popup)

end event

type dw_customer from u_base_dw_ext within w_pa_inquiry
integer x = 9
integer y = 12
integer width = 1093
integer height = 484
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_pa_inquiry_header"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)

This.Object.delivery_date[1] = Today()

This.Modify('delivery_date.Protect = 1 delivery_date.BackGround.Color = 12632256 ' + &
				'customer_id.Protect = 1 customer_id.Background.Color = 12632256 ' + &
				'processing_type.Protect = 1' + 'div_code.Protect = 1 div_code.Background.Color = 12632256 ')
end event

event clicked;call super::clicked;String ls_protected
Long ll_count

If IsValid(w_pa_inquiry_dddw) Then Close(w_pa_inquiry_dddw)

is_columnname = dwo.name

choose case dwo.name
	case 'plant1'
		ib_header = true
		il_selected_row = row
		ib_open = true
		wf_get_selected_plants(this)
		OpenWithParm( w_locations_popup, parent)
	case 'price_override'
		If dw_customer.GetItemString(1, 'price_override') = 'N' then
			For ll_count = 1 to dw_main.RowCount()
				ls_protected = dw_main.GetItemString(ll_count, 'detail_errors')
				If Mid(ls_protected, 18, 1) = 'E' then
					dw_main.SetItem(ll_count, 'price_override', 'Y')
				end if
				If Mid(ls_protected, 18, 1) = 'M' then
					dw_main.SetItem(ll_count, 'price_override', 'Y')
				end if
			Next
		else 
			For ll_count = 1 to dw_main.RowCount()
				ls_protected = dw_main.GetItemString(ll_count, 'detail_errors')
				If Mid(ls_protected, 18, 1) = 'E' then
					dw_main.SetItem(ll_count, 'price_override', 'N')
				end if
				If Mid(ls_protected, 18, 1) = 'M' then
					dw_main.SetItem(ll_count, 'price_override', 'N')
				end if
			Next		
		end if
			
				
			
		
end choose
end event

event ue_postconstructor;call super::ue_postconstructor;DatawindowChild	ldwc_division, ldwc_trans_mode, ldwc_salespeople

u_project_functions	lu_project_functions

////New to filter by division
dw_customer.GetChild("div_code", ldwc_division)
lu_project_functions.nf_gettutltype &
		(ldwc_division, "div_code")

ldwc_division.InsertRow(1)
ldwc_division.SetItem(1, "type_code", ' ')
ldwc_division.SetItem(1, "type_desc", 'ALL DIVISIONS')

This.GetChild('trans_mode', ldwc_trans_mode)
lu_project_functions.nf_gettutltype(ldwc_trans_mode, 'TRANMODE')
This.SetItem(1, 'trans_mode', 'T')

This.GetChild('tsr', ldwc_salespeople)
ldwc_salespeople.Reset()  // This to remove the one line retained on save in the DW
If ldwc_salespeople.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_salespeople, &
			message.is_smanlocation)
End If

end event

event itemfocuschanged;call super::itemfocuschanged;is_columnname = dwo.name
end event

event losefocus;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = true
			il_selected_row = this.getrow()
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, parent)
	end choose
end if
end event

type dw_main from u_base_dw_ext within w_pa_inquiry
event ue_mouseup pbm_lbuttonup
integer width = 3538
integer height = 1708
integer taborder = 10
string dataobject = "d_pa_inquiry_main"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;Long						ll_row, ll_new_row, ll_rowCount
u_project_functions 	lu_project_functions
String					ls_columnname, ls_text

ls_text = data
ls_columnname = This.GetColumnName()
ll_row = row

Choose Case ls_columnname
	Case 'ordered_uom'
		If ls_text = '03' Then This.SetItem(ll_row, 'ordered_units', 1)
	Case 'sku_product_code'
		ll_rowCount = This.RowCount()
		If ll_row = ll_rowcount and ll_rowcount < 98 Then
			ll_new_row = This.InsertRow(0)
			wf_default_newrow()
			ls_columnname = This.GetItemString(ll_new_row, 'detail_errors')
			If isNull(ls_columnname) Then ls_columnname = "                    "
			ls_columnname = Left(ls_columnname, 20) + &
								"  V      V      V      V      V      V      V      V      V      V"
			This.SetItem(ll_new_row, 'detail_errors', ls_columnname)
		End If
		string 	ls_ship_customer
		Boolean	lb_Fresh
	
		ls_ship_customer	= dw_customer.GetItemString(1, "customer_id")
		lb_Fresh = (dw_customer.GetItemString(1, "processing_type") = "Y")
												
		This.SetItem( ll_Row, 'ordered_age', lu_project_functions.nf_get_age_code(Data, ls_ship_customer, lb_Fresh))
		dw_main.SetItem(ll_Row, 'sales_price', 0)
	//	

	Case 'ordered_units'
		If This.GetItemString(ll_row, 'ordered_uom') = '03' Then
			If ls_text > '1' Then
				iw_frame.SetMicroHelp('You can only have one load.')
				Return 1
			End IF
		End If
	
End Choose		
//
if (this.rowcount() < 98) then
	ib_NewRowOnChange = True
else
	ib_NewRowOnChange = False
end if
end event

event constructor;call super::constructor;//This.Modify('ordered_units.Background.Color = 12632256 ' + &
//				'ordered_units.Protect = 1 ' + &
//				'product_code.Background.Color = 12632256 ' + &
//				'product_code.Protect = 1 ' + &
//				'ordered_uom.Background.Color = 12632256 ' + &
//				'ordered_uom.Protect = 1 ' + &
//				'ordered_age.Background.Color = 12632256 ' + &
//				'ordered_age.Protect = 1 ')
				
This.InsertRow(0)

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_uom

This.GetChild('ordered_uom', ldwc_uom)
ldwc_uom.SetTransObject(SQLCA)
ldwc_uom.Retrieve('OUOM')







end event

event doubleclicked;call super::doubleclicked;String	ls_OrderID
			
Window	lw_OrderDetail

u_string_functions	lu_string_function

SetPointer(HourGlass!)
If Left(dwo.Name, 9) = 'order_id_' Then
	ls_OrderID = This.GetItemString(1, string(dwo.name))
	If lu_string_function.nf_IsEmpty(ls_OrderID) Then return
	OpenSheetWithParm(lw_OrderDetail, ls_OrderID, 'w_sales_order_detail', iw_frame, 0, &
				iw_frame.im_menu.iao_ArrangeOpen)
End if
end event

event clicked;call super::clicked;Window		lw_detail
String		ls_number

If IsValid(w_pa_inquiry_dddw) Then Close(w_pa_inquiry_dddw)
If Left(dwo.name, 9) = 'order_id_' Then
	IF IsNumber (right(dwo.name, 1) ) Then
		ls_number = mid(dwo.name,10)
		is_orderid = dwo.name
		is_plant = dw_main.GetITemString(1, 'plant_' + ls_number)
		is_ship_date = string(dw_main.GetItemdate(1, 'ship_date_' + ls_number), "yyyy/mm/dd")
		OpenWithParm(w_pa_inquiry_dddw, PARENT, PARENT)
	End IF
End if

If Left(dwo.name, 12) = 'b_so_by_cust' Then
	If IsNumber (right(dwo.name, 1) ) Then
		ls_number = mid(dwo.name,13)
  		is_orderid = dwo.name
		is_customer = dw_customer.GetItemString(1, 'customer_id')
		is_division_code = dw_customer.GetItemString(1, 'div_code')
		If is_division_code = '  ' Then
			is_division_code = dw_main.GetItemString(1, 'division_code')
		End If
		is_ship_date = String(dw_main.GetItemDate(1, 'ship_date_' + ls_number), "yyyy/mm/dd")
		is_delivery_date = String(dw_customer.GetItemDate(1, 'delivery_date'), "yyyy/mm/dd")
		is_plant = dw_main.GetITemString(1, 'plant_' + ls_number)

		ids_co_header = Create DataStore
		ids_co_header.DataObject = 'd_sales-order-by-customer_header'
		ids_co_header.Reset()
		ids_co_header.insertrow(0)
		
		ids_co_header.SetItem(1 , 2 , is_customer)
		ids_co_header.SetItem(1 , 5 , " ")
		ids_co_header.SetItem(1 , 6 , " ")
		ids_co_header.SetItem(1 , 3 , "  ")
		ids_co_header.SetItem(1 , 37, "Y")
		ids_co_header.SetItem(1 , 1 , Date(is_ship_date))
		ids_co_header.SetItem(1 , 45, Date(is_ship_date))
		ids_co_header.SetItem(1 , 4 , " ")
		ids_co_header.SetItem(1 , 40, "Y")
		ids_co_header.SetItem(1 , 41, Date(is_delivery_date))
		ids_co_header.SetItem(1 , 46, Date(is_delivery_date))
		ids_co_header.SetItem(1 , 38, "Y")
		ids_co_header.SetItem(1 , 39, "U")
		ids_co_header.SetItem(1 , 42, "Y")
		ids_co_header.SetItem(1 , 43, is_plant)
		
		is_input_string = ids_co_header.describe("DataWindow.Data")

		OpenSheetWithParm(lw_detail, is_input_string, 'w_sales-orders-by-customer', &
								iw_Frame, 0, iw_Frame.im_menu.iao_ArrangeOpen)

	End IF
End if

choose case dwo.name
	case 'plant1'
		ib_header = false
		il_selected_row = row
		ib_open = true
		wf_get_selected_plants(this)
		OpenWithParm(w_locations_popup, parent)
end choose

end event

event rbuttondown;call super::rbuttondown;m_orp_pext_popup NewMenu

NewMenu = CREATE m_orp_pext_popup

NewMenu.M_file.m_cascadeplants.enabled=False
NewMenu.M_file.m_cascadeplants.visible=False
NewMenu.m_file.m_padisplay.Enabled=False
NewMenu.m_file.m_padisplay.visible=False
NewMenu.m_file.m_pasummary.Enabled=False
NewMenu.m_file.m_pasummary.visible=False

NewMenu.m_file.m_generate.Enabled=True
NewMenu.m_file.m_completeorder.Enabled=True
NewMenu.M_file.m_reinquire.Enabled = TRUE

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu
end event

event itemfocuschanged;call super::itemfocuschanged;string	ls_message

Int		li_currentColumnNumber, &
			j, lI_Order_Column[10]
			
CHOOSE CASE dwo.name
	CASE 'order_id_1', 'requested_units_1'
		ls_message = dw_main.DESCRIBE("microhelp1.Text")
	CASE 'order_id_2','requested_units_2'
		ls_message = dw_main.DESCRIBE("microhelp2.Text")	
	CASE 'order_id_3','requested_units_3'
		ls_message = dw_main.DESCRIBE("microhelp3.Text")
	CASE 'order_id_4','requested_units_4'
		ls_message = dw_main.DESCRIBE("microhelp4.Text")	
	CASE 'order_id_5','requested_units_5'
		ls_message = dw_main.DESCRIBE("microhelp5.Text")
	CASE 'order_id_6','requested_units_6'
		ls_message = dw_main.DESCRIBE("microhelp6.Text")
	CASE 'order_id_7','requested_units_7'
		ls_message = dw_main.DESCRIBE("microhelp7.Text")	
	CASE 'order_id_8','requested_units_8'
		ls_message = dw_main.DESCRIBE("microhelp8.Text")
	CASE 'order_id_9','requested_units_9'
		ls_message = dw_main.DESCRIBE("microhelp9.Text")
	CASE 'order_id_10','requested_units_10'
		ls_message = dw_main.DESCRIBE("microhelp10.Text")
End Choose
if Len(Trim(ls_message)) > 0 then
	iw_frame.SetMicroHelp(ls_Message)
Else
	iw_frame.SetMicroHelp("Ready")
End IF

is_columnname = dwo.name
end event

event ue_next;call super::ue_next;ic_ind_process_option = 'N'
ii_pageno++  // pjm 09/04/2014
parent.PostEvent("ue_query")
end event

event ue_previous;call super::ue_previous;ic_ind_process_option = 'P'
ii_pageno --  // pjm 09/04/2014
parent.PostEvent("ue_query")
end event

event itemerror;call super::itemerror;Return 1
end event

event ue_setbusinessrule;call super::ue_setbusinessrule;//====================================
String ls_BusinessRule

Long		ll_Column
//if ISNull(This.GetItemString(Row,"detail_errors")) Then This.SetItem(Row,"detail_errors",Space(18))
//Currently these correspond to the column number, but if a column is added they will not
Choose Case name
	Case 'ordered_units'
	 	ll_column = 1
	Case 'sku_product_code'
   	ll_Column = 2
	Case 'ordered_age'
		ll_column = 3
	Case 'ordered_uom'
		ll_column = 4
	Case 'sales_price'
		ll_Column = 5
	Case 'price_override'
		ll_column = 6
END CHOOSE
ls_businessRule =This.GetItemString(row,"detail_errors")
If IsNull(ls_BusinessRule) Then 
	ls_BusinessRule = "                    " + &
							"  V      V      V      V      V      V      V      V      V      V"
End If
ls_BusinessRule = Left(ls_BusinessRule,ll_Column -1) &
						+ Value + Mid(ls_BusinessRule,ll_Column +1)
//ls_BusinessRule = Left(ls_BusinessRule, 20) + &
//							"  V      V      V      V      V      V      V      V      V      V"
This.SetItem(row,"detail_errors", ls_BusinessRule)	
Return 1

end event

event ue_mousemove;call super::ue_mousemove;//String	ls_description, &
//			ls_product, &
//			ls_line_number, &
//			ls_find_string
//			
//long		ll_rowcount
//
//String	ls_column
int li_last_pos

is_colname = String(dwo.name)

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50

IF ia_apptool.ToolBarTips = TRUE THEN
// pjm 08/05/2014 - tool tips not always showing.
//	If row > 0  AND il_Row <> Row Then
 	If row > 0 Then
 	
		choose case dwo.name
			// pjm 08/08/2014 added for production dates
			case 'pa_units_1'
				if ii_pageno = 1 Then
					
					wf_tooltip_age_info("1",row)
				else   
					wf_tooltip_age_info("11",row)
				end if
			case 'pa_units_2'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("2",row)
				else
					wf_tooltip_age_info("12",row)
				end if
			case 'pa_units_3'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("3",row)		
				else
					wf_tooltip_age_info("13",row)	
				end if
			case 'pa_units_4'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("4",row)	
				else
					wf_tooltip_age_info("14",row)
				end if
			case 'pa_units_5'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("5",row)	
				else
					wf_tooltip_age_info("15",row)
				end if
			case 'pa_units_6'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("6",row)
				else
					wf_tooltip_age_info("16",row)
				end if
			case 'pa_units_7'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("7",row)	
				else
					wf_tooltip_age_info("17",row)
				end if
			case 'pa_units_8'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("8",row)
				else
					wf_tooltip_age_info("18",row)
				end if
			case 'pa_units_9'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("9",row)
				else
					wf_tooltip_age_info("19",row)
				end if
			case 'pa_units_10'
				if ii_pageno = 1 Then
					wf_tooltip_age_info("10",row)	
				else
					wf_tooltip_age_info("20",row)	
				end if
			// end pjm
			case 'requested_units_1','schedule_units_1'
				wf_tooltip("1",row)
			case 'requested_units_2','schedule_units_2'
				wf_tooltip("2",row)
			case 'requested_units_3','schedule_units_3'
				wf_tooltip("3",row)
			case 'requested_units_4','schedule_units_4'
				wf_tooltip("4",row)
			case 'requested_units_5','schedule_units_5'
				wf_tooltip("5",row)
			case 'requested_units_6','schedule_units_6'
				wf_tooltip("6",row)
			case 'requested_units_7','schedule_units_7'
				wf_tooltip("7",row)
			case 'requested_units_8','schedule_units_8'
				wf_tooltip("8",row)
			case 'requested_units_9','schedule_units_9'
				wf_tooltip("9",row)
			case 'requested_units_10','schedule_units_10'
				wf_tooltip("10",row)
			case else
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
		End Choose
	Else
		If IsValid(iw_tooltip) Then
		   If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	END IF    
END IF

If Row = 0 AND String(dwo.name) = 'b_so_by_cust1'  Then
	If is_lastcolumn <> 'b_so_by_cust1' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If
		wf_tooltip_ltl("1",1)
//		is_description = "01~t01~tClick to view LTL's"
//		il_row = 1
//		is_col = '01'
//		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
//		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else
	If IsValid(iw_tooltip) Then
		If il_Row <> Row Or is_col <> is_save_col Then
			close(iw_tooltip)
		End If
	End If
End IF

If Row = 0 AND String(dwo.name) = 'b_so_by_cust2'  Then
	If is_lastcolumn <> 'b_so_by_cust2' Or il_Row <> Row OR is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If  // end pjm
End IF

If Row = 0 AND String(dwo.name) = 'b_so_by_cust3'  Then
	If is_lastcolumn <> 'b_so_by_cust3' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If  // end pjm
End IF

If Row = 0 AND String(dwo.name) = 'b_so_by_cust4'  Then
	If is_lastcolumn <> 'b_so_by_cust4' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If  // end pjm
End IF

If Row = 0 AND String(dwo.name) = 'b_so_by_cust5'  Then
	If is_lastcolumn <> 'b_so_by_cust5' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If  // end pjm
End IF

If Row = 0 AND String(dwo.name) = 'b_so_by_cust6'  Then
	If is_lastcolumn <> 'b_so_by_cust6' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If
End IF  // end pjm

If Row = 0 AND String(dwo.name) = 'b_so_by_cust7'  Then
	If is_lastcolumn <> 'b_so_by_cust7' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If
End IF  // end pjm

If Row = 0 AND String(dwo.name) = 'b_so_by_cust8'  Then
	If is_lastcolumn <> 'b_so_by_cust8' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If
End IF  // end pjm

If Row = 0 AND String(dwo.name) = 'b_so_by_cust9'  Then
	If is_lastcolumn <> 'b_so_by_cust9' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014 for extended tooltip
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If
End IF // end pjm

If Row = 0 AND String(dwo.name) = 'b_so_by_cust10'  Then
	If is_lastcolumn <> 'b_so_by_cust10' Or il_Row <> Row Or is_col <> is_save_col Then
		If IsValid(w_tooltip) Then
			Close(w_tooltip)
		End If

		is_description = "01~t01~tClick to view LTL's"
		If len(is_description) > 0 Then OpenWithParm(iw_tooltip, iw_this, iw_frame)
		il_row = Row
	Else
		If IsValid(iw_tooltip) Then
			If il_Row <> Row Or is_col <> is_save_col Then
				close(iw_tooltip)
			End If
		End If
	End If
Else  // pjm 08/11/2014
//	If IsValid(iw_tooltip) Then
//		If il_Row <> Row Or is_col <> is_save_col Then
//			close(iw_tooltip)
//		End If
//	End If
End IF  // end pjm

il_Row = Row
is_save_col = is_col
is_lastcolumn = String(dwo.name)
// pjm 08/11/2014 - added for extended tooltip
If IsValid(iw_tooltip) Then
	If Lower(Trim(string(dwo.name))) = 'datawindow' Then
		close(iw_tooltip)
	End If
End If


end event

event losefocus;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = false
			il_selected_row = this.getrow()
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, parent)
	end choose
end if
end event

event ue_keydown;call super::ue_keydown;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = false
			il_selected_row = this.getrow()
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, parent)
	end choose
end if
end event

event scrollhorizontal;choose case pane
	case 1
		il_horz_scroll_pos1 = scrollpos
		if il_horz_scroll_pos1 > 340 then
			this.Object.DataWindow.HorizontalScrollPosition = 340
		else
			this.Object.DataWindow.HorizontalScrollPosition = il_horz_scroll_pos1
		end if
	case 2
		il_horz_scroll_pos2 = scrollpos
		if scrollpos < 1460 then 
			this.Object.DataWindow.HorizontalScrollPosition2 = 1460
		end if
end choose
end event

