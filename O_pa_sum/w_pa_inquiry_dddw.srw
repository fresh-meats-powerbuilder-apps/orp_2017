HA$PBExportHeader$w_pa_inquiry_dddw.srw
forward
global type w_pa_inquiry_dddw from window
end type
type dw_salesorders from datawindow within w_pa_inquiry_dddw
end type
end forward

global type w_pa_inquiry_dddw from window
integer x = 672
integer y = 256
integer width = 2889
integer height = 488
windowtype windowtype = child!
event ue_postopen ( )
event ue_closethis ( )
dw_salesorders dw_salesorders
end type
global w_pa_inquiry_dddw w_pa_inquiry_dddw

type variables
w_base_sheet	iw_parent
end variables

event ue_closethis;CloseWithReturn(This, '')
end event

event open;Long			ll_rowCount				
				
String		ls_salesorder, &
				ls_plant, &
				ls_shipdate, &
				ls_test
				
integer		li_temp

iw_parent = message.powerobjectparm

u_string_functions	lu_string_function

iw_parent.Event Trigger ue_get_data('list_sales_orders')
ls_salesorder = message.StringParm
dw_salesorders.ImportString(ls_SalesOrder)

iw_parent.Event Trigger ue_get_data('shipdate')
ls_shipdate = message.StringParm

iw_parent.Event Trigger ue_get_data('plant')
ls_plant = message.StringParm

ls_test = "plant = '"+ls_plant+"' and ship_date =Date('"+ls_shipdate+"')"
// IF ls test set the filter so nothing shows
if IsNull(ls_test) then ls_test = "plant = 'xxx'" 

li_temp = dw_salesorders.SetFilter(ls_Test)
li_temp = dw_salesorders.Filter()
dw_salesorders.sort()

If ll_rowcount = 0 Then 
	dw_salesorders.SelectRow(1, True)
	Return 
End if 
If lu_string_function.nf_isempty(ls_salesorder) Then
	dw_salesorders.SelectRow(1, True)
End IF
end event

on w_pa_inquiry_dddw.create
this.dw_salesorders=create dw_salesorders
this.Control[]={this.dw_salesorders}
end on

on w_pa_inquiry_dddw.destroy
destroy(this.dw_salesorders)
end on

event deactivate;This.PostEvent("ue_closethis")


end event

type dw_salesorders from datawindow within w_pa_inquiry_dddw
integer x = 5
integer width = 2821
integer height = 472
integer taborder = 1
string dataobject = "d_so_choices"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;long ll_newrow

ll_newrow = This.InsertRow(1) 
This.SetItem(1, ("Order_id"), "")

end event

event clicked;integer li_rtn

String ls_order_id

IF This.RowCount() < 1 Then Close(Parent)

If row > 0 then 
	dw_salesorders.SelectRow(0, False)
	dw_salesorders.SelectRow(Row, True)
	ls_order_id = This.GetItemString(Row, 	"Order_id")
	iw_PARENT.Event trigger ue_set_data("order_id", ls_order_id)
	li_rtn = Close(parent)
	Return
end if















end event

