HA$PBExportHeader$w_pa_inquiry_inq.srw
forward
global type w_pa_inquiry_inq from w_base_response_ext
end type
type dw_inquire from u_base_dw_ext within w_pa_inquiry_inq
end type
type dw_order from u_base_dw_ext within w_pa_inquiry_inq
end type
end forward

global type w_pa_inquiry_inq from w_base_response_ext
integer x = 503
integer y = 364
integer width = 1504
integer height = 1300
string title = "Inquire"
long backcolor = 12632256
dw_inquire dw_inquire
dw_order dw_order
end type
global w_pa_inquiry_inq w_pa_inquiry_inq

type variables
Private:
Boolean		ib_OKToClose, &
		ib_customer_changed = false

w_base_sheet		iw_Parent
end variables

event ue_postopen;String	ls_Data, &
			ls_Inquire, &
			ls_order, ls_direction
Long		ll_row, ll_rows, ll_test
DataWindowChild	ldwc_division
u_string_functions	lu_string_function

iw_parent.event trigger ue_get_data('inquire-new')
ls_direction = message.stringparm
iw_Parent.Triggerevent ("ue_GetData", 0, "Inquire")
iw_parent.event trigger ue_get_data('dw_customer')
ls_Data = message.stringparm

This.SetRedraw(False)
lu_string_function.nf_ParseLeftRight(ls_Data, '~x', ls_Inquire, ls_Order)

If Left(ls_Inquire, 1) = '~t' Then
	dw_inquire.SetItem(1, 'delivery_date', Today())
	dw_inquire.SelectText(4, 2)
Else
	dw_inquire.Reset()
	ll_row = dw_Inquire.ImportString(ls_Inquire)
End If

If ls_direction = "NEW" Then
	dw_order.Reset()
	dw_order.InsertRow(0)
Else
	If Not lu_string_function.nf_IsEmpty(ls_Order) Then
		dw_order.Reset()
		ll_rows = dw_Order.ImportString(ls_Order)
		if ll_rows < 98 then
			dw_order.InsertRow(0)
		end if
	End If
End If
iw_parent.TriggerEvent("ue_showprocessType")
If Message.StringParm = 'YES' Then
	dw_inquire.Object.processing_type.Visible = True
	dw_Inquire.SetItem(1 ,"processing_type", 'Y')
End If
This.SetRedraw(True)
////IBDKCJR
//for ll_test = 1 to 65
//	dw_order.setitem(ll_test,"product_code","D0101DM")
//	dw_order.setitem(ll_test,"ordered_age","B")
//	dw_order.setitem(ll_test,"ordered_units",ll_test)
//	if (dw_order.rowcount() < 98) then
//		dw_order.insertrow(0)
//	end if
//next
end event

event open;call super::open;DatawindowChild	ldwc_division

u_project_functions	lu_project_functions

iw_Parent = Message.PowerObjectParm
If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

//////New to filter by division
dw_inquire.GetChild("div_code", ldwc_division)
lu_project_functions.nf_gettutltype &
		(ldwc_division, "div_code")

ldwc_division.InsertRow(1)
ldwc_division.SetItem(1, "type_code", ' ')
ldwc_division.SetItem(1, "type_desc", 'ALL DIVISIONS')

This.Title = iw_Parent.Title + " Inquire"
end event

on w_pa_inquiry_inq.create
int iCurrent
call super::create
this.dw_inquire=create dw_inquire
this.dw_order=create dw_order
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_inquire
this.Control[iCurrent+2]=this.dw_order
end on

on w_pa_inquiry_inq.destroy
call super::destroy
destroy(this.dw_inquire)
destroy(this.dw_order)
end on

on close;call w_base_response_ext::close;If Not ib_OKToClose Then
	Message.StringParm = ''
End if
end on

event ue_base_ok;Date	ldt_delivery

Long		ll_RowCount,&
			ll_RowFound

String	ls_ReturnString

boolean 	lb_product_flag

u_string_functions	lu_string_function

If dw_inquire.AcceptText() = -1 Then return 
If dw_order.AcceptText() = -1 Then return 

// Make sure Delivery date and customer are filled in
ldt_delivery = dw_inquire.GetItemDate(1, 'delivery_date')
If ldt_delivery < Today() Or IsNull(ldt_delivery) Then
	iw_Frame.SetMicroHelp("Delivery Date is a required field and must be greater than today")
	dw_inquire.SetColumn('delivery_date')
	dw_inquire.SetFocus()
	return
End if

If lu_string_function.nf_IsEmpty(dw_inquire.GetItemString(1, 'customer_id')) Then
	iw_frame.SetMicroHelp('Customer is a required field')
	dw_inquire.SetColumn('customer_id')
	dw_inquire.SetFocus()
	return
End if

ll_RowCount = 1
do until (lb_product_flag or ll_RowCount > dw_order.RowCount())
	If not lu_string_function.nf_IsEmpty(dw_order.GetItemString(ll_RowCount, 'product_code')) Then
		lb_product_flag = true
	end if
	ll_RowCount = ll_RowCount + 1
LOOP
	
if lu_string_function.nf_IsEmpty(dw_order.GetItemString(1, 'product_code')) then
	dw_order.setitem(1, 'product_code', '     ')
end if

If not lb_product_flag then
	iw_frame.SetMicroHelp('Please enter product before continuing')
	dw_order.SetColumn('product_code')
	dw_order.SetFocus()
	return
End if

ll_RowCount = dw_order.RowCount()
If ll_RowCount < 1 Then
	iw_Frame.SetMicroHelp("Please enter products before continuing")
	dw_Order.SetFocus()
	return
End if

If dw_Order.GetItemStatus(ll_RowCount, 0, Primary!) = New! Then
	dw_order.DeleteRow(ll_RowCount)
End if
ll_RowCount = dw_order.RowCount()
Do 
	ll_RowFound = dw_Order.Find( "product_code <= '        '", 1, ll_RowCount )
	IF ll_RowFOund > 0 Then dw_Order.DeleteRow( ll_RowFound)
Loop While ll_RowFound > 0

if ib_customer_changed then
	iw_parent.event trigger ue_set_data('window_ind', '0')
else
	iw_parent.event trigger ue_set_data('window_ind', '1')
end if
ib_OKToClose = True
ls_ReturnString = dw_inquire.Describe("DataWindow.Data") + '~x' + &
						dw_order.Describe("DataWindow.Data") + '~x' + "OK"
						
CloseWithReturn(This, ls_ReturnString)
return
end event

event ue_base_cancel;CloseWithReturn(This, "")
end event

event key;call super::key;IF keyflags = 0 THEN
	IF key = Keyenter! THEN
		GraphicObject which_control
		CommandButton cb_which
		string text_value
		which_control = GetFocus( ) 
		CHOOSE CASE TypeOf(which_control)
			CASE CommandButton!
				cb_which = which_control
				text_value = cb_which.Text
			CASE ELSE
				text_value = ""
		END CHOOSE
		IF text_value = '&OK' Then
			TriggerEvent("ue_base_ok")
		ElseIf text_value = '&Cancel' Then
			TriggerEvent("ue_base_cancel")
		End If
	END IF
END IF
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pa_inquiry_inq
integer x = 1207
integer y = 260
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pa_inquiry_inq
integer x = 1207
integer y = 144
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pa_inquiry_inq
integer x = 1207
integer y = 32
integer taborder = 40
boolean default = false
end type

type cb_browse from w_base_response_ext`cb_browse within w_pa_inquiry_inq
end type

type dw_inquire from u_base_dw_ext within w_pa_inquiry_inq
integer x = 14
integer y = 16
integer width = 1015
integer height = 368
integer taborder = 20
string dataobject = "d_pa_inquiry_inq"
boolean border = false
end type

on getfocus;call u_base_dw_ext::getfocus;If This.GetColumnName() = 'delivery_date' Then
	This.SelectText(4,2)
End if
end on

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;If This.GetColumnName() = 'delivery_date' Then
	This.SelectText(4,2)
End if
end on

event constructor;call super::constructor;String ls_rtn

ls_rtn = This.Modify("price_override.Visible = 0")
ls_rtn = This.Modify("price_override_t.Visible = 0")

This.InsertRow(0)

end event

event ue_dwndropdown;DataWindowChild	ldwc_customer, &
						ldwc_division	
						
u_project_functions	lu_project_functions						

This.GetChild('customer_id', ldwc_customer)
lu_project_functions.nf_getcustomers &
		(ldwc_customer, message.is_smanlocation)
end event

event itemchanged;call super::itemchanged;integer	li_row_count

IF dwo.name = 'customer_id' Then
	For li_row_count = 1 To dw_order.RowCount()
		dw_order.SetItem(li_row_count, 'sales_price', 0)
		dw_order.SetItem(li_row_count, 'inc_exl_ind', ' ')
		dw_order.SetItem(li_row_count, 'plant1', '   ')
		dw_order.SetItem(li_row_count, 'plant2', '   ')
		dw_order.SetItem(li_row_count, 'plant3', '   ')
		dw_order.SetItem(li_row_count, 'plant4', '   ')
		dw_order.SetItem(li_row_count, 'plant5', '   ')
		dw_order.SetItem(li_row_count, 'plant6', '   ')
		dw_order.SetItem(li_row_count, 'plant7', '   ')
		dw_order.SetItem(li_row_count, 'plant8', '   ')
		dw_order.SetItem(li_row_count, 'plant9', '   ')
		dw_order.SetItem(li_row_count, 'plant10', '   ')
	Next
	ib_customer_changed = true
//	iw_parent.event trigger ue_set_data('window_ind', '0')
End If

end event

type dw_order from u_base_dw_ext within w_pa_inquiry_inq
integer x = 5
integer y = 392
integer width = 1193
integer height = 808
integer taborder = 30
string dataobject = "d_pa_inquiry_order"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
ib_NewRowOnChange = True
ib_firstcolumnonnextrow = True


end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_uom

This.GetChild('ordered_uom', ldwc_uom)
ldwc_uom.SetTransObject(SQLCA)
ldwc_uom.Retrieve('OUOM')
end event

event itemchanged;call super::itemchanged;String 			ls_text

Long				ll_row

u_project_functions lu_project_functions

ll_row = row
ls_text = data
Choose Case dwo.name
	Case 'ordered_uom'
		If ls_text = '03' Then This.SetItem(ll_row, 'ordered_units', 1)
	Case 'ordered_units'
		If This.GetItemString(ll_row, 'ordered_uom') = '03' Then
			If ls_text > '1' Then
				iw_frame.SetMicroHelp('You can only have one load.')
				Return 1
			End IF
		End If
	Case 'product_code'
		string 	ls_ship_customer
		Boolean 	lb_fresh

		ls_ship_customer	= dw_inquire.GetItemString(1, "customer_id")
		lb_fresh = (dw_inquire.GetItemString(1, "processing_type") = "Y")
		
		This.SetItem( ll_Row, 'ordered_age', lu_project_functions.nf_get_age_code(Data, ls_ship_customer, lb_fresh ))	
		dw_order.SetItem(ll_Row, 'sales_price', 0)
End Choose
if (this.rowcount() < 98) then
	ib_NewRowOnChange = True
else
	ib_NewRowOnChange = False
end if
end event

event itemerror;call super::itemerror;Return 1
end event

event ue_setbusinessrule;call super::ue_setbusinessrule;String ls_BusinessRule

Long		ll_Column
//if ISNull(This.GetItemString(Row,"detail_errors")) Then This.SetItem(Row,"detail_errors",Space(18))
//Currently these correspond to the column number, but if a column is added they will not
Choose Case name
	Case 'ordered_units'
	 	ll_column = 1
	Case 'product_code'
   	ll_Column = 2
	Case 'ordered_age'
		ll_column = 3
	Case 'ordered_uom'
		ll_column = 4
	Case 'sales_price'
		ll_Column = 5
	Case 'price_override'
		ll_column = 6
END CHOOSE
ls_businessRule =This.GetItemString(row,"detail_errors")
if IsNull(ls_BusinessRule) Then ls_BusinessRule = 'VVVVVV'
ls_BusinessRule = Left(ls_BusinessRule,ll_Column -1) &
						+ Value + Mid(ls_BusinessRule,ll_Column +1)
						
This.SetItem(row,"detail_errors", ls_BusinessRule)	
Return 1
end event

