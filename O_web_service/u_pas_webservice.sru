HA$PBExportHeader$u_pas_webservice.sru
forward
global type u_pas_webservice from nonvisualobject
end type
end forward

global type u_pas_webservice from nonvisualobject
end type
global u_pas_webservice u_pas_webservice

type variables
// Declare soap connection variable
soapconnection ispcn_conn

// Declare input and output program interface variables
pasp00sr_programinterface	ipgif_output
pasp00sr_programinterface1	ipgif_input

//Declare container interface variables
pasp00sr_programinterfacepasp00ci1	ictif_cics_container_in
pasp00sr_programinterfacepasp00in1	ictif_input_container_in
pasp00sr_programinterfacepasp00ot	ictif_output_container_out
pasp00sr_programinterfacepasp00pg1	ictif_program_container_in
pasp00sr_programinterfacepasp00pg	ictif_program_container_out

//Declare container variables
pasp00sr_programinterfacepasp00cipas000sr_cics_container1 ict_pas000sr_cics_container
pasp00sr_programinterfacepasp00pgpas000sr_program_container1 ict_pas000sr_program_container_in
pasp00sr_programinterfacepasp00pgpas000sr_program_container ict_pas000sr_program_container_out

//Web Service path
String		is_web_service_address

//u_abstractdefaultmanager	iu_defaultmanager


//Error Handle Variables

// Error structure used in all RPC Calls
s_error	istr_error

// Holds information on the CommHandles
//str_CommHandles		istr_CommHandles[]

integer	ii_messagebox_rtn

//u_wrkbench16_externals		luo_wkb16
//u_wrkbench32_externals		luo_wkb32
end variables

forward prototypes
public function boolean uf_initialize ()
public function integer uf_rpccall (string as_imput_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id)
public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id)
public function boolean uf_display_message_ws (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle)
public function integer uf_checkrpcerror_ws (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle, boolean ab_visual)
public function integer uf_rpccall (string as_input_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number)
public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number)
end prototypes

public function boolean uf_initialize ();s_error lstr_error_info

is_web_service_address = ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Web Service URL", "pasp00SRpath", "") 
If is_web_service_address = '' then
	lstr_error_info.se_procedure_name = ''
	lstr_error_info.se_return_code = ''
	lstr_error_info.se_message = "Can't initialize default manager for web service info"
	uf_display_message_ws(-1, lstr_error_info, 0) 
	Return False
end if

//Instantiate soapconnection
ispcn_conn = create soapconnection

//Instantiate input and output program interfaces
ipgif_output = create pasp00sr_programinterface
ipgif_input = create pasp00sr_programinterface1

//Instantiate input and output container interfaces
ictif_cics_container_in = create pasp00sr_programinterfacepasp00ci1
ipgif_input.pasp00ci = ictif_cics_container_in

ictif_input_container_in = create pasp00sr_programinterfacepasp00in1
ipgif_input.pasp00in = ictif_input_container_in

ictif_output_container_out = create pasp00sr_programinterfacepasp00ot
ipgif_output.pasp00ot = ictif_output_container_out

ictif_program_container_in = create pasp00sr_programinterfacepasp00pg1
ipgif_input.pasp00pg = ictif_program_container_in

ictif_program_container_out = create pasp00sr_programinterfacepasp00pg
ipgif_output.pasp00pg = ictif_program_container_out

//Instantiate container variables
ict_pas000sr_cics_container = create pasp00sr_programinterfacepasp00cipas000sr_cics_container1
ictif_cics_container_in.pas000sr_cics_container = ict_pas000sr_cics_container

ict_pas000sr_program_container_in  = create pasp00sr_programinterfacepasp00pgpas000sr_program_container1
ictif_program_container_in.pas000sr_program_container = ict_pas000sr_program_container_in

ict_pas000sr_program_container_out  = create pasp00sr_programinterfacepasp00pgpas000sr_program_container
ictif_program_container_out.pas000sr_program_container = ict_pas000sr_program_container_out

return True

end function

public function integer uf_rpccall (string as_imput_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind
boolean	lb_temp_class_error

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")
SetPointer(HourGlass!)
pasp00sr_pasp00srservice p_obj

ict_pas000sr_cics_container.pas000sr_req_password = SQLCA.DBPass
ict_pas000sr_cics_container.pas000sr_req_userid =  SQLCA.Userid
ict_pas000sr_cics_container.pas000sr_req_program = as_program_name
ict_pas000sr_cics_container.pas000sr_req_tranid = as_tran_id

ict_pas000sr_program_container_in.pas000sr_rval = 0
ict_pas000sr_program_container_in.pas000sr_message = space(200)
ict_pas000sr_program_container_in.pas000sr_version_number = 0

ipgif_input.pasp00in.value = as_imput_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "pasp00sr_pasp00srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

as_output_string= ''

lb_temp_class_error = False

Do
	
	ict_pas000sr_program_container_in.pas000sr_last_record_num = ll_last_record_num
	ict_pas000sr_program_container_in.pas000sr_max_record_num = ll_max_record_num
	ict_pas000sr_program_container_in.pas000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				if mid(e.Text,1,36) = "Unable to generate a temporary class" Then
					lb_temp_class_error = True
				else	
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = e.Text
					uf_display_message_ws(-1, astr_error_info, 0) 
					Return -1
				end if
	end try
	
	IF lb_temp_class_error = True Then
	//   Attempt a 2nd time to make web service call  //
	
		try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e2)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e2.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				Return -1
		end try
//		MessageBox ("Temporary Class Error Handled", "The temporary class error successfully handled, RPC call successful.  Contact IS if you receive this message.")
	End If
		
	ictif_program_container_out = ipgif_output.pasp00pg 
	li_rval = ictif_program_container_out.pas000sr_program_container.pas000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
		//check for any RPC errors to display messages
		ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
		
		If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
			astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_message = ls_message
		else
			astr_error_info.se_procedure_name = ''
			astr_error_info.se_return_code = ''
			astr_error_info.se_message = ls_message
		end if
		
		uf_display_message_ws(li_rval, astr_error_info, 0) 
		Return -1
	else
		ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.pas000sr_program_container.pas000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.pas000sr_program_container.pas000sr_max_record_num
		ll_task_number = ictif_program_container_out.pas000sr_program_container.pas000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.pasp00ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
			as_output_string +=  trim(ipgif_output.pasp00ot.value)
		end if
	end if
Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0)  
SetPointer(Arrow!)
Return li_rval


end function

public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind
boolean	lb_temp_class_error 

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")
SetPointer(HourGlass!)
pasp00sr_pasp00srservice p_obj

ict_pas000sr_cics_container.pas000sr_req_password = SQLCA.DBPass
ict_pas000sr_cics_container.pas000sr_req_userid =  SQLCA.Userid
ict_pas000sr_cics_container.pas000sr_req_program = as_program_name
ict_pas000sr_cics_container.pas000sr_req_tranid = as_tran_id

ict_pas000sr_program_container_in.pas000sr_rval = 0
ict_pas000sr_program_container_in.pas000sr_message = space(200)
ict_pas000sr_program_container_in.pas000sr_version_number = 0

ipgif_input.pasp00in.value = as_input_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "pasp00sr_pasp00srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

lb_temp_class_error = False

//as_output_string= ''

//Do
	
	ict_pas000sr_program_container_in.pas000sr_last_record_num = ll_last_record_num
	ict_pas000sr_program_container_in.pas000sr_max_record_num = ll_max_record_num
	ict_pas000sr_program_container_in.pas000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				if mid(e.Text,1,36) = "Unable to generate a temporary class" Then
					lb_temp_class_error = True
				else			
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = e.Text
					uf_display_message_ws(-1, astr_error_info, 0) 
					
					//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
					ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
					
					If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
						astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
						astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
						astr_error_info.se_message = ls_message
					else
						astr_error_info.se_procedure_name = ''
						astr_error_info.se_return_code = ''
						astr_error_info.se_message = ls_message
					end if
					
					uf_display_message_ws(li_rval, astr_error_info, 0) 
					Return -1
				end if				
	end try
	
	IF lb_temp_class_error = True Then
	//   Attempt a 2nd time to make web service call  //
	
		try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e2)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e2.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
				
				If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
					astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
					astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
					astr_error_info.se_message = ls_message
				else
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = ls_message
				end if
				
				uf_display_message_ws(li_rval, astr_error_info, 0) 
				Return -1

		end try
//		MessageBox ("Temporary Class Error Handled", "The temporary class error successfully handled, RPC call successful.  Contact IS if you receive this message.")
	End If	
		
	ictif_program_container_out = ipgif_output.pasp00pg
	li_rval = ictif_program_container_out.pas000sr_program_container.pas000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
//		ll_last_record_num = ictif_program_container_out.orpo03sr_program_container.orpo03sr_last_record_num
//		ll_max_record_num = ictif_program_container_out.orpo03sr_program_container.orpo03sr_max_record_num
		ll_task_number = ictif_program_container_out.pas000sr_program_container.pas000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.pasp00ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
//			as_output_string +=  trim(ipgif_output.utlU01ot.value)
		end if
	end if
//Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

//  PASP37FR has special code to handle uf_display_message_ws
If as_program_name <> 'PASP37FR' Then
	uf_display_message_ws(li_rval, astr_error_info, 0)
End If	 
SetPointer(Arrow!)
Return li_rval


end function

public function boolean uf_display_message_ws (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle);Boolean  lb_retval
			
Int      li_rpc_error_rtn, &
			li_rtn
String	ls_microhelp_str, &
			ls_active_sheet_title
			
s_rpc_error 			lstr_rpc_error_info			
			
window					lw_RPCError			

LB_RETVAL = TRUE

gw_netwise_frame.setmicrohelp("")
 
//li_rpc_error_rtn = This.uf_CheckRPCError_ws(ai_rtn,	astr_errorinfo, ai_commhandle, True)

IF ai_rtn < 0 THEN
	lstr_rpc_error_info.se_app_name = astr_errorinfo.se_app_name
	lstr_rpc_error_info.se_window_name = astr_errorinfo.se_window_name
	lstr_rpc_error_info.se_function_name = astr_errorinfo.se_function_name
	lstr_rpc_error_info.se_event_name = astr_errorinfo.se_event_name
	lstr_rpc_error_info.se_procedure_name = astr_errorinfo.se_procedure_name
	lstr_rpc_error_info.se_user_id = astr_errorinfo.se_user_id
	lstr_rpc_error_info.se_return_code = astr_errorinfo.se_return_code
	lstr_rpc_error_info.se_message = astr_errorinfo.se_message
	lstr_rpc_error_info.se_rval = ai_rtn

	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
	if ai_rtn >= 0 then
		li_rpc_error_rtn = -25
	else
		li_rpc_error_rtn = ai_rtn
	end if
END IF

IF li_rpc_error_rtn >= 0 THEN
	IF ai_rtn = 0 and li_rpc_error_rtn = 0 then
		IF LenA(TRIM( astr_errorinfo.se_Message)) > 0 THEN
			ls_microHelp_str = astr_errorinfo.se_Message
			ls_microHelp_str = LeftA( ls_microHelp_str,71)
			gw_netwise_frame.SetMicroHelp( Ls_MicroHelp_Str)
			RETURN TRUE
		END IF
	END IF
	IF ai_rtn < 0 THEN
		MessageBox("Error Updating", astr_errorinfo.se_Message)
		lb_retval = FALSE		
  	ELSE
		IF ai_rtn > 0 and ai_rtn < 10 THEN
			// Set Micro Help      
			ls_microHelp_str = astr_errorinfo.se_Message
		   ls_microHelp_str = LeftA( ls_microHelp_str,71)
			gw_netwise_frame.SetMicroHelp( Ls_MicroHelp_str)
			lb_retval = FALSE
		END IF
	END IF
	IF ai_rtn >= 10 THEN
		If IsValid( gw_netwise_frame) Then
			Window	lw_active_sheet

			lw_active_sheet = gw_netwise_frame.GetActiveSheet()
			If IsValid(lw_active_sheet) Then
				ls_Active_Sheet_Title = lw_active_sheet.Title
			End If
		END IF
		CHOOSE CASE ai_rtn
			CASE	10
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message)
				ii_messagebox_rtn = 1
			CASE	11
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	12
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	13
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	14
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	15
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, INFORMATION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	20
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, OK!)
				ii_messagebox_rtn = 1
			CASE	21
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	22
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	23
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	24
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	25
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, STOPSIGN!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	30
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, OK!)
				ii_messagebox_rtn = 1
			CASE	31
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	32
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	33
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	34
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	35
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, EXCLAMATION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
			CASE	40
				MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, OK!)
				ii_messagebox_rtn = 1
			CASE	41
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, OKCANCEL!)	
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = li_rtn
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	42
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, YESNO!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSE
					ii_messagebox_rtn = 3
				END IF
			CASE	43
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, YESNOCANCEL!)		
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 2
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 3
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	44
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, RETRYCANCEL!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 4
				END IF
			CASE	45
				li_rtn = MessageBox(ls_active_sheet_title, &
					astr_errorinfo.se_Message, QUESTION!, ABORTRETRYIGNORE!)
				IF li_rtn = 1 THEN
					ii_messagebox_rtn = 5
				ELSEIF li_rtn = 2 THEN
					ii_messagebox_rtn = 6
				ELSE
					ii_messagebox_rtn = 7
				END IF
		END CHOOSE	
			lb_retval = FALSE
		END IF					
ELSE
		lb_retval = FALSE
END IF



Return lb_retval
end function

public function integer uf_checkrpcerror_ws (integer ai_returnval, s_error astr_errorinfo, integer ai_commhandle, boolean ab_visual);// To be called after running an RPC.

integer 					li_result, &
							li_commerror, &
							li_file, &
							li_temp, &
							li_upper_bound
				
long 						ll_neterror, &
							ll_primaryerror, &
							ll_secondaryerror
				
string 					ls_neterrmsg, &
							ls_commerrmsg, &
							ls_server_name

s_rpc_error 			lstr_rpc_error_info

u_string_functions	lu_string

window					lw_RPCError



ls_neterrmsg = space(100)
ls_commerrmsg = space(100)

If ab_visual Then
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
End if
return(ai_ReturnVal)


end function

public function integer uf_rpccall (string as_input_string, ref string as_output_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind
boolean	lb_temp_class_error 

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Inquiring Database")
SetPointer(HourGlass!)
pasp00sr_pasp00srservice p_obj

ict_pas000sr_cics_container.pas000sr_req_password = SQLCA.DBPass
ict_pas000sr_cics_container.pas000sr_req_userid =  SQLCA.Userid
ict_pas000sr_cics_container.pas000sr_req_program = as_program_name
ict_pas000sr_cics_container.pas000sr_req_tranid = as_tran_id

ict_pas000sr_program_container_in.pas000sr_rval = 0
ict_pas000sr_program_container_in.pas000sr_message = space(200)
//ict_pas000sr_program_container_in.pas000sr_version_number = 0
ict_pas000sr_program_container_in.pas000sr_version_number = ai_version_number

ipgif_input.pasp00in.value = as_input_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "pasp00sr_pasp00srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

as_output_string= ''
lb_temp_class_error = False

Do
	
	ict_pas000sr_program_container_in.pas000sr_last_record_num = ll_last_record_num
	ict_pas000sr_program_container_in.pas000sr_max_record_num = ll_max_record_num
	ict_pas000sr_program_container_in.pas000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				if mid(e.Text,1,36) = "Unable to generate a temporary class" Then
					lb_temp_class_error = True
				else	
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = e.Text
					uf_display_message_ws(-1, astr_error_info, 0) 
					Return -1
				end if
	end try
	
	IF lb_temp_class_error = True Then
	//   Attempt a 2nd time to make web service call  //
	
		try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e2)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				astr_error_info.se_procedure_name = ''
				astr_error_info.se_return_code = ''
				astr_error_info.se_message = e2.Text
				uf_display_message_ws(-1, astr_error_info, 0) 
				Return -1
		end try
//		MessageBox ("Temporary Class Error Handled", "The temporary class error successfully handled, RPC call successful.  Contact IS if you receive this message.")
	End If
		
	ictif_program_container_out = ipgif_output.pasp00pg 
	li_rval = ictif_program_container_out.pas000sr_program_container.pas000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
		//check for any RPC errors to display messages
		ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
		
		If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
			astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
			astr_error_info.se_message = ls_message
		else
			astr_error_info.se_procedure_name = ''
			astr_error_info.se_return_code = ''
			astr_error_info.se_message = ls_message
		end if
		
		uf_display_message_ws(li_rval, astr_error_info, 0) 
		Return -1
	else
		ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
		ll_last_record_num = ictif_program_container_out.pas000sr_program_container.pas000sr_last_record_num
		ll_max_record_num = ictif_program_container_out.pas000sr_program_container.pas000sr_max_record_num
		ll_task_number = ictif_program_container_out.pas000sr_program_container.pas000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.pasp00ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
			as_output_string +=  trim(ipgif_output.pasp00ot.value)
		end if
	end if
Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

uf_display_message_ws(li_rval, astr_error_info, 0)  
SetPointer(Arrow!)
Return li_rval


end function

public function integer uf_rpcupd (string as_input_string, ref s_error astr_error_info, string as_program_name, string as_tran_id, integer ai_version_number);long		ll_ret, ll_last_record_num, ll_max_record_num, ll_task_number
integer	li_rtn, li_rval
string		ls_options, ls_message, ls_temp, ls_target_address, ls_OutputString, ls_output, ls_Stringind
boolean	lb_temp_class_error 

u_String_Functions		lu_string

gw_netwise_frame.SetMicroHelp ("Wait.. Updating Database")
SetPointer(HourGlass!)
pasp00sr_pasp00srservice p_obj

ict_pas000sr_cics_container.pas000sr_req_password = SQLCA.DBPass
ict_pas000sr_cics_container.pas000sr_req_userid =  SQLCA.Userid
ict_pas000sr_cics_container.pas000sr_req_program = as_program_name
ict_pas000sr_cics_container.pas000sr_req_tranid = as_tran_id

ict_pas000sr_program_container_in.pas000sr_rval = 0
ict_pas000sr_program_container_in.pas000sr_message = space(200)
ict_pas000sr_program_container_in.pas000sr_version_number = ai_version_number

ipgif_input.pasp00in.value = as_input_string

//Set options for connection
ls_options = ("SoapLog=~"c:\\soaplog.txt~",userID=~""+string( SQLCA.Userid)+"~",Password=~""+string(SQLCA.DBPass)+"~"")
ispcn_conn.setoptions(ls_options)

//Create proxy instance
ll_ret = ispcn_conn.CreateInstance(p_obj, "pasp00sr_pasp00srservice", is_web_service_address)
if ll_ret <> 0 then
	MessageBox("Error", "Cannot create instance of proxy")
	Return ll_ret
end if

ll_last_record_num = 0
ll_max_record_num = 0
ll_task_number = 0

lb_temp_class_error = False

//as_output_string= ''

//Do
	
	ict_pas000sr_program_container_in.pas000sr_last_record_num = ll_last_record_num
	ict_pas000sr_program_container_in.pas000sr_max_record_num = ll_max_record_num
	ict_pas000sr_program_container_in.pas000sr_task_num = ll_task_number
	
	try
		  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
		  catch (SoapException e)
				//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
				if mid(e.Text,1,36) = "Unable to generate a temporary class" Then
					lb_temp_class_error = True
				else			
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = e.Text
					uf_display_message_ws(-1, astr_error_info, 0) 
					
					//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
					ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
					
					If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
						astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
						astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
						astr_error_info.se_message = ls_message
					else
						astr_error_info.se_procedure_name = ''
						astr_error_info.se_return_code = ''
						astr_error_info.se_message = ls_message
					end if
					
					uf_display_message_ws(li_rval, astr_error_info, 0) 
					Return -1
				end if
	end try
	
	
	IF lb_temp_class_error = True Then		
		try
			  ipgif_output = p_obj.pasp00sroperation(ipgif_input)
			  catch (SoapException e2)
					//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
					astr_error_info.se_procedure_name = ''
					astr_error_info.se_return_code = ''
					astr_error_info.se_message = e2.Text
					uf_display_message_ws(-1, astr_error_info, 0) 
					
					//This.uf_CheckWebServiceError(au_ErrorContext, e, as_windowname, as_functionname, as_eventname)
					ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
					
					If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
						astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
						astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
						astr_error_info.se_message = ls_message
					else
						astr_error_info.se_procedure_name = ''
						astr_error_info.se_return_code = ''
						astr_error_info.se_message = ls_message
					end if
					
					uf_display_message_ws(li_rval, astr_error_info, 0) 
					Return -1
		end try	
//		MessageBox ("Temporary Class Error Handled", "The temporary class error successfully handled, RPC call successful.  Contact IS if you receive this message.")
	end if
		
	ictif_program_container_out = ipgif_output.pasp00pg
	li_rval = ictif_program_container_out.pas000sr_program_container.pas000sr_rval
	if li_rval < 0 then
//		This.uf_CheckRpcError(au_ErrorContext, as_windowname, as_functionname, as_eventname, li_rval, ls_banner) 
	else
		ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message
//		au_ErrorContext.uf_AppendText(ls_message)
//		au_ErrorContext.uf_SetReturnCode(li_rval)		
		
//		ll_last_record_num = ictif_program_container_out.orpo03sr_program_container.orpo03sr_last_record_num
//		ll_max_record_num = ictif_program_container_out.orpo03sr_program_container.orpo03sr_max_record_num
		ll_task_number = ictif_program_container_out.pas000sr_program_container.pas000sr_task_num
// strings are coming in with spaces to reserve space for Netwise calls, so clear strings 		
		
				
		
		if isnull(ipgif_output.pasp00ot) then
//			as_header_string = ''
//			as_recv_window_inq_string = ''
		else	
//			as_output_string +=  trim(ipgif_output.utlU01ot.value)
		end if
	end if
//Loop while ll_last_record_num <> ll_max_record_num	

//check for any RPC errors to display messages
ls_message = ictif_program_container_out.pas000sr_program_container.pas000sr_message

If lu_string.nf_CountOccurrences(ls_message, '~t') > 0 then
	astr_error_info.se_procedure_name = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_return_code = lu_string.nf_gettoken(ls_message, '~t')
	astr_error_info.se_message = ls_message
else
	astr_error_info.se_procedure_name = ''
	astr_error_info.se_return_code = ''
	astr_error_info.se_message = ls_message
end if

//  PASP37FR has special code to handle uf_display_message_ws
If as_program_name <> 'PASP37FR' Then
	uf_display_message_ws(li_rval, astr_error_info, 0)
End If	 
SetPointer(Arrow!)
Return li_rval


end function

on u_pas_webservice.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_pas_webservice.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;uf_initialize()
end event

