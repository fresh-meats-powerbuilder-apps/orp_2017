HA$PBExportHeader$w_ctrl_tab_window_switching.srw
forward
global type w_ctrl_tab_window_switching from window
end type
type sle_1 from statictext within w_ctrl_tab_window_switching
end type
type p_1 from picture within w_ctrl_tab_window_switching
end type
end forward

global type w_ctrl_tab_window_switching from window
integer x = 480
integer y = 640
integer width = 2016
integer height = 312
windowtype windowtype = response!
long backcolor = 12632256
event ue_keyup pbm_keyup
sle_1 sle_1
p_1 p_1
end type
global w_ctrl_tab_window_switching w_ctrl_tab_window_switching

type variables
Window	iw_ActiveSheet,&
			iw_LastSheet
			
String 	is_new_code			
end variables

event ue_keyup;IF Not KeyDown(KeyControl!) Then
	IF IsValid (iw_ActiveSheet) Then
		/* ibdkdld removed bringtotop code
		for some reason when the VB Planned Transfer Window are maximized
		it would make iw_activeSheet Null	
		*/
		if is_new_code <> "Y"  then
			iw_ActiveSheet.BringtoTop = True
			iw_ActiveSheet.SetFocus()
		Else
			iw_ActiveSheet.SetFocus()
//			if iw_ActiveSheet.BringtoTop = False Then iw_ActiveSheet.BringtoTop = True
		End IF			
		Close(This)
	end if
END IF

end event

event key;Window	lw_LastSheet
IF KeyDown( KeyShift!) Then
	IF KeyDown( KeyControl!) And KeyDown( KeyTab!) Then
		iw_ActiveSheet = gw_base_frame.GetFirstSheet()
		DO 
			lw_LastSheet = iw_ActiveSheet
			iw_ActiveSheet = gw_base_Frame.GetNextSheet( iw_ActiveSheet)
		Loop While IsValid( iw_ActiveSheet) AND iw_ActiveSheet<> iw_LastSheet
		iw_LastSheet = lw_LastSheet
		iw_ActiveSheet = lw_LastSheet
	END IF
ELSE
	IF KeyDown( KeyControl!) And KeyDown( KeyTab!) Then
		IF IsValid( iw_ActiveSheet) Then
			iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
			IF Not IsValid( iw_ActiveSheet) Then
				iw_ActiveSheet = gw_base_frame.GetFirstSheet()
			END IF
		ELSE
			iw_ActiveSheet = gw_base_frame.GetFirstSheet()
			iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
		END IF
	END IF
END IF
If IsValid( iw_ActiveSheet) Then
	sle_1.Text = Trim(iw_ActiveSheet.Title)
ELSE
	Close(This)
END IF

end event

event open;Window	lw_LastSheet

String	ls_appid


ls_appid	=	Message.nf_Get_app_id()

//ibdkdld added code begin
is_new_code = upper(profilestring(iw_frame.is_userini, ls_appid,	"W_ctrl_tab_window.Run new Keyup Code", "")) 	
If is_new_code = "" Then
	setprofilestring(iw_frame.is_userini, ls_appid,	"W_ctrl_tab_window.Run new Keyup Code", "Y") 
	is_new_code = "Y"
End If
//ibdkdld added code end
	
p_1.PictureName	=	ls_appid + '.BMP'
iw_activesheet = gw_base_frame.GetFirstSheet()
IF KeyDown(KeyShift!) Then
	DO
		lw_LastSheet = iw_ActiveSheet
		iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
	Loop While IsValid( iw_ActiveSheet)
	iw_ActiveSheet = lw_LastSheet
ELSE
	IF IsValid( iw_ActiveSheet) Then
		iw_activesheet = gw_base_frame.GetNextSHeet( iw_ActiveSheet)
	ELSE
		Close(This)
	END IF
END IF
IF IsValid ( Iw_ActiveSheet) Then
	sle_1.Text = Trim(iw_ActiveSheet.Title)
ELSE
	Close( This)
END IF
end event

on w_ctrl_tab_window_switching.create
this.sle_1=create sle_1
this.p_1=create p_1
this.Control[]={this.sle_1,&
this.p_1}
end on

on w_ctrl_tab_window_switching.destroy
destroy(this.sle_1)
destroy(this.p_1)
end on

type sle_1 from statictext within w_ctrl_tab_window_switching
integer x = 320
integer y = 92
integer width = 1458
integer height = 100
integer textsize = -11
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "none"
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type p_1 from picture within w_ctrl_tab_window_switching
integer x = 151
integer y = 80
integer width = 146
integer height = 120
boolean focusrectangle = false
end type

