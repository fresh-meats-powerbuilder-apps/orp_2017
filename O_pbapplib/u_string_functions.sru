HA$PBExportHeader$u_string_functions.sru
forward
global type u_string_functions from nonvisualobject
end type
end forward

global type u_string_functions from nonvisualobject autoinstantiate
end type

type variables

end variables

forward prototypes
public function long nf_arraytostring (string as_source[], string as_delimiter, ref string as_ref_string)
public function long nf_countoccurrences (string as_source, string as_target)
public function long nf_countoccurrences (string as_source, string as_target, boolean ab_ignorecase)
public function string nf_getkeyvalue (string as_source, string as_keyword, string as_separator)
public function string nf_gettoken (ref string as_source, string as_separator)
public function string nf_globalreplace (string as_source, string as_old, string as_new)
public function string nf_globalreplace (string as_source, string as_old, string as_new, boolean ab_ignorecase)
public function boolean nf_isalpha (string as_source)
public function boolean nf_isalphanum (string as_source)
public function boolean nf_isarithmeticoperator (string as_source)
public function boolean nf_iscomparisonoperator (string as_source)
public function boolean nf_isempty (string as_source)
public function boolean nf_isformat (string as_source)
public function boolean nf_islower (string as_source)
public function boolean nf_isprintable (string as_source)
public function boolean nf_ispunctuation (string as_source)
public function boolean nf_isspace (string as_source)
public function boolean nf_isupper (string as_source)
public function boolean nf_iswhitespace (string as_source)
public function long nf_lastpos (string as_source, string as_target)
public function long nf_lastpos (string as_source, string as_target, long al_start)
public function string nf_lefttrim (string as_source)
public function string nf_lefttrim (string as_source, boolean ab_remove_spaces)
public function string nf_lefttrim (string as_source, boolean ab_remove_spaces, boolean ab_remove_nonprint)
public function string nf_padleft (string as_source, long al_length)
public function string nf_padright (string as_source, long al_length)
public function long nf_parsetoarray (string as_source, string as_delimiter, ref string as_array[])
public function string nf_quote (string as_source)
public function string nf_removenonprint (string as_source)
public function string nf_removewhitespace (string as_source)
public function string nf_righttrim (string as_source)
public function string nf_righttrim (string as_source, boolean ab_remove_spaces)
public function string nf_righttrim (string as_source, boolean ab_remove_spaces, boolean ab_remove_nonprint)
public function integer nf_setkeyvalue (ref string as_source, string as_keyword, string as_keyvalue, string as_separator)
public function string nf_trim (string as_source)
public function string nf_trim (string as_source, boolean ab_remove_spaces)
public function string nf_trim (string as_source, boolean ab_remove_spaces, boolean ab_remove_nonprint)
public function string nf_wordcap (string as_source)
public function string nf_globalreplace (string as_EntireString, string as_ReplaceString, string as_ReplaceWithString, long al_StartingPosition)
public function integer nf_parseobjstring (datawindow dw_arg, ref string obj_list[], string obj_type, string band)
public function long nf_npos (string as_string1, string as_string2, long al_start, long al_occurrances)
public function boolean nf_isalpha (string as_check_string, integer ai_startpos, integer ai_end_pos)
public function string nf_encrypt (string as_text)
public function string nf_decrypt (string as_text)
public function boolean nf_amiempty (string as_string)
public function integer nf_parseleftright (string as_initial_string, string as_delimeter, ref string as_left_string, ref string as_right_string)
public function integer nf_replacerows (string as_datatoimport, ref long al_rowstoreplace[], ref datawindow adw_datawindowtarget)
public function string nf_buildupdatestring (datawindow adw_tobuildfrom)
public function long nf_compare_dates (long al_row, string as_searchstring, datawindow adw_tosearch)
end prototypes

public function long nf_arraytostring (string as_source[], string as_delimiter, ref string as_ref_string);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_ArrayToString
//
//	Access:  		public
//
//	Arguments:
//	as_source[]		The array of string to be moved into a single string.
//	as_Delimiter	The delimeter string.
//	as_ref_string	The string to be filled with the array of strings,
//						passed by reference.
//
//	Returns:  		long
//						1 for a successful transfer.
//						-1 if a problem was found.
//
//	Description:  	Create a single string from an array of strings separated by
//						the passed delimeter.
//						Note: Function will not include on the single string any 
//								array entries which match an empty string.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_Count, ll_ArrayUpBound
//Get the array size
ll_ArrayUpBound = UpperBound(as_source[])

//Check parameters
IF IsNull(as_delimiter) or (Not ll_ArrayUpBound>0) Then
	Return -1
End If

//Reset the Reference string
as_ref_string = ''

For ll_Count = 1 to ll_ArrayUpBound
	//Do not include any entries that match an empty string
	If as_source[ll_Count] <> '' Then
		If Len(as_ref_string) = 0 Then
			//Initialize string
			as_ref_string = as_source[ll_Count]
		else
			//Concatenate to string
			as_ref_string = as_ref_string + as_delimiter + as_source[ll_Count]
		End If
	End If
Next 

return 1

end function

public function long nf_countoccurrences (string as_source, string as_target);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_CountOccurrences
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string in which to search.
//	as_Target		The string to search for.
//
//	Returns: 		long
//						The number of occurrences of as_Target in as_source.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Count the occurrences of one string within another.
//
//////////////////////////////////////////////////////////////////////////////

Long	ll_Count

//Check for parameters
If IsNull(as_source) or IsNull(as_target) Then
	long ll_null
	SetNull(ll_null)
	Return ll_null
End If

//Default is to ignore case.
ll_Count = nf_CountOccurrences (as_source, as_target, True)

Return ll_Count

end function

public function long nf_countoccurrences (string as_source, string as_target, boolean ab_ignorecase);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_CountOccurrences
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string in which to search.
//	as_Target		The string to search for.
//	ab_IgnoreCase	A boolean stating to ignore case sensitivity.
//
//	Returns: 		long
//						The number of occurrences of as_Target in as_source.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Count the occurrences of one string within another.
//
//////////////////////////////////////////////////////////////////////////////


Long	ll_Count, ll_Pos, ll_Len

//Check for parameters
If IsNull(as_source) or IsNull(as_target) or IsNull(ab_ignorecase) Then
	long ll_null
	SetNull(ll_null)
	Return ll_null
End If

//Should function ignore case?
If ab_ignorecase Then
	as_source = Lower(as_source)
	as_target = Lower(as_target)
End If

ll_Len = Len(as_Target)
ll_Count = 0

ll_Pos = Pos(as_source, as_Target)

Do While ll_Pos > 0
	ll_Count ++
	ll_Pos = Pos(as_source, as_Target, (ll_Pos + ll_Len))
Loop

Return ll_Count

end function

public function string nf_getkeyvalue (string as_source, string as_keyword, string as_separator);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GetKeyValue
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string to be searched.
//	as_keyword		The keyword to be searched for.
//	as_separator	The separator character used in the source string.
//
//	Returns:  		string	
//						The value found for the keyword.
//						If no matching keyword is found, an empty string is returned.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Gets the value portion of a keyword=value pair from a string.
//
//////////////////////////////////////////////////////////////////////////////

boolean	lb_done=false
integer	li_keyword, &
			li_separator
string	ls_keyvalue

//Check parameters
If IsNull(as_source) or IsNull(as_keyword) or IsNull(as_separator) Then
	string ls_null
	SetNull (ls_null)
	Return ls_null
End If

//Initialize key value
ls_keyvalue = ''

do while not lb_done
	li_keyword = Pos (Lower(as_source), Lower(as_keyword))
	if li_keyword > 0 then
		as_source = LeftTrim(Right(as_source, Len(as_source) - (li_keyword + Len(as_keyword) - 1)))

		if Left(as_source, 1) = "=" then
			li_separator = Pos (as_source, as_separator, 2)
			if li_separator > 0 then
				ls_keyvalue = Mid(as_source, 2, li_separator - 2)
			else
				ls_keyvalue = Mid(as_source, 2)
			end if
			ls_keyvalue = Trim(ls_keyvalue)
			lb_done = true
		end if
	else
		lb_done = true
	end if
loop

return ls_keyvalue
end function

public function string nf_gettoken (ref string as_source, string as_separator);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GetToken
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string passed by reference
//	as_separator	Separator character in the source string which will be 
//						used to determine the length of characters to strip from
//						the left end of the source string.
//
//	Returns:  		string
//						The token stripped off of the source string.
//						If the separator character does not appear in the string, 
//						the entire source string is returned.
//						Otherwise, it returns the token stripped off of the left
//						end of the source string (not including the separator character)
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	This function strips a source string (from the left) up 
//						to the occurrence of a specified separator character.
//
//
//////////////////////////////////////////////////////////////////////////////

Long		ll_pos
string 	ls_ret

//Check parameters
If IsNull(as_source) or IsNull(as_separator) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

/////////////////////////////////////////////////////////////////////////////////
// Get the position of the separator
/////////////////////////////////////////////////////////////////////////////////
ll_pos = Pos(as_source, as_separator)	

/////////////////////////////////////////////////////////////////////////////////
// Compute the length of the token to be stripped off of the source string.
/////////////////////////////////////////////////////////////////////////////////

// If no separator, the token to be stripped is the entire source string
if ll_pos = 0 then
	ls_ret = as_source
	as_source = ""	
else
	// Otherwise, return just the token and strip it & the separator from the source string
	ls_ret = Mid(as_source, 1, ll_pos - 1)
	as_source = Right(as_source, Len(as_source) - (ll_pos+Len(as_separator)-1) )
end if

return ls_ret
end function

public function string nf_globalreplace (string as_source, string as_old, string as_new);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GlobalReplace
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string being searched.
//	as_Old			The old string being replaced.
//	as_New			The new string.
// 
//Returns:  		string
//						as_Source with all occurrences of as_Old replaced with as_New.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Replace all occurrences of one string inside another with
//						a new string.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright $$HEX2$$a9002000$$ENDHEX$$1996 Powersoft Corporation.  All Rights Reserved.
//	Any distribution of the PowerBuilder Foundation Classes (PFC)
//	source code by other than Powersoft is prohibited.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) or IsNull(as_old) or IsNull(as_new) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

//The default is to ignore Case
as_Source = nf_GlobalReplace (as_source, as_old, as_new, True)

Return as_Source


end function

public function string nf_globalreplace (string as_source, string as_old, string as_new, boolean ab_ignorecase);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_GlobalReplace
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string being searched.
//	as_Old			The old string being replaced.
//	as_New			The new string.
// ab_IgnoreCase	A boolean stating to ignore case sensitivity.
//
//	Returns:  		string
//						as_Source with all occurrences of as_Old replaced with as_New.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Replace all occurrences of one string inside another with
//						a new string.
//
//////////////////////////////////////////////////////////////////////////////

Long	ll_Start, ll_OldLen, ll_NewLen
String ls_Source

//Check parameters
If IsNull(as_source) or IsNull(as_old) or IsNull(as_new) or IsNull(ab_ignorecase) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

//Get the string lenghts
ll_OldLen = Len(as_Old)
ll_NewLen = Len(as_New)

//Should function respect case.
If ab_ignorecase Then
	as_old = Lower(as_old)
	ls_source = Lower(as_source)
Else
	ls_source = as_source
End If

//Search for the first occurrence of as_Old
ll_Start = Pos(ls_Source, as_Old)

Do While ll_Start > 0
	// replace as_Old with as_New
	as_Source = Replace(as_Source, ll_Start, ll_OldLen, as_New)
	
	//Should function respect case.
	If ab_ignorecase Then 
		ls_source = Lower(as_source)
	Else
		ls_source = as_source
	End If
	
	// find the next occurrence of as_Old
	ll_Start = Pos(ls_Source, as_Old, (ll_Start + ll_NewLen))
Loop

Return as_Source

end function

public function boolean nf_isalpha (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsAlpha
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains alphabetic characters. 
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string contains only alphabetic
//						characters.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_count=0
long		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if Non Alpha character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	// 'A'=65, 'Z'=90, 'a'=97, 'z'=122
	if li_ascii<65 or (li_ascii>90 and li_ascii<97) or li_ascii>122 then
		/* Character is Not an Alpha */
		Return False
	end if
loop
	
// Entire string is alpha.
return True
end function

public function boolean nf_isalphanum (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsAlphaNum
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains alphabetic and Numeric
//						characters. 
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string contains only alphabetic and
//						numeric characters.
//
//////////////////////////////////////////////////////////////////////////////

long ll_count=0
long ll_length
char lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters.
//Quit loop if Non Alphanemeric character is found.
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	// '0'= 48, '9'=57, 'A'=65, 'Z'=90, 'a'=97, 'z'=122
	If li_ascii<48 or (li_ascii>57 and li_ascii<65) or &
		(li_ascii>90 and li_ascii<97) or li_ascii>122 then
		/* Character is Not an AlphaNumeric */
		Return False
	end if
loop
	
// Entire string is AlphaNumeric.
return True

end function

public function boolean nf_isarithmeticoperator (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsArithmeticOperator
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains Arithmetic Operator
//						characters.
//						If as_source is NULL, the function returns NULL.
//
//	Description:  	Determines whether a string contains only Arithmetic
//						Operator characters.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_count=0
long		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if Non Operator character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	If li_ascii=40 or			/* ( left parenthesis */	 & 
		li_ascii=41 or			/* ) right parenthesis */	 & 
		li_ascii=43 or			/* + addition */				 & 
		li_ascii=45 or			/* - subtraction */			 & 
		li_ascii=42 or			/* * multiplication */		 & 
		li_ascii=47 or			/* / division */				 & 
		li_ascii=94 Then		/* ^ power */	
		//Character is an operator.
		//Continue with the next character.
	Else
		Return False
	End If
loop
	
// Entire string is made of arithmetic operators.
return True

end function

public function boolean nf_iscomparisonoperator (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsComparisonOperator
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains Comparison Operator
//						characters.
//						If as_source is NULL, the function returns NULL.
//
//	Description:  	Determines whether a string contains only Comparison
//						Operator characters.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_count=0
long		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if Non Operator character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	If li_ascii=60 or			/* < less than */	 & 
		li_ascii=61 or			/* = equal */		 & 
		li_ascii=62 Then		/* > greater than */
		//Character is an Comparison Operator.
		//Continue with the next character.
	Else
		Return False
	End If
loop
	
// Entire string is made of Comparison Operators.
return True

end function

public function boolean nf_isempty (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsEmpty
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string has a lenght of 0 or is NULL.
//
//	Description:  	Determines whether a string has a lenght of 0 or is NULL.
//
//////////////////////////////////////////////////////////////////////////////

if IsNull(as_source) or Len(Trim(as_source))=0 then
	//String is empty
	Return True
end if
	
//String is Not empty
return False
end function

public function boolean nf_isformat (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsFormat
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains Formatting characters.
//						If as_source is NULL, the function returns NULL.
//
//	Description:  	Determines whether a string contains only Formatting
//						characters.  Format characters for this function
//						are all printable characters that are not AlphaNumeric.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_count=0
long		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if Non Operator character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	If (li_ascii>=33 and li_ascii<=47) or &
		(li_ascii>=58 and li_ascii<=64) or &
		(li_ascii>=91 and li_ascii<=96) or &
		(li_ascii>=123 and li_ascii<=126) Then
		//Character is a Format.
		//Continue with the next character.
	Else
		Return False
	End If
loop
	
// Entire string is made of Format characters.
return True

end function

public function boolean nf_islower (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsLower
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains lowercase characters. 
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string contains only lowercase 
//						characters.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

If as_source = Lower(as_source) Then
	Return True
Else
	Return False
End If
end function

public function boolean nf_isprintable (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsPrintable
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains Printable characters.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string is composed entirely of 
//						Printable characters.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_count=0
long		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if NonPrintable character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	// 'space'=32, '~'=126
	if li_ascii<32 or li_ascii>126 then
		/* Not a printable character */
		Return False
	end if
loop
	
// Entire string is of printable characters.
return True

end function

public function boolean nf_ispunctuation (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsPunctuation
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains punctuation characters.
//						If as_source is NULL, the function returns NULL.
//
//	Description:  	Determines whether a string contains only punctuation
//						characters.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_count=0
long		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if Non Punctuation character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	If li_ascii=33 or			/* '!' */		 & 
		li_ascii=34 or			/* '"' */		 & 
		li_ascii=39 or			/* ''' */		 & 
		li_ascii=44 or			/* ',' */		 & 
		li_ascii=46 or			/* '.' */		 & 
		li_ascii=58 or			/* ':' */		 & 
		li_ascii=59 or			/* ';' */		 & 	
		li_ascii=63 Then 		/* '?' */
		//Character is a punctuation.
		//Continue with the next character.
	Else
		Return False
	End If
loop
	
// Entire string is punctuation.
return True

end function

public function boolean nf_isspace (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsSpace
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains space characters. 
//						False if the string is empty or if it contains other
//						non-space characters.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string contains only space characters.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Check for an empty string
If Len(as_source)=0 Then
	Return False
End If

If Trim(as_source) = '' Then
	// Entire string is made of spaces.
	return True
end if

//String is not made up entirely of spaces.
Return False

end function

public function boolean nf_isupper (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsUpper
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains uppercase characters. 
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string contains only uppercase 
//						characters.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

If as_source = Upper(as_source) Then
	Return True
Else
	Return False
End If
end function

public function boolean nf_iswhitespace (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_IsWhiteSpace
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		Boolean
//						True if the string only contains White Space characters. 
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Determines whether a string contains only White Space
//						characters. White Space characters include Newline, Tab,
//						Vertical tab, Carriage return, Formfeed, and Backspace.
//
//////////////////////////////////////////////////////////////////////////////

long 		ll_count=0
long 		ll_length
char		lc_char[]
integer	li_ascii

//Check parameters
If IsNull(as_source) Then
	boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

//Get the length
ll_length = Len (as_source)

//Check for at least one character
If ll_length=0 Then
	Return False
End If

//Move string into array of chars
lc_char = as_source

//Perform loop around all characters
//Quit loop if Non WhiteSpace character is found
do while ll_count<ll_length
	ll_count ++
	
	//Get ASC code of character.
	li_ascii = Asc (lc_char[ll_count])
	
	If li_ascii=8	or			/* BackSpae */		 		& 
		li_ascii=9 	or			/* Tab */		 			& 
		li_ascii=10 or			/* NewLine */				& 
		li_ascii=11 or			/* Vertical Tab */		& 
		li_ascii=12 or			/* Form Feed */			& 
		li_ascii=13 or			/* Carriage Return */	&
		li_ascii=32 Then		/* Space */		
		//Character is a WhiteSpace.
		//Continue with the next character.
	Else
		/* Character is Not a White Space. */
		Return False
	End If
loop
	
// Entire string is White Space.
return True

end function

public function long nf_lastpos (string as_source, string as_target);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_LastPos	
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string being searched.
//	as_Target		The string being searched for.
//
//	Returns:  		Long	
//						The position of as_Target.
//						If as_Target is not found, function returns a 0.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  Search backwards through a string to find the last occurrence of another string
//
//////////////////////////////////////////////////////////////////////////////

//Check for Null Parameters.
IF IsNull(as_source) or IsNull(as_target) Then
	Long ll_null
	SetNull(ll_null)
	Return ll_null
End If

//Set the starting position and perform the search
Return nf_LastPos (as_source, as_target, Len(as_Source))

end function

public function long nf_lastpos (string as_source, string as_target, long al_start);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_LastPos	
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string being searched.
//	as_Target		The being searched for.
//	al_start			The starting position, 0 means start at the end.
//
//	Returns:  		Long	
//						The position of as_Target.
//						If as_Target is not found, function returns a 0.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Search backwards through a string to find the last occurrence 
//						of another string.
//
//////////////////////////////////////////////////////////////////////////////

Long	ll_Cnt, ll_Pos

//Check for Null Parameters.
IF IsNull(as_source) or IsNull(as_target) or IsNull(al_start) Then
	SetNull(ll_Cnt)
	Return ll_Cnt
End If

//Check for an empty string
If Len(as_Source) = 0 Then
	Return 0
End If

// Check for the starting position, 0 means start at the end.
If al_start=0 Then  
	al_start=Len(as_Source)
End If

//Perform find
For ll_Cnt = al_start to 1 Step -1
	ll_Pos = Pos(as_Source, as_Target, ll_Cnt)
	If ll_Pos = ll_Cnt Then 
		//String was found
		Return ll_Cnt
	End If
Next

//String was not found
Return 0

end function

public function string nf_lefttrim (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_LeftTrim
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string to be trimmed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the left end 
//						of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the left end of a string.
//						The options depending on the parameters are:
//							Remove spaces from the beginning of a string.
//							Remove nonprintable characters from the beginning of a string.
//							Remove spaces and nonprintable characters from the 
//							beginning of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

// Remove spaces=True, NonPrintCharacters=False
return nf_LeftTrim (as_source, True, False)
end function

public function string nf_lefttrim (string as_source, boolean ab_remove_spaces);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_LeftTrim
//
//	Access:  		public
//
//	Arguments:
//	as_source			The string to be trimmed.
//	ab_remove_spaces	A boolean stating if spaces should be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the left end 
//						of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the left end of a string.
//						The options depending on the parameters are:
//							Remove spaces from the beginning of a string.
//							Remove nonprintable characters from the beginning of a string.
//							Remove spaces and nonprintable characters from the 
//							beginning of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) or IsNull(ab_remove_spaces) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

// Remove spaces=ab_remove_spaces, NonPrintCharacters=False
return nf_LeftTrim (as_source, ab_remove_spaces, False)
end function

public function string nf_lefttrim (string as_source, boolean ab_remove_spaces, boolean ab_remove_nonprint);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_LeftTrim
//
//	Access:  		public
//
//	Arguments:
//	as_source				The string to be trimmed.
//	ab_remove_spaces		A boolean stating if spaces should be removed.
//	ab_remove_nonprint	A boolean stating if nonprint characters should be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the left end of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the left end of a string.
//						The options depending on the parameters are:
//							Remove spaces from the beginning of a string.
//							Remove nonprintable characters from the beginning of a string.
//							Remove spaces and nonprintable characters from the beginning of a string.
//////////////////////////////////////////////////////////////////////////////

char		lc_char
boolean	lb_char
boolean	lb_printable_char

//Check parameters
If IsNull(as_source) or IsNull(ab_remove_spaces) or IsNull(ab_remove_nonprint) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

If ab_remove_spaces and ab_remove_nonprint Then
	// Remove spaces and nonprintable characters from the beginning of a string.
	do while Len (as_source) > 0 and not lb_char
		lc_char = as_source
		if nf_IsPrintable(lc_char) and Not nf_IsSpace(lc_char) then
			lb_char = true
		else
			as_source = Mid (as_source, 2)
		end if
	loop
	return as_source
ElseIf ab_remove_nonprint Then
	// Remove nonprintable characters from the beginning of a string.
	do while Len (as_source) > 0 and not lb_printable_char
		lc_char = as_source
		if nf_IsPrintable(lc_char) then
			lb_printable_char = true
		else
			as_source = Mid (as_source, 2)
		end if
	loop
	return as_source
ElseIf ab_remove_spaces Then
	//Remove spaces from the beginning of a string.
	return LeftTrim(as_source)
End If

return as_source


end function

public function string nf_padleft (string as_source, long al_length);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_PadLeft
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string being searched.
//	al_length		The desired length of the string.
//
//	Returns:  		String
//						A string of length al_length wich contains as_source with
//						spaces added to its left.
//						If any argument's value is NULL, function returns NULL.
//						If al_length is less or equal to length of as_source, the 
//						function returns the original as_source.
//
//	Description:  	Pad the original string with spaces on its left to make it of
//					   the desired length.
//
//////////////////////////////////////////////////////////////////////////////

string	ls_return

//Check for Null Parameters.
IF IsNull(as_source) or IsNull(al_length) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

//Check for the lengths
If al_length <= Len(as_Source) Then
	//Return the original string
	Return as_source
End If

//Create the left padded string
ls_return = space(al_length - Len(as_Source)) + as_source

//Return the left padded string
Return ls_return
end function

public function string nf_padright (string as_source, long al_length);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_PadRight
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string being searched.
//	al_length		The desired length of the string.
//
//	Returns:  		String
//						A string of length al_length wich contains as_source with
//						spaces added to its right.
//						If any argument's value is NULL, function returns NULL.
//						If al_length is less or equal to length of as_source, the 
//						function returns the original as_source.
//
//	Description:  	Pad the original string with spaces on its right to make it of
//					   the desired length.
//
//////////////////////////////////////////////////////////////////////////////

string	ls_return

//Check for Null Parameters.
IF IsNull(as_source) or IsNull(al_length) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

//Check for the lengths
If al_length <= Len(as_Source) Then
	//Return the original string
	Return as_source
End If

//Create the right padded string
ls_return = as_source + space(al_length - Len(as_Source))

//Return the right padded string
Return ls_return
end function

public function long nf_parsetoarray (string as_source, string as_delimiter, ref string as_array[]);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_ParseToArray
//
//	Access:  		public
//
//	Arguments:
//	as_Source		The string to parse.
//	as_Delimiter	The delimeter string.
//	as_Array[]		The array to be filled with the parsed strings, passed by reference.
//
//	Returns:  		long
//						The number of elements in the array.
//						If as_Source or as_Delimeter is NULL, function returns NULL.
//
//	Description:  Parse a string into array elements using a delimeter string.
//
//////////////////////////////////////////////////////////////////////////////

long		ll_DelLen, ll_Pos, ll_Count, ll_Start, ll_Length

//Check for NULL
IF IsNull(as_source) or IsNull(as_delimiter) Then
	long ll_null
	SetNull(ll_null)
	Return ll_null
End If

//Check for at leat one entry
If Trim(as_source) = '' Then
	Return 0
End If

//Get the length of the delimeter
ll_DelLen = Len(as_Delimiter)

//ll_Pos =  Pos(as_source, as_Delimiter)
ll_Pos =  Pos(Upper(as_source), Upper(as_Delimiter))

//Only one entry was found
If ll_Pos = 0 Then
	as_Array[1] = as_source
	Return 1
End if

//More than one entry was found - loop to get all of them
ll_Count = 1
ll_Start = 1
Do While ll_Pos > 0
	
	//Set current entry
	ll_Length = ll_Pos - ll_Start
	as_Array[ll_Count] = Mid(as_source, ll_Start, ll_Length)
	
	//Set the new starting position
	ll_Start = ll_Pos + ll_DelLen

	//ll_Pos =  Pos(as_source, as_Delimiter, ll_Start)
	ll_Pos =  Pos(Upper(as_source), Upper(as_Delimiter), ll_Start)
	
	ll_Count ++
Loop

//Set last entry
as_Array[ll_Count] = Mid(as_source, ll_Start, Len(as_source))

//Return the number of entries found
Return ll_Count

end function

public function string nf_quote (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Quote
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		String
//						The original string enclosed in quotations.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Enclose the original string in quotations.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	Return as_source
End If

// Enclosed original string in quotations.
return '"' + as_source + '"'

end function

public function string nf_removenonprint (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_RemoveNonPrint
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string from which all nonprint characters are to
//						be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes all nonprint characters.
//
//////////////////////////////////////////////////////////////////////////////

char		lch_char

long		ll_pos = 1
long		ll_loop
string	ls_source
long		ll_source_len

//Check parameters
If IsNull(as_source) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

ls_source = as_source
ll_source_len = Len(ls_source)

// Remove nonprintable characters 
FOR ll_loop = 1 TO ll_source_len
	lch_char = Mid(ls_source, ll_pos, 1)
	if nf_IsPrintable(lch_char) then
		ll_pos ++	
	else
		ls_source = Replace(ls_source, ll_pos, 1, "")
	end if 
NEXT

Return ls_source

end function

public function string nf_removewhitespace (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_RemoveWhiteSpace
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string from which all WhiteSpace characters are to
//						be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes all WhiteSpace characters.
//
//////////////////////////////////////////////////////////////////////////////

char		lch_char

long		ll_pos = 1
long		ll_loop
string	ls_source
long		ll_source_len

//Check parameters
If IsNull(as_source) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

ls_source = as_source
ll_source_len = Len(ls_source)

// Remove WhiteSpace characters 
FOR ll_loop = 1 TO ll_source_len
	lch_char = Mid(ls_source, ll_pos, 1)
	if Not nf_IsWhiteSpace(lch_char) then
		ll_pos ++	
	else
		ls_source = Replace(ls_source, ll_pos, 1, "")
	end if 
NEXT

Return ls_source

end function

public function string nf_righttrim (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_RightTrim
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string to be trimmed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the right end 
//						of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the right end of a string.
//						The options depending on the parameters are:
//							Remove spaces from the end of a string.
//							Remove nonprintable characters from the end of a string.
//							Remove spaces and nonprintable characters from the end 
//							of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

// Remove spaces=True, NonPrintCharacters=False
return nf_RightTrim (as_source, True, False)
end function

public function string nf_righttrim (string as_source, boolean ab_remove_spaces);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_RightTrim
//
//	Access:  		public
//
//	Arguments:
//	as_source			The string to be trimmed.
//	ab_remove_spaces	A boolean stating if spaces should be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the right end 
//						of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the right end of a string.
//						The options depending on the parameters are:
//							Remove spaces from the end of a string.
//							Remove nonprintable characters from the end of a string.
//							Remove spaces and nonprintable characters from the end 
//							of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) or IsNull(ab_remove_spaces) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

// Remove spaces=ab_remove_spaces, NonPrintCharacters=False
return nf_RightTrim (as_source, ab_remove_spaces, False)
end function

public function string nf_righttrim (string as_source, boolean ab_remove_spaces, boolean ab_remove_nonprint);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_RightTrim
//
//	Access:  		public
//
//	Arguments:
//	as_source				The string to be trimmed.
//	ab_remove_spaces		A boolean stating if spaces should be removed.
//	ab_remove_nonprint	A boolean stating if nonprint characters should be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the right
//						end of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the right end of a string.
//						The options depending on the parameters are:
//							Remove spaces from the end of a string.
//							Remove nonprintable characters from the end of a string.
//							Remove spaces and nonprintable characters from the end of
//							a string.
//////////////////////////////////////////////////////////////////////////////

boolean	lb_char
char		lc_char
boolean	lb_printable_char

//Check parameters
If IsNull(as_source) or IsNull(ab_remove_spaces) or IsNull(ab_remove_nonprint) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

If ab_remove_spaces and ab_remove_nonprint Then
	// Remove spaces and nonprintable characters from the end of a string.
	do while Len (as_source) > 0 and not lb_char
		lc_char = Right (as_source, 1)
		if nf_IsPrintable(lc_char) and Not nf_IsSpace(lc_char) then
			lb_char = true
		else
			as_source = Left (as_source, Len (as_source) - 1)
		end if
	loop
	return as_source
	
ElseIf ab_remove_nonprint Then
	// Remove nonprintable characters from the end of a string.
	do while Len (as_source) > 0 and not lb_printable_char
		lc_char = Right (as_source, 1)
		if nf_IsPrintable(lc_char) then
			lb_printable_char = true
		else
			as_source = Left (as_source, Len (as_source) - 1)
		end if
	loop
	return as_source
	
ElseIf ab_remove_spaces Then
	//Remove spaces from the end of a string.
	return RightTrim(as_source)
End If

return as_source
end function

public function integer nf_setkeyvalue (ref string as_source, string as_keyword, string as_keyvalue, string as_separator);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_SetKeyValue
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string to have the set performed on.  Passed by reference.
//							Format:  keyword = value; ...
//	as_keyword		The keyword to set a value for.
//	as_keyvalue		The new value for the specified keyword.
//	as_separator	The separator character used in the source string.
//
//	Returns:			integer
//						1 Successful operation.
//						-1 The specified keywork did not exist in the source string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:	Sets the value portion of a keyword=value pair from a string
//
//////////////////////////////////////////////////////////////////////////////

integer	li_found=-1
integer	li_keyword, &
			li_separator, &
			li_equal
string	ls_temp

//Check paramemeters
If IsNull(as_source) or IsNull(as_keyword) or IsNull(as_keyvalue) or IsNull(as_separator) Then
	integer li_null
	SetNull (li_null)
	Return li_null
End If

do 
	li_keyword = Pos (Lower(as_source), Lower(as_keyword), li_keyword + 1)
	if li_keyword > 0 then
		ls_temp = LeftTrim (Right (as_source, Len(as_source) - (li_keyword + Len(as_keyword) - 1)))
		if Left (ls_temp, 1) = "=" then
			li_equal = Pos (as_source, "=", li_keyword + 1)
			li_separator = Pos (as_source, as_separator, li_equal + 1)
			if li_separator > 0 then
				as_source = Left(as_source, li_equal) + as_keyvalue + as_separator + Right(as_source, Len(as_source) - li_separator)
			else
				as_source = Left(as_source, li_equal) + as_keyvalue
			end if
			li_found = 1
		end if
	end if
loop while li_keyword > 0

return li_found
end function

public function string nf_trim (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Trim
//
//	Access:  		public
//
//	Arguments:
//	as_source		The string to be trimmed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the left end 
//						of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the left and right end of 
//						a string.
//						The options depending on the parameters are:
//							Remove spaces from the beginning and end of a string.
//							Remove nonprintable characters from the beginning and 
//							end of a string.
//							Remove spaces and nonprintable characters from the 
//							beginning and end of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

// Remove Spaces=True, NonPrintCharacters=False
return nf_Trim (as_source, True, False)

end function

public function string nf_trim (string as_source, boolean ab_remove_spaces);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Trim
//
//	Access:  		public
//
//	Arguments:
//	as_source			The string to be trimmed.
//	ab_remove_spaces	A boolean stating if spaces should be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the left end 
//						of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the left and right end of 
//						a string.
//						The options depending on the parameters are:
//							Remove spaces from the beginning and end of a string.
//							Remove nonprintable characters from the beginning and 
//							end of a string.
//							Remove spaces and nonprintable characters from the 
//							beginning and end of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) or IsNull(ab_remove_spaces) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

// Remove Spaces=ab_remove_spaces, NonPrintCharacters=False
return nf_Trim (as_source, ab_remove_spaces, False)

end function

public function string nf_trim (string as_source, boolean ab_remove_spaces, boolean ab_remove_nonprint);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Trim
//
//	Access:  		public
//
//	Arguments:
//	as_source				The string to be trimmed.
//	ab_remove_spaces		A boolean stating if spaces should be removed.
//	ab_remove_nonprint	A boolean stating if nonprint characters should be removed.
//
//	Returns:  		string
//						as_source with all desired characters removed from the left and 
//						right end of the string.
//						If any argument's value is NULL, function returns NULL.
//
//	Description: 	Removes desired characters from the left and right end of 
//						a string.
//						The options depending on the parameters are:
//							Remove spaces from the beginning and end of a string.
//							Remove nonprintable characters from the beginning and 
//							end of a string.
//							Remove spaces and nonprintable characters from the 
//							beginning and end of a string.
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) or IsNull(ab_remove_spaces) or IsNull(ab_remove_nonprint) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

If ab_remove_spaces and ab_remove_nonprint Then
	// Remove spaces and nonprintable characters from the beginning and end 
	// of a string.
	as_source = nf_LeftTrim (as_source, ab_remove_spaces, ab_remove_nonprint)
	as_source = nf_RightTrim(as_source, ab_remove_spaces, ab_remove_nonprint)

ElseIf ab_remove_nonprint Then
	// Remove nonprintable characters from the beginning and end
	// of a string.
	as_source = nf_LeftTrim (as_source, ab_remove_spaces, ab_remove_nonprint)
	as_source = nf_RightTrim(as_source, ab_remove_spaces, ab_remove_nonprint)

ElseIf ab_remove_spaces Then
	//Remove spaces from the beginning and end of a string.
	as_source = Trim(as_source)

End If

return as_source
end function

public function string nf_wordcap (string as_source);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_WordCap
//
//	Access: 			public
//
//	Arguments:
//	as_source		The source string.
//
//	Returns:  		String
//						Returns string with the first letter of each word set to
//						uppercase and the remaining letters lowercase if it succeeds
//						and NULL if an error occurs.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Sets the first letter of each word in a string to a capital 
//						letter and all other letters to lowercase (for example, 
//						ROBERT E. LEE would be Robert E. Lee).
//////////////////////////////////////////////////////////////////////////////

integer	li_pos
boolean	lb_capnext
string 	ls_ret
long		ll_stringlength
char		lc_char
char		lc_string[]

//Check parameters
If IsNull(as_source) Then
	string ls_null
	SetNull(ls_null)
	Return ls_null
End If

//Get and check length
ll_stringlength = Len(as_source)
If ll_stringlength = 0 Then
	Return as_source
End If

//Convert all characters to lowercase and put it into Character Array
lc_string = Lower(as_source)

//The first character should be capitalized
lb_capnext = TRUE

//Loop through the entire string
For li_pos = 1 to ll_stringlength
	//Get one character at a time
	lc_char = lc_string[li_pos]
	
	If Not nf_IsAlpha(lc_char) Then
		//The next character should be capitalized
		lb_capnext = True
	ElseIf lb_capnext Then
		//Capitalize this Alphabetic character
		lc_string[li_pos] = Upper(lc_char)
		//The next character should not be capitalized
		lb_capnext = False
	End If
Next

//Copy the Character array back to a string variable
ls_ret = lc_string

//Return the 
return ls_ret
end function

public function string nf_globalreplace (string as_EntireString, string as_ReplaceString, string as_ReplaceWithString, long al_StartingPosition);// This function replaces all occurrences of:	as_ReplaceString
//	with:														as_ReplaceWithString
// in the string:											as_EntireString
//	beginning at the position specified in:		al_StartingPosition.

Long		ll_ReplaceLength

// Get the lenth of the string we will replace.
ll_ReplaceLength = Len( as_ReplaceString )

// Set the starting position to 1 if the user passed it as zero.
IF al_StartingPosition = 0 THEN al_StartingPosition = 1

// Find the first occurence of the string to be replaced in the
// Entire String, beginning at the starting position.
al_StartingPosition = Pos( as_EntireString, as_ReplaceString, &
	al_StartingPosition )

// If the string to be replaced was found, replace it and then find
// the next occurence.  Repeat this process until the string is no
// longer found.
DO UNTIL al_StartingPosition < 1
	as_EntireString = Replace( as_EntireString, al_StartingPosition, &
		ll_ReplaceLength, as_ReplaceWithString )
	al_StartingPosition = Pos( as_EntireString, as_ReplaceString, &
		al_StartingPosition + Len(as_ReplaceWithString) )
LOOP

Return as_EntireString
end function

public function integer nf_parseobjstring (datawindow dw_arg, ref string obj_list[], string obj_type, string band);/*
ARGUMENTS:
		dw_arg	(the datawindow control passed by value)
		obj_list   (a string array passed by reference)
		obj_type  (a string passed by value)
		band  (a string passed by value)

THE FOLLOWING FUNCTION WILL PARSE THE LIST OF OBJECTS CONTAINED
IN THE DATAOBJECT ASSOCIATED WITH A DATAWINDOW CONTROL, RETURNING 
THEIR NAMES INTO A STRING ARRAY PASSED BY REFERENCE,AND RETURNING 
THE NUMBER OF NAMES IN THE ARRAY AS THE RETURN VALUE OF THE FUNCTION.

YOU CAN CONTROL THE OBJECTS  RETURNED BY TYPE AND BY BAND.
SPECIFYING AN "*" FOR EITHER TYPE OR BAND MEANS THAT YOU WANT ALL
OCCURENCES OF OBJECTS ACROSS TYPE OR BAND.  

NOTE:	IF THIS FUNCTION WILL BE USED AGAINST COLUMNS, ALL OF YOUR
	COLUMNS MUST HAVE NAMES!! 

EXAMPLE1:	obj_num = f_parse_obj_string(dw_1,mylist,"text","header")
		WILL RETURN THE NAMES OF ALL TEXT OBJECTS IN THE HEADER
		BAND OF dw_1 INTO mylist, AND THE NUMBER OF NAMES
		RETURNED INTO obj_num.
EXAMPLE2:	obj_num = f_parse_obj_string(dw_1,mylist,"column","*")
		WILL RETURN THE NAMES OF ALL COLUMN OBJECTS IN dw_1 INTO
		mylist, AND THE NUMBER OF NAMES RETURNED INTO obj_num.
EXAMPLE3:	obj_num = f_parse_obj_string(dw_1,mylist,"*","summary")
		WILL RETURN THE NAMES OF ALL OBJECTS IN THE SUMMARY
		BAND OF dw_1 INTO mylist, AND THE NUMBER OF NAMES
		RETURNED INTO obj_num.
EXAMPLE4:	obj_num = f_parse_obj_string(dw_1,mylist,"*","*")
		WILL RETURN THE NAMES OF ALL OBJECTS IN dw_1  INTO mylist, AND
		THE NUMBER OF NAMES RETURNED INTO obj_num. 
*/

string obj_string, obj_holder
int start_pos=1, tab_pos, count = 0

obj_string = Describe(dw_arg,"datawindow.objects")

tab_pos =  Pos(obj_string,"~t",start_pos)
DO WHILE tab_pos > 0
	obj_holder = Mid(obj_string,start_pos,(tab_pos - start_pos))
	IF (Describe(dw_arg,obj_holder+".type") = obj_type or obj_type = "*") AND &
		 (Describe(dw_arg,obj_holder+".band") = band or band = "*") THEN
			count = count + 1
			obj_list[count] = obj_holder
	END IF
start_pos = tab_pos + 1
tab_pos =  Pos(obj_string,"~t",start_pos)	
LOOP 
obj_holder = Mid(obj_string,start_pos,Len(obj_string))
IF (Describe(dw_arg,obj_holder+".type") = obj_type or obj_type = "*") AND &
		 (Describe(dw_arg,obj_holder+".band") = band or band = "*") THEN
	count = count + 1
	obj_list[count] = obj_holder
END IF

Return count
end function

public function long nf_npos (string as_string1, string as_string2, long al_start, long al_occurrances);// this will find the nth occurrence of a substring in a string
// very similar to Pos()
// If al_occurrances is < 0 Then it will find the Nth occurrence from the right

Long	ll_counter, &
		ll_return

String	ls_temp

If al_occurrances >= 0 Then
	ll_return = al_start - 1
	For ll_counter = 1 to al_occurrances
		ll_return = Pos(as_string1, as_string2, ll_return + 1)
		If ll_return = 0 Then Exit 
	Next
Else
	ls_temp = Reverse(as_string1)
	
	ll_return = This.nf_npos(ls_temp, Reverse(as_string2), Len(as_string1) - al_start + 1, Abs(al_occurrances))
	ll_return = (Len(as_string1) - ll_return) + 1
End if
return ll_return
end function

public function boolean nf_isalpha (string as_check_string, integer ai_startpos, integer ai_end_pos);Int li_Loop_cnt

as_check_string = Upper(Trim(as_check_string))
IF ai_startpos = 0 Then
   ai_startpos = 1
END IF

IF ai_end_pos = 0 Then
   IF LEN(as_check_string) = 0 Then
      RETURN FALSE   
   ELSE
	   ai_end_pos = LEN(as_check_string)
	END IF
END IF

IF ai_end_pos < ai_startpos Then
   RETURN FALSE
End if

For li_Loop_cnt = ai_startpos TO ai_end_pos
    IF Asc(MID( as_check_string,li_loop_cnt,1)) > 90 OR &
       Asc(MID( as_check_string,li_loop_cnt,1)) < 65 THEN
		 Return FALSE // NOT A - Z
    END IF
Next
Return True
end function

public function string nf_encrypt (string as_text);Int	li_len, &
		li_counter

String	ls_return


ls_return = ''
li_len = Len(as_text)
For li_counter = 1 to li_len
	ls_return += String(Char(Asc(as_text) * 2))
	as_text = Right(as_text, Len(as_text) - 1)
Next

return ls_return

end function

public function string nf_decrypt (string as_text);Int	li_len, &
		li_counter

String	ls_return


ls_return = ''
li_len = Len(as_text)
For li_counter = 1 to li_len
	ls_return += String(Char(Asc(as_text) / 2))
	as_text = Right(as_text, Len(as_text) - 1)
Next

return ls_return

end function

public function boolean nf_amiempty (string as_string);// Checks to see if a string has more than Spaces,Tabs or zeros in it
// IF it does it is not considered empty
// IF it is NULL or only has spaces tabs or zeroes it os empty


Long	ll_pos,&
		ll_Tab_To_Find

String ls_Tab

ll_Tab_To_Find = 1

ls_Tab = '~t'

If IsNull(as_String) or Len(Trim(as_String)) = 0 then return true

ll_pos = nf_npos( as_String, ls_Tab, 1, ll_Tab_To_Find)
DO While ll_Pos > 0 
	as_String = Replace(As_String, ll_Pos, 1, " ")
	ll_pos = nf_npos(as_String, ls_tab,1,ll_Tab_To_Find)
Loop
ll_pos = nf_npos( as_String, "0", 1, ll_Tab_To_Find)
DO While ll_Pos > 0 
	as_String = Replace(As_String, ll_Pos, 1, " ")
	ll_pos = nf_npos(as_String, "0",1,ll_Tab_To_Find)
Loop

IF Len(Trim(as_String)) = 0 Then Return TRUE

return false
end function

public function integer nf_parseleftright (string as_initial_string, string as_delimeter, ref string as_left_string, ref string as_right_string);/*
purpose:	This function will return everything to the left of the as_delimeter 
			into as_left_string, and everything to the right of parsing string into
			as_right_string.

args:		as_initial_string		string			... string to parse
			as_delimeter		string			... what to look for
			as_left_string			string by ref	... filled with values to left of parsing string
			as_right_string		string by ref	... filled with values to the right of parsing string

returns:	int		... 1 if parsing string is found, -1 if parsing string is not found
*/

long ll_parser_position

ll_parser_position = Pos( as_initial_string, as_delimeter )
IF ll_parser_position = 0 THEN
	RETURN -1
END IF

as_left_string = Left( as_initial_string,  ll_parser_position - 1 )
as_right_string = Right( as_initial_string, Len( as_initial_string ) - ll_parser_position - Len( as_delimeter ) + 1 )
RETURN 1
end function

public function integer nf_replacerows (string as_datatoimport, ref long al_rowstoreplace[], ref datawindow adw_datawindowtarget);Int	lia_SelectedRows[], &
		li_SelectedUpperBound, &
		li_Counter, &
		li_RowsToReplaceUpperBound

Long	ll_StartRow,&
		ll_EndRow,&
		ll_CRLF,&
		ll_OldCRLF, &
		ll_ArrayPos
		

String	ls_Replace
ll_EndRow = adw_DataWindowTarget.RowCount()

adw_DataWindowTarget.SetRedraw(False)

For li_Counter = 1 to ll_EndRow
	If adw_DataWindowTarget.IsSelected(li_Counter) Then
		li_SelectedUpperBound ++
		lia_SelectedRows[li_SelectedUpperBound] = li_counter
	End if
Next

li_RowsToReplaceUpperBound = UpperBound(al_rowstoreplace)

ll_ArrayPos = 1

ll_OldCRLF = 1

For li_Counter = 1 to li_RowsToReplaceUpperBound
	ll_CRLF = POS( as_DataToImport, "~r~n", ll_CRLF + 1)
	IF ll_crlf > 0 Then
		ls_Replace = Mid( as_DataToImport, ll_OldCRLF, ll_CRLF - ll_OldCRLF) 
	ELSE
		LS_replace = Mid(as_DataToImport, ll_OldCRLF)
	END IF
	ll_StartRow = al_RowsToReplace[ll_ArrayPos] + 1
	IF ll_StartRow <= ll_EndRow Then
		adw_DataWindowTarget.RowsMove( ll_StartRow, ll_EndRow, Primary!, &
												adw_DataWindowTarget, 1, Filter! )	

		adw_DataWindowTarget.RowsDiscard( al_RowsToReplace[ll_ArrayPos], &
											al_RowsToReplace[ll_ArrayPos], Primary!) 
		adw_DataWindowTarget.ImportString( ls_Replace)
		adw_DataWindowTarget.RowsMove( 1, adw_DataWindowTarget.FilteredCount(), Filter!, &
												adw_DataWindowTarget, adw_datawindowtarget.RowCount() +1 , Primary! )	
		ll_ArrayPos ++
	ELSE
		adw_DataWindowTarget.RowsDiscard( al_RowsToReplace[ll_ArrayPos], &
											al_RowsToReplace[ll_ArrayPos], Primary!) 
		adw_DataWindowTarget.ImportString( ls_Replace)
	END IF
	ll_OldCRLF = ll_CRLF + 2

Next


adw_DataWindowTarget.SelectRow(0, False)
For li_Counter = 1 to li_SelectedUpperBound
	adw_DataWindowTarget.SelectRow(lia_SelectedRows[li_counter], True)
Next

adw_DataWindowTarget.SetRedraw(True)
Return ll_ArrayPos
end function

public function string nf_buildupdatestring (datawindow adw_tobuildfrom);///////////////////////////////////////////////////////////////////////////////////////
//
// Modified Date		Author			Description
//
// 7/18/96				Tim Bornholtz	This will now read the delete buffer also.
// 04/16/2002			Elwin McKernan	PB8 Migration. 
//							SetRedraw(True) when RowCount = 0 causes Memory Violation in w_pas_source
///////////////////////////////////////////////////////////////////////////////////////
Int		li_counter, &
			lia_SelectedRows[], &
			li_SelectedUpperBound,&
			li_MaskCounter

Long		ll_RowCount, &
			ll_ColumnCount, &
			ll_DeletedCount

String 	ls_update_flag, &
			ls_data, &
			ls_OldMask[]

Boolean  lb_ResetRedraw

If Integer(adw_tobuildfrom.Describe("update_flag.id")) < 1 Then 
	return "you don't have an update_flag column"
End if

ll_RowCount = adw_tobuildfrom.RowCount()

//** IBDKEEM ** 04/16/2002 ** PB8 Migration 
IF ll_RowCount > 0 THEN
	adw_tobuildfrom.SetRedraw(False)
	lb_ResetRedraw = true
else
	lb_ResetRedraw = false
END IF

For li_Counter = 1 to ll_RowCount
	If adw_ToBuildFrom.IsSelected(li_Counter) Then
		li_SelectedUpperBound ++
		lia_SelectedRows[li_SelectedUpperBound] = li_counter
	End if
Next

For li_counter = 1 to ll_RowCount
	ls_update_flag = adw_ToBuildFrom.GetItemString(li_counter, 'update_flag')
	If ls_update_flag = 'A' or ls_update_flag = 'U' Then
		adw_ToBuildFrom.RowsCopy(li_counter, li_counter, Primary!, adw_ToBuildFrom, 10000, Filter!)
	Else
		adw_ToBuildFrom.RowsMove(li_counter, li_counter, Primary!, adw_ToBuildFrom, 10000, Filter!)
		ll_RowCount --
		li_counter --
	End if
Next

ll_DeletedCount = adw_ToBuildFrom.DeletedCount()
If ll_DeletedCount > 0 Then
	adw_ToBuildFrom.RowsMove( 1, ll_DeletedCount, Delete!, adw_ToBuildFrom, 10000, Primary!)
	// Set update flag to 'D'
	// ll_RowCount is set to the OLD RowCount
	For li_Counter = ll_RowCount + 1 to ll_RowCount + ll_DeletedCount
		adw_ToBuildFrom.SetItem(li_Counter, 'update_flag', 'D')
	Next
End if

ll_ColumnCount = Long(adw_ToBuildFrom.Describe("DataWindow.Column.Count"))
For li_Counter = 1 to ll_ColumnCount
	// Get the column type of the clicked field
	If Left( Lower(adw_ToBuildFrom.Describe("#" + String(li_Counter) + ".ColType")), 4) = 'date' Then
		li_MaskCounter++
		ls_OldMask[li_MaskCounter] = adw_tobuildfrom.Describe("#" + String(li_Counter) + ".EditMask.Mask")
		adw_tobuildfrom.Modify("#" + String(li_Counter) + ".Edit.format = 'yyyy-mm-dd'")
	End if
Next

ls_data = adw_ToBuildFrom.Describe("DataWindow.Data")
li_MaskCounter = 0
For li_Counter = 1 to ll_ColumnCount
	If Left( Lower(adw_ToBuildFrom.Describe("#" + String(li_Counter) + ".ColType")), 4) = 'date' Then
			li_MaskCounter ++
			adw_tobuildfrom.Modify("#" + String(li_Counter) + ".EditMask.Mask ='" +ls_OldMask[li_MaskCounter]+"'" )
	End if
Next

adw_ToBuildFrom.RowsDiscard(1, adw_ToBuildFrom.RowCount(), Primary!)
adw_ToBuildFrom.RowsMove(1, adw_ToBuildFrom.FilteredCount(), Filter!, adw_ToBuildFrom, 1, Primary!)

adw_ToBuildFrom.SelectRow(0, False)
For li_Counter = 1 to li_SelectedUpperBound
	adw_ToBuildFrom.SelectRow(lia_SelectedRows[li_counter], True)
Next

//** IBDKEEM ** 04/16/2002 ** PB8 Migration 
IF lb_ResetRedraw THEN
	adw_tobuildfrom.SetFocus()
	adw_tobuildfrom.SetRedraw(True)
END IF

return ls_data
end function

public function long nf_compare_dates (long al_row, string as_searchstring, datawindow adw_tosearch);///////////////////////////////////////////////////////////////////////////////////////
// This function will check that the start date and end date on 
//	a row do not overlap with another row of the same key
// !!! The fields in datawindow must be named start_date and end_date
// Modified Date		Author			Description
//
// 
// 
///////////////////////////////////////////////////////////////////////////////////////
Date			ldt_found_start_date, &
				ldt_found_end_date, &
				ldt_start_date, &
				ldt_end_date 
				
long 			ll_nbrrows, &
				ll_rtn

ll_nbrrows = adw_tosearch.RowCount()

ldt_start_date = adw_tosearch.GetItemDate &
	(al_row, "start_date")
ldt_end_date = adw_tosearch.GetItemDate &
	(al_row, "end_date")

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = adw_tosearch.Find  &
				( as_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = adw_tosearch.Find ( as_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = adw_tosearch.Find  &
			(as_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = adw_tosearch.Find ( as_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
	ldt_found_start_date = adw_tosearch.GetItemDate &
	(ll_rtn, "start_date")
	ldt_found_end_date = adw_tosearch.GetItemDate &
	(ll_rtn, "end_date")
	
	If (ldt_found_start_date > ldt_start_date And ldt_found_start_date < ldt_end_date) Then

		Return ll_rtn
	ElseIf (ldt_found_end_date > ldt_start_date And ldt_found_end_date < ldt_end_date) Then
	
		 Return ll_rtn
	ElseIf (ldt_start_date > ldt_found_start_date and ldt_start_date < ldt_found_end_date) Then

		Return ll_rtn
	ElseIf(ldt_end_date > ldt_found_start_date and ldt_end_date < ldt_found_end_date) Then

		Return ll_rtn
	ElseIf (ldt_found_start_date = ldt_start_date) Then
	
		Return ll_rtn
	ElseIf (ldt_found_end_date = ldt_start_date) Then
			  
		Return ll_rtn
	Else
		ll_rtn = 0
		Return ll_rtn
	End if
Else
	Return ll_rtn
End if

end function

on u_string_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_string_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

