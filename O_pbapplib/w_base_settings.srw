HA$PBExportHeader$w_base_settings.srw
forward
global type w_base_settings from window
end type
type tab_1 from tab within w_base_settings
end type
type tabpage_3 from userobject within tab_1
end type
type dw_window from datawindow within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_window dw_window
end type
type tabpage_2 from userobject within tab_1
end type
type dw_toolbars from datawindow within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_toolbars dw_toolbars
end type
type tab_1 from tab within w_base_settings
tabpage_3 tabpage_3
tabpage_2 tabpage_2
end type
type cb_reset from commandbutton within w_base_settings
end type
type cb_ok from commandbutton within w_base_settings
end type
type s_toolbar from structure within w_base_settings
end type
end forward

type s_toolbar from structure
	string		se_alignment
	integer		se_text
	integer		se_tips
	integer		se_hide
end type

global type w_base_settings from window
integer x = 352
integer y = 248
integer width = 2094
integer height = 1008
boolean titlebar = true
string title = "Settings"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
tab_1 tab_1
cb_reset cb_reset
cb_ok cb_ok
end type
global w_base_settings w_base_settings

type variables
// Holds the pointer to its parent window
w_base_frame  iw_parent

// Holds pointer to application object
application ia_app

// Holds the current toolbar alignment
ToolBarAlignMent  ie_ToolBarAlignment

//microhelp color values
long	il_message_color, &
	il_status_color, &
	il_userid_color, &
	il_date_color

integer	il_show_userid, &
	il_show_status, &	
	il_show_date

boolean	ib_colorwindowopen

datawindow	idw_active

integer	ii_prompt_on_exit, &
	ii_prompt_to_save_onexit, &
	ii_save_dw_layouts, &	
	ii_display_hscrollbars, &
	ii_display_vscrollbars, &
	ii_prompt_on_close
string	is_always_openas		


private:
s_toolbar	istr_toolbar
end variables

forward prototypes
public subroutine wf_set_toolbars ()
public subroutine wf_initialize (datawindow adw_target)
end prototypes

public subroutine wf_set_toolbars ();u_conversion_functions	lu_conv

CHOOSE CASE iw_parent.ToolbarAlignment
	CASE AlignAtLeft! 
		istr_toolbar.se_alignment	=	'alignatleft'
	CASE AlignAtRight! 
		istr_toolbar.se_alignment	=	'alignatright'
	CASE AlignAtTop!  
		istr_toolbar.se_alignment	=	'alignattop'
	CASE Floating!  
		istr_toolbar.se_alignment	=	'floating'
END CHOOSE
If iw_parent.ToolBarVisible Then
	istr_toolbar.se_hide = 1
Else
	istr_toolbar.se_hide = 0
End If

	istr_toolbar.se_text = lu_conv.nf_integer(ia_app.ToolBarText)

	istr_toolbar.se_tips = lu_conv.nf_integer(ia_app.ToolBarTips)

end subroutine

public subroutine wf_initialize (datawindow adw_target);u_base_data	lu_base_data

lu_base_data = gw_base_frame.iu_base_data

ii_prompt_on_exit				=	lu_base_data.ii_prompt_on_exit
ii_prompt_to_save_onexit	=	lu_base_data.ii_prompt_to_save_onexit
ii_save_dw_layouts			=	lu_base_data.ii_save_dw_layouts
ii_display_hscrollbars		=	lu_base_data.ii_display_hscrollbars
ii_display_vscrollbars		=	lu_base_data.ii_display_vscrollbars
ii_prompt_on_close			=	lu_base_data.ii_prompt_on_close
is_always_openas				=	lu_base_data.is_always_openas
adw_target.Object.prompt_on_exit[1]						=	ii_prompt_on_exit
adw_target.Object.prompt_to_save_onexit[1]			=	ii_prompt_to_save_onexit
adw_target.Object.save_datawindow_layouts[1]			=	ii_save_dw_layouts
adw_target.Object.display_horizontal_scrollbars[1]	=	ii_display_hscrollbars
adw_target.Object.display_vertical_scrollbars[1]	=	ii_display_vscrollbars
adw_target.Object.prompt_on_close[1]					=	ii_prompt_on_close
adw_target.Object.always_openas[1]						=	is_always_openas

end subroutine

on w_base_settings.create
this.tab_1=create tab_1
this.cb_reset=create cb_reset
this.cb_ok=create cb_ok
this.Control[]={this.tab_1,&
this.cb_reset,&
this.cb_ok}
end on

on w_base_settings.destroy
destroy(this.tab_1)
destroy(this.cb_reset)
destroy(this.cb_ok)
end on

event open;// Get the passed pointer to the parent window
// Note: This window must be opened with Parm (OpenWithParm)
// and passed the pointer of the parent window (frame window)
iw_parent = Message.PowerObjectParm

// Get a pointer to the applicaition object
ia_app = GetApplication()

Long	ll_FrameX, &
		ll_FrameY
		
Integer	li_ret		
		
ll_FrameX = gw_netwise_frame.X
ll_FrameY = gw_netwise_frame.Y

li_ret = This.Move(ll_FrameX + 400, ll_FrameY + 200)

wf_initialize(tab_1.tabpage_3.dw_window)





end event

type tab_1 from tab within w_base_settings
event selectionchanging pbm_tcnselchanging
integer x = 5
integer width = 2075
integer height = 752
integer taborder = 10
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean showpicture = false
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tabpage_3 tabpage_3
tabpage_2 tabpage_2
end type

event selectionchanging;DragObject	ldo_object

IF oldindex <= UpperBound(Control) AND oldindex > 0 THEN
	This.Control[oldindex].Control[1].TriggerEvent("ue_update")
END IF

IF newindex <= UpperBound(Control) AND newindex > 0 THEN
	ldo_object = This.Control[newindex].Control[1]
	ldo_object.SetFocus()
END IF
end event

on tab_1.create
this.tabpage_3=create tabpage_3
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_3,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_3)
destroy(this.tabpage_2)
end on

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2039
integer height = 624
long backcolor = 12632256
string text = "Window"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_window dw_window
end type

on tabpage_3.create
this.dw_window=create dw_window
this.Control[]={this.dw_window}
end on

on tabpage_3.destroy
destroy(this.dw_window)
end on

type dw_window from datawindow within tabpage_3
event ue_reset ( )
integer x = 5
integer y = 4
integer width = 2034
integer height = 616
integer taborder = 3
string dataobject = "d_settings"
boolean border = false
boolean livescroll = true
end type

event ue_reset;This.Object.prompt_on_exit[1]						=	1
This.Object.prompt_to_save_onexit[1]			=	0
This.Object.save_datawindow_layouts[1]			=	0
This.Object.display_horizontal_scrollbars[1]	=	0
This.Object.display_vertical_scrollbars[1]	=	0
This.Object.prompt_on_close[1]					=	1
This.Object.always_openas[1]						=	'original'
ii_prompt_on_exit				=	1
ii_prompt_to_save_onexit	=	0
ii_save_dw_layouts			=	0
ii_display_hscrollbars		=	0
ii_display_vscrollbars		=	0
ii_prompt_on_close			=	1
is_always_openas				=	'original'

end event

event constructor;InsertRow(0)

end event

event destructor;gw_base_frame.iu_base_data.Trigger Event ue_reset_winsettings(ii_prompt_on_exit, &
	ii_prompt_to_save_onexit, ii_save_dw_layouts, ii_display_hscrollbars, &
	ii_display_vscrollbars, ii_prompt_on_close, is_always_openas)
end event

event itemchanged;
CHOOSE CASE	dwo.Name
	CASE	'prompt_on_exit'
		ii_prompt_on_exit				=	Long(Data)
	CASE	'prompt_to_save_onexit'
		ii_prompt_to_save_onexit	=	Long(Data)
	CASE	'save_datawindow_layouts'
		ii_save_dw_layouts			=	Long(Data)
	CASE	'display_horizontal_scrollbars'
		ii_display_hscrollbars		=	Long(Data)
	CASE	'display_vertical_scrollbars'
		ii_display_vscrollbars		=	Long(Data)
	CASE	'prompt_on_close'
		ii_prompt_on_close			=	Long(Data)
	CASE	'always_openas'
		is_always_openas				=	String(Data)
END CHOOSE
end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 2039
integer height = 624
long backcolor = 12632256
string text = "Toolbar"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_toolbars dw_toolbars
end type

on tabpage_2.create
this.dw_toolbars=create dw_toolbars
this.Control[]={this.dw_toolbars}
end on

on tabpage_2.destroy
destroy(this.dw_toolbars)
end on

type dw_toolbars from datawindow within tabpage_2
event constructor pbm_constructor
event getfocus pbm_dwnsetfocus
event itemchanged pbm_dwnitemchange
event ue_update pbm_custom01
event ue_reset ( )
integer y = 8
integer width = 1870
integer height = 616
integer taborder = 2
string dataobject = "d_toolbars"
boolean border = false
boolean livescroll = true
end type

event constructor;InsertRow(0)
end event

event getfocus;wf_set_toolbars()
This.Object.Data[1]	=	istr_toolbar
end event

event itemchanged;string	ls_data

u_conversion_functions	lu_conv

ls_data	=	String(data)
CHOOSE CASE dwo.Name
	CASE	'toolbar_position'
		CHOOSE CASE ls_data
			CASE	'alignatleft'
				iw_parent.ToolBarAlignment = AlignAtLeft! 
			CASE	'alignatright'
				iw_parent.ToolBarAlignment = AlignAtRight!
			CASE	'alignattop'
				iw_parent.ToolBarAlignment = AlignAtTop!
			CASE	'Bottom'
				iw_parent.ToolBarAlignment = AlignAtBottom!
			CASE	'Floating'
				iw_parent.ToolBarAlignment = Floating!
		END CHOOSE
	CASE	'show_text'
		ia_app.ToolBarText = lu_conv.nf_boolean(ls_data)
	CASE	'show_tips'
		ia_app.ToolBarTips = lu_conv.nf_boolean(ls_data)
	CASE	'hide_orshow'
		iw_parent.ToolBarVisible = lu_conv.nf_boolean(ls_data)
END CHOOSE
		
end event

event ue_reset;iw_parent.ToolBarAlignment = AlignAtTop! 
ia_app.ToolBarText = False
ia_app.ToolBarTips = True
iw_parent.ToolBarVisible = True

This.Object.toolbar_position[1]	=	'alignattop'
This.Object.show_text[1]			=	0
This.Object.show_tips[1]			=	1
This.Object.hide_orshow[1]			=	1


		


end event

type cb_reset from commandbutton within w_base_settings
integer x = 992
integer y = 792
integer width = 247
integer height = 108
integer taborder = 20
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;Tab_1.tabpage_2.dw_toolbars.Trigger Event ue_reset()
Tab_1.tabpage_3.dw_window.Trigger Event ue_reset()
end event

type cb_ok from commandbutton within w_base_settings
integer x = 686
integer y = 792
integer width = 247
integer height = 108
integer taborder = 30
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Close"
boolean default = true
end type

event clicked;Close(Parent)
end event

