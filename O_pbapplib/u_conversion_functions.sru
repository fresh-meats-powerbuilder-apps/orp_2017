HA$PBExportHeader$u_conversion_functions.sru
forward
global type u_conversion_functions from nonvisualobject
end type
end forward

global type u_conversion_functions from nonvisualobject autoinstantiate
end type
global u_conversion_functions u_conversion_functions

forward prototypes
public function boolean nf_boolean (integer ai_val)
public function boolean nf_boolean (string as_val)
public function integer nf_button (string as_source, ref button ae_button)
public function integer nf_icon (string as_source, ref icon ae_icon)
public function integer nf_integer (boolean ab_arg)
public function integer nf_sqlpreviewtype (string as_source, ref sqlpreviewtype a_sqlpreviewtype)
public function string nf_string (boolean ab_parm)
public function string nf_string (boolean ab_parm, string as_type)
public function string nf_string (button ae_button)
public function string nf_string (icon ae_icon)
public function string nf_string (sqlpreviewtype a_sqlpreviewtype)
public function string nf_string (toolbaralignment ae_alignment)
public function string nf_string (windowstate aws_windowstate)
public function integer nf_toolbaralignment (string as_align, ref toolbaralignment ae_toolbaralign)
public function integer nf_windowstate (string as_windowstate, ref windowstate aws_windowstate)
public function datetime nf_datetime (string as_datetime)
end prototypes

public function boolean nf_boolean (integer ai_val);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Boolean
//
//	Access:			public
//
//	Arguments:
//	ai_val			The integer to be converted to a boolean value.
//
//	Returns:  		boolean
//						The boolean representation of the integer value.
//						If any argument's value is NULL, function returns NULL.
//						If any argument's value is Invalid, function returns NULL.
//
//	Description:  Converts a integer value to a boolean.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(ai_val) or (ai_val>1) or (ai_val<0) Then
	Boolean lb_null
	SetNull(lb_null)
	Return lb_null
End If

If ai_val=1 Then
	Return True
End If

Return False

end function

public function boolean nf_boolean (string as_val);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Boolean
//
//	Access:  		public
//
//	Arguments:
//	as_val			The string to be converted to a boolean value.
//
//	Returns: 		boolean
// 					The boolean value of the string.
//						If any argument's value is NULL, function returns NULL.
//						If any argument's value is Invalid, function returns NULL.
//
//	Description:  	Converts a string value to a boolean value.
//
//////////////////////////////////////////////////////////////////////////////

Boolean lb_null
SetNull(lb_null)

//Check parameters
If IsNull(as_val) Then
	Return lb_null
End If

//Convert to uppercase
as_val = Upper(as_val)

Choose Case as_val
	Case 'TRUE', 'T', 'YES', 'Y', '1'
		Return True
	Case 'FALSE', 'F', 'NO', 'N', '0'
		Return False
End Choose

//Invalid input parameter
Return lb_null

end function

public function integer nf_button (string as_source, ref button ae_button);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_button
//
//	Access:  		public
//
//	Arguments:
//	as_source			The string value to be converted to button	datatype value.
//	a_sqlpreviewtype	A button variable passed by reference which will
//							hold the button value that the string value was
//							converted to.
//
//	Returns: 		integer	 
//						1 if a successful conversion was made.
//						If as_source value is NULL, function returns -1
//						If as_source value is Invalid, function returns -1
//
//	Description:  Converts a string value to a button data type value.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	Return -1
End If

//Convert to lowercase
as_source = Lower (as_source)

Choose Case as_source
	Case "ok", "ok!"
		ae_button = OK!

	Case "okcancel", "okcancel!"
		ae_button = OKCancel!

	Case "yesno", "yesno!"
		ae_button = YesNo!

	Case "yesnocancel", "yesnocancel!"
		ae_button = YesNoCancel!

	Case "retrycancel", "retrycancel!"
		ae_button = RetryCancel!

	Case "abortretryignore", "abortretryignore!"
		ae_button = AbortRetryIgnore!
		
	Case Else
		Return -1
End Choose

Return 1

end function

public function integer nf_icon (string as_source, ref icon ae_icon);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_Icon
//
//	Access:  		public
//
//	Arguments:
//	as_source			The string value to be converted to Icon datatype value.
//	a_sqlpreviewtype	A icon variable passed by reference which will
//							hold the icon value that the string value was
//							converted to.
//
//	Returns: 		integer	 
//						1 if a successful conversion was made.
//						If as_source value is NULL, function returns -1
//						If as_source value is Invalid, function returns -1
//
//	Description:  Converts a string value to a icon data type value.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	Return -1
End If

//Convert to lowercase
as_source = Lower (as_source)

Choose Case as_source
	Case "none", "none!"
		ae_icon = None!

	Case "question", "question!"
		ae_icon = Question!

	Case "information", "information!"
		ae_icon = Information!

	Case "stopsign", "stopsign!"
		ae_icon = StopSign!

	Case "exclamation", "exclamation!"
		ae_icon = Exclamation!
		
	Case Else
		Return -1
End Choose

Return 1

end function

public function integer nf_integer (boolean ab_arg);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		nf_Integer
//
//	Access: 		 	public
//
//	Arguments:
//	ab_arg			The boolean argument to be converted to an integer value.
//
//	Returns: 		integer
//						The integer representation of the boolean value.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:	Converts a boolean value to an integer value.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(ab_arg) Then
	Integer li_null
	SetNull(li_null)
	Return li_null
End If

If ab_arg Then
	//True
	Return 1
End If

//False
Return 0

end function

public function integer nf_sqlpreviewtype (string as_source, ref sqlpreviewtype a_sqlpreviewtype);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_SQLPreviewType
//
//	Access:  		public
//
//	Arguments:
//	as_source			The string value to be converted to SQLPreviewType
//							datatype value.
//	a_sqlpreviewtype	A SQLPreviewType variable passed by reference which will
//							hold the SQLPreviewType value that the string value was
//							converted to.
//
//	Returns: 		integer	 
//						1 if a successful conversion was made.
//						If as_source value is NULL, function returns NULL.
//						If as_source value is Invalid, function returns -1.
//
//	Description:  Converts a string value to a SQLPreviewType data type value.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(as_source) Then
	Integer li_null
	SetNull(li_null)
	Return li_null
End If

//Convert to lowercase
as_source = Lower (as_source)

If Pos (as_source, "insert") > 0 Then
	a_sqlpreviewtype = PreviewInsert!
	Return 1
ElseIf Pos (as_source, "delete") > 0 Then
	a_sqlpreviewtype = PreviewDelete!
	Return 1
ElseIf Pos (as_source, "update") > 0 Then
	a_sqlpreviewtype = PreviewUpdate!
	Return 1
ElseIf Pos (as_source, "retrieve") > 0 or &
		Pos (as_source, "select") > 0 Then
	a_sqlpreviewtype = PreviewSelect!
	Return 1
End If

//Invalid parameter.
Return -1

end function

public function string nf_string (boolean ab_parm);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		nf_String
//
//	Access:			public
//
//	Arguments:
//	ab_parm			The boolean value to be converted to a string.
//
//	Returns:  		string	
//						The string value of the passed boolean argument.
//						If any argument's value is NULL, function returns NULL.
//
//	Description:  	Returns the passed boolean value as a string.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(ab_parm) Then
	String ls_null
	SetNull(ls_null)
	Return ls_null
End If

Return nf_String(ab_parm, 'TRUEFALSE')

end function

public function string nf_string (boolean ab_parm, string as_type);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		nf_String
//
//	Access:			public
//
//	Arguments:
//	ab_parm			The boolean value to be converted to a string.
//	as_type			The string containing the desired return value
//						i.e., TrueFalse, TF, YesNo, YN, ZEROONE
//
//	Returns:  		string	
//						The string value of the passed boolean argument.
//						If any argument's value is NULL, function returns NULL.
//						If any argument's value is Invalid, function returns '!'.
//
//	Description:  Returns the passed boolean value as a string.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
String ls_true, ls_false

//Check parameters
If IsNull(ab_parm) or IsNull(as_type) Then
	String ls_null
	SetNull(ls_null)
	Return ls_null
End If

//Convert to uppercase
as_type = Upper(as_type)

//Check valid type and define true and false return values
Choose Case as_type
	Case 'TRUEFALSE'
		ls_true = 'TRUE'
		ls_false = 'FALSE'
	Case 'TF'
		ls_true = 'T'
		ls_false = 'F'
	Case 'YESNO'
		ls_true = 'YES'
		ls_false = 'NO'		
	Case 'YN'
		ls_true = 'Y'
		ls_false = 'N'
	Case 'ZEROONE'
		ls_true = '1'
		ls_false = '0'		
	Case Else
		Return '!'
End Choose
	
If ab_parm Then 
	Return ls_true
End If

Return ls_false

end function

public function string nf_string (button ae_button);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		nf_String
//
//	Access:			public
//
//	Arguments:
//	ae_button	The button value to be converted to a string.
//
//	Returns:  		string		
//						A string representation of the button value.
//						If ae_button is NULL, the function returns NULL.
//						If ae_button is Invalid, the function returns '!'.
//
//	Description:	Converts the button enumerated datatype to a 
//						readable string representation.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(ae_button) Then
	String ls_null
	SetNull(ls_null)
	Return ls_null
End If

Choose Case ae_button

	Case OK!
		Return "OK"

	Case OKCancel!
		Return "OKCancel"

	Case YesNo!
		Return "YesNo"

	Case YesNoCancel!
		Return "YesNoCancel"

	Case RetryCancel!
		Return "RetryCancel"

	Case AbortRetryIgnore!
		Return "AbortRetryIgnore"
	
End Choose

//Invalid parameter value
Return "!"
end function

public function string nf_string (icon ae_icon);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		nf_String
//
//	Access:			public
//
//	Arguments:
//	ae_icon			The icon value to be converted to a string.
//
//	Returns:  		string		
//						A string representation of the icon value.
//						If ae_icon is NULL, the function returns NULL.
//						If ae_icon is Invalid, the function returns '!'.
//
//	Description:	Converts the icon enumerated datatype to a 
//						readable string representation.
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(ae_icon) Then
	String ls_null
	SetNull(ls_null)
	Return ls_null
End If

Choose Case ae_icon

	Case None!
		Return "None"

	Case Question!
		Return "Question"

	Case Information!
		Return "Information"

	Case StopSign!
		Return "StopSign"

	Case exclamation!
		Return "Exclamation"
	
End Choose

//Invalid parameter value
Return "!"
end function

public function string nf_string (sqlpreviewtype a_sqlpreviewtype);//////////////////////////////////////////////////////////////////////////////
//
//	Function: 		nf_String
//
//	Access:  		public
//
//	Arguments:
//	a_sqlpreviewtype		The sqlpreviewtype that needs conversion.
//
//	Returns:  		string
//						A string representation of the sqlpreviewtype value.
//						If ae_alignment is NULL, the function returns NULL.
//						If ae_alignment is Invalid, the function returns '!'.
//
//	Description:  	Converts the sqlpreviewtype enumerated datatype to a 
//						readable string representation.
//
//////////////////////////////////////////////////////////////////////////////

If IsNull(a_sqlpreviewtype) Then
	String ls_null
	SetNull (ls_null)
	Return ls_null
End If
	
Choose Case a_sqlpreviewtype
	Case PreviewInsert!
		Return 'Insert'
	Case PreviewDelete!
		Return 'Delete'
	Case PreviewUpdate!
		Return 'Update'
	Case PreviewSelect!
		Return 'Retrieve'
End Choose

//Invalid parameter
Return '!'
end function

public function string nf_string (toolbaralignment ae_alignment);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		nf_String
//
//	Access:			public
//
//	Arguments:
//	ae_alignment	The toolbaralignment value to be converted to a string.
//
//	Returns:  		string		
//						A string representation of the toolbaralignment value.
//						If ae_alignment is NULL, the function returns NULL.
//						If ae_alignment is Invalid, the function returns '!'.
//
//	Description:	Converts the toolbaralignment enumerated datatype to a 
//						readable string representation.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(ae_alignment) Then
	String ls_null
	SetNull(ls_null)
	Return ls_null
End If

Choose Case ae_alignment

	Case alignattop!
		Return "Top"

	Case alignatbottom!
		Return "Bottom"

	Case alignatright!
		Return "Right"

	Case alignatleft!
		Return "Left"

	Case floating!
		Return "Floating"
	
End Choose

//Invalid parameter value
Return "!"
end function

public function string nf_string (windowstate aws_windowstate);//////////////////////////////////////////////////////////////////////////////
//
//	Function:		of_String
//
//	Access:			public
//
//	Arguments:
//	aws_windowstate	The windowstate value to be converted to a string.
//
//	Returns:  		string		
//						A string representation of the windowstate value.
//						If aws_windowstate is NULL, the function returns NULL.
//						If aws_windowstate is Invalid, the function returns '!'.
//
//	Description:	Converts the windowstate enumerated datatype to a 
//						readable string representation.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////

//Check parameters
If IsNull(aws_windowstate) Then
	String ls_null
	SetNull(ls_null)
	Return ls_null
End If

Choose Case aws_windowstate

	Case Normal!
		Return "normal"

	Case Maximized!
		Return "maximized"

	Case Minimized!
		Return "minimized"

End Choose

//Invalid parameter value
Return "!"
end function

public function integer nf_toolbaralignment (string as_align, ref toolbaralignment ae_toolbaralign);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_ToolbarAlignment
//
//	Access:  		public
//
//	Arguments:
//	as_align				The string value to be converted to toolbaralignment data type value
//	ae_toolbaralign	A toolbaralignment variable passed by reference which will hold the
//							toolbaralignment value that the string value was converted to
//
//	Returns: 		integer	 
//						1 if a successful conversion was made.
//						If as_align value is NULL, function returns NULL.
//						If as_align value is Invalid, function returns -1.
//
//	Description:  Converts a string value to a toolbaralignment data type value.
//
//////////////////////////////////////////////////////////////////////////////

//Convert to lowercase
as_align = Lower (as_align)

//Check parameters
If IsNull(as_align) Then
	Integer li_null
	SetNull(li_null)
	Return li_null
End If

If Pos (as_align, "top") > 0 Then
	ae_toolbaralign = alignattop!
	Return 1
	
ElseIf Pos (as_align, "bottom") > 0 Then
	ae_toolbaralign = alignatbottom!
	Return 1
	
ElseIf Pos (as_align, "left") > 0 Then
	ae_toolbaralign = alignatleft!
	Return 1
	
ElseIf Pos (as_align, "right") > 0 Then
	ae_toolbaralign = alignatright!
	Return 1
	
ElseIf Pos (as_align, "floating") > 0 Then
	ae_toolbaralign = floating!
	Return 1
	
End If

//Invalid parameter.
Return -1
end function

public function integer nf_windowstate (string as_windowstate, ref windowstate aws_windowstate);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  		nf_WindowState
//
//	Access:  		public
//
//	Arguments:
//	as_windowstate		The string value to be converted to windowstate data type value.
//	aws_windowstate	A windowstate variable passed by reference which will hold the
//							windowstate value that the string value was converted to.
//
//	Returns: 		integer	 
//						1 if a successful conversion was made.
//						If as_windowstate value is NULL, function returns NULL.
//						If as_windowstate value is Invalid, function returns -1.
//
//	Description:  Converts a string value to a windowstate data type value.
//
//////////////////////////////////////////////////////////////////////////////

//Convert to lowercase
as_windowstate = Lower (as_windowstate)

//Check parameters
If IsNull(as_windowstate) Then
	Integer li_null
	SetNull(li_null)
	Return li_null
End If

If Pos (as_windowstate, "maximized") > 0 Then
	aws_windowstate = Maximized!
	Return 1
	
ElseIf Pos (as_windowstate, "minimized") > 0 Then
	aws_windowstate = Minimized!
	Return 1
	
ElseIf Pos (as_windowstate, "normal") > 0 Then
	aws_windowstate = Normal!
	Return 1
	
End If

//Invalid parameter.
Return -1
end function

public function datetime nf_datetime (string as_datetime);//////////////////////////////////////////////////////////////////////
//
//	Function:		f_StringToDateTime
//	Description:	Converts a string in datetime format into a datetime
//						variable.  The string must be in standard PB format
//						with a space between the date and time fields.
//	Arguments:		as_DateTime	A string in the format:
//						"yyyy-mm-dd hh:mm:ss.ffffff"
//	Return:			DateTime
//						Returns 1900-01-01 00:00:00.000000 if an error
//						occurs.
//
//	Developer		Date		Description
//	---------		----		-----------
//
//////////////////////////////////////////////////////////////////////

DateTime	ldt_Return
Integer	li_SpacePosition
String	ls_Date, &
			ls_Time

// Get the position of the space within the string.
li_SpacePosition = Pos( as_DateTime, " ", 1 )

// If there is not a space, assume the entire string is the date.
IF li_SpacePosition < 1 THEN
	ldt_Return = DateTime( Date( as_DateTime ) )
ELSE
	// A space was found in the passed string, so parse out the characters
	// on the left as the date and on the right as the time.
	ls_Date = Left( as_DateTime, li_SpacePosition - 1 )
	ls_Time = Mid( as_DateTime, li_SpacePosition + 1 )

	// Convert the strings into a datetime.
	ldt_Return = DateTime( Date( ls_Date ), Time( ls_Time ) )

END IF

Return	ldt_Return
end function

on u_conversion_functions.create
TriggerEvent( this, "constructor" )
end on

on u_conversion_functions.destroy
TriggerEvent( this, "destructor" )
end on

