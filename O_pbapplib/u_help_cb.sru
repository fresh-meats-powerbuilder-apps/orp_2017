HA$PBExportHeader$u_help_cb.sru
forward
global type u_help_cb from u_base_commandbutton
end type
end forward

global type u_help_cb from u_base_commandbutton
string Text="&Help"
end type
global u_help_cb u_help_cb

on clicked;call u_base_commandbutton::clicked;parent.triggerevent("ue_help")
end on

