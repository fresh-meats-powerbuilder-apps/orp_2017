HA$PBExportHeader$w_about.srw
forward
global type w_about from w_base_response
end type
type p_ibp from picture within w_about
end type
type cb_ok from commandbutton within w_about
end type
type sle_exe_name from singlelineedit within w_about
end type
type sle_os from singlelineedit within w_about
end type
type sle_pbtype from singlelineedit within w_about
end type
type sle_free_resources from singlelineedit within w_about
end type
type sle_colors from singlelineedit within w_about
end type
type sle_height from singlelineedit within w_about
end type
type sle_width from singlelineedit within w_about
end type
type sle_datetime from singlelineedit within w_about
end type
type st_userid from statictext within w_about
end type
end forward

global type w_about from w_base_response
int X=691
int Y=209
int Width=1404
int Height=1345
long BackColor=1090519039
p_ibp p_ibp
cb_ok cb_ok
sle_exe_name sle_exe_name
sle_os sle_os
sle_pbtype sle_pbtype
sle_free_resources sle_free_resources
sle_colors sle_colors
sle_height sle_height
sle_width sle_width
sle_datetime sle_datetime
st_userid st_userid
end type
global w_about w_about

type variables

end variables

on w_about.create
int iCurrent
call w_base_response::create
this.p_ibp=create p_ibp
this.cb_ok=create cb_ok
this.sle_exe_name=create sle_exe_name
this.sle_os=create sle_os
this.sle_pbtype=create sle_pbtype
this.sle_free_resources=create sle_free_resources
this.sle_colors=create sle_colors
this.sle_height=create sle_height
this.sle_width=create sle_width
this.sle_datetime=create sle_datetime
this.st_userid=create st_userid
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=p_ibp
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=sle_exe_name
this.Control[iCurrent+4]=sle_os
this.Control[iCurrent+5]=sle_pbtype
this.Control[iCurrent+6]=sle_free_resources
this.Control[iCurrent+7]=sle_colors
this.Control[iCurrent+8]=sle_height
this.Control[iCurrent+9]=sle_width
this.Control[iCurrent+10]=sle_datetime
this.Control[iCurrent+11]=st_userid
end on

on w_about.destroy
call w_base_response::destroy
destroy(this.p_ibp)
destroy(this.cb_ok)
destroy(this.sle_exe_name)
destroy(this.sle_os)
destroy(this.sle_pbtype)
destroy(this.sle_free_resources)
destroy(this.sle_colors)
destroy(this.sle_height)
destroy(this.sle_width)
destroy(this.sle_datetime)
destroy(this.st_userid)
end on

event open;call super::open;environment	en_environ

u_sdkcalls	lu_sdkcalls

string		ls_filename

Constant Date ld_date = Today()
Constant Time lt_time = Now()


getenvironment(en_environ)
This.Title = "About " + gw_base_frame.Title

lu_sdkCalls = Create u_sdkCalls
Choose Case en_environ.OSType
	Case windows!
		If en_environ.Win16 Then
			sle_pbtype.Text = sle_pbtype.Text + "/16"
		Else
			sle_pbtype.Text = sle_pbtype.Text + "/32"
		End If

		If en_environ.OSMajorRevision = 4 Or &
			en_environ.OSMinorRevision = 95 Then
				sle_os.Text = sle_os.Text + " Windows 95"
		Else
			sle_os.Text = sle_os.Text + " Windows " + String(en_environ.OSMajorRevision) + &
								"." + String(en_environ.OSMinorRevision)
		End If
		
	Case windowsnt!
		If en_environ.Win16 Then
			sle_pbtype.Text = sle_pbtype.Text + "/16"
		Else
			sle_pbtype.Text = sle_pbtype.Text + "/32"
		End If
		
		sle_os.Text = sle_os.Text + " Windows NT " + String(en_environ.OSMajorRevision) + &
							"." + String(en_environ.OSMinorRevision)
							
	Case sol2!
		sle_pbtype.Text = sle_pbtype.Text + "/Unix"
		sle_os.Text = sle_os.Text + " Solaris " +  + String(en_environ.OSMajorRevision) + &
							"." + String(en_environ.OSMinorRevision)
	
	Case macintosh!
		sle_pbtype.Text = sle_pbtype.Text + "/Mac"
		sle_os.Text = sle_os.Text + " Macintosh " +  + String(en_environ.OSMajorRevision) + &
							"." + String(en_environ.OSMinorRevision)
End Choose

sle_pbtype.Text = "PowerBuilder "
If en_environ.PBType = desktop! Then
	sle_pbtype.Text = sle_pbtype.Text + "Desktop Version" + String(en_environ.PBMajorRevision) + &
						"." + String(en_environ.PBMinorRevision) + &
						"." + String(en_environ.PBFixesRevision)
Else
	sle_pbtype.Text = sle_pbtype.Text + "Enterprise Version " + String(en_environ.PBMajorRevision) + &
						"." + String(en_environ.PBMinorRevision) + &
						"." + String(en_environ.PBFixesRevision)
End If

sle_free_resources.text	=	sle_free_resources.text + &
		String(lu_sdkCalls.nf_GetFreeResourcePercent()) + '%'
sle_colors.text	= sle_colors.text + String(en_environ.Numberofcolors)
sle_height.text	= sle_height.text + String(en_environ.screenheight)
sle_width.text		= sle_width.text +String(en_environ.screenwidth)

ls_filename			=	lower(lu_sdkCalls.nf_GetModulefilename())

//Display File Creation Date/Time only for 32 bit operating systems
If en_environ.OSMajorRevision = 4 Or en_environ.OsType = WindowsNT! &
						Then
//	If lu_sdkcalls.nf_getcreationdatetime(ls_FileName, ls_Date, ls_Time) <> 1 Then
//		MessageBox("FileServices", "Error getting file date/time", Exclamation!)
//		Return
//	End If
	sle_exe_name.text	=	"Release Date/Time : " + String(ld_date) + ' ' + String( lt_time)
	sle_datetime.text	=	"Path : " + ls_filename
ELSE
	sle_exe_name.text	=	"Path : " + ls_filename
	sle_datetime.text	=	''
END IF
st_userid.text	=	'UserID:' + SQLCA.UserID
Destroy lu_sdkcalls

end event

type p_ibp from picture within w_about
int X=23
int Y=33
int Width=1354
int Height=629
boolean BringToTop=true
string PictureName="Ibpshado.bmp"
boolean FocusRectangle=false
end type

type cb_ok from commandbutton within w_about
int X=1084
int Y=993
int Width=247
int Height=109
int TabOrder=20
boolean BringToTop=true
string Text="&OK"
boolean Default=true
boolean Cancel=true
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Close(Parent)
end event

type sle_exe_name from singlelineedit within w_about
event ue_postopen pbm_custom01
int X=55
int Y=753
int Width=1313
int Height=65
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event constructor;PostEvent("ue_PostOpen")


end event

type sle_os from singlelineedit within w_about
event ue_postopen ( )
int X=5
int Y=809
int Width=1367
int Height=65
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
string Text="Operating System:"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_pbtype from singlelineedit within w_about
int X=14
int Y=869
int Width=1349
int Height=61
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
string Text="PowerBuilder"
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_free_resources from singlelineedit within w_about
int X=252
int Y=929
int Width=723
int Height=65
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
string Text="Free Resources:  "
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_colors from singlelineedit within w_about
int X=202
int Y=989
int Width=773
int Height=65
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
string Text="Number of Colors:  "
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_height from singlelineedit within w_about
int X=293
int Y=1109
int Width=723
int Height=65
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
string Text="Screen Height:  "
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_width from singlelineedit within w_about
int X=316
int Y=1049
int Width=714
int Height=65
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
string Text="Screen Width:  "
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type sle_datetime from singlelineedit within w_about
int X=51
int Y=685
int Width=1313
int Height=69
boolean BringToTop=true
boolean Border=false
boolean DisplayOnly=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_userid from statictext within w_about
int X=490
int Y=1173
int Width=554
int Height=65
boolean Enabled=false
boolean BringToTop=true
string Text="UserID"
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
boolean Italic=true
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

