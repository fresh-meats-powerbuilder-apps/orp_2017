HA$PBExportHeader$w_pa_resolve_response.srw
forward
global type w_pa_resolve_response from w_base_response_ext
end type
type dw_detail from u_base_dw_ext within w_pa_resolve_response
end type
type cb_complete from commandbutton within w_pa_resolve_response
end type
end forward

global type w_pa_resolve_response from w_base_response_ext
int Width=1591
int Height=1517
boolean TitleBar=true
string Title="Products Locked By PA Resolve"
long BackColor=12632256
dw_detail dw_detail
cb_complete cb_complete
end type
global w_pa_resolve_response w_pa_resolve_response

type variables

end variables

on open;call w_base_response_ext::open;String		ls_input_string


ls_input_string = Message.StringParm

dw_detail.SetRedraw(False)

dw_detail.ImportString(ls_input_string)

dw_detail.SetRedraw(True)

end on

on close;call w_base_response_ext::close;dw_detail.SetFilter("")
dw_detail.Filter()
dw_detail.Sort()
dw_detail.ShareDataOff()

end on

on w_pa_resolve_response.create
int iCurrent
call w_base_response_ext::create
this.dw_detail=create dw_detail
this.cb_complete=create cb_complete
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_detail
this.Control[iCurrent+2]=cb_complete
end on

on w_pa_resolve_response.destroy
call w_base_response_ext::destroy
destroy(this.dw_detail)
destroy(this.cb_complete)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, 'ABORT')
end event

event ue_base_ok;call super::ue_base_ok;CloseWithReturn( This, 'R')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pa_resolve_response
int X=1281
int Y=477
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pa_resolve_response
int X=1276
int Y=345
int TabOrder=60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pa_resolve_response
int X=1276
int Y=93
int Width=279
int TabOrder=50
string Text="&Retry"
end type

type dw_detail from u_base_dw_ext within w_pa_resolve_response
int X=1
int Y=1
int Width=1221
int Height=1421
int TabOrder=20
string DataObject="d_sales_detail_base"
boolean VScrollBar=true
boolean LiveScroll=true
end type

on ue_postconstructor;call u_base_dw_ext::ue_postconstructor;String			ls_modify_string
String			ls_temp

ls_modify_string = "ordered_units.Protect = 1 " + &
						"ordered_units.Background.Color = '12632256' " + &
						"scheduled_units.Protect = 1 " + &
						"scheduled_units.Background.Color = '12632256' " + &
						"product_code.Protect = 1 " + &
						"product_code.Background.Color = '12632256' " + &
						"ordered_age.Protect = 1 " + &
						"ordered_age.Background.Color = '12632256' "

ls_temp = dw_detail.Modify(ls_modify_string)

Return
end on

type cb_complete from commandbutton within w_pa_resolve_response
int X=1281
int Y=217
int Width=266
int Height=109
int TabOrder=40
string Text="&Complete"
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;CloseWithReturn(Parent, 'C')

end on

