HA$PBExportHeader$w_customer_order.srw
$PBExportComments$This is the Single/Multiple Customer Order Window
forward
global type w_customer_order from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_customer_order
end type
type dw_print from u_base_dw_ext within w_customer_order
end type
type tab_1 from tab within w_customer_order
end type
type tabpage_customer_order_detail from userobject within tab_1
end type
type dw_detail from u_base_dw_ext within tabpage_customer_order_detail
end type
type tabpage_customer_order_detail from userobject within tab_1
dw_detail dw_detail
end type
type tabpage_edi_comments_detail from userobject within tab_1
end type
type mle_1 from u_base_multilineedit_ext within tabpage_edi_comments_detail
end type
type st_1 from statictext within tabpage_edi_comments_detail
end type
type tabpage_edi_comments_detail from userobject within tab_1
mle_1 mle_1
st_1 st_1
end type
type tab_1 from tab within w_customer_order
tabpage_customer_order_detail tabpage_customer_order_detail
tabpage_edi_comments_detail tabpage_edi_comments_detail
end type
end forward

global type w_customer_order from w_base_sheet_ext
integer x = 5
integer y = 48
integer width = 3077
integer height = 1360
string title = "Customer Order"
event ue_cancel pbm_custom01
event ue_set_header_plant pbm_custom03
event ue_gensales ( )
event ue_getfirstproduct ( )
event ue_gotopadisplay ( )
event ue_getdefaults ( )
event ue_getx ( )
event ue_gety ( )
event ue_ncclicked pbm_nclbuttondown
dw_header dw_header
dw_print dw_print
tab_1 tab_1
end type
global w_customer_order w_customer_order

type variables
Private:
s_error		istr_error_info

// Is this 'single' or 'multiple' customer order
String		is_order_type, &
		is_initial_reference, &
		is_Additional_Data,&
		is_Customers, &
		is_description, &
		is_old_description, &
		is_load_instruction, &
		is_additional_data_input, &
		is_plant_table, &
		is_selected_plant_array, &
		is_header_ind, &
		is_modified_ind, &
		is_columnname, &
		is_dept_code, &
		is_cust_target, &
		is_gtl_split_ind, & 
		is_dest_country, &
		is_store_door, &
		is_deck_code, &
		is_temp_additional_data
		
boolean		ib_header, &
		ib_modified = false, &
		ib_open, &
		ib_Key_down = false
Integer		ii_RowsSelected,&
		ii_x, ii_y, &
		ii_dddw_x, ii_dddw_y

Long		il_RowBelowMouse, &
 		il_xpos, &
		il_ypos, &
		il_row, &
		il_old_row, &
		il_selected_row, &
		il_selected_plant_max = 10, &
		il_horz_scroll_pos
Window		iw_this
u_orp204	  	iu_orp204
u_orp002		iu_orp002
u_ws_orp2		iu_ws_orp2
u_ws_orp3		iu_ws_orp3
w_tooltip_orp	iw_tooltip


u_ws_orp4     iu_ws_orp4

end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_filenew ()
public subroutine wf_cancel ()
public function integer wf_window_update ()
public function boolean wf_update ()
public subroutine wf_get_selected_plants (datawindow adw_datawindow)
public subroutine wf_default_newrow ()
public subroutine wf_retrieve_cust_plant_defaults (string as_customer)
public subroutine wf_set_selected_plants ()
public function integer wf_validate (ref string as_customer, ref string as_division, ref date ad_delv_date, ref date ad_ship_date, ref character ac_full_load)
public function integer wf_gensales ()
end prototypes

on ue_cancel;call w_base_sheet_ext::ue_cancel;wf_cancel()
end on

event ue_set_header_plant;String		ls_plant 


ls_plant = message.StringParm

dw_header.SetItem(1, 'plant', ls_plant)
end event

event ue_gensales;wf_gensales()
end event

event ue_gotopadisplay;Char						lc_full_load

Date						ldt_del_date, &
							ldt_ship_date

String					ls_customer, &
							ls_division
							
Window 					lw_Open

u_string_functions	lu_string_functions

iw_Frame.SetMicroHelp("Saving")
//The variables coming from the function are not used here.
If This.wf_validate(ls_customer, ls_division, ldt_del_date, ldt_ship_date, lc_full_load) 	< 1 Then 
	Return
END IF

This.TriggerEvent("ue_getdata",0,"detail")
IF lu_string_functions.nf_IsEmpty(Message.StringParm) Then  
	iw_frame.SetMicroHelp("No Lines to generate - ue_gotopadisplay - Line 21") 
	Return 
END IF	
SetPointer(HourGlass!)

This.TriggerEvent("ue_getdefaults")
OpenSheetWithParm(lw_open, This, "w_pa_inquiry", iw_frame, 8, iw_frame.im_menu.iao_arrangeopen)
end event

event ue_getdefaults();DataStore lds_DataStore

Long	ll_RowSelected

String ls_Location,&
		 ls_Division,&
		 ls_Sman_Code
		 

ll_RowSelected = tab_1.tabpage_customer_order_detail.dw_detail.Find("IsSelected()",1, tab_1.tabpage_customer_order_detail.dw_detail.RowCount())
IF ll_RowSelected < 1 then 
	is_additional_data = ''
	Return
END IF

ls_Division  = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_RowSelected,"division_code")
ls_Sman_Code = dw_Header.GetItemString( 1, "sales_person_code")

lds_DataStore = Create DataStore
lds_DataStore.DataObject = 'd_additional_data_rpc'

SELECT salesman.smanloc
	INTO :ls_Location
	FROM salesman
	WHERE salesman.smancode = :ls_Sman_Code AND salesman.smantype = 's'
	USING SQLCA;

IF SQLCA.SQLCode = 100 Then
	iw_Frame.SetMicroHelp("Invalid TSR entered")
	return
END IF
IF SQLCA.SQLCode < 0 Then
	MessageBox("Select Error", sqlca.SQLErrText	)
	Return
END IF

lds_DataStore.Retrieve( tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_RowSelected,"customer_id"),&
								ls_Location	,&
								ls_Division)
IF SQLCA.SQLCode = 100 then
	// This will set default values
	ls_Division = '11'
	lds_DataStore.Retrieve( tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_RowSelected,"customer_id"),&
								ls_Location	,&
								ls_Division)
	IF SQLCA.SQLCode = 100 then
		MessageBox("Service Center Data", "No Service Center Data Found for Customer ID: "+&
						tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_RowSelected,"customer_id")+ " ls_Location: = "+ ls_Location+&
						" Division: "+ ls_Division)
		Destroy lds_DataStore
		is_Additional_data = ''
		Return
	END IF
END IF

is_additional_data = lds_DataStore.Describe("DataWindow.Data")
Destroy( lds_DataStore)
end event

event ue_getx;call super::ue_getx;Message.LongParm = ii_x
end event

event ue_gety;Message.LongParm = ii_y
end event

event ue_ncclicked;if ib_open then close(w_locations_popup)
end event

public function boolean wf_retrieve ();String	ls_return, &
			ls_customerorderid

u_string_functions	lu_string_function

This.TriggerEvent('CloseQuery')
If Message.ReturnValue = 1 Then return False

If Not lu_string_function.nf_IsEmpty(is_initial_reference) Then
	ls_return = is_initial_reference
	is_initial_reference = ''
Else
	ls_customerorderid = dw_header.GetItemString(1, 'customer_order_id')
	If ls_customerorderid = '0000000' Then
		ls_return = ''
	Else
	ls_return = ls_customerorderid + '~t~t~t~t~t' + &
				String(dw_header.GetItemDate(1, 'month'), 'mm/yyyy')
	End if
	OpenWithParm(w_cust_res_open, ls_return)

	ls_return = Message.StringParm
	If Lower(ls_return) = 'cancel' Then return false
End if

This.TriggerEvent("ue_Query", 0, ls_return)

tab_1.tabpage_customer_order_detail.dw_detail.SetFocus()
This.SetRedraw(True)

tab_1.tabpage_edi_comments_detail.mle_1.setfocus()
this.setredraw(true)

is_temp_additional_data = ''

return true
end function

public subroutine wf_filenew ();String				ls_Customer_Type, &
						ls_salesman, &
						ls_location_name
						

This.Title = 'Customer Order'
ls_Customer_Type = "~t"+dw_Header.GetItemString(1,"customer_type")+"~t~t~t~t~t~t~tA"
dw_header.Reset()
dw_Header.ImportString(ls_Customer_Type)

//Default the salesperson
  SELECT salesman.smanname
    INTO :ls_salesman  
    FROM salesman  
  WHERE salesman.smancode = :Message.is_salesperson_code AND salesman.smantype = 's';

  SELECT locations.location_name  
    INTO :ls_location_name  
    FROM locations  
   WHERE locations.location_code = :message.is_smanlocation;

dw_Header.Object.customer_name.Text = ''
dw_Header.Object.customer_city.Text = ''																			

dw_header.SetItem(1, 'sales_person_code', Message.is_salesperson_code)
dw_header.object.sales_person_name.Text = ls_salesman
dw_header.object.sales_location_code.Text = Message.is_smanlocation
dw_header.object.sales_location_name.Text = ls_location_name  

// New orders are NOT EDI
tab_1.tabpage_customer_order_detail.dw_detail.Modify('type_of_order.Width = 0')

tab_1.tabpage_customer_order_detail.dw_detail.Reset()
tab_1.tabpage_customer_order_detail.dw_detail.InsertRow(0)
wf_default_newrow()

dw_header.SetFocus()
dw_Header.Setcolumn("customer_id")

//
is_temp_additional_data = ''

return
end subroutine

public subroutine wf_cancel ();Int		li_ret, &
			li_PageNumber

Double	ld_TaskNumber

String	ls_header, &
			ls_detail, &
			ls_sales,&
			ls_Return

If dw_header.GetItemString(1, 'update_flag') = 'N' Then
	MessageBox("Cancel", "New Orders cannot be canceled")
	return 
End if

If MessageBox("Warning!", "You will not be able to modify this order again.~r~n" + &
				"Are you sure?", Question!, YesNo!, 2) = 2 Then return

istr_error_info.se_event_name = "wf_cancel"

dw_header.SetItem(1, "update_flag", "C")

ls_Header = dw_header.Describe("DataWindow.Data")
ls_Detail = ""
//
//li_ret = iu_orp003.nf_orpo36ar( ls_header, &
//											istr_Error_Info, &
//											ls_detail, &
//											ls_Sales, &
//											ld_TaskNumber, &
//											li_PageNumber)
											
li_ret = iu_ws_orp2.nf_orpo36fr(ls_header, &
								ls_detail, &
								ls_Sales, &
								istr_error_info)

ls_return = dw_header.GetItemString(1, 'customer_order_id') + '~tF~t' + &
				String(dw_header.GetItemDate(1, 'month'), 'mm/yyyy')
	

//wf_retrieve()
This.PostEvent("ue_Query", 0 , ls_Return)
return
end subroutine

public function integer wf_window_update ();Date		ld_date

Decimal	ldc_Ordered_Units

Double	ld_Task_Number

Int		li_page_number, &
			li_ret, &
			li_counter, &
			li_ChangedCounter,&
			li_SelectedCounter,&
			li_UnselectedCounter


Long		ll_RowCount, &
			lla_ChangedRows[], &
			ll_current_row,&
			ll_LoopCount,&
			ll_RowsSelectedforUpdate[],&
			ll_RowsSelectedforGen[]

String	ls_header, &
			ls_detail, &
			ls_so_id, &
			ls_temp, &
			ls_order_type,&
			ls_Line_Status, &
			ls_test
			
u_project_functions	lu_project_functions		
u_string_functions	lu_string_functions

SetPointer(HourGlass!)
If dw_header.AcceptText() = -1 Then return -1
If tab_1.tabpage_customer_order_detail.dw_detail.AcceptText() = -1 Then return -1
	
ll_current_row = tab_1.tabpage_customer_order_detail.dw_detail.GetRow()
ll_rowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
If ll_RowCount = 0 Then
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	return -1
End if

IF lu_string_functions.nf_IsEmpty(dw_header.GetItemString(1,"customer_id")) Then
	SetMicroHelp("Customer ID is a required field")
	return -1
END IF
ld_date = tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate(1, 'delivery_date') 
If IsNull(ld_date) Then
	iw_Frame.SetMicroHelp("Delivery Date is a required field.")
	tab_1.tabpage_customer_order_detail.dw_detail.SetColumn('delivery_date')
	tab_1.tabpage_customer_order_detail.dw_detail.SetRow(1)
	tab_1.tabpage_customer_order_detail.dw_detail.SetFocus()
	return -1
End if

This.SetRedraw(False)
//Delete All Rows With No Product Codes OR zero quantity
For ll_LoopCount = 1 to ll_rowCount
	ls_Line_Status = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(ll_loopCount, 'line_status')
	ldc_Ordered_Units = tab_1.tabpage_customer_order_detail.dw_detail.GetItemNumber( ll_LoopCount, "ordered_units")
	IF IsNull( ldc_ordered_Units) Then 
		ldc_Ordered_Units = 0
	ELSE
		ldc_ordered_Units = tab_1.tabpage_customer_order_detail.dw_detail.GetItemDecimal( ll_LoopCount, "ordered_units")
	END IF
	IF IsNull(ls_Line_Status) Then ls_Line_status = ''
	IF ls_Line_Status <> 'C' AND & 
  	   lu_string_functions.nF_IsEmpty(tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_LoopCount,"sku_product_code"))Then
		  //OR &
		 //ldc_ordered_Units < 1) Then
			
			tab_1.tabpage_customer_order_detail.dw_detail.DeleteRow( ll_LoopCount)
			ll_LoopCount -- 
			ll_RowCount  --
	END IF
END FOR

ls_detail = lu_project_functions.nf_buildUpdateString(tab_1.tabpage_customer_order_detail.dw_detail)

If IsNull(ls_detail) then 	ls_detail = ""

ls_order_type += 'M'

dw_header.SetItem(1, 'type_of_order', ls_order_type)
ls_header = dw_header.Describe("DataWindow.Data")

If lu_string_functions.nf_IsEmpty(ls_detail) AND Not(dw_Header.GetItemString( 1,"update_flag") = 'U') Then
	iw_frame.SetMicroHelp("No Update Necessary")
	This.SetRedraw(True)
	// We have deleted all the rows with no product codes need to add back in the last row
	tab_1.tabpage_customer_order_detail.dw_detail.InsertRow(0)
	wf_default_newrow()
	return 1
End if

ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
For li_Counter = 1 to ll_RowCount
	ls_temp = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(li_counter, 'update_flag')
	If ls_temp = 'A' or ls_temp = 'U' Then
		li_ChangedCounter ++
		lla_ChangedRows[li_ChangedCounter] = li_Counter
		IF Not tab_1.tabpage_customer_order_detail.dw_detail.IsSelected( li_Counter) Then
			li_SelectedCounter++
			ll_RowsSelectedforUpdate[li_SelectedCounter] = li_Counter
		END IF
		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow( li_Counter,True)
	ELSE
		IF tab_1.tabpage_customer_order_detail.dw_detail.IsSelected( li_Counter) Then
			li_UnselectedCounter++
			ll_RowsSelectedforGen[ li_UnselectedCounter] = li_Counter
			tab_1.tabpage_customer_order_detail.dw_detail.SelectRow( li_Counter,FALSE)
		END IF	
	End if
Next

istr_error_info.se_event_name = "wf_update"

//li_ret = iu_orp003.nf_orpo36ar(ls_header, &
//								istr_error_info, &
//				 				ls_detail, &
//								ls_so_id, &
//								ld_task_number, &
//								li_page_number)
								
							
li_ret = iu_ws_orp2.nf_orpo36fr(ls_header, &
								ls_detail, &
								ls_so_id, &
								istr_error_info)
//
If Not lu_string_functions.nf_IsEmpty(ls_header) Then
	dw_header.Reset()
	dw_header.ImportString(ls_header)
End if

//This is done because we delete all rows with no product codes
//tab_1.tabpage_customer_order_detail.dw_detail.DeleteRow(ll_RowCount)
IF li_ChangedCounter > 0 Then &
				lu_project_functions.nf_Replace_Rows( tab_1.tabpage_customer_order_detail.dw_detail, ls_detail)

tab_1.tabpage_customer_order_detail.dw_detail.InsertRow(0)

ls_test = tab_1.tabpage_customer_order_detail.dw_detail.Describe("DataWindow.Data")

wf_default_newrow()

ll_RowCount  = UpperBound( ll_RowsSelectedForUpdate)

For li_Counter =  1 to ll_RowCount 
	tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(ll_RowsSelectedforUpdate[li_Counter],False)
Next

ll_RowCount  = UpperBound( ll_RowsSelectedforGen)

For li_Counter =  1 to ll_RowCount
	tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(ll_RowsSelectedforGen[li_Counter],True)
Next

// If the rpc had an error, return now that the fields in error are highlighted
If li_ret <> 0 Then 
	This.SetRedraw(True)
	return -1
End If

dw_header.ResetUpdate()
tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()

////
//ll_RowCount  = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
//
//For li_Counter =  1 to ll_RowCount
//	If (Mid(tab_1.tabpage_customer_order_detail.dw_detail.Getitemstring(li_Counter, "detail_errors") , 18, 1) = 'V') or (tab_1.tabpage_customer_order_detail.dw_detail.Getitemstring(li_Counter, 'price_override') = 'Y') then
//		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(li_Counter, True)
//	else
//		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(li_Counter, False)
//	end if
//Next

tab_1.tabpage_customer_order_detail.dw_detail.ScrollToRow(ll_current_row)

This.SetRedraw(True)

return 0
end function

public function boolean wf_update ();IF wf_Window_Update() < 0 Then
	Return False
Else
	Return True
END IF
end function

public subroutine wf_get_selected_plants (datawindow adw_datawindow);long							ll_count
string						ls_plant
integer						li_windowx, li_windowy, li_height, li_height_H
Application					la_Application

la_Application = GetApplication()
is_selected_plant_array = ""
for ll_count = 1 to il_selected_plant_max
	ls_plant = adw_datawindow.getitemstring(il_selected_row, "plant" + string(ll_count)) + "~r~n"
	if isnull(ls_plant) then
		is_selected_plant_array += " " + "~r~n"
	else
		is_selected_plant_array += ls_plant + "~r~n"
	end if
next

Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose


// find opening x/y positions for "Multi select drop down"

if ib_Header then
	if (iw_frame.width - (this.x + integer(dw_header.X) + integer(dw_header.Object.plant1.X))) > 1179 then
		ii_dddw_x = integer(dw_header.Object.plant1.X) + 24 + this.x		
	else
		ii_dddw_x = iw_frame.width - 1179 - 30
		if ii_dddw_x < 0 then ii_dddw_x = integer(dw_header.Object.plant1.X) + 24 + this.x
	end if
	if (iw_frame.height - (this.y + integer(dw_header.Object.plant1.y) +  &
			integer(dw_header.Object.plant1.height) + integer(dw_header.y))+ li_height_H > (676 + 450)) then	
		ii_dddw_y = integer(dw_header.Object.plant1.y) + integer(dw_header.Object.plant1.height) + 208 + this.y + li_height_H
	else
		ii_dddw_y = integer(dw_header.Object.plant1.y) + 208 + this.y - 676 + li_height_H
		if ii_dddw_y < 0 then ii_dddw_y = integer(dw_header.Object.plant1.y) + integer(dw_header.Object.plant1.height) + &
							208 + this.y + li_height_H
	end if
else
	if (iw_frame.width - (this.x + integer(tab_1.tabpage_customer_order_detail.dw_detail.X) + integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.X) - il_horz_scroll_pos)) > 1179 then
		ii_dddw_x = integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.X) + 32 + this.x - il_horz_scroll_pos
	else
		ii_dddw_x = iw_frame.width - 1179 - 32
		if ii_dddw_x < 0 then ii_dddw_x = integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.X) + 32 + this.x - il_horz_scroll_pos
	end if
	if (iw_frame.height - (this.y + integer(tab_1.tabpage_customer_order_detail.dw_detail.y) + ((integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.y) + &
					integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.DataWindow.FirstRowOnPage) - 1)) + &
					322 + li_height)) > (676 + 200)) then
				
		ii_dddw_y = this.y + integer(tab_1.tabpage_customer_order_detail.dw_detail.y) + ((integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.y) + &
					integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.DataWindow.FirstRowOnPage) - 1))) + 322 + li_height
	else
		ii_dddw_y = (integer(dw_header.Object.plant1.y) + 348 + this.y + (((integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.y) + &
					integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.height) + 8)) * &
					(il_selected_row - (integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.DataWindow.FirstRowOnPage) - 1))))  - 676 + li_height
		if ii_dddw_y < 0 then ii_dddw_y = this.y + integer(tab_1.tabpage_customer_order_detail.dw_detail.y) + ((integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.y) + &
					integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(tab_1.tabpage_customer_order_detail.dw_detail.Object.DataWindow.FirstRowOnPage) - 1))) + 322 + li_height
	end if
end if

////test
//messagebox("pointery", string(li_windowy))
end subroutine

public subroutine wf_default_newrow ();long	ll_count, ll_rowcount
string ls_detailerrors

ll_rowcount = tab_1.tabpage_customer_order_detail.dw_detail.rowcount()
//messagebox("wf_default1",mid (tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(ll_rowcount,"detail_errors"),9,1))
if ISNull(tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(ll_rowcount,"detail_errors")) Then 
	tab_1.tabpage_customer_order_detail.dw_detail.SetItem(ll_rowcount,"detail_errors","        V         ")
else
	ls_detailerrors = left(tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(ll_rowcount,"detail_errors"), 8) + &
							"V" + mid(tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(ll_rowcount,"detail_errors"), 10)
	tab_1.tabpage_customer_order_detail.dw_detail.SetItem(ll_rowcount,"detail_errors",ls_detailerrors)	
end if
//messagebox("wf_default2",mid (tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(ll_rowcount,"detail_errors"),9,1))
this.setredraw(true)
for ll_count = 1 to il_selected_plant_max
	tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_rowcount, "plant" + string(ll_count), dw_header.getitemstring(1, "plant" + string(ll_count)))
	tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_rowcount, "inc_exl_ind", dw_header.getitemstring(1, "inc_exl_ind"))
next
tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_rowcount,'update_flag','A')

end subroutine

public subroutine wf_retrieve_cust_plant_defaults (string as_customer);string			ls_input, ls_output
DataStore			lds_selected_plants
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt, ll_rowcnt_detail
integer			li_ret



istr_error_info.se_event_name = 'wf_retrieve_cust_plant_defaults'
If Not IsValid(iu_orp204) Then
	iu_orp204 = Create u_orp204
End if

ls_input = as_customer + '~r~n'						
								
li_ret = iu_ws_orp4.nf_ORPO99FR(istr_error_info, &
								ls_input, &
								ls_output)					
								
////test
dw_header.setitem(1,"inc_exl_ind",left(ls_output, 1))
ls_output = mid(ls_output,2)
lds_selected_plants = Create DataStore
lds_selected_plants.DataObject = 'd_plant_code'
ll_rowcount = lds_selected_plants.importstring(ls_output)
for ll_count = 1 to il_selected_plant_max
	if ll_count <= ll_rowcount then
		dw_header.setitem(1, "plant" + string(ll_count) , &
											lds_selected_plants.getitemstring(ll_count, "plant"))
	else
		dw_header.setitem(1, "plant" + string(ll_count) , " ")
	end if
next
ll_rowcnt_detail = tab_1.tabpage_customer_order_detail.dw_detail.rowcount()
if ll_rowcnt_detail > 0 then
	for ll_cnt = 1 to ll_rowcnt_detail
		tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_cnt,"inc_exl_ind",dw_header.getitemstring(1,"inc_exl_ind"))
		for ll_count = 1 to il_selected_plant_max
			if ll_count <= ll_rowcount then
				tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_cnt, "plant" + string(ll_count), &
												lds_selected_plants.getitemstring(ll_count, "plant"))
			else
				tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_cnt, "plant" + string(ll_count), " ")
			end if
		next
	next
end if

iw_frame.SetMicroHelp("Ready")

ib_modified = false
end subroutine

public subroutine wf_set_selected_plants ();DataStore			lds_selected_plants
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt, ll_count2, ll_linenumber

lds_selected_plants = Create DataStore
lds_selected_plants.DataObject = 'd_plant_code'
ll_rowcount = lds_selected_plants.importstring(is_selected_plant_array)
if ib_header then
	for ll_count = 1 to il_selected_plant_max
		if ll_count <= ll_rowcount then
			dw_header.setitem(il_selected_row, "plant" + string(ll_count) , &
												lds_selected_plants.getitemstring(ll_count, "plant"))
		else
			dw_header.setitem(il_selected_row, "plant" + string(ll_count) , " ")
		end if
	next
	ll_rowcount = tab_1.tabpage_customer_order_detail.dw_detail.rowcount()
	if ll_rowcount > 0 then
		for ll_count2 = 1 to ll_rowcount
			ll_linenumber = tab_1.tabpage_customer_order_detail.dw_detail.getitemnumber (ll_count2, "line_number")
			if ll_linenumber = 0 or isnull(ll_linenumber) then
				tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_count2, "inc_exl_ind", dw_header.getitemstring(1, "inc_exl_ind"))
				for ll_count = 1 to il_selected_plant_max
					tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_count2, "plant" + string(ll_count), dw_header.getitemstring(1, "plant" + string(ll_count)))
				next
			end if
		next 
	end if
//	dw_header.triggerevent("itemchanged")
	tab_1.tabpage_customer_order_detail.dw_detail.setcolumn("ordered_units")
	tab_1.tabpage_customer_order_detail.dw_detail.setrow(1)
	tab_1.tabpage_customer_order_detail.dw_detail.SetFocus()
else
	for ll_count = 1 to il_selected_plant_max
		if ll_count <= ll_rowcount then
			tab_1.tabpage_customer_order_detail.dw_detail.setitem(il_selected_row, "plant" + string(ll_count), &
											lds_selected_plants.getitemstring(ll_count, "plant"))
		else
			tab_1.tabpage_customer_order_detail.dw_detail.setitem(il_selected_row, "plant" + string(ll_count), " ")
		end if
	next
	if ib_modified and not (tab_1.tabpage_customer_order_detail.dw_detail.getitemstring(il_selected_row, 'update_flag') = 'A') then
		If tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(il_selected_row, 'update_flag') = ' ' Then 
			tab_1.tabpage_customer_order_detail.dw_detail.SetItem(il_selected_row, 'update_flag', 'U')
		end if
	end if
	tab_1.tabpage_customer_order_detail.dw_detail.SetFocus()
end if

ib_modified = false
ib_open = false
end subroutine

public function integer wf_validate (ref string as_customer, ref string as_division, ref date ad_delv_date, ref date ad_ship_date, ref character ac_full_load);Long						ll_RowCount,&
							ll_Loop,&
							ll_FoundRow, &
							ll_division_row
				
String					ls_find_string, &
							ls_temp, &
							ls_division_priority, &
							ls_division, &
							ls_type_code

Decimal					ldc_ordered_units

u_string_functions	lu_string_functions

If tab_1.tabpage_customer_order_detail.dw_detail.AcceptText() < 1 Then Return -1

If This.wf_Window_Update() < 0 Then Return -1

ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
ll_FoundRow = tab_1.tabpage_customer_order_detail.dw_detail.Find("IsSelected()", 1, ll_RowCount)

IF ll_FoundRow < 1 Then 
		This.TriggerEvent("ue_getdata",0,"detail")
		IF lu_string_functions.nf_IsEmpty(Message.StringParm) Then Return -1
		// Get the first selected rows delv date and customer
		ll_FoundRow = tab_1.tabpage_customer_order_detail.dw_detail.Find("IsSelected()", 1, ll_RowCount)
END IF

ad_delv_Date = tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate( ll_FoundRow,"delivery_date")
ad_ship_Date = tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate( ll_FoundRow,"ship_date")
as_customer = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_FoundRow, "customer_id")

//check division on all rows
ls_division_priority = ' '
ll_division_row = tab_1.tabpage_customer_order_detail.dw_detail.Getselectedrow(ll_division_row)

Do While ll_division_row > 0
	ls_temp = tab_1.tabpage_customer_order_detail.dw_detail.Getitemstring(ll_division_row, 'division_code') 
	SELECT tutltypes.type_code  
    INTO :ls_type_code
    FROM tutltypes  
   WHERE ( tutltypes.type_short_desc = :ls_temp ) AND  
         ( tutltypes.record_type = 'MULTIDIV' );
	
	if ls_type_code < ' ' then
		if ls_division_priority = ' ' then
			if ls_temp = '99' then
				//do nothing
			else
				ls_division = ls_temp 
			end if
		end if
   else
		if ls_division_priority = ' ' then
			ls_division_priority = ls_type_code
			ls_division = ls_temp 
		else
			if ls_type_code < ls_division_priority then
				ls_division_priority = ls_type_code
				ls_division = ls_temp
			end if
		end if 
	end if 
	
	ll_division_row = tab_1.tabpage_customer_order_detail.dw_detail.Getselectedrow(ll_division_row)
LOOP

if ls_division > ' ' Then
   as_division = ls_division
else
  This.TriggerEvent("ue_getdata",0,"division")
 	as_Division = Message.StringParm
end if

IF lu_string_functions.nf_IsEmpty( as_customer) Then
	IF lu_string_functions.nf_IsEmpty( dw_header.GetItemString(1,"customer_id")) Then
		iw_frame.SetMicroHelp("You must have a customer to generate an order.")
		Return -1
	ELSE
		IF dw_Header.GetItemString( 1, "customer_type") = 'B' OR &
			dw_Header.GetItemString( 1, "customer_type") = 'C' Then
			iw_frame.SetMicroHelp("No customer selected, No defaults will be specified")
			as_customer = ''
		ELSE
			as_customer =  dw_header.GetItemString(1,"customer_id")
			for ll_loop = 1 to ll_RowCount
				IF tab_1.tabpage_customer_order_detail.dw_detail.IsSelected(ll_Loop) Then &
					tab_1.tabpage_customer_order_detail.dw_detail.SetItem( ll_Loop, "customer_id", as_customer)
			Next
		END IF
	END IF		
End if

////See if there any other rows selected with a different delivery date
ls_find_string = "IsSelected() and Not(delivery_date = Date('"+ string(ad_delv_Date)+"'))"

IF tab_1.tabpage_customer_order_detail.dw_detail.Find( ls_find_String,ll_FoundRow+1, ll_RowCount) > 0 Then 
		iw_frame.SetMicroHelp("All lines must have the same delivery date to generate")
		Return -1
END IF		

////See if there any other rows selected with a different ship date
ls_find_string = "IsSelected() and Not(ship_date = Date('"+ string(ad_ship_Date)+"'))"

if not isnull(ls_find_string) then
	IF tab_1.tabpage_customer_order_detail.dw_detail.Find( ls_find_String,ll_FoundRow+1, ll_RowCount) > 0 Then 
			iw_frame.SetMicroHelp("All lines must have the same ship date to generate")
			Return -1
	END IF	
end if

//See if there any other rows selected with a different customer
ls_find_string = "IsSelected() and customer_id <>'"+ as_customer+"'"
IF tab_1.tabpage_customer_order_detail.dw_detail.Find( ls_find_string, ll_FoundRow+1, ll_RowCount) > 0 Then 
		iw_frame.SetMicroHelp("All lines must have the same customer to generate")
		Return -1
END IF		

//See if there any other rows selected with a full load.

ac_full_load = 'U'
ls_find_string = "IsSelected()"
If tab_1.tabpage_customer_order_detail.dw_detail.Find( ls_find_string, ll_FoundRow + 1, ll_RowCount + 1) > 0 Then
	ls_find_string = "IsSelected() and ordered_units_uom = '03'"
	IF tab_1.tabpage_customer_order_detail.dw_detail.Find( ls_find_string, ll_FoundRow, ll_RowCount) > 0 Then 
			iw_frame.SetMicroHelp("You can only generate one full load.")
			Return -1
	END IF		
Else
	IF tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_FoundRow, "ordered_units_uom") = '03' Then
		IF IsNull(tab_1.tabpage_customer_order_detail.dw_detail.GetItemNumber(	ll_FoundRow, "ordered_units")) THEN
			ldc_ordered_units	=	0.00
		ELSE
			ldc_ordered_units	=	tab_1.tabpage_customer_order_detail.dw_detail.GetItemDecimal(	ll_FoundRow, "ordered_units")
		END IF
		IF  ldc_ordered_units = 1.00 Then
			ac_full_load = 'F'			
		Else
			iw_frame.SetMicroHelp('You can only generate One full load.')
			Return - 1
		End If
	END IF
End If

Return 1
end function

public function integer wf_gensales ();Char						lc_full_Load

DataStore 				lds_Tmp, lds_tmp2

Date						ld_delv_Date, &
							ld_ship_date

Integer					li_rtn,&
							li_Import, &
							li_Counter
		
Long						ll_pos_x,&
							ll_FoundRow, &
							ll_rowcount, &
							ll_temprow
					
String					ls_OpenParm,&
							ls_Customer,&
							ls_messagestringparm,&
							ls_button_classname,&
							ls_Additional_Data,&
							ls_Header,&
							ls_detail,&
							ls_So_ID,&
							ls_Division,&
							ls_ResReductions,&
							ls_ParmString,&
							ls_Temp,&
							ls_TranMode, &
							ls_instruction_ind, &
							ls_load_instructions
					
s_error					lstr_s_error 		

u_project_functions	lu_project_functions	

tab_1.tabpage_customer_order_detail.dw_detail.accepttext()

ls_OpenParm = ''

tab_1.tabpage_customer_order_detail.dw_detail.setRedraw(False)
If wf_validate(ls_customer, ls_division, ld_delv_Date, ld_ship_date, lc_full_load) &
		< 1 Then 
		tab_1.tabpage_customer_order_detail.dw_detail.setRedraw(True)
		Return -1
End If
ll_rowcount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
ll_FoundRow = tab_1.tabpage_customer_order_detail.dw_detail.Find("IsSelected()", 1, tab_1.tabpage_customer_order_detail.dw_detail.RowCount())

ls_TranMode = dw_Header.GetItemString( 1, "trans_mode")

if isnull(ld_ship_date) then
	is_additional_data_input = "C" + "~t" + String(ld_delv_Date) + '~t~t~t' + &
								ls_customer + '~t' + &
								lc_Full_Load+ '~t' + &
								ls_TranMode+'~t'+ &
								ls_Division+'~t'+ &
								tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_FoundRow, "div_po_num") + '~t' + &
								dw_header.GetItemString( 1, 'sales_person_code') + '~t' + &
								tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_FoundRow, "corp_po_num")
else
	
	is_additional_data_input = "C" + "~t" + String(ld_delv_Date) + "~t" + string(ld_ship_date) + '~t~t' + &
								ls_customer + '~t' + &
								lc_Full_Load+ '~t' + &
								ls_TranMode+'~t'+ &
								ls_Division+'~t'+ &
								tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_FoundRow, "div_po_num") + '~t' + &
								dw_header.GetItemString( 1, 'sales_person_code') + '~t' + &
								tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_FoundRow, "corp_po_num")
end if

if is_temp_additional_data <> '' then
	is_additional_data_input += '~t' + is_temp_additional_data
end if
							
OpenWithParm( w_additional_data, this)
IF tab_1.tabpage_customer_order_detail.dw_detail.Find("isSelected()",1, ll_RowCount) < 1 Then 
	MessageBox("Customer_order","No Lines Selected")
END IF					
ls_MessageStringParm = is_additional_data_input
// Message.StringParm is populated with the data+ "~x"+Classname 
// of the button pressed
ll_Pos_x = POS( ls_MessageStringParm, "~x")
ls_button_ClassName = Mid( ls_MessageStringParm, ll_Pos_x + 1)
ls_Additional_Data = Left(ls_MessageStringParm,ll_Pos_x - 1)
ls_instruction_ind = Mid(ls_messageStringParm,ll_Pos_x - 1,1)
is_additional_data = ls_Additional_Data
Choose Case ls_button_ClassName 
	Case"Cancel" 
			tab_1.tabpage_customer_order_detail.dw_detail.SetRedraw(True)
			return -1
	Case "cb_browse"
		lds_Tmp = Create DataStore		
		lds_Tmp.DataObject = 'd_additional_data_rpc'
		ll_temprow = lds_tmp.ImportString( ls_Additional_Data)
		ls_ParmString = "~t~t"
		ls_Temp = String(lds_Tmp.GetItemDate(1, "schd_ship_date"),"mm/dd/yyyy")
		ls_ParmString += ls_Temp+"~t"
//		ls_ParmString += String( ld_delv_Date,"mm/dd/yyyy")+"~t"
		ls_ParmString += String(lds_Tmp.GetItemDate(1, "schd_delv_date"),"mm/dd/yyyy") + "~t"
		ls_Temp = lds_Tmp.GetItemString( 1, "sman_code")
		IF ISNULL(ls_Temp) Then
			ls_ParmString += dw_Header.GetItemString( 1, "sales_person_code")+"~x"
		ELSE
			ls_ParmString += ls_Temp+"~x"
		END IF
		
		ls_ParmString += "~t"
		ls_Temp = lds_Tmp.GetItemString( 1, "customer_id")
		IF ISNULL( ls_Temp) Then
			ls_ParmString += dw_Header.GetItemString( 1, "customer_id")+"~x"
		ELSE
			ls_ParmString += ls_Temp+"~x"			
		END IF	
		
		lds_Tmp2 = Create DataStore	
		lds_Tmp2.DataObject = 'd_customer_order_b'
		lds_tmp2.Object.Data = tab_1.tabpage_customer_order_detail.dw_detail.Object.Data.Selected
		
		lds_Tmp.DataObject = 'd_customer_order_rpc'
		lds_tmp.Object.Data = lds_tmp2.Object.Data [ 1, 1, lds_tmp2.rowcount(), 20 ] 
		ls_ParmString += lds_tmp.Object.DataWindow.Data
		OpenWithParm(w_res_browse-product, ls_ParmString)
		ls_ResReductions = Message.StringParm
		IF ls_ResReductions = 'Cancel' Then
			Destroy lds_tmp
			tab_1.tabpage_customer_order_detail.dw_detail.SetRedraw(True)
			Return -1
		END IF
	CASE ELSE
		lds_Tmp = Create DataStore
		// Fall through Generate was pressed
		is_temp_additional_data = is_additional_data
END CHOOSE

//lds_Tmp.DataObject = 'd_reservation_header_rpc'
lds_Tmp.DataObject = 'd_customer_order_header'
lds_tmp.Object.Data = dw_Header.Object.Data
ls_Header = lds_tmp.Object.DataWindow.Data


//lds_Tmp.DataObject = 'd_customer_order_rpc'
lds_Tmp.DataObject = 'd_customer_order_b'
lds_tmp.Object.Data = tab_1.tabpage_customer_order_detail.dw_detail.Object.Data.Selected
ls_Detail = lds_tmp.Object.DataWindow.Data

lstr_s_error.se_function_name = "wf_GenSales"
lstr_s_error.se_event_name = "Customer Gen"
lstr_s_error.se_procedure_name = "orpo39ar"

ls_additional_data += '~t' + is_load_instruction + '~t' + is_dept_code + '~t' + is_dest_country  + '~t' &
								+ is_store_door + '~t' + is_deck_code + '~t'

//li_rtn = iu_orp003.nf_orpo39ar( lstr_s_error, 'C',ls_Header, ls_Detail, ls_Additional_Data,ls_ResReductions)

li_rtn = iu_ws_orp2.nf_orpo39fr('C',ls_Header, ls_Detail, ls_Additional_Data,ls_ResReductions,lstr_s_error)

//
tab_1.tabpage_customer_order_detail.dw_detail.SetRedraw(False)
//
Choose Case li_rtn
Case 0
	SetPointer(HourGlass!)
	// Replace the rows
	lds_tmp.Reset()
	li_Import = lds_tmp.ImportString( ls_Detail)
	IF li_Import > 0 Then lu_project_functions.nf_replace_rows(tab_1.tabpage_customer_order_detail.dw_detail, ls_detail)
	tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()
	//
	tab_1.tabpage_customer_order_detail.dw_detail.setRedraw(True)
	//
	is_temp_additional_data = ''
	tab_1.tabpage_customer_order_detail.dw_detail.Modify("sales_price.Background.Color = '16777215'")
Case 1
	SetPointer(HourGlass!)
	// Replace the rows
	lds_tmp.Reset()
	li_Import = lds_tmp.ImportString( ls_Detail)
//	If Li_Import > 0 Then tab_1.tabpage_customer_order_detail.dw_detail.Object.Data.Selected = lds_tmp.Object.Data 
	IF li_Import > 0 Then lu_project_functions.nf_replace_rows(tab_1.tabpage_customer_order_detail.dw_detail, ls_detail)
	//
	tab_1.tabpage_customer_order_detail.dw_detail.setRedraw(True)
	//
	is_temp_additional_data = is_additional_data
Case 2, 5, 6
	Window		lw_detail
	SetPointer(HourGlass!)
	// Replace the rows
	lds_tmp.Reset()
	li_Import = lds_tmp.ImportString( ls_Detail)
//	IF li_Import > 0 Then	tab_1.tabpage_customer_order_detail.dw_detail.Object.Data.Selected = lds_tmp.Object.Data 
	IF li_Import > 0 Then lu_project_functions.nf_replace_rows(tab_1.tabpage_customer_order_detail.dw_detail, ls_detail)
	tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()
	ls_so_id  = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( tab_1.tabpage_customer_order_detail.dw_detail.Find("isSelected()",1, ll_RowCount),& 
					"sales_order_id")
					
	if (ls_instruction_ind = 'Y') and (li_rtn = 6) then
		ls_so_id = ls_so_id + '~t' + 'I'
	end if
	//
	tab_1.tabpage_customer_order_detail.dw_detail.setRedraw(True)
	//
	OpenSheetWithParm(lw_detail, ls_so_id, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)

	tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()
	dw_Header.ResetUpdate()
	tab_1.tabpage_customer_order_detail.dw_detail.Modify("sales_price.Background.Color = '16777215'")
	is_temp_additional_data = ''
	
CASE 3
	This.TriggerEvent("ue_gotopadisplay")
	dw_Header.SetFocus()
	Destroy lds_tmp
	tab_1.tabpage_customer_order_detail.dw_detail.SetRedraw(True)
	return 0
Case Else
		tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()
		tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()
		//
		tab_1.tabpage_customer_order_detail.dw_detail.setRedraw(True)
		//
End Choose

dw_Header.SetFocus()
Destroy lds_tmp
tab_1.tabpage_customer_order_detail.dw_detail.SetRedraw(True)
tab_1.tabpage_customer_order_detail.dw_detail.Selectrow(0,False)
//
ll_RowCount  = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
//
//For li_Counter = 1 to ll_RowCount
//	If Mid(tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(li_Counter, "detail_errors"), 18, 1) = 'E' then
//		dw_header.modify("price_override.Visible= '1'")
//		dw_header.modify("reference_num_t.Visible='1'")
//	End if
//Next
		
//
//For li_Counter =  1 to ll_RowCount
//	If Mid(tab_1.tabpage_customer_order_detail.dw_detail.Getitemstring(li_Counter, "detail_errors") , 18, 1) = 'E' then
//		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(li_Counter, True)
//	else
//		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(li_Counter, False)
//	end if
//Next
//
Return 0
end function

event ue_postopen;call super::ue_postopen;

DataWindowChild	ldwc_temp

u_string_functions	lu_string_function
u_project_functions	lu_project_functions

istr_error_info.se_app_name = Message.nf_Get_App_ID()
istr_error_info.se_user_id = SQLCA.userid
istr_error_info.se_window_name = "Cust Ord"

iu_orp003 = Create u_orp003
iu_orp002 = Create u_orp002
iu_ws_orp2     = Create u_ws_orp2
iu_ws_orp3     = Create u_ws_orp3
iu_ws_orp4 = Create u_ws_orp4


If Message.ReturnValue = -1 Then Close(This)

// Set Up the window for new order
dw_header.SetItem(1, 'update_flag', 'A')
tab_1.tabpage_customer_order_detail.dw_detail.InsertRow(0)
wf_default_newrow()

tab_1.tabpage_customer_order_detail.dw_detail.GetChild('ordered_units_uom', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'OUOM')

dw_header.SetFocus()
dw_Header.Setcolumn("customer_id")

tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate ( )

If Not lu_string_function.nf_IsEmpty(is_initial_reference) Then
	iw_frame.im_menu.m_file.m_inquire.PostEvent(Clicked!)
End If
end event

event ue_query;call super::ue_query;Int		li_ret,&
			li_Page_Number, &
			li_Counter

Double	ld_TaskNumber

String	ls_return,&
			ls_Header,&
			ls_Input, &
			ls_Detail,&
			ls_Output, &
			ls_So_id, &
			ls_smancode, &
			ls_smanlocation, &
			ls_smanname, &
			ls_location_name,&
			ls_CustomerName,&
			ls_CustomerCity, ls_business_rules
			
Long	ll_RowCount
			
DataWindowChild	ldwc_Sales_Order

u_project_functions	lu_project_functions

u_string_functions	lu_string_functions

ls_return = String(Message.LongParm, "address")

ls_return = Left(ls_return, len(ls_return) - 1)

If Right(ls_return, 1) = 'E' Then
	tab_1.tabpage_customer_order_detail.dw_detail.Modify('type_of_order.Width = 87')
	dw_header.SetItem(1, 'type_of_order', 'E')
End if
    
dw_header.SetItem(1, 'customer_order_id', Left(ls_return, Pos(ls_return, '~t') - 1))
dw_header.SetItem(1, 'update_flag', 'I')
ls_header = dw_header.Describe("DataWindow.Data")

tab_1.tabpage_customer_order_detail.dw_detail.GetChild('sales_order_id', ldwc_sales_order)

dw_header.SetRedraw(False)

tab_1.tabpage_customer_order_detail.dw_detail.Reset()
ldwc_sales_order.Reset()

This.SetRedraw(False)

istr_error_info.se_event_name = 'wf_retrieve'
If Not IsValid(iu_ws_orp2) Then
	iu_ws_orp2 = Create u_ws_orp2
End if

Do
//li_ret = iu_orp003.nf_orpo36ar(ls_header, &
//								istr_error_info, &
//								ls_detail, &
//								ls_so_id, &
//								ld_tasknumber, &
//								li_page_number)
								
li_ret = iu_ws_orp2.nf_orpo36fr(ls_header, &
								ls_detail, &
								ls_so_id, &
								istr_error_info)

If li_ret < 0 Then 
	This.SetRedraw(True)
	return 
End if

If Not lu_string_functions.nf_IsEmpty(ls_header) Then
	dw_header.Reset()
	dw_header.ImportString(ls_header)
	This.Title = 'Customer Order-' + dw_header.GetItemString( 1, 'customer_order_id')
End if
wf_retrieve_cust_plant_defaults(dw_header.getitemstring(1,"customer_id"))
tab_1.tabpage_customer_order_detail.dw_detail.ImportString(ls_detail)
ldwc_sales_order.ImportString(ls_so_id)

Loop While ld_tasknumber <> 0 And li_Page_Number <> 0

ls_smancode = dw_header.GetItemString(1, 'Sales_person_code')
If Not lu_string_functions.nf_IsEmpty(ls_smancode) Then

  SELECT salesman.smanname,   
         salesman.smanloc  
    INTO :ls_smanname,   
         :ls_smanlocation  
    FROM salesman  
   WHERE salesman.smancode = :ls_smancode AND salesman.smantype = 's';

  SELECT locations.location_name  
    INTO :ls_location_name  
    FROM locations  
   WHERE locations.location_code = :message.is_smanlocation;

	dw_header.object.sales_person_name.Text = ls_smanname
	dw_header.object.sales_location_code.Text = ls_smanlocation
	dw_header.object.sales_location_name.Text = ls_location_name  
	
	IF lu_project_functions.nf_ValidateCustomer( dw_Header.GetItemString( 1, "customer_ID"),&
																		   dw_header.GetItemString(1,"customer_type"),&
																			ls_CustomerName,ls_CustomerCity) < 1 Then
		dw_Header.Object.customer_name.Text = ''
		dw_Header.Object.customer_city.Text = ''
//  if customer is not valid, leave type and id unprotected    		
		dw_header.SetItem( 1, 'fields_in_error', Replace(dw_header.GetItemString( 1, 'fields_in_error'), 2, 2, 'UU'))
		dw_header.SetRedraw( True)
	ELSE
		dw_Header.Object.customer_name.Text = ls_CustomerName
		dw_Header.Object.customer_city.Text = ls_CustomerCity
	END IF
End IF

if dw_header.GetItemString(1,"type_of_order") = 'E' then
	tab_1.tabpage_customer_order_detail.dw_detail.object.ship_date.Protect = 0
	tab_1.tabpage_customer_order_detail.dw_detail.object.ship_date.BackGround.Color = 16777215
else
	tab_1.tabpage_customer_order_detail.dw_detail.object.ship_date.Protect = 1
	tab_1.tabpage_customer_order_detail.dw_detail.object.ship_date.BackGround.Color = 12632256
end if

If dw_header.RowCount() < 1 Then Dw_header.InsertRow(0)
dw_header.SetRedraw( True)
tab_1.tabpage_customer_order_detail.dw_detail.SetRedraw(True)
tab_1.tabpage_customer_order_detail.dw_detail.InsertRow(0)
wf_default_newrow()

tab_1.tabpage_edi_comments_detail.mle_1.clear()

ls_Input = dw_header.Getitemstring( 1, "customer_order_id") + '~t' + 'E' + '~r~n'

//li_ret = iu_orp003.nf_orpo30br(istr_error_info, ls_Input, ls_Output)

li_ret = iu_ws_orp2.nf_orpo30fr(ls_input,ls_output,istr_error_info)

if li_ret = 0 then
	tab_1.tabpage_edi_comments_detail.enabled = False
	tab_1.selectedtab = 1
Else
	tab_1.tabpage_edi_comments_detail.enabled = True
	tab_1.selectedtab = 2
	tab_1.tabpage_edi_comments_detail.mle_1.text = ls_output
end if
//
ll_RowCount  = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()



For li_Counter =  1 to ll_RowCount
	ls_business_rules = tab_1.tabpage_customer_order_detail.dw_detail.Getitemstring(li_Counter, "detail_errors")
next
//	If Mid(tab_1.tabpage_customer_order_detail.dw_detail.Getitemstring(li_Counter, "detail_errors") , 18, 1) = 'E' then
//		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(li_Counter, True)
//	else
//		tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(li_Counter, False)
//	end if
//Next

//
This.SetRedraw(True)
dw_header.ResetUpdate()
tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()

end event

event close;call super::close;If IsValid(iu_orp003) Then Destroy iu_orp003
If IsValid(iu_orp002) Then Destroy iu_orp002
If IsValid(iw_toolTip) Then Close(iw_ToolTip)
If IsValid(iu_ws_orp2) Then Destroy (iu_ws_orp2)
If IsValid(iu_ws_orp3) Then Destroy (iu_ws_orp3)

DESTROY iu_ws_orp4
end event

event open;call super::open;String	ls_ret

is_initial_reference = Message.StringParm

ls_ret = dw_header.Modify('month.visible = 0 ' + &
										'month_t.visible = 0')


iw_this = this
end event

event activate;call super::activate;iw_frame.im_menu.mf_enable('m_cancelorder')
iw_frame.im_menu.mf_enable('m_generatesalesorder')
iw_frame.im_menu.mf_enable('m_padisplay')
iw_frame.im_menu.mf_enable('m_print')




end event

event deactivate;iw_frame.im_menu.mf_disable('m_cancelorder')
iw_frame.im_menu.mf_disable('m_generatesalesorder')
iw_frame.im_menu.mf_disable('m_padisplay')
iw_frame.im_menu.mf_disable('m_print')
end event

event resize;call super::resize;//tab_1.tabpage_customer_order_detail.dw_detail.Width = This.Width - tab_1.tabpage_customer_order_detail.dw_detail.X - 90
//tab_1.tabpage_customer_order_detail.dw_detail.Height = This.Height - tab_1.tabpage_customer_order_detail.dw_detail.Y - 600

long       ll_x = 50, &
           ll_y = 110

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
end if


//tab_1.Resize(This.width - tab_1.X - 50, This.height - tab_1.Y - 110)
tab_1.Resize(This.width - tab_1.X - ll_x, This.height - tab_1.Y - ll_y)

tab_1.tabpage_customer_order_detail.Resize(tab_1.width - tab_1.tabpage_customer_order_detail.X - 10, &
		tab_1.height - tab_1.tabpage_customer_order_detail.Y - 25)


tab_1.tabpage_customer_order_detail.dw_detail.Resize(tab_1.tabpage_customer_order_detail.width - tab_1.tabpage_customer_order_detail.dw_detail.X - 15, &
		tab_1.tabpage_customer_order_detail.height - tab_1.tabpage_customer_order_detail.dw_detail.Y)

tab_1.tabpage_edi_comments_detail.Resize(tab_1.width - tab_1.tabpage_edi_comments_detail.X - 10, &
		tab_1.height - tab_1.tabpage_edi_comments_detail.Y - 25)
//tab_1.tabpage_edi_comments_detail.dw_edi_comments_detail.Resize(tab_1.tabpage_edi_comments_detail.width - tab_1.tabpage_edi_comments_detail.dw_edi_comments_detail.X - 15, &
//		tab_1.tabpage_edi_comments_detail.height - tab_1.tabpage_edi_comments_detail.dw_edi_comments_detail.Y)

end event

on w_customer_order.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_print=create dw_print
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_print
this.Control[iCurrent+3]=this.tab_1
end on

on w_customer_order.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_print)
destroy(this.tab_1)
end on

event ue_getdata;call super::ue_getdata;Long	ll_RowCount,&
		ll_Row,&
		ll_Loop,&
		ll_Number_OF_RowsSelected
		
DataStore lds_Tmp

String	ls_Detail

Choose Case Lower(String(Message.LongParm, "address"))
	Case "additional"
		Message.StringParm = is_additional_data
	Case "detail"	
		lds_tmp = Create DataStore
		IF tab_1.tabpage_customer_order_detail.dw_detail.Find("IsSelected()", 1, tab_1.tabpage_customer_order_detail.dw_detail.RowCount()) < 1 Then
				//// Select The rows
				ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
				ll_RowCount --
				////new line of code
				tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(ll_RowCount, TRUE)
				For ll_Loop = 1 to ll_RowCount
					IF tab_1.tabpage_customer_order_detail.dw_detail.GetItemNumber( ll_Loop, "ordered_units") > 0 Then 
						tab_1.tabpage_customer_order_detail.dw_detail.SelectRow( ll_loop, TRUE)
						ll_Number_OF_RowsSelected ++
					END IF
				Next
		ELSE
			ll_Number_OF_RowsSelected = 1
		END IF
				
		IF ll_Number_OF_RowsSelected > 0 Then
			lds_Tmp.DataObject = tab_1.tabpage_customer_order_detail.dw_detail.DataObject
			lds_tmp.Object.Data = tab_1.tabpage_customer_order_detail.dw_detail.Object.Data.Selected
			ls_Detail = lds_tmp.Object.DataWindow.Data
		ELSE
			ls_Detail = ''
		END IF
				
		Destroy lds_tmp
		Message.StringParm = ls_Detail
	Case 'price_override'
		Message.StringParm = dw_header.GetItemString(1, 'price_override')
	Case 'transmode'
		Message.StringParm = dw_header.GetItemString(1, 'trans_mode')
	Case 'custorderid'
		Message.StringParm = dw_header.GetItemString(1, 'customer_order_id')
	Case 'salesman'
		Message.StringParm = dw_header.GetItemString(1, 'sales_person_code')
	Case 'product_code'
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( 1,"sku_product_code")
			ELSE
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_Row,"sku_product_code")
			END IF
		ELSE
			Message.StringParm = ''
		END IF
	Case 'division'
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( 1,"division_code")
			ELSE
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_Row,"division_code")
			END IF
		ELSE
			Message.StringParm = Message.is_Smandivision
		END IF
	Case 'delivery_date'	
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = String(tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate( 1,"delivery_date"),"mm/dd/yyyy")
			ELSE
				Message.StringParm = String(tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate( ll_Row,"delivery_date"),"mm/dd/yyyy")
			END IF
		ELSE
			Message.StringParm = String(Today(),"mm/dd/yyyy")
		END IF
	Case 'customer_id'	
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( 1,"customer_id")
			ELSE
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_Row,"customer_id")
			END IF
		ELSE
			Choose Case dw_Header.GetItemString( 1, "customer_type")
				Case 'S'
					Message.StringParm = dw_header.GetItemString( 1, "customer_id")
				Case 'B'
					Message.StringParm = dw_header.GetItemString( 1, "billto_customer_id")
				Case 'C'
					Message.StringParm = dw_header.GetItemString( 1, "corporate_customer_id")
			End Choose
			Message.StringParm = ''
		END IF

	Case 'product_state'
			Message.StringParm = dw_Header.GetItemString( 1, "fresh_product")
	Case 'customer_defined_description'
			IF il_RowBelowMouse > 0 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(il_RowBelowMouse,&
											"EDI_description")
											
			END IF
			IF Len(Trim(Message.StringParm)) < 1 Then Message.StringParm = &
						'No Customer description given'
			IF IsNull(Message.StringParm) Then Message.StringParm = &
									'No Customer description given'
	Case else
		Message.StringParm = ''
End Choose
return
end event

event ue_get_data;Long			ll_RowCount, ll_Row, ll_Loop, ll_number_of_rowsSelected, ll_count
String		ls_Detail, ls_selected_plant_array, ls_plant, ls_product_code, ls_micro_test_ind, ls_micro_test_product_found

Choose Case (as_value)
	Case "additional"
		message.StringParm = is_additional_data
	Case "headerIncExc"
		ls_selected_plant_array = ""
		for ll_count = 1 to il_selected_plant_max
			ls_plant = dw_header.getitemstring(1, "plant" + string(ll_count))
			if isnull(ls_plant) then
				ls_selected_plant_array += "  " + "~r~n"
			else
				ls_selected_plant_array += ls_plant + "~r~n"
			end if
		next
		message.StringParm = dw_header.getitemstring(1, "inc_exl_ind") + ls_selected_plant_array
	Case "detail"
//		lds_tmp = Create DataStore
		ls_Detail = ''
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		//new line on datawindow is reason for next line
		ll_RowCount --
		IF tab_1.tabpage_customer_order_detail.dw_detail.Find("IsSelected()", 1, tab_1.tabpage_customer_order_detail.dw_detail.RowCount()) < 1 Then
				tab_1.tabpage_customer_order_detail.dw_detail.SelectRow(ll_RowCount, TRUE)
				For ll_Loop = 1 to ll_RowCount
					IF tab_1.tabpage_customer_order_detail.dw_detail.GetItemNumber( ll_Loop, "ordered_units") > 0 Then 
						tab_1.tabpage_customer_order_detail.dw_detail.SelectRow( ll_loop, TRUE)
						ll_number_of_rowsSelected ++
					END IF
				Next
		ELSE
			ll_number_of_rowsSelected = 1
		END IF
		
		IF ll_number_of_rowsSelected > 0 Then
			For ll_Loop = 1 to ll_RowCount
				If tab_1.tabpage_customer_order_detail.dw_detail.IsSelected(ll_loop) Then
					tab_1.tabpage_customer_order_detail.dw_detail.object.EDI_description[ll_loop] = ""
				End If
			Next
//			lds_Tmp.DataObject = tab_1.tabpage_customer_order_detail.dw_detail.DataObject
//			lds_tmp.Object.Data = tab_1.tabpage_customer_order_detail.dw_detail.Object.Data.Selected
//			ls_Detail = lds_tmp.Object.DataWindow.Data
			tab_1.tabpage_customer_order_detail.dw_detail.ResetUpdate()
			
			ll_row = tab_1.tabpage_customer_order_detail.dw_detail.GetSelectedRow(0)
			Do While ll_row > 0
				ls_detail += String(tab_1.tabpage_customer_order_detail.dw_detail.object.ordered_units[ll_row]) + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.sku_product_code[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.ordered_age[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.ordered_units_uom[ll_row] + "~t"
				ls_detail += String(tab_1.tabpage_customer_order_detail.dw_detail.object.sales_price[ll_row]) + "~t"
				ls_detail += String(tab_1.tabpage_customer_order_detail.dw_detail.object.delivery_date[ll_row]) + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.customer_id[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.div_po_num[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.bid_price_override[ll_row] + "~t"
				ls_detail += String(tab_1.tabpage_customer_order_detail.dw_detail.object.line_number[ll_row]) + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.division_code[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.line_status[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.sales_order_id[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.corp_po_num[ll_row] + "~t~t~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.aged_ind[ll_row] + "~t"
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.Price_Override[ll_row] + "~t~t~t" 
				ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.inc_exl_ind[ll_row] + "~t"
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant1[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant1[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant2[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant2[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant3[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant3[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant4[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant4[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant5[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant5[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant6[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant6[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant7[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant7[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant8[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant8[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant9[ll_row]) then
					ls_detail += "   " + "~t"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant9[ll_row] + "~t"
				end if
				if isnull(tab_1.tabpage_customer_order_detail.dw_detail.object.plant10[ll_row]) then
					ls_detail += "   " + "~r~n"
				else
					ls_detail += tab_1.tabpage_customer_order_detail.dw_detail.object.plant10[ll_row] + "~r~n"
				end if
				
				ll_row = tab_1.tabpage_customer_order_detail.dw_detail.GetSelectedRow(ll_row)
			Loop
		ELSE
			ls_Detail = ''
		END IF
		
//		Destroy lds_tmp		
		Message.StringParm = ls_Detail
	Case 'price_override'
			Message.StringParm = dw_header.GetItemString(1, 'price_override')
	Case 'transmode'
		Message.StringParm = dw_header.GetItemString(1, 'trans_mode')
	Case 'custorderid'
		Message.StringParm = dw_header.GetItemString(1, 'customer_order_id')
	Case 'salesman'
		Message.StringParm = dw_header.GetItemString(1, 'sales_person_code')
	Case 'product_code'
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( 1,"sku_product_code")
			ELSE
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_Row,"sku_product_code")
			END IF
		ELSE
			Message.StringParm = ''
		END IF
	Case 'division'
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( 1,"division_code")
			ELSE
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_Row,"division_code")
			END IF
		ELSE
			Message.StringParm = Message.is_Smandivision
		END IF
	Case 'delivery_date'	
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = String(tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate( 1,"delivery_date"),"mm/dd/yyyy")
			ELSE
				Message.StringParm = String(tab_1.tabpage_customer_order_detail.dw_detail.GetItemDate( ll_Row,"delivery_date"),"mm/dd/yyyy")
			END IF
		ELSE
			Message.StringParm = String(Today(),"mm/dd/yyyy")
		END IF
	Case 'customer_id'	
		ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		IF ll_RowCount > 0 Then
			ll_Row = tab_1.tabpage_customer_order_detail.dw_detail.Find('IsSelected()',1,ll_RowCount)
			IF ll_Row < 1 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( 1,"customer_id")
			ELSE
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString( ll_Row,"customer_id")
			END IF
		ELSE
			Choose Case dw_Header.GetItemString( 1, "customer_type")
				Case 'S'
					Message.StringParm = dw_header.GetItemString( 1, "customer_id")
				Case 'B'
					Message.StringParm = dw_header.GetItemString( 1, "billto_customer_id")
				Case 'C'
					Message.StringParm = dw_header.GetItemString( 1, "corporate_customer_id")
			End Choose
			Message.StringParm = ''
		END IF
	Case 'product_state'
			Message.StringParm = dw_Header.GetItemString( 1, "fresh_product")
	Case 'ToolTip'
			IF il_RowBelowMouse > 0 Then 
				Message.StringParm = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(il_RowBelowMouse,&
											"EDI_description")
				is_description = Message.StringParm
				  
			END IF
			IF Len(Trim(Message.StringParm)) < 1 Then Message.StringParm = &
						'No Customer description given'
						
			IF IsNull(Message.StringParm) Then Message.StringParm = &
									'No Customer description given'
			is_description = Message.StringParm
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case "load_instruction"
		Message.StringParm = is_load_instruction
	case "additonal_data_input"
		Message.StringParm = is_additional_data_input
	case "plant"
		Message.StringParm = ''
	case "selected"
		Message.StringParm = is_selected_plant_array
	case "header"
		if ib_header then
			Message.StringParm = "true"
		else
			Message.StringParm = 'false'
		end if
	case "dddwxpos"
		message.StringParm = string(ii_dddw_x)
	case "dddwypos"
		message.StringParm = string(ii_dddw_y)
	case "updateheader"
		message.StringParm = "true"
	case "updatedetail"
		message.StringParm = "true"
	Case "micro_test_product"  //slh SR25459
		ls_micro_test_product_found = "N"
	     ll_RowCount = tab_1.tabpage_customer_order_detail.dw_detail.RowCount()
		For ll_Row = 1 to ll_RowCount
				If tab_1.tabpage_customer_order_detail.dw_detail.IsSelected(ll_Row) Then
						ls_product_code = tab_1.tabpage_customer_order_detail.dw_detail.object.sku_product_code[ll_row]
						
						SELECT micro_test_ind
						INTO :ls_micro_test_ind
						FROM sku_products 
                           WHERE sku_product_code = :ls_product_code
						USING SQLCA;
					
						If ls_micro_test_ind = "Y" Then
							ls_micro_test_product_found = "Y"
							EXIT
						End If
				End If
		Next
		Message.StringParm = ls_micro_test_product_found
	Case else
		Message.StringParm = ''
End Choose
return
end event

event ue_set_data;datastore	lds_selected_plants
long			ll_rowcount, ll_count, ll_datawindow

choose case as_data_item
	case "load_instruction"
		is_load_instruction = as_value
	case "dept_code"
		is_dept_code = as_value	
	case "additonal_data_input"
		is_additional_data_input = as_value
//		Messagebox("DATA",is_additional_data_input)
	case "selected"
		is_selected_plant_array = as_value
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
//	case "cust_target_date"
//		is_cust_target = as_value
//	case "gtl_split_ind"
//		is_gtl_split_ind = as_value
	case "export_country"
  		is_dest_country = as_value
	case "store_door"
  		is_store_door = as_value
	case "deck_code"
  		is_deck_code = as_value		  
end choose
end event

event ue_fileprint;call super::ue_fileprint;INTEGER	li_rtn

String	ls_input_string

istr_error_info.se_event_name = 'ue_build_loads'

ls_input_string = dw_header.GetItemString(1, 'customer_order_id') + '~t'

SetPointer(HourGlass!)

//li_rtn = iu_orp002.nf_orpo74br_customer_order_print(istr_error_info, &
//										ls_input_string)

li_rtn = iu_ws_orp3.uf_orpo74fr_customer_order_print(istr_error_info, &
										ls_input_string)
									
SetPointer(Arrow!)									
									
if li_rtn = 0 then
	MessageBox("Customer Order Print", "Customer Order sent to view file.")
else
	MessageBox("Customer Order Print", "Customer Order print failed")
end if

Return 0
end event

event clicked;if ib_open then close(w_locations_popup)
end event

event ue_filenew;call super::ue_filenew;tab_1.tabpage_customer_order_detail.dw_detail.object.ship_date.Protect = 1
tab_1.tabpage_customer_order_detail.dw_detail.object.ship_date.BackGround.Color = 12632256


end event

type dw_header from u_base_dw_ext within w_customer_order
event ue_post_itemchanged pbm_custom46
event ue_dwndropdown pbm_dwndropdown
event ue_checkvalidcustomerid ( )
integer width = 3003
integer height = 360
integer taborder = 10
string dataobject = "d_customer_order_header"
boolean border = false
end type

event ue_dwndropdown;DataWindowChild	ldwc_temp

u_project_functions	lu_project_functions

Choose Case This.GetColumnName()
	Case 'sales_person_code'
		This.GetChild('sales_person_code', ldwc_Temp)
		If ldwc_temp.RowCount() <= 1 Then
			lu_project_functions.nf_gettsrs(ldwc_temp, &
					message.is_smanlocation)
		End If
End Choose
end event

event ue_checkvalidcustomerid;call super::ue_checkvalidcustomerid;String 	ls_Name, &
			ls_City, & 
			ls_Customer_id , ls_state

Choose Case This.GetItemString(1, "customer_type")
	Case 'S'
		ls_Customer_id = This.GetItemString( 1, "customer_id")
		SELECT customers.short_name, customers.city , customers.state
			INTO :ls_Name, :ls_city , :ls_state
			FROM customers
			WHERE customers.customer_id = :ls_Customer_id
			USING SQLCA;
		If SQLCA.SQLCode = 100 then
			iw_frame.SetMicroHelp("Invalid/Inactive shipto selected")
			This.SetColumn("customer_id")
		END IF
	
	Case 'B'
		ls_Customer_id = This.GetItemString( 1, "billto_customer_id")
		SELECT billtocust.name, billtocust.city , billtocust.state
			INTO :ls_Name, :ls_city , :ls_state
			FROM billtocust
			WHERE billtocust.bill_to_id = :ls_Customer_id
			USING SQLCA;

	Case 'C'
		ls_Customer_id = This.GetItemString( 1, "corporate_customer_id")
		SELECT corpcust.name, corpcust.city , corpcust.state
			INTO :ls_Name, :ls_city , :ls_state
			FROM corpcust
			WHERE corpcust.corp_id = :ls_Customer_id
			USING SQLCA;

END CHOOSE
This.Object.customer_name.Text = ls_Name
This.Object.customer_city.Text = ls_City
This.Object.customer_state.Text = ls_state
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_customer, &
						ldwc_corporate_customer, &
						ldwc_corporate_customer_name, &
						ldwc_corporate_customer_city, &
						ldwc_billto_customer, &
						ldwc_billto_customer_name, &
						ldwc_billto_customer_city, &
						ldwc_salespeople, &
						ldwc_trans_mode

String				ls_salesman

u_project_functions	lu_project_functions

This.GetChild('sales_person_code', ldwc_salespeople)
ldwc_salespeople.Reset()  // This to remove the one line retained on save in the DW

//This.GetChild('customer_id', ldwc_customer)
//ldwc_customer.Reset()		// This to remove the one line retained on save in the DW
//This.GetChild('customer_id', ldwc_customer)

//This.GetChild('customer_name', ldwc_customer_name)
//This.GetChild('customer_city', ldwc_customer_city)

iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)
//iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer_name)
//iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer_city)

//lu_project_functions.nf_getcustomers(ldwc_customer, &
//		message.is_smanlocation)

This.GetChild('corporate_customer_id', ldwc_corporate_customer)
This.GetChild('corporate_customer_name', ldwc_corporate_customer_name)
This.GetChild('corporate_customer_city', ldwc_corporate_customer_city)

ldwc_corporate_customer.ShareData(ldwc_corporate_customer_name)
ldwc_corporate_customer.ShareData(ldwc_corporate_customer_city)

This.GetChild('billto_customer_id', ldwc_billto_customer)
This.GetChild('billto_customer_name', ldwc_billto_customer_name)
This.GetChild('billto_customer_city', ldwc_billto_customer_city)

ldwc_billto_customer.ShareData(ldwc_billto_customer_name)
ldwc_billto_customer.ShareData(ldwc_billto_customer_city)

  SELECT salesman.smanname
    INTO :ls_salesman  
    FROM salesman  
  WHERE salesman.smancode = :Message.is_salesperson_code AND salesman.smantype = 's';

This.SetItem(1, 'sales_person_code', Message.is_salesperson_code)
This.object.sales_person_name.Text = ls_salesman
This.object.sales_location_code.Text = Message.is_smanlocation

String ls_service_center
	SELECT locations.location_name
	  INTO :ls_service_center
	  FROM locations
	WHERE locations.location_code = :Message.is_smanlocation;
This.Object.sales_location_name.Text = ls_service_center

This.GetChild('trans_mode', ldwc_trans_mode)
lu_project_functions.nf_gettutltype(ldwc_trans_mode, 'TRANMODE')
This.SetItem(1, 'trans_mode', 'T')
end event

event itemchanged;call super::itemchanged;Int			li_row_count, &
				li_loop_count, &
				li_column

String		ls_temp, &
				ls_salesloc, &
				ls_salesman, &
				ls_text, &
				ls_location_name,&
				ls_CustomerName,&
				ls_CustomerCity, &
				ls_customer_state

long			ll_count, ll_rowcount, ll_count2, ll_linenumber
				
u_project_functions	lu_project_functions				
				
li_column = This.GetColumn()
ls_text = data

Choose Case This.GetColumnName()
Case 'customer_id'
	li_Row_Count = tab_1.tabpage_customer_order_detail.dw_detail.RowCOunt()
	For li_Loop_Count = 1 to li_Row_Count
		tab_1.tabpage_customer_order_detail.dw_detail.SetItem( li_Loop_Count, "customer_id", data )
	NEXT
	IF lu_project_functions.nf_validatecustomerstate( Data, This.GetItemString(1,"customer_type"),&
																			ls_CustomerName,ls_CustomerCity, ls_customer_state) < 1 Then
		This.Object.customer_name.Text = ''
		This.Object.customer_city.Text = ''	
		This.Object.customer_state.Text = ''
		Return 1
	ELSE
		if isnull(ls_CustomerName) then
			This.Object.customer_name.Text = ''
		else
			This.Object.customer_name.Text = ls_CustomerName
		end if
		if isnull(ls_CustomerCity) then
			This.Object.customer_city.Text = ''
		else
			This.Object.customer_city.Text = ls_CustomerCity
		end if
		if isnull(ls_Customer_state) then
			This.Object.customer_state.Text = ''
		else
			This.Object.customer_state.Text = ls_Customer_state
		end if
		
		ls_Text = lu_project_functions.nf_getdefault_tsr_for_customer ( Data )
		this.setitem( 1, "sales_person_code", ls_Text)
		SELECT salesman.smanname, 
				 salesman.smanloc
			INTO :ls_salesman, 
			     :ls_salesloc
			FROM 	salesman  
			WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';

	   SELECT locations.location_name  
   	   INTO :ls_location_name  
	      FROM locations  
	      WHERE locations.location_code = :ls_salesloc;

		This.object.sales_person_name.Text = ls_salesman
		This.object.sales_location_code.Text = ls_salesloc
		This.object.sales_location_name.Text = ls_location_name  
	END IF
	
	wf_retrieve_cust_plant_defaults(data)
Case 'inc_exl_ind'
	ll_rowcount = tab_1.tabpage_customer_order_detail.dw_detail.rowcount()
	if ll_rowcount > 0 then
		for ll_count2 = 1 to ll_rowcount
			ll_linenumber = tab_1.tabpage_customer_order_detail.dw_detail.getitemnumber (ll_count2, "line_number")
			if ll_linenumber = 0 or isnull(ll_linenumber) then
				tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_count2, "inc_exl_ind", data)
				for ll_count = 1 to il_selected_plant_max
					tab_1.tabpage_customer_order_detail.dw_detail.setitem(ll_count2, "plant" + string(ll_count), dw_header.getitemstring(1, "plant" + string(ll_count)))
				next
			end if
		next 
	end if
	
Case 'billto_customer_id', 'corporate_customer_id'
	IF lu_project_functions.nf_ValidateCustomerstate( Data, This.GetItemString(1,"customer_type"),&
																			ls_CustomerName,ls_CustomerCity, ls_customer_state ) < 1 Then
		This.Object.customer_name.Text = ''
		This.Object.customer_city.Text = ''	
		This.Object.customer_state.Text = ''
		Return 1
	ELSE
		This.Object.customer_name.Text = ls_CustomerName
		This.Object.customer_city.Text = ls_CustomerCity
		This.Object.customer_state.Text = ls_Customer_state
		iw_frame.SetMicroHelp("Ready")
	END IF
Case 'customer_type'
	li_Row_Count = tab_1.tabpage_customer_order_detail.dw_detail.RowCOunt()
	For li_Loop_Count = 1 to li_Row_Count
		tab_1.tabpage_customer_order_detail.dw_detail.SetItem( li_Loop_Count, "customer_id", "" )
	NEXT

CASE 'sales_person_code'
	SELECT salesman.smanname, 
			salesman.smanloc
		INTO :ls_salesman, 
				:ls_salesloc
		FROM salesman  
	WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';

  SELECT locations.location_name  
    INTO :ls_location_name  
    FROM locations  
   WHERE locations.location_code = :ls_salesloc;

	This.object.sales_person_name.Text = ls_salesman
	This.object.sales_location_code.Text = ls_salesloc
	This.object.sales_location_name.Text = ls_location_name  
CASE ELSE	
End Choose

iw_frame.SetMicroHelp("Ready")
This.SetItem(1, 'fields_in_error', Replace(This.GetItemString(1, 'fields_in_error'), &
				li_column, 1, 'M'))
ls_temp = This.GetItemString(1, 'fields_in_error')
Return
end event

event constructor;call super::constructor;DataWindowChild ldwc_temp

This.InsertRow(0)

This.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)

This.GetChild('billto_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_Billtos.ShareData( ldwc_temp)

This.GetChild('corporate_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_Corps.ShareData( ldwc_temp)

This.Is_selection = '0'


end event

event itemerror;call super::itemerror;Return 1

end event

event clicked;call super::clicked;Long count
String ls_protected
is_columnname = dwo.name

choose case dwo.name
	case 'plant1'
		ib_header = true
		il_selected_row = row
		ib_open = true
		wf_get_selected_plants(this)
		OpenWithParm( w_locations_popup, parent)
	case 'price_override'
		dw_header.AcceptText()
		If (dw_header.GetItemString(1, "price_override") = 'N') then
		For count = 1 to tab_1.tabpage_customer_order_detail.dw_detail.rowcount()
			ls_protected = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(count, "detail_errors") 
			If (Mid(ls_protected, 18, 1) = 'E') then 
				tab_1.tabpage_customer_order_detail.dw_detail.SetItem(count, 'price_override', 'Y')
			end if
			If (Mid(ls_protected, 18, 1) = 'M') then 
				tab_1.tabpage_customer_order_detail.dw_detail.SetItem(count, 'price_override', 'Y')
			end if
		Next
	else
		For count = 1 to tab_1.tabpage_customer_order_detail.dw_detail.rowcount()
			ls_protected = tab_1.tabpage_customer_order_detail.dw_detail.GetItemString(count, "detail_errors") 
			If (Mid(ls_protected, 18, 1) = 'E') then 
				tab_1.tabpage_customer_order_detail.dw_detail.SetItem(count, 'price_override', 'N')
			end if
			If (Mid(ls_protected, 18, 1) = 'M') then 
				tab_1.tabpage_customer_order_detail.dw_detail.SetItem(count, 'price_override', 'N')
			end if
		Next
	End if
		
end choose
end event

event itemfocuschanged;call super::itemfocuschanged;is_columnname = dwo.name
end event

event losefocus;this.AcceptText()

if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = true
			il_selected_row = this.getrow()
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, parent)
	end choose
end if
end event

type dw_print from u_base_dw_ext within w_customer_order
boolean visible = false
integer x = 357
integer y = 556
integer height = 316
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_customerorder_print"
end type

type tab_1 from tab within w_customer_order
event create ( )
event destroy ( )
integer x = 5
integer y = 360
integer width = 3026
integer height = 900
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 79741120
boolean raggedright = true
integer selectedtab = 1
tabpage_customer_order_detail tabpage_customer_order_detail
tabpage_edi_comments_detail tabpage_edi_comments_detail
end type

on tab_1.create
this.tabpage_customer_order_detail=create tabpage_customer_order_detail
this.tabpage_edi_comments_detail=create tabpage_edi_comments_detail
this.Control[]={this.tabpage_customer_order_detail,&
this.tabpage_edi_comments_detail}
end on

on tab_1.destroy
destroy(this.tabpage_customer_order_detail)
destroy(this.tabpage_edi_comments_detail)
end on

type tabpage_customer_order_detail from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 100
integer width = 2990
integer height = 784
long backcolor = 79741120
string text = "Customer Order Detail"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
dw_detail dw_detail
end type

on tabpage_customer_order_detail.create
this.dw_detail=create dw_detail
this.Control[]={this.dw_detail}
end on

on tabpage_customer_order_detail.destroy
destroy(this.dw_detail)
end on

type dw_detail from u_base_dw_ext within tabpage_customer_order_detail
integer x = 9
integer y = 20
integer width = 2939
integer height = 764
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_customer_order_b"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;// Verify if a valid row was clicked on
IF IsValid(iw_ToolTip)Then Close(iw_ToolTip)
If row < 1 Then
	Return
End If

IF dwo.name = "line_number"  &
		AND dw_detail.GetItemNumber( row, "ordered_units") > 0 Then
	Call Super::Clicked	
ELSE
   This.SelectRow(row, False)
END IF
is_columnname = dwo.name

choose case dwo.name
	case 'plant1'
		ib_header = false
		il_selected_row = row
		ib_open = true
		wf_get_selected_plants(this)
		openwithparm( w_locations_popup, iw_this)
end choose

end event

event constructor;call super::constructor;This.Modify("DataWindow.Selected.Mouse = No")
ib_NewRowOnChange = True
is_selection = '4'
ib_firstcolumnonnextrow = TRUE
ib_updateable = TRUE

end event

event doubleclicked;call super::doubleclicked;Decimal	ldc_OriginalValue, &
			ldc_NewValue

Int		li_SplitCount, &
			li_Counter, &
			li_null

Long		ll_LineNumber

String	ls_so_id, &
			ls_SplitCount, &
			ls_LineStatus

Window	lw_detail

u_string_functions	lu_string_functions

IF IsValid(iw_ToolTip)Then Close(iw_ToolTip)
Choose Case dwo.name
		
Case 'delivery_date'
	// let the framework handle this one
Case 'sales_order_id'
	SetPointer(HourGlass!)
	ls_so_id = This.GetItemString(Row, 'sales_order_id')
	If lu_string_functions.nf_IsEmpty(ls_so_id) Then return
	OpenSheetWithParm(lw_detail, ls_so_id, 'w_sales_order_detail', &
							iw_frame, 8, iw_Frame.im_menu.iao_ArrangeOpen)

Case 'line_number'
	Open(w_customer_order_split)
	ls_SplitCount = Message.StringParm
	If lu_string_functions.nf_IsEmpty(ls_SplitCount) Then return

	li_SplitCount = Integer(ls_SplitCount)
	If li_SplitCount < 1 Then return 
	IF IsNull(This.GetItemNumber(Row, 'ordered_units')) THEN
		ldc_OriginalValue =	0
	ELSE
		ldc_OriginalValue = This.GetItemDecimal(Row, 'ordered_units')
	END IF

	ldc_NewValue = ldc_OriginalValue / li_SplitCount
	
	This.SetRedraw(False)
	This.SetItem(Row, 'ordered_units', ldc_NewValue)

	ll_LineNumber = This.GetItemNumber(Row, 'line_number')
	SetNull(li_null)
	This.SetItem(Row, 'line_number', li_Null)

	ls_LineStatus = This.GetItemString(Row, 'update_flag')
	This.SetItem(Row, 'update_flag', 'A')

	For li_Counter = 1 to li_SplitCount - 1
		This.RowsCopy(Row, Row, Primary!, This, Row, Primary!)
	Next

	This.SetItem(Row, 'line_number', ll_LineNumber)
	IF lu_string_functions.nf_IsEmpty(ls_LineStatus) Or ls_LineStatus = 'U' Then
		This.SetItem(Row, 'update_flag', 'U')
		This.SetItem(Row, 'detail_errors','M'+ &
							mid( This.GetItemString( Row, "detail_errors"),2))
	ELSE
		This.SetItem(Row, 'update_flag', 'A')
	END IF

	This.SetRow(Row)
	This.PostEvent(RowFocusChanged!)
	This.SetRedraw(True)
End Choose
end event

event itemchanged;call super::itemchanged;Int		li_count, &
			li_column

Date		ldt_date, &
			ldt_date2

Long		ll_rowcount

string 	ls_customer
boolean	lb_fresh
		
u_project_functions lu_project_functions

ll_RowCount = This.RowCount()

// Put All Validation before this
li_Column = This.GetColumn()
Choose Case Lower( dwo.Name)
	Case 'ordered_units'
		//This.GetItemStatus( row, 0, Primary!)  = New!
		If Row = ll_RowCount -1  And row > 1   Then
			IF IsNull( This.GetItemDate( row, 'delivery_date')) Then
				This.SetItem( Row, 'delivery_date', GetItemDate( row - 1, 'delivery_date'))
			END IF
		End IF
	Case 'aged_ind'
		li_Column -= 2
	Case 'delivery_date'
			ldt_date = Date( Data)
			ldt_date2 = this.GetItemDate(row,"ship_date")
			IF row = 1 Then 
				For li_count = 2 to ll_RowCount - 1
					IF String(This.GetItemDate(li_count, 'delivery_date'), "yyyy-mm-dd") = "" Then
						This.SetItem(li_count, 'delivery_date', ldt_date)
					Else
						if String(This.GetItemDate(li_count, 'delivery_date'), "yyyy-mm-dd") = '0001-01-01' and & 
								This.GetItemDate(li_count, 'ship_date')  = ldt_date2 then
							This.SetItem(li_count, 'delivery_date', ldt_date)
						end if
					End If
				Next
			ELSE
				ldt_date = Date( Data)
				ldt_date2 = this.GetItemDate(row,"ship_date")
				for li_count = row + 1 to ll_RowCount - 1 
					if String(This.GetItemDate(li_count, 'delivery_date'), "yyyy-mm-dd") = '0001-01-01' and & 
								This.GetItemDate(li_count, 'ship_date')  = ldt_date2 then
							This.SetItem(li_count, 'delivery_date', ldt_date)
					end if
				Next
			End IF
	Case 'ship_date'
			ldt_date = Date( Data)
			IF row = 1 Then 
				For li_count = 2 to ll_RowCount - 1
					IF String(This.GetItemDate(li_count, 'ship_date'), "yyyy-mm-dd") = "" Then
						This.SetItem(li_count, 'ship_date', ldt_date)
					Else
						Exit
					End If
				Next
			End IF		
	Case 'sku_product_code'
		ls_customer	= dw_header.GetItemString(1, "corporate_customer_id")
		lb_fresh = (dw_header.GetItemString(1, "fresh_product") = 'Y')

		This.SetItem( Row, 'ordered_age', lu_project_functions.nf_get_age_code(Data, ls_customer, lb_fresh))	
		
	Case 'ordered_units_uom'
		If data = '03' Then 
			This.SetItem( row, 'ordered_units', 1)
			iw_frame.SetMicrohelp('Ordered Units have been changed to 1.00 for LOADS')
		END IF
	Case ELSE
END CHOOSE

IF This.ib_NewRowOnChange AND (This.il_CurrentRow + 1 = This.RowCount()) THEN
	wf_default_newrow()
END IF

This.SetItem( row, 'detail_errors', Replace(This.GetItemString( row, 'detail_errors'), &
				li_column, 1, 'M'))
If This.GetItemString(row, 'update_flag') = ' ' Then &
		This.SetItem( row, 'update_flag', 'U')

IF dw_Header.GetItemString(1, "customer_order_id") <> "0000000"	Then
	dw_Header.SetItem( 1, "update_flag", "U")
END IF


end event

event itemfocuschanged;call super::itemfocuschanged;is_columnname = dwo.name
end event

event losefocus;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = false
			il_selected_row = this.getrow()
			wf_get_selected_plants(this)
			ib_open = true
			openwithparm( w_locations_popup, iw_this)			
	end choose
end if
end event

event rbuttondown;call super::rbuttondown;m_orp_pext_popup NewMenu

NewMenu = CREATE m_orp_pext_popup
NewMenu.m_file.m_padisplay.Enabled=True
NewMenu.m_file.m_generate.Enabled=True

NewMenu.M_file.m_cascadeplants.enabled=False
NewMenu.M_file.m_cascadeplants.visible=False
NewMenu.M_file.m_reinquire.enabled=False
NewMenu.M_file.m_reinquire.visible=False
NewMenu.M_file.m_pasummary.enabled=False
NewMenu.M_file.m_pasummary.visible=False
NewMenu.M_file.m_completeorder.enabled=False
NewMenu.M_file.m_completeorder.visible=False

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu

end event

event scrollhorizontal;il_horz_scroll_pos = scrollpos
end event

event ue_keydown;call super::ue_keydown;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = false
			il_selected_row = this.getrow()
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, iw_this)
	end choose
end if
end event

event ue_mousemove;call super::ue_mousemove;Long	ll_row
String	ls_AtPointer
ls_AtPointer = This.GetObjectAtPointer()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50
ll_Row = Long( Mid(ls_AtPointer,Pos(ls_AtPointer,"~t")+1))
	il_rowBelowMouse = ll_row
If ll_row > 0  Then
	IF dw_header.GetItemString(1,"type_of_order") = 'E' AND &
		Mid(This.GetItemString(ll_row, 'detail_errors'),2,1) = 'E'Then
		IF dwo.name = 'sku_product_code' Then
			IF IsValid(iw_tooltip) Then
				if is_description <> is_old_description or il_old_row <> ll_row then
					close(iw_toolTip)
					openwithparm(iw_ToolTip, iw_this, iw_frame)
					is_old_description = is_description
					il_old_row = ll_row
				end if
			Else
				openwithparm(iw_ToolTip, iw_this, iw_frame)
			END IF
		END IF
	ELSE
		IF IsValid(iw_ToolTip) Then &
			Close(iw_ToolTip)
	END IF
END IF
end event

event ue_setbusinessrule;String ls_BusinessRule

Long		ll_Column

if (row > this.rowcount()) or (row = 0) then
	return 1
end if


if ISNull(This.GetItemString(Row,"detail_errors")) Then This.SetItem(Row,"detail_errors","        V         ")


//Currently these correspond to the column number, but if a column is added they will not
Choose Case name
	Case 'ordered_units'
	 	ll_column = 1
	Case 'sku_product_code'
   	ll_Column = 2
	Case 'ordered_age'
		ll_column = 3
	Case 'ordered_units_uom'
		ll_column = 4
	Case 'sales_price'
		ll_Column = 5
	Case Else
		Return -1
END CHOOSE

ls_BusinessRule = Left(This.GetItemString(row,"detail_errors"),ll_Column -1) &
						+ Value + Mid(This.GetItemString(row,"detail_errors"),ll_Column +1)
						
This.SetItem(row,"detail_errors", ls_BusinessRule)
//messagebox("ue_setBusinessRule","U R Here")
	
Return 1
end event

type tabpage_edi_comments_detail from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2990
integer height = 784
boolean enabled = false
long backcolor = 79741120
string text = "EDI Comments"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
mle_1 mle_1
st_1 st_1
end type

on tabpage_edi_comments_detail.create
this.mle_1=create mle_1
this.st_1=create st_1
this.Control[]={this.mle_1,&
this.st_1}
end on

on tabpage_edi_comments_detail.destroy
destroy(this.mle_1)
destroy(this.st_1)
end on

type mle_1 from u_base_multilineedit_ext within tabpage_edi_comments_detail
integer x = 489
integer y = 88
integer width = 2121
integer height = 440
integer taborder = 11
boolean bringtotop = true
fontcharset fontcharset = ansi!
long textcolor = 0
long backcolor = 12632256
integer limit = 200
boolean displayonly = true
end type

type st_1 from statictext within tabpage_edi_comments_detail
integer x = 87
integer y = 104
integer width = 343
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "EDI Comments:"
boolean focusrectangle = false
end type

