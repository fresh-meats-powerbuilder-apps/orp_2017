HA$PBExportHeader$pasp00sr_programinterface1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterface1 from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterface1 from nonvisualobject
end type

type variables
    pasp00sr_ProgramInterfacePasp00ci1 pasp00ci
    pasp00sr_ProgramInterfacePasp00pg1 pasp00pg
    pasp00sr_ProgramInterfacePasp00in1 pasp00in
    pasp00sr_ProgramInterfacePasp00ot1 pasp00ot
    boolean channel
end variables

on pasp00sr_ProgramInterface1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterface1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

