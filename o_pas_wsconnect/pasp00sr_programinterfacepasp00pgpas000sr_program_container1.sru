HA$PBExportHeader$pasp00sr_programinterfacepasp00pgpas000sr_program_container1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00pgPas000sr_program_container1 from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00pgPas000sr_program_container1 from nonvisualobject
end type

type variables
    int pas000sr_rval
    string pas000sr_message
    ulong pas000sr_task_num
    ulong pas000sr_max_record_num
    ulong pas000sr_last_record_num
    uint pas000sr_version_number
end variables

on pasp00sr_ProgramInterfacePasp00pgPas000sr_program_container1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00pgPas000sr_program_container1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

