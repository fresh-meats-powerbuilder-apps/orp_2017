HA$PBExportHeader$pasp00sr_programinterface.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterface from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterface from nonvisualobject
end type

type variables
    pasp00sr_ProgramInterfacePasp00ci pasp00ci
    pasp00sr_ProgramInterfacePasp00pg pasp00pg
    pasp00sr_ProgramInterfacePasp00in pasp00in
    pasp00sr_ProgramInterfacePasp00ot pasp00ot
    boolean channel
end variables

on pasp00sr_ProgramInterface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

