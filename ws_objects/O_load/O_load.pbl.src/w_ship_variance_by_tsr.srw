﻿$PBExportHeader$w_ship_variance_by_tsr.srw
forward
global type w_ship_variance_by_tsr from w_base_sheet_ext
end type
type dw_header_variance from u_base_dw_ext within w_ship_variance_by_tsr
end type
type dw_data_variance from u_base_dw_ext within w_ship_variance_by_tsr
end type
end forward

global type w_ship_variance_by_tsr from w_base_sheet_ext
integer width = 3986
integer height = 2096
string title = "Ship Variance by TSR"
dw_header_variance dw_header_variance
dw_data_variance dw_data_variance
end type
global w_ship_variance_by_tsr w_ship_variance_by_tsr

type variables
s_error		istr_error_info
string ls_param_inquire
u_ws_orp3   iu_ws_orp3
Boolean  	ib_ReInquire
Window	iw_sales_detail
String 	is_Sales_ID
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();string ls_parameters,ls_location, ls_output_string,ls_tsr
date ls_ship_date
integer li_rval
long row_numbers

If Not ib_ReInquire Then
	//send parameters to inquire window
	ls_ship_date = Today()
	ls_parameters = string(ls_ship_date) + '~t' 
	ib_ReInquire = True
	OpenWithParm( w_ship_variance_by_tsr_inq, ls_parameters)
Else
	ls_ship_date = dw_header_variance.GetItemDate(1,"sched_ship_date")
	ls_location = dw_header_variance.GetItemString(1,"location")
	ls_tsr = dw_header_variance.GetItemString(1,"tsr")
	ls_parameters = string(ls_ship_date) + '~t' + ls_location + '~t' + ls_tsr
	OpenWithParm( w_ship_variance_by_tsr_inq, ls_parameters) 
End if

ls_param_inquire = Message.StringParm

IF ls_param_inquire = 'CANCEL' or ls_param_inquire = ' ' THEN
	iw_frame.SetMicroHelp("Ready")
	dw_header_variance.Modify("tsr.Protect=1")
	dw_header_variance.Modify("tsr.Background.Color='12632256'")
	dw_header_variance.Modify("sched_ship_date.Protect=1")
	dw_header_variance.Modify("sched_ship_date.Background.Color='12632256'")
	Return False
ELSE
	//format for header
	dw_header_variance.Reset()
	dw_header_variance.ImportString(ls_param_inquire)
	dw_header_variance.Modify("tsr.Protect=1")
	dw_header_variance.Modify("tsr.Background.Color='12632256'")
	dw_header_variance.Modify("sched_ship_date.Protect=1")
	dw_header_variance.Modify("sched_ship_date.Background.Color='12632256'")
	
	//logic to call the rpc
	li_rval = iu_ws_orp3.uf_orpo99gr(istr_error_info,ls_param_inquire,ls_output_string)
	
	IF li_rval = 0 THEN
		dw_data_variance.Reset()
		dw_data_variance.ImportString(ls_output_string)
		row_numbers = dw_data_variance.RowCount()
		iw_frame.SetMicroHelp(String(row_numbers) + " Rows Retrieved")
	END IF
	
END IF


return true 
end function

on w_ship_variance_by_tsr.create
int iCurrent
call super::create
this.dw_header_variance=create dw_header_variance
this.dw_data_variance=create dw_data_variance
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header_variance
this.Control[iCurrent+2]=this.dw_data_variance
end on

on w_ship_variance_by_tsr.destroy
call super::destroy
destroy(this.dw_header_variance)
destroy(this.dw_data_variance)
end on

event ue_postopen;call super::ue_postopen;iu_ws_orp3 = CREATE u_ws_orp3

istr_error_info.se_app_name 		= message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_ship_variance_by_tsr"
istr_error_info.se_user_id 		= sqlca.userid

dw_header_variance.InsertRow(1)
dw_data_variance.InsertRow(1)

ib_ReInquire = False
wf_Retrieve()

end event

event close;call super::close;Destroy (iu_ws_orp3)
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_deleterow")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")
iw_Frame.im_Menu.mf_disable("m_print")
end event

event ue_get_data;call super::ue_get_data;String ls_indicator

Choose Case as_value
	Case "Order_ID"
		Message.StringParm = is_sales_id
End Choose
end event

type dw_header_variance from u_base_dw_ext within w_ship_variance_by_tsr
integer x = 69
integer y = 32
integer width = 1390
integer height = 376
integer taborder = 10
string dataobject = "d_ship_variance_by_tsr_inq"
borderstyle borderstyle = styleraised!
end type

type dw_data_variance from u_base_dw_ext within w_ship_variance_by_tsr
integer x = 73
integer y = 452
integer width = 3803
integer height = 1468
integer taborder = 10
string dataobject = "d_ship_variance_by_tsr"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;STRING							ls_SalesID, ls_ColumnName

Long								ll_row

w_sales_order_detail			lw_Temp

// This has to be clickedrow because if you are are double clicking on 
// a protected row the row focus does change
ll_row = row

// You must get clicked column because order ID is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name

CHOOSE CASE ls_ColumnName
CASE	"order_num"
	ls_SalesID = This.GetItemString(ll_row, "order_num")
	IF IsValid( iw_sales_detail) Then
		iw_sales_detail.dynamic wf_set_to_order_id( ls_SalesID)
		iw_sales_detail.SetFocus() 
		iw_sales_detail.PostEvent("Ue_Move_Rows")			
		iw_sales_detail = lw_temp
	ELSE
		iw_frame.iu_project_functions.nf_list_sales_orders(ls_salesid)
	END IF
END CHOOSE
end event

event rbuttondown;call super::rbuttondown;m_ship_variance_popup NewMenu
m_gtl_cascade lm_pop

u_project_functions lu_project_functions
// dld Added for gtl Project
If dwo.name = 'gtl_split_ind' then
	lm_pop = CREATE m_gtl_cascade
	dw_data_variance.SetRow ( row )
	lm_pop.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
	Destroy lm_pop
	Return
End If
 
If lu_project_functions.nf_isscheduler() Then Return

//If Row > 0 Then
//	is_Sales_ID = This.GetItemString(row,"order_num")
//End IF

If this.getselectedrow(0) > 0 Then
	is_Sales_ID = This.GetItemString(this.getselectedrow(0),"order_num")
End IF

NewMenu = CREATE m_ship_variance_popup
NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
end event

event constructor;call super::constructor;is_selection = '1'
end event

