﻿$PBExportHeader$w_popup_ordered_bol.srw
forward
global type w_popup_ordered_bol from w_base_popup
end type
type r_1 from rectangle within w_popup_ordered_bol
end type
type st_1 from statictext within w_popup_ordered_bol
end type
end forward

global type w_popup_ordered_bol from w_base_popup
integer x = 663
integer y = 1444
integer width = 1399
integer height = 216
boolean titlebar = false
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
boolean border = false
long backcolor = 1090519039
r_1 r_1
st_1 st_1
end type
global w_popup_ordered_bol w_popup_ordered_bol

on w_popup_ordered_bol.create
int iCurrent
call super::create
this.r_1=create r_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.r_1
this.Control[iCurrent+2]=this.st_1
end on

on w_popup_ordered_bol.destroy
call super::destroy
destroy(this.r_1)
destroy(this.st_1)
end on

type r_1 from rectangle within w_popup_ordered_bol
long linecolor = 16711680
integer linethickness = 21
long fillcolor = 12632256
integer width = 1400
integer height = 216
end type

type st_1 from statictext within w_popup_ordered_bol
integer x = 55
integer y = 60
integer width = 1280
integer height = 76
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Please wait preparing Ordered/BOL Comparison"
boolean focusrectangle = false
end type

