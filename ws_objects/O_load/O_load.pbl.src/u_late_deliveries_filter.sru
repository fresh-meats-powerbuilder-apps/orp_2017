﻿$PBExportHeader$u_late_deliveries_filter.sru
forward
global type u_late_deliveries_filter from nonvisualobject
end type
end forward

global type u_late_deliveries_filter from nonvisualobject autoinstantiate
end type

type variables
String	carrier, &
	complex, &
	sales_loc, &
	billto_cust, &
	shipto_cust, &
	tsr, &
	svc_region, &
	division, &
	delv_from_date, &
	delv_to_date, &
	trans_mode

boolean	cancel

end variables

on u_late_deliveries_filter.create
TriggerEvent( this, "constructor" )
end on

on u_late_deliveries_filter.destroy
TriggerEvent( this, "destructor" )
end on

