﻿$PBExportHeader$u_214_review.sru
forward
global type u_214_review from nonvisualobject
end type
end forward

global type u_214_review from nonvisualobject autoinstantiate
end type

type variables
string	load_key, &
	from_carrier, &
	to_carrier, &
	status_code, &
	trans_date_from, &
	trans_date_to, &
	ibp_process_code

boolean	cancel
end variables

on u_214_review.create
TriggerEvent( this, "constructor" )
end on

on u_214_review.destroy
TriggerEvent( this, "destructor" )
end on

