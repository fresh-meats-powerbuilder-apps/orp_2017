﻿$PBExportHeader$w_ordered_bol_comparison.srw
forward
global type w_ordered_bol_comparison from w_base_sheet_ext
end type
type dw_ordered_bol_comparison from u_base_dw_ext within w_ordered_bol_comparison
end type
end forward

global type w_ordered_bol_comparison from w_base_sheet_ext
string title = "Ordered/BOL Comparison Report"
dw_ordered_bol_comparison dw_ordered_bol_comparison
end type
global w_ordered_bol_comparison w_ordered_bol_comparison

type variables
string is_orderid

u_ws_orp3	iu_ws_orp3

s_error istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();string ls_orderid
OpenWithParm( w_sales_Detail_Inq, is_orderid)
ls_orderid	=	Message.StringParm
IF ls_orderid = 'Abort' Then 
	RETURN FALSE
Else
	is_orderid	=	ls_orderid
	this.triggerevent('ue_query')
END IF
return true
end function

on w_ordered_bol_comparison.create
int iCurrent
call super::create
this.dw_ordered_bol_comparison=create dw_ordered_bol_comparison
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ordered_bol_comparison
end on

on w_ordered_bol_comparison.destroy
call super::destroy
destroy(this.dw_ordered_bol_comparison)
end on

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_print")
end event

event close;call super::close;destroy iu_ws_orp3
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_disable("m_print")
end event

event open;call super::open;ib_faxing_available = TRUE
is_orderid = Message.StringParm
end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return
dw_ordered_bol_comparison.Print()
end event

event ue_postopen;call super::ue_postopen;is_typecode = Message.is_smanlocation

u_string_functions lu_string_functions
IF lu_string_functions.nf_isempty ( is_orderid ) Then
	if not wf_retrieve() then
		close (this)
	end if
ELSE
	This.PostEvent("ue_query")
END IF

end event

event ue_query;call super::ue_query;DataWindowChild	ldwc_header,&
						ldwc_detail,&
						ldwc_pallet,&
						ldwc_tare,&
						ldwc_hides
						
Open(w_popup_ordered_bol)

dw_ordered_bol_comparison.GetChild("dw_header",ldwc_header)
dw_ordered_bol_comparison.GetChild("dw_detail", ldwc_detail)
dw_ordered_bol_comparison.Getchild('dw_pallet',ldwc_pallet)
dw_ordered_bol_comparison.GetChild('dw_tare', ldwc_tare)
dw_ordered_bol_comparison.GetChild('dw_hides', ldwc_hides)

if NOT IsValid( iu_ws_orp3) Then iu_ws_orp3 = Create u_ws_orp3
iu_ws_orp3.uf_orpo98gr(is_orderid, ldwc_header, &
								ldwc_detail, ldwc_pallet, ldwc_tare, ldwc_hides, istr_error_info )
								
if istr_error_info.se_message <> '' then
	iw_frame.SetMicroHelp(istr_error_info.se_message)
else
	iw_frame.SetMicroHelp("Ready")
end if
	
Close(w_popup_ordered_bol)
end event

type dw_ordered_bol_comparison from u_base_dw_ext within w_ordered_bol_comparison
integer x = 27
integer y = 8
integer width = 2775
integer height = 1308
integer taborder = 10
string dataobject = "d_ordered_bol_composite"
boolean hscrollbar = true
boolean vscrollbar = true
end type

