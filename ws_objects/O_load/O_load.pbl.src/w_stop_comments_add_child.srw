﻿$PBExportHeader$w_stop_comments_add_child.srw
forward
global type w_stop_comments_add_child from w_base_response_ext
end type
type dw_stop_comments_add from u_base_dw_ext within w_stop_comments_add_child
end type
end forward

global type w_stop_comments_add_child from w_base_response_ext
int X=174
int Y=473
int Width=2524
int Height=961
boolean TitleBar=true
string Title="Stop Comments Add"
long BackColor=12632256
dw_stop_comments_add dw_stop_comments_add
end type
global w_stop_comments_add_child w_stop_comments_add_child

type variables
u_otr003              iu_otr003

Character            ic_load_key[7], &
                          ic_stop_code[2]


end variables

forward prototypes
public function integer wf_add_stop_comments ()
end prototypes

public function integer wf_add_stop_comments ();String   ls_load_key, &
         ls_stop_code, &
         ls_comments, &
         ls_comments_1, &
         ls_comments_2, &
         ls_comments_3, &
         ls_comments_4, &
         ls_comments_5, &
         ls_comments_6, &
         ls_comments_7, &
         ls_comments_8, &
         ls_comments_9, &
         ls_comments_10

integer  li_rtn, &
			li_length, & 
         li_difference

long     ll_Row, &
         ll_RowCount
s_error  lstr_Error_Info


lstr_error_info.se_event_name = "wf_add_stop_comments"
lstr_error_info.se_window_name = "w_stop_comments_add_child"
lstr_error_info.se_procedure_name = "wf_add_stop_comments"
lstr_error_info.se_user_id = SQLCA.Userid
lstr_error_info.se_message = Space(71)

  dw_stop_comments_add.AcceptText()
  ll_Row =  dw_stop_comments_add.GetRow()
  ll_RowCount =  dw_stop_comments_add.RowCount()

  ls_load_key  = ic_load_key     
  ls_stop_code = ic_stop_code

  ls_comments_1  = dw_stop_comments_add.object.stop_comments_1[1]
  li_length = Len(ls_comments_1) 
  li_difference = 70 - li_length
  ls_comments_1 = ls_comments_1 + space(li_difference)

  ls_comments_2  = dw_stop_comments_add.object.stop_comments_2[1]
  li_length = Len(ls_comments_2) 
  li_difference = 70 - li_length
  ls_comments_2 = ls_comments_2 + space(li_difference)
 
  ls_comments_3  = dw_stop_comments_add.object.stop_comments_3[1]
  li_length = Len(ls_comments_3) 
  li_difference = 70 - li_length
  ls_comments_3 = ls_comments_3 + space(li_difference)
  
  ls_comments_4  = dw_stop_comments_add.object.stop_comments_4[1]
  li_length = Len(ls_comments_4) 
  li_difference = 70 - li_length
  ls_comments_4 = ls_comments_4 + space(li_difference)
  
  ls_comments_5  = dw_stop_comments_add.object.stop_comments_5[1]
  li_length = Len(ls_comments_5) 
  li_difference = 70 - li_length
  ls_comments_5 = ls_comments_5 + space(li_difference)

  ls_comments_6  = dw_stop_comments_add.object.stop_comments_6[1]
  li_length = Len(ls_comments_6) 
  li_difference = 70 - li_length
  ls_comments_6 = ls_comments_6 + space(li_difference)
  
  ls_comments_7  = dw_stop_comments_add.object.stop_comments_7[1]
  li_length = Len(ls_comments_7) 
  li_difference = 70 - li_length
  ls_comments_7 = ls_comments_7 + space(li_difference)
    
  ls_comments_8  = dw_stop_comments_add.object.stop_comments_8[1]
  li_length = Len(ls_comments_8) 
  li_difference = 70 - li_length
  ls_comments_8 = ls_comments_8 + space(li_difference)
    
  ls_comments_9  = dw_stop_comments_add.object.stop_comments_9[1]
  li_length = Len(ls_comments_9) 
  li_difference = 70 - li_length
  ls_comments_9 = ls_comments_9 + space(li_difference)
  
  ls_comments_10 = dw_stop_comments_add.object.stop_comments_10[1]
  li_length = Len(ls_comments_10) 
  li_difference = 70 - li_length
  ls_comments_10 = ls_comments_10 + space(li_difference)
  
  ls_comments  = ls_comments_1 + ls_comments_2 + ls_comments_3 + ls_comments_4 + &
                 ls_comments_5 + ls_comments_6 + ls_comments_7 + ls_comments_8 + &
                 ls_comments_9 + ls_comments_10

  li_rtn = iu_otr003.nf_otrt32ar(ls_load_key, ls_stop_code, &
           ls_comments, lstr_error_info, 0)
  
// Check the return code from the above function call
  If li_rtn < 0 Then
     Return -1
  Else 
     iw_frame.SetMicroHelp( "Ready...")
     Close ( w_stop_comments_add_child) 
  End If

Return 1


end function

event ue_base_ok;call super::ue_base_ok;Integer        li_rc


IF dw_stop_comments_add.ModifiedCount( ) > 0 THEN 
	li_rc = MessageBox( "Modified Data" , "Save changes before closing ?", &
           Question! , YesNoCancel! )
ELSE
   Close ( This )
   iw_frame.SetMicroHelp ("Ready...")
END IF 

IF li_rc = 1 Then
   wf_add_stop_comments()
ELSE
  IF li_rc = 2 Then
     Close ( This )
  END IF
END IF
end event

event close;call super::close;DESTROY iu_otr003
end event

event open;call super::open;iu_otr003  =  CREATE u_otr003
 
long          ll_newrow

string        ls_input_string, &
              ls_temp, &
              ls_space



ls_input_String = Message.StringParm

ls_temp = Left(ls_input_string, 7)	
ic_load_key =  ls_temp

ls_temp =  Mid(ls_input_string, 8, 2)	
ic_stop_code =  ls_temp

This.Title = "Comments for Load Key "+ic_load_key[] + " Stop " + ic_stop_code[]  

ls_space = space(70)

ll_newrow = dw_stop_comments_add.InsertRow(0) 
dw_stop_comments_add.ScrollToRow(ll_newrow)
dw_stop_comments_add.SetFocus()

dw_stop_comments_add.object.stop_comments_1[1] = ls_space
dw_stop_comments_add.object.stop_comments_2[2] = ls_space
dw_stop_comments_add.object.stop_comments_3[3] = ls_space
dw_stop_comments_add.object.stop_comments_4[4] = ls_space
dw_stop_comments_add.object.stop_comments_5[5] = ls_space
dw_stop_comments_add.object.stop_comments_6[6] = ls_space
dw_stop_comments_add.object.stop_comments_7[7] = ls_space
dw_stop_comments_add.object.stop_comments_8[8] = ls_space
dw_stop_comments_add.object.stop_comments_9[9] = ls_space
dw_stop_comments_add.object.stop_comments_10[10] = ls_space

dw_stop_comments_add.SetFocus()

end event

on w_stop_comments_add_child.create
int iCurrent
call w_base_response_ext::create
this.dw_stop_comments_add=create dw_stop_comments_add
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_stop_comments_add
end on

on w_stop_comments_add_child.destroy
call w_base_response_ext::destroy
destroy(this.dw_stop_comments_add)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_stop_comments_add_child
int X=1486
int Y=713
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_stop_comments_add_child
int X=1166
int Y=713
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_stop_comments_add_child
int X=846
int Y=713
int TabOrder=20
end type

type dw_stop_comments_add from u_base_dw_ext within w_stop_comments_add_child
int X=37
int Y=29
int Width=2442
int Height=641
int TabOrder=10
string DataObject="d_stop_comments_add"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event ue_postconstructor;call super::ue_postconstructor;ib_updateable = TRUE
end event

