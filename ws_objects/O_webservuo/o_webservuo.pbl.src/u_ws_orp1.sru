﻿$PBExportHeader$u_ws_orp1.sru
forward
global type u_ws_orp1 from u_orp_webservice
end type
end forward

global type u_ws_orp1 from u_orp_webservice
end type
global u_ws_orp1 u_ws_orp1

forward prototypes
public function integer nf_orpo06fr (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_orpo07fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo08fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, string as_header_string)
public function integer nf_orpo09fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo10fr (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo11fr (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo12fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo13fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo14fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo15fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo17fr (ref s_error astr_error_info, string as_input_string, ref string as_output_header_string, ref string as_output_detail_string)
public function integer nf_orpo18fr (ref s_error astr_error_info, ref string as_required_info, ref string as_ship_date)
public function integer nf_orpo25fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo19fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo20fr (ref s_error astr_error_info, string as_input_string, string as_output_string_in)
public function integer nf_orpo23fr (ref s_error astr_error_info, string as_input_string, ref string as_output_header_string, ref string as_output_detail_string)
public function integer nf_orpo24fr (ref s_error astr_error_info, string as_input_string, string as_header_string_in, ref string as_header_string_out, string as_detail_string_in, ref string as_detail_string_out)
public function integer nf_orpo05fr (ref s_error astr_error_info, ref string ls_order_id, ref string ls_load_status, ref string ls_order_status, ref string ls_weight_error_flag, ref string ls_weight_override, ref string ls_net_order_flag, ref string ls_detail_data, ref integer li_shtg_line_count, ref string ls_send_to_sched_ind)
public function integer nf_orpo22fr (s_error astr_error_info, string as_to_id, string as_from_id, string as_option, string as_detail_in, ref string as_detail_out)
public function integer nf_orpo42fr (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref integer ai_pagenumber)
end prototypes

public function integer nf_orpo06fr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO06FR'
ls_tran_id = 'O06F'


ls_input_string = ''

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)



gw_netwise_frame.SetMicroHelp( "Ready")
return li_rval

end function

public function integer nf_orpo07fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO07FR'
ls_tran_id = 'O07F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)



gw_netwise_frame.SetMicroHelp( "Ready")
return li_rval

end function

public function integer nf_orpo08fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO08FR'
ls_tran_id = 'O08F'


ls_input_string =  as_header_string   + as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo09fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO09FR'
ls_tran_id = 'O09F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo10fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO10FR'
ls_tran_id = 'O10F'


ls_input_string = as_input_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo11fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO11FR'
ls_tran_id = 'O11F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo12fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO12FR'
ls_tran_id = 'O12F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo13fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO13FR'
ls_tran_id = 'O13F'


ls_input_string =as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)


gw_netwise_frame.SetMicroHelp( "Ready")
return li_rval

end function

public function integer nf_orpo14fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO14FR'
ls_tran_id = 'O14F'


ls_input_string =as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo15fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO15FR'
ls_tran_id = 'O15F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo17fr (ref s_error astr_error_info, string as_input_string, ref string as_output_header_string, ref string as_output_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO17FR'
ls_tran_id = 'O17F'


ls_input_string =as_input_string




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_output_header_string += ls_output
			Case 'D'
				as_output_detail_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function integer nf_orpo18fr (ref s_error astr_error_info, ref string as_required_info, ref string as_ship_date);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO18FR'
ls_tran_id = 'O18F'


ls_input_string = as_required_info

li_rval = uf_rpccall(ls_input_string,as_ship_date, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval

end function

public function integer nf_orpo25fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO25FR'
ls_tran_id = 'O25F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo19fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO19FR'
ls_tran_id = 'O19F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo20fr (ref s_error astr_error_info, string as_input_string, string as_output_string_in);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO20FR'
ls_tran_id = 'O20F'


ls_input_string = as_input_string + '~h7F'+as_output_string_in

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo23fr (ref s_error astr_error_info, string as_input_string, ref string as_output_header_string, ref string as_output_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO23FR'
ls_tran_id = 'O23F'


ls_input_string = as_input_string




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_output_header_string += ls_output
			Case 'D'
				as_output_detail_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function integer nf_orpo24fr (ref s_error astr_error_info, string as_input_string, string as_header_string_in, ref string as_header_string_out, string as_detail_string_in, ref string as_detail_string_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO24FR'
ls_tran_id = 'O24F'


ls_input_string = as_input_string +'~h7F' + as_header_string_in +'~h7F' + as_detail_string_in




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_string_out += ls_output
			Case 'D'
				as_detail_string_out += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function integer nf_orpo05fr (ref s_error astr_error_info, ref string ls_order_id, ref string ls_load_status, ref string ls_order_status, ref string ls_weight_error_flag, ref string ls_weight_override, ref string ls_net_order_flag, ref string ls_detail_data, ref integer li_shtg_line_count, ref string ls_send_to_sched_ind);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, as_output_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO05FR'
ls_tran_id = 'O05F'


ls_input_string = ls_order_id + '~t' + ls_load_status + '~t' + ls_order_status + '~t' + ls_weight_error_flag + '~t' + ls_weight_override + '~t' + &
                                ls_net_order_flag + '~t' + string(li_shtg_line_count) + '~t' + ls_send_to_sched_ind +'~t' + ls_detail_data 


li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

u_string_functions          lu_string_functions

ls_order_id         = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_load_status   = lu_string_functions.nf_gettoken(as_output_string,'~t')
ls_order_status = lu_string_functions.nf_gettoken(as_output_string,'~t')
ls_weight_error_flag = lu_string_functions.nf_gettoken(as_output_string,'~t')
ls_weight_override = lu_string_functions.nf_gettoken(as_output_string,'~t')
ls_net_order_flag = lu_string_functions.nf_gettoken(as_output_string,'~t')
li_shtg_line_count = integer(lu_string_functions.nf_gettoken(as_output_string,'~t'))
ls_send_to_sched_ind = lu_string_functions.nf_gettoken(as_output_string,'~t')
ls_detail_data = as_output_string




return li_rval

end function

public function integer nf_orpo22fr (s_error astr_error_info, string as_to_id, string as_from_id, string as_option, string as_detail_in, ref string as_detail_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring,ls_option
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO22FR'
ls_tran_id = 'O22F'





ls_input_string = as_to_id +'~h7F' + as_from_id +'~h7F' +as_option+'~h7F'+as_detail_in

li_rval = uf_rpccall(ls_input_string,as_detail_out, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo42fr (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref integer ai_pagenumber);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header, ls_detail, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO42FR'
ls_tran_id = 'O42F'


ls_input_string = as_header_info +'~h7F'+ as_detail_info




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header += ls_output
			Case 'D'
				ls_detail += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_header_info = ls_header
as_detail_info = ls_detail

return li_rval

end function

on u_ws_orp1.create
call super::create
end on

on u_ws_orp1.destroy
call super::destroy
end on

