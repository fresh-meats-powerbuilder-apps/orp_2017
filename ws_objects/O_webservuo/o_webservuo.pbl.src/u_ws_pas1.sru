﻿$PBExportHeader$u_ws_pas1.sru
forward
global type u_ws_pas1 from u_pas_webservice
end type
end forward

global type u_ws_pas1 from u_pas_webservice
end type
global u_ws_pas1 u_ws_pas1

forward prototypes
public function boolean nf_pasp01fr (ref s_error astr_error_info, ref string as_tpasfab)
public function boolean nf_pasp02fr (ref s_error astr_error_info, string as_header_string, ref string as_product_string, ref string as_tree)
public function boolean nf_pasp04fr (ref s_error astr_error_info, string as_header_string, ref string as_fab_product_descr, ref string as_fab_group, ref string as_mfg_step_string)
public function boolean nf_pasp05fr (ref s_error astr_error_info, string as_header_string, ref integer ai_shift_duration, ref string as_step_type, ref string as_output_string)
public function boolean nf_pasp06fr (ref s_error astr_error_info, string as_header_string, string as_detail_string, ref integer ai_new_step_sequence)
public function boolean nf_pasp07fr (ref s_error astr_error_info, string as_header_string, ref string as_fab_group, ref string as_plant_string, ref string as_char_string)
public function boolean nf_pasp08fr (ref s_error astr_error_info, string as_header_string, string as_plant_string, string as_char_string)
public function boolean nf_pasp14fr (ref s_error astr_error_info, string as_input_string, ref string ac_product_type, ref string as_fab_product_descr, ref string as_characteristic_string)
public function boolean nf_pasp15fr (ref s_error astr_error_info, string as_input, ref string as_mfg_steps, ref string as_instances, ref string as_parameters, ref string as_auto_conv, ref string as_plants, ref string as_piece_count, ref string as_timestamp_string, ref string as_ratios)
public function integer nf_pasp16fr (ref s_error astr_error_info, string as_header, string as_instances, ref string as_timestamp)
public function boolean nf_pasp25fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp26fr (ref s_error astr_error_info, string as_header, string as_weight_dist)
public function boolean nf_pasp09fr (ref s_error astr_error_info, string as_input, ref string as_source)
public function boolean nf_pasp10fr (ref s_error astr_error_info, string as_input, ref datawindow adw_source)
public function boolean nf_pasp11fr (ref s_error astr_error_info, string as_source)
public function boolean nf_pasp12fr (ref s_error astr_error_info, string as_input, ref string as_product, ref string as_inventory)
public function boolean nf_pasp13fr (ref s_error astr_error_info, string as_header, string as_detail)
public function integer nf_pasp00fr (ref s_error astr_error_info, string ac_fab_product_code, character ac_product_state, ref string ac_projections_exist, ref string ac_product_status, ref string as_tpasfab_string)
public function boolean nf_pasp03fr (ref s_error astr_error_info, string ac_fab_product_code, ref string as_fab_product_descr)
public function boolean nf_pasp20fr (ref s_error astr_error_info, string as_input, ref string as_transmission_complete, ref string as_inventory_string)
public function boolean nf_pasp23fr (ref s_error astr_error_info, string as_input, ref string as_message)
public function integer nf_pasp29fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp30fr (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp33fr (ref s_error astr_error_info, string as_input, ref string as_plant_constraint)
public function boolean nf_pasp34fr (ref s_error astr_error_info, string as_header, string as_plant_constraint)
public function boolean nf_pasp36fr (ref s_error astr_error_info, string as_input, ref string as_end_of_shift, ref string as_man_prod)
public function boolean nf_pasp37fr (ref s_error astr_error_info, string as_header, string as_detail)
public function boolean nf_pasp38fr (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp51fr (ref string as_header_in, ref s_error astr_error_info, ref datawindow adw_target)
public function integer nf_pasp54fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp21fr (ref s_error astr_error_info, string as_inventory)
public function boolean nf_pasp18fr (ref s_error astr_error_info, string as_input, ref datawindow adw_projections)
public function boolean nf_pasp35fr (ref s_error astr_error_info, string as_input, ref datawindow adw_prod_var)
public function integer nf_pasp55fr (ref s_error astr_error_info, string as_input_header, string as_input_detail)
public function boolean nf_pasp65gr (ref s_error astr_error_info, string as_update_string, string as_header_string)
public function integer nf_pasp66gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp17fr (ref s_error astr_error_info, ref string as_input, ref string as_output_values, ref string as_qty_by_age_code, ref string as_shift_production, ref string as_sold_qty_by_age, ref string as_sold_qty_by_type, ref string as_status_info, ref string as_weekly_totals, ref string as_epa_pt_info, ref string as_qname)
public function boolean nf_pasp24fr (ref s_error astr_error_info, string as_input_string, ref string as_output, string as_qname)
public function integer nf_pasp12gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_plants)
public function boolean nf_pasp03fr (ref s_error astr_error_info, string as_fab_product_code, string as_product_state, ref string as_fab_product_descr, ref string as_valid_product_states)
public subroutine nf_write_benchmark (time at_starttime, time at_endtime, string as_program_name)
end prototypes

public function boolean nf_pasp01fr (ref s_error astr_error_info, ref string as_tpasfab);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP01FR'
ls_tran_id = 'P01F'


ls_input_string = as_tpasfab

li_rval = uf_rpcupd(ls_input_string,astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then

return true

else
	return false
end if

end function

public function boolean nf_pasp02fr (ref s_error astr_error_info, string as_header_string, ref string as_product_string, ref string as_tree);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP02FR'
ls_tran_id = 'P02F'


ls_input_string = as_header_string +'~h7F'+as_product_string




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)


	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'P'
				as_product_string += ls_output
			Case 'T'
				as_tree += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop


if li_rval = 0 then

	return true

else
	return false
end if
end function

public function boolean nf_pasp04fr (ref s_error astr_error_info, string as_header_string, ref string as_fab_product_descr, ref string as_fab_group, ref string as_mfg_step_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP04FR'
ls_tran_id = 'P04F'


ls_input_string = as_header_string+'~h7f'+as_fab_product_descr+'~h7f'+as_fab_group




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7f')
	
		Choose Case ls_StringInd
			Case 'D'
				as_fab_product_descr += ls_output
			Case 'G'
				as_fab_group += ls_output
			Case 'M'
				as_mfg_step_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
as_fab_group = Trim(as_fab_group)


if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp05fr (ref s_error astr_error_info, string as_header_string, ref integer ai_shift_duration, ref string as_step_type, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP05FR'
ls_tran_id = 'P05F'


ls_input_string = as_header_string +'~h7F'+ string(ai_shift_duration)+'~h7F'+as_step_type




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'D'
				ai_shift_duration = Integer(ls_output)
			Case 'T'
				as_step_type += ls_output
			Case 'P'
		     	as_output_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
If li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp06fr (ref s_error astr_error_info, string as_header_string, string as_detail_string, ref integer ai_new_step_sequence);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP06FR'
ls_tran_id = 'P06F'


ls_input_string = as_header_string +'~h7F'+as_detail_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ai_new_step_sequence = integer(ls_outputstring)


if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp07fr (ref s_error astr_error_info, string as_header_string, ref string as_fab_group, ref string as_plant_string, ref string as_char_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP07FR'
ls_tran_id = 'P07F'


ls_input_string = as_header_string +'~h7F'+as_fab_group+'~h7F'+as_plant_string+'~h7F'+as_char_string




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'G'
				as_fab_group += ls_output
			Case 'P'
				as_plant_string += ls_output
			Case 'C'
				as_char_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_fab_group = Trim(as_fab_group)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp08fr (ref s_error astr_error_info, string as_header_string, string as_plant_string, string as_char_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP08FR'
ls_tran_id = 'P08F'


ls_input_string = as_header_string+'~h7F'+as_plant_string+'~h7F'+as_char_string+'~t'

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp14fr (ref s_error astr_error_info, string as_input_string, ref string ac_product_type, ref string as_fab_product_descr, ref string as_characteristic_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval

Time							lt_starttime, &
								lt_endtime
								
u_String_Functions  lu_string

ls_program_name= 'PASP14FR'
ls_tran_id = 'P14F'


ls_input_string =as_input_string +'~h7F' +ac_product_type+'~h7F' +as_fab_product_descr+'~h7F' +as_characteristic_string

lt_starttime = Now()

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_program_name)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'T'
				ac_product_type += ls_output
			Case 'P'
				as_fab_product_descr += ls_output
			Case 'C'
				as_characteristic_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
ac_product_type = Trim(ac_product_type)
if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp15fr (ref s_error astr_error_info, string as_input, ref string as_mfg_steps, ref string as_instances, ref string as_parameters, ref string as_auto_conv, ref string as_plants, ref string as_piece_count, ref string as_timestamp_string, ref string as_ratios);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,&
		ls_plants, &
		ls_piece_count, &
		ls_timestamp_string
integer li_rval, li_version_number
Boolean					lb_first_time

lb_first_time = true
u_String_Functions  lu_string

ls_program_name= 'PASP15FR'
ls_tran_id = 'P15F'


ls_input_string = as_input
as_timestamp_string = ""

li_version_number = 1


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, li_version_number)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'S'
				as_mfg_steps += ls_output
			Case 'I'
				as_instances += ls_output
			Case 'P'
				as_parameters += ls_output
			Case 'A'
				as_auto_conv += ls_output
			Case 'V'
				as_plants += ls_output
			Case 'E'
				as_piece_count += ls_output
			Case 'T'
				as_timestamp_string += ls_output
			Case 'R'
				as_ratios += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp16fr (ref s_error astr_error_info, string as_header, string as_instances, ref string as_timestamp);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring,header,instances,timestamp
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP16FR'
ls_tran_id = 'P16F'

//replace(as_header,'~r~n',header)
//replace(as_instances,'~r~n',instances)
//replace(as_timestamp,'~r~n',timestamp)


ls_input_string = as_header +'~h7F' +as_instances +'~h7F'+as_timestamp






li_rval = uf_rpccall(ls_input_string,as_timestamp, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp25fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP25FR'
ls_tran_id = 'P25F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp26fr (ref s_error astr_error_info, string as_header, string as_weight_dist);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP26FR'
ls_tran_id = 'P26F'


ls_input_string = as_header+'~h7F'+as_weight_dist

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp09fr (ref s_error astr_error_info, string as_input, ref string as_source);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP09FR'
ls_tran_id = 'P09F'


ls_input_string = as_input

li_rval = uf_rpccall(ls_input_string,as_source, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp10fr (ref s_error astr_error_info, string as_input, ref datawindow adw_source);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP10FR'
ls_tran_id = 'P10F'


ls_input_string =as_input

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	adw_source.Reset()
	adw_source.ImportString(ls_outputstring)
end if

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp11fr (ref s_error astr_error_info, string as_source);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP11FR'
ls_tran_id = 'P11F'


ls_input_string = as_source

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp12fr (ref s_error astr_error_info, string as_input, ref string as_product, ref string as_inventory);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP12FR'
ls_tran_id = 'P12F'


ls_input_string = as_input




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'P'
				as_product += ls_output
			Case 'I'
				as_inventory += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
as_product = Trim(as_product)
as_inventory = Trim(as_inventory)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp13fr (ref s_error astr_error_info, string as_header, string as_detail);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP13FR'
ls_tran_id = 'P13F'


ls_input_string = as_header+'~h7F'+as_detail

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp00fr (ref s_error astr_error_info, string ac_fab_product_code, character ac_product_state, ref string ac_projections_exist, ref string ac_product_status, ref string as_tpasfab_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP00FR'
ls_tran_id = 'P00F'


ls_input_string =ac_fab_product_code +'~h7F' + string(ac_product_state) +'~h7F' + String(ac_projections_exist) +'~h7F' + String(ac_product_status) 

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
as_tpasfab_string = Trim(as_tpasfab_string)
if  li_rval = 0 then
	
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring,'~h7F')
	
		Choose Case ls_StringInd
			Case 'S'
				ac_product_status += ls_output
			Case 'D'
				as_tpasfab_string += ls_output
				Case 'E'
				ac_projections_exist += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
ac_projections_exist = Trim(ac_projections_exist)
return li_rval


end function

public function boolean nf_pasp03fr (ref s_error astr_error_info, string ac_fab_product_code, ref string as_fab_product_descr);string ls_valid_product_states, ls_product_state

ls_product_state = " "

return nf_pasp03fr(astr_error_info,ac_fab_product_code,ls_product_state ,as_fab_product_descr,ls_valid_product_states)
end function

public function boolean nf_pasp20fr (ref s_error astr_error_info, string as_input, ref string as_transmission_complete, ref string as_inventory_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP20FR'
ls_tran_id = 'P20F'


ls_input_string = as_input




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'T'
				as_transmission_complete += ls_output
			Case 'O'
				as_inventory_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
as_transmission_complete = Trim(as_transmission_complete)
if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp23fr (ref s_error astr_error_info, string as_input, ref string as_message);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP23FR'
ls_tran_id = 'P23F'


ls_input_string = as_input

li_rval = uf_rpccall(ls_input_string,as_message, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp29fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP29FR'
ls_tran_id = 'P29F'


ls_input_string = as_input_string
li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp30fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP30FR'
ls_tran_id = 'P30F'


ls_input_string = as_input_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return true

end function

public function boolean nf_pasp33fr (ref s_error astr_error_info, string as_input, ref string as_plant_constraint);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP33FR'
ls_tran_id = 'P33F'


ls_input_string = as_input

li_rval = uf_rpccall(ls_input_string,as_plant_constraint, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp34fr (ref s_error astr_error_info, string as_header, string as_plant_constraint);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP34FR'
ls_tran_id = 'P34F'


ls_input_string = as_header +'~h7F'+ as_plant_constraint

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp36fr (ref s_error astr_error_info, string as_input, ref string as_end_of_shift, ref string as_man_prod);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP36FR'
ls_tran_id = 'P36F'


ls_input_string = as_input




li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'S'
				as_end_of_shift += ls_output
			Case 'O'
				as_man_prod += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp37fr (ref s_error astr_error_info, string as_header, string as_detail);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval
boolean lb_rtn


u_String_Functions  lu_string

ls_program_name= 'PASP37FR'
ls_tran_id = 'P37F'


ls_input_string = as_header +'~h7f'+as_detail

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

This.ii_messagebox_rtn = 1
lb_rtn = uf_display_message_ws(li_rval, astr_error_info, 0)
If This.ii_messagebox_rtn = 4 Then return False
If This.ii_messagebox_rtn = 1 Then return True
return lb_rtn 
end function

public function boolean nf_pasp38fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP38FR'
ls_tran_id = 'P38F'


ls_input_string = as_input_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp51fr (ref string as_header_in, ref s_error astr_error_info, ref datawindow adw_target);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_data_out
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP51FR'
ls_tran_id = 'P51F'


ls_input_string = as_header_in




li_rval = uf_rpccall(ls_input_string,ls_data_out, astr_error_info, ls_program_name, ls_tran_id)

//if  li_rval = 0 then
//	ls_stringInd = Mid(ls_outputstring, 2, 1)
//	ls_outputstring = Mid(ls_outputstring, 3)
//
//	Do While Len(Trim(ls_outputstring)) > 0
//		ls_output = lu_string.nf_GetToken(ls_outputstring, '>')
//	
//		Choose Case ls_StringInd
//			Case 'H'
//				as_header_in += ls_output
//			Case 'D'
//				ls_data_out += ls_output
//
//		End Choose
//		ls_stringInd = Left(ls_OutputString, 1)
//		ls_outputstring = Mid(ls_OutputString, 2)
//	Loop
//	
//end if	
if li_rval = 0 then
	adw_target.ImportString(ls_data_out)
	return true
else
	return false
end if

end function

public function integer nf_pasp54fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring,ls_sold,ls_buffer,ls_stringInd,ls_output
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP54FR'
ls_tran_id = 'P54F'


ls_input_string = as_input_string



li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'S'
				ls_sold += ls_output
			Case 'B'
				ls_buffer += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

ls_sold = Trim(ls_sold)
as_output_string = ls_sold + 	ls_buffer

return li_rval

end function

public function boolean nf_pasp21fr (ref s_error astr_error_info, string as_inventory);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP21FR'
ls_tran_id = 'P21F'


ls_input_string = as_inventory

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp18fr (ref s_error astr_error_info, string as_input, ref datawindow adw_projections);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP18FR'
ls_tran_id = 'P18F'


ls_input_string =as_input

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	adw_projections.Reset()
	adw_projections.ImportString(ls_outputstring)
	return true
else
	return false
end if

end function

public function boolean nf_pasp35fr (ref s_error astr_error_info, string as_input, ref datawindow adw_prod_var);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP35FR'
ls_tran_id = 'P35F'


ls_input_string = as_input


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	adw_prod_var.Reset()
	adw_prod_var.ImportString(ls_outputstring)
	return true
else
	return false
end if

end function

public function integer nf_pasp55fr (ref s_error astr_error_info, string as_input_header, string as_input_detail);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP55FR'
ls_tran_id = 'P55F'


ls_input_string = as_input_header+'~h7F'+as_input_detail

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp65gr (ref s_error astr_error_info, string as_update_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP65GR'
ls_tran_id = 'P65G'


ls_input_string = as_update_string + '~h7F'+ as_header_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp66gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP66GR'
ls_tran_id = 'P66G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp17fr (ref s_error astr_error_info, ref string as_input, ref string as_output_values, ref string as_qty_by_age_code, ref string as_shift_production, ref string as_sold_qty_by_age, ref string as_sold_qty_by_type, ref string as_status_info, ref string as_weekly_totals, ref string as_epa_pt_info, ref string as_qname);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_data_out,ls_dates,ls_task
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP17FR'
ls_tran_id = 'P17F'


ls_input_string = as_input

as_qname = ''
as_output_values = ''
as_qty_by_age_code = ''
as_shift_production = ''
as_sold_qty_by_age = ''
as_sold_qty_by_type =''
as_status_info = ''
as_weekly_totals = ''
as_epa_pt_info = ''
ls_dates = ''


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'O'
				as_output_values += ls_output
			Case 'C'
				as_qty_by_age_code += ls_output
			Case 'P'
				as_shift_production += ls_output
			Case 'A'
				as_sold_qty_by_age += ls_output
			Case 'T'
				as_sold_qty_by_type += ls_output
			Case 'S'
				as_status_info += ls_output
			Case 'W'
				as_weekly_totals += ls_output
			Case 'E'
				as_epa_pt_info += ls_output
	         Case 'I'
				ls_dates += ls_output
			Case 'X'
				as_qname = ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
	
end if	
as_output_values = TRIM(as_output_values)
as_qty_by_age_code = TRIM(as_qty_by_age_code)
as_shift_production = TRIM(as_shift_production)
as_sold_qty_by_age = TRIM(as_sold_qty_by_age)
as_sold_qty_by_type = TRIM(as_sold_qty_by_type)
as_status_info = TRIM(as_status_info)
as_weekly_totals = TRIM(as_weekly_totals)
as_epa_pt_info = TRIM(as_epa_pt_info)
as_qname = Trim(as_qname)
as_input = ls_dates

return true 
//if li_rval = 0 then
//	return true
//else
//	return false
//end if
end function

public function boolean nf_pasp24fr (ref s_error astr_error_info, string as_input_string, ref string as_output, string as_qname);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP24FR'
ls_tran_id = 'P24F'
as_input_string = Trim(as_input_string)

ls_input_string = as_input_string + '~h7F'+as_qname

li_rval = uf_rpccall(ls_input_string,as_output, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp12gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_plants);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP12GR'
ls_tran_id = 'P12G'


ls_input_string = as_input_string

as_plants = ""


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'O'
				as_output_string += ls_output
			Case 'E'		
				as_plants += Trim(ls_output)

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function boolean nf_pasp03fr (ref s_error astr_error_info, string as_fab_product_code, string as_product_state, ref string as_fab_product_descr, ref string as_valid_product_states);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval

Time							lt_starttime, &
								lt_endtime

u_String_Functions  lu_string

ls_program_name= 'PASP03FR'
ls_tran_id = 'P03F'


ls_input_string = as_fab_product_code +'~h7F'+as_product_state+'~h7F'+as_fab_product_descr+'~h7F'+as_valid_product_states

lt_starttime = Now()

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_program_name)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'D'
				as_fab_product_descr += ls_output
			Case 'V'
				as_valid_product_states += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
IF li_rval = 0 then
	return true
else
	return false
end if

end function

public subroutine nf_write_benchmark (time at_starttime, time at_endtime, string as_program_name);Integer		li_TimeVal, li_fileNum 

Long			ll_SecondsAfter

String		ls_path, ls_FileName 

li_TimeVal = ProfileInt(gw_netwise_frame.is_WorkingDir + "ibp002.ini","BenchMark", Message.nf_Get_App_ID(),0)

ll_SecondsAfter = SecondsAfter(at_starttime,at_endtime) 

IF (li_TimeVal = 0) or (ll_SecondsAfter >= li_TimeVal) Then 
	ls_path = "C:\WSlog\"
	ls_fileName = ls_path + "BM" + String(Month(Today()), '00') + String(Day(Today()), '00') + ".log"
	li_FileNum = FileOpen(ls_FileName,  lineMode!, Write!, LockWrite! , Append! )
	
	If li_FileNum >= 0 then
	
		FileWrite(li_FileNum,Message.nf_Get_App_ID() + &
					"~tUser:~t" + SQLCA.Userid + &
					'~t' + Trim(as_program_name) + &
					"~tStart:~t" + String(At_StartTime, "hh:mm:ss.ffffff") + &
					"~tEnd:~t" + String(At_EndTime, "hh:mm:ss.ffffff") + &
					"~tElapsed Seconds:~t" + String(ll_SecondsAfter))
					
		FileClose(li_FileNum)
	End If
End If
end subroutine

on u_ws_pas1.create
call super::create
end on

on u_ws_pas1.destroy
call super::destroy
end on

