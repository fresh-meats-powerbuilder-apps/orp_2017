﻿$PBExportHeader$u_ws_otr_appointments.sru
forward
global type u_ws_otr_appointments from u_otr_webservice
end type
end forward

global type u_ws_otr_appointments from u_otr_webservice
end type
global u_ws_otr_appointments u_ws_otr_appointments

forward prototypes
public function integer uf_otrt23er (string as_customer_from_char, string as_customer_to_char, string as_division, ref string as_appt_except_inq_string, ref s_error astr_error_info, string as_complex, string as_plant, string as_carrier)
public function integer uf_otrt18er (string as_carrier_code, string as_from_ship_date, string as_to_ship_date, string as_load_key, string as_date_ind, string as_division, ref string as_appt_update_string, ref s_error astr_error_info)
public function integer uf_otrt37er (ref string as_tdiscntl_inq_upd_string, ref s_error astr_error_info)
public function integer uf_otrt40er (string as_req_carrier_start, string as_req_carrier_end, string as_req_delv_from_date, string as_req_delv_to_date, string as_req_date_ind, ref string as_carr_no_appt_string, s_error astr_error_info)
public function integer uf_otrt77er (ref s_error s_error_info, string as_input_string, ref string as_output_string)
public function integer uf_otrt78er (s_error s_error_info, string as_input_header_string, string as_input_detail_string, ref string as_output_string)
public function integer uf_otrt19er (string as_load_key, string as_stop_code, string as_appt_contact, string as_appt_date, string as_appt_time, string as_eta_date, string as_eta_time, string as_override_ind, string as_notify_carrier_ind, ref s_error astr_error_info)
end prototypes

public function integer uf_otrt23er (string as_customer_from_char, string as_customer_to_char, string as_division, ref string as_appt_except_inq_string, ref s_error astr_error_info, string as_complex, string as_plant, string as_carrier);string ls_program_name, ls_tran_id, ls_input_string

integer li_rval

u_String_Functions lu_string

ls_program_name = 'OTRT23ER'
ls_tran_id = 'T23E'

ls_input_string = as_customer_from_char + '~t' + as_customer_to_char + '~t' + as_division 

li_rval = uf_rpccall(ls_input_string,as_appt_except_inq_string, astr_error_info, ls_program_name, ls_tran_id)



return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

end function

public function integer uf_otrt18er (string as_carrier_code, string as_from_ship_date, string as_to_ship_date, string as_load_key, string as_date_ind, string as_division, ref string as_appt_update_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'OTRT18ER'
ls_tran_id = 'T18E'

ls_input_string = as_carrier_code + '~t' + as_from_ship_date + '~t' + as_to_ship_date + '~t' + as_load_key + '~t' + as_date_ind + '~t' + as_division

li_rval = uf_rpccall(ls_input_string, as_appt_update_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

end function

public function integer uf_otrt37er (ref string as_tdiscntl_inq_upd_string, ref s_error astr_error_info);string ls_input_string, ls_program_name, ls_tran_id, ls_output_string
integer li_rval

ls_program_name= 'OTRT37ER'
ls_tran_id = 'T37E'

li_rval = uf_rpccall(as_tdiscntl_inq_upd_string,ls_output_string, astr_error_info, ls_program_name, ls_tran_id)

//li_rval = uf_rpcupd(as_tdiscntl_inq_upd_string, astr_error_info, ls_program_name, ls_tran_id)


if li_rval = 0 then
	as_tdiscntl_inq_upd_string = ls_output_string
end if





RETURN li_rval
end function

public function integer uf_otrt40er (string as_req_carrier_start, string as_req_carrier_end, string as_req_delv_from_date, string as_req_delv_to_date, string as_req_date_ind, ref string as_carr_no_appt_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'OTRT40ER'
ls_tran_id = 'T40E'

ls_input_string = as_req_carrier_start + '~t' + as_req_carrier_end + '~t' + as_req_delv_from_date + '~t' + as_req_delv_to_date + '~t' + as_req_date_ind

li_rval = uf_rpccall(ls_input_string, as_carr_no_appt_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_otrt77er (ref s_error s_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_output_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'OTRT77ER'
ls_tran_id = 'T77E'

li_rval = uf_rpccall(as_input_string, as_output_string, s_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer uf_otrt78er (s_error s_error_info, string as_input_header_string, string as_input_detail_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions  lu_string

ls_program_name = 'OTRT78ER'
ls_tran_id = 'T78E'

ls_input_string = as_input_header_string + as_input_detail_string

li_rval = uf_rpcupd(ls_input_string, s_error_info, ls_program_name, ls_tran_id)



return li_rval
end function

public function integer uf_otrt19er (string as_load_key, string as_stop_code, string as_appt_contact, string as_appt_date, string as_appt_time, string as_eta_date, string as_eta_time, string as_override_ind, string as_notify_carrier_ind, ref s_error astr_error_info);string ls_input_string, ls_program_name, ls_tran_id
integer li_rval

ls_program_name = 'OTRT19ER'
ls_tran_id = 'T19E'

ls_input_string = as_load_key + '~t' + as_stop_code + '~t' + as_appt_contact + '~t' + as_appt_date + '~t' + as_appt_time + '~t' + as_eta_date + '~t' + as_eta_time + '~t' + as_override_ind + '~t' + as_notify_carrier_ind

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

on u_ws_otr_appointments.create
call super::create
end on

on u_ws_otr_appointments.destroy
call super::destroy
end on

