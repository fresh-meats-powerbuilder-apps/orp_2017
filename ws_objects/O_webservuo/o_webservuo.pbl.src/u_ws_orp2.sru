﻿$PBExportHeader$u_ws_orp2.sru
forward
global type u_ws_orp2 from u_orp_webservice
end type
end forward

global type u_ws_orp2 from u_orp_webservice
end type
global u_ws_orp2 u_ws_orp2

forward prototypes
public function integer nf_orpo26fr (string as_header_string, string as_input_string, string as_output_string, ref s_error astr_error_info)
public function integer nf_orpo29fr (string as_header_string_in, string as_detail_string_in, ref string as_detail_string_out, ref s_error astr_error_info)
public function integer nf_orpo28fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_orpo30fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_orpo31fr (string as_input_string, s_error astr_error_info)
public function integer nf_orpo37fr (ref string as_instruction_key, string as_perm_ins_in, ref string as_perm_out, ref s_error astr_error_info)
public function integer nf_orpo43fr (ref string as_header_string, ref string as_detail_string, ref s_error astr_error_info)
public function integer nf_orpo44fr (string as_input_info_in, string as_main_info_in, character ac_ind_process_option, ref string as_main_info_out, ref string as_so_info_out, ref string as_age_info_out, ref string as_production_dates, double ad_task_number_in, double ad_page_number_in, ref double ad_task_number_out, ref double ad_page_number_out, string as_window_ind, ref s_error astr_error_info)
public function integer nf_orpo45fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_orpo47fr (ref string as_header_info, ref string as_detail_info, string as_shortage_lines, character ac_ind, ref s_error astr_error_info)
public function integer nf_orpo48fr (ref string as_header_info, ref string as_detail_info, character ac_ind, ref s_error astr_error_info)
public function integer nf_orpo41fr (string as_userid, ref string as_sales_person_info_string, ref s_error astr_error_info)
public function integer nf_orpo35fr (ref string as_sales_id, ref character ac_auto_resolve, string as_shortage_lines, ref s_error astr_error_info)
public function integer nf_orpo36fr (ref string as_header_string, ref string as_detail_string, ref string as_so_id_string, ref s_error astr_error_info)
public function integer nf_orpo38fr (character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out, string as_customer_id, string as_date_type_ind, string as_detail_astring, ref string as_prod_data, ref s_error astr_error_info)
public function integer nf_orpo39fr (string ac_calling_window_ind, ref string as_rescus_header, ref string as_rescus_detail, ref string as_additional_data, string as_res_info, ref s_error astr_error_info)
public function integer nf_orpo50fr (ref s_error astr_error_info, string as_from_id, ref string as_to_id, string as_to_plant, string as_to_ship_date, string as_detail, string as_additional_data, string as_shortage_string_in, ref string as_shortage_string_out, string as_dup_product_string)
public function boolean nf_cfmc01fr (string as_header, ref string as_inquire, string as_update, ref s_error astr_error_info)
public function boolean nf_cfmc05fr (string as_header, ref string as_inquire, string as_update, ref s_error astr_error_info)
public function boolean nf_cfmc09fr (string as_header, ref string as_inquire, string as_update, ref s_error astr_error_info)
public function boolean nf_cfmc25fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info)
public function boolean nf_cfmc27fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info)
public function boolean nf_cfmc28fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info)
public function integer nf_cfmc35fr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_cool_string, ref string as_prod_cat_string)
public function integer nf_cfmc29fr (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref string as_refetch_division, ref s_error astr_error_info)
public function integer nf_cfmc36fr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_cool_string, ref string as_prod_cat_string, ref string as_cust_excl_string)
public function boolean nf_cfmc31fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info)
public function integer nf_cfmc34fr (ref s_error astr_error_info, string as_header, string as_input, ref string as_output, ref string as_detail, ref string as_recorder_string)
public function integer nf_orpo33fr (ref string as_order_id, ref string as_detail_info, ref string as_weight_str, ref string as_res_reduce_in, ref string as_pa_resolve_out, string as_action_ind, ref string as_production_date, ref string as_dept_code, ref string as_dup_products_ind, ref string as_end_customers, ref s_error astr_error_info)
public function integer nf_orpo34fr (ref string as_customer_info, ref string as_weight_info, ref s_error astr_error_info, ref string as_program_info)
public function integer nf_orpo32fr (ref string as_header_info, character ac_action_ind, ref string as_weight_override_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country, ref s_error astr_error_info, string as_linked_orders_ind)
end prototypes

public function integer nf_orpo26fr (string as_header_string, string as_input_string, string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO26FR'
ls_tran_id ='O26F'

ls_input_string = as_header_string + as_input_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)



return li_rval
end function

public function integer nf_orpo29fr (string as_header_string_in, string as_detail_string_in, ref string as_detail_string_out, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO29FR'
ls_tran_id ='O29F'

ls_input_string = as_header_string_in +'~h7f'+ as_detail_string_in

li_rval = uf_rpccall(ls_input_string,as_detail_string_out, astr_error_info, ls_program_name, ls_tran_id)

//as_detail_string_out = ls_outputstring

return li_rval
end function

public function integer nf_orpo28fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO28FR'
ls_tran_id ='O28F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval
end function

public function integer nf_orpo30fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO30FR'
ls_tran_id ='O30F'

li_rval = uf_rpccall(as_input_string, as_output_string,astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo31fr (string as_input_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO31FR'
ls_tran_id ='O31F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo37fr (ref string as_instruction_key, string as_perm_ins_in, ref string as_perm_out, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_detail_info,ls_output, ls_stringind,ls_stringsep
integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO37FR'
ls_tran_id ='O37F'
as_instruction_key = Trim(as_instruction_key)
ls_input_string = as_instruction_key + '~h7f' + as_perm_ins_in 

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
as_instruction_key = TRIM(' ')

//UNSTRING OUTPUTSTRING 
If  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'P'
				as_instruction_key += Trim(ls_output)
			Case 'O'
				as_perm_out +=Trim(ls_output)
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

return li_rval
end function

public function integer nf_orpo43fr (ref string as_header_string, ref string as_detail_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_detail_info,ls_output, ls_header_string,ls_detail_string, ls_stringind,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO43FR'
ls_tran_id ='O43F'

ls_input_string = as_header_string + '~h7f' +  as_detail_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
//UNSTRING OUTPUTSTRING 
If  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header_string += ls_output
			Case 'D'
				ls_detail_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_header_string		=	ls_header_string
as_detail_string		=	ls_detail_string



return li_rval
end function

public function integer nf_orpo44fr (string as_input_info_in, string as_main_info_in, character ac_ind_process_option, ref string as_main_info_out, ref string as_so_info_out, ref string as_age_info_out, ref string as_production_dates, double ad_task_number_in, double ad_page_number_in, ref double ad_task_number_out, ref double ad_page_number_out, string as_window_ind, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_detail_info,ls_output, ls_string_ind,ls_output_string, ls_tsknum, ls_so_info_out, &
ls_stringInd, ls_main_info_out, ls_age_info_out,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO44FR'
ls_tran_id ='O44F'


ls_input_string = as_input_info_in +'~h7f' + as_main_info_in + '~h7f' + ac_ind_process_option + '~h7f' + as_window_ind +'~h7f'+ string(ad_task_number_in) &
					+'~h7f'+	string(ad_page_number_in) +'~h7f'

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
//unstring output-string
ls_string_ind = Mid(ls_outputstring, 2, 1)
	ls_output_string = Mid(ls_outputstring, 3)
		Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_stringInd
			Case 'S'
				ls_so_info_out += Trim(ls_output)
			Case 'M'
				ls_main_info_out += Trim(ls_output)
			Case 'A'
				ls_age_info_out += Trim(ls_output)
			Case 'T'
				ls_tsknum += Trim(ls_output)
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
	
	as_so_info_out = ls_so_info_out
	as_main_info_out = ls_main_info_out
	as_age_info_out = ls_age_info_out
	ad_task_number_out	   = Double (lu_string.nf_gettoken( ls_tsknum, '~t'))
	ad_page_number_out	   = Double (lu_string.nf_gettoken( ls_tsknum, '~t'))
		

return li_rval
end function

public function integer nf_orpo45fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO45FR'
ls_tran_id ='O45F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)


return li_rval
end function

public function integer nf_orpo47fr (ref string as_header_info, ref string as_detail_info, string as_shortage_lines, character ac_ind, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringInd, ls_output, ls_header_info, ls_detail_info,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO47FR'
ls_tran_id ='O47F'

ls_input_string = as_header_info +'~h7f'+ as_shortage_lines +'~h7f'+ ac_ind

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
//UNSTRING OUTPUTSTRING 
If  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header_info +=Trim(ls_output)
			Case 'D'
				ls_detail_info += Trim(ls_output)

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
 
 as_header_info = ''
 as_detail_info = ''
 as_header_info = Trim(ls_header_info)
 as_detail_info = Trim(ls_detail_info)


return li_rval
end function

public function integer nf_orpo48fr (ref string as_header_info, ref string as_detail_info, character ac_ind, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO48FR'
ls_tran_id ='O48F'

ls_input_string = as_header_info + '~h7f' + as_detail_info + '~h7f' + ac_ind

li_rval = uf_rpccall(ls_input_string, ls_outputstring,astr_error_info, ls_program_name, ls_tran_id)

as_detail_info=''
as_detail_info = Trim(ls_outputstring)

return li_rval
end function

public function integer nf_orpo41fr (string as_userid, ref string as_sales_person_info_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringInd, ls_output

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO41FR'
ls_tran_id ='O41F'

ls_input_string = as_userid 

li_rval = uf_rpccall(ls_input_string, as_sales_person_info_string, astr_error_info, ls_program_name, ls_tran_id)


return li_rval
end function

public function integer nf_orpo35fr (ref string as_sales_id, ref character ac_auto_resolve, string as_shortage_lines, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO35FR'
ls_tran_id ='O35F'

ls_input_string = as_sales_id + '~h7f' + ac_auto_resolve + '~h7f' + as_shortage_lines + '~h7f'

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_sales_id	= Trim(lu_string.nf_gettoken( ls_outputstring, '~h7f'))
ac_auto_resolve	= Trim(lu_string.nf_gettoken( ls_outputstring, '~h7f'))

return li_rval
end function

public function integer nf_orpo36fr (ref string as_header_string, ref string as_detail_string, ref string as_so_id_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_header_string_out,ls_detail_string_out,ls_so_id_string_out, ls_stringind, &
ls_output,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO36FR'
ls_tran_id ='O36F'

ls_input_string = as_header_string +'~h7f'+ as_detail_string +'~h7f'

li_rval = uf_rpccall(ls_input_string, ls_outputstring,astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)

//UNSTRING OUTPUTSTRING 

	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header_string_out += Trim(ls_output)
			Case 'D'
				ls_detail_string_out += Trim(ls_output)
			Case 'S'
				ls_so_id_string_out += Trim(ls_output)

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop


as_header_string		=	Trim(ls_header_string_out)
as_detail_string		=	Trim(ls_detail_string_out)
as_so_id_string         = Trim(ls_so_id_string_out)

return li_rval
end function

public function integer nf_orpo38fr (character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out, string as_customer_id, string as_date_type_ind, string as_detail_astring, ref string as_prod_data, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringInd, ls_output, ls_plant_data, ls_detail_data, ls_file_descr_out, &
ls_prod_data,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO38FR'
ls_tran_id ='O38F'

ls_input_string = ac_process_option +'~h7f' + ac_availability_date +'~h7f' + as_page_descr_in +'~h7f' + as_plant_data +'~h7f' + & 
as_detail_data +'~h7f' + as_file_descr_out +'~h7f' + as_customer_id +'~h7f' + as_date_type_ind +'~h7f'+ as_detail_astring  +'~h7f'+ as_prod_data +'~h7f'


li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)

//UNSTRING OUTPUTSTRING 

	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_stringInd
			Case 'P' 
				ls_prod_data += Trim(ls_output)
			Case '='   //  this rpc returns a string container a > character followed by = so put the = back in
				ls_prod_data += '>=' + Trim(ls_output)				
			Case 'I'
				ls_detail_data += Trim(ls_output)
			Case 'L'
				ls_plant_data += Trim(ls_output)
			Case 'V'
				ls_file_descr_out += Trim(ls_output)
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop

as_prod_data = ls_prod_data
as_detail_data = ls_detail_data
as_plant_data = ls_plant_data
as_file_descr_out = ls_file_descr_out

return li_rval
end function

public function integer nf_orpo39fr (string ac_calling_window_ind, ref string as_rescus_header, ref string as_rescus_detail, ref string as_additional_data, string as_res_info, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_rescus_header, ls_rescus_detail, ls_additional_data,ls_stringInd,ls_output,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO39FR'
ls_tran_id ='O39F'

ls_input_string = ac_calling_window_ind +'~h7f' + as_rescus_header +'~h7f' + as_rescus_detail +'>' + as_additional_data +'~h7f' + as_res_info +'~h7f' 

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)

//UNSTRING OUTPUTSTRING 
//If  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_stringInd
			Case 'H'
				ls_rescus_header += Trim(ls_output)
			Case 'D'
				ls_rescus_detail += Trim(ls_output)
			Case 'A'
				ls_additional_data += Trim(ls_output)
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if

as_rescus_header = ''
as_rescus_detail = ''
as_additional_data = ''

as_rescus_header = ls_rescus_header
as_rescus_detail = ls_rescus_detail 
as_additional_data = ls_additional_data


return li_rval
end function

public function integer nf_orpo50fr (ref s_error astr_error_info, string as_from_id, ref string as_to_id, string as_to_plant, string as_to_ship_date, string as_detail, string as_additional_data, string as_shortage_string_in, ref string as_shortage_string_out, string as_dup_product_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO50FR'
ls_tran_id = 'O50F'


//ls_input_string = as_from_id +'<' + as_to_id+'<' + as_to_plant+ '<'+ as_to_ship_date +'<'+ as_detail + '<'+ as_additional_data + '<'+ as_shortage_string_in + '<' + as_dup_product_string

ls_input_string= as_to_id +'~h7f'+ as_to_plant+'~h7f'+as_to_ship_date+'~h7f'+as_from_id+'~h7f'+as_detail+'~h7f'+as_additional_data+'~h7f'+as_shortage_string_in+'~h7f'+as_dup_product_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, 1)

if li_rval = 3 then
	as_shortage_string_out = ls_outputstring
else
	if len(ls_outputstring) = 5 then
		as_to_id = ls_outputstring
	end if
end if
	

return li_rval

end function

public function boolean nf_cfmc01fr (string as_header, ref string as_inquire, string as_update, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring,ls_stringsep

Boolean li_res

integer li_rval

u_String_Functions lu_string

ls_program_name='CFMC01FR'
ls_tran_id ='C01F'

ls_input_string = as_header +'~h7f'+ as_inquire +'~h7f'+ as_update +'~h7f'

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_inquire+= Trim(ls_outputstring)

If li_rval=0 then
	li_res=true
else
	li_res=false
end if

return li_res
end function

public function boolean nf_cfmc05fr (string as_header, ref string as_inquire, string as_update, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

Boolean ls_res

integer li_rval

u_String_Functions lu_string

ls_program_name='CFMC05FR'
ls_tran_id ='C05F'

ls_input_string = as_header +'~h7f'+ as_inquire +'~h7f'+ as_update +'~h7f'

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_inquire += Trim(ls_outputstring)

If li_rval = 0 Then
	ls_res=true
else
	ls_res = false
end if

return ls_res
end function

public function boolean nf_cfmc09fr (string as_header, ref string as_inquire, string as_update, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

Boolean ls_res

integer li_rval

u_String_Functions lu_string

ls_program_name='CFMC09FR'
ls_tran_id ='C09F'

ls_input_string = as_header +'~h7f'+ as_inquire +'~h7f'+ as_update +'~h7f'

li_rval = uf_rpccall(ls_input_string, ls_outputstring , astr_error_info, ls_program_name, ls_tran_id)
as_inquire = ''
as_inquire +=Trim(ls_outputstring)

iF li_rval = 0 Then
	ls_res=true
else
	ls_res=false
end if

return ls_res
end function

public function boolean nf_cfmc25fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

Boolean ls_res

integer li_rval

u_String_Functions lu_string

ls_program_name='CFMC25FR'
ls_tran_id ='C25F'

li_rval = uf_rpccall(as_inputstring, as_outputstring, astr_error_info, ls_program_name, ls_tran_id)


If li_rval = 0 Then
	ls_res=true
else
	ls_res = false
end if

return ls_res
end function

public function boolean nf_cfmc27fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

Boolean ls_res

integer li_rval

u_String_Functions lu_string

ls_program_name='CFMC27FR'
ls_tran_id ='C27F'

li_rval = uf_rpccall(as_inputstring, as_outputstring, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then
	ls_res=true
else
	ls_res = false
end if

return ls_res
end function

public function boolean nf_cfmc28fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

Boolean ls_res

integer li_rval

u_String_Functions lu_string

ls_program_name='CFMC28FR'
ls_tran_id ='C28F'

li_rval = uf_rpccall(as_inputstring, as_outputstring, astr_error_info, ls_program_name, ls_tran_id, 2)

If li_rval = 0 Then
	ls_res=true
else
	ls_res = false
end if

return ls_res
end function

public function integer nf_cfmc35fr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_cool_string, ref string as_prod_cat_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_stringsep
integer li_rval

u_String_Functions lu_string

ls_program_name = 'CFMC35FR'
ls_tran_id = 'C35F'

ls_input_string = as_header_string + '~h7f' + as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
if li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2,1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
		Choose Case ls_stringind
			Case 'C'
				as_cool_string += ls_output
			Case 'P'
				as_prod_cat_string += ls_output
		End Choose
		ls_stringInd = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
End if

return li_rval 
end function

public function integer nf_cfmc29fr (string as_req_customer_id, string as_req_division, ref string as_recv_window_inq_string, ref string as_header_string, ref string as_refetch_division, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringid, ls_output, ls_stringind,ls_stringsep
integer li_rval

u_String_Functions lu_string

ls_program_name = 'CFMC29FR'
ls_tran_id = 'C29F'

ls_input_string = as_req_customer_id + '~h7f' + as_req_division

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
if li_rval = 0 then
	ls_stringind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	
	Do While Len(Trim(ls_outputstring)) > 0 
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
		
		Choose Case ls_stringind
				
			Case 'H'
				as_header_string += ls_output
			Case 'R'
				as_recv_window_inq_string += ls_output
				
		End Choose
		ls_stringind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
end if

return li_rval
end function

public function integer nf_cfmc36fr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_cool_string, ref string as_prod_cat_string, ref string as_cust_excl_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_stringsep
integer li_rval
long ll_e_row=0

u_String_Functions  lu_string


ls_program_name= 'CFMC36FR'
ls_tran_id = 'C36F'


ls_input_string = as_header_string +'~h7f' + as_input_string

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'D'
				as_cool_string = as_cool_string + ls_output
			Case 'M'			
				as_prod_cat_string = as_prod_cat_string + ls_output
			Case 'E' 
				ll_e_row++  // pjm
				// pjm 08/25/2014 - can have multiple plants, so I added "," for parsing out
				If Len(Trim(ls_output)) > 2 Then
				//	gf_cool_code(ls_output, Len(ls_output))
					as_cust_excl_string = as_cust_excl_string + string(ll_e_row) + "~t" + ls_output + ","
				End If
			
		End Choose
		
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
		
	Loop
	
end if
return li_rval

		
		

end function

public function boolean nf_cfmc31fr (string as_inputstring, ref string as_outputstring, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'CFMC31FR'
ls_tran_id = 'C31F'

li_rval = uf_rpccall(as_inputstring,as_outputstring, astr_error_info, ls_program_name, ls_tran_id)

If li_rval =0 then
	return true
else
	return false
end if 



end function

public function integer nf_cfmc34fr (ref s_error astr_error_info, string as_header, string as_input, ref string as_output, ref string as_detail, ref string as_recorder_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_detail, ls_stringind,ls_stringsep
integer li_rval
u_String_Functions lu_string

ls_program_name = 'CFMC34FR'
ls_tran_id = 'C34F'

ls_input_string = as_header +'~h7f' + as_input +'~h7f' + as_detail

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_OutputString, 1)
if li_rval = 0 then
	ls_stringind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
		
		Choose Case ls_StringInd
			Case 'O' 
				as_output += ls_output
			Case 'R'
				as_recorder_string += ls_output
			Case 'D'
				as_detail += ls_output
		End Choose
		ls_stringInd = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
end if
return li_rval
end function

public function integer nf_orpo33fr (ref string as_order_id, ref string as_detail_info, ref string as_weight_str, ref string as_res_reduce_in, ref string as_pa_resolve_out, string as_action_ind, ref string as_production_date, ref string as_dept_code, ref string as_dup_products_ind, ref string as_end_customers, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringInd, ls_output,ls_weight_str,ls_detail_info, &
		ls_dept_code,ls_pa_resolve_out,ls_production_date,ls_stringsep, ls_end_customers

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO33FR'
ls_tran_id ='O33F'

ls_input_string = as_action_ind +'~h7f' + as_order_id +'~h7f' + as_detail_info +'~h7f' + as_weight_str +'~h7f' + as_res_reduce_in +'~h7f' + as_production_date &
+'~h7f' + as_dept_code +'~h7f' + as_dup_products_ind

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, 2)
ls_stringsep = Left(ls_OutputString, 1)

//UNSTRING OUTPUTSTRING 

	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_stringInd
			Case 'W'
				ls_weight_str += Trim(ls_output)
			Case 'D'
				ls_detail_info += Trim(ls_output)
			Case 'C'
				ls_dept_code += Trim(ls_output)
			Case 'P'
				ls_pa_resolve_out += Trim(ls_output)
			Case 'R'
				ls_production_date += Trim(ls_output)
			Case 'E'
				ls_end_customers += Trim(ls_output)				

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop


as_weight_str = ls_weight_str
as_detail_info = ls_detail_info
as_dept_code = ls_dept_code
as_pa_resolve_out = ls_pa_resolve_out
as_production_date = ls_production_date
as_end_customers = ls_end_customers 


return li_rval
end function

public function integer nf_orpo34fr (ref string as_customer_info, ref string as_weight_info, ref s_error astr_error_info, ref string as_program_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_detail_info,ls_output, ls_stringind,ls_stringsep

integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO34FR'
ls_tran_id ='O34F'

ls_input_string = as_customer_info +'~h7f'+ as_weight_info

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, 1)
ls_stringsep = Left(ls_OutputString, 1)
as_customer_info =''
as_weight_info =''
as_program_info =''
//UNSTRING OUTPUTSTRING 
If  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'C'
				as_customer_info += ls_output
			Case 'W'
				as_weight_info += ls_output
			Case 'P'
				as_program_info += ls_output				

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	


return li_rval
end function

public function integer nf_orpo32fr (ref string as_header_info, character ac_action_ind, ref string as_weight_override_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country, ref s_error astr_error_info, string as_linked_orders_ind);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_detail_info,ls_output, &
ls_weight_cost_out, ls_header_out, ls_stringind, lc_addition_out, ls_age_type_code_out, &
ls_pss_ind_out, ls_loading_instr_out, ls_protect_out, ls_shortage_out, ls_gtl_code, ls_dept_code, ls_exp_country, ls_weight_override_info,ls_stringsep 
integer li_rval

u_String_Functions lu_string

ls_program_name='ORPO32FR'
ls_tran_id ='O32F'

ls_input_string = ac_action_ind +'~h7f' +as_header_info +'~h7f' + as_weight_cost_info +'~h7f' + as_weight_override_info &
+'~h7f' + ac_addition_in +'~h7f' + as_age_type_code +'~h7f' + as_pss_ind +'~h7f' + as_load_instr &
+'~h7f' + as_protect +'~h7f' + as_pa_option +'~h7f' + as_gtl_code +'~h7f' + as_dept_code +'~h7f' + as_dom_exp_ind &
+ '~h7f' +as_exp_country + '~h7f' + as_linked_orders_ind

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, 3)
ls_stringsep = Left(ls_OutputString, 1)
//UNSTRING OUTPUTSTRING 
If  (li_rval >= 0) then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'W'
				ls_weight_cost_out += ls_output
			Case 'H'
				ls_header_out += ls_output
			Case 'S'
				ls_shortage_out += ls_output				
			Case 'O'
				ls_weight_override_info += ls_output
			Case 'D'
				ls_detail_info += ls_output				

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_weight_cost_info		=	ls_weight_cost_out
as_header_info				=	ls_header_out
as_shortage_out 			=  ls_shortage_out
as_weight_override_info	= 	ls_weight_override_info 

ac_addition_in				= lu_string.nf_gettoken( ls_detail_info, '~t')
as_age_type_code 		= lu_string.nf_gettoken( ls_detail_info, '~t')
as_pss_ind					= lu_string.nf_gettoken( ls_detail_info, '~t')
as_load_instr				= lu_string.nf_gettoken( ls_detail_info, '~t')
as_protect					= lu_string.nf_gettoken( ls_detail_info, '~t')
as_gtl_code			   	     = lu_string.nf_gettoken( ls_detail_info, '~t')
as_dept_code				= lu_string.nf_gettoken( ls_detail_info, '~t')
as_exp_country				= lu_string.nf_gettoken( ls_detail_info, '~t')
as_dom_exp_ind			= lu_string.nf_gettoken( ls_detail_info, '~t')


//check for any RPC errors to display messages
//nf_display_message(li_rtn, astr_error_info, ii_otr003_commhandle)

return li_rval
end function

on u_ws_orp2.create
call super::create
end on

on u_ws_orp2.destroy
call super::destroy
end on

