﻿$PBExportHeader$u_ws_sma.sru
forward
global type u_ws_sma from u_sma_webservice
end type
end forward

global type u_ws_sma from u_sma_webservice
end type
global u_ws_sma u_ws_sma

forward prototypes
public function integer nf_smas20er (string as_type_ind, ref s_error astr_error_info, ref string as_output)
public function integer nf_smas22er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring)
public function integer nf_smas21er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring)
public function integer nf_smas23er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring)
public function integer nf_smas20er (string as_type_ind, ref s_error astr_error_info, ref string as_output, integer ai_version_number)
public function integer nf_smas21er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring, integer ai_version_number)
end prototypes

public function integer nf_smas20er (string as_type_ind, ref s_error astr_error_info, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'SMAS20ER'
ls_tran_id = 'S20E'

ls_input_string = as_type_ind

li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id)

return li_rval


end function

public function integer nf_smas22er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'SMAS22ER'
ls_tran_id = 'S22E'

ls_input_string = as_inputstring

li_rval = uf_rpccall(ls_input_string, as_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval


end function

public function integer nf_smas21er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'SMAS21ER'
ls_tran_id = 'S21E'

ls_input_string = as_inputstring

li_rval = uf_rpccall(ls_input_string, as_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval


end function

public function integer nf_smas23er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'SMAS23ER'
ls_tran_id = 'S23E'

ls_input_string = as_inputstring

li_rval = uf_rpccall(ls_input_string, as_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval


end function

public function integer nf_smas20er (string as_type_ind, ref s_error astr_error_info, ref string as_output, integer ai_version_number);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'SMAS20ER'
ls_tran_id = 'S20E'

ls_input_string = as_type_ind

li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id, ai_version_number)

return li_rval


end function

public function integer nf_smas21er (string as_inputstring, ref s_error astr_error_info, ref string as_outputstring, integer ai_version_number);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'SMAS21ER'
ls_tran_id = 'S21E'

ls_input_string = as_inputstring

li_rval = uf_rpccall(ls_input_string, as_outputstring, astr_error_info, ls_program_name, ls_tran_id, ai_version_number)

return li_rval


end function

on u_ws_sma.create
call super::create
end on

on u_ws_sma.destroy
call super::destroy
end on

