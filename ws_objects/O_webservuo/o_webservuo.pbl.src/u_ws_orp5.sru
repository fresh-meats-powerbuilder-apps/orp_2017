﻿$PBExportHeader$u_ws_orp5.sru
forward
global type u_ws_orp5 from u_orp_webservice
end type
end forward

global type u_ws_orp5 from u_orp_webservice
end type
global u_ws_orp5 u_ws_orp5

forward prototypes
public function boolean nf_cfmc32fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_cfmc33fr (string as_input_string, ref s_error astr_error_info)
public function boolean nf_cfmc37fr (string as_header_string, string as_input_string, ref string as_output_string, ref s_error astr_error_info)
end prototypes

public function boolean nf_cfmc32fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions lu_string

ls_program_name = 'CFMC32FR'
ls_tran_id = 'C32F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end If
end function

public function boolean nf_cfmc33fr (string as_input_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id
integer li_rval
u_String_Functions lu_string

ls_program_name = 'CFMC33FR'
ls_tran_id = 'C33F'

li_rval = uf_rpcupd(as_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean nf_cfmc37fr (string as_header_string, string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

ls_program_name= 'CFMC37FR'
ls_tran_id = 'C37F'

ls_input_string = as_header_string +'~h7f' + as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end if
end function

on u_ws_orp5.create
call super::create
end on

on u_ws_orp5.destroy
call super::destroy
end on

