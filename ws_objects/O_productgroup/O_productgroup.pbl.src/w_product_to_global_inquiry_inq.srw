﻿$PBExportHeader$w_product_to_global_inquiry_inq.srw
forward
global type w_product_to_global_inquiry_inq from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_product_to_global_inquiry_inq
end type
type dw_detail from u_base_dw_ext within w_product_to_global_inquiry_inq
end type
end forward

global type w_product_to_global_inquiry_inq from w_base_response
integer x = 1075
integer y = 485
integer width = 1728
integer height = 460
string title = "Product Information Inquire"
boolean controlmenu = false
long backcolor = 12632256
cb_browse cb_browse
dw_detail dw_detail
end type
global w_product_to_global_inquiry_inq w_product_to_global_inquiry_inq

type variables


w_base_sheet_ext		iw_parentwindow
w_base_sheet	iw_parent
String			is_openstring
Boolean	ib_valid_return
DataWindowChild	idwc_document

u_orp003		iu_orp003

s_error	istr_error_info
end variables

forward prototypes
public function integer wf_reset_prod_color (integer ac_show)
end prototypes

public function integer wf_reset_prod_color (integer ac_show);Long		ll_RowCount, ll_index
String 	ls_value
Boolean	lb_finished
//u_string_functions	lu_string_functions

lb_finished = False
ll_RowCount = dw_detail.RowCount()
ll_index = 1

Do
	ls_value = dw_detail.object.detail_errors[ll_index]
	If IsNull(ls_value) Then ls_value = '  '
	Choose case ac_show
	case 0	
		ls_value = Right(ls_value,1)
		ls_value = ls_value + 'V'
	case 1
		ls_value = Left(ls_value,1)
		ls_value = ls_value + ls_value
	case 3
		ls_value = Left(ls_value,1)
		ls_value = ls_value + 'V'
	End Choose
	dw_detail.SetItem(ll_index,"detail_errors", ls_value)	
		
	ll_index ++
	If ll_index > ll_RowCount Or ll_RowCount = 0 Then lb_finished = True
Loop Until lb_finished

RETURN ll_RowCount
end function

on w_product_to_global_inquiry_inq.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
this.Control[iCurrent+2]=this.dw_detail
end on

on w_product_to_global_inquiry_inq.destroy
call super::destroy
destroy(this.cb_browse)
destroy(this.dw_detail)
end on

event open;call super::open;//datawindowchild			ldwc_temp
//
//								
String						ls_data


iw_parentwindow = Message.PowerObjectParm
ls_data = Message.StringParm

If dw_detail.ImportString(ls_data) < 1 Then 
	dw_detail.InsertRow(0)
End If

//dw_detail.InsertRow(0)
end event

event ue_base_ok;
String ls_product_code, &
       ls_value

u_string_functions	lu_string_functions


dw_detail.getchild( "product_code", idwc_document)

dw_detail.AcceptText()


ls_value = dw_detail.GetItemString(1, 'product_code')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('Product Code is a required field.')
	Return
End If

dw_detail.TriggerEvent('ue_setbusinessrule')
//dw_detail.Event ue_setbusinessrule
//dw_detail.Event("ue_setbusinessrule")
//wf_reset_prod_color(1)
//wf_reset_prod_color(dw_detail)

ls_product_code = dw_detail.getitemstring(1, 'product_code')
iw_parentwindow.Event ue_set_data('product_code', ls_product_code)

ib_valid_return = True

CloseWithReturn(This, "OK")




end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen(unsignedlong wparam, long lparam);
string    ls_product_code

u_string_functions	lu_string_functions

iw_parentwindow.Event ue_get_data('product_code')
//iw_parent.Event ue_get_data('product_code')
ls_product_code = Message.StringParm
If Not lu_string_functions.nf_IsEmpty(ls_product_code) Then
  dw_detail.SetItem(1, "product_code", ls_product_code)
  dw_detail.SetColumn('product_code')
End If






end event

type cb_base_help from w_base_response`cb_base_help within w_product_to_global_inquiry_inq
boolean visible = false
integer x = 992
integer y = 256
integer taborder = 40
boolean enabled = false
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_product_to_global_inquiry_inq
integer x = 1376
integer y = 188
integer taborder = 50
end type

type cb_base_ok from w_base_response`cb_base_ok within w_product_to_global_inquiry_inq
integer x = 1376
integer y = 60
integer taborder = 30
end type

type cb_browse from u_base_commandbutton_ext within w_product_to_global_inquiry_inq
boolean visible = false
integer x = 2126
integer y = 396
integer height = 108
integer taborder = 10
string text = "&Browse"
end type

type dw_detail from u_base_dw_ext within w_product_to_global_inquiry_inq
integer x = 50
integer y = 48
integer width = 873
integer height = 204
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_prod_order_inq"
end type

event constructor;call super::constructor;dw_detail.Modify("product_code_t.visible = false")
dw_detail.Modify("product_code_2_t.visible = true")

end event

event ue_setbusinessrule;call super::ue_setbusinessrule;String ls_BusinessRule
Long		ll_Column

If row <= this.RowCount() Then
	Choose Case name
		Case 'product_code'
		 	ll_column = 2
	END CHOOSE
	ls_businessRule =This.GetItemString(row,"detail_errors")
	if IsNull(ls_BusinessRule) Then ls_BusinessRule = '  '
	ls_BusinessRule = Left(ls_BusinessRule,ll_Column -1) &
							+ Value + Mid(ls_BusinessRule,ll_Column +1)
							
	This.SetItem(1,"detail_errors", ls_BusinessRule)	
	Return 1
End If
Return 1
end event

