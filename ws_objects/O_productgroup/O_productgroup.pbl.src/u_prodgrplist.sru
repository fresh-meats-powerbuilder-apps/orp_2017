﻿$PBExportHeader$u_prodgrplist.sru
forward
global type u_prodgrplist from listview
end type
end forward

global type u_prodgrplist from listview
integer width = 1609
integer height = 852
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
grsorttype sorttype = ascending!
listviewview view = listviewreport!
string largepicturename[] = {"Custom039!"}
integer largepicturewidth = 32
integer largepictureheight = 32
long largepicturemaskcolor = 536870912
string smallpicturename[] = {"Custom039!"}
integer smallpicturewidth = 16
integer smallpictureheight = 16
long smallpicturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
event ue_clear ( )
event ue_arrange ( )
event ue_viewchanging ( listviewview av_view )
event ue_copygrp ( )
event ue_copy ( )
event ue_deletegrp ( )
event ue_newgrp ( )
event ue_modifygrp ( )
end type
global u_prodgrplist u_prodgrplist

type variables
u_prodgrptree	itv_tree
u_grpmaintenance_new	iu_grpmaintlv_new
long		il_level, il_grpindex

end variables

forward prototypes
public function long uf_getlevel ()
public subroutine uf_setdata (long al_level, long al_pgi)
public function long uf_get_index ()
public function boolean uf_initialize (u_prodgrptree au_treeview, u_grpmaintenance_new au_grpmaint_new)
end prototypes

event ue_clear;//		Eventused to delete all items in the Listview before 
//		repopulating.  This allows reuse of the object for different lists.

this.DeleteColumns()
this.DeleteItems()

end event

event ue_arrange;This.Arrange()
end event

event ue_copygrp();treeviewitem	ltvi_root

String			ls_return, &
					ls_ownergrp, &
					ls_description, &
					ls_grouptype, &
					ls_groupcomment

Integer			li_prodsetid, &
					li_index
					
long				ll_handle, &
					ll_parenthandle

ListViewItem	llvi_item

u_TreeViewFunctions		lu_TreeFunctions


li_index = This.SelectedIndex()

If li_index <= 0 Then 
	iu_grpmaintlv_new.uf_SetMicrohelp('No group selected')
	Return
End If

This.GetItem(li_index, llvi_item)
ls_description = llvi_item.Label

ls_grouptype = iu_grpmaintlv_new.uf_getgrouptype()
ls_groupcomment = iu_grpmaintlv_new.uf_getgroupcomment(long(llvi_item.Data))

SetPointer(hourglass!)
OpenWithParm(w_groupmaintcpy, ls_description + '~t' + ls_grouptype + '~t')
ls_return = Message.StringParm

ll_parenthandle = iu_grpmaintlv_new.uf_GetOwnerHandle()
If Not lu_TreeFunctions.uf_checkfordupdescriptions(ls_return, ll_parenthandle, itv_tree) Then 
	MessageBox("Error","You can not have duplicate Descriptions.")
	Return
End If

If Len(Trim(ls_return)) > 0 Then 
	iu_grpmaintlv_new.uf_copygroup(ls_return, long(llvi_item.Data), ls_groupcomment)
Else
//	.setmicrohelp("No Product Group Created")
End If


end event

event ue_deletegrp();Long				ll_rc, &
					ll_index
ListViewItem	lvi_list
Boolean			lb_rtn


ll_index = This.SelectedIndex()
If ll_index <= 0 Then 
	MessageBox('Delete', 'No Group Selected.  Please Select a group to delete.')
	Return
End if

IF MessageBox("Delete Selected Item", &
		"Are you sure you want to delete the selected item?", &
		Question!, YesNo!, 2) = 1 THEN 
	
	SetPointer(hourglass!)
	This.GetItem(ll_index, lvi_list)
	lb_rtn = iu_grpmaintlv_new.uf_DeleteGroup(Long(lvi_list.Data))
	If lb_rtn Then 
		ll_rc = This.DeleteItem(ll_index)
	End If
	This.Arrange()
END IF

end event

event ue_newgrp();String			ls_rtn, ls_grouptype

Long				ll_index

ListViewItem	llvi_item


ll_index = This.SelectedIndex()
This.GetItem(ll_index, llvi_item)

iu_grpmaintlv_new.uf_setgroupind(ll_index)
iu_grpmaintlv_new.uf_setnewgroupind(True)

ls_grouptype = iu_grpmaintlv_new.uf_getgrouptype()

If ls_grouptype = 'P' Then
	OpenWithParm(w_groupmaintproduct_new, iu_grpmaintlv_new)
Else
	If ls_grouptype = 'C' Then
		OpenWithParm(w_groupmaintcustomer_new, iu_grpmaintlv_new)
	Else
		OpenWithParm(w_groupmaintlocation_new, iu_grpmaintlv_new)
	End If
End If

ls_rtn = Message.StringParm

If ls_rtn = "OK" Then 
	SetPointer(HourGlass!)
	iu_grpmaintlv_new.uf_inquire()
End If

end event

event ue_modifygrp();ListViewItem	llvi_item
TreeViewItem	ltvi_item

String			ls_rtn, ls_grouptype

long				ll_index, ll_handle


ll_index = This.SelectedIndex()

if ll_index >= 1 Then
	This.GetItem(ll_index, llvi_item)
	iu_grpmaintlv_new.uf_setgroupind(long(llvi_item.Data))
else
	ll_handle = itv_tree.FindItem(CurrentTreeItem!, 0)
	itv_tree.GetItem(ll_handle, ltvi_item)
	iu_grpmaintlv_new.uf_setgroupind(long(ltvi_item.Data))
end if

iu_grpmaintlv_new.uf_setnewgroupind(False)

ls_grouptype = iu_grpmaintlv_new.uf_getgrouptype()

If ls_grouptype = 'P' Then
	OpenWithParm(w_groupmaintproduct_new, iu_grpmaintlv_new)
Else
	If ls_grouptype = 'C' Then
		OpenWithParm(w_groupmaintcustomer_new, iu_grpmaintlv_new)
	Else
		OpenWithParm(w_groupmaintlocation_new, iu_grpmaintlv_new)
	End If
End If

ls_rtn = Message.StringParm
If ls_rtn = "OK" Then 
	SetPointer(HourGlass!)
	iu_grpmaintlv_new.uf_inquire()
End If

end event

public function long uf_getlevel ();Return il_level

end function

public subroutine uf_setdata (long al_level, long al_pgi);il_level = al_level
il_grpindex = al_pgi

end subroutine

public function long uf_get_index ();
Return il_grpindex
end function

public function boolean uf_initialize (u_prodgrptree au_treeview, u_grpmaintenance_new au_grpmaint_new);itv_tree = au_treeview
iu_grpmaintlv_new = au_grpmaint_new

Return True

end function

event key;IF KeyDown(KeyControl!) AND KeyDown(KeyM!) THEN
	This.TriggerEvent (RightClicked!)
END IF
end event

event rightclicked;Boolean				lb_disable

m_pfm_listView		lm_popmenu
Long					ll_spotx, &
						ll_spoty, &
						ll_index, &
						ll_handle, &
						ll_ownerhandle, &
						ll_parenthandle
						
String					ls_parentowner, &
						ls_updateowner
						
ListViewView		llvv_setting

u_TreeViewFunctions	lu_TreeFunctions

TreeViewItem	ltvi_item, ltvi_parent

ll_handle = itv_tree.FindItem(CurrentTreeItem!, 0)
itv_tree.GetItem(ll_handle, ltvi_item)

ll_parenthandle = itv_tree.FindItem(ParentTreeItem!, ll_handle)

If ll_parenthandle > 0 Then
	itv_tree.GetItem(ll_parenthandle,ltvi_parent)
	ls_parentowner = righttrim(string(ltvi_parent.Label))
Else
	ls_parentowner = righttrim(string(ltvi_item.Label))
End If

ls_updateowner = iu_grpmaintlv_new.is_updateowner
// If the tree handle isn't the application running then, all menu items exept for copy are
// disabled.
lb_disable = Not (ls_parentowner = ls_updateowner)

llvv_setting = This.view

lm_popmenu = CREATE m_pfm_listView
lm_popmenu.mf_initialize(llvv_setting, il_level, lb_disable)

iu_grpmaintlv_new.uf_pointerxy(ll_spotx, ll_spoty)
lm_popmenu.m_popup.Popmenu(ll_spotx, ll_spoty)
end event

event doubleclicked;listviewitem 	lvi_temp
TreeViewItem	ltvi_temp
Any				la_data
long				ll_tvi


If index > 0 Then
	This.GetItem(index, lvi_temp)
	la_data = lvi_temp.Data
	
	ll_tvi = itv_tree.FindItem(CurrentTreeItem! , 0)
	itv_tree.ExpandItem(ll_tvi)
	itv_tree.SetFocus()
	ll_tvi = itv_tree.FindItem(childtreeitem!, ll_tvi)
	If ll_tvi >= 0 Then
		itv_tree.GetItem(ll_tvi,ltvi_temp)
		DO Until String(ltvi_temp.Data) = String(la_data) Or ll_tvi = -1
			ll_tvi = itv_tree.FindItem(nexttreeitem!, ll_tvi)
			itv_tree.GetItem(ll_tvi,ltvi_temp)
		Loop 
		If ll_tvi > 0 Then itv_tree.SelectItem(ll_tvi)
	End If
End If

end event

event begindrag;listviewitem 		lvi_list


Choose Case il_level
	Case 1
		This.GetItem(index, lvi_list)
		il_grpindex = lvi_list.Data
		
	Case 2 , 3
		This.GetItem(index, lvi_list)
		lvi_list.Selected = False
		This.SetItem(This.SelectedIndex () , lvi_list)
		
End Choose
end event

event clicked;
listviewitem 		lvi_list


Choose Case il_level
	Case 1
		If index > 0 Then
			This.GetItem(index, lvi_list)
			il_grpindex = lvi_list.Data
		End If
	Case 2 , 3
		This.GetItem(index, lvi_list)
//		lvi_list.Selected = False
//		This.SetItem(This.SelectedIndex () , lvi_list)
		
End Choose

end event

on u_prodgrplist.create
end on

on u_prodgrplist.destroy
end on

