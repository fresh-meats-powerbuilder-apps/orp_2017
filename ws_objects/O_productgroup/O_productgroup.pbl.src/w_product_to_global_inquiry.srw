﻿$PBExportHeader$w_product_to_global_inquiry.srw
$PBExportComments$Product to global inquiry
forward
global type w_product_to_global_inquiry from w_base_sheet_ext
end type
type dw_product_plant from u_base_dw_ext within w_product_to_global_inquiry
end type
type dw_detail from u_base_dw_ext within w_product_to_global_inquiry
end type
type dw_header from u_base_dw_ext within w_product_to_global_inquiry
end type
end forward

global type w_product_to_global_inquiry from w_base_sheet_ext
integer width = 3781
integer height = 2940
string title = "Product Information"
boolean maxbox = false
dw_product_plant dw_product_plant
dw_detail dw_detail
dw_header dw_header
end type
global w_product_to_global_inquiry w_product_to_global_inquiry

type variables
s_error		istr_error_info


u_orp001		iu_orp001

u_ws_orp3		iu_ws_orp3

u_ws_orp4		iu_ws_orp4

//long		il_selected_rows, &
// 			il_product_seq

//INT		ii_row_count

String	is_customer_inq, &
			is_update_string, &
			is_columnname, &
			is_colname

Boolean  ib_modified = false, &
			ib_updating , &
			ib_reinquire, &
			ib_retrieve, &
			ib_updateable
			
DataStore ids_sales_person

end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_update ();
  String	ls_update_string, &
         ls_product_code, &
  			ls_fresh_shelf_code, &
      	ls_update_flag
					
Integer	li_rtn

if dw_detail.AcceptText() = -1 then
	Return False
end if

ls_product_code  = dw_header.GetItemString(1,"product_code")
ls_fresh_shelf_code  = dw_detail.GetItemString(1,"fresh_shelf_code")

ls_update_string = ls_product_code  + "~t" + &
                  ls_fresh_shelf_code + '~r~n'

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_orp201.orpo10br_edi_cust_prod_exc_upd"
istr_error_info.se_message = Space(71)

//li_rtn =  iu_orp003.nf_orpo10cr_product_to_global_upd (istr_error_info, & 
//											ls_update_string ) 

li_rtn =  iu_ws_orp3.uf_orpo10gr_product_to_global_upd (istr_error_info, & 
											ls_update_string ) 


if li_rtn <> 0 then
	this.SetRedraw(True)
	SetPointer(Arrow!)																		
	return false
end if


dw_detail.ResetUpdate()
ib_ReInquire = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")

dw_detail.SetFocus()
dw_detail.SetReDraw(True)

Return True

end function

public function boolean wf_retrieve ();Boolean	lb_show_message
String		ls_input, &
				ls_output_string, &			
				ls_product_code, &
				ls_detail_string
				
				
Integer		li_rtn

Long		ll_value


TriggerEvent("closequery")

ls_input = Message.StringParm

If Not ib_ReInquire Then
	OpenWithParm(w_product_to_global_inquiry_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_product_code = dw_header.GetItemString( 1, "product_code")
If IsNull(ls_product_code) Then
	ls_product_code = ""
End If

ls_input =  ls_product_code + '~r~n'
			  
istr_error_info.se_function_name = "wf_retrieve"

dw_detail.Reset()
dw_product_plant.Reset()
//li_rtn = iu_orp003.nf_orpo09cr_product_to_global_inq (istr_error_info, ls_input, ls_output_string, ls_detail_string)
li_rtn = iu_ws_orp4.NF_ORPO09GR(istr_error_info, ls_input, ls_output_string, ls_detail_string)

ib_retrieve = True
dw_detail.ImportString(ls_output_string)
dw_detail.ResetUpdate()
dw_detail.SetReDraw(True)
This.SetRedraw(True)

dw_product_plant.ImportString(ls_detail_string)
dw_product_plant.ResetUpdate()
dw_product_plant.SetReDraw(True)
//
Return TRUE
end function

on w_product_to_global_inquiry.create
int iCurrent
call super::create
this.dw_product_plant=create dw_product_plant
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_plant
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_header
end on

on w_product_to_global_inquiry.destroy
call super::destroy
destroy(this.dw_product_plant)
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event close;call super::close;If IsValid(iu_orp003) Then Destroy iu_orp003
If IsValid(iu_ws_orp3) Then Destroy iu_ws_orp3

If IsValid(iu_ws_orp4) Then Destroy iu_ws_orp4

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")
end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")


end event

event ue_query;call super::ue_query;IF wf_Retrieve() = false Then
		This.SetRedraw( TRUE)
		CLOSE(THIS)
		RETURN
END IF
end event

event ue_set_data;call super::ue_set_data;//choose case as_data_item
//	case "edi_doc_id"
//		dw_header.setItem(1, 'edi_doc_id', as_value)
//	case "edi_doc_desc"
//		dw_header.setItem(1, 'edi_doc_desc', as_value)
//end choose
choose case as_data_item
	case "product_code"
		dw_header.setItem(1, 'product_code', as_value)
end choose
end event

event open;call super::open;iu_orp003 = Create u_orp003
iu_ws_orp3 = Create u_ws_orp3
iu_ws_orp4 = Create u_ws_orp4
dw_detail.InsertRow(0)
			







end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'product_code'
		message.StringParm = dw_header.getitemstring (1, "product_code")
End choose
end event

type dw_product_plant from u_base_dw_ext within w_product_to_global_inquiry
integer x = 46
integer y = 2040
integer width = 3616
integer height = 756
integer taborder = 30
string dataobject = "d_product_plants"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type dw_detail from u_base_dw_ext within w_product_to_global_inquiry
integer x = 37
integer y = 224
integer width = 3639
integer height = 1788
integer taborder = 20
string dataobject = "d_product_to_global_detail"
borderstyle borderstyle = StyleRaised!
boolean ib_scrollable = false
end type

event itemchanged;call super::itemchanged;//changed from shelf code to number of days
//string   ls_type_desc, & 
//         ls_string
//
//If dwo.name = "fresh_shelf_code" Then
//	
//	SELECT tutltypes.type_short_desc
//	INTO :ls_type_desc
//	FROM tutltypes
//	WHERE tutltypes.record_type = 'SHLFLIFE'
//	  AND tutltypes.type_code   = :data
//	USING SQLCA ;
//
//	This.SetItem(row, "fresh_shelf_life_days", data)
//End If

//ls_string = This.GetItemString(1, 'fresh_shelf_life_days')
//
//This.InsertRow(0)
//dw_detail.ResetUpdate()
//dw_detail.SetReDraw(True)

end event

event constructor;call super::constructor;//datawindowchild	ldwc_type, &
//                  ldwc_type2
						  
u_project_functions		lu_project_functions

//is_selection = '2'
ib_updateable = true
//ib_NewRowOnChange = False

this.object.fresh_shelf_code.protect = 0

//sept 2015 - changed from code to number of days.
//This.GetChild('fresh_shelf_code', ldwc_type)
//If ldwc_type.RowCount() <= 1 Then
//	lu_project_functions.nf_gettutltype(ldwc_type, 'SHLFLIFE')
//End IF
//
//
//This.GetChild('fresh_shelf_life_days', ldwc_type2)
//If ldwc_type2.RowCount() <= 1 Then
//	lu_project_functions.nf_gettutltype(ldwc_type2, 'SHLFLIFE')
//End IF

end event

event ue_dwndropdown;call super::ue_dwndropdown;//sept 2015 - changed from code to number of days.
//DataWindowChild	ldwc_type, &
//                  ldwc_type2
//
//u_project_functions	lu_project_functions
//
//Choose Case This.GetColumnName()
//	Case 'fresh_shelf_code'
//		This.GetChild('fresh_shelf_code', ldwc_type)
//		If ldwc_type.RowCount() <= 1 Then
//			lu_project_functions.nf_gettutltype(ldwc_type, 'SHLFLIFE')
//		End If
//		This.GetChild('fresh_shelf_life_days', ldwc_type2)
//		If ldwc_type2.RowCount() <= 1 Then
//			lu_project_functions.nf_gettutltype(ldwc_type2, 'SHLFLIFE')
//		End If
//END CHOOSE
//



end event

type dw_header from u_base_dw_ext within w_product_to_global_inquiry
integer x = 37
integer width = 1682
integer height = 192
integer taborder = 10
string dataobject = "d_product_to_global_header"
boolean border = false
end type

event constructor;call super::constructor;dw_header.InsertRow(0)
end event

