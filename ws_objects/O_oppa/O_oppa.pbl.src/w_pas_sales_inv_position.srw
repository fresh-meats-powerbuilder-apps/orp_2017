﻿$PBExportHeader$w_pas_sales_inv_position.srw
forward
global type w_pas_sales_inv_position from w_netwise_sheet
end type
type dw_product_status_window from datawindow within w_pas_sales_inv_position
end type
type dw_sales_inv_position_report from u_netwise_dw within w_pas_sales_inv_position
end type
type dw_division from u_division within w_pas_sales_inv_position
end type
type dw_sales_inv_position_header from u_base_dw within w_pas_sales_inv_position
end type
type tab_1 from tab within w_pas_sales_inv_position
end type
type tp_inventory from userobject within tab_1
end type
type dw_inventory from u_netwise_dw within tp_inventory
end type
type cbx_showall from checkbox within tp_inventory
end type
type tp_inventory from userobject within tab_1
dw_inventory dw_inventory
cbx_showall cbx_showall
end type
type tp_production from userobject within tab_1
end type
type dw_production from u_netwise_dw within tp_production
end type
type tp_production from userobject within tab_1
dw_production dw_production
end type
type tp_sales from userobject within tab_1
end type
type dw_sales from u_netwise_dw within tp_sales
end type
type tp_sales from userobject within tab_1
dw_sales dw_sales
end type
type tab_1 from tab within w_pas_sales_inv_position
tp_inventory tp_inventory
tp_production tp_production
tp_sales tp_sales
end type
end forward

global type w_pas_sales_inv_position from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 3570
integer height = 1496
string title = "Sales / Inventory Position"
long backcolor = 12632256
long il_borderpaddingwidth = 100
dw_product_status_window dw_product_status_window
dw_sales_inv_position_report dw_sales_inv_position_report
dw_division dw_division
dw_sales_inv_position_header dw_sales_inv_position_header
tab_1 tab_1
end type
global w_pas_sales_inv_position w_pas_sales_inv_position

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
Long		il_SelectedColor, &
		il_SelectedTextColor, &
		il_selected_prod_status_max = 5, &
		il_selected_row

DataStore	ids_report

DataWindow	idw_top_dw

u_pas203		iu_pas203
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

boolean		ib_reinquire, first_time

String		is_pork_sales_option = "B", &
            is_selected_status_array, output_data

				
long		   il_selected_status_max = 8

Integer	ii_dddw_x, ii_dddw_y

end variables

forward prototypes
public function string wf_get_plant ()
public subroutine wf_print ()
public function string wf_get_boxes_or_loads ()
public subroutine wf_filter ()
public subroutine wf_get_selected_prod_status (datawindow adw_datawindow)
public function string wf_get_selected_status ()
public subroutine wf_set_format ()
public function boolean wf_retrieve ()
public subroutine wf_set_dataobjects ()
end prototypes

public function string wf_get_plant ();String			ls_plant

ls_plant = dw_sales_inv_position_header.GetItemString(1, "plant_code") + "~t" + &
					dw_sales_inv_position_header.GetItemString(1, "plant_descr") 

Return(ls_plant)
end function

public subroutine wf_print ();
end subroutine

public function string wf_get_boxes_or_loads ();String			ls_boxes_or_loads

ls_boxes_or_loads = dw_sales_inv_position_header.GetItemString(1, "boxes_or_loads")

Return(ls_boxes_or_loads)
end function

public subroutine wf_filter ();Integer		li_counter, ll_count

Long			ll_nbrrows

String		ls_filter, &
				ls_row_message, &
				ls_rmt_selection, &
				ls_neg_selection, ls_boxes_or_loads
				

ls_rmt_selection = dw_sales_inv_position_header.GetItemString(1,"rmt_selection") 
ls_neg_selection = dw_sales_inv_position_header.GetItemString(1,"neg_selection") 

if ls_rmt_selection = 'N' and ls_neg_selection = "Y" then
	 ls_filter = "((inv_a_qty < 0 or inv_b_qty < 0 or inv_c_qty < 0 or " + &
					"inv_f_qty < 0 or prod_est_end < 0 or prod_next_est_end " + &
					"< 0 or prod_net < 0) and product_state < '3') "
	ls_row_message = " rows with negative values without raw material"
else
if ls_rmt_selection = 'N' then
	 ls_filter = "product_state < '3' "
	 If not first_time Then
		dw_sales_inv_position_report.Reset()
		dw_sales_inv_position_report.ImportString(output_data)
		ls_boxes_or_loads = dw_sales_inv_position_header.GetItemString(1,"boxes_or_loads")
		//I did this because pb60 didn't like the first function in the datawindow painter.
		For ll_count = 1 to dw_sales_inv_position_report.RowCount()
			dw_sales_inv_position_report.SetItem(ll_count,"boxes_or_loads", ls_boxes_or_loads)
			dw_sales_inv_position_report.SetItem(ll_count, "back_color", il_selectedcolor)
			dw_sales_inv_position_report.SetItem(ll_count, "text_color", il_selectedtextcolor)
		Next
	End If
	first_time = False
	ls_row_message = " raw material rows only"
else	
if ls_neg_selection = "Y" then
	ls_filter = "inv_a_qty < 0 or inv_b_qty < 0 or inv_c_qty < 0 or " + &
					"inv_f_qty < 0 or prod_est_end < 0 or prod_next_est_end " + &
					"< 0 or prod_net < 0"
	ls_row_message = " rows with negative values"
Else
		ls_filter = ""
		ls_row_message = " rows total"
		End IF
	end if
end if

dw_sales_inv_position_report.SetFilter(ls_filter)	
dw_sales_inv_position_report.Filter()
ll_nbrrows = dw_sales_inv_position_report.RowCount()

iw_frame.SetMicroHelp(String(ll_nbrrows) + ls_row_message)

Return
end subroutine

public subroutine wf_get_selected_prod_status (datawindow adw_datawindow);
end subroutine

public function string wf_get_selected_status ();//1-13 jac
long  ll_count
string ls_product_status, & 
		ls_stat_desc, &
       ls_input 

is_selected_status_array = ''
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if ll_count = 1 then
		 If isnull(ls_product_status) Then
	   	ls_product_status = 'ALL'
	   end if
	else
	   If isnull(ls_product_status) Then
	   	ls_product_status = " "
	   end if
	end if
	is_selected_status_array += ls_product_status + "~t"
   ls_stat_desc = dw_product_status_window.GetItemString(1, "status_desc" + string(ll_count))
   ls_stat_desc = TRIM(ls_stat_desc)
	if ll_count = 1 then
	   If isnull(ls_stat_desc) Then
		   ls_stat_desc = 'ALL'
		end if
	else
	   If isnull(ls_stat_desc) Then
		   ls_stat_desc = " "
		end if 
	end if
	is_selected_status_array += ls_stat_desc + "~t"
next


Return is_selected_status_array
//



end function

public subroutine wf_set_format ();If dw_sales_inv_position_header.GetItemString(1,"boxes_or_loads") = 'B' Then
	dw_sales_inv_position_report.Modify("inv_a_comp.Format='###,##0' " + &
			"inv_b_comp.Format='###,##0' " + &
			"inv_c_comp.Format='###,##0' " + &
			"inv_f_comp.Format='###,##0' " + &
			"inv_total_comp.Format='###,##0' " + &
			"prod_shift_a_comp.Format='###,##0' " + &
			"prod_shift_b_comp.Format='###,##0' " + &
			"prod_auto_tran_comp.Format='###,##0' " + &
			"prod_delay_comp.Format='###,##0' " + &
			"prod_current_ship_comp.Format='###,##0' " + &
			"prod_est_end_comp.Format='###,##0' " + &
			"prod_next_est_end_comp.Format='###,##0' " + &
			"prod_net_comp.Format='###,##0' " + &
			"sales_comp_1.Format='###,##0' " + &
			"sales_comp_2.Format='###,##0' " + &
			"sales_comp_3.Format='###,##0' " + &
			"sales_comp_4.Format='###,##0' " + &
			"sales_comp_5.Format='###,##0' " + &
			"sales_comp_6.Format='###,##0' " + &
			"sales_comp_7.Format='###,##0' " + &
			"sales_comp_8.Format='###,##0' " + &
			"sales_comp_9.Format='###,##0' " + &
			"inventory_sum_comp.Format='###,##0' " + &
			"production_sum_comp.Format='###,##0' " + &
			"prod_tran_total.Format='###,##0' " + &
			"ship_sum_comp.Format='###,##0' ")
	If dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report_long" Then
		dw_sales_inv_position_report.Modify("inv_d_comp.Format='###,##0' " + &
			"inv_e_comp.Format='###,##0' ")
	End If
	If dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report_long_jac" Then
		dw_sales_inv_position_report.Modify("inv_d_comp.Format='###,##0' " + &
			"inv_e_comp.Format='###,##0' ")
	End If
	tab_1.tp_inventory.dw_inventory.Modify("inv_a_comp.Format='###,##0' " + &
			"inv_b_comp.Format='###,##0' " + &
			"inv_c_comp.Format='###,##0' " + &
			"inv_f_comp.Format='###,##0' " + &
			"total_inv_comp.Format='###,##0' " + &
			"sum_total_comp.Format='###,##0' ")
	If tab_1.tp_inventory.dw_inventory.DataObject = "d_pas_sales_inv_position_inv_long" Then
		tab_1.tp_inventory.dw_inventory.Modify("inv_d_comp.Format='###,##0' " + &
			"inv_e_comp.Format='###,##0' ")
	End If
		If tab_1.tp_inventory.dw_inventory.DataObject = "d_pas_sales_inv_position_inv_long_jac" Then
		tab_1.tp_inventory.dw_inventory.Modify("inv_d_comp.Format='###,##0' " + &
			"inv_e_comp.Format='###,##0' ")
	End If
	tab_1.tp_production.dw_production.Modify("prod_shift_a_comp.Format='###,##0' " + &
			"prod_shift_b_comp.Format='###,##0' " + &
			"prod_auto_tran_comp.Format='###,##0' " + &
			"prod_delay_comp.Format='###,##0' " + &
			"prod_current_ship_comp.Format='###,##0' " + &
			"prod_est_end_comp.Format='###,##0' " + &
			"prod_next_end_comp.Format='###,##0' " + &
			"prod_net_comp.Format='###,##0' " + &
			"current_ship_total_comp.Format='###,##0' " + &
			"prod_tran_total.Format='###,##0' " + &
			"shift_total_comp.Format='###,##0' ")
	tab_1.tp_sales.dw_sales.Modify("sales_comp_1.Format='###,##0' " + &
			"sales_comp_2.Format='###,##0' " + &
			"sales_comp_3.Format='###,##0' " + &
			"sales_comp_4.Format='###,##0' " + &
			"sales_comp_5.Format='###,##0' " + &
			"sales_comp_6.Format='###,##0' " + &
			"sales_comp_7.Format='###,##0' " + &
			"sales_comp_8.Format='###,##0' " + &
			"sales_comp_9.Format='###,##0' ") 			
Else
	dw_sales_inv_position_report.Modify("inv_a_comp.Format='###,##0.0' " + &
			"inv_b_comp.Format='###,##0.0' " + &
			"inv_c_comp.Format='###,##0.0' " + &
			"inv_f_comp.Format='###,##0.0' " + &
			"inv_total_comp.Format='###,##0.0' " + &
			"prod_shift_a_comp.Format='###,##0.0' " + &
			"prod_shift_b_comp.Format='###,##0.0' " + &
			"prod_delay_comp.Format='###,##0.0' " + &
			"prod_current_ship_comp.Format='###,##0.0' " + &
			"prod_auto_tran_comp.Format='###,##0.0' " + &
			"prod_est_end_comp.Format='###,##0.0' " + &
			"prod_next_est_end_comp.Format='###,##0.0' " + &
			"prod_net_comp.Format='###,##0.0' " + &
			"sales_comp_1.Format='###,##0.0' " + &
			"sales_comp_2.Format='###,##0.0' " + &
			"sales_comp_3.Format='###,##0.0' " + &
			"sales_comp_4.Format='###,##0.0' " + &
			"sales_comp_5.Format='###,##0.0' " + &
			"sales_comp_6.Format='###,##0.0' " + &
			"sales_comp_7.Format='###,##0.0' " + &
			"sales_comp_8.Format='###,##0.0' " + &
			"sales_comp_9.Format='###,##0.0' " + &
			"inventory_sum_comp.Format='###,##0.0' " + &
			"production_sum_comp.Format='###,##0.0' " + &
			"prod_tran_total.Format='###,##0.0' " + &
			"ship_sum_comp.Format='###,##0.0' ")
	If dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report_long" Then
		dw_sales_inv_position_report.Modify("inv_d_comp.Format='###,##0.0' " + &
			"inv_e_comp.Format='###,##0.0' ")
	End If
	If dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report_long_jac" Then
		dw_sales_inv_position_report.Modify("inv_d_comp.Format='###,##0.0' " + &
			"inv_e_comp.Format='###,##0.0' ")
	End If
	tab_1.tp_inventory.dw_inventory.Modify("inv_a_comp.Format='###,##0.0' " + &
			"inv_b_comp.Format='###,##0.0' " + &
			"inv_c_comp.Format='###,##0.0' " + &
			"inv_f_comp.Format='###,##0.0' " + &
			"total_inv_comp.Format='###,##0.0' " + &
			"sum_total_comp.Format='###,##0.0' ")
	If tab_1.tp_inventory.dw_inventory.DataObject = "d_pas_sales_inv_position_inv_long" Then
		tab_1.tp_inventory.dw_inventory.Modify("inv_d_comp.Format='###,##0.0' " + &
			"inv_e_comp.Format='###,##0.0' ")
	End If
	If tab_1.tp_inventory.dw_inventory.DataObject = "d_pas_sales_inv_position_inv_long_jac" Then
		tab_1.tp_inventory.dw_inventory.Modify("inv_d_comp.Format='###,##0.0' " + &
			"inv_e_comp.Format='###,##0.0' ")
	End If
	tab_1.tp_production.dw_production.Modify("prod_shift_a_comp.Format='###,##0.0' " + &
			"prod_shift_b_comp.Format='###,##0.0' " + &
			"prod_current_ship_comp.Format='###,##0.0' " + &
			"prod_delay_comp.Format='###,##0.0' " + &
			"prod_auto_tran_comp.Format='###,##0.0' " + &
			"prod_est_end_comp.Format='###,##0.0' " + &
			"prod_next_end_comp.Format='###,##0.0' " + &
			"prod_net_comp.Format='###,##0.0' " + &
			"current_ship_total_comp.Format='###,##0.0' " + &
			"prod_tran_total.Format='###,##0.0' " + &
			"shift_total_comp.Format='###,##0.0' ")
	tab_1.tp_sales.dw_sales.Modify("sales_comp_1.Format='###,##0.0' " + &
			"sales_comp_2.Format='###,##0.0' " + &
			"sales_comp_3.Format='###,##0.0' " + &
			"sales_comp_4.Format='###,##0.0' " + &
			"sales_comp_5.Format='###,##0.0' " + &
			"sales_comp_6.Format='###,##0.0' " + &
			"sales_comp_7.Format='###,##0.0' " + &
			"sales_comp_8.Format='###,##0.0' " + &
			"sales_comp_9.Format='###,##0.0' ")
End If
end subroutine

public function boolean wf_retrieve ();String		ls_string, &
				ls_date_time_stamp, &
				ls_input, &
				ls_plant_code, &
				ls_plant_descr, &
				ls_division_code, &
				ls_division_descr, &
				ls_selection, &
				ls_date, &
				ls_boxes_or_loads, &
				ls_string2, & 
				ls_product_status

DateTime		ldt_datetime


Long			ll_rec_count, ll_count, ll_pos, &
            ll_counter, ll_counter_max

Integer		li_counter

Call w_base_sheet::closequery

OpenWithParm(w_pas_sales_inv_position_inq, This)
ls_string = Message.StringParm
If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False

dw_sales_inv_position_header.Reset()
dw_sales_inv_position_report.Reset()

If dw_sales_inv_position_header.ImportString(Left(ls_string,iw_frame.iu_string.nf_nPos(ls_string,"~t",1,2)-1)) < 1  &
		Then Return False
dw_sales_inv_position_header.SetItem(1,"boxes_or_loads",Left(Right(ls_string,5),1))

tab_1.tp_inventory.cbx_showall.Checked = (Left(Right(ls_string,3),1) = "L")
wf_set_dataobjects()

//1-13 jac
ll_pos = 0
ll_counter = 1
ll_counter_max = 3
for ll_counter = 1 to ll_counter_max
ll_pos = Pos(ls_string,"~t", ll_pos + 1)
ll_counter = ll_counter +1 
next

ls_string2 = Mid(ls_string,ll_pos + 1,iw_frame.iu_string.nf_nPos(ls_string,"~t",ll_pos + 1,16))

dw_product_status_window.ImportString(ls_string2)
dw_product_status_window.AcceptText()
//
SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

istr_error_info.se_event_name = "wf_retrieve"

ls_plant_code = dw_sales_inv_position_header.GetItemString &
		(1, "plant_code")
ls_plant_descr = dw_sales_inv_position_header.GetItemString &
		(1, "plant_descr")
ls_division_code = dw_division.uf_get_division()
ls_division_descr = dw_division.uf_get_division_descr()

is_pork_sales_option = Right(ls_string,1)
ls_input = ls_plant_code + "~t" + ls_division_code + "~t" + is_pork_sales_option + "~t" 

//1-13 jac
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
	ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) then
		ls_product_status = " "
	end if
   ls_input += ls_product_status + "~t" 
next
//

If Not iu_ws_pas_share.nf_pasp39fr(istr_error_info, &
													ls_input, &
													ls_date_time_stamp, &
													ids_report, output_data) Then 																										
	SetRedraw(True)
	Return False
End If

first_time = True

wf_filter()
//1-14  jac
wf_set_dataobjects()
//
ids_report.ShareData( dw_sales_inv_position_report)
ids_report.ShareData( tab_1.tp_inventory.dw_inventory)
ids_report.ShareData( tab_1.tp_production.dw_production)
ids_report.ShareData( tab_1.tp_sales.dw_sales)

SetRedraw (False)

ll_rec_count = ids_report.RowCount()

ldt_datetime = DateTime(Date(String(mid(ls_date_time_stamp, 1,10))), &
					Time(String(mid(ls_date_time_stamp, 12,5))))

dw_sales_inv_position_header.SetItem(1,"last_updated", ldt_datetime)

ls_date = String(DateTime(Date(String(mid(ls_date_time_stamp, 1,10))), &
					Time(String(mid(ls_date_time_stamp, 12,5)))), 'mm/dd/yyyy hh:mm am/pm')
dw_sales_inv_position_report.object.last_updated_t.text =  ls_date

wf_set_format()

ls_boxes_or_loads = dw_sales_inv_position_header.GetItemString(1,"boxes_or_loads")
//I did this because pb60 didn't like the first function in the datawindow painter.
For ll_count = 1 to dw_sales_inv_position_report.RowCount()
	dw_sales_inv_position_report.SetItem(ll_count,"boxes_or_loads", ls_boxes_or_loads)
	dw_sales_inv_position_report.SetItem(ll_count, "back_color", il_selectedcolor)
	dw_sales_inv_position_report.SetItem(ll_count, "text_color", il_selectedtextcolor)
Next
	
dw_sales_inv_position_report.object.plant_code_t.text =  ls_plant_code
dw_sales_inv_position_report.object.plant_descr_t.text =  ls_plant_descr
dw_sales_inv_position_report.object.division_code_t.text =  ls_division_code
dw_sales_inv_position_report.object.division_descr_t.text =  ls_division_descr

ls_date = String(RelativeDate(Date(mid(ls_date_time_stamp, 1, 10)), 1), 'yyyy-mm-dd')
dw_sales_inv_position_report.object.sales_date_1.expression =  ls_date
tab_1.tp_sales.dw_sales.object.sales_date_1.expression =  ls_date

//dw_sales_inv_position_report.Sort()
dw_sales_inv_position_report.GroupCalc()

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

SetRedraw(True)

Return (True)
end function

public subroutine wf_set_dataobjects ();
String  		ls_plant_code, &
				ls_plant_descr, &
				ls_division_code, &
				ls_division_descr, &
				ls_date, &
				ls_date_time_stamp, &
				ls_c_d_shift
	
Integer     li_shift_c, &
            li_shift_d, & 
				ll_row, & 
				ll_nbrRows
				
DateTime		ldt_datetime

//1-14 jac
ls_c_d_shift = 'N'
ll_row = 1
ll_nbrRows = ids_report.RowCount()

 DO WHILE ll_Row <= ll_NbrRows
	if ll_nbrRows > 0 then
		li_shift_c = ids_report.GetItemDecimal &	
		(ll_row, "prod_shift_c")
		li_shift_d = ids_report.GetItemDecimal &	
		(ll_row, "prod_shift_d")
//	   ll_row = ids_report.GetNextModified(ll_Row, Primary!)
      If li_shift_c > 0 then
			ls_c_d_shift = 'Y'
		end if
		If li_shift_d > 0 then
			ls_c_d_shift = 'Y'
		end if     
	end if
	 ll_Row = ll_Row + 1
loop
//

		


If tab_1.tp_inventory.cbx_showall.Checked Then
	tab_1.tp_inventory.dw_inventory.DataObject = "d_pas_sales_inv_position_inv_long"
	if ls_c_d_shift = 'N' then
	  dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report_long"
   else 
	  dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report_long_jac"
   end if 
Else
	tab_1.tp_inventory.dw_inventory.DataObject = "d_pas_sales_inv_position_inv"
	dw_sales_inv_position_report.DataObject = "d_pas_sales_inv_position_report"
End If

ids_report.ShareData( dw_sales_inv_position_report)
ids_report.ShareData( tab_1.tp_inventory.dw_inventory)
ids_report.ShareData( tab_1.tp_production.dw_production)
ids_report.ShareData( tab_1.tp_sales.dw_sales)


//1-13 jac

ls_plant_code = dw_sales_inv_position_header.GetItemString &
		(1, "plant_code")
ls_plant_descr = dw_sales_inv_position_header.GetItemString &
		(1, "plant_descr")
ls_division_code = dw_division.uf_get_division()
ls_division_descr = dw_division.uf_get_division_descr()

dw_sales_inv_position_report.object.plant_code_t.text =  ls_plant_code
dw_sales_inv_position_report.object.plant_descr_t.text =  ls_plant_descr
dw_sales_inv_position_report.object.division_code_t.text =  ls_division_code
dw_sales_inv_position_report.object.division_descr_t.text =  ls_division_descr


ldt_datetime = DateTime(Date(String(mid(ls_date_time_stamp, 1,10))), &
					Time(String(mid(ls_date_time_stamp, 12,5))))

dw_sales_inv_position_header.SetItem(1,"last_updated", ldt_datetime)

ls_date = String(DateTime(Date(String(mid(ls_date_time_stamp, 1,10))), &
					Time(String(mid(ls_date_time_stamp, 12,5)))), 'mm/dd/yyyy hh:mm am/pm')
dw_sales_inv_position_report.object.last_updated_t.text =  ls_date
//




end subroutine

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')

//iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end event

event ue_postopen;call super::ue_postopen;Environment			le_env

iu_pas203 = create u_pas203
iu_ws_pas_share = Create u_ws_pas_share

ids_report = Create DataStore
ids_report.DataObject='d_pas_sales_inv_position_report'

If Message.ReturnValue = -1 Then Close(This)

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "salinvpos"
istr_error_info.se_user_id				= sqlca.userid

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

ids_report.ShareData(dw_sales_inv_position_report)
ids_report.ShareData( tab_1.tp_inventory.dw_inventory)
ids_report.ShareData( tab_1.tp_production.dw_production)
ids_report.ShareData( tab_1.tp_sales.dw_sales)

Choose Case tab_1.SelectedTab
	Case 1
		idw_top_dw = tab_1.tp_inventory.dw_inventory
	Case 2
		idw_top_dw = tab_1.tp_production.dw_production
	Case 3
		idw_top_dw = tab_1.tp_sales.dw_sales
	Case Else
		idw_top_dw = tab_1.tp_inventory.dw_inventory
		tab_1.SelectedTab = 1
End Choose
 


This.PostEvent('ue_query')
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

on w_pas_sales_inv_position.create
int iCurrent
call super::create
this.dw_product_status_window=create dw_product_status_window
this.dw_sales_inv_position_report=create dw_sales_inv_position_report
this.dw_division=create dw_division
this.dw_sales_inv_position_header=create dw_sales_inv_position_header
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_status_window
this.Control[iCurrent+2]=this.dw_sales_inv_position_report
this.Control[iCurrent+3]=this.dw_division
this.Control[iCurrent+4]=this.dw_sales_inv_position_header
this.Control[iCurrent+5]=this.tab_1
end on

on w_pas_sales_inv_position.destroy
call super::destroy
destroy(this.dw_product_status_window)
destroy(this.dw_sales_inv_position_report)
destroy(this.dw_division)
destroy(this.dw_sales_inv_position_header)
destroy(this.tab_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_generatesales')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_enable('m_print')

end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

If IsValid( ids_report ) Then
	Destroy ids_report
End If


Destroy iu_ws_pas_share
end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

If dw_sales_inv_position_header.GetItemString(1, 'neg_selection') = "Y" Then
	dw_sales_inv_position_report.Modify("inventory_sum.Visible = 0 " + &
													"production_sum.Visible = 0 " + &
													"ship_sum.Visible = 0")
End if

dw_sales_inv_position_report.print()

dw_sales_inv_position_report.Modify("inventory_sum.Visible = 1 " + &
													"production_sum.Visible = 1 " + &
													"ship_sum.Visible = 1")

end event

event open;call super::open;Environment			le_env


GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if


end event

event resize;call super::resize;constant integer li_tab_x		= 20
constant integer li_tab_y		= 240
  
 integer li_dw_x		= 70
 integer li_dw_y		= 420

constant integer li_cbx_x		= 676

if il_BorderPaddingWidth > li_dw_x Then
   li_dw_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_dw_y Then
   li_dw_y = il_BorderPaddingHeight
End If

    
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tp_inventory.dw_inventory.width = newwidth - li_dw_x
tab_1.tp_inventory.dw_inventory.height = newheight - li_dw_y

tab_1.tp_inventory.cbx_showall.x = &
		tab_1.tp_inventory.dw_inventory.width - li_cbx_x
		
tab_1.tp_production.dw_production.width = newwidth - li_dw_x
tab_1.tp_production.dw_production.height = newheight - li_dw_y

tab_1.tp_sales.dw_sales.width = newwidth - li_dw_x
tab_1.tp_sales.dw_sales.height = newheight - li_dw_y


end event

event ue_get_data;call super::ue_get_data;//1-13 jac
Choose Case as_value
	Case "product_status"
		message.StringParm = ''
	Case "selected"
		message.StringParm = is_selected_status_array
 	Case "dddwxpos"
		message.StringParm = string(ii_dddw_x)
	case "dddwypos"
		message.StringParm = string(ii_dddw_x)
End choose
//
end event

type dw_product_status_window from datawindow within w_pas_sales_inv_position
boolean visible = false
integer x = 1413
integer y = 192
integer width = 686
integer height = 96
integer taborder = 30
string title = "none"
string dataobject = "d_product_status_2"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;//ib_updateable = False
InsertRow(0)
end event

type dw_sales_inv_position_report from u_netwise_dw within w_pas_sales_inv_position
boolean visible = false
integer x = 1175
integer y = 4
integer width = 1623
integer height = 780
integer taborder = 0
string dataobject = "d_pas_sales_inv_position_report"
end type

on constructor;call u_netwise_dw::constructor;ib_updateable = False

end on

type dw_division from u_division within w_pas_sales_inv_position
integer y = 88
integer taborder = 10
end type

on constructor;call u_division::constructor;This.disable()
end on

type dw_sales_inv_position_header from u_base_dw within w_pas_sales_inv_position
integer x = 18
integer width = 2770
integer height = 216
integer taborder = 30
string dataobject = "d_pas_sales_inv_position_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;Long					ll_count

IF This.GetColumnName() = "neg_selection" Then
	dw_sales_inv_position_header.setitem(1, "neg_selection", data)
	wf_filter()
End If
IF This.GetColumnName() = "rmt_selection" Then
	dw_sales_inv_position_header.setitem(1, "rmt_selection", data)
	wf_filter()
	End If
If dwo.Name = "boxes_or_loads" Then
//I did this because pb60 didn't like the first function in the datawindow painter.
	For ll_count = 1 to ids_report.RowCount()
		ids_report.SetItem(ll_count,"boxes_or_loads", data)
	Next
	Parent.Post wf_set_format()
End If
end event

on constructor;call u_base_dw::constructor;ib_updateable = False
InsertRow(0)
end on

type tab_1 from tab within w_pas_sales_inv_position
event create ( )
event destroy ( )
integer y = 224
integer width = 3438
integer height = 896
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tp_inventory tp_inventory
tp_production tp_production
tp_sales tp_sales
end type

on tab_1.create
this.tp_inventory=create tp_inventory
this.tp_production=create tp_production
this.tp_sales=create tp_sales
this.Control[]={this.tp_inventory,&
this.tp_production,&
this.tp_sales}
end on

on tab_1.destroy
destroy(this.tp_inventory)
destroy(this.tp_production)
destroy(this.tp_sales)
end on

event selectionchanged;String 	ls_firstrow

If IsValid(idw_top_dw) Then
	ls_firstrow = idw_top_dw.Describe("DataWindow.VerticalScrollPosition")
	// This should synchronize the two datawindows' scrolling
	Choose Case newindex
	 	Case 1   // Switched to Inventory
			idw_top_dw = This.tp_inventory.dw_inventory
		Case 2   // Switched to current
			idw_top_dw = This.tp_production.dw_production
		Case 3   // Switched to sales
			idw_top_dw = This.tp_sales.dw_sales
	End Choose
	idw_top_dw.Modify("DataWindow.VerticalScrollPosition = '" + ls_firstrow + "'")	
End If
return 0  // Allow the tabpage selection to change

//String		ls_firstproduct
//
//Long			ll_lastdwfirstrow, &
//				ll_newdwfirstrow
//
//If IsValid(idw_top_dw) Then
//	ll_Lastdwfirstrow = Long(idw_top_dw.Object.DataWindow.FirstRowOnPage)
////	ls_firstproduct = idw_top_dw.GetItemString(ll_firstrow, 'product_code')
//	// This should synchronize the two datawindows' scrolling
//	Choose Case newindex
//	 	Case 1   // Switched to Unsold Inventory
//			idw_top_dw = This.tp_inventory.dw_inventory
//		Case 2   // Switched to Pct of Prod
//			idw_top_dw = This.tp_production.dw_production
//		Case 3   // Switched to Prod Buffer
//			idw_top_dw = This.tp_sales.dw_sales
//	End Choose
//	ll_Newdwfirstrow = Long(idw_top_dw.Object.DataWindow.FirstRowOnPage)
//	This.tp_production.dw_production.Scroll(ll_lastdwfirstrow - ll_newdwfirstrow)
//End If
//return 0  // Allow the tabpage selection to change
//
//



end event

type tp_inventory from userobject within tab_1
integer x = 18
integer y = 100
integer width = 3401
integer height = 780
long backcolor = 12632256
string text = "Ready to Ship"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
dw_inventory dw_inventory
cbx_showall cbx_showall
end type

on tp_inventory.create
this.dw_inventory=create dw_inventory
this.cbx_showall=create cbx_showall
this.Control[]={this.dw_inventory,&
this.cbx_showall}
end on

on tp_inventory.destroy
destroy(this.dw_inventory)
destroy(this.cbx_showall)
end on

type dw_inventory from u_netwise_dw within tp_inventory
integer y = 56
integer width = 2871
integer height = 724
integer taborder = 2
string dataobject = "d_pas_sales_inv_position_inv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;iw_Parent = w_pas_sales_inv_position

This.Postevent("ue_postconstructor")
end event

event scrollvertical;call super::scrollvertical;idw_top_dw = This
//tab_1.tp_sales.dw_sales.Event scrollvertical(scrollpos)
//tab_1.tp_production.dw_production.Event scrollvertical(scrollpos)

end event

type cbx_showall from checkbox within tp_inventory
integer x = 2103
integer y = 8
integer width = 521
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Show All Age Codes"
boolean lefttext = true
end type

event clicked;wf_set_dataobjects()
wf_set_format()
end event

type tp_production from userobject within tab_1
integer x = 18
integer y = 100
integer width = 3401
integer height = 780
long backcolor = 12632256
string text = "Production"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_production dw_production
end type

on tp_production.create
this.dw_production=create dw_production
this.Control[]={this.dw_production}
end on

on tp_production.destroy
destroy(this.dw_production)
end on

type dw_production from u_netwise_dw within tp_production
integer x = -18
integer y = 28
integer width = 3401
integer height = 736
integer taborder = 2
string dataobject = "d_pas_sales_inv_position_production"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;iw_Parent = w_pas_sales_inv_position

This.Postevent("ue_postconstructor")
end event

event scrollvertical;call super::scrollvertical;idw_top_dw = This
end event

type tp_sales from userobject within tab_1
integer x = 18
integer y = 100
integer width = 3401
integer height = 780
long backcolor = 12632256
string text = "Sales"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_sales dw_sales
end type

on tp_sales.create
this.dw_sales=create dw_sales
this.Control[]={this.dw_sales}
end on

on tp_sales.destroy
destroy(this.dw_sales)
end on

type dw_sales from u_netwise_dw within tp_sales
integer y = 56
integer width = 2779
integer height = 980
integer taborder = 3
string dataobject = "d_pas_sales_inv_position_sales"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;iw_Parent = w_pas_sales_inv_position

This.Postevent("ue_postconstructor")
end event

event scrollvertical;call super::scrollvertical;idw_top_dw = This
end event

