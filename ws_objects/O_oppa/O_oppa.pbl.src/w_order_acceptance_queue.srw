﻿$PBExportHeader$w_order_acceptance_queue.srw
forward
global type w_order_acceptance_queue from w_netwise_sheet
end type
type dw_1 from u_netwise_dw within w_order_acceptance_queue
end type
end forward

global type w_order_acceptance_queue from w_netwise_sheet
integer width = 5266
integer height = 1660
string title = "Order Acceptance Queue History"
dw_1 dw_1
end type
global w_order_acceptance_queue w_order_acceptance_queue

type variables
Boolean		ib_open_inq_window

u_ws_orp1	iu_ws_orp1

u_ws_orp2	iu_ws_orp2

u_ws_orp4	iu_ws_orp4

s_error		istr_error_info

string		is_inquire_window_name, &
			is_order_number

w_netwise_response	iw_inquirewindow, &
			iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_complete_order ()
end prototypes

public function boolean wf_retrieve ();Long				ll_value

Boolean			lb_return

String				ls_header_string, ls_detail_string
				
u_string_functions	lu_string

u_project_functions	lu_project_functions

Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

is_inquire_window_name = 'w_order_acceptance_queue_inq'
If ib_open_inq_window Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
End If

dw_1.object.t_order_number.Text = is_order_number

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ls_header_string = is_order_number + "~t" + "I" + Message.nf_Get_App_ID()

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_orpo15gr"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_ws_orp4 ) THEN
	iu_ws_orp4	=  CREATE u_ws_orp4
END IF

This.SetRedraw( False )

lb_return	= iu_ws_orp4.nf_orpo15gr( istr_error_info, ls_header_string, ls_detail_string)

dw_1.Reset()
dw_1.ImportString(ls_detail_string)
dw_1.ResetUpdate()

ib_open_inq_window = True

IF NOT lb_return THEN
	This.SetRedraw(True)
	Return( False )
END IF

If (dw_1.GetItemString( 1, "sales_loc") <> Message.is_smanlocation) &
  	AND (Message.is_smanlocation <> '630' AND Message.is_smanlocation <> '606' AND Message.is_smanlocation <> '006') & 
   AND ( NOT lu_project_functions.nf_isscheduler() )	&
	AND ( NOT lu_project_functions.nf_isschedulermgr() )	&
	AND ( NOT lu_project_functions.nf_issalesmgr() ) THEN 

	dw_1.Reset()
	dw_1.InsertRow(0)
	MessageBox("Warning", "Your location does not match the orders location.")
	Return False
End If

ll_value = dw_1.RowCount()
If ll_value < 0 Then ll_value = 0

dw_1.SelectRow(0,False)
dw_1.ResetUpdate()
dw_1.SetFocus()

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )
Return( True )

 
end function

public function boolean wf_update ();Integer	li_rowcount, &
			li_sub, &
			li_ret

Char		lc_AutoResolve

String		ls_detail_string, &
			ls_order_id, &
			ls_microhelp


IF Not IsValid(iu_ws_orp2 ) THEN
	iu_ws_orp2=  CREATE u_ws_orp2
END IF

dw_1.AcceptText()

ls_detail_string = ''
lc_AutoResolve = 'A' // accept as is ''

ls_order_id = is_order_number

li_rowcount = dw_1.RowCount()
For li_sub = 1 to li_rowcount
	If dw_1.GetItemString(li_sub, "accept_flag") = "Y" Then 
		ls_detail_string += dw_1.GetItemString(li_sub, "line_number") + '~t'
	End If
Next	

//use local order number variable for call to ORPO35FR, function call sets to blank
If len(ls_detail_string) > 0 Then
	iu_ws_orp2.nf_orpo35fr(ls_order_id, lc_AutoResolve, ls_detail_string, istr_error_info)
	wf_complete_order()
	ls_microhelp = iw_frame.wf_getmicrohelp()
	ib_open_inq_window = False
	wf_retrieve()
	iw_frame.SetMicroHelp(ls_microhelp)
	ib_open_inq_window = True
End If

Return True
end function

public subroutine wf_complete_order ();String		ls_Load_Status, &
			ls_weight_error_Flag, &
			ls_weight_Override, &
			ls_net_order_flag, &
			ls_order_status, &
			ls_send_to_sched_ind, &
			ls_order_id, &
			ls_detail_data
			
Integer	li_ret, &
			li_shtg_line_count
			
IF Not IsValid(iu_ws_orp1 ) THEN
	iu_ws_orp1=  CREATE u_ws_orp1
END IF			
	
ls_Load_Status = 'N'
ls_weight_error_Flag = ' '
ls_weight_Override  = ' '
ls_net_order_flag = ' '
ls_order_status = ' '
ls_send_to_sched_ind	 = 'N'
ls_order_id = is_order_number
ls_detail_data = ''
li_shtg_line_count = 0

li_ret = iu_ws_orp1.nf_orpo05fr(istr_error_info, &
									ls_order_id, &
									ls_Load_Status, &
									ls_order_status, &
									ls_weight_error_Flag, &
									ls_weight_Override, &
									ls_net_order_flag, &
									ls_detail_data, &
									li_shtg_line_count, &
									ls_send_to_sched_ind)	
end subroutine

on w_order_acceptance_queue.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_order_acceptance_queue.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")

If Message.nf_Get_App_ID() = 'PAS' Then
	dw_1.Object.accept_flag.Visible = False
	dw_1.Object.accept_flag_t.Visible = False
Else
	dw_1.Object.accept_flag_t.Visible = True
End If
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event open;call super::open;String ls_temp 

ls_temp = Message.StringParm	

If Len(ls_temp) > 1 and lower(ls_temp) <> 'w_order_acceptance-queue' Then
	is_order_number = mid(ls_temp,1,5)
	ib_open_inq_window = False
Else
	ib_open_inq_window = True
End If
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'order_number'
		dw_1.object.t_order_number.Text = as_value
		is_order_Number = as_value
End Choose
end event

event resize;call super::resize;constant integer li_x		= 20
constant integer li_y		= 50
  
dw_1.width	= newwidth - li_x
dw_1.height	= newheight - li_y

end event

event activate;call super::activate;If Message.nf_Get_App_ID() = 'PAS' Then
	iw_frame.im_menu.mf_disable('m_save')
Else
	iw_frame.im_menu.mf_enable('m_save')
End If
	
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'order_number'
		If isnull(dw_1.object.t_order_number.Text) Then
			Message.StringParm = ''
		Else			
			Message.StringParm = dw_1.object.t_order_number.Text
		End if
End choose

end event

type dw_1 from u_netwise_dw within w_order_acceptance_queue
integer y = 16
integer width = 5211
integer height = 1512
integer taborder = 10
string dataobject = "d_order_accept_queue"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild('queue', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

This.GetChild('status', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()
end event

