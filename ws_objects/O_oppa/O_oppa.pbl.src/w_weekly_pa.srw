﻿$PBExportHeader$w_weekly_pa.srw
$PBExportComments$Weekly PA Window
forward
global type w_weekly_pa from w_netwise_sheet
end type
type dw_product_status_window from datawindow within w_weekly_pa
end type
type dw_weekly_pa_detail from u_netwise_dw within w_weekly_pa
end type
type dw_plant from u_plant within w_weekly_pa
end type
type dw_division from u_division within w_weekly_pa
end type
type dw_week_end_date from u_week_end_date within w_weekly_pa
end type
type dw_loads_boxes from u_loads_boxes within w_weekly_pa
end type
type cbx_neg_selection from checkbox within w_weekly_pa
end type
end forward

global type w_weekly_pa from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 2665
integer height = 1324
string title = "Weekly PA "
long backcolor = 12632256
dw_product_status_window dw_product_status_window
dw_weekly_pa_detail dw_weekly_pa_detail
dw_plant dw_plant
dw_division dw_division
dw_week_end_date dw_week_end_date
dw_loads_boxes dw_loads_boxes
cbx_neg_selection cbx_neg_selection
end type
global w_weekly_pa w_weekly_pa

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
u_pas201		iu_pas201
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Long		il_color

Long		il_SelectedColor

Long		il_SelectedTextColor, &
         il_selected_status_max = 8


Boolean		ib_inquire, &
		ib_reinquire

Integer		ii_max_page_number
//1-13 jac
Integer	ii_dddw_x, ii_dddw_y
//
string		is_inquire_window_name, &
         is_selected_status_array


Datastore		ids_report

w_netwise_response	iw_inquirewindow, &
			iw_parentwindow
end variables

forward prototypes
public subroutine wf_format ()
public subroutine wf_filter ()
public function boolean wf_retrieve ()
public function string wf_get_selected_status ()
end prototypes

public subroutine wf_format ();string	ls_temp

if dw_loads_boxes.GetItemString(1,"boxes_loads") = 'B' Then
	ls_temp = dw_weekly_pa_detail.Modify("weekly_sum_1_comp.Format='##,##0' " + &
			"weekly_sum_2_comp.Format='##,##0' " + &
			"weekly_sum_3_comp.Format='##,##0' " + &
			"weekly_sum_4_comp.Format='##,##0' " + &
			"weekly_sum_5_comp.Format='##,##0' " + &
			"weekly_sum_6_comp.Format='##,##0' " + &
			"weekly_sum_7_comp.Format='##,##0' " + &
			"weekly_sum_8_comp.Format='##,##0' " + &
			"weekly_sum_9_comp.Format='##,##0' " + &
			"weekly_sum_10_comp.Format='##,##0' " + &
			"weekly_sum_11_comp.Format='##,##0' " + &
			"weekly_sum_12_comp.Format='##,##0' " + &
			"weekly_sum_13_comp.Format='##,##0' " + &
			"weekly_sum_14_comp.Format='##,##0' " + &
			"weekly_sum_15_comp.Format='##,##0' " + &
			"weekly_sum_16_comp.Format='##,##0' " + &
			"weekly_sum_17_comp.Format='##,##0' " + &
			"weekly_sum_18_comp.Format='##,##0' " + &
			"weekly_sum_19_comp.Format='##,##0' " + &
			"weekly_sum_20_comp.Format='##,##0' " + &
			"weekly_sum_21_comp.Format='##,##0' " + &
			"weekly_sum_22_comp.Format='##,##0' ")
	Else
		dw_weekly_pa_detail.Modify("weekly_sum_1_comp.Format='##0.00' " + &
			"weekly_sum_2_comp.Format='##0.00' " + &			
			"weekly_sum_3_comp.Format='##0.00' " + &			
			"weekly_sum_4_comp.Format='##0.00' " + &			
			"weekly_sum_5_comp.Format='##0.00' " + &			
			"weekly_sum_6_comp.Format='##0.00' " + &			
			"weekly_sum_7_comp.Format='##0.00' " + &			
			"weekly_sum_8_comp.Format='##0.00' " + &			
			"weekly_sum_9_comp.Format='##0.00' " + &			
			"weekly_sum_10_comp.Format='##0.00' " + &			
			"weekly_sum_11_comp.Format='##0.00' " + &			
			"weekly_sum_12_comp.Format='##0.00' " + &			
			"weekly_sum_13_comp.Format='##0.00' " + &			
			"weekly_sum_14_comp.Format='##0.00' " + &			
			"weekly_sum_15_comp.Format='##0.00' " + &			
			"weekly_sum_16_comp.Format='##0.00' " + &			
			"weekly_sum_17_comp.Format='##0.00' " + &			
			"weekly_sum_18_comp.Format='##0.00' " + &			
			"weekly_sum_19_comp.Format='##0.00' " + &			
			"weekly_sum_20_comp.Format='##0.00' " + &			
			"weekly_sum_21_comp.Format='##0.00' " + &
			"weekly_sum_22_comp.Format='##0.00' ")
	End If
end subroutine

public subroutine wf_filter ();Integer		li_counter

Long			ll_nbrrows

String		ls_filter, &
				ls_row_message

dw_weekly_pa_detail.SetRedraw(False)
If cbx_neg_selection.checked  Then
	ls_filter = "weekly_sum1 < 0 or weekly_sum2 < 0 or weekly_sum3 < 0 or " + &
					"weekly_sum4 < 0 or weekly_sum5 < 0 or weekly_sum6 < 0 or " + &
					"weekly_sum7 < 0 or weekly_sum8 < 0 or weekly_sum9 < 0 or " + &	
					"weekly_sum10 < 0 or weekly_sum11 < 0 or weekly_sum12 < 0 or " + &
					"weekly_sum13 < 0 or weekly_sum14 < 0 or weekly_sum15 < 0 or " + &
					"weekly_sum16 < 0 or weekly_sum17 < 0 or weekly_sum18 < 0 or " + &
					"weekly_sum19 < 0 or weekly_sum20 < 0 or weekly_sum21 < 0 or " + &
					"weekly_sum22 < 0 "
	ls_row_message = " Rows with negative values"
Else
	ls_filter = ""
	ls_row_message = " Rows Total"
End IF

dw_weekly_pa_detail.SetFilter(ls_filter)	
dw_weekly_pa_detail.Filter()
dw_weekly_pa_detail.sort()
ll_nbrrows = dw_weekly_pa_detail.RowCount()

// Set the highlighting again.
dw_weekly_pa_detail.SetItem(1,"back_color", il_SelectedColor)
dw_weekly_pa_detail.SetItem(1,"text_color", il_SelectedTextColor)

iw_frame.SetMicroHelp(String(ll_nbrrows) + ls_row_message)
dw_weekly_pa_detail.SetRedraw(True)

Return
end subroutine

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row

string		ls_division, &  
            ls_division_desc, &
				ls_plant, &
				ls_plant_desc, & 
				ls_loads_boxes, &
				ls_temp, &
				ls_week_end_date, &
				ls_weekly_data, &
				ls_header, & 
				ls_string_status, & 
				ls_product_status, &
				ls_string2, &
				ls_string

date			ldt_week_end_date

integer     ll_count, & 
            ll_counter, &
				ll_counter_max, & 
				ll_pos

u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
//1-13 jac
ls_string = Message.StringParm
If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False
//
If Not ib_inquire Then
	Return False
End If
//1-13 jac
//ll_pos = 0
//ll_counter = 1
//ll_counter_max = 3
//for ll_counter = 1 to ll_counter_max
//ll_pos = Pos(ls_string,"~t", ll_pos + 1)
//ll_counter = ll_counter +1 
//next

ls_string2 = Left(ls_string,iw_frame.iu_string.nf_nPos(ls_string,"~t",1,16))

//if dw_product_status_window.ImportString(Mid(ls_string,ll_pos + 1,iw_frame.iu_string.nf_nPos(ls_string,"~t",ll_pos + 1,8)-1)) < 1  &
//                                Then Return False
dw_product_status_window.ImportString(ls_string2)
dw_product_status_window.AcceptText()
//

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp73br_inq_weekly_pa"
istr_error_info.se_message = Space(71)


				
ls_division = dw_division.uf_get_division()
ls_division_desc = dw_division.uf_get_division_descr()

ls_plant = dw_plant.uf_get_plant_code()
ls_plant_desc = dw_plant.uf_get_plant_descr()  

ls_loads_boxes = dw_loads_boxes.uf_get_loads_boxes()

ls_week_end_date = string(dw_week_end_date.uf_get_week_end_date(), "yyyy-mm-dd" )
dw_weekly_pa_detail.object.weekly_sum1_date.expression = ls_week_end_date


//1-13 jac moved week_end_date 
ls_header = ls_plant + '~t' + &
				ls_division + '~t' + &
				ls_week_end_date + '~t'
				

for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) then
		ls_product_status = " "
	end if
   ls_header += ls_product_status + "~t"
next

ls_header += ls_header + "~r~n"

//

dw_weekly_pa_detail.reset()

If iu_ws_pas_share.nf_pasp73fr(istr_error_info, & 
										ls_header, &
										ls_weekly_data) < 0 Then
										This.SetRedraw(True) 
										Return False
End If										

//ls_weekly_data = 'd0101dm~ttrim~t80~t-500~t2~t3~t4~t-55555~t6~t7~t8~t9~t10~t11~t12~t13~t14~t15~t16~t17~t18~t19~t20~t21~t22~r~n' + &
//					  'D01014DMCE*~tMORETRIMMING~t80~t1~t2~t3~t99999~t5~t6~t7~t8~t9~t10~t11~t12~t13~t14~t15~t16~t17~t18~t19~t20~t21~t22~r~n' //+ &
					  
//MessageBox('Return Data',string(ls_weekly_data))

If Not lu_string.nf_IsEmpty(ls_weekly_data) Then
	ll_rec_count = dw_weekly_pa_detail.ImportString(ls_weekly_Data)
	If ll_rec_count > 0 Then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

              
FOR ll_row = 1 TO ll_rec_count
	dw_weekly_pa_detail.SetItem ( ll_row, "loads_boxes", ls_loads_boxes)
	dw_weekly_pa_detail.SetItem ( ll_row, "back_color", il_selectedcolor)
	dw_weekly_pa_detail.SetItem ( ll_row, "text_color", il_selectedtextcolor)
NEXT 

wf_format()

wf_filter()

dw_weekly_pa_detail.object.last_week_end_date.expression =  &
				string(dw_week_end_date.uf_get_last_week_end_date(), "yyyy-mm-dd")

// Must do because of the reinquire to set the scroll bar
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'
//1-13 jac 
//dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '966'
//dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '966'
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '1100'
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '1100'
//
This.SetRedraw(True) 

dw_weekly_pa_detail.ResetUpdate()
dw_weekly_pa_detail.SetFocus()
 
Return True

end function

public function string wf_get_selected_status ();//1-13 jac
long  ll_count
string ls_product_status, & 
		ls_stat_desc, &
       ls_input 

is_selected_status_array = ''
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if ll_count = 1 then
		If isnull(ls_product_status) THEN
		   ls_product_status = 'ALL'
		end if 
	ELSE 
	  If isnull(ls_product_status) Then
	     ls_product_status = " "
	  end if
	end if
	is_selected_status_array += ls_product_status + "~t"
   ls_stat_desc = dw_product_status_window.GetItemString(1, "status_desc" + string(ll_count))
   ls_stat_desc = TRIM(ls_stat_desc)
	if ll_count = 1 then
		If isnull(ls_stat_desc) Then
		   ls_stat_desc = 'ALL'
	   end if
	ELSE
	  If isnull(ls_stat_desc) Then
		ls_stat_desc = " "
	  end if
   END IF
	is_selected_status_array += ls_stat_desc + "~t"
next


Return is_selected_status_array
//



end function

on w_weekly_pa.create
int iCurrent
call super::create
this.dw_product_status_window=create dw_product_status_window
this.dw_weekly_pa_detail=create dw_weekly_pa_detail
this.dw_plant=create dw_plant
this.dw_division=create dw_division
this.dw_week_end_date=create dw_week_end_date
this.dw_loads_boxes=create dw_loads_boxes
this.cbx_neg_selection=create cbx_neg_selection
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_status_window
this.Control[iCurrent+2]=this.dw_weekly_pa_detail
this.Control[iCurrent+3]=this.dw_plant
this.Control[iCurrent+4]=this.dw_division
this.Control[iCurrent+5]=this.dw_week_end_date
this.Control[iCurrent+6]=this.dw_loads_boxes
this.Control[iCurrent+7]=this.cbx_neg_selection
end on

on w_weekly_pa.destroy
call super::destroy
destroy(this.dw_product_status_window)
destroy(this.dw_weekly_pa_detail)
destroy(this.dw_plant)
destroy(this.dw_division)
destroy(this.dw_week_end_date)
destroy(this.dw_loads_boxes)
destroy(this.cbx_neg_selection)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')

iw_frame.im_menu.mf_enable('m_print')


end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')




end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288
// 
//dw_weekly_pa_detail.width	= newwidth - (30 + li_x)
//dw_weekly_pa_detail.height	= newheight - (30 + li_y)

integer li_x		= 27
integer li_y		= 278


if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_weekly_pa_detail.width  = newwidth - (li_x)
dw_weekly_pa_detail.height = newheight - (li_y)

// Must do because of the split horizonal bar 
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'
//1-13 jac
//dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '966'
//dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '966'
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '1100'
dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '1100'
//

end event

event ue_fileprint;call super::ue_fileprint;String						ls_mod, &
								ls_temp, &
								ls_week_end_date

DataStore					lds_print


lds_print = create u_print_datastore
lds_print.DataObject = 'd_weekly_pa_report'

lds_print.object.plant_code_t.text = dw_plant.uf_Get_plant_code() 
lds_print.object.plant_descr_t.text = dw_plant.uf_Get_plant_descr()   
lds_print.object.division_code_t.text = dw_division.uf_get_division()
lds_print.object.division_descr_t.text = dw_division.uf_get_division_descr()
ls_week_end_date = string(dw_week_end_date.uf_get_week_end_date(), "yyyy-mm-dd" )
lds_print.object.weekly_sum1_date.expression = ls_week_end_date
lds_print.object.last_week_end_date.expression =  &
				string(dw_week_end_date.uf_get_last_week_end_date(), "yyyy-mm-dd")



dw_weekly_pa_detail.sharedata(lds_print)

If dw_loads_boxes.uf_Get_loads_boxes() = 'B' Then
	ls_mod ="weekly_sum_1_comp.Format='##,##0' " + &
			"weekly_sum_2_comp.Format='##,##0' " + &
			"weekly_sum_3_comp.Format='##,##0' " + &
			"weekly_sum_4_comp.Format='##,##0' " + &
			"weekly_sum_5_comp.Format='##,##0' " + &
			"weekly_sum_6_comp.Format='##,##0' " + &
			"weekly_sum_7_comp.Format='##,##0' " + &
			"weekly_sum_8_comp.Format='##,##0' " + &
			"weekly_sum_9_comp.Format='##,##0' " + &
			"weekly_sum_10_comp.Format='##,##0' " + &
			"weekly_sum_11_comp.Format='##,##0' " + &
			"weekly_sum_12_comp.Format='##,##0' " + &
			"weekly_sum_13_comp.Format='##,##0' " + &
			"weekly_sum_14_comp.Format='##,##0' " + &
			"weekly_sum_15_comp.Format='##,##0' " + &
			"weekly_sum_16_comp.Format='##,##0' " + &
			"weekly_sum_17_comp.Format='##,##0' " + &
			"weekly_sum_18_comp.Format='##,##0' " + &
			"weekly_sum_19_comp.Format='##,##0' " + &
			"weekly_sum_20_comp.Format='##,##0' " + &
			"weekly_sum_21_comp.Format='##,##0' " + &
			"weekly_sum_22_comp.Format='##,##0' "
Else
		ls_mod ="weekly_sum_1_comp.Format='##0.00' " + &
			"weekly_sum_2_comp.Format='##0.00' " + &			
			"weekly_sum_3_comp.Format='##0.00' " + &			
			"weekly_sum_4_comp.Format='##0.00' " + &			
			"weekly_sum_5_comp.Format='##0.00' " + &			
			"weekly_sum_6_comp.Format='##0.00' " + &			
			"weekly_sum_7_comp.Format='##0.00' " + &			
			"weekly_sum_8_comp.Format='##0.00' " + &			
			"weekly_sum_9_comp.Format='##0.00' " + &			
			"weekly_sum_10_comp.Format='##0.00' " + &			
			"weekly_sum_11_comp.Format='##0.00' " + &			
			"weekly_sum_12_comp.Format='##0.00' " + &			
			"weekly_sum_13_comp.Format='##0.00' " + &			
			"weekly_sum_14_comp.Format='##0.00' " + &			
			"weekly_sum_15_comp.Format='##0.00' " + &			
			"weekly_sum_16_comp.Format='##0.00' " + &			
			"weekly_sum_17_comp.Format='##0.00' " + &			
			"weekly_sum_18_comp.Format='##0.00' " + &			
			"weekly_sum_19_comp.Format='##0.00' " + &			
			"weekly_sum_20_comp.Format='##0.00' " + &			
			"weekly_sum_21_comp.Format='##0.00' " + &
			"weekly_sum_22_comp.Format='##0.00' " 
End If

ls_temp = lds_print.modify(ls_mod)

lds_print.print()




end event

event ue_get_data;date		ldt_date	

Choose Case as_value
	Case 'division'
		message.StringParm = dw_division.uf_get_division()
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'week_end_date'
		ldt_date = dw_week_end_date.uf_get_week_end_date()
		message.StringParm = string(ldt_date) 
		//dw_week_end_date.uf_get_week_end_date()
	Case 'loads_boxes'
		message.StringParm = dw_loads_boxes.uf_get_loads_boxes()
//1-13 jac
	Case "product_status"
		message.StringParm = ''
	Case "selected"
		message.StringParm = is_selected_status_array
 	Case "dddwxpos"
		message.StringParm = string(ii_dddw_x)
	case "dddwypos"
		message.StringParm = string(ii_dddw_x)
//
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env


GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

//ib_reinquire = FALSE

This.PostEvent('ue_query')//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Weekpa"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_weekly_pa_inq'

iu_pas201 = Create u_pas201
iu_ws_pas_share = Create u_ws_pas_share

wf_retrieve()

end event

event ue_set_data;u_conversion_functions		lu_conv


Choose Case as_data_item
	Case 'division'
		dw_division.uf_set_division(as_value)
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'week_end_date'
		dw_week_end_date.uf_set_week_end_date(Date(as_value))
	Case 'loads_boxes'
		dw_loads_boxes.uf_set_loads_boxes(as_value)
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)
End Choose
end event

event close;call super::close;Destroy iu_ws_pas_share
end event

type dw_product_status_window from datawindow within w_weekly_pa
boolean visible = false
integer x = 2391
integer y = 40
integer width = 686
integer height = 80
integer taborder = 40
string title = "none"
string dataobject = "d_product_status_2"
boolean border = false
boolean livescroll = true
end type

event constructor;//ib_updateable = False
InsertRow(0)
end event

type dw_weekly_pa_detail from u_netwise_dw within w_weekly_pa
integer y = 280
integer width = 2523
integer height = 928
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_weekly_pa_detail"
boolean minbox = true
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = False
end event

type dw_plant from u_plant within w_weekly_pa
integer x = 251
integer width = 1582
integer taborder = 40
end type

event ue_postconstructor;call super::ue_postconstructor;This.disable()
end event

type dw_division from u_division within w_weekly_pa
integer x = 219
integer y = 80
integer taborder = 30
end type

event constructor;call super::constructor;This.disable()
end event

type dw_week_end_date from u_week_end_date within w_weekly_pa
event ue_post_constructor ( )
integer y = 168
integer width = 923
integer height = 92
integer taborder = 20
end type

event constructor;call super::constructor;Date		ldt_week_end_date

This.uf_enable(False)
This.insertrow(0)
//Find Sunday
ldt_week_end_date = RelativeDate(Today(), 1 - DayNumber( &
			Today())) 
//Default Three Weeks out 
ldt_week_end_date = RelativeDate(ldt_week_end_date, 21)
This.uf_set_week_end_date(ldt_week_end_date)
This.uf_set_week_end_type('W')
end event

type dw_loads_boxes from u_loads_boxes within w_weekly_pa
integer x = 1934
integer height = 276
integer taborder = 40
boolean bringtotop = true
end type

event itemchanged;Long					ll_count


For ll_count = 1 to dw_weekly_pa_detail.RowCount()
	dw_weekly_pa_detail.SetItem(ll_count,"loads_boxes", data)
Next
Parent.Post wf_format()

end event

type cbx_neg_selection from checkbox within w_weekly_pa
integer x = 1097
integer y = 176
integer width = 722
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Display Negative Rows Only:  "
boolean lefttext = true
end type

event clicked;wf_filter()
end event

