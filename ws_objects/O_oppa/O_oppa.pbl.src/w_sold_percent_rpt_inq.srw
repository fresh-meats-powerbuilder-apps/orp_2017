﻿$PBExportHeader$w_sold_percent_rpt_inq.srw
forward
global type w_sold_percent_rpt_inq from w_netwise_response
end type
type dw_plant from u_plant within w_sold_percent_rpt_inq
end type
type dw_division from u_division within w_sold_percent_rpt_inq
end type
end forward

global type w_sold_percent_rpt_inq from w_netwise_response
int Width=2025
int Height=560
long BackColor=12632256
dw_plant dw_plant
dw_division dw_division
end type
global w_sold_percent_rpt_inq w_sold_percent_rpt_inq

type variables
boolean			ib_valid_to_close

w_sold_percent_rpt		iw_parent
end variables

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
				ls_division, &
				ls_string
				

If dw_division.AcceptText() = -1 Then return
If dw_plant.AcceptText() = -1 Then return

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End If

ls_plant = dw_plant.uf_get_plant_code()
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End If

iw_parent.Event ue_set_data('division',ls_division)
iw_parent.Event ue_set_data('plant',ls_plant)

ls_string = 'ok to inquire'
ib_valid_to_close = True
CloseWithReturn(This, ls_string)

end event

event ue_postopen;call super::ue_postopen;iw_parent.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)

iw_parent.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)


end event

on w_sold_percent_rpt_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_division
end on

on w_sold_percent_rpt_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_division)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event close;call super::close;If Not ib_valid_to_close Then Message.StringParm = ""
end event

event open;call super::open;iw_parent = Message.PowerObjectParm

This.Title = iw_parent.Title + " Inquire"


end event

type cb_base_help from w_netwise_response`cb_base_help within w_sold_percent_rpt_inq
int X=1669
int Y=284
int TabOrder=40
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_sold_percent_rpt_inq
int X=1669
int Y=160
int TabOrder=30
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_sold_percent_rpt_inq
int X=1669
int Y=36
int TabOrder=20
end type

type dw_plant from u_plant within w_sold_percent_rpt_inq
int X=46
int Y=184
int Width=1477
int TabOrder=10
end type

type dw_division from u_division within w_sold_percent_rpt_inq
int X=14
int Y=96
int TabOrder=2
end type

