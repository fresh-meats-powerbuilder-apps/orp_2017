﻿$PBExportHeader$w_age_avail_inq.srw
$PBExportComments$Age Availability Inquire Window
forward
global type w_age_avail_inq from w_netwise_response
end type
type dw_product_state from u_product_state within w_age_avail_inq
end type
type dw_plant from u_plant within w_age_avail_inq
end type
type st_or from statictext within w_age_avail_inq
end type
type st_location_group from statictext within w_age_avail_inq
end type
type cbx_plant from u_base_checkbox_ext within w_age_avail_inq
end type
type cbx_location_group from u_base_checkbox_ext within w_age_avail_inq
end type
type dw_division from u_division within w_age_avail_inq
end type
type cbx_division from u_base_checkbox_ext within w_age_avail_inq
end type
type cbx_product_state from u_base_checkbox_ext within w_age_avail_inq
end type
type st_or2 from statictext within w_age_avail_inq
end type
type st_product_group from statictext within w_age_avail_inq
end type
type cbx_product from u_base_checkbox_ext within w_age_avail_inq
end type
type cbx_product_group from u_base_checkbox_ext within w_age_avail_inq
end type
type dw_product from u_fab_product_code within w_age_avail_inq
end type
type dw_product_status from u_product_status within w_age_avail_inq
end type
type cbx_product_status from u_base_checkbox_ext within w_age_avail_inq
end type
type uo_location_groups from u_group_list within w_age_avail_inq
end type
type uo_product_groups from u_group_list within w_age_avail_inq
end type
type dw_day_range from u_base_dw_ext within w_age_avail_inq
end type
end forward

global type w_age_avail_inq from w_netwise_response
integer x = 5
integer y = 376
integer width = 1801
integer height = 2552
long backcolor = 67108864
dw_product_state dw_product_state
dw_plant dw_plant
st_or st_or
st_location_group st_location_group
cbx_plant cbx_plant
cbx_location_group cbx_location_group
dw_division dw_division
cbx_division cbx_division
cbx_product_state cbx_product_state
st_or2 st_or2
st_product_group st_product_group
cbx_product cbx_product
cbx_product_group cbx_product_group
dw_product dw_product
dw_product_status dw_product_status
cbx_product_status cbx_product_status
uo_location_groups uo_location_groups
uo_product_groups uo_product_groups
dw_day_range dw_day_range
end type
global w_age_avail_inq w_age_avail_inq

type variables
Boolean			ib_ok_to_close, &
			ib_inquire

w_age_avail		iw_parentwindow


end variables

on w_age_avail_inq.create
int iCurrent
call super::create
this.dw_product_state=create dw_product_state
this.dw_plant=create dw_plant
this.st_or=create st_or
this.st_location_group=create st_location_group
this.cbx_plant=create cbx_plant
this.cbx_location_group=create cbx_location_group
this.dw_division=create dw_division
this.cbx_division=create cbx_division
this.cbx_product_state=create cbx_product_state
this.st_or2=create st_or2
this.st_product_group=create st_product_group
this.cbx_product=create cbx_product
this.cbx_product_group=create cbx_product_group
this.dw_product=create dw_product
this.dw_product_status=create dw_product_status
this.cbx_product_status=create cbx_product_status
this.uo_location_groups=create uo_location_groups
this.uo_product_groups=create uo_product_groups
this.dw_day_range=create dw_day_range
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_state
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.st_or
this.Control[iCurrent+4]=this.st_location_group
this.Control[iCurrent+5]=this.cbx_plant
this.Control[iCurrent+6]=this.cbx_location_group
this.Control[iCurrent+7]=this.dw_division
this.Control[iCurrent+8]=this.cbx_division
this.Control[iCurrent+9]=this.cbx_product_state
this.Control[iCurrent+10]=this.st_or2
this.Control[iCurrent+11]=this.st_product_group
this.Control[iCurrent+12]=this.cbx_product
this.Control[iCurrent+13]=this.cbx_product_group
this.Control[iCurrent+14]=this.dw_product
this.Control[iCurrent+15]=this.dw_product_status
this.Control[iCurrent+16]=this.cbx_product_status
this.Control[iCurrent+17]=this.uo_location_groups
this.Control[iCurrent+18]=this.uo_product_groups
this.Control[iCurrent+19]=this.dw_day_range
end on

on w_age_avail_inq.destroy
call super::destroy
destroy(this.dw_product_state)
destroy(this.dw_plant)
destroy(this.st_or)
destroy(this.st_location_group)
destroy(this.cbx_plant)
destroy(this.cbx_location_group)
destroy(this.dw_division)
destroy(this.cbx_division)
destroy(this.cbx_product_state)
destroy(this.st_or2)
destroy(this.st_product_group)
destroy(this.cbx_product)
destroy(this.cbx_product_group)
destroy(this.dw_product)
destroy(this.dw_product_status)
destroy(this.cbx_product_status)
destroy(this.uo_location_groups)
destroy(this.uo_product_groups)
destroy(this.dw_day_range)
end on

event close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF

end event

event open;call super::open;If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
		This.Title = iw_parentwindow.Title + " Inquire"
	End If
End If

end event

event ue_base_cancel;Close(This)
end event

event ue_base_ok;String	ls_plant, &
			ls_loc_system, &
			ls_loc_group, &
			ls_loc_desc, &
			ls_division, &
			ls_product_state, &
			ls_product_status, &
			ls_product, &
			ls_product_desc, &
			ls_prod_system, &
			ls_prod_group, &
			ls_prod_desc, &
			ls_return_string, &
			ls_checked_boxes, &
			ls_from_range, &
			ls_to_range

u_string_functions		lu_strings

dw_plant.AcceptText()
dw_division.AcceptText()
dw_product_state.AcceptText()
dw_product_status.AcceptText()
dw_product.AcceptText()
dw_day_range.AcceptText()

If cbx_plant.checked = True Then
	ls_plant = dw_plant.uf_get_plant_code()
	If lu_strings.nf_IsEmpty(ls_plant) then
		MessageBox('Plant Code Error', "Plant Code is Missing")
		dw_plant.SetFocus()
		Return
	End If 
Else
	ls_plant = '   '
End If

If cbx_location_group.checked = True Then
	//ls_loc_system = string(ole_location_group.object.systemname())
	ls_loc_system = uo_location_groups.uf_get_owner()
	//ls_loc_group = string(ole_location_group.object.groupID())
	ls_loc_group = string(uo_location_groups.uf_get_sel_id(ls_loc_group))
	//ls_loc_desc = string(ole_location_group.object.GroupDescription())
	//ls_loc_desc = string(uo_location_groups.uf_get_sel_desc(ls_loc_desc))
	uo_location_groups.uf_get_sel_desc(ls_loc_desc)
Else
	ls_loc_system = '     '
	ls_loc_group = '0'
	ls_loc_desc = ''
End If

If cbx_division.checked = True Then
	ls_division = dw_division.GetItemString(1, "division_code")
	If isnull(ls_division) or ls_division <= '  ' Then
		MessageBox('Division Code Error', "Division Code is Missing")
		dw_division.SetFocus()
		Return
	End If 
Else
	ls_division = '  '
End If

If cbx_product_state.checked = True Then
	ls_product_state = dw_product_state.uf_get_product_state()
	If lu_strings.nf_IsEmpty(ls_product_state) Then
		iw_frame.SetMicroHelp("Product State is a required field")
		dw_product_state.SetFocus()
		return
	End If 
Else
	ls_product_state = ' '
End If

If cbx_product_status.checked = True Then
	ls_product_status = dw_product_status.uf_get_product_status()
	If lu_strings.nf_IsEmpty(ls_product_status) Then
		iw_frame.SetMicroHelp("Product Status is a required field")
		dw_product_status.SetFocus()
		return
	End If 
Else
	ls_product_status = ' '
End If

If cbx_product.checked = True Then
	ls_product = dw_product.GetItemString(1, "fab_product_code")
	ls_product_desc = dw_product.GetItemString(1, "fab_product_description")
	If lu_strings.nf_IsEmpty(ls_product) Then
		iw_frame.SetMicroHelp("Product is a required field")
		dw_product.SetFocus()
		return
	End If 
Else
	ls_product = '          '
End If
	
If cbx_product_group.checked = True Then
	//ls_prod_system = string(ole_product_group.object.systemname())
	ls_prod_system = uo_product_groups.uf_get_owner()
	//ls_prod_group = string(ole_product_group.object.groupID())
	ls_prod_group = string(uo_product_groups.uf_get_sel_id(ls_prod_group))
	//ls_prod_desc = string(ole_product_group.object.GroupDescription())
	uo_product_groups.uf_get_sel_desc(ls_prod_desc)
Else
	ls_prod_system = '     '
	ls_prod_group = '0'
	ls_prod_desc = ''
End If	

If dw_day_range.GetItemNumber(1, "from_range") >= dw_day_range.GetItemNumber(1, "to_range") Then
	iw_frame.SetMicroHelp("From days must be less than to days")
	dw_day_range.SetFocus()
	Return
Else	
	If dw_day_range.GetItemNumber(1, "to_range") - dw_day_range.GetItemNumber(1, "from_range") > 18 Then
		iw_frame.SetMicroHelp("Day range cannot be greater than 18 days")
		dw_day_range.SetFocus()
		Return
	Else
		If dw_day_range.GetItemNumber(1, "to_range") - dw_day_range.GetItemNumber(1, "from_range") < 1 Then 
			iw_frame.SetMicroHelp("Day range cannot be less than 1 day")
			dw_day_range.SetFocus()
			Return		
		End If
	End if
End If

ls_from_range = String(dw_day_range.GetItemNumber(1, "from_range"))
ls_to_range = String(dw_day_range.GetItemNumber(1, "to_range"))
	
iw_parentwindow.Event ue_set_data('plant',ls_plant)	
iw_parentwindow.Event ue_set_data('loc_system',ls_loc_system)
iw_parentwindow.Event ue_set_data('loc_group',ls_loc_group)
iw_parentwindow.Event ue_set_data('loc_desc',ls_loc_desc)
iw_parentwindow.Event ue_set_data('division',ls_division)
iw_parentwindow.Event ue_set_data('product_state',ls_product_state)
iw_parentwindow.Event ue_set_data('product_status',ls_product_status)
iw_parentwindow.Event ue_set_data('product',ls_product)
iw_parentwindow.Event ue_set_data('product_desc',ls_product_desc)
iw_parentwindow.Event ue_set_data('prod_system',ls_prod_system)
iw_parentwindow.Event ue_set_data('prod_group',ls_prod_group)
iw_parentwindow.Event ue_set_data('prod_desc',ls_prod_desc)
iw_parentwindow.Event ue_set_data('from_range',ls_from_range)
SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "age_from_range", ls_from_range)
iw_parentwindow.Event ue_set_data('to_range',ls_to_range)
SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "age_to_range", ls_to_range)


//SetProfileString("

ls_return_string  = ls_plant +'~t'
ls_return_string += ls_loc_system +'~t'
ls_return_string += ls_loc_group +'~t'
ls_return_string += ls_division+'~t'
ls_return_string += ls_product_state +'~t'
ls_return_string += ls_product_status +'~t'
ls_return_string += ls_product +'~t'
ls_return_string += ls_prod_system +'~t'
ls_return_string += ls_prod_group +'~t'
ls_return_string += ls_from_range +'~t'
ls_return_string += ls_to_range +'~r~n'

ls_checked_boxes = ''
If cbx_plant.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If	

If cbx_location_group.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_division.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_product_state.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_product_status.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_product.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_product_group.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

iw_parentwindow.Event ue_set_data('checked_boxes',ls_checked_boxes)
iw_parentwindow.Event ue_set_data('return_string',ls_return_string)

ib_ok_to_close = True

Close(This)
        

end event

event ue_postopen;
string	ls_prod, &
			ls_checked_boxes, &
			ls_prod_desc, &
			ls_from_days, &
			ls_to_days, &
			ls_save_auth, &
			ls_group_id

dw_product.uf_enable(true)
dw_product.uf_enable_state(false)
dw_product.uf_enable_status(false)

If dw_product.RowCount() < 1 Then
	dw_product.InsertRow(0)
End If

iw_parentwindow.Event ue_get_data('product_state')
dw_product_state.uf_set_product_state(Message.StringParm)

iw_parentwindow.Event ue_get_data('product_status')
dw_product_status.uf_set_product_status(Message.StringParm)

iw_parentwindow.Event ue_get_data('product')
dw_product.uf_set_product_code(Message.StringParm)
iw_parentwindow.Event ue_get_data('product_desc')
dw_product.uf_set_product_desc(Message.StringParm)

ls_prod = dw_product.GetItemString(1, 'fab_product_code') + "~t" + &
					dw_product.GetItemString(1, 'fab_product_description') + "~t" + &
					dw_product.GetItemString(1, 'product_state') + "~t" + &
					dw_product.GetItemString(1, 'product_state_description')  + "~t" + &
					dw_product.GetItemString(1, 'product_status') + "~t" + &
					dw_product.GetItemString(1, 'product_status_description')
								
If Not iw_frame.iu_string.nf_IsEmpty(ls_prod) Then 
	dw_product.Reset()
	dw_product.uf_importstring(ls_prod, TRUE)
End If

//ORP has issue with spaces in product code, clear those out
If (dw_product.GetItemString(1, "fab_product_code") <= '          ') Then
	dw_product.SetItem(1, "fab_product_code", "")
End If

If Message.nf_Get_App_ID() = 'ORP' Then
	dw_day_range.SetItem(1, "from_range", 0)
	dw_day_range.SetItem(1, "to_range", 11)
	dw_day_range.Object.from_range.Protect = True
	dw_day_range.Object.to_range.Protect = True
	dw_day_range.Object.from_range.background.color = 12632256
	dw_day_range.Object.to_range.background.color = 12632256
Else
	ls_group_id = iw_frame.iu_netwise_data.is_groupid
	ls_save_auth = 'N'
	
	  SELECT group_profile.modify_auth
		 INTO :ls_save_auth 
		 FROM group_profile  
		WHERE ( group_profile.appid = 'PRD' ) AND  
				( group_profile.group_id = :ls_group_id ) AND  
				( group_profile.window_name = 'W_AGE_AVAIL_RANGE' ) AND  
				( group_profile.menuid = 'M_AGEAVAILABILITYSUMMARY_RANGE' )   ;	
				
	if ls_save_auth = 'Y' Then			
	
		dw_day_range.Object.from_range.Protect = False
		dw_day_range.Object.to_range.Protect = False
		dw_day_range.Object.from_range.background.color = 16777215
		dw_day_range.Object.to_range.background.color = 16777215
		iw_parentwindow.Event ue_get_data('from_range')
		ls_from_days = Message.StringParm
		iw_parentwindow.Event ue_get_data('to_range')
		ls_to_days = Message.StringParm
		If ls_from_days = "0" and ls_to_days = "0" Then
			ls_from_days = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "age_from_range", "0")
			ls_to_days = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "age_to_range", "0")
			If ls_from_days = "0" and ls_to_days = "0" Then
				dw_day_range.SetItem(1, "from_range", -1)
				dw_day_range.SetItem(1, "to_range", 11)
			Else
				dw_day_range.SetItem(1, "from_range", Integer(ls_from_days))
				dw_day_range.SetItem(1, "to_range", Integer(ls_to_days))
			End iF
		Else
			dw_day_range.SetItem(1, "from_range", Integer(ls_from_days))
			dw_day_range.SetItem(1, "to_range", Integer(ls_to_days))		
		End If
	Else	
		dw_day_range.SetItem(1, "from_range", 0)
		dw_day_range.SetItem(1, "to_range", 11)
		dw_day_range.Object.from_range.Protect = True
		dw_day_range.Object.to_range.Protect = True
		dw_day_range.Object.from_range.background.color = 12632256
		dw_day_range.Object.to_range.background.color = 12632256		
	End If	
End If	
			
dw_product.uf_set_product_state('1')
dw_product.uf_set_product_status('G')

//ole_location_group.object.GroupType(1)
//ole_location_group.object.LoadObject()

//ole_product_group.object.GroupType(3)
//ole_product_group.object.LoadObject()

uo_location_groups.uf_load_groups('L')
uo_product_groups.uf_load_groups('P')
iw_parentwindow.Event ue_get_data('checked_boxes')
ls_checked_boxes = Message.StringParm

If ls_checked_boxes = '' Then
	ls_checked_boxes = 'NNYYYNN'
End If
	
If mid(ls_checked_boxes,1,1) = 'Y' Then
	cbx_plant.Checked = True
Else	
	cbx_plant.Checked = False
End If

If mid(ls_checked_boxes,2,1) = 'Y' Then
	cbx_location_group.Checked = True
Else	
	cbx_location_group.Checked = False
End If	

If mid(ls_checked_boxes,3,1) = 'Y' Then
	cbx_division.Checked = True
Else	
	cbx_division.Checked = False
End If	

If mid(ls_checked_boxes,4,1) = 'Y' Then
	cbx_product_state.Checked = True
Else	
	cbx_product_state.Checked = False
End If	

If mid(ls_checked_boxes,5,1) = 'Y' Then
	cbx_product_status.Checked = True
Else	
	cbx_product_status.Checked = False
End If

If mid(ls_checked_boxes,6,1) = 'Y' Then
	cbx_product.Checked = True
Else	
	cbx_product.Checked = False
End If	

If mid(ls_checked_boxes,7,1) = 'Y' Then
	cbx_product_group.Checked = True
Else	
	cbx_product_group.Checked = False
End If





end event

type cb_base_help from w_netwise_response`cb_base_help within w_age_avail_inq
integer x = 1481
integer y = 2328
integer taborder = 160
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_age_avail_inq
integer x = 1193
integer y = 2328
integer taborder = 150
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_age_avail_inq
integer x = 910
integer y = 2328
integer taborder = 140
end type

type dw_product_state from u_product_state within w_age_avail_inq
event ue_postconstructor ( )
integer x = 206
integer y = 936
integer taborder = 90
boolean bringtotop = true
end type

event ue_postconstructor();This.uf_Enable(True)
This.SetItem(1,'product_state', '1')
end event

event constructor;call super::constructor;This.PostEvent('ue_postconstructor')
end event

type dw_plant from u_plant within w_age_avail_inq
integer x = 169
integer y = 32
integer height = 92
integer taborder = 20
boolean bringtotop = true
end type

type st_or from statictext within w_age_avail_inq
integer x = 238
integer y = 108
integer width = 78
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "or"
boolean focusrectangle = false
end type

type st_location_group from statictext within w_age_avail_inq
integer x = 201
integer y = 176
integer width = 357
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Location Group"
boolean focusrectangle = false
end type

type cbx_plant from u_base_checkbox_ext within w_age_avail_inq
integer x = 64
integer y = 40
integer width = 82
integer taborder = 10
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_plant.Checked = TRUE Then
	cbx_location_group.Checked = False
End If
	
end event

type cbx_location_group from u_base_checkbox_ext within w_age_avail_inq
integer x = 64
integer y = 172
integer width = 91
integer taborder = 40
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_location_group.Checked = TRUE Then
	//uo_location_groups.uf_load_groups('L')
	cbx_plant.Checked = False
End If
end event

type dw_division from u_division within w_age_avail_inq
integer x = 187
integer y = 844
integer taborder = 70
boolean bringtotop = true
end type

type cbx_division from u_base_checkbox_ext within w_age_avail_inq
integer x = 69
integer y = 848
integer width = 91
integer taborder = 60
boolean bringtotop = true
string text = ""
end type

type cbx_product_state from u_base_checkbox_ext within w_age_avail_inq
integer x = 69
integer y = 940
integer width = 87
integer taborder = 80
boolean bringtotop = true
string text = ""
end type

type st_or2 from statictext within w_age_avail_inq
integer x = 256
integer y = 1156
integer width = 78
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "or"
boolean focusrectangle = false
end type

type st_product_group from statictext within w_age_avail_inq
integer x = 215
integer y = 1320
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Product Group"
boolean focusrectangle = false
end type

type cbx_product from u_base_checkbox_ext within w_age_avail_inq
integer x = 69
integer y = 1172
integer width = 82
integer taborder = 100
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_product.Checked = TRUE Then
	cbx_product_group.Checked = False
End If
end event

type cbx_product_group from u_base_checkbox_ext within w_age_avail_inq
integer x = 69
integer y = 1304
integer width = 87
integer taborder = 120
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_product_group.Checked = TRUE Then
	//uo_product_groups.uf_load_groups('P')
	cbx_product.Checked = False
End If
end event

type dw_product from u_fab_product_code within w_age_avail_inq
integer x = 178
integer y = 1144
integer height = 96
integer taborder = 110
boolean bringtotop = true
end type

type dw_product_status from u_product_status within w_age_avail_inq
event ue_postconstructor ( )
integer x = 174
integer y = 1020
integer width = 965
integer taborder = 30
boolean bringtotop = true
end type

event ue_postconstructor();This.uf_Enable(True)
This.SetItem(1,'product_status', 'G')
end event

event constructor;call super::constructor;This.PostEvent('ue_postconstructor')
end event

type cbx_product_status from u_base_checkbox_ext within w_age_avail_inq
integer x = 64
integer y = 1020
integer width = 101
boolean bringtotop = true
string text = ""
end type

type uo_location_groups from u_group_list within w_age_avail_inq
integer x = 352
integer y = 260
integer width = 1349
integer height = 552
integer taborder = 50
boolean bringtotop = true
end type

on uo_location_groups.destroy
call u_group_list::destroy
end on

type uo_product_groups from u_group_list within w_age_avail_inq
integer x = 302
integer y = 1380
integer taborder = 130
boolean bringtotop = true
end type

on uo_product_groups.destroy
call u_group_list::destroy
end on

type dw_day_range from u_base_dw_ext within w_age_avail_inq
integer x = 293
integer y = 2032
integer width = 1307
integer height = 84
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_age_avail_range"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)


	
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Dw_age_avail_inq.bin 
2B00000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000685034c001ce323700000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000685034c001ce3237685034c001ce3237000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
21ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
22ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000685034c001ce323700000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000685034c001ce3237685034c001ce3237000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Dw_age_avail_inq.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
