﻿$PBExportHeader$w_sold_by_product_group_inq.srw
forward
global type w_sold_by_product_group_inq from w_netwise_response
end type
type dw_product_productgroup from u_product_productgroup within w_sold_by_product_group_inq
end type
type dw_loads_boxes from u_loads_boxes within w_sold_by_product_group_inq
end type
type dw_product_code_short from u_product_code_short within w_sold_by_product_group_inq
end type
type uo_groups from u_group_list within w_sold_by_product_group_inq
end type
end forward

global type w_sold_by_product_group_inq from w_netwise_response
integer x = 14
integer y = 336
integer width = 2487
integer height = 888
long backcolor = 67108864
dw_product_productgroup dw_product_productgroup
dw_loads_boxes dw_loads_boxes
dw_product_code_short dw_product_code_short
uo_groups uo_groups
end type
global w_sold_by_product_group_inq w_sold_by_product_group_inq

type variables
Boolean			ib_ok_to_close, &
			ib_inquire

w_sold_by_product_group	iw_parentwindow

string                                        is_group_owner
end variables

forward prototypes
public function boolean uf_set_product_code (string as_product_code)
end prototypes

public function boolean uf_set_product_code (string as_product_code);DataWindowChild	ldwc_code, &
						ldwc_descr 
String		ls_Description 


dw_product_code_short.uf_setproductcode(as_product_code )

// If this is disabled, then the drop down is empty
	// It might have been added, check for it
	dw_product_code_short.GetChild('sku_product_code', ldwc_code)
	If ldwc_code.Find("sku_product_code = '" + as_product_code + "'", 1, &
							ldwc_code.RowCount()) <= 0 Then
		Select 	short_description
		into 		:ls_Description 
		from sku_products
		where sku_product_code = :as_product_code
		using SQLCA;
		
		If SQLCA.SQLCode = 0 Then
			ldwc_code.ImportString(as_product_code + "~t" + ls_description)
			dw_product_code_short.GetChild('product_short_descr', ldwc_descr)
			ldwc_code.ShareData(ldwc_descr)
		End if
	End if

return True
end function

on w_sold_by_product_group_inq.create
int iCurrent
call super::create
this.dw_product_productgroup=create dw_product_productgroup
this.dw_loads_boxes=create dw_loads_boxes
this.dw_product_code_short=create dw_product_code_short
this.uo_groups=create uo_groups
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_productgroup
this.Control[iCurrent+2]=this.dw_loads_boxes
this.Control[iCurrent+3]=this.dw_product_code_short
this.Control[iCurrent+4]=this.uo_groups
end on

on w_sold_by_product_group_inq.destroy
call super::destroy
destroy(this.dw_product_productgroup)
destroy(this.dw_loads_boxes)
destroy(this.dw_product_code_short)
destroy(this.uo_groups)
end on

event close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF
end event

event open;call super::open;If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
		This.Title = iw_parentwindow.Title + " Inquire"
	End If
End If


end event

event ue_base_cancel;CloseWithReturn(This, "Abort")
end event

event ue_base_ok;String		ls_product, ls_new_string, &
				ls_string, ls_owner, &
				ls_product_productgroup, &
				ls_loads_boxes, ls_desc, &
				ls_prod_group_id, &
				ls_prod_group_desc, &
				ls_product_desc
				
long			ll_string_len
				
integer		li_rtn

u_string_functions		lu_strings


If dw_loads_boxes.AcceptText() = -1 & 
	or dw_product_productgroup.AcceptText() = -1 & 
    or (dw_product_code_short.AcceptText() = -1 &
   and lu_strings.nf_IsEmpty(uo_groups.uf_get_owner())) &
   Then return 
//	 ibdkdld and lu_strings.nf_IsEmpty(uo_selectprodgroupbyowner.uf_get_owner())) &
	
ls_loads_boxes = dw_loads_boxes.uf_get_loads_boxes()
If lu_strings.nf_IsEmpty(ls_loads_boxes) Then
	iw_frame.SetMicroHelp("Loads or Boxes is a required field!")
	dw_loads_boxes.SetFocus()
	Return
End if

ls_product_productgroup = dw_product_productgroup.uf_get_product_productgroup()
If lu_strings.nf_IsEmpty(ls_product_productgroup) Then
	iw_frame.SetMicroHelp("Product or Product Group is a required field!")
	dw_product_productgroup.SetFocus()
	Return
End if
	
If ls_product_productgroup = 'P' Then
	ls_product = dw_product_code_short.uf_getproductcode()
	dw_product_code_short.Visible = True
//ibdkdld	uo_selectprodgroupbyowner.Visible = False
	//ole_groups.Visible = False
	uo_groups.Visible = False
	If lu_strings.nf_IsEmpty(ls_product) Then
		iw_frame.SetMicroHelp("Product is a required field!")
		dw_product_code_short.SetFocus()
		return
	End If
	
	ls_product_desc = dw_product_code_short.uf_getproductdesc()
	
	iw_parentwindow.Event ue_set_data('Product',ls_product)
	iw_parentwindow.Event ue_set_data('Product_visible','true')
	iw_parentwindow.Event ue_set_data('prod_group_visible','false')
Else
	IF ls_product_productgroup = 'G' Then 
		// ibdkdld ls_owner = uo_selectprodgroupbyowner.uf_get_owner()
		//ls_owner = ole_groups.object.systemname()
		ls_owner = uo_groups.uf_get_owner()
		If lu_strings.nf_IsEmpty(ls_owner) Then
			iw_frame.SetMicroHelp("Product System is a required field")
// ibdkdld			uo_selectprodgroupbyowner.SetFocus()
			//ole_groups.SetFocus()
			uo_groups.SetFocus()
		return
		End If
// ibdkdld		li_rtn = uo_selectprodgroupbyowner.uf_get_sel_id(ls_prod_group_id)
// ibdkdld		If li_rtn < 1 Then
		//ls_prod_group_id = string(ole_groups.object.Groupid())
		ls_prod_group_id = string(uo_groups.uf_get_sel_id(ls_prod_group_id))
		//ls_prod_group_id = string(uo_groups.dw_group_detail.getitemnumber(1, "group_id"))
		If lu_strings.nf_isempty(ls_prod_group_id) Then
			iw_frame.SetMicroHelp("Product Group is a required field")
// ibdkdld			uo_selectprodgroupbyowner.SetFocus()
			//ole_groups.SetFocus()
			Return
		else
// ibdkdld			li_rtn = uo_selectprodgroupbyowner.uf_get_sel_desc(ls_prod_group_desc)
			//ls_prod_group_desc = ole_groups.object.Groupdescription()
			uo_groups.uf_get_sel_desc(ls_prod_group_desc)
		End if
		ls_new_string = ''
		Do 
			ls_desc = lu_strings.nf_gettoken(ls_prod_group_desc,'~r~n')
			ls_new_string += Trim(ls_desc) + ', '
		Loop While Len(ls_prod_group_desc) > 0 
		ll_string_len = len(ls_new_string) - 2		
		ls_new_string = Left ( ls_new_string, ll_string_len)
		iw_parentwindow.Event ue_set_data('Product_visible','false')
		iw_parentwindow.Event ue_set_data('product_owner',ls_owner)
		iw_parentwindow.Event ue_set_data('product_group_desc',ls_new_string)
		iw_parentwindow.Event ue_set_data('product_group_id',ls_prod_group_id)
		iw_parentwindow.Event ue_set_data('prod_group_visible','true')
	End If
End If

iw_parentwindow.Event ue_set_data('loads_boxes',ls_loads_boxes)
iw_parentwindow.Event ue_set_data('product_productgroup',ls_product_productgroup)


ib_ok_to_close = True

Close(This)
end event

event ue_postopen;String	ls_inquire_type, &
			ls_owner, &
			ls_Description
			
DataWindowChild	ldwc_code, &
						ldwc_descr 
			
u_string_functions 	lu_strings


// ibdkdld uo_selectprodgroupbyowner.uf_initialize(1,sqlca.userid,sqlca.dbpass)
//ole_groups.object.GroupType(3)
//ole_Groups.object.LoadObject()

uo_groups.uf_load_groups('P')
iw_parentwindow.Event ue_get_data('loads_boxes')
dw_loads_boxes.uf_set_loads_boxes(Message.StringParm)

iw_parentwindow.Event ue_get_data('product_productgroup')
dw_product_productgroup.uf_set_product_productgroup(Message.StringParm)

ls_inquire_type = dw_product_productgroup.uf_get_product_productgroup()

If ls_inquire_type = 'P' then
	iw_parentwindow.Event ue_get_data('Product')
	uf_set_product_code(Message.StringParm)
	dw_product_code_short.visible = True
//ibdkdld	uo_selectprodgroupbyowner.visible = False
	//ole_groups.visible = False
	uo_groups.visible = False
Else
	If ls_inquire_type = 'G' then
//		iw_parentwindow.Event ue_get_data('product_owner')
//		uo_selectprodgroupbyowner.uf_set_owner(Message.StringParm)
//		is_group_owner = Message.StringParm
		//is_group_owner = ole_groups.object.systemname()
		iw_parentwindow.Event ue_get_data('product_group_id')
	//	uo_selectprodgroupbyowner.uf_set_default_by_id(is_group_owner, Message.StringParm)
		dw_product_code_short.visible = False
//ibdkdld		uo_selectprodgroupbyowner.visible = True
		//ole_groups.visible = True
		uo_groups.visible = True
	End If
End If

end event

event activate;// ibdkdld uo_selectprodgroupbyowner.uf_initialize(1,sqlca.userid,sqlca.dbpass)
end event

type cb_base_help from w_netwise_response`cb_base_help within w_sold_by_product_group_inq
integer x = 2089
integer y = 380
integer taborder = 60
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_sold_by_product_group_inq
integer x = 2089
integer y = 220
integer taborder = 50
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_sold_by_product_group_inq
integer x = 2089
integer y = 60
integer taborder = 45
end type

type dw_product_productgroup from u_product_productgroup within w_sold_by_product_group_inq
integer x = 27
integer y = 32
integer height = 284
boolean bringtotop = true
boolean livescroll = false
end type

event itemchanged;CHOOSE CASE data
	CASE 'P'
		dw_product_code_short.Visible = True
//ibdkdld		uo_selectprodgroupbyowner.Visible = False
		//ole_groups.Visible = False
		uo_groups.Visible = False
	CASE 'G'
		dw_product_code_short.Visible = False
// ibdkdld      uo_selectprodgroupbyowner.Visible = True
      //ole_groups.Visible = True
		uo_groups.visible = True
		
END CHOOSE
end event

type dw_loads_boxes from u_loads_boxes within w_sold_by_product_group_inq
integer x = 23
integer y = 324
integer width = 398
integer height = 276
integer taborder = 20
boolean bringtotop = true
end type

type dw_product_code_short from u_product_code_short within w_sold_by_product_group_inq
integer x = 585
integer y = 232
integer taborder = 40
boolean bringtotop = true
end type

event constructor;call super::constructor;this.insertrow(0)
this.uf_enable()
this.Modify("sku_product_code.Background.Color='16777215'")
end event

type uo_groups from u_group_list within w_sold_by_product_group_inq
boolean visible = false
integer x = 613
integer y = 64
integer width = 1394
integer taborder = 70
boolean bringtotop = true
end type

on uo_groups.destroy
call u_group_list::destroy
end on


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Cw_sold_by_product_group_inq.bin 
2000000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff000000030000000000000000000000000000000000000000000000000000000072e827b001c2b04e00000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f30000000072e827b001c2b04e72e827b001c2b04e000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
2Effffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d700073006e006c0020006e006f002000670070005b006d00620064005f00650064007800650063006500740075005d006500720000006d00650074006f006800650074006f0069006c006b006e00740073007200610020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f006500640064006100690076006500730000005d00650072006f006d00650074006f0068006c0074006e00690073006b006f00740020007000200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006e00750064006100690076006500730000005d00650072006f006d006500740065007200750071007300650020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006500720075007100730065005d00745409000069aafcf8a79c9b043d390077c59f719a4e35f4aba96c0c16cf45a8a0c100d55b8f4ff509ecf7e3fef5090000fcfe8f4ff509ecb75ba5964f00b5df7a8f4ff509d86fb7fe719a3d39237ec59f00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Cw_sold_by_product_group_inq.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
