﻿$PBExportHeader$w_box_inventory_new.srw
$PBExportComments$Boxed inventory Report
forward
global type w_box_inventory_new from w_netwise_sheet
end type
type st_8 from statictext within w_box_inventory_new
end type
type st_7 from statictext within w_box_inventory_new
end type
type st_6 from statictext within w_box_inventory_new
end type
type st_5 from statictext within w_box_inventory_new
end type
type st_4 from statictext within w_box_inventory_new
end type
type st_3 from statictext within w_box_inventory_new
end type
type rb_eight_days from radiobutton within w_box_inventory_new
end type
type rb_seven_days from radiobutton within w_box_inventory_new
end type
type rb_ready_to_ship_date from radiobutton within w_box_inventory_new
end type
type rb_total_on_hand from radiobutton within w_box_inventory_new
end type
type rb_weight from radiobutton within w_box_inventory_new
end type
type rb_quantity from radiobutton within w_box_inventory_new
end type
type st_start_date from statictext within w_box_inventory_new
end type
type st_1 from statictext within w_box_inventory_new
end type
type st_divisions from statictext within w_box_inventory_new
end type
type sle_division_list from singlelineedit within w_box_inventory_new
end type
type st_groups from statictext within w_box_inventory_new
end type
type dw_uom_code from u_uom_code within w_box_inventory_new
end type
type cbx_combined from checkbox within w_box_inventory_new
end type
type cbx_uncombined from checkbox within w_box_inventory_new
end type
type cbx_gpo from checkbox within w_box_inventory_new
end type
type cbx_reservation from checkbox within w_box_inventory_new
end type
type dw_weekly_summary from u_netwise_dw within w_box_inventory_new
end type
type st_boxessold from statictext within w_box_inventory_new
end type
type st_2 from statictext within w_box_inventory_new
end type
type sle_last_update from singlelineedit within w_box_inventory_new
end type
type ole_1 from olecustomcontrol within w_box_inventory_new
end type
type gb_display_uom from groupbox within w_box_inventory_new
end type
type gb_inv_position from groupbox within w_box_inventory_new
end type
type gb_number_of_days from groupbox within w_box_inventory_new
end type
end forward

global type w_box_inventory_new from w_netwise_sheet
integer width = 2935
integer height = 1748
string title = "Boxed Inventory Report "
long backcolor = 67108864
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
rb_eight_days rb_eight_days
rb_seven_days rb_seven_days
rb_ready_to_ship_date rb_ready_to_ship_date
rb_total_on_hand rb_total_on_hand
rb_weight rb_weight
rb_quantity rb_quantity
st_start_date st_start_date
st_1 st_1
st_divisions st_divisions
sle_division_list sle_division_list
st_groups st_groups
dw_uom_code dw_uom_code
cbx_combined cbx_combined
cbx_uncombined cbx_uncombined
cbx_gpo cbx_gpo
cbx_reservation cbx_reservation
dw_weekly_summary dw_weekly_summary
st_boxessold st_boxessold
st_2 st_2
sle_last_update sle_last_update
ole_1 ole_1
gb_display_uom gb_display_uom
gb_inv_position gb_inv_position
gb_number_of_days gb_number_of_days
end type
global w_box_inventory_new w_box_inventory_new

type variables
Boolean		ib_m_Print_Status

u_pas203		iu_pas203

u_ws_pas_share   iu_ws_pas_share

s_error		istr_error_info

Int		ii_column_width

String 		is_groupid




end variables

forward prototypes
public function string wf_transpose (ref datastore adw_source)
public function integer wf_change_loads_required_calc ()
public function boolean wf_retrieve ()
public function string wf_convert_date (date adt_date)
public subroutine wf_calc_production_weight ()
public subroutine wf_calc_inventory_weight ()
public subroutine wf_calc_loads_reqd_weight ()
public subroutine wf_calc_invent_qty ()
public subroutine wf_calc_invent_qty_wt ()
public subroutine wf_reset_invent_qty ()
public subroutine wf_reset_sales_weight1 ()
public subroutine wf_reset_production_qty ()
public subroutine wf_calc_sales_weight ()
public subroutine wf_reset_sales_qty ()
public subroutine wf_calc_reserv_weight ()
public subroutine wf_reset_reserv_qty ()
end prototypes

public function string wf_transpose (ref datastore adw_source);DataStore	lds_2, &
				lds_3, &
				lds_4

String		ls_temp = "", &
				ls_syntax_begin = 'release 5; datawindow(units=0 timer_interval=0 processing=0 ) table(', &
				ls_syntax4, &
				ls_syntax3, &
				ls_syntax2

Integer		li_counter, &
				li_row_count, &
				li_col_count

// Here we create three temporary dynamic DataStores to hold the data after transposition
lds_2 = Create DataStore
lds_3 = Create DataStore
lds_4 = Create DataStore
ls_syntax4 = ls_syntax_begin
ls_syntax3 = ls_syntax_begin
ls_syntax2 = ls_syntax_begin
For li_counter = 1 to 15
	ls_syntax4 += 'column=(type=char(1) name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
	ls_syntax3 += 'column=(type=char(3) name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
	ls_syntax2 += 'column=(type=number name=col'+ String(li_counter) + ' dbname="col' + &
			String(li_counter) + '" ) '
Next
ls_syntax4 += ')'
ls_syntax3 += ')'
ls_syntax2 += ')'
lds_4.Create(ls_syntax4)
lds_3.Create(ls_syntax3)
lds_2.Create(ls_syntax2)

lds_3.InsertRow(0)
lds_3.InsertRow(0)
li_row_count = adw_source.RowCount()
For li_counter = 1 to li_row_count
	lds_3.SetItem(1,li_counter,adw_source.GetItemString(li_counter,1))
	lds_3.SetItem(2,li_counter,adw_source.GetItemString(li_counter,2))
Next

For li_col_count = 1 to 14*8 + 3
	lds_2.InsertRow(0)
	For li_counter = 1 to li_row_count
		lds_2.SetItem(li_col_count,li_counter,adw_source.GetItemNumber(li_counter,li_col_count + 2))
	Next
Next

lds_4.InsertRow(0)
For li_counter = 1 to li_row_count
	lds_4.SetItem(1,li_counter,adw_source.GetItemString(li_counter,14*8+6))
Next

ls_temp = lds_3.Object.DataWindow.Data + "~r~n"
ls_temp += lds_2.Object.DataWindow.Data + "~r~n"
ls_temp += lds_4.Object.DataWindow.Data + "~r~n"
  
Destroy lds_2
Destroy lds_3
Destroy lds_4

Return ls_temp
end function

public function integer wf_change_loads_required_calc ();String			ls_average_box, &
				ls_uom


ls_uom = dw_uom_code.uf_get_uom_code()

SELECT tutltypes.TYPE_SHORT_DESC  
    INTO :ls_average_box  
    FROM tutltypes  
   WHERE ( tutltypes.RECORD_TYPE = "AVAILLDS" ) AND  
         ( tutltypes.TYPE_CODE = :ls_uom );  

ole_1.object.EntryRC(7,3,"=IF(C11 > 0,IF(C11 > C164, (C11 - C164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,4,"=IF(D11 > 0,IF(D11 > D164, (D11 - D164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,5,"=IF(E11 > 0,IF(E11 > E164, (E11 - E164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,6,"=IF(F11 > 0,IF(F11 > F164, (F11 - F164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,7,"=IF(G11 > 0,IF(G11 > G164, (G11 - G164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,8,"=IF(H11 > 0,IF(H11 > H164, (H11 - H164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,9,"=IF(I11 > 0,IF(I11 > I164, (I11 - I164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,10,"=IF(J11 > 0,IF(J11 > J164, (J11 - J164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,11,"=IF(K11 > 0,IF(K11 > K164, (K11 - K164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,12,"=IF(L11 > 0,IF(L11 > L164, (L11 - L164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,13,"=IF(M11 > 0,IF(M11 > M164, (M11 - M164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,14,"=IF(N11 > 0,IF(N11 > N164, (N11 - N164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,15,"=IF(O11 > 0,IF(O11 > O164, (O11 - O164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,16,"=IF(P11 > 0,IF(P11 > P164, (P11 - P164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,17,"=IF(Q11 > 0,IF(Q11 > Q164, (Q11 - Q164) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(7,19,"=IF(S11 > 0,IF(S11 > S164, (S11 - S164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,20,"=IF(T11 > 0,IF(T11 > T164, (T11 - T164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,21,"=IF(U11 > 0,IF(U11 > U164, (U11 - U164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,22,"=IF(V11 > 0,IF(V11 > V164, (V11 - V164) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(7,23,"=IF(W11 > 0,IF(W11 > W164, (W11 - W164) /" +  &
		ls_average_box + ", 0),0)")
		
ole_1.object.EntryRC(15,3,"=IF(C19 > 0,IF(C19 > C178, (C19 - C178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,4,"=IF(D19 > 0,IF(D19 > D178, (D19 - D178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,5,"=IF(E19 > 0,IF(E19 > E178, (E19 - E178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,6,"=IF(F19 > 0,IF(F19 > F178, (F19 - F178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,7,"=IF(G19 > 0,IF(G19 > G178, (G19 - G178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,8,"=IF(H19 > 0,IF(H19 > H178, (H19 - H178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,9,"=IF(I19 > 0,IF(I19 > I178, (I19 - I178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,10,"=IF(J19 > 0,IF(J19 > J178, (J19 - J178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,11,"=IF(K19 > 0,IF(K19 > K178, (K19 - K178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,12,"=IF(L19 > 0,IF(L19 > L178, (L19 - L178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,13,"=IF(M19 > 0,IF(M19 > M178, (M19 - M178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,14,"=IF(N19 > 0,IF(N19 > N178, (N19 - N178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,15,"=IF(O19 > 0,IF(O19 > O178, (O19 - O178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,16,"=IF(P19 > 0,IF(P19 > P178, (P19 - P178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,17,"=IF(Q19 > 0,IF(Q19 > Q178, (Q19 - Q178) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(15,19,"=IF(S19 > 0,IF(S19 > S178, (S19 - S178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,20,"=IF(T19 > 0,IF(T19 > T178, (T19 - T178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,21,"=IF(U19 > 0,IF(U19 > U178, (U19 - U178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,22,"=IF(V19 > 0,IF(V19 > V178, (V19 - V178) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(15,23,"=IF(W19 > 0,IF(W19 > W178, (W19 - W178) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(23,3,"=IF(C27 > 0,IF(C27 > C192, (C27 - C192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,4,"=IF(D27 > 0,IF(D27 > D192, (D27 - D192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,5,"=IF(E27 > 0,IF(E27 > E192, (E27 - E192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,6,"=IF(F27 > 0,IF(F27 > F192, (F27 - F192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,7,"=IF(G27 > 0,IF(G27 > G192, (G27 - G192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,8,"=IF(H27 > 0,IF(H27 > H192, (H27 - H192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,9,"=IF(I27 > 0,IF(I27 > I192, (I27 - I192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,10,"=IF(J27 > 0,IF(J27 > J192, (J27 - J192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,11,"=IF(K27 > 0,IF(K27 > K192, (K27 - K192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,12,"=IF(L27 > 0,IF(L27 > L192, (L27 - L192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,13,"=IF(M27 > 0,IF(M27 > M192, (M27 - M192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,14,"=IF(N27 > 0,IF(N27 > N192, (N27 - N192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,15,"=IF(O27 > 0,IF(O27 > O192, (O27 - O192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,16,"=IF(P27 > 0,IF(P27 > P192, (P27 - P192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,17,"=IF(Q27 > 0,IF(Q27 > Q192, (Q27 - Q192) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(23,19,"=IF(S27 > 0,IF(S27 > S192, (S27 - S192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,20,"=IF(T27 > 0,IF(T27 > T192, (T27 - T192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,21,"=IF(U27 > 0,IF(U27 > U192, (U27 - U192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,22,"=IF(V27 > 0,IF(V27 > V192, (V27 - V192) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(23,23,"=IF(W27 > 0,IF(W27 > W192, (W27 - W192) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(31,3,"=IF(C32 > 0,IF(C32 > C206, (C32 - C206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,4,"=IF(D32 > 0,IF(D32 > D206, (D32 - D206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,5,"=IF(E32 > 0,IF(E32 > E206, (E32 - E206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,6,"=IF(F32 > 0,IF(F32 > F206, (F32 - F206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,7,"=IF(G32 > 0,IF(G32 > G206, (G32 - G206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,8,"=IF(H32 > 0,IF(H32 > H206, (H32 - H206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,9,"=IF(I32 > 0,IF(I32 > I206, (I32 - I206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,10,"=IF(J32 > 0,IF(J32 > J206, (J32 - J206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,11,"=IF(K32 > 0,IF(K32 > K206, (K32 - K206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,12,"=IF(L32 > 0,IF(L32 > L206, (L32 - L206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,13,"=IF(M32 > 0,IF(M32 > M206, (M32 - M206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,14,"=IF(N32 > 0,IF(N32 > N206, (N32 - N206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,15,"=IF(O32 > 0,IF(O32 > O206, (O32 - O206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,16,"=IF(P32 > 0,IF(P32 > P206, (P32 - P206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,17,"=IF(Q32 > 0,IF(Q32 > Q206, (Q32 - Q206) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(31,19,"=IF(S32 > 0,IF(S32 > S206, (S32 - S206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,20,"=IF(T32 > 0,IF(T32 > T206, (T32 - T206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,21,"=IF(U32 > 0,IF(U32 > U206, (U32 - U206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,22,"=IF(V32 > 0,IF(V32 > V206, (V32 - V206) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(31,23,"=IF(W32 > 0,IF(W32 > W206, (W32 - W206) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(36,3,"=IF(C37 > 0,IF(C37 > C220, (C37 - C220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,4,"=IF(D37 > 0,IF(D37 > D220, (D37 - D220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,5,"=IF(E37 > 0,IF(E37 > E220, (E37 - E220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,6,"=IF(F37 > 0,IF(F37 > F220, (F37 - F220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,7,"=IF(G37 > 0,IF(G37 > G220, (G37 - G220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,8,"=IF(H37 > 0,IF(H37 > H220, (H37 - H220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,9,"=IF(I37 > 0,IF(I37 > I220, (I37 - I220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,10,"=IF(J37 > 0,IF(J37 > J220, (J37 - J220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,11,"=IF(K37 > 0,IF(K37 > K220, (K37 - K220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,12,"=IF(L37 > 0,IF(L37 > L220, (L37 - L220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,13,"=IF(M37 > 0,IF(M37 > M220, (M37 - M220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,14,"=IF(N37 > 0,IF(N37 > N220, (N37 - N220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,15,"=IF(O37 > 0,IF(O37 > O220, (O37 - O220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,16,"=IF(P37 > 0,IF(P37 > P220, (P37 - P220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,17,"=IF(Q37 > 0,IF(Q37 > Q220, (Q37 - Q220) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(36,19,"=IF(S37 > 0,IF(S37 > S220, (S37 - S220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,20,"=IF(T37 > 0,IF(T37 > T220, (T37 - T220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,21,"=IF(U37 > 0,IF(U37 > U220, (U37 - U220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,22,"=IF(V37 > 0,IF(V37 > V220, (V37 - V220) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(36,23,"=IF(W37 > 0,IF(W37 > W220, (W37 - W220) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(41,3,"=IF(C42 > 0,IF(C42 > C234, (C42 - C234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,4,"=IF(D42 > 0,IF(D42 > D234, (D42 - D234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,5,"=IF(E42 > 0,IF(E42 > E234, (E42 - E234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,6,"=IF(F42 > 0,IF(F42 > F234, (F42 - F234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,7,"=IF(G42 > 0,IF(G42 > G234, (G42 - G234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,8,"=IF(H42 > 0,IF(H42 > H234, (H42 - H234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,9,"=IF(I42 > 0,IF(I42 > I234, (I42 - I234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,10,"=IF(J42 > 0,IF(J42 > J234, (J42 - J234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,11,"=IF(K42 > 0,IF(K42 > K234, (K42 - K234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,12,"=IF(L42 > 0,IF(L42 > L234, (L42 - L234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,13,"=IF(M42 > 0,IF(M42 > M234, (M42 - M234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,14,"=IF(N42 > 0,IF(N42 > N234, (N42 - N234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,15,"=IF(O42 > 0,IF(O42 > O234, (O42 - O234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,16,"=IF(P42 > 0,IF(P42 > P234, (P42 - P234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,17,"=IF(Q42 > 0,IF(Q42 > Q234, (Q42 - Q234) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(41,19,"=IF(S42 > 0,IF(S42 > S234, (S42 - S234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,20,"=IF(T42 > 0,IF(T42 > T234, (T42 - T234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,21,"=IF(U42 > 0,IF(U42 > U234, (U42 - U234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,22,"=IF(V42 > 0,IF(V42 > V234, (V42 - V234) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(41,23,"=IF(W42 > 0,IF(W42 > W234, (W42 - W234) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(46,3,"=IF(C47 > 0,IF(C47 > C248, (C47 - C248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,4,"=IF(D47 > 0,IF(D47 > D248, (D47 - D248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,5,"=IF(E47 > 0,IF(E47 > E248, (E47 - E248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,6,"=IF(F47 > 0,IF(F47 > F248, (F47 - F248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,7,"=IF(G47 > 0,IF(G47 > G248, (G47 - G248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,8,"=IF(H47 > 0,IF(H47 > H248, (H47 - H248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,9,"=IF(I47 > 0,IF(I47 > I248, (I47 - I248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,10,"=IF(J47 > 0,IF(J47 > J248, (J47 - J248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,11,"=IF(K47 > 0,IF(K47 > K248, (K47 - K248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,12,"=IF(L47 > 0,IF(L47 > L248, (L47 - L248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,13,"=IF(M47 > 0,IF(M47 > M248, (M47 - M248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,14,"=IF(N47 > 0,IF(N47 > N248, (N47 - N248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,15,"=IF(O47 > 0,IF(O47 > O248, (O47 - O248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,16,"=IF(P47 > 0,IF(P47 > P248, (P47 - P248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,17,"=IF(Q47 > 0,IF(Q47 > Q248, (Q47 - Q248) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(46,19,"=IF(S47 > 0,IF(S47 > S248, (S47 - S248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,20,"=IF(T47 > 0,IF(T47 > T248, (T47 - T248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,21,"=IF(U47 > 0,IF(U47 > U248, (U47 - U248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,22,"=IF(V47 > 0,IF(V47 > V248, (V47 - V248) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(46,23,"=IF(W47 > 0,IF(W47 > W248, (W47 - W248) /" +  &
		ls_average_box + ", 0),0)")


//Need to calculate the projected beginning inventory to use in the last
//days calculation.
ole_1.object.EntryRC(52,3,"=C47+C48-C49-C50")
ole_1.object.EntryRC(52,4,"=D47+D48-D49-D50")
ole_1.object.EntryRC(52,5,"=E47+E48-E49-E50")
ole_1.object.EntryRC(52,6,"=F47+F48-F49-F50")
ole_1.object.EntryRC(52,7,"=G47+G48-G49-G50")
ole_1.object.EntryRC(52,8,"=H47+H48-H49-H50")
ole_1.object.EntryRC(52,9,"=I47+I48-I49-I50")
ole_1.object.EntryRC(52,10,"=J47+J48-J49-J50")
ole_1.object.EntryRC(52,11,"=K47+K48-K49-K50")
ole_1.object.EntryRC(52,12,"=L47+L48-L49-L50")
ole_1.object.EntryRC(52,13,"=M47+M48-M49-M50")
ole_1.object.EntryRC(52,14,"=N47+N48-N49-N50")
ole_1.object.EntryRC(52,15,"=O47+O48-O49-O50")
ole_1.object.EntryRC(52,16,"=P47+P48-P49-P50")
ole_1.object.EntryRC(52,17,"=Q47+Q48-Q49-Q50")

ole_1.object.EntryRC(52,19,"=S47+S48-S49-S50")
ole_1.object.EntryRC(52,20,"=T47+T48-T49-T50")
ole_1.object.EntryRC(52,21,"=U47+U48-U49-U50")
ole_1.object.EntryRC(52,22,"=V47+V48-V49-V50")
ole_1.object.EntryRC(52,23,"=W47+W48-W49-W50")


ole_1.object.EntryRC(51,3,"=IF(C52 > 0,IF(C52 > C262, (C52 - C262) /" + &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,4,"=IF(D52 > 0,IF(D52 > D262, (D52 - D262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,5,"=IF(E52 > 0,IF(E52 > E262, (E52 - E262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,6,"=IF(F52 > 0,IF(F52 > F262, (F52 - F262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,7,"=IF(G52 > 0,IF(G52 > G262, (G52 - G262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,8,"=IF(H52 > 0,IF(H52 > H262, (H52 - H262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,9,"=IF(I52 > 0,IF(I52 > I262, (I52 - I262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,10,"=IF(J52 > 0,IF(J52 > J262, (J52 - J262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,11,"=IF(K52 > 0,IF(K52 > K262, (K52 - K262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,12,"=IF(L52 > 0,IF(L52 > L262, (L52 - L262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,13,"=IF(M52 > 0,IF(M52 > M262, (M52 - M262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,14,"=IF(N52 > 0,IF(N52 > N262, (N52 - N262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,15,"=IF(O52 > 0,IF(O52 > O262, (O52 - O262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,16,"=IF(P52 > 0,IF(P52 > P262, (P52 - P262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,17,"=IF(Q52 > 0,IF(Q52 > Q262, (Q52 - Q262) /" +  &
		ls_average_box + ", 0),0)")

ole_1.object.EntryRC(51,19,"=IF(S52 > 0,IF(S52 > S262, (S52 - S262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,20,"=IF(T52 > 0,IF(T52 > T262, (T52 - T262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,21,"=IF(U52 > 0,IF(U52 > U262, (U52 - U262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,22,"=IF(V52 > 0,IF(V52 > V262, (V52 - V262) /" +  &
		ls_average_box + ", 0),0)")
ole_1.object.EntryRC(51,23,"=IF(W52 > 0,IF(W52 > W262, (W52 - W262) /" +  &
		ls_average_box + ", 0),0)")


Return 0
end function

public function boolean wf_retrieve ();Int		 	li_ret, &
			li_counter, &
			li_date_offset

Long		ll_row, &
			ll_col, &
			ll_plant_count, &
			ll_temp

String		ls_input_string, &
				ls_Plant_Data, &
				ls_weekly_data, &
				ls_last_update, &
				ls_date, &
				ls_time, &
				ls_plant, &
				ls_complex, &
				ls_inquire, ls_string
DataStore	lds_plant

Date			ldt_start_date

//If Not IsValid(iu_pas203) Then
//	iu_pas203 = Create u_pas203
//End if

If Not IsValid(iu_ws_pas_share) Then
	iu_ws_pas_share = Create u_ws_pas_share
End if

OpenWithParm(w_box_inventory_inq_new, This)
ls_inquire = message.StringParm

IF not ls_inquire = "True" Then
	Return False
End If

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")
// change inquire parms ibdkdld 12/15/02
//ls_input_string = dw_division.uf_get_division()
ls_input_string = sle_division_list.Text 
ls_input_string += "~t" + dw_uom_code.uf_get_uom_code()
ls_input_string += "~t" + is_groupid + "~t"
ls_input_string += String(Date(st_start_date.Text),"yyyy-mm-dd")  + "~t"

If rb_quantity.Checked = True Then
	ls_input_string += "Q"  + "~t"
Else
	ls_input_string += "W" + "~t"
End If
	
If rb_total_on_hand.Checked = True Then
	ls_input_string += "T"  + "~t"
Else
	ls_input_string += "S" + "~t"
End If	

If rb_seven_days.Checked = True Then
	ls_input_string += "7"  + "~t"
Else
	ls_input_string += "8" + "~t"
End If


ole_1.Object.ClearRange(150, 1, 555, 35, 1)

dw_weekly_summary.Reset()
dw_weekly_summary.InsertRow(0)


//If Not iu_pas203.nf_pasp00dr_inq_box_inventory(istr_error_info, &
//									ls_input_string, &
//									ls_last_update, &
//									ls_Plant_Data, &
//									ls_Weekly_data) Then return False

If Not iu_ws_pas_share.nf_pasp00hr(istr_error_info, &
									ls_input_string, &
									ls_last_update, &
									ls_Plant_Data, &
									ls_Weekly_data) Then return False

//This.SetRedraw(False)

//This is to set the formulas for loads required.  We are unable to change
// the actual spreadsheet.

wf_change_loads_required_calc()



iw_frame.iu_string.nf_parseLeftRight(ls_last_update, ' ', ls_date, ls_time)
sle_last_update.Text = String(Date(ls_date), 'mm/dd/yyyy') + ' ' + &
								String(Time(ls_time), 'hh:mm am/pm')

// make sure all columns are visible
ole_1.Object.SetColWidth(3, 17, ii_column_width, False)
ole_1.Object.SetColWidth(19,23, ii_column_width, False)

// Put the plant data into a local DataStore to separate foreign from domestic
lds_plant = Create DataStore
lds_plant.DataObject = 'd_invt_vs_sales'

lds_plant.reset()

lds_plant.ImportString(ls_Plant_Data)
li_ret = lds_plant.SetFilter("domestic_or_foreign = 'D'") 
li_ret = lds_plant.Filter()

//ls_Plant_Data = lds_plant.Describe("DataWindow.Data")

ls_Plant_data = wf_transpose(lds_plant)

ole_1.Object.SetTabbedText(149,3, ref ll_row, ref ll_col, False, ls_Plant_Data)
ll_plant_count = lds_plant.RowCount()
// Set the plants in the column headings
For li_counter = 1 to ll_plant_count
	ls_plant = Space(32)
	ls_plant = ole_1.Object.EntryRC(149, 2 + li_counter)
	ls_complex = ole_1.Object.EntryRC(150, 2 + li_counter)
	ls_plant = String(Integer(ls_plant), "000") + " " + ls_complex
	ole_1.Object.ColText(li_counter + 2, ls_plant)
Next

If ll_plant_count < 15 Then
	//the first two columns are headings
	// Also, don't hide the last plant
	ole_1.Object.SetColWidth(ll_plant_count + 3, 17, 0, False)
End if

li_ret = lds_plant.SetFilter("domestic_or_foreign = 'F'")
li_ret = lds_plant.Filter()
//ls_Plant_Data = lds_plant.Describe("DataWindow.Data")
ls_Plant_data = wf_transpose(lds_plant)

ole_1.Object.SetTabbedText(149,19, ref ll_row, ref ll_col, False, ls_Plant_Data)
ll_plant_count = lds_plant.RowCount()
// Set the plants in the column headings
For li_counter = 1 to ll_plant_count
	ls_plant = Space(32)
	ls_plant = ole_1.Object.EntryRC(149,li_counter + 18)
	ls_complex = ole_1.Object.EntryRC(150, li_counter + 18)
	ls_plant = String(Integer(ls_plant), "000") + " " + ls_complex
	ole_1.Object.ColText(li_counter + 18, ls_plant)
Next
If ll_plant_count < 5 Then
	//the first two columns are headings
	// Also, don't hide the last plant
	ole_1.Object.SetColWidth(ll_plant_count + 19,23,0,False)
End if

ole_1.Object.MaxCol = 18 + ll_plant_count

If rb_eight_days.Checked = True Then
	ole_1.Object.MaxRow = 51
Else
	ole_1.Object.MaxRow = 46
End If

ldt_start_date = Date(st_start_date.Text)
li_date_offset = 0
ole_1.Object.EntryRC(3, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
li_date_offset ++
ole_1.Object.EntryRC(11, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
li_date_offset ++
ole_1.Object.EntryRC(19, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
li_date_offset ++	
ole_1.Object.EntryRC(27, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
li_date_offset ++	
ole_1.Object.EntryRC(32, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
li_date_offset ++
ole_1.Object.EntryRC(37, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
li_date_offset ++	
ole_1.Object.EntryRC(42, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
If rb_eight_days.Checked = True Then
	li_date_offset ++	
	ole_1.Object.EntryRC(47, 1, wf_convert_date(RelativeDate(ldt_start_date, li_date_offset)))
End if

If rb_quantity.Checked = True Then
	st_boxessold.Text = "Boxes Sold"
Else
	st_boxessold.Text = "Weight Sold"
End If

dw_weekly_summary.Reset()
dw_weekly_summary.ImportString(ls_Weekly_Data)

SetPointer(Arrow!)
iw_frame.SetMicroHelp('Ready')

If rb_weight.Checked Then
	wf_calc_inventory_weight()	
	wf_calc_production_weight()
	wf_calc_sales_weight()
	wf_calc_reserv_weight()
	wf_calc_invent_qty_wt()
	wf_calc_loads_reqd_weight()	
Else
	wf_reset_invent_qty()
	wf_reset_production_qty()
	wf_reset_sales_qty()
	wf_reset_reserv_qty()
End If

lds_plant.setfilter("")
lds_plant.filter()

This.SetRedraw(True)
return true





end function

public function string wf_convert_date (date adt_date);Integer	li_month
String	ls_month, &
			ls_day

li_month = Month(adt_date)
ls_day	= String(Day(adt_date))

Choose Case li_month
	Case 1
		ls_month = 'Jan'
	Case 2
		ls_month = 'Feb'
	Case 3
		ls_month = 'Mar'
	Case 4
		ls_month = 'Apr'
	Case 5
		ls_month = 'May'
	Case 6
		ls_month = 'Jun'
	Case 7
		ls_month = 'Jul'
	Case 8
		ls_month = 'Aug'
	Case 9
		ls_month = 'Sep'
	Case 10
		ls_month = 'Oct'
	Case 11
		ls_month = 'Nov'
	Case 12
		ls_month = 'Dec'
End Choose

Return ls_month + ' ' + ls_day
                                              
end function

public subroutine wf_calc_production_weight ();String		ls_string

ls_string = '=C155'
ole_1.object.EntryRC(4,3,ls_string)
ls_string = '=C169'
ole_1.object.EntryRC(12,3,ls_string)
ls_string = '=C183'
ole_1.object.EntryRC(20,3,ls_string)
ls_string = '=C197'
ole_1.object.EntryRC(28,3,ls_string)
ls_string = '=C211'
ole_1.object.EntryRC(33,3,ls_string)
ls_string = '=C225'
ole_1.object.EntryRC(38,3,ls_string)
ls_string = '=C239'
ole_1.object.EntryRC(43,3,ls_string)
ls_string = '=C253'
ole_1.object.EntryRC(48,3,ls_string)

ls_string = '=D155'
ole_1.object.EntryRC(4,4,ls_string)
ls_string = '=D169'
ole_1.object.EntryRC(12,4,ls_string)
ls_string = '=D183'
ole_1.object.EntryRC(20,4,ls_string)
ls_string = '=D197'
ole_1.object.EntryRC(28,4,ls_string)
ls_string = '=D211'
ole_1.object.EntryRC(33,4,ls_string)
ls_string = '=D225'
ole_1.object.EntryRC(38,4,ls_string)
ls_string = '=D239'
ole_1.object.EntryRC(43,4,ls_string)
ls_string = '=D253'
ole_1.object.EntryRC(48,4,ls_string)

ls_string = '=E155'
ole_1.object.EntryRC(4,5,ls_string)
ls_string = '=E169'
ole_1.object.EntryRC(12,5,ls_string)
ls_string = '=E183'
ole_1.object.EntryRC(20,5,ls_string)
ls_string = '=E197'
ole_1.object.EntryRC(28,5,ls_string)
ls_string = '=E211'
ole_1.object.EntryRC(33,5,ls_string)
ls_string = '=E225'
ole_1.object.EntryRC(38,5,ls_string)
ls_string = '=E239'
ole_1.object.EntryRC(43,5,ls_string)
ls_string = '=E253'
ole_1.object.EntryRC(48,5,ls_string)

ls_string = '=F155'
ole_1.object.EntryRC(4,6,ls_string)
ls_string = '=F169'
ole_1.object.EntryRC(12,6,ls_string)
ls_string = '=F183'
ole_1.object.EntryRC(20,6,ls_string)
ls_string = '=F197'
ole_1.object.EntryRC(28,6,ls_string)
ls_string = '=F211'
ole_1.object.EntryRC(33,6,ls_string)
ls_string = '=F225'
ole_1.object.EntryRC(38,6,ls_string)
ls_string = '=F239'
ole_1.object.EntryRC(43,6,ls_string)
ls_string = '=F253'
ole_1.object.EntryRC(48,6,ls_string)

ls_string = '=G155'
ole_1.object.EntryRC(4,7,ls_string)
ls_string = '=G169'
ole_1.object.EntryRC(12,7,ls_string)
ls_string = '=G183'
ole_1.object.EntryRC(20,7,ls_string)
ls_string = '=G197'
ole_1.object.EntryRC(28,7,ls_string)
ls_string = '=G211'
ole_1.object.EntryRC(33,7,ls_string)
ls_string = '=G225'
ole_1.object.EntryRC(38,7,ls_string)
ls_string = '=G239'
ole_1.object.EntryRC(43,7,ls_string)
ls_string = '=G253'
ole_1.object.EntryRC(48,7,ls_string)

ls_string = '=H155'
ole_1.object.EntryRC(4,8,ls_string)
ls_string = '=H169'
ole_1.object.EntryRC(12,8,ls_string)
ls_string = '=H183'
ole_1.object.EntryRC(20,8,ls_string)
ls_string = '=H197'
ole_1.object.EntryRC(28,8,ls_string)
ls_string = '=H211'
ole_1.object.EntryRC(33,8,ls_string)
ls_string = '=H225'
ole_1.object.EntryRC(38,8,ls_string)
ls_string = '=H239'
ole_1.object.EntryRC(43,8,ls_string)
ls_string = '=H253'
ole_1.object.EntryRC(48,8,ls_string)

ls_string = '=I155'
ole_1.object.EntryRC(4,9,ls_string)
ls_string = '=I169'
ole_1.object.EntryRC(12,9,ls_string)
ls_string = '=I183'
ole_1.object.EntryRC(20,9,ls_string)
ls_string = '=I197'
ole_1.object.EntryRC(28,9,ls_string)
ls_string = '=I211'
ole_1.object.EntryRC(33,9,ls_string)
ls_string = '=I225'
ole_1.object.EntryRC(38,9,ls_string)
ls_string = '=I239'
ole_1.object.EntryRC(43,9,ls_string)
ls_string = '=I253'
ole_1.object.EntryRC(48,9,ls_string)

ls_string = '=J155'
ole_1.object.EntryRC(4,10,ls_string)
ls_string = '=J169'
ole_1.object.EntryRC(12,10,ls_string)
ls_string = '=J183'
ole_1.object.EntryRC(20,10,ls_string)
ls_string = '=J197'
ole_1.object.EntryRC(28,10,ls_string)
ls_string = '=J211'
ole_1.object.EntryRC(33,10,ls_string)
ls_string = '=J225'
ole_1.object.EntryRC(38,10,ls_string)
ls_string = '=J239'
ole_1.object.EntryRC(43,10,ls_string)
ls_string = '=J253'
ole_1.object.EntryRC(48,10,ls_string)

ls_string = '=K155'
ole_1.object.EntryRC(4,11,ls_string)
ls_string = '=K169'
ole_1.object.EntryRC(12,11,ls_string)
ls_string = '=K183'
ole_1.object.EntryRC(20,11,ls_string)
ls_string = '=K197'
ole_1.object.EntryRC(28,11,ls_string)
ls_string = '=K211'
ole_1.object.EntryRC(33,11,ls_string)
ls_string = '=K225'
ole_1.object.EntryRC(38,11,ls_string)
ls_string = '=K239'
ole_1.object.EntryRC(43,11,ls_string)
ls_string = '=K253'
ole_1.object.EntryRC(48,11,ls_string)

ls_string = '=L155'
ole_1.object.EntryRC(4,12,ls_string)
ls_string = '=L169'
ole_1.object.EntryRC(12,12,ls_string)
ls_string = '=L183'
ole_1.object.EntryRC(20,12,ls_string)
ls_string = '=L197'
ole_1.object.EntryRC(28,12,ls_string)
ls_string = '=L211'
ole_1.object.EntryRC(33,12,ls_string)
ls_string = '=L225'
ole_1.object.EntryRC(38,12,ls_string)
ls_string = '=L239'
ole_1.object.EntryRC(43,12,ls_string)
ls_string = '=L253'
ole_1.object.EntryRC(48,12,ls_string)

ls_string = '=M155'
ole_1.object.EntryRC(4,13,ls_string)
ls_string = '=M169'
ole_1.object.EntryRC(12,13,ls_string)
ls_string = '=M183'
ole_1.object.EntryRC(20,13,ls_string)
ls_string = '=M197'
ole_1.object.EntryRC(28,13,ls_string)
ls_string = '=M211'
ole_1.object.EntryRC(33,13,ls_string)
ls_string = '=M225'
ole_1.object.EntryRC(38,13,ls_string)
ls_string = '=M239'
ole_1.object.EntryRC(43,13,ls_string)
ls_string = '=M253'
ole_1.object.EntryRC(48,13,ls_string)

ls_string = '=N155'
ole_1.object.EntryRC(4,14,ls_string)
ls_string = '=N169'
ole_1.object.EntryRC(12,14,ls_string)
ls_string = '=N183'
ole_1.object.EntryRC(20,14,ls_string)
ls_string = '=N197'
ole_1.object.EntryRC(28,14,ls_string)
ls_string = '=N211'
ole_1.object.EntryRC(33,14,ls_string)
ls_string = '=N225'
ole_1.object.EntryRC(38,14,ls_string)
ls_string = '=N239'
ole_1.object.EntryRC(43,14,ls_string)
ls_string = '=N253'
ole_1.object.EntryRC(48,14,ls_string)

ls_string = '=O155'
ole_1.object.EntryRC(4,15,ls_string)
ls_string = '=O169'
ole_1.object.EntryRC(12,15,ls_string)
ls_string = '=O183'
ole_1.object.EntryRC(20,15,ls_string)
ls_string = '=O197'
ole_1.object.EntryRC(28,15,ls_string)
ls_string = '=O211'
ole_1.object.EntryRC(33,15,ls_string)
ls_string = '=O225'
ole_1.object.EntryRC(38,15,ls_string)
ls_string = '=O239'
ole_1.object.EntryRC(43,15,ls_string)
ls_string = '=O253'
ole_1.object.EntryRC(48,15,ls_string)

ls_string = '=P155'
ole_1.object.EntryRC(4,16,ls_string)
ls_string = '=P169'
ole_1.object.EntryRC(12,16,ls_string)
ls_string = '=P183'
ole_1.object.EntryRC(20,16,ls_string)
ls_string = '=P197'
ole_1.object.EntryRC(28,16,ls_string)
ls_string = '=P211'
ole_1.object.EntryRC(33,16,ls_string)
ls_string = '=P225'
ole_1.object.EntryRC(38,16,ls_string)
ls_string = '=P239'
ole_1.object.EntryRC(43,16,ls_string)
ls_string = '=P253'
ole_1.object.EntryRC(48,16,ls_string)

ls_string = '=Q155'
ole_1.object.EntryRC(4,17,ls_string)
ls_string = '=Q169'
ole_1.object.EntryRC(12,17,ls_string)
ls_string = '=Q183'
ole_1.object.EntryRC(20,17,ls_string)
ls_string = '=Q197'
ole_1.object.EntryRC(28,17,ls_string)
ls_string = '=Q211'
ole_1.object.EntryRC(33,17,ls_string)
ls_string = '=Q225'
ole_1.object.EntryRC(38,17,ls_string)
ls_string = '=Q239'
ole_1.object.EntryRC(43,17,ls_string)
ls_string = '=Q253'
ole_1.object.EntryRC(48,17,ls_string)
end subroutine

public subroutine wf_calc_inventory_weight ();String		ls_string

ls_string = '=C153'
ole_1.object.EntryRC(3,3,ls_string)

ls_string = '=D153'
ole_1.object.EntryRC(3,4,ls_string)

ls_string = '=E153'
ole_1.object.EntryRC(3,5,ls_string)

ls_string = '=F153'
ole_1.object.EntryRC(3,6,ls_string)

ls_string = '=G153'
ole_1.object.EntryRC(3,7,ls_string)

ls_string = '=H153'
ole_1.object.EntryRC(3,8,ls_string)

ls_string = '=I153'
ole_1.object.EntryRC(3,9,ls_string)

ls_string = '=J153'
ole_1.object.EntryRC(3,10,ls_string)

ls_string = '=K153'
ole_1.object.EntryRC(3,11,ls_string)

ls_string = '=L153'
ole_1.object.EntryRC(3,12,ls_string)

ls_string = '=M153'
ole_1.object.EntryRC(3,13,ls_string)

ls_string = '=N153'
ole_1.object.EntryRC(3,14,ls_string)

ls_string = '=O153'
ole_1.object.EntryRC(3,15,ls_string)

ls_string = '=P153'
ole_1.object.EntryRC(3,16,ls_string)

ls_string = '=Q153'
ole_1.object.EntryRC(3,17,ls_string)
end subroutine

public subroutine wf_calc_loads_reqd_weight ();
String			ls_average_box, &
					ls_uom
					

ls_uom = dw_uom_code.uf_get_uom_code()

SELECT tutltypes.TYPE_SHORT_DESC  
    INTO :ls_average_box  
    FROM tutltypes  
   WHERE ( tutltypes.RECORD_TYPE = "AVAILLDS" ) AND  
         ( tutltypes.TYPE_CODE = :ls_uom );  
			
			
if rb_weight.Checked Then			

	ole_1.object.EntryRC(7,3,"=IF(C301 > 0,IF(C301 > C164, (C301 - C164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,4,"=IF(D301 > 0,IF(D301 > D164, (D301 - D164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,5,"=IF(E301 > 0,IF(E301 > E164, (E301 - E164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,6,"=IF(F301 > 0,IF(F301 > F164, (F301 - F164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,7,"=IF(G301 > 0,IF(G301 > G164, (G301 - G164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,8,"=IF(H301 > 0,IF(H301 > H164, (H301 - H164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,9,"=IF(I301 > 0,IF(I301 > I164, (I301 - I164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,10,"=IF(J301 > 0,IF(J301 > J164, (J301 - J164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,11,"=IF(K301 > 0,IF(K301 > K164, (K301 - K164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,12,"=IF(L301 > 0,IF(L301 > L164, (L301 - L164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,13,"=IF(M301 > 0,IF(M301 > M164, (M301 - M164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,14,"=IF(N301 > 0,IF(N301 > N164, (N301 - N164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,15,"=IF(O301 > 0,IF(O301 > O164, (O301 - O164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,16,"=IF(P301 > 0,IF(P301 > P164, (P301 - P164) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(7,17,"=IF(Q301 > 0,IF(Q301 > Q164, (Q301 - Q164) /" +  &
			ls_average_box + ", 0),0)")
			
	ole_1.object.EntryRC(15,3,"=IF(C302 > 0,IF(C302 > C178, (C302 - C178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,4,"=IF(D302 > 0,IF(D302 > D178, (D302 - D178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,5,"=IF(E302 > 0,IF(E302 > E178, (E302 - E178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,6,"=IF(F302 > 0,IF(F302 > F178, (F302 - F178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,7,"=IF(G302 > 0,IF(G302 > G178, (G302 - G178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,8,"=IF(H302 > 0,IF(H302 > H178, (H302 - H178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,9,"=IF(I302 > 0,IF(I302 > I178, (I302 - I178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,10,"=IF(J302 > 0,IF(J302 > J178, (J302 - J178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,11,"=IF(K302 > 0,IF(K302 > K178, (K302 - K178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,12,"=IF(L302 > 0,IF(L302 > L178, (L302 - L178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,13,"=IF(M302 > 0,IF(M302 > M178, (M302 - M178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,14,"=IF(N302 > 0,IF(N302 > N178, (N302 - N178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,15,"=IF(O302 > 0,IF(O302 > O178, (O302 - O178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,16,"=IF(P302 > 0,IF(P302 > P178, (P302 - P178) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(15,17,"=IF(Q302 > 0,IF(Q302 > Q178, (Q302 - Q178) /" +  &
			ls_average_box + ", 0),0)")
			
	ole_1.object.EntryRC(23,3,"=IF(C303 > 0,IF(C303 > C192, (C303 - C192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,4,"=IF(D303 > 0,IF(D303 > D192, (D303 - D192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,5,"=IF(E303 > 0,IF(E303 > E192, (E303 - E192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,6,"=IF(F303 > 0,IF(F303 > F192, (F303 - F192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,7,"=IF(G303 > 0,IF(G303 > G192, (G303 - G192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,8,"=IF(H303 > 0,IF(H303 > H192, (H303 - H192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,9,"=IF(I303 > 0,IF(I303 > I192, (I303 - I192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,10,"=IF(J303 > 0,IF(J303 > J192, (J303 - J192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,11,"=IF(K303 > 0,IF(K303 > K192, (K303 - K192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,12,"=IF(L303 > 0,IF(L303 > L192, (L303 - L192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,13,"=IF(M303 > 0,IF(M303 > M192, (M303 - M192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,14,"=IF(N303 > 0,IF(N303 > N192, (N303 - N192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,15,"=IF(O303 > 0,IF(O303 > O192, (O303 - O192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,16,"=IF(P303 > 0,IF(P303 > P192, (P303 - P192) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(23,17,"=IF(Q303 > 0,IF(Q303 > Q192, (Q303 - Q192) /" +  &
			ls_average_box + ", 0),0)")

	ole_1.object.EntryRC(31,3,"=IF(C304 > 0,IF(C304 > C206, (C304 - C206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,4,"=IF(D304 > 0,IF(D304 > D206, (D304 - D206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,5,"=IF(E304 > 0,IF(E304 > E206, (E304 - E206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,6,"=IF(F304 > 0,IF(F304 > F206, (F304 - F206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,7,"=IF(G304 > 0,IF(G304 > G206, (G304 - G206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,8,"=IF(H304 > 0,IF(H304 > H206, (H304 - H206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,9,"=IF(I304 > 0,IF(I304 > I206, (I304 - I206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,10,"=IF(J304 > 0,IF(J304 > J206, (J304 - J206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,11,"=IF(K304 > 0,IF(K304 > K206, (K304 - K206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,12,"=IF(L304 > 0,IF(L304 > L206, (L304 - L206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,13,"=IF(M304 > 0,IF(M304 > M206, (M304 - M206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,14,"=IF(N304 > 0,IF(N304 > N206, (N304 - N206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,15,"=IF(O304 > 0,IF(O304 > O206, (O304 - O206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,16,"=IF(P304 > 0,IF(P304 > P206, (P304 - P206) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(31,17,"=IF(Q304 > 0,IF(Q304 > Q206, (Q304 - Q206) /" +  &
			ls_average_box + ", 0),0)")		
			
	ole_1.object.EntryRC(36,3,"=IF(C305 > 0,IF(C305 > C220, (C305 - C220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,4,"=IF(D305 > 0,IF(D305 > D220, (D305 - D220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,5,"=IF(E305 > 0,IF(E305 > E220, (E305 - E220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,6,"=IF(F305 > 0,IF(F305 > F220, (F305 - F220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,7,"=IF(G305 > 0,IF(G305 > G220, (G305 - G220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,8,"=IF(H305 > 0,IF(H305 > H220, (H305 - H220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,9,"=IF(I305 > 0,IF(I305 > I220, (I305 - I220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,10,"=IF(J305 > 0,IF(J305 > J220, (J305 - J220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,11,"=IF(K305 > 0,IF(K305 > K220, (K305 - K220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,12,"=IF(L305 > 0,IF(L305 > L220, (L305 - L220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,13,"=IF(M305 > 0,IF(M305 > M220, (M305 - M220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,14,"=IF(N305 > 0,IF(N305 > N220, (N305 - N220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,15,"=IF(O305 > 0,IF(O305 > O220, (O305 - O220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,16,"=IF(P305 > 0,IF(P305 > P220, (P305 - P220) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(36,17,"=IF(Q305 > 0,IF(Q305 > Q220, (Q305 - Q220) /" +  &
			ls_average_box + ", 0),0)")

	ole_1.object.EntryRC(41,3,"=IF(C306 > 0,IF(C306 > C234, (C306 - C234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,4,"=IF(D306 > 0,IF(D306 > D234, (D306 - D234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,5,"=IF(E306 > 0,IF(E306 > E234, (E306 - E234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,6,"=IF(F306 > 0,IF(F306 > F234, (F306 - F234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,7,"=IF(G306 > 0,IF(G306 > G234, (G306 - G234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,8,"=IF(H306 > 0,IF(H306 > H234, (H306 - H234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,9,"=IF(I306 > 0,IF(I306 > I234, (I306 - I234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,10,"=IF(J306 > 0,IF(J306 > J234, (J306 - J234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,11,"=IF(K306 > 0,IF(K306 > K234, (K306 - K234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,12,"=IF(L306 > 0,IF(L306 > L234, (L306 - L234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,13,"=IF(M306 > 0,IF(M306 > M234, (M306 - M234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,14,"=IF(N306 > 0,IF(N306 > N234, (N306 - N234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,15,"=IF(O306 > 0,IF(O306 > O234, (O306 - O234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,16,"=IF(P306 > 0,IF(P306 > P234, (P306 - P234) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(41,17,"=IF(Q306 > 0,IF(Q306 > Q234, (Q306 - Q234) /" +  &
			ls_average_box + ", 0),0)")
	
	ole_1.object.EntryRC(46,3,"=IF(C307 > 0,IF(C307 > C248, (C307 - C248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,4,"=IF(D307 > 0,IF(D307 > D248, (D307 - D248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,5,"=IF(E307 > 0,IF(E307 > E248, (E307 - E248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,6,"=IF(F307 > 0,IF(F307 > F248, (F307 - F248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,7,"=IF(G307 > 0,IF(G307 > G248, (G307 - G248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,8,"=IF(H307 > 0,IF(H307 > H248, (H307 - H248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,9,"=IF(I307 > 0,IF(I307 > I248, (I307 - I248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,10,"=IF(J307 > 0,IF(J307 > J248, (J307 - J248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,11,"=IF(K307 > 0,IF(K307 > K248, (K307 - K248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,12,"=IF(L307 > 0,IF(L307 > L248, (L307 - L248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,13,"=IF(M307 > 0,IF(M307 > M248, (M307 - M248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,14,"=IF(N307 > 0,IF(N307 > N248, (N307 - N248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,15,"=IF(O307 > 0,IF(O307 > O248, (O307 - O248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,16,"=IF(P307 > 0,IF(P307 > P248, (P307 - P248) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(46,17,"=IF(Q307 > 0,IF(Q307 > Q248, (Q307 - Q248) /" +  &
			ls_average_box + ", 0),0)")

	ole_1.object.EntryRC(51,3,"=IF(C308 > 0,IF(C308 > C262, (C308 - C262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,4,"=IF(D308 > 0,IF(D308 > D262, (D308 - D262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,5,"=IF(E308 > 0,IF(E308 > E262, (E308 - E262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,6,"=IF(F308 > 0,IF(F308 > F262, (F308 - F262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,7,"=IF(G308 > 0,IF(G308 > G262, (G308 - G262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,8,"=IF(H308 > 0,IF(H308 > H262, (H308 - H262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,9,"=IF(I308 > 0,IF(I308 > I262, (I308 - I262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,10,"=IF(J308 > 0,IF(J308 > J262, (J308 - J262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,11,"=IF(K308 > 0,IF(K308 > K262, (K308 - K262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,12,"=IF(L308 > 0,IF(L308 > L262, (L308 - L262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,13,"=IF(M308 > 0,IF(M308 > M262, (M308 - M262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,14,"=IF(N308 > 0,IF(N308 > N262, (N308 - N262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,15,"=IF(O308 > 0,IF(O308 > O262, (O308 - O262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,16,"=IF(P308 > 0,IF(P308 > P262, (P308 - P262) /" +  &
			ls_average_box + ", 0),0)")
	ole_1.object.EntryRC(51,17,"=IF(Q308 > 0,IF(Q308 > Q262, (Q308 - Q262) /" +  &
			ls_average_box + ", 0),0)")	


End If	


end subroutine

public subroutine wf_calc_invent_qty ();String		ls_string

ls_string = '=C153'
ole_1.object.EntryRC(3,3,ls_string)

ls_string = '=D153'
ole_1.object.EntryRC(3,4,ls_string)

ls_string = '=E153'
ole_1.object.EntryRC(3,5,ls_string)

ls_string = '=F153'
ole_1.object.EntryRC(3,6,ls_string)

ls_string = '=G153'
ole_1.object.EntryRC(3,7,ls_string)

ls_string = '=H153'
ole_1.object.EntryRC(3,8,ls_string)

ls_string = '=I153'
ole_1.object.EntryRC(3,9,ls_string)

ls_string = '=J153'
ole_1.object.EntryRC(3,10,ls_string)

ls_string = '=K153'
ole_1.object.EntryRC(3,11,ls_string)

ls_string = '=L153'
ole_1.object.EntryRC(3,12,ls_string)

ls_string = '=M153'
ole_1.object.EntryRC(3,13,ls_string)

ls_string = '=N153'
ole_1.object.EntryRC(3,14,ls_string)

ls_string = '=O153'
ole_1.object.EntryRC(3,15,ls_string)

ls_string = '=P153'
ole_1.object.EntryRC(3,16,ls_string)

ls_string = '=Q153'
ole_1.object.EntryRC(3,17,ls_string)
end subroutine

public subroutine wf_calc_invent_qty_wt ();String		ls_string
ole_1.object.EntryRC(300,3,'=C152')
ls_string = '=C300+C154-(IF($A$79=' + '"' + 'Y' + '"' + ',C156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C162,0)'
ole_1.object.EntryRC(301,3,ls_string)
ls_string = '=C301+C168-(IF($A$79=' + '"' + 'Y' + '"' + ',C170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C176,0)'
ole_1.object.EntryRC(302,3,ls_string)
ls_string = '=C302+C182-(IF($A$79=' + '"' + 'Y' + '"' + ',C184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C190,0)'
ole_1.object.EntryRC(303,3,ls_string)
ls_string = '=C303+C196-(IF($A$79=' + '"' + 'Y' + '"' + ',C198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C204,0)'
ole_1.object.EntryRC(304,3,ls_string)
ls_string = '=C304+C210-(IF($A$79=' + '"' + 'Y' + '"' + ',C212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C218,0)'
ole_1.object.EntryRC(305,3,ls_string)
ls_string = '=C305+C224-(IF($A$79=' + '"' + 'Y' + '"' + ',C226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C232,0)'
ole_1.object.EntryRC(306,3,ls_string)
ls_string = '=C306+C238-(IF($A$79=' + '"' + 'Y' + '"' + ',C240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C246,0)'
ole_1.object.EntryRC(307,3,ls_string)
ls_string = '=C307+C252-(IF($A$79=' + '"' + 'Y' + '"' + ',C254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C260,0)'
ole_1.object.EntryRC(308,3,ls_string)

ole_1.object.EntryRC(300,4,'=D152')
ls_string = '=D300+D154-(IF($A$79=' + '"' + 'Y' + '"' + ',D156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D162,0)'
ole_1.object.EntryRC(301,4,ls_string)
ls_string = '=D301+D168-(IF($A$79=' + '"' + 'Y' + '"' + ',D170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D176,0)'
ole_1.object.EntryRC(302,4,ls_string)
ls_string = '=D302+D182-(IF($A$79=' + '"' + 'Y' + '"' + ',D184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D190,0)'
ole_1.object.EntryRC(303,4,ls_string)
ls_string = '=D303+D196-(IF($A$79=' + '"' + 'Y' + '"' + ',D198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D204,0)'
ole_1.object.EntryRC(304,4,ls_string)
ls_string = '=D304+D210-(IF($A$79=' + '"' + 'Y' + '"' + ',D212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D218,0)'
ole_1.object.EntryRC(305,4,ls_string)
ls_string = '=D305+D224-(IF($A$79=' + '"' + 'Y' + '"' + ',D226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D232,0)'
ole_1.object.EntryRC(306,4,ls_string)
ls_string = '=D306+D238-(IF($A$79=' + '"' + 'Y' + '"' + ',D240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D246,0)'
ole_1.object.EntryRC(307,4,ls_string)
ls_string = '=D307+D252-(IF($A$79=' + '"' + 'Y' + '"' + ',D254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D260,0)'
ole_1.object.EntryRC(308,4,ls_string)

ole_1.object.EntryRC(300,5,'=E152')
ls_string = '=E300+E154-(IF($A$79=' + '"' + 'Y' + '"' + ',E156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E162,0)'
ole_1.object.EntryRC(301,5,ls_string)
ls_string = '=E301+E168-(IF($A$79=' + '"' + 'Y' + '"' + ',E170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E176,0)'
ole_1.object.EntryRC(302,5,ls_string)
ls_string = '=E302+E182-(IF($A$79=' + '"' + 'Y' + '"' + ',E184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E190,0)'
ole_1.object.EntryRC(303,5,ls_string)
ls_string = '=E303+E196-(IF($A$79=' + '"' + 'Y' + '"' + ',E198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E204,0)'
ole_1.object.EntryRC(304,5,ls_string)
ls_string = '=E304+E210-(IF($A$79=' + '"' + 'Y' + '"' + ',E212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E218,0)'
ole_1.object.EntryRC(305,5,ls_string)
ls_string = '=E305+E224-(IF($A$79=' + '"' + 'Y' + '"' + ',E226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E232,0)'
ole_1.object.EntryRC(306,5,ls_string)
ls_string = '=E306+E238-(IF($A$79=' + '"' + 'Y' + '"' + ',E240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E246,0)'
ole_1.object.EntryRC(307,5,ls_string)
ls_string = '=E307+E252-(IF($A$79=' + '"' + 'Y' + '"' + ',E254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E260,0)'
ole_1.object.EntryRC(308,5,ls_string)

ole_1.object.EntryRC(300,6,'=F152')
ls_string = '=F300+F154-(IF($A$79=' + '"' + 'Y' + '"' + ',F156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F162,0)'
ole_1.object.EntryRC(301,6,ls_string)
ls_string = '=F301+F168-(IF($A$79=' + '"' + 'Y' + '"' + ',F170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F176,0)'
ole_1.object.EntryRC(302,6,ls_string)
ls_string = '=F302+F182-(IF($A$79=' + '"' + 'Y' + '"' + ',F184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F190,0)'
ole_1.object.EntryRC(303,6,ls_string)
ls_string = '=F303+F196-(IF($A$79=' + '"' + 'Y' + '"' + ',F198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F204,0)'
ole_1.object.EntryRC(304,6,ls_string)
ls_string = '=F304+F210-(IF($A$79=' + '"' + 'Y' + '"' + ',F212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F218,0)'
ole_1.object.EntryRC(305,6,ls_string)
ls_string = '=F305+F224-(IF($A$79=' + '"' + 'Y' + '"' + ',F226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F232,0)'
ole_1.object.EntryRC(306,6,ls_string)
ls_string = '=F306+F238-(IF($A$79=' + '"' + 'Y' + '"' + ',F240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F246,0)'
ole_1.object.EntryRC(307,6,ls_string)
ls_string = '=F307+F252-(IF($A$79=' + '"' + 'Y' + '"' + ',F254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F260,0)'
ole_1.object.EntryRC(308,6,ls_string)

ole_1.object.EntryRC(300,7,'=G152')
ls_string = '=G300+G154-(IF($A$79=' + '"' + 'Y' + '"' + ',G156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G162,0)'
ole_1.object.EntryRC(301,7,ls_string)
ls_string = '=G301+G168-(IF($A$79=' + '"' + 'Y' + '"' + ',G170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G176,0)'
ole_1.object.EntryRC(302,7,ls_string)
ls_string = '=G302+G182-(IF($A$79=' + '"' + 'Y' + '"' + ',G184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G190,0)'
ole_1.object.EntryRC(303,7,ls_string)
ls_string = '=G303+G196-(IF($A$79=' + '"' + 'Y' + '"' + ',G198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G204,0)'
ole_1.object.EntryRC(304,7,ls_string)
ls_string = '=G304+G210-(IF($A$79=' + '"' + 'Y' + '"' + ',G212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G218,0)'
ole_1.object.EntryRC(305,7,ls_string)
ls_string = '=G305+G224-(IF($A$79=' + '"' + 'Y' + '"' + ',G226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G232,0)'
ole_1.object.EntryRC(306,7,ls_string)
ls_string = '=G306+G238-(IF($A$79=' + '"' + 'Y' + '"' + ',G240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G246,0)'
ole_1.object.EntryRC(307,7,ls_string)
ls_string = '=G307+G252-(IF($A$79=' + '"' + 'Y' + '"' + ',G254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G260,0)'
ole_1.object.EntryRC(308,7,ls_string)

ole_1.object.EntryRC(300,8,'=H152')
ls_string = '=H300+H154-(IF($A$79=' + '"' + 'Y' + '"' + ',H156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H162,0)'
ole_1.object.EntryRC(301,8,ls_string)
ls_string = '=H301+H168-(IF($A$79=' + '"' + 'Y' + '"' + ',H170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H176,0)'
ole_1.object.EntryRC(302,8,ls_string)
ls_string = '=H302+H182-(IF($A$79=' + '"' + 'Y' + '"' + ',H184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H190,0)'
ole_1.object.EntryRC(303,8,ls_string)
ls_string = '=H303+H196-(IF($A$79=' + '"' + 'Y' + '"' + ',H198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H204,0)'
ole_1.object.EntryRC(304,8,ls_string)
ls_string = '=H304+H210-(IF($A$79=' + '"' + 'Y' + '"' + ',H212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H218,0)'
ole_1.object.EntryRC(305,8,ls_string)
ls_string = '=H305+H224-(IF($A$79=' + '"' + 'Y' + '"' + ',H226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H232,0)'
ole_1.object.EntryRC(306,8,ls_string)
ls_string = '=H306+H238-(IF($A$79=' + '"' + 'Y' + '"' + ',H240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H246,0)'
ole_1.object.EntryRC(307,8,ls_string)
ls_string = '=H307+H252-(IF($A$79=' + '"' + 'Y' + '"' + ',H254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',H260,0)'
ole_1.object.EntryRC(308,8,ls_string)

ole_1.object.EntryRC(300,9,'=I152')
ls_string = '=I300+I154-(IF($A$79=' + '"' + 'Y' + '"' + ',I156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I162,0)'
ole_1.object.EntryRC(301,9,ls_string)
ls_string = '=I301+I168-(IF($A$79=' + '"' + 'Y' + '"' + ',I170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I176,0)'
ole_1.object.EntryRC(302,9,ls_string)
ls_string = '=I302+I182-(IF($A$79=' + '"' + 'Y' + '"' + ',I184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I190,0)'
ole_1.object.EntryRC(303,9,ls_string)
ls_string = '=I303+I196-(IF($A$79=' + '"' + 'Y' + '"' + ',I198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I204,0)'
ole_1.object.EntryRC(304,9,ls_string)
ls_string = '=I304+I210-(IF($A$79=' + '"' + 'Y' + '"' + ',I212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I218,0)'
ole_1.object.EntryRC(305,9,ls_string)
ls_string = '=I305+I224-(IF($A$79=' + '"' + 'Y' + '"' + ',I226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I232,0)'
ole_1.object.EntryRC(306,9,ls_string)
ls_string = '=I306+I238-(IF($A$79=' + '"' + 'Y' + '"' + ',I240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I246,0)'
ole_1.object.EntryRC(307,9,ls_string)
ls_string = '=I307+I252-(IF($A$79=' + '"' + 'Y' + '"' + ',I254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',I260,0)'
ole_1.object.EntryRC(308,9,ls_string)

ole_1.object.EntryRC(300,10,'=J152')
ls_string = '=J300+J154-(IF($A$79=' + '"' + 'Y' + '"' + ',J156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J162,0)'
ole_1.object.EntryRC(301,10,ls_string)
ls_string = '=J301+J168-(IF($A$79=' + '"' + 'Y' + '"' + ',J170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J176,0)'
ole_1.object.EntryRC(302,10,ls_string)
ls_string = '=J302+J182-(IF($A$79=' + '"' + 'Y' + '"' + ',J184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J190,0)'
ole_1.object.EntryRC(303,10,ls_string)
ls_string = '=J303+J196-(IF($A$79=' + '"' + 'Y' + '"' + ',J198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J204,0)'
ole_1.object.EntryRC(304,10,ls_string)
ls_string = '=J304+J210-(IF($A$79=' + '"' + 'Y' + '"' + ',J212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J218,0)'
ole_1.object.EntryRC(305,10,ls_string)
ls_string = '=J305+J224-(IF($A$79=' + '"' + 'Y' + '"' + ',J226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J232,0)'
ole_1.object.EntryRC(306,10,ls_string)
ls_string = '=J306+J238-(IF($A$79=' + '"' + 'Y' + '"' + ',J240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J246,0)'
ole_1.object.EntryRC(307,10,ls_string)
ls_string = '=J307+J252-(IF($A$79=' + '"' + 'Y' + '"' + ',J254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',J260,0)'
ole_1.object.EntryRC(308,10,ls_string)

ole_1.object.EntryRC(300,11,'=K152')
ls_string = '=K300+K154-(IF($A$79=' + '"' + 'Y' + '"' + ',K156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K162,0)'
ole_1.object.EntryRC(301,11,ls_string)
ls_string = '=K301+K168-(IF($A$79=' + '"' + 'Y' + '"' + ',K170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K176,0)'
ole_1.object.EntryRC(302,11,ls_string)
ls_string = '=K302+K182-(IF($A$79=' + '"' + 'Y' + '"' + ',K184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K190,0)'
ole_1.object.EntryRC(303,11,ls_string)
ls_string = '=K303+K196-(IF($A$79=' + '"' + 'Y' + '"' + ',K198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K204,0)'
ole_1.object.EntryRC(304,11,ls_string)
ls_string = '=K304+K210-(IF($A$79=' + '"' + 'Y' + '"' + ',K212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K218,0)'
ole_1.object.EntryRC(305,11,ls_string)
ls_string = '=K305+K224-(IF($A$79=' + '"' + 'Y' + '"' + ',K226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K232,0)'
ole_1.object.EntryRC(306,11,ls_string)
ls_string = '=K306+K238-(IF($A$79=' + '"' + 'Y' + '"' + ',K240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K246,0)'
ole_1.object.EntryRC(307,11,ls_string)
ls_string = '=K307+K252-(IF($A$79=' + '"' + 'Y' + '"' + ',K254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',K260,0)'
ole_1.object.EntryRC(308,11,ls_string)

ole_1.object.EntryRC(300,12,'=L152')
ls_string = '=L300+L154-(IF($A$79=' + '"' + 'Y' + '"' + ',L156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L162,0)'
ole_1.object.EntryRC(301,12,ls_string)
ls_string = '=L301+L168-(IF($A$79=' + '"' + 'Y' + '"' + ',L170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L176,0)'
ole_1.object.EntryRC(302,12,ls_string)
ls_string = '=L302+L182-(IF($A$79=' + '"' + 'Y' + '"' + ',L184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L190,0)'
ole_1.object.EntryRC(303,12,ls_string)
ls_string = '=L303+L196-(IF($A$79=' + '"' + 'Y' + '"' + ',L198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L204,0)'
ole_1.object.EntryRC(304,12,ls_string)
ls_string = '=L304+L210-(IF($A$79=' + '"' + 'Y' + '"' + ',L212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L218,0)'
ole_1.object.EntryRC(305,12,ls_string)
ls_string = '=L305+L224-(IF($A$79=' + '"' + 'Y' + '"' + ',L226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L232,0)'
ole_1.object.EntryRC(306,12,ls_string)
ls_string = '=L306+L238-(IF($A$79=' + '"' + 'Y' + '"' + ',L240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L246,0)'
ole_1.object.EntryRC(307,12,ls_string)
ls_string = '=L307+L252-(IF($A$79=' + '"' + 'Y' + '"' + ',L254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',L260,0)'
ole_1.object.EntryRC(308,12,ls_string)

ole_1.object.EntryRC(300,13,'=M152')
ls_string = '=M300+M154-(IF($A$79=' + '"' + 'Y' + '"' + ',M156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M162,0)'
ole_1.object.EntryRC(301,13,ls_string)
ls_string = '=M301+M168-(IF($A$79=' + '"' + 'Y' + '"' + ',M170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M176,0)'
ole_1.object.EntryRC(302,13,ls_string)
ls_string = '=M302+M182-(IF($A$79=' + '"' + 'Y' + '"' + ',M184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M190,0)'
ole_1.object.EntryRC(303,13,ls_string)
ls_string = '=M303+M196-(IF($A$79=' + '"' + 'Y' + '"' + ',M198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M204,0)'
ole_1.object.EntryRC(304,13,ls_string)
ls_string = '=M304+M210-(IF($A$79=' + '"' + 'Y' + '"' + ',M212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M218,0)'
ole_1.object.EntryRC(305,13,ls_string)
ls_string = '=M305+M224-(IF($A$79=' + '"' + 'Y' + '"' + ',M226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M232,0)'
ole_1.object.EntryRC(306,13,ls_string)
ls_string = '=M306+M238-(IF($A$79=' + '"' + 'Y' + '"' + ',M240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M246,0)'
ole_1.object.EntryRC(307,13,ls_string)
ls_string = '=M307+M252-(IF($A$79=' + '"' + 'Y' + '"' + ',M254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',M260,0)'
ole_1.object.EntryRC(308,13,ls_string)

ole_1.object.EntryRC(300,14,'=N152')
ls_string = '=N300+N154-(IF($A$79=' + '"' + 'Y' + '"' + ',N156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N162,0)'
ole_1.object.EntryRC(301,14,ls_string)
ls_string = '=N301+N168-(IF($A$79=' + '"' + 'Y' + '"' + ',N170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N176,0)'
ole_1.object.EntryRC(302,14,ls_string)
ls_string = '=N302+N182-(IF($A$79=' + '"' + 'Y' + '"' + ',N184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N190,0)'
ole_1.object.EntryRC(303,14,ls_string)
ls_string = '=N303+N196-(IF($A$79=' + '"' + 'Y' + '"' + ',N198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N204,0)'
ole_1.object.EntryRC(304,14,ls_string)
ls_string = '=N304+N210-(IF($A$79=' + '"' + 'Y' + '"' + ',N212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N218,0)'
ole_1.object.EntryRC(305,14,ls_string)
ls_string = '=N305+N224-(IF($A$79=' + '"' + 'Y' + '"' + ',N226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N232,0)'
ole_1.object.EntryRC(306,14,ls_string)
ls_string = '=N306+N238-(IF($A$79=' + '"' + 'Y' + '"' + ',N240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N246,0)'
ole_1.object.EntryRC(307,14,ls_string)
ls_string = '=N307+N252-(IF($A$79=' + '"' + 'Y' + '"' + ',N254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',N260,0)'
ole_1.object.EntryRC(308,14,ls_string)

ole_1.object.EntryRC(300,15,'=O152')
ls_string = '=O300+O154-(IF($A$79=' + '"' + 'Y' + '"' + ',O156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O162,0)'
ole_1.object.EntryRC(301,15,ls_string)
ls_string = '=O301+O168-(IF($A$79=' + '"' + 'Y' + '"' + ',O170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O176,0)'
ole_1.object.EntryRC(302,15,ls_string)
ls_string = '=O302+O182-(IF($A$79=' + '"' + 'Y' + '"' + ',O184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O190,0)'
ole_1.object.EntryRC(303,15,ls_string)
ls_string = '=O303+O196-(IF($A$79=' + '"' + 'Y' + '"' + ',O198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O204,0)'
ole_1.object.EntryRC(304,15,ls_string)
ls_string = '=O304+O210-(IF($A$79=' + '"' + 'Y' + '"' + ',O212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O218,0)'
ole_1.object.EntryRC(305,15,ls_string)
ls_string = '=O305+O224-(IF($A$79=' + '"' + 'Y' + '"' + ',O226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O232,0)'
ole_1.object.EntryRC(306,15,ls_string)
ls_string = '=O306+O238-(IF($A$79=' + '"' + 'Y' + '"' + ',O240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O246,0)'
ole_1.object.EntryRC(307,15,ls_string)
ls_string = '=O307+O252-(IF($A$79=' + '"' + 'Y' + '"' + ',O254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',O260,0)'
ole_1.object.EntryRC(308,15,ls_string)

ole_1.object.EntryRC(300,16,'=P152')
ls_string = '=P300+P154-(IF($A$79=' + '"' + 'Y' + '"' + ',P156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P162,0)'
ole_1.object.EntryRC(301,16,ls_string)
ls_string = '=P301+P168-(IF($A$79=' + '"' + 'Y' + '"' + ',P170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P176,0)'
ole_1.object.EntryRC(302,16,ls_string)
ls_string = '=P302+P182-(IF($A$79=' + '"' + 'Y' + '"' + ',P184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P190,0)'
ole_1.object.EntryRC(303,16,ls_string)
ls_string = '=P303+P196-(IF($A$79=' + '"' + 'Y' + '"' + ',P198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P204,0)'
ole_1.object.EntryRC(304,16,ls_string)
ls_string = '=P304+P210-(IF($A$79=' + '"' + 'Y' + '"' + ',P212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P218,0)'
ole_1.object.EntryRC(305,16,ls_string)
ls_string = '=P305+P224-(IF($A$79=' + '"' + 'Y' + '"' + ',P226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P232,0)'
ole_1.object.EntryRC(306,16,ls_string)
ls_string = '=P306+P238-(IF($A$79=' + '"' + 'Y' + '"' + ',P240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P246,0)'
ole_1.object.EntryRC(307,16,ls_string)
ls_string = '=P307+P252-(IF($A$79=' + '"' + 'Y' + '"' + ',P254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',P260,0)'
ole_1.object.EntryRC(308,16,ls_string)

ole_1.object.EntryRC(300,17,'=Q152')
ls_string = '=Q300+Q154-(IF($A$79=' + '"' + 'Y' + '"' + ',Q156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q160,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q162,0)'
ole_1.object.EntryRC(301,17,ls_string)
ls_string = '=Q301+Q168-(IF($A$79=' + '"' + 'Y' + '"' + ',Q170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q174,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q176,0)'
ole_1.object.EntryRC(302,17,ls_string)
ls_string = '=Q302+Q182-(IF($A$79=' + '"' + 'Y' + '"' + ',Q184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q188,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q190,0)'
ole_1.object.EntryRC(303,17,ls_string)
ls_string = '=Q303+Q196-(IF($A$79=' + '"' + 'Y' + '"' + ',Q198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q202,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q204,0)'
ole_1.object.EntryRC(304,17,ls_string)
ls_string = '=Q304+Q210-(IF($A$79=' + '"' + 'Y' + '"' + ',Q212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q216,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q218,0)'
ole_1.object.EntryRC(305,17,ls_string)
ls_string = '=Q305+Q224-(IF($A$79=' + '"' + 'Y' + '"' + ',Q226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q230,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q232,0)'
ole_1.object.EntryRC(306,17,ls_string)
ls_string = '=Q306+Q238-(IF($A$79=' + '"' + 'Y' + '"' + ',Q240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q244,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q246,0)'
ole_1.object.EntryRC(307,17,ls_string)
ls_string = '=Q307+Q252-(IF($A$79=' + '"' + 'Y' + '"' + ',Q254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q258,0))'
ls_string += '-' + 'IF($A$82=' + '"' + 'Y' + '"' + ',Q260,0)'
ole_1.object.EntryRC(308,17,ls_string)

end subroutine

public subroutine wf_reset_invent_qty ();String		ls_string

ls_string = '=C152'
ole_1.object.EntryRC(3,3,ls_string)

ls_string = '=D152'
ole_1.object.EntryRC(3,4,ls_string)

ls_string = '=E152'
ole_1.object.EntryRC(3,5,ls_string)

ls_string = '=F152'
ole_1.object.EntryRC(3,6,ls_string)

ls_string = '=G152'
ole_1.object.EntryRC(3,7,ls_string)

ls_string = '=H152'
ole_1.object.EntryRC(3,8,ls_string)

ls_string = '=I152'
ole_1.object.EntryRC(3,9,ls_string)

ls_string = '=J152'
ole_1.object.EntryRC(3,10,ls_string)

ls_string = '=K152'
ole_1.object.EntryRC(3,11,ls_string)

ls_string = '=L152'
ole_1.object.EntryRC(3,12,ls_string)

ls_string = '=M152'
ole_1.object.EntryRC(3,13,ls_string)

ls_string = '=N152'
ole_1.object.EntryRC(3,14,ls_string)

ls_string = '=O152'
ole_1.object.EntryRC(3,15,ls_string)

ls_string = '=P152'
ole_1.object.EntryRC(3,16,ls_string)

ls_string = '=Q152'
ole_1.object.EntryRC(3,17,ls_string)
end subroutine

public subroutine wf_reset_sales_weight1 ();String		ls_string	
	
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C161,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C163,0)'
ole_1.object.EntryRC(5,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C175,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C177,0)'
ole_1.object.EntryRC(13,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C189,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C191,0)'
ole_1.object.EntryRC(21,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C203,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C205,0)'
ole_1.object.EntryRC(29,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C217,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C219,0)'
ole_1.object.EntryRC(34,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C231,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C233,0)'
ole_1.object.EntryRC(39,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C245,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C247,0)'
ole_1.object.EntryRC(43,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C259,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',C261,0)'
ole_1.object.EntryRC(49,3,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D161,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D163,0)'
ole_1.object.EntryRC(5,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D175,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D177,0)'
ole_1.object.EntryRC(13,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D189,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D191,0)'
ole_1.object.EntryRC(21,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D203,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D205,0)'
ole_1.object.EntryRC(29,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D217,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D219,0)'
ole_1.object.EntryRC(34,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D231,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D233,0)'
ole_1.object.EntryRC(39,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D245,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D247,0)'
ole_1.object.EntryRC(43,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D259,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',D261,0)'
ole_1.object.EntryRC(49,4,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E161,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E163,0)'
ole_1.object.EntryRC(5,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E175,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E177,0)'
ole_1.object.EntryRC(13,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E189,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E191,0)'
ole_1.object.EntryRC(21,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E203,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E205,0)'
ole_1.object.EntryRC(29,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E217,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E219,0)'
ole_1.object.EntryRC(34,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E231,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E233,0)'
ole_1.object.EntryRC(39,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E245,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E247,0)'
ole_1.object.EntryRC(43,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E259,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',E261,0)'
ole_1.object.EntryRC(49,5,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F161,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F163,0)'
ole_1.object.EntryRC(5,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F175,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F177,0)'
ole_1.object.EntryRC(13,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F189,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F191,0)'
ole_1.object.EntryRC(21,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F203,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F205,0)'
ole_1.object.EntryRC(29,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F217,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F219,0)'
ole_1.object.EntryRC(34,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F231,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F233,0)'
ole_1.object.EntryRC(39,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F245,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F247,0)'
ole_1.object.EntryRC(43,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F259,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',F261,0)'
ole_1.object.EntryRC(49,6,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G161,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G163,0)'
ole_1.object.EntryRC(5,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G175,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G177,0)'
ole_1.object.EntryRC(13,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G189,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G191,0)'
ole_1.object.EntryRC(21,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G203,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G205,0)'
ole_1.object.EntryRC(29,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G217,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G219,0)'
ole_1.object.EntryRC(34,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G231,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G233,0)'
ole_1.object.EntryRC(39,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G245,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G247,0)'
ole_1.object.EntryRC(43,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G259,0)'
ls_string += '+' + 'IF($A$82=' + '"' + 'Y' + '"' + ',G261,0)'
ole_1.object.EntryRC(49,7,ls_string)	

end subroutine

public subroutine wf_reset_production_qty ();String		ls_string

ls_string = '=C154'
ole_1.object.EntryRC(4,3,ls_string)
ls_string = '=C168'
ole_1.object.EntryRC(12,3,ls_string)
ls_string = '=C182'
ole_1.object.EntryRC(20,3,ls_string)
ls_string = '=C196'
ole_1.object.EntryRC(28,3,ls_string)
ls_string = '=C210'
ole_1.object.EntryRC(33,3,ls_string)
ls_string = '=C224'
ole_1.object.EntryRC(38,3,ls_string)
ls_string = '=C238'
ole_1.object.EntryRC(43,3,ls_string)
ls_string = '=C252'
ole_1.object.EntryRC(48,3,ls_string)

ls_string = '=D154'
ole_1.object.EntryRC(4,4,ls_string)
ls_string = '=D168'
ole_1.object.EntryRC(12,4,ls_string)
ls_string = '=D182'
ole_1.object.EntryRC(20,4,ls_string)
ls_string = '=D196'
ole_1.object.EntryRC(28,4,ls_string)
ls_string = '=D210'
ole_1.object.EntryRC(34,4,ls_string)
ls_string = '=D224'
ole_1.object.EntryRC(38,4,ls_string)
ls_string = '=D238'
ole_1.object.EntryRC(43,4,ls_string)
ls_string = '=D252'
ole_1.object.EntryRC(48,4,ls_string)

ls_string = '=E154'
ole_1.object.EntryRC(4,5,ls_string)
ls_string = '=E168'
ole_1.object.EntryRC(12,5,ls_string)
ls_string = '=E182'
ole_1.object.EntryRC(20,5,ls_string)
ls_string = '=E196'
ole_1.object.EntryRC(28,5,ls_string)
ls_string = '=E210'
ole_1.object.EntryRC(34,5,ls_string)
ls_string = '=E224'
ole_1.object.EntryRC(38,5,ls_string)
ls_string = '=E238'
ole_1.object.EntryRC(43,5,ls_string)
ls_string = '=E252'
ole_1.object.EntryRC(48,5,ls_string)

ls_string = '=F154'
ole_1.object.EntryRC(4,6,ls_string)
ls_string = '=F168'
ole_1.object.EntryRC(12,6,ls_string)
ls_string = '=F182'
ole_1.object.EntryRC(20,6,ls_string)
ls_string = '=F196'
ole_1.object.EntryRC(28,6,ls_string)
ls_string = '=F210'
ole_1.object.EntryRC(34,6,ls_string)
ls_string = '=F224'
ole_1.object.EntryRC(38,6,ls_string)
ls_string = '=F238'
ole_1.object.EntryRC(43,6,ls_string)
ls_string = '=F252'
ole_1.object.EntryRC(48,6,ls_string)

ls_string = '=G154'
ole_1.object.EntryRC(4,7,ls_string)
ls_string = '=G168'
ole_1.object.EntryRC(12,7,ls_string)
ls_string = '=G182'
ole_1.object.EntryRC(20,7,ls_string)
ls_string = '=G196'
ole_1.object.EntryRC(28,7,ls_string)
ls_string = '=G210'
ole_1.object.EntryRC(34,7,ls_string)
ls_string = '=G224'
ole_1.object.EntryRC(38,7,ls_string)
ls_string = '=G238'
ole_1.object.EntryRC(43,7,ls_string)
ls_string = '=G252'
ole_1.object.EntryRC(48,7,ls_string)

ls_string = '=H154'
ole_1.object.EntryRC(4,8,ls_string)
ls_string = '=H168'
ole_1.object.EntryRC(12,8,ls_string)
ls_string = '=H182'
ole_1.object.EntryRC(20,8,ls_string)
ls_string = '=H196'
ole_1.object.EntryRC(28,8,ls_string)
ls_string = '=H210'
ole_1.object.EntryRC(34,8,ls_string)
ls_string = '=H224'
ole_1.object.EntryRC(38,8,ls_string)
ls_string = '=H238'
ole_1.object.EntryRC(43,8,ls_string)
ls_string = '=H252'
ole_1.object.EntryRC(48,8,ls_string)

ls_string = '=I154'
ole_1.object.EntryRC(4,9,ls_string)
ls_string = '=I168'
ole_1.object.EntryRC(12,9,ls_string)
ls_string = '=I182'
ole_1.object.EntryRC(20,9,ls_string)
ls_string = '=I196'
ole_1.object.EntryRC(28,9,ls_string)
ls_string = '=I210'
ole_1.object.EntryRC(34,9,ls_string)
ls_string = '=I224'
ole_1.object.EntryRC(38,9,ls_string)
ls_string = '=I238'
ole_1.object.EntryRC(43,9,ls_string)
ls_string = '=I252'
ole_1.object.EntryRC(48,9,ls_string)

ls_string = '=J154'
ole_1.object.EntryRC(4,10,ls_string)
ls_string = '=J168'
ole_1.object.EntryRC(12,10,ls_string)
ls_string = '=J182'
ole_1.object.EntryRC(20,10,ls_string)
ls_string = '=J196'
ole_1.object.EntryRC(28,10,ls_string)
ls_string = '=J210'
ole_1.object.EntryRC(34,10,ls_string)
ls_string = '=J224'
ole_1.object.EntryRC(38,10,ls_string)
ls_string = '=J238'
ole_1.object.EntryRC(43,10,ls_string)
ls_string = '=J252'
ole_1.object.EntryRC(48,10,ls_string)

ls_string = '=K154'
ole_1.object.EntryRC(4,11,ls_string)
ls_string = '=K168'
ole_1.object.EntryRC(12,11,ls_string)
ls_string = '=K182'
ole_1.object.EntryRC(20,11,ls_string)
ls_string = '=K196'
ole_1.object.EntryRC(28,11,ls_string)
ls_string = '=K210'
ole_1.object.EntryRC(34,11,ls_string)
ls_string = '=K224'
ole_1.object.EntryRC(38,11,ls_string)
ls_string = '=K238'
ole_1.object.EntryRC(43,11,ls_string)
ls_string = '=K252'
ole_1.object.EntryRC(48,11,ls_string)

ls_string = '=L154'
ole_1.object.EntryRC(4,12,ls_string)
ls_string = '=L168'
ole_1.object.EntryRC(12,12,ls_string)
ls_string = '=L182'
ole_1.object.EntryRC(20,12,ls_string)
ls_string = '=L196'
ole_1.object.EntryRC(28,12,ls_string)
ls_string = '=L210'
ole_1.object.EntryRC(34,12,ls_string)
ls_string = '=L224'
ole_1.object.EntryRC(38,12,ls_string)
ls_string = '=L238'
ole_1.object.EntryRC(43,12,ls_string)
ls_string = '=L252'
ole_1.object.EntryRC(48,12,ls_string)

ls_string = '=M154'
ole_1.object.EntryRC(4,13,ls_string)
ls_string = '=M168'
ole_1.object.EntryRC(12,13,ls_string)
ls_string = '=M182'
ole_1.object.EntryRC(20,13,ls_string)
ls_string = '=M196'
ole_1.object.EntryRC(28,13,ls_string)
ls_string = '=M210'
ole_1.object.EntryRC(34,13,ls_string)
ls_string = '=M224'
ole_1.object.EntryRC(38,13,ls_string)
ls_string = '=M238'
ole_1.object.EntryRC(43,13,ls_string)
ls_string = '=M252'
ole_1.object.EntryRC(48,13,ls_string)

ls_string = '=N154'
ole_1.object.EntryRC(4,14,ls_string)
ls_string = '=N168'
ole_1.object.EntryRC(12,14,ls_string)
ls_string = '=N182'
ole_1.object.EntryRC(20,14,ls_string)
ls_string = '=N196'
ole_1.object.EntryRC(28,14,ls_string)
ls_string = '=N210'
ole_1.object.EntryRC(34,14,ls_string)
ls_string = '=N224'
ole_1.object.EntryRC(38,14,ls_string)
ls_string = '=N238'
ole_1.object.EntryRC(43,14,ls_string)
ls_string = '=N252'
ole_1.object.EntryRC(48,14,ls_string)

ls_string = '=O154'
ole_1.object.EntryRC(4,15,ls_string)
ls_string = '=O168'
ole_1.object.EntryRC(12,15,ls_string)
ls_string = '=O182'
ole_1.object.EntryRC(20,15,ls_string)
ls_string = '=O196'
ole_1.object.EntryRC(28,15,ls_string)
ls_string = '=O210'
ole_1.object.EntryRC(34,15,ls_string)
ls_string = '=O224'
ole_1.object.EntryRC(38,15,ls_string)
ls_string = '=O238'
ole_1.object.EntryRC(43,15,ls_string)
ls_string = '=O252'
ole_1.object.EntryRC(48,15,ls_string)

ls_string = '=P154'
ole_1.object.EntryRC(4,16,ls_string)
ls_string = '=P168'
ole_1.object.EntryRC(12,16,ls_string)
ls_string = '=P182'
ole_1.object.EntryRC(20,16,ls_string)
ls_string = '=P196'
ole_1.object.EntryRC(28,16,ls_string)
ls_string = '=P210'
ole_1.object.EntryRC(34,16,ls_string)
ls_string = '=P224'
ole_1.object.EntryRC(38,16,ls_string)
ls_string = '=P238'
ole_1.object.EntryRC(43,16,ls_string)
ls_string = '=P252'
ole_1.object.EntryRC(48,16,ls_string)

ls_string = '=Q154'
ole_1.object.EntryRC(4,17,ls_string)
ls_string = '=Q168'
ole_1.object.EntryRC(12,17,ls_string)
ls_string = '=Q182'
ole_1.object.EntryRC(20,17,ls_string)
ls_string = '=Q196'
ole_1.object.EntryRC(28,17,ls_string)
ls_string = '=Q210'
ole_1.object.EntryRC(34,17,ls_string)
ls_string = '=Q224'
ole_1.object.EntryRC(38,17,ls_string)
ls_string = '=Q238'
ole_1.object.EntryRC(43,17,ls_string)
ls_string = '=Q252'
ole_1.object.EntryRC(48,17,ls_string)
end subroutine

public subroutine wf_calc_sales_weight ();String		ls_string	
	
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C161,0)'
ole_1.object.EntryRC(5,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C175,0)'
ole_1.object.EntryRC(13,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C189,0)'
ole_1.object.EntryRC(21,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C203,0)'
ole_1.object.EntryRC(29,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C217,0)'
ole_1.object.EntryRC(34,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C231,0)'
ole_1.object.EntryRC(39,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C245,0)'
ole_1.object.EntryRC(44,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C259,0)'
ole_1.object.EntryRC(49,3,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D161,0)'
ole_1.object.EntryRC(5,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D175,0)'
ole_1.object.EntryRC(13,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D189,0)'
ole_1.object.EntryRC(21,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D203,0)'
ole_1.object.EntryRC(29,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D217,0)'
ole_1.object.EntryRC(34,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D231,0)'
ole_1.object.EntryRC(39,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D245,0)'
ole_1.object.EntryRC(44,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D259,0)'
ole_1.object.EntryRC(49,4,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E161,0)'
ole_1.object.EntryRC(5,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E175,0)'
ole_1.object.EntryRC(13,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E189,0)'
ole_1.object.EntryRC(21,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E203,0)'
ole_1.object.EntryRC(29,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E217,0)'
ole_1.object.EntryRC(34,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E231,0)'
ole_1.object.EntryRC(39,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E245,0)'
ole_1.object.EntryRC(44,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E259,0)'
ole_1.object.EntryRC(49,5,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F161,0)'
ole_1.object.EntryRC(5,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F175,0)'
ole_1.object.EntryRC(13,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F189,0)'
ole_1.object.EntryRC(21,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F203,0)'
ole_1.object.EntryRC(29,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F217,0)'
ole_1.object.EntryRC(34,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F231,0)'
ole_1.object.EntryRC(39,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F245,0)'
ole_1.object.EntryRC(44,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F259,0)'
ole_1.object.EntryRC(49,6,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G161,0)'
ole_1.object.EntryRC(5,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G175,0)'
ole_1.object.EntryRC(13,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G189,0)'
ole_1.object.EntryRC(21,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G203,0)'
ole_1.object.EntryRC(29,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G217,0)'
ole_1.object.EntryRC(34,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G231,0)'
ole_1.object.EntryRC(39,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G245,0)'
ole_1.object.EntryRC(44,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G259,0)'
ole_1.object.EntryRC(49,7,ls_string)	

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H161,0)'
ole_1.object.EntryRC(5,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H175,0)'
ole_1.object.EntryRC(13,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H189,0)'
ole_1.object.EntryRC(21,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H203,0)'
ole_1.object.EntryRC(29,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H217,0)'
ole_1.object.EntryRC(34,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H231,0)'
ole_1.object.EntryRC(39,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H245,0)'
ole_1.object.EntryRC(44,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H259,0)'
ole_1.object.EntryRC(49,8,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I161,0)'
ole_1.object.EntryRC(5,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I175,0)'
ole_1.object.EntryRC(13,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I189,0)'
ole_1.object.EntryRC(21,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I203,0)'
ole_1.object.EntryRC(29,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I217,0)'
ole_1.object.EntryRC(34,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I231,0)'
ole_1.object.EntryRC(39,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I245,0)'
ole_1.object.EntryRC(44,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I259,0)'
ole_1.object.EntryRC(49,9,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J161,0)'
ole_1.object.EntryRC(5,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J175,0)'
ole_1.object.EntryRC(13,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J189,0)'
ole_1.object.EntryRC(21,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J203,0)'
ole_1.object.EntryRC(29,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J217,0)'
ole_1.object.EntryRC(34,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J231,0)'
ole_1.object.EntryRC(39,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J245,0)'
ole_1.object.EntryRC(44,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J259,0)'
ole_1.object.EntryRC(49,10,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K161,0)'
ole_1.object.EntryRC(5,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K175,0)'
ole_1.object.EntryRC(13,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K189,0)'
ole_1.object.EntryRC(21,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K203,0)'
ole_1.object.EntryRC(29,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K217,0)'
ole_1.object.EntryRC(34,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K231,0)'
ole_1.object.EntryRC(39,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K245,0)'
ole_1.object.EntryRC(44,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K259,0)'
ole_1.object.EntryRC(49,11,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L161,0)'
ole_1.object.EntryRC(5,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L175,0)'
ole_1.object.EntryRC(13,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L189,0)'
ole_1.object.EntryRC(21,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L203,0)'
ole_1.object.EntryRC(29,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L217,0)'
ole_1.object.EntryRC(34,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L231,0)'
ole_1.object.EntryRC(39,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L245,0)'
ole_1.object.EntryRC(44,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L259,0)'
ole_1.object.EntryRC(49,12,ls_string)	

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M161,0)'
ole_1.object.EntryRC(5,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M175,0)'
ole_1.object.EntryRC(13,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M189,0)'
ole_1.object.EntryRC(21,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M203,0)'
ole_1.object.EntryRC(29,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M217,0)'
ole_1.object.EntryRC(34,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M231,0)'
ole_1.object.EntryRC(39,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M245,0)'
ole_1.object.EntryRC(44,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M259,0)'
ole_1.object.EntryRC(49,13,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N161,0)'
ole_1.object.EntryRC(5,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N175,0)'
ole_1.object.EntryRC(13,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N189,0)'
ole_1.object.EntryRC(21,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N203,0)'
ole_1.object.EntryRC(29,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N217,0)'
ole_1.object.EntryRC(34,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N231,0)'
ole_1.object.EntryRC(39,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N245,0)'
ole_1.object.EntryRC(44,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N259,0)'
ole_1.object.EntryRC(49,14,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O161,0)'
ole_1.object.EntryRC(5,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O175,0)'
ole_1.object.EntryRC(13,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O189,0)'
ole_1.object.EntryRC(21,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O203,0)'
ole_1.object.EntryRC(29,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O217,0)'
ole_1.object.EntryRC(34,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O231,0)'
ole_1.object.EntryRC(39,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O245,0)'
ole_1.object.EntryRC(44,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O259,0)'
ole_1.object.EntryRC(49,15,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P161,0)'
ole_1.object.EntryRC(5,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P175,0)'
ole_1.object.EntryRC(13,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P189,0)'
ole_1.object.EntryRC(21,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P203,0)'
ole_1.object.EntryRC(29,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P217,0)'
ole_1.object.EntryRC(34,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P231,0)'
ole_1.object.EntryRC(39,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P245,0)'
ole_1.object.EntryRC(44,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P259,0)'
ole_1.object.EntryRC(49,16,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q157,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q159,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q161,0)'
ole_1.object.EntryRC(5,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q171,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q173,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q175,0)'
ole_1.object.EntryRC(13,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q185,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q187,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q189,0)'
ole_1.object.EntryRC(21,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q199,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q201,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q203,0)'
ole_1.object.EntryRC(29,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q213,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q215,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q217,0)'
ole_1.object.EntryRC(34,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q227,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q229,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q231,0)'
ole_1.object.EntryRC(39,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q241,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q243,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q245,0)'
ole_1.object.EntryRC(44,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q255,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q257,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q259,0)'
ole_1.object.EntryRC(49,17,ls_string)	



end subroutine

public subroutine wf_reset_sales_qty ();String		ls_string	
	
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C160,0)'
ole_1.object.EntryRC(5,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C174,0)'
ole_1.object.EntryRC(13,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C188,0)'
ole_1.object.EntryRC(21,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C202,0)'
ole_1.object.EntryRC(29,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C216,0)'
ole_1.object.EntryRC(34,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C230,0)'
ole_1.object.EntryRC(39,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C244,0)'
ole_1.object.EntryRC(44,3,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',C254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',C256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',C258,0)'
ole_1.object.EntryRC(49,3,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D160,0)'
ole_1.object.EntryRC(5,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D174,0)'
ole_1.object.EntryRC(13,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D188,0)'
ole_1.object.EntryRC(21,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D202,0)'
ole_1.object.EntryRC(29,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D216,0)'
ole_1.object.EntryRC(34,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D230,0)'
ole_1.object.EntryRC(39,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D244,0)'
ole_1.object.EntryRC(44,4,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',D254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',D256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',D258,0)'
ole_1.object.EntryRC(49,4,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E160,0)'
ole_1.object.EntryRC(5,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E174,0)'
ole_1.object.EntryRC(13,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E188,0)'
ole_1.object.EntryRC(21,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E202,0)'
ole_1.object.EntryRC(29,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E216,0)'
ole_1.object.EntryRC(34,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E230,0)'
ole_1.object.EntryRC(39,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E244,0)'
ole_1.object.EntryRC(44,5,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',E254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',E256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',E258,0)'
ole_1.object.EntryRC(49,5,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F160,0)'
ole_1.object.EntryRC(5,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F174,0)'
ole_1.object.EntryRC(13,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F188,0)'
ole_1.object.EntryRC(21,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F202,0)'
ole_1.object.EntryRC(29,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F216,0)'
ole_1.object.EntryRC(34,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F230,0)'
ole_1.object.EntryRC(39,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F244,0)'
ole_1.object.EntryRC(44,6,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',F254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',F256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',F258,0)'
ole_1.object.EntryRC(49,6,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G160,0)'
ole_1.object.EntryRC(5,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G174,0)'
ole_1.object.EntryRC(13,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G188,0)'
ole_1.object.EntryRC(21,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G202,0)'
ole_1.object.EntryRC(29,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G216,0)'
ole_1.object.EntryRC(34,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G230,0)'
ole_1.object.EntryRC(39,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G244,0)'
ole_1.object.EntryRC(44,7,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',G254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',G256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',G258,0)'
ole_1.object.EntryRC(49,7,ls_string)	

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H160,0)'
ole_1.object.EntryRC(5,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H174,0)'
ole_1.object.EntryRC(13,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H188,0)'
ole_1.object.EntryRC(21,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H202,0)'
ole_1.object.EntryRC(29,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H216,0)'
ole_1.object.EntryRC(34,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H230,0)'
ole_1.object.EntryRC(39,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H244,0)'
ole_1.object.EntryRC(44,8,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',H254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',H256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',H258,0)'
ole_1.object.EntryRC(49,8,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I160,0)'
ole_1.object.EntryRC(5,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I174,0)'
ole_1.object.EntryRC(13,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I188,0)'
ole_1.object.EntryRC(21,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I202,0)'
ole_1.object.EntryRC(29,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I216,0)'
ole_1.object.EntryRC(34,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I230,0)'
ole_1.object.EntryRC(39,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I244,0)'
ole_1.object.EntryRC(44,9,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',I254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',I256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',I258,0)'
ole_1.object.EntryRC(49,9,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J160,0)'
ole_1.object.EntryRC(5,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J174,0)'
ole_1.object.EntryRC(13,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J188,0)'
ole_1.object.EntryRC(21,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J202,0)'
ole_1.object.EntryRC(29,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J216,0)'
ole_1.object.EntryRC(34,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J230,0)'
ole_1.object.EntryRC(39,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J244,0)'
ole_1.object.EntryRC(44,10,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',J254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',J256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',J258,0)'
ole_1.object.EntryRC(49,10,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K160,0)'
ole_1.object.EntryRC(5,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K174,0)'
ole_1.object.EntryRC(13,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K188,0)'
ole_1.object.EntryRC(21,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K202,0)'
ole_1.object.EntryRC(29,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K216,0)'
ole_1.object.EntryRC(34,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K230,0)'
ole_1.object.EntryRC(39,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K244,0)'
ole_1.object.EntryRC(44,11,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',K254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',K256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',K258,0)'
ole_1.object.EntryRC(49,11,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L160,0)'
ole_1.object.EntryRC(5,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L174,0)'
ole_1.object.EntryRC(13,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L188,0)'
ole_1.object.EntryRC(21,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L202,0)'
ole_1.object.EntryRC(29,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L216,0)'
ole_1.object.EntryRC(34,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L230,0)'
ole_1.object.EntryRC(39,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L244,0)'
ole_1.object.EntryRC(44,12,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',L254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',L256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',L258,0)'
ole_1.object.EntryRC(49,12,ls_string)	

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M160,0)'
ole_1.object.EntryRC(5,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M174,0)'
ole_1.object.EntryRC(13,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M188,0)'
ole_1.object.EntryRC(21,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M202,0)'
ole_1.object.EntryRC(29,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M216,0)'
ole_1.object.EntryRC(34,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M230,0)'
ole_1.object.EntryRC(39,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M244,0)'
ole_1.object.EntryRC(44,13,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',M254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',M256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',M258,0)'
ole_1.object.EntryRC(49,13,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N160,0)'
ole_1.object.EntryRC(5,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N174,0)'
ole_1.object.EntryRC(13,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N188,0)'
ole_1.object.EntryRC(21,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N202,0)'
ole_1.object.EntryRC(29,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N216,0)'
ole_1.object.EntryRC(34,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N230,0)'
ole_1.object.EntryRC(39,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N244,0)'
ole_1.object.EntryRC(44,14,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',N254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',N256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',N258,0)'
ole_1.object.EntryRC(49,14,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O160,0)'
ole_1.object.EntryRC(5,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O174,0)'
ole_1.object.EntryRC(13,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O188,0)'
ole_1.object.EntryRC(21,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O202,0)'
ole_1.object.EntryRC(29,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O216,0)'
ole_1.object.EntryRC(34,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O230,0)'
ole_1.object.EntryRC(39,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O244,0)'
ole_1.object.EntryRC(44,15,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',O254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',O256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',O258,0)'
ole_1.object.EntryRC(49,15,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P160,0)'
ole_1.object.EntryRC(5,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P174,0)'
ole_1.object.EntryRC(13,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P188,0)'
ole_1.object.EntryRC(21,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P202,0)'
ole_1.object.EntryRC(29,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P216,0)'
ole_1.object.EntryRC(34,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P230,0)'
ole_1.object.EntryRC(39,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P244,0)'
ole_1.object.EntryRC(44,16,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',P254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',P256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',P258,0)'
ole_1.object.EntryRC(49,16,ls_string)

ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q156,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q158,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q160,0)'
ole_1.object.EntryRC(5,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q170,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q172,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q174,0)'
ole_1.object.EntryRC(13,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q184,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q186,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q188,0)'
ole_1.object.EntryRC(21,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q198,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q200,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q202,0)'
ole_1.object.EntryRC(29,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q212,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q214,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q216,0)'
ole_1.object.EntryRC(34,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q226,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q228,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q230,0)'
ole_1.object.EntryRC(39,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q240,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q242,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q244,0)'
ole_1.object.EntryRC(44,17,ls_string)
ls_string = '=IF($A$79=' + '"' + 'Y' + '"' + ',Q254,0)+' + 'IF($A$80=' + '"' + 'Y' + '"' + ',Q256,0)+' + 'IF($A$81=' + '"' + 'Y' + '"' + ',Q258,0)'
ole_1.object.EntryRC(49,17,ls_string)	
end subroutine

public subroutine wf_calc_reserv_weight ();String		ls_string	
	
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C163)'
ole_1.object.EntryRC(6,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C177)'
ole_1.object.EntryRC(14,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C191)'
ole_1.object.EntryRC(22,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C205)'
ole_1.object.EntryRC(30,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C219)'
ole_1.object.EntryRC(35,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C233)'
ole_1.object.EntryRC(40,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C247)'
ole_1.object.EntryRC(45,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C261)'
ole_1.object.EntryRC(50,3,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D163)'
ole_1.object.EntryRC(6,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D177)'
ole_1.object.EntryRC(14,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D191)'
ole_1.object.EntryRC(22,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D205)'
ole_1.object.EntryRC(30,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D219)'
ole_1.object.EntryRC(35,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D233)'
ole_1.object.EntryRC(40,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D247)'
ole_1.object.EntryRC(45,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D261)'
ole_1.object.EntryRC(50,4,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E163)'
ole_1.object.EntryRC(6,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E177)'
ole_1.object.EntryRC(14,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E191)'
ole_1.object.EntryRC(22,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E205)'
ole_1.object.EntryRC(30,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E219)'
ole_1.object.EntryRC(35,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E233)'
ole_1.object.EntryRC(40,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E247)'
ole_1.object.EntryRC(45,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E261)'
ole_1.object.EntryRC(50,5,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F163)'
ole_1.object.EntryRC(6,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F177)'
ole_1.object.EntryRC(14,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F191)'
ole_1.object.EntryRC(22,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F205)'
ole_1.object.EntryRC(30,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F219)'
ole_1.object.EntryRC(35,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F233)'
ole_1.object.EntryRC(40,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F247)'
ole_1.object.EntryRC(45,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F261)'
ole_1.object.EntryRC(50,6,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G163)'
ole_1.object.EntryRC(6,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G177)'
ole_1.object.EntryRC(14,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G191)'
ole_1.object.EntryRC(22,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G205)'
ole_1.object.EntryRC(30,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G219)'
ole_1.object.EntryRC(35,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G233)'
ole_1.object.EntryRC(40,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G247)'
ole_1.object.EntryRC(45,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G261)'
ole_1.object.EntryRC(50,7,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H163)'
ole_1.object.EntryRC(6,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H177)'
ole_1.object.EntryRC(14,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H191)'
ole_1.object.EntryRC(22,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H205)'
ole_1.object.EntryRC(30,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H219)'
ole_1.object.EntryRC(35,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H233)'
ole_1.object.EntryRC(40,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H247)'
ole_1.object.EntryRC(45,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H261)'
ole_1.object.EntryRC(50,8,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I163)'
ole_1.object.EntryRC(6,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I177)'
ole_1.object.EntryRC(14,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I191)'
ole_1.object.EntryRC(22,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I205)'
ole_1.object.EntryRC(30,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I219)'
ole_1.object.EntryRC(35,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I233)'
ole_1.object.EntryRC(40,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I247)'
ole_1.object.EntryRC(45,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I261)'
ole_1.object.EntryRC(50,9,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J163)'
ole_1.object.EntryRC(6,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J177)'
ole_1.object.EntryRC(14,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J191)'
ole_1.object.EntryRC(22,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J205)'
ole_1.object.EntryRC(30,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J219)'
ole_1.object.EntryRC(35,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J233)'
ole_1.object.EntryRC(40,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J247)'
ole_1.object.EntryRC(45,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J261)'
ole_1.object.EntryRC(50,10,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K163)'
ole_1.object.EntryRC(6,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K177)'
ole_1.object.EntryRC(14,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K191)'
ole_1.object.EntryRC(22,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K205)'
ole_1.object.EntryRC(30,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K219)'
ole_1.object.EntryRC(35,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K233)'
ole_1.object.EntryRC(40,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K247)'
ole_1.object.EntryRC(45,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K261)'
ole_1.object.EntryRC(50,11,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L163)'
ole_1.object.EntryRC(6,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L177)'
ole_1.object.EntryRC(14,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L191)'
ole_1.object.EntryRC(22,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L205)'
ole_1.object.EntryRC(30,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L219)'
ole_1.object.EntryRC(35,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L233)'
ole_1.object.EntryRC(40,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L247)'
ole_1.object.EntryRC(45,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L261)'
ole_1.object.EntryRC(50,12,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M163)'
ole_1.object.EntryRC(6,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M177)'
ole_1.object.EntryRC(14,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M191)'
ole_1.object.EntryRC(22,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M205)'
ole_1.object.EntryRC(30,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M219)'
ole_1.object.EntryRC(35,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M233)'
ole_1.object.EntryRC(40,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M247)'
ole_1.object.EntryRC(45,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M261)'
ole_1.object.EntryRC(50,13,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N163)'
ole_1.object.EntryRC(6,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N177)'
ole_1.object.EntryRC(14,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N191)'
ole_1.object.EntryRC(22,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N205)'
ole_1.object.EntryRC(30,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N219)'
ole_1.object.EntryRC(35,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N233)'
ole_1.object.EntryRC(40,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N247)'
ole_1.object.EntryRC(45,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N261)'
ole_1.object.EntryRC(50,14,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O163)'
ole_1.object.EntryRC(6,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O177)'
ole_1.object.EntryRC(14,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O191)'
ole_1.object.EntryRC(22,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O205)'
ole_1.object.EntryRC(30,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O219)'
ole_1.object.EntryRC(35,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O233)'
ole_1.object.EntryRC(40,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O247)'
ole_1.object.EntryRC(45,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O261)'
ole_1.object.EntryRC(50,15,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P163)'
ole_1.object.EntryRC(6,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P177)'
ole_1.object.EntryRC(14,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P191)'
ole_1.object.EntryRC(22,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P205)'
ole_1.object.EntryRC(30,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P219)'
ole_1.object.EntryRC(35,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P233)'
ole_1.object.EntryRC(40,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P247)'
ole_1.object.EntryRC(45,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P261)'
ole_1.object.EntryRC(50,16,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q163)'
ole_1.object.EntryRC(6,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q177)'
ole_1.object.EntryRC(14,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q191)'
ole_1.object.EntryRC(22,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q205)'
ole_1.object.EntryRC(30,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q219)'
ole_1.object.EntryRC(35,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q233)'
ole_1.object.EntryRC(40,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q247)'
ole_1.object.EntryRC(45,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q261)'
ole_1.object.EntryRC(50,17,ls_string)






end subroutine

public subroutine wf_reset_reserv_qty ();String		ls_string	
	
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C162)'
ole_1.object.EntryRC(6,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C176)'
ole_1.object.EntryRC(14,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C190)'
ole_1.object.EntryRC(22,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C204)'
ole_1.object.EntryRC(30,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C218)'
ole_1.object.EntryRC(35,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C232)'
ole_1.object.EntryRC(40,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C246)'
ole_1.object.EntryRC(45,3,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',C260)'
ole_1.object.EntryRC(50,3,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D162)'
ole_1.object.EntryRC(6,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D176)'
ole_1.object.EntryRC(14,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D190)'
ole_1.object.EntryRC(22,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D204)'
ole_1.object.EntryRC(30,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D218)'
ole_1.object.EntryRC(35,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D232)'
ole_1.object.EntryRC(40,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D246)'
ole_1.object.EntryRC(45,4,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',D260)'
ole_1.object.EntryRC(50,4,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E162)'
ole_1.object.EntryRC(6,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E176)'
ole_1.object.EntryRC(14,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E190)'
ole_1.object.EntryRC(22,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E204)'
ole_1.object.EntryRC(30,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E218)'
ole_1.object.EntryRC(35,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E232)'
ole_1.object.EntryRC(40,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E246)'
ole_1.object.EntryRC(45,5,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',E260)'
ole_1.object.EntryRC(50,5,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F162)'
ole_1.object.EntryRC(6,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F176)'
ole_1.object.EntryRC(14,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F190)'
ole_1.object.EntryRC(22,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F204)'
ole_1.object.EntryRC(30,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F218)'
ole_1.object.EntryRC(35,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F232)'
ole_1.object.EntryRC(40,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F246)'
ole_1.object.EntryRC(45,6,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',F260)'
ole_1.object.EntryRC(50,6,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G162)'
ole_1.object.EntryRC(6,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G176)'
ole_1.object.EntryRC(14,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G190)'
ole_1.object.EntryRC(22,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G204)'
ole_1.object.EntryRC(30,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G218)'
ole_1.object.EntryRC(35,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G232)'
ole_1.object.EntryRC(40,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G246)'
ole_1.object.EntryRC(45,7,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',G260)'
ole_1.object.EntryRC(50,7,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H162)'
ole_1.object.EntryRC(6,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H176)'
ole_1.object.EntryRC(14,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H190)'
ole_1.object.EntryRC(22,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H204)'
ole_1.object.EntryRC(30,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H218)'
ole_1.object.EntryRC(35,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H232)'
ole_1.object.EntryRC(40,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H246)'
ole_1.object.EntryRC(45,8,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',H260)'
ole_1.object.EntryRC(50,8,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I162)'
ole_1.object.EntryRC(6,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I176)'
ole_1.object.EntryRC(14,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I190)'
ole_1.object.EntryRC(22,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I204)'
ole_1.object.EntryRC(30,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I218)'
ole_1.object.EntryRC(35,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I232)'
ole_1.object.EntryRC(40,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I246)'
ole_1.object.EntryRC(45,9,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',I260)'
ole_1.object.EntryRC(50,9,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J162)'
ole_1.object.EntryRC(6,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J176)'
ole_1.object.EntryRC(14,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J190)'
ole_1.object.EntryRC(22,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J204)'
ole_1.object.EntryRC(30,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J218)'
ole_1.object.EntryRC(35,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J232)'
ole_1.object.EntryRC(40,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J246)'
ole_1.object.EntryRC(45,10,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',J260)'
ole_1.object.EntryRC(50,10,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K162)'
ole_1.object.EntryRC(6,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K176)'
ole_1.object.EntryRC(14,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K190)'
ole_1.object.EntryRC(22,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K204)'
ole_1.object.EntryRC(30,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K218)'
ole_1.object.EntryRC(35,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K232)'
ole_1.object.EntryRC(40,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K246)'
ole_1.object.EntryRC(45,11,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',K260)'
ole_1.object.EntryRC(50,11,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L162)'
ole_1.object.EntryRC(6,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L176)'
ole_1.object.EntryRC(14,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L190)'
ole_1.object.EntryRC(22,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L204)'
ole_1.object.EntryRC(30,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L218)'
ole_1.object.EntryRC(35,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L232)'
ole_1.object.EntryRC(40,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L246)'
ole_1.object.EntryRC(45,12,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',L260)'
ole_1.object.EntryRC(50,12,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M162)'
ole_1.object.EntryRC(6,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M176)'
ole_1.object.EntryRC(14,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M190)'
ole_1.object.EntryRC(22,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M204)'
ole_1.object.EntryRC(30,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M218)'
ole_1.object.EntryRC(35,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M232)'
ole_1.object.EntryRC(40,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M246)'
ole_1.object.EntryRC(45,13,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',M260)'
ole_1.object.EntryRC(50,13,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N162)'
ole_1.object.EntryRC(6,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N176)'
ole_1.object.EntryRC(14,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N190)'
ole_1.object.EntryRC(22,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N204)'
ole_1.object.EntryRC(30,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N218)'
ole_1.object.EntryRC(35,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N232)'
ole_1.object.EntryRC(40,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N246)'
ole_1.object.EntryRC(45,14,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',N260)'
ole_1.object.EntryRC(50,14,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O162)'
ole_1.object.EntryRC(6,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O176)'
ole_1.object.EntryRC(14,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O190)'
ole_1.object.EntryRC(22,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O204)'
ole_1.object.EntryRC(30,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O218)'
ole_1.object.EntryRC(35,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O232)'
ole_1.object.EntryRC(40,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O246)'
ole_1.object.EntryRC(45,15,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',O260)'
ole_1.object.EntryRC(50,15,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P162)'
ole_1.object.EntryRC(6,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P176)'
ole_1.object.EntryRC(14,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P190)'
ole_1.object.EntryRC(22,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P204)'
ole_1.object.EntryRC(30,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P218)'
ole_1.object.EntryRC(35,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P232)'
ole_1.object.EntryRC(40,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P246)'
ole_1.object.EntryRC(45,16,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',P260)'
ole_1.object.EntryRC(50,16,ls_string)

ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q162)'
ole_1.object.EntryRC(6,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q176)'
ole_1.object.EntryRC(14,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q190)'
ole_1.object.EntryRC(22,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q204)'
ole_1.object.EntryRC(30,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q218)'
ole_1.object.EntryRC(35,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q232)'
ole_1.object.EntryRC(40,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q246)'
ole_1.object.EntryRC(45,17,ls_string)
ls_string = '=IF($A$82=' + '"' + 'Y' + '"' + ',Q260)'
ole_1.object.EntryRC(50,17,ls_string)






end subroutine

event resize;call super::resize; integer li_x		= 100
 integer li_y		= 600

if il_BorderPaddingWidth > li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

//ole_1.width = width - (90 + li_x)
ole_1.width = width - (li_x)

// ibdkdld changed height adjustment 12/12/02 from 125 to 170
//ole_1.height = height - dw_weekly_summary.Height - (170 + li_y)
ole_1.height = height - dw_weekly_summary.Height - (li_y) 

dw_weekly_summary.Y = This.Height - dw_weekly_summary.Height - 110
//dw_weekly_summary.Y = This.Height - dw_weekly_summary.Height - il_BorderPaddingHeight
st_boxessold.Y = dw_weekly_summary.Y



end event

event close;call super::close;If IsValid(iu_pas203) Then
	Destroy(iu_pas203)
End If
If IsValid (iu_ws_pas_share) Then
	Destroy(iu_ws_pas_share)
End If
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

//For orp menus.
iw_frame.im_menu.mf_enable('m_complete')
iw_frame.im_menu.mf_enable('m_generatesales')

If Not ib_m_print_status Then iw_frame.im_menu.mf_Disable('m_print')

end event

event ue_postopen;call super::ue_postopen;Int	li_fileType

iu_pas203 = Create u_pas203
If Message.ReturnValue = -1 Then Close(This)

iu_ws_pas_share = Create u_ws_pas_share
If Message.ReturnValue =-1 Then Close(This)

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = 'Invt/Sales'
istr_error_info.se_user_id = sqlca.userid

ole_1.object.Read(iw_frame.is_workingdir + "pinvvsls.vts", ref li_fileType)

//Uncomment the 2nd set of lines lines to be able to open spreadsheet, Use a double right-click on the spreadsheet
ole_1.object.ShowTabs = 0
ole_1.Object.ShowRowHeading = 0
ole_1.Object.ShowHScrollBar = 1
ole_1.Object.ShowVScrollBar = 1
ole_1.Object.AllowDesigner = False
ole_1.Object.AllowInCellEditing = False
ole_1.Object.AllowSelections = False
ole_1.Object.AllowDelete = False
ole_1.Object.AllowEditHeaders = False
ole_1.Object.AllowFormulas = False

ole_1.object.ShowTabs = 1
ole_1.Object.ShowRowHeading = 1
ole_1.Object.ShowHScrollBar = 1
ole_1.Object.ShowVScrollBar = 1
ole_1.Object.AllowDesigner = True
ole_1.Object.AllowInCellEditing = True
ole_1.Object.AllowSelections = True
ole_1.Object.AllowDelete = True
ole_1.Object.AllowEditHeaders = True
ole_1.Object.AllowFormulas = True


ii_column_width = ole_1.Object.ColWidth(3)
ole_1.Object.ColText(1,"")
ole_1.Object.ColText(2,"")
ole_1.Object.ColText(18,"Total")


This.PostEvent("ue_query")
end event

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
end on

on w_box_inventory_new.create
int iCurrent
call super::create
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.rb_eight_days=create rb_eight_days
this.rb_seven_days=create rb_seven_days
this.rb_ready_to_ship_date=create rb_ready_to_ship_date
this.rb_total_on_hand=create rb_total_on_hand
this.rb_weight=create rb_weight
this.rb_quantity=create rb_quantity
this.st_start_date=create st_start_date
this.st_1=create st_1
this.st_divisions=create st_divisions
this.sle_division_list=create sle_division_list
this.st_groups=create st_groups
this.dw_uom_code=create dw_uom_code
this.cbx_combined=create cbx_combined
this.cbx_uncombined=create cbx_uncombined
this.cbx_gpo=create cbx_gpo
this.cbx_reservation=create cbx_reservation
this.dw_weekly_summary=create dw_weekly_summary
this.st_boxessold=create st_boxessold
this.st_2=create st_2
this.sle_last_update=create sle_last_update
this.ole_1=create ole_1
this.gb_display_uom=create gb_display_uom
this.gb_inv_position=create gb_inv_position
this.gb_number_of_days=create gb_number_of_days
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_8
this.Control[iCurrent+2]=this.st_7
this.Control[iCurrent+3]=this.st_6
this.Control[iCurrent+4]=this.st_5
this.Control[iCurrent+5]=this.st_4
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.rb_eight_days
this.Control[iCurrent+8]=this.rb_seven_days
this.Control[iCurrent+9]=this.rb_ready_to_ship_date
this.Control[iCurrent+10]=this.rb_total_on_hand
this.Control[iCurrent+11]=this.rb_weight
this.Control[iCurrent+12]=this.rb_quantity
this.Control[iCurrent+13]=this.st_start_date
this.Control[iCurrent+14]=this.st_1
this.Control[iCurrent+15]=this.st_divisions
this.Control[iCurrent+16]=this.sle_division_list
this.Control[iCurrent+17]=this.st_groups
this.Control[iCurrent+18]=this.dw_uom_code
this.Control[iCurrent+19]=this.cbx_combined
this.Control[iCurrent+20]=this.cbx_uncombined
this.Control[iCurrent+21]=this.cbx_gpo
this.Control[iCurrent+22]=this.cbx_reservation
this.Control[iCurrent+23]=this.dw_weekly_summary
this.Control[iCurrent+24]=this.st_boxessold
this.Control[iCurrent+25]=this.st_2
this.Control[iCurrent+26]=this.sle_last_update
this.Control[iCurrent+27]=this.ole_1
this.Control[iCurrent+28]=this.gb_display_uom
this.Control[iCurrent+29]=this.gb_inv_position
this.Control[iCurrent+30]=this.gb_number_of_days
end on

on w_box_inventory_new.destroy
call super::destroy
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.rb_eight_days)
destroy(this.rb_seven_days)
destroy(this.rb_ready_to_ship_date)
destroy(this.rb_total_on_hand)
destroy(this.rb_weight)
destroy(this.rb_quantity)
destroy(this.st_start_date)
destroy(this.st_1)
destroy(this.st_divisions)
destroy(this.sle_division_list)
destroy(this.st_groups)
destroy(this.dw_uom_code)
destroy(this.cbx_combined)
destroy(this.cbx_uncombined)
destroy(this.cbx_gpo)
destroy(this.cbx_reservation)
destroy(this.dw_weekly_summary)
destroy(this.st_boxessold)
destroy(this.st_2)
destroy(this.sle_last_update)
destroy(this.ole_1)
destroy(this.gb_display_uom)
destroy(this.gb_inv_position)
destroy(this.gb_number_of_days)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

//For orp menus.
iw_frame.im_menu.mf_Disable('m_complete')
iw_frame.im_menu.mf_Disable('m_generatesales')

ib_m_print_status = iw_frame.im_menu.mf_Enabled('m_print')
iw_frame.im_menu.mf_Enable('m_print')


end event

event ue_fileprint;call super::ue_fileprint;String	ls_header, &
			ls_footer
			
Integer li_length

// ibdkdld change header data 12/12/02
ls_header = "&RLast Update: " + trim(sle_last_update.Text) + &
				"&C" + trim(This.Title) + "~r~n" + &
				"Divs:" + trim(sle_division_list.Text) + "~r~n" + &
				"UOM Code - " + trim(dw_uom_code.uf_Get_description()) + "~r~n" + &
				trim(st_groups.text) + "~r~n"  
//				"UOM Code - " + dw_uom_code.uf_Get_description() + "   " + &
//				left(st_groups.text, 15) + "~r~n" + & 
//				mid(st_groups.text, 16) + "~r~n"  

If cbx_combined.Checked Then 		ls_header += "Comb."
If cbx_uncombined.Checked Then 	ls_header += " Uncomb."
If cbx_gpo.Checked Then 			ls_header += " GPO"
If cbx_reservation.Checked Then 	ls_header += " Reserv." + "~r~n"  

If rb_quantity.Checked Then
	ls_header += "Disp UOM: Qty"  
Else
	ls_header += "Disp UOM: Wt" 
ENd If

If rb_total_on_hand.Checked Then
	ls_header += "  Inv Pos: Total on Hand" + "~r~n"  
Else
	ls_header += "  Inv Pos: Ready to Ship Date" + "~r~n"  
ENd If

li_length = len(ls_header)

if rb_quantity.Checked Then

	ls_footer = "&l" + String(Today(), "mmm dd") + "-" + &
					String(RelativeDate(Today(), 6), "mmm dd") + "     " + &
					String(dw_weekly_summary.GetItemNumber(1, "week1")) + &
					"~r~n" + String(RelativeDate(Today(), 23 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 29 - DayNumber(Today()) ), "mmm dd")  + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week4")) + &
					"&cBoxes Sold~r~n" + &
					String(RelativeDate(Today(), 9 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 15 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week2")) + &
					"~r~n" + String(RelativeDate(Today(), 30 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 36 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week5")) + &
					"&r" + String(RelativeDate(Today(), 16 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 22 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week3")) + &
					"~r~n" + String(RelativeDate(Today(), 37 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 43 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week6"))
else	
	ls_footer = "&l" + String(Today(), "mmm dd") + "-" + &
					String(RelativeDate(Today(), 6), "mmm dd") + "     " + &
					String(dw_weekly_summary.GetItemNumber(1, "week1")) + &
					"~r~n" + String(RelativeDate(Today(), 23 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 29 - DayNumber(Today()) ), "mmm dd")  + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week4")) + &
					"&cWeight Sold~r~n" + &
					String(RelativeDate(Today(), 9 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 15 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week2")) + &
					"~r~n" + String(RelativeDate(Today(), 30 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 36 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week5")) + &
					"&r" + String(RelativeDate(Today(), 16 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 22 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week3")) + &
					"~r~n" + String(RelativeDate(Today(), 37 - DayNumber(Today()) ), "mmm dd") + &
					"-" + String(RelativeDate(Today(), 43 - DayNumber(Today()) ), "mmm dd") + &
					"     " + String(dw_weekly_summary.GetItemNumber(1, "week6"))	
end if

ole_1.Object.PrintTopMargin = 1.50
ole_1.Object.PrintHeader = ls_header
ole_1.Object.PrintFooter = ls_footer
ole_1.Object.PrintColHeading = True

If rb_eight_days.Checked = True Then
	ole_1.Object.PrintArea = "$A$3:$W$51"
Else
	ole_1.Object.PrintArea = "$A$3:$W$46"
End If

ole_1.Object.FilePrint(False)


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case "division_list"
		sle_division_list.Text = as_value
	Case "UOM Code"
		dw_uom_code.uf_set_uom_code(as_value)
// add 3 ibdkdld 12/09/02
	Case 'groupid'
		is_groupID = as_value
	Case 'group_system'
		st_groups.text = "SYSTEM - " + as_value
	Case 'group_description'
		st_groups.text += "   GROUP - " + as_value
	Case 'start_date'
		st_start_date.text = String(Date(as_value), "mm/dd/yyyy")		
	Case 'display_uom'
		If as_value = 'Q' then
			rb_quantity.Checked = True
		Else
			rb_weight.Checked = True
		End If
	Case 'inv_position'
		If as_value = 'T' then
			rb_total_on_hand.Checked = True
		Else
			rb_ready_to_ship_date.Checked = True
		End If
	Case 'number_of_days'
		If as_value = '7' then
			rb_seven_days.Checked = True
		Else
			rb_eight_days.Checked = True
		End If		
End Choose
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case	"number_of_days"
		If rb_eight_days.Checked Then
			message.StringParm = "8"
		Else
			If rb_seven_days.Checked Then
				message.StringParm = "7"
			Else
				message.Stringparm = ''
			End If
		End If
	Case	"display_uom"
		If rb_weight.Checked Then
			message.StringParm = "W"
		Else
			If rb_quantity.Checked Then
				message.StringParm = "Q"
			Else
				message.StringParm = ''
			End If
		End If
	Case	"inv_position"
		If rb_total_on_hand.Checked Then
			message.StringParm = "T"
		Else
			If rb_ready_to_ship_date.Checked Then
				message.StringParm = "S"
			Else
				message.StringParm = ''
			End If
		End If		
	Case	"start_date"
		If st_start_date.text = '' Then
			message.StringParm = String(Today(),"mm/dd/yyyy")
		Else
			message.StringParm = st_start_date.Text
		End If		
	Case	"division_list"
		If sle_division_list.Text = '' Then
			message.StringParm = ''
		Else
			message.StringParm = sle_division_list.Text + ',' // pass extra comma at end
		End if	
	
End Choose				
end event

type st_8 from statictext within w_box_inventory_new
integer x = 1673
integer y = 376
integer width = 183
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "8"
boolean focusrectangle = false
end type

type st_7 from statictext within w_box_inventory_new
integer x = 1678
integer y = 312
integer width = 142
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "7"
boolean focusrectangle = false
end type

type st_6 from statictext within w_box_inventory_new
integer x = 919
integer y = 376
integer width = 480
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ready to Ship Date"
boolean focusrectangle = false
end type

type st_5 from statictext within w_box_inventory_new
integer x = 914
integer y = 308
integer width = 343
integer height = 68
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Total On Hand"
boolean focusrectangle = false
end type

type st_4 from statictext within w_box_inventory_new
integer x = 197
integer y = 368
integer width = 471
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Weight in Thousands"
boolean focusrectangle = false
end type

type st_3 from statictext within w_box_inventory_new
integer x = 197
integer y = 312
integer width = 343
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Quantity"
boolean focusrectangle = false
end type

type rb_eight_days from radiobutton within w_box_inventory_new
integer x = 1600
integer y = 368
integer width = 96
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_seven_days from radiobutton within w_box_inventory_new
integer x = 1600
integer y = 304
integer width = 119
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_ready_to_ship_date from radiobutton within w_box_inventory_new
integer x = 841
integer y = 368
integer width = 105
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_total_on_hand from radiobutton within w_box_inventory_new
integer x = 841
integer y = 304
integer width = 101
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_weight from radiobutton within w_box_inventory_new
integer x = 119
integer y = 368
integer width = 96
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type rb_quantity from radiobutton within w_box_inventory_new
integer x = 119
integer y = 304
integer width = 110
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
end type

type st_start_date from statictext within w_box_inventory_new
integer x = 2391
integer y = 252
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_1 from statictext within w_box_inventory_new
integer x = 2030
integer y = 252
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Start Date:"
boolean focusrectangle = false
end type

type st_divisions from statictext within w_box_inventory_new
integer x = 37
integer y = 12
integer width = 238
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Divisions:"
boolean focusrectangle = false
end type

type sle_division_list from singlelineedit within w_box_inventory_new
integer x = 297
integer y = 4
integer width = 1367
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
borderstyle borderstyle = stylelowered!
end type

type st_groups from statictext within w_box_inventory_new
integer x = 23
integer y = 176
integer width = 2130
integer height = 68
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Courier New"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_uom_code from u_uom_code within w_box_inventory_new
integer y = 88
integer width = 763
integer height = 88
integer taborder = 50
end type

event constructor;call super::constructor;This.uf_enable(false)
end event

type cbx_combined from checkbox within w_box_inventory_new
integer x = 1733
integer y = 8
integer width = 347
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Combined"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(80, 1, 'Y')
	dw_weekly_summary.SetItem(1, "comb_cbx", 'Y')
Else
	ole_1.Object.TextRC(80, 1, 'N')
	dw_weekly_summary.SetItem(1, "comb_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type cbx_uncombined from checkbox within w_box_inventory_new
integer x = 2139
integer y = 8
integer width = 507
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Uncombined"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(79, 1, 'Y')
	dw_weekly_summary.SetItem(1, "uncomb_cbx", 'Y')
Else
	ole_1.Object.TextRC(79, 1, 'N')
	dw_weekly_summary.SetItem(1, "uncomb_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type cbx_gpo from checkbox within w_box_inventory_new
integer x = 1733
integer y = 92
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "GPO"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(81, 1, 'Y')
	dw_weekly_summary.SetItem(1, "gpo_cbx", 'Y')
Else
	ole_1.Object.TextRC(81, 1, 'N')
	dw_weekly_summary.SetItem(1, "gpo_cbx", 'N')
End if
ole_1.Object.Recalc()
end event

type cbx_reservation from checkbox within w_box_inventory_new
integer x = 2139
integer y = 92
integer width = 475
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Reservation"
boolean checked = true
end type

event clicked;If This.Checked Then
	ole_1.Object.TextRC(82, 1, 'Y')
	dw_weekly_summary.SetItem(1, "reserv_cbx", 'Y')
Else
	ole_1.Object.TextRC(82, 1, 'N')
	dw_weekly_summary.SetItem(1, "reserv_cbx", 'N')
End if
ole_1.Object.Recalc()


end event

type dw_weekly_summary from u_netwise_dw within w_box_inventory_new
integer x = 320
integer y = 1424
integer width = 2437
integer height = 188
integer taborder = 20
string dataobject = "d_invt_vs_sales_summary"
boolean border = false
end type

on constructor;call u_netwise_dw::constructor;ib_updateable = False

This.InsertRow(0)
end on

type st_boxessold from statictext within w_box_inventory_new
integer y = 1420
integer width = 357
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Boxes Sold :"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_box_inventory_new
integer x = 750
integer y = 112
integer width = 315
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Last Update"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_last_update from singlelineedit within w_box_inventory_new
integer x = 1083
integer y = 100
integer width = 581
integer height = 72
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string pointer = "Arrow!"
long textcolor = 33554432
long backcolor = 67108864
boolean autohscroll = false
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type ole_1 from olecustomcontrol within w_box_inventory_new
event click ( long nrow,  long ncol )
event dblclick ( long nrow,  long ncol )
event canceledit ( )
event selchange ( )
event startedit ( ref string editstring,  ref integer ab_cancel )
event endedit ( ref string editstring,  ref integer ab_cancel )
event startrecalc ( )
event endrecalc ( )
event topleftchanged ( )
event objclick ( ref string objname,  long objid )
event objdblclick ( ref string objname,  long objid )
event rclick ( long nrow,  long ncol )
event rdblclick ( long nrow,  long ncol )
event objvaluechanged ( ref string objname,  long objid )
event modified ( )
event mousedown ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mouseup ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mousemove ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event objgotfocus ( ref string objname,  long objid )
event objlostfocus ( ref string objname,  long objid )
event validationfailed ( ref string pentry,  long nsheet,  long nrow,  long ncol,  ref string pshowmessage,  ref integer paction )
event keypress ( ref integer keyascii )
event keydown ( ref integer keycode,  integer shift )
event keyup ( ref integer keycode,  integer shift )
integer x = 27
integer y = 460
integer width = 2825
integer height = 916
integer taborder = 40
boolean bringtotop = true
long backcolor = 67108864
string binarykey = "w_box_inventory_new.win"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
end type

type gb_display_uom from groupbox within w_box_inventory_new
integer x = 37
integer y = 244
integer width = 654
integer height = 204
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Display UOM"
end type

type gb_inv_position from groupbox within w_box_inventory_new
integer x = 731
integer y = 252
integer width = 690
integer height = 196
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Inv Position"
end type

type gb_number_of_days from groupbox within w_box_inventory_new
integer x = 1481
integer y = 248
integer width = 416
integer height = 200
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Number of Days"
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Cw_box_inventory_new.bin 
2E00001800e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000007fffffffe00000004000000050000000600000008fffffffe000000090000000afffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000005f8f4c1001d3f9e50000000300000c800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000540000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000020000067900000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004042badc511ce5e58415210b601004853000000005f8f4c1001d3f9e55f8f4c1001d3f9e500000000000000000000000000000001fffffffe000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f000000100000001100000012000000130000001400000015000000160000001700000018000000190000001a0000001bfffffffe0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002f0000003000000031fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
28ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f004300790070006900720068006700200074006300280020002900390031003500390056002000730069006100750020006c006f00430070006d006e006f006e0065007300740020002c006e0049002e006300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fffe00020106042badc511ce5e58415210b60100485300000001fb8f0821101b01640008ed8413c72e2b000000300000064900000008000001000000004800000101000000500000010200000058000001030000006000000104000000680000010500000070000001060000007c00000000000005c000000003000100000000000300003fdf00000003000017ab00000003000000600000000200000001000000080000000100000000000000410000053b00090000ee000505000000040014ef000000000000000000ffffff00ffffffff3dfff3ff00000012000000000000380001000000310258000000c814907fff00000000010500000061697241c814316cff0000000002bc7f0000000072410500316c61690200c814907fff00000000010500000061697241c814316cff0002000002bc7f0000000072410500316c61690000c814907fff00000000010500000061697241051a1e6c24221700232c2322295f302322285c3b2c2322245c302323061f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c201e295c221d00072c2322242e302323295f303022285c3b2c2322242e302323295c30300008251e2224222223232c2330302e305b3b295f5d6465522422285c232c2322302e30231e295c3032002a352422285f23202a223023232c5f3b295f22242228285c202a23232c233b295c302422285f22202a22295f222d40285f3b2c1e295f5f29002923202a283023232c5f3b295f5c202a28232c2328295c30232a285f3b222d22205f3b295f295f4028002c3d1e22285f3a202a222423232c2330302e305f3b295f22242228285c202a23232c2330302e305f3b295c222422282d22202a5f3f3f22285f3b291e295f4031002b34202a285f23232c2330302e305f3b295f5c202a28232c2328302e30233b295c30202a285f3f222d223b295f3f5f40285f321f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c251e295c222200332c2322242e302323295f303065525b3b285c5d64232224223023232c5c30302e0005ed2900000000000003ec0014e000f5000000c00020ff0000002000000000e00000000000011420fff5000020c0c400000000000000000114e000f5000000c0c420ff0000002000000000e00000000000021420fff5000020c0c400000000000000000214e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e000000000000014200001000020c00000000000000000000514e000f5003300c0c820ff0000002000000000e00000003200051420fff5000020c0c800000000000000000514e000f5000c00c0c820ff0000002000000000e00000000a00051420fff5000020c0c800000000000000000514e000f5000d00c0c820ff0000002000000000e00000000000041412fff0000020c04800000000000000000009850068530600317465650500090a010d001000640c0010000011d2f1a9fc3f50624d2a00015f012b000000012500018c00ff81000100031404c11541260261500708262065670000835026000084000000003fe8000000000027e80000000000283f0000000000293ff000000000a13ff0006400012201000100060001000000000000000000e00000000000003fe00000000000013f000000000000000018f70000ccff009200ffffff00c0c0c03fff00ff0000000000000000000010f200000000000000000000000015f6ffff150015004000ff00030f1d060000000000010000000000000a3e0000000002360000000064a0000099006400000a092600000008000000000000000100010600000009006c6966006d616e6501030065000c0000735f00006b636f74706f727001040073000c00006f620000726564726c7974730101006500090000655f00006e65747802007874090000015f000000657478650079746e00000105000000086e70706100656d6100000100000000097265765f6e6f697300000000000000000001000000003fdf000017ab00000060000100010101010101010101010101010101010100053b000900000000050500000004ee14ef00000000000000000000ffff0000fffffffffff3ffff0000123d0000000000380000000000000258000100c814317fff000000000190000000006972410514316c61000000c802bc7fff00000000410500006c61697200c814317fff0002
2800000190000000006972410514316c61000200c802bc7fff00000000410500006c61697200c814317fff00000000019000000000697241051a1e6c61221700052c2322245f302323285c3b29232224223023232c1f1e295c221c00062c2322245f302323525b3b295c5d6465006f00430074006e006e00650073007400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000001c00000568000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002224222823232c231e295c301d000720232224223023232c5f30302e285c3b29232224223023232c5c30302e08251e2924222200232c2322302e30233b295f306465525b22285c5d2c2322242e302323295c3030002a351e22285f32202a222423232c233b295f302422285f5c202a22232c2328295c302322285f3b202a22245f222d22285f3b291e295f402900292c202a285f23232c233b295f30202a285f2c23285c5c302323285f3b292d22202a3b295f225f40285f2c3d1e29285f3a002a222422232c2320302e30233b295f302422285f5c202a22232c2328302e30233b295c302422285f22202a223f3f222d5f3b295f295f4028002b341e2a285f31232c2320302e30233b295f30202a285f2c23285c2e302323295c30302a285f3b222d2220295f3f3f40285f3b1f1e295f221c00322c2322245f302323525b3b295c5d64652224222823232c231e295c3022003325232224223023232c5f30302e525b3b295c5d64652224222823232c2330302e3005ed295c000000000003ec0014e00000000000000020fff5000020c00000000000000000000114e0fff5000020c0c420000000000000000014e0000000000001c420fff5000020c00000000000000000000214e0fff5000020c0c420000000000000000014e0000000000002c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e00001000020c00020000000000000000014e0000000330005c820fff5000020c00000000000000000000514e0fff5003220c0c820000000000000000014e00000000c0005c820fff5000020c00000000000000000000514e0fff5000a20c0c820000000000000000014e00000000d0005c820fff5000020c00000000000000000000414e0fff0000020c04812000000000000000009850000530600007465656800090a310d001005640c000100001100f1a9fc1050624dd200015f3f2b00002a012500018c00ff00000100011404c181412602035007081520656761008350260000840000000026e80000000000273f0000000000283fe800000000293ff000000000003ff00000000122a1000100640001000100000006000000000000000000003fe00000000000013fe00000000000000000f7000000ff009218ffffffccc0c0c000ff00ff000000003f000000000010f200000000000000000000000000f6ffff000015001500ff00150f1d06400000000301000000000000003e0000000002360a00000000a0000000006400640a09269901ff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Cw_box_inventory_new.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
