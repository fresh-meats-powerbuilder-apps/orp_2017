﻿$PBExportHeader$w_age_avail.srw
$PBExportComments$Age Availabilty Window
forward
global type w_age_avail from w_netwise_sheet
end type
type dw_date_range from u_base_dw_ext within w_age_avail
end type
type dw_product_status from u_product_status within w_age_avail
end type
type dw_product from u_fab_product_code within w_age_avail
end type
type dw_loc_group_info from u_location_group_info within w_age_avail
end type
type dw_plant from u_plant within w_age_avail
end type
type dw_age_avail_detail from datawindow within w_age_avail
end type
type dw_product_state from u_product_state within w_age_avail
end type
type dw_division from u_division within w_age_avail
end type
type dw_prod_group_info_1 from u_owner_group_info within w_age_avail
end type
end forward

global type w_age_avail from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 4206
integer height = 2060
string title = "Age Availability Summary"
long backcolor = 67108864
long il_borderpaddingwidth = 100
dw_date_range dw_date_range
dw_product_status dw_product_status
dw_product dw_product
dw_loc_group_info dw_loc_group_info
dw_plant dw_plant
dw_age_avail_detail dw_age_avail_detail
dw_product_state dw_product_state
dw_division dw_division
dw_prod_group_info_1 dw_prod_group_info_1
end type
global w_age_avail w_age_avail

type variables
u_pas201		iu_pas201
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Boolean		ib_inquire, &
		ib_reinquire

Integer		ii_max_page_number

string		is_inquire_window_name, &
		is_product_group_id, &
		is_product_group_desc, &
		is_product_owner, &
		is_inquire_options, &
		is_return_string, &
		is_checked_boxes, &
		is_prod_desc

Datastore		ids_report

w_netwise_response	iw_inquirewindow, &
			iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_set_headings ()
end prototypes

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row

string		ls_options, &
				ls_division, &  
            ls_division_desc, &
				ls_date, &
				ls_product_owner,&
				ls_product_groups, &
				ls_product_state, &
				ls_product_status, &
				ls_data, &
				ls_header

u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If mid(is_checked_boxes,1,1) = 'Y' Then
	dw_plant.Visible = True
Else	
	dw_plant.Visible = False
End If

If mid(is_checked_boxes,2,1) = 'Y' Then
	dw_loc_group_info.Visible = True
Else	
	dw_loc_group_info.Visible = False
End If

If mid(is_checked_boxes,3,1) = 'Y' Then
	dw_division.Visible = True
Else	
	dw_division.Visible = False
End If

If mid(is_checked_boxes,4,1) = 'Y' Then
	dw_product_state.Visible = True
Else	
	dw_product_state.Visible = False
End If

If mid(is_checked_boxes,5,1) = 'Y' Then
	dw_product_status.Visible = True
Else	
	dw_product_status.Visible = False
End If

If mid(is_checked_boxes,6,1) = 'Y' Then
	dw_product.Visible = True
Else	
	dw_product.Visible = False
End If

If mid(is_checked_boxes,7,1) = 'Y' Then
	dw_prod_group_info_1.Visible = True
Else	
	dw_prod_group_info_1.Visible = False
End If

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp77br_inq_age_avail"
istr_error_info.se_message = Space(71)

ls_division = dw_division.uf_get_division()
ls_division_desc = dw_division.uf_get_division_descr()

ls_product_owner = is_product_owner
ls_product_groups = Trim(lu_string.nf_globalreplace(is_product_group_id,'~r~n',' '))
ls_options		  = is_inquire_options

ls_date = string(Today(), "yyyy-mm-dd")

ls_product_state = dw_product_state.uf_get_product_state()

ls_header =  	ls_date + '~t' + is_return_string

wf_set_headings()

dw_age_avail_detail.reset()

//MessageBox('Inquire Data',string(ls_header))

//If iu_pas201.nf_pasp77br_inq_age_avail(istr_error_info, & 
//										ls_header, &
//										ls_data) < 0 Then

If iu_ws_pas_share.nf_pasp77fr(istr_error_info, & 
									ls_header, &
									ls_data) < 0 Then
									This.SetRedraw(True) 
									Return False
End If										

 
If Not lu_string.nf_IsEmpty(ls_data) Then
	ll_rec_count = dw_age_avail_detail.ImportString(ls_data) 
	If ll_rec_count > 0 Then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

dw_age_avail_detail.ResetUpdate()
dw_age_avail_detail.SetFocus()
 
Return True

end function

public subroutine wf_set_headings ();Integer	li_from_range, &
			li_to_range, &
			li_sub
			
li_from_range = dw_date_range.GetItemNumber(1, "from_range")	
li_to_range = dw_date_range.GetItemNumber(1, "to_range")

li_sub = li_from_range
dw_age_avail_detail.Object.age_label_1_t.Text = String(li_sub)
dw_age_avail_detail.Object.age_1.Visible = True
dw_age_avail_detail.Object.compute_1.Visible = True
li_sub ++

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_2_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_2_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_2.Visible = True
	dw_age_avail_detail.Object.compute_2.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_2_t.Text = ''
	dw_age_avail_detail.Object.age_2.Visible = False
	dw_age_avail_detail.Object.compute_2.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_3_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_3_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_3.Visible = True
	dw_age_avail_detail.Object.compute_3.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_3_t.Text = ''
	dw_age_avail_detail.Object.age_3.Visible = False
	dw_age_avail_detail.Object.compute_3.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_4_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_4_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_4.Visible = True
	dw_age_avail_detail.Object.compute_4.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_4_t.Text = ''
	dw_age_avail_detail.Object.age_4.Visible = False
	dw_age_avail_detail.Object.compute_4.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_5_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_5_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_5.Visible = True
	dw_age_avail_detail.Object.compute_5.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_5_t.Text = ''
	dw_age_avail_detail.Object.age_5.Visible = False
	dw_age_avail_detail.Object.compute_5.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_6_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_6_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_6.Visible = True
	dw_age_avail_detail.Object.compute_6.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_6_t.Text = ''
	dw_age_avail_detail.Object.age_6.Visible = False
	dw_age_avail_detail.Object.compute_6.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_7_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_7_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_7.Visible = True
	dw_age_avail_detail.Object.compute_7.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_7_t.Text = ''
	dw_age_avail_detail.Object.age_7.Visible = False
	dw_age_avail_detail.Object.compute_7.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_8_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_8_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_8.Visible = True
	dw_age_avail_detail.Object.compute_8.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_8_t.Text = ''
	dw_age_avail_detail.Object.age_8.Visible = False
	dw_age_avail_detail.Object.compute_8.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_9_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_9_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_9.Visible = True
	dw_age_avail_detail.Object.compute_9.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_9_t.Text = ''
	dw_age_avail_detail.Object.age_9.Visible = False
	dw_age_avail_detail.Object.compute_9.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_10_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_10_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_10.Visible = True
	dw_age_avail_detail.Object.compute_10.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_10_t.Text = ''
	dw_age_avail_detail.Object.age_10.Visible = False
	dw_age_avail_detail.Object.compute_10.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_11_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_11_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_11.Visible = True
	dw_age_avail_detail.Object.compute_11.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_11_t.Text = ''
	dw_age_avail_detail.Object.age_11.Visible = False
	dw_age_avail_detail.Object.compute_11.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_12_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_12_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_12.Visible = True
	dw_age_avail_detail.Object.compute_12.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_12_t.Text = ''
	dw_age_avail_detail.Object.age_12.Visible = False
	dw_age_avail_detail.Object.compute_12.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_13_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_13_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_13.Visible = True
	dw_age_avail_detail.Object.compute_13.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_13_t.Text = ''
	dw_age_avail_detail.Object.age_13.Visible = False
	dw_age_avail_detail.Object.compute_13.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_14_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_14_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_14.Visible = True
	dw_age_avail_detail.Object.compute_14.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_14_t.Text = ''
	dw_age_avail_detail.Object.age_14.Visible = False
	dw_age_avail_detail.Object.compute_14.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_15_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_15_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_15.Visible = True
	dw_age_avail_detail.Object.compute_15.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_15_t.Text = ''
	dw_age_avail_detail.Object.age_15.Visible = False
	dw_age_avail_detail.Object.compute_15.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_16_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_16_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_16.Visible = True
	dw_age_avail_detail.Object.compute_16.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_16_t.Text = ''
	dw_age_avail_detail.Object.age_16.Visible = False
	dw_age_avail_detail.Object.compute_16.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_17_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_17_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_17.Visible = True
	dw_age_avail_detail.Object.compute_17.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_17_t.Text = ''
	dw_age_avail_detail.Object.age_17.Visible = False
	dw_age_avail_detail.Object.compute_17.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_18_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_18_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_18.Visible = True
	dw_age_avail_detail.Object.compute_18.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_18_t.Text = ''
	dw_age_avail_detail.Object.age_18.Visible = False
	dw_age_avail_detail.Object.compute_18.Visible = False
End If

If li_sub <= li_to_range Then
	If li_sub = li_to_range Then
		dw_age_avail_detail.Object.age_label_19_t.Text = String(li_sub) + '+'
	Else	
		dw_age_avail_detail.Object.age_label_19_t.Text = String(li_sub)
	End If
	dw_age_avail_detail.Object.age_19.Visible = True
	dw_age_avail_detail.Object.compute_19.Visible = True
	li_sub ++
Else
	dw_age_avail_detail.Object.age_label_19_t.Text = ''
	dw_age_avail_detail.Object.age_19.Visible = False
	dw_age_avail_detail.Object.compute_19.Visible = False
End If



end subroutine

on w_age_avail.create
int iCurrent
call super::create
this.dw_date_range=create dw_date_range
this.dw_product_status=create dw_product_status
this.dw_product=create dw_product
this.dw_loc_group_info=create dw_loc_group_info
this.dw_plant=create dw_plant
this.dw_age_avail_detail=create dw_age_avail_detail
this.dw_product_state=create dw_product_state
this.dw_division=create dw_division
this.dw_prod_group_info_1=create dw_prod_group_info_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_date_range
this.Control[iCurrent+2]=this.dw_product_status
this.Control[iCurrent+3]=this.dw_product
this.Control[iCurrent+4]=this.dw_loc_group_info
this.Control[iCurrent+5]=this.dw_plant
this.Control[iCurrent+6]=this.dw_age_avail_detail
this.Control[iCurrent+7]=this.dw_product_state
this.Control[iCurrent+8]=this.dw_division
this.Control[iCurrent+9]=this.dw_prod_group_info_1
end on

on w_age_avail.destroy
call super::destroy
destroy(this.dw_date_range)
destroy(this.dw_product_status)
destroy(this.dw_product)
destroy(this.dw_loc_group_info)
destroy(this.dw_plant)
destroy(this.dw_age_avail_detail)
destroy(this.dw_product_state)
destroy(this.dw_division)
destroy(this.dw_prod_group_info_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')

iw_frame.im_menu.mf_enable('m_print')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

//iw_frame.im_menu.mf_disable('m_print')


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288
//  
//dw_age_avail_detail.width	= newwidth - (30 + li_x)
//dw_age_avail_detail.height	= newheight - (30 + li_y)

long       ll_x = 50, ll_y = 150

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If


dw_age_avail_detail.Resize(&
						this.width - dw_age_avail_detail.x - ll_x, &
						this.height - dw_age_avail_detail.y - ll_y)
						
//dw_age_avail_detail.Resize(&
//						this.width - dw_age_avail_detail.x - 50, &
//						this.height - dw_age_avail_detail.y - 150)


end event

event ue_fileprint;call super::ue_fileprint;String						ls_mod, &
								ls_temp, &
								ls_week_end_date
								
Integer					li_sub, li_print_sub								

DataStore					lds_print


lds_print = create u_print_datastore

If Message.nf_Get_App_ID() = 'ORP' Then
	lds_print.DataObject = 'd_age_avail_report_orp'
Else
	lds_print.DataObject = 'd_age_avail_report'
End If

If mid(is_checked_boxes,1,1) = 'Y' Then
	lds_print.object.plant_label_t.Visible = True
	lds_print.object.plant_code_t.Visible = True
	lds_print.object.plant_code_t.Text = dw_plant.uf_get_plant_code()
	lds_print.object.plant_desc_t.Visible = True
	lds_print.object.plant_desc_t.Text = dw_plant.uf_get_plant_descr()
Else
	lds_print.object.plant_label_t.Visible = False
	lds_print.object.plant_code_t.Visible = False
	lds_print.object.plant_desc_t.Visible = False
End If

If mid(is_checked_boxes,2,1) = 'Y' Then
	lds_print.object.loc_group_system_label_t.Visible = True
	lds_print.object.loc_group_label_t.Visible = True
	lds_print.object.location_system_t.Visible = True
	lds_print.object.location_system_t.Text = dw_loc_group_info.Object.owner_t.Text
	lds_print.object.loc_group_t.Visible = True
	lds_print.object.loc_group_t.Text = dw_loc_group_info.GetItemString(1, "loc_group_info")
Else
	lds_print.object.loc_group_system_label_t.Visible = False
	lds_print.object.loc_group_label_t.Visible = False
	lds_print.object.location_system_t.Visible = False
	lds_print.object.loc_group_t.Visible = False
End If

If mid(is_checked_boxes,3,1) = 'Y' Then
	lds_print.object.division_label_t.Visible = True
	lds_print.object.division_code_t.Visible = True
	lds_print.object.division_code_t.Text = dw_division.uf_get_division()
	lds_print.object.division_descr_t.Visible = True
	lds_print.object.division_descr_t.Text = dw_division.uf_get_division_descr()
Else
	lds_print.object.division_label_t.Visible = False
	lds_print.object.division_code_t.Visible = False
	lds_print.object.division_descr_t.Visible = False
End If

If mid(is_checked_boxes,4,1) = 'Y' Then
	lds_print.object.product_state_label_t.Visible = True
	lds_print.object.product_state_t.Visible = True
	lds_print.object.product_state_t.Text = dw_product_state.uf_get_product_state_desc() + ' PRODUCT'
Else
	lds_print.object.product_state_label_t.Visible = False
	lds_print.object.product_state_t.Visible = False
End If

If mid(is_checked_boxes,5,1) = 'Y' Then
	lds_print.object.product_status_label_t.Visible = True
	lds_print.object.product_status_t.Visible = True
	lds_print.object.product_status_t.Text = dw_product_status.uf_get_product_status_desc() + ' PRODUCT'
Else
	lds_print.object.product_status_label_t.Visible = False
	lds_print.object.product_status_t.Visible = False
End If

If mid(is_checked_boxes,6,1) = 'Y' Then
	lds_print.object.product_code_label_t.Visible = True
	lds_print.object.prod_code_t.Visible = True
	lds_print.object.prod_code_t.Text = dw_product.uf_get_product_code()
	lds_print.object.product_desc_t.Visible = True
	lds_print.object.product_desc_t.Text = dw_product.uf_get_product_desc()
Else
	lds_print.object.product_code_label_t.Visible = False
	lds_print.object.prod_code_t.Visible = False
	lds_print.object.product_desc_t.Visible = False
End If

If mid(is_checked_boxes,7,1) = 'Y' Then
	lds_print.object.prod_group_system_label_t.Visible = True
	lds_print.object.prod_group_label_t.Visible = True
	lds_print.object.product_system_t.Visible = True
	lds_print.object.product_system_t.Text = dw_prod_group_info_1.Object.owner_t.Text
	lds_print.object.prod_group_t.Visible = True
	lds_print.object.prod_group_t.Text = dw_prod_group_info_1.GetItemString(1, "prod_group_info")
Else
	lds_print.object.prod_group_system_label_t.Visible = False
	lds_print.object.prod_group_label_t.Visible = False
	lds_print.object.product_system_t.Visible = False
	lds_print.object.prod_group_t.Visible = False
End If

lds_print.object.day_range_from_t.Text = String(dw_date_range.GetItemNumber(1, "from_range"))
lds_print.object.day_range_to_t.Text = String(dw_date_range.GetItemNumber(1, "to_range"))

lds_print.object.age_label_1_t.Text = dw_age_avail_detail.object.age_label_1_t.Text
lds_print.object.age_label_2_t.Text = dw_age_avail_detail.object.age_label_2_t.Text
lds_print.object.age_label_3_t.Text = dw_age_avail_detail.object.age_label_3_t.Text
lds_print.object.age_label_4_t.Text = dw_age_avail_detail.object.age_label_4_t.Text
lds_print.object.age_label_5_t.Text = dw_age_avail_detail.object.age_label_5_t.Text
lds_print.object.age_label_6_t.Text = dw_age_avail_detail.object.age_label_6_t.Text
lds_print.object.age_label_7_t.Text = dw_age_avail_detail.object.age_label_7_t.Text
lds_print.object.age_label_8_t.Text = dw_age_avail_detail.object.age_label_8_t.Text
lds_print.object.age_label_9_t.Text = dw_age_avail_detail.object.age_label_9_t.Text
lds_print.object.age_label_10_t.Text = dw_age_avail_detail.object.age_label_10_t.Text
lds_print.object.age_label_11_t.Text = dw_age_avail_detail.object.age_label_11_t.Text
lds_print.object.age_label_12_t.Text = dw_age_avail_detail.object.age_label_12_t.Text

If Message.nf_Get_App_ID() <> 'ORP' Then
	lds_print.object.age_label_13_t.Text = dw_age_avail_detail.object.age_label_13_t.Text
	lds_print.object.age_label_14_t.Text = dw_age_avail_detail.object.age_label_14_t.Text
	lds_print.object.age_label_15_t.Text = dw_age_avail_detail.object.age_label_15_t.Text
	lds_print.object.age_label_16_t.Text = dw_age_avail_detail.object.age_label_16_t.Text
	lds_print.object.age_label_17_t.Text = dw_age_avail_detail.object.age_label_17_t.Text
	lds_print.object.age_label_18_t.Text = dw_age_avail_detail.object.age_label_18_t.Text
	lds_print.object.age_label_19_t.Text = dw_age_avail_detail.object.age_label_19_t.Text
End If

lds_print.object.age_1.Visible = dw_age_avail_detail.object.age_1.Visible 
lds_print.object.age_2.Visible = dw_age_avail_detail.object.age_2.Visible 
lds_print.object.age_3.Visible = dw_age_avail_detail.object.age_3.Visible 
lds_print.object.age_4.Visible = dw_age_avail_detail.object.age_4.Visible 
lds_print.object.age_5.Visible = dw_age_avail_detail.object.age_5.Visible 
lds_print.object.age_6.Visible = dw_age_avail_detail.object.age_6.Visible 
lds_print.object.age_7.Visible = dw_age_avail_detail.object.age_7.Visible 
lds_print.object.age_8.Visible = dw_age_avail_detail.object.age_8.Visible 
lds_print.object.age_9.Visible = dw_age_avail_detail.object.age_9.Visible 
lds_print.object.age_10.Visible = dw_age_avail_detail.object.age_10.Visible 
lds_print.object.age_11.Visible = dw_age_avail_detail.object.age_11.Visible 
lds_print.object.age_12.Visible = dw_age_avail_detail.object.age_12.Visible 
If Message.nf_Get_App_ID() <> 'ORP' Then
	lds_print.object.age_13.Visible = dw_age_avail_detail.object.age_13.Visible 
	lds_print.object.age_14.Visible = dw_age_avail_detail.object.age_14.Visible 
	lds_print.object.age_15.Visible = dw_age_avail_detail.object.age_15.Visible 
	lds_print.object.age_16.Visible = dw_age_avail_detail.object.age_16.Visible 
	lds_print.object.age_17.Visible = dw_age_avail_detail.object.age_17.Visible 
	lds_print.object.age_18.Visible = dw_age_avail_detail.object.age_18.Visible 
	lds_print.object.age_19.Visible = dw_age_avail_detail.object.age_19.Visible 
End if

lds_print.object.compute_1.Visible = dw_age_avail_detail.object.compute_1.Visible
lds_print.object.compute_2.Visible = dw_age_avail_detail.object.compute_2.Visible
lds_print.object.compute_3.Visible = dw_age_avail_detail.object.compute_3.Visible
lds_print.object.compute_4.Visible = dw_age_avail_detail.object.compute_4.Visible
lds_print.object.compute_5.Visible = dw_age_avail_detail.object.compute_5.Visible
lds_print.object.compute_6.Visible = dw_age_avail_detail.object.compute_6.Visible
lds_print.object.compute_7.Visible = dw_age_avail_detail.object.compute_7.Visible
lds_print.object.compute_8.Visible = dw_age_avail_detail.object.compute_8.Visible
lds_print.object.compute_9.Visible = dw_age_avail_detail.object.compute_9.Visible
lds_print.object.compute_10.Visible = dw_age_avail_detail.object.compute_10.Visible
lds_print.object.compute_11.Visible = dw_age_avail_detail.object.compute_11.Visible
lds_print.object.compute_12.Visible = dw_age_avail_detail.object.compute_12.Visible
If Message.nf_Get_App_ID() <> 'ORP' Then
	lds_print.object.compute_13.Visible = dw_age_avail_detail.object.compute_13.Visible
	lds_print.object.compute_14.Visible = dw_age_avail_detail.object.compute_14.Visible
	lds_print.object.compute_15.Visible = dw_age_avail_detail.object.compute_15.Visible
	lds_print.object.compute_16.Visible = dw_age_avail_detail.object.compute_16.Visible
	lds_print.object.compute_17.Visible = dw_age_avail_detail.object.compute_17.Visible
	lds_print.object.compute_18.Visible = dw_age_avail_detail.object.compute_18.Visible
	lds_print.object.compute_19.Visible = dw_age_avail_detail.object.compute_19.Visible
End If

//If is_inquire_options = 'D' Then
//	lds_print.object.pg_t.Visible = 0
//	lds_print.object.pgs_t.Visible = 0
//	lds_print.object.prod_group_t.Visible = 0
//	lds_print.object.owner_t.Visible = 0
//ElseIf is_inquire_options = 'P' Then
//		lds_print.object.division_code_t.visible = 0
//		lds_print.object.division_descr_t.visible = 0
//		lds_print.object.d_t.visible = 0
//	End  If
//lds_print.object.division_code_t.text = dw_division.uf_get_division()
//lds_print.object.division_descr_t.text = dw_division.uf_get_division_descr()
//lds_print.object.product_state_t.text = dw_product_state.uf_get_product_state_desc()
//
//lds_print.object.owner_t.text = is_product_owner
//lds_print.object.prod_group_t.text = is_product_group_desc
//

If Message.nf_Get_App_ID() = 'ORP' Then
	lds_print.Reset()
	For li_sub = 1 to dw_age_avail_detail.RowCount()
		li_print_sub = lds_print.InsertRow(0)
		lds_print.SetItem(li_print_sub, "product_code", dw_age_avail_detail.GetItemString(li_sub, "product_code"))
		lds_print.SetItem(li_print_sub, "plant", dw_age_avail_detail.GetItemString(li_sub, "plant"))
		lds_print.SetItem(li_print_sub, "age_1", dw_age_avail_detail.GetItemNumber(li_sub, "age_1"))
		lds_print.SetItem(li_print_sub, "age_2", dw_age_avail_detail.GetItemNumber(li_sub, "age_2"))
		lds_print.SetItem(li_print_sub, "age_3", dw_age_avail_detail.GetItemNumber(li_sub, "age_3"))
		lds_print.SetItem(li_print_sub, "age_4", dw_age_avail_detail.GetItemNumber(li_sub, "age_4"))
		lds_print.SetItem(li_print_sub, "age_5", dw_age_avail_detail.GetItemNumber(li_sub, "age_5"))
		lds_print.SetItem(li_print_sub, "age_6", dw_age_avail_detail.GetItemNumber(li_sub, "age_6"))
		lds_print.SetItem(li_print_sub, "age_7", dw_age_avail_detail.GetItemNumber(li_sub, "age_7"))
		lds_print.SetItem(li_print_sub, "age_8", dw_age_avail_detail.GetItemNumber(li_sub, "age_8"))
		lds_print.SetItem(li_print_sub, "age_9", dw_age_avail_detail.GetItemNumber(li_sub, "age_9"))
		lds_print.SetItem(li_print_sub, "age_10", dw_age_avail_detail.GetItemNumber(li_sub, "age_10"))
		lds_print.SetItem(li_print_sub, "age_11", dw_age_avail_detail.GetItemNumber(li_sub, "age_11"))
		lds_print.SetItem(li_print_sub, "age_12", dw_age_avail_detail.GetItemNumber(li_sub, "age_12"))
	Next
Else
	dw_age_avail_detail.sharedata(lds_print)
End If

ls_temp = lds_print.modify(ls_mod)

lds_print.print()

end event

event ue_get_data;Choose Case as_value
	Case 'division'
		message.StringParm = dw_division.uf_get_division()
	Case 'div_visible'
		message.StringParm = dw_division.uf_get_division()
	Case 'product_state'
		message.StringParm = dw_product_state.uf_get_product_state()
	Case 'product_status'
		message.StringParm = dw_product_status.uf_get_product_status()		
	Case 'product_group_id'
		message.StringParm = is_product_group_id
	Case 'product_owner'
		message.StringParm = is_product_owner
	Case 'inquire_options'
		message.StringParm = is_inquire_options
	Case 'checked_boxes'
		message.StringParm = is_checked_boxes
	Case 'product'
		message.Stringparm = dw_product.uf_get_product_code()	
	Case 'product_desc'
		message.Stringparm = dw_product.uf_get_product_desc()	
	Case 'from_range'
		If IsNull(dw_date_range.GetItemNumber(1, "from_range")) Then
			message.Stringparm = "0"
		Else
			message.Stringparm = string(dw_date_range.GetItemNumber(1, "from_range"))
		End If
	Case 'to_range'
		If IsNull(dw_date_range.GetItemNumber(1, "to_range")) Then
			message.Stringparm = "0"
		Else
			message.Stringparm = string(dw_date_range.GetItemNumber(1, "to_range"))
		End If			
End choose

end event

event ue_set_data;u_conversion_functions		lu_conv


Choose Case as_data_item
	Case 'division'
		dw_division.uf_set_division(as_value)
	Case 'div_visible'
		If as_value = 'False' Then
			dw_division.Visible = False
		else	
			dw_division.Visible = True
		End If
	Case 'prod_group_visible'
		If as_value = 'False' Then
			dw_prod_group_info_1.Visible = False
		else	
			dw_prod_group_info_1.Visible = True
		End If
	Case 'product_state'
		dw_product_state.uf_set_product_state(as_value)
	Case 'product_status'
		dw_product_status.uf_set_product_status(as_value)		
	Case 'product_group_desc'
		is_product_group_desc = as_value
		dw_prod_group_info_1.SetItem(1,'prod_group_info',as_value)
	Case 'product_group_id'
		is_product_group_id = as_value
	Case 'product_owner'
		is_product_owner = as_value
		dw_prod_group_info_1.Object.owner_t.Text = is_product_owner
	Case 'inquire_options'
		is_inquire_options = as_value
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)	
	Case 'loc_system'
		dw_loc_group_info.Object.owner_t.Text = as_value
//	Case 'loc_group'
//		dw_loc_group_info.SetItem(1, "loc_group_info", as_value)	
	Case 'loc_desc'
		dw_loc_group_info.SetItem(1, "loc_group_info", as_value)
	Case 'prod_system'
		dw_prod_group_info_1.Object.owner_t.Text = as_value
//	Case 'prod_group'
//		dw_prod_group_info.SetItem(1, "prod_group_info", as_value)	
	Case 'prod_desc'
		dw_prod_group_info_1.SetItem(1, "prod_group_info", as_value)	
	Case 'product'
		dw_product.uf_set_product_code(as_value)
	Case 'product_desc'
		dw_product.uf_set_product_desc(as_value)	
	Case 'return_string'
		is_return_string = as_value
	Case 'checked_boxes'
		is_checked_boxes = as_value	
	Case 'from_range'
		dw_date_range.SetItem(1, "from_range", Integer(as_value)) 	
	Case 'to_range'
		dw_date_range.SetItem(1, "to_range", Integer(as_value)) 			
End Choose
end event

event ue_postopen;call super::ue_postopen;Environment	le_env


GetEnvironment(le_env)
//If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
// 	il_SelectedColor = GetSysColor(13)
//	il_SelectedTextColor = GetSysColor(14)
//Else
// 	il_SelectedColor = 255
//	il_SelectedTextColor = 0
//End if
//
This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "ageavail"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_age_avail_inq'

string ls_prod

ls_prod = dw_product.GetItemString(1, 'fab_product_code') + "~t" + &
					dw_product.GetItemString(1, 'fab_product_description') + "~t" + &
					dw_product.GetItemString(1, 'product_state') + "~t" + &
					dw_product.GetItemString(1, 'product_status') + "~t" + &
					dw_product.GetItemString(1, 'product_state_description') + "~t" + &
					dw_product.GetItemString(1, 'product_status_description')
					
								 
If Not iw_frame.iu_string.nf_IsEmpty(ls_prod) Then 
	dw_product.Reset()
	dw_product.uf_importstring(ls_prod, TRUE)
End If

dw_product.uf_enable(false)
dw_product.uf_set_product_state('1')
dw_product.uf_set_product_status('G')

iu_pas201 = Create u_pas201
iu_ws_pas_share = Create u_ws_pas_share

wf_retrieve()

end event

type dw_date_range from u_base_dw_ext within w_age_avail
integer y = 196
integer width = 1157
integer height = 96
integer taborder = 10
boolean enabled = false
string dataobject = "d_age_avail_range"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_product_status from u_product_status within w_age_avail
integer x = 1463
integer y = 128
integer width = 987
integer height = 64
integer taborder = 0
end type

event constructor;call super::constructor;This.uf_Enable(FALSE)

This.uf_set_product_status('G')
This.Enabled = False //don't know why I need to do this since the uf_Enable above should work -SCS.
end event

type dw_product from u_fab_product_code within w_age_avail
integer x = 1463
integer y = 212
integer width = 1390
integer height = 96
integer taborder = 10
boolean enabled = false
end type

type dw_loc_group_info from u_location_group_info within w_age_avail
integer y = 336
integer width = 2592
integer taborder = 0
boolean vscrollbar = false
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_plant from u_plant within w_age_avail
integer x = 37
integer y = 12
integer width = 1385
integer taborder = 0
end type

event constructor;call super::constructor;This.ib_protected = TRUE
end event

type dw_age_avail_detail from datawindow within w_age_avail
integer x = 9
integer y = 684
integer width = 3986
integer height = 1176
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_age_avail_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_product_state from u_product_state within w_age_avail
integer x = 1495
integer y = 24
integer height = 88
integer taborder = 0
boolean bringtotop = true
boolean enabled = false
boolean livescroll = false
end type

event constructor;call super::constructor;This.uf_Enable(FALSE)

This.uf_set_product_state('1')
end event

type dw_division from u_division within w_age_avail
integer y = 96
integer width = 1431
integer taborder = 0
boolean bringtotop = true
boolean enabled = false
end type

event constructor;call super::constructor;This.disable()
end event

type dw_prod_group_info_1 from u_owner_group_info within w_age_avail
integer y = 508
integer width = 2587
integer taborder = 0
boolean bringtotop = true
boolean vscrollbar = false
boolean border = false
boolean livescroll = false
borderstyle borderstyle = stylebox!
end type

event constructor;This.InsertRow(0)
end event

