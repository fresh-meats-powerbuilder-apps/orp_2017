﻿$PBExportHeader$w_pas_freezer_inv_position.srw
forward
global type w_pas_freezer_inv_position from w_netwise_sheet
end type
type dw_product_status_window from datawindow within w_pas_freezer_inv_position
end type
type dw_freezer_inv_position_report from u_netwise_dw within w_pas_freezer_inv_position
end type
type dw_division from u_division within w_pas_freezer_inv_position
end type
type tab_1 from tab within w_pas_freezer_inv_position
end type
type tp_inventory from userobject within tab_1
end type
type dw_inventory from u_netwise_dw within tp_inventory
end type
type tp_inventory from userobject within tab_1
dw_inventory dw_inventory
end type
type tp_production from userobject within tab_1
end type
type dw_production from u_netwise_dw within tp_production
end type
type tp_production from userobject within tab_1
dw_production dw_production
end type
type tp_sales from userobject within tab_1
end type
type dw_sales from u_netwise_dw within tp_sales
end type
type tp_sales from userobject within tab_1
dw_sales dw_sales
end type
type tab_1 from tab within w_pas_freezer_inv_position
tp_inventory tp_inventory
tp_production tp_production
tp_sales tp_sales
end type
type dw_freezer_inv_position_header from u_base_dw within w_pas_freezer_inv_position
end type
end forward

global type w_pas_freezer_inv_position from w_netwise_sheet
integer x = 0
integer y = 0
integer width = 2926
integer height = 1532
string title = "Freezer Sales / Inventory Position"
long backcolor = 12632256
long il_borderpaddingwidth = 100
dw_product_status_window dw_product_status_window
dw_freezer_inv_position_report dw_freezer_inv_position_report
dw_division dw_division
tab_1 tab_1
dw_freezer_inv_position_header dw_freezer_inv_position_header
end type
global w_pas_freezer_inv_position w_pas_freezer_inv_position

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
Integer	ii_dddw_x, ii_dddw_y
Long		il_SelectedColor, &
		il_SelectedTextColor

u_pas203		iu_pas203
u_ws_pas_share iu_ws_pas_share

s_error		istr_error_info

boolean		ib_reinquire

DataWindow	idw_top_dw

long il_selected_status_max = 8

String         is_selected_status_array

end variables

forward prototypes
public function string wf_get_plant ()
public subroutine wf_filter ()
public function string wf_get_boxes_or_loads ()
public function string wf_get_selected_status ()
public function boolean wf_retrieve ()
end prototypes

public function string wf_get_plant ();String			ls_plant

ls_plant = dw_freezer_inv_position_header.GetItemString(1, "plant_code") + "~t" + &
					dw_freezer_inv_position_header.GetItemString(1, "plant_descr") 

Return(ls_plant)
end function

public subroutine wf_filter ();Integer		li_counter

Long			ll_nbrrows

String		ls_filter, &
				ls_row_message

If dw_freezer_inv_position_header.GetText() = "Y" Then

	ls_filter = "inv_1_qty < 0 or inv_2_qty < 0 or inv_3_qty < 0 or " + &
					"inv_4_qty < 0 or inv_5_qty < 0 or inv_6_qty  " + &
					"< 0 or inv_7_qty < 0"
	ls_row_message = " rows with negative values"
Else
	ls_filter = ""
	ls_row_message = " rows total"
End IF

dw_freezer_inv_position_report.SetFilter(ls_filter)	
dw_freezer_inv_position_report.Filter()
ll_nbrrows = dw_freezer_inv_position_report.RowCount()

// Set the highlighting again.
dw_freezer_inv_position_report.SetItem(1,"back_color", il_selectedcolor)
dw_freezer_inv_position_report.SetItem(1,"text_color", il_selectedtextcolor)
dw_freezer_inv_position_report.SetItem(1,"last_updated",  &
		dw_freezer_inv_position_header.GetItemDateTime(1, "last_updated"))
dw_freezer_inv_position_report.SetItem(1,"plant_code", &
		dw_freezer_inv_position_header.GetItemString(1, "plant_code"))
dw_freezer_inv_position_report.SetItem(1,"plant_descr", &
		dw_freezer_inv_position_header.GetItemString(1, "plant_descr"))
dw_freezer_inv_position_report.SetItem(1,"division_code", &
		dw_division.uf_get_division())
dw_freezer_inv_position_report.SetItem(1,"division_descr", &
		dw_division.uf_get_division_descr())

iw_frame.SetMicroHelp(String(ll_nbrrows) + ls_row_message)

Return
end subroutine

public function string wf_get_boxes_or_loads ();String			ls_boxes_or_loads

ls_boxes_or_loads = dw_freezer_inv_position_header.GetItemString(1, "boxes_or_loads")

Return(ls_boxes_or_loads)
end function

public function string wf_get_selected_status ();
//1-13 jac
long  ll_count
string ls_product_status, & 
		ls_stat_desc, &
       ls_input 

is_selected_status_array = ''
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	 if ll_count = 1 then
		If isnull(ls_product_status) THEN
		   ls_product_status = 'ALL'
		end if 
	 END IF
	 if ll_count > 1 then
	  If isnull(ls_product_status) Then
		  ls_product_status = " "
	  end if
	end if 
	is_selected_status_array += ls_product_status + "~t"
   ls_stat_desc = dw_product_status_window.GetItemString(1, "status_desc" + string(ll_count))
   ls_stat_desc = TRIM(ls_stat_desc)
	if ll_count = 1 then
		If isnull(ls_stat_desc) Then
	    	ls_stat_desc = 'ALL'		
		END IF
	ELSE 
	  If isnull(ls_stat_desc) Then
		ls_stat_desc = " "
	  end if
   end if
	is_selected_status_array += ls_stat_desc + "~t"
next


Return is_selected_status_array
//



end function

public function boolean wf_retrieve ();String		ls_string, &
            ls_string2, &
				ls_date_time_stamp, &
				ls_input, &
				ls_plant_code, &
				ls_plant_descr, &
				ls_division_code, &
				ls_division_descr, &
				ls_selection, &
				ls_product_status, &
				ls_stat_desc

DateTime		ldt_datetime

Long			ll_rec_count, &
            ll_count, &
				ll_pos, &
            ll_counter, &
            ll_counter_max

Integer		li_counter

Call w_base_sheet::closequery

SetPointer(HourGlass!)

OpenWithParm(w_pas_freezer_inv_position_inq, This)
ls_string = Message.StringParm
If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False

dw_freezer_inv_position_header.Reset()
dw_freezer_inv_position_report.Reset()

If dw_freezer_inv_position_header.ImportString(Left(ls_string,iw_frame.iu_string.nf_nPos(ls_string,"~t",1,2)-1)) < 1  &
		Then Return False
dw_freezer_inv_position_header.SetItem(1,"boxes_or_loads",Right(ls_string,1))

//1-13 jac
ll_pos = 0
ll_counter = 1
ll_counter_max = 2
for ll_counter = 1 to ll_counter_max
ll_pos = Pos(ls_string,"~t", ll_pos + 1)
//ll_counter = ll_counter +1 
next

ls_string2 = Mid(ls_string,ll_pos + 1,iw_frame.iu_string.nf_nPos(ls_string,"~t",ll_pos + 1,16))

//if dw_product_status_window.ImportString(Mid(ls_string,ll_pos + 1,iw_frame.iu_string.nf_nPos(ls_string,"~t",ll_pos + 1,8)-1)) < 1  &
//                                Then Return False
dw_product_status_window.ImportString(ls_string2)
dw_product_status_window.AcceptText()
//
SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

istr_error_info.se_event_name = "wf_retrieve"

ls_plant_code = dw_freezer_inv_position_header.GetItemString &
		(1, "plant_code")
ls_plant_descr = dw_freezer_inv_position_header.GetItemString &
		(1, "plant_descr")
ls_division_code = dw_division.uf_get_division()
ls_division_descr = dw_division.uf_get_division_descr()

ls_input = ls_plant_code + "~t" + ls_division_code + "~t" 
//1-13 jac
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
	ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) then
		ls_product_status = " "
	end if
   ls_input += ls_product_status + "~t"
next
//

//If Not iu_pas203.nf_inq_freezer_inv_position(istr_error_info, &
//													ls_input, &
//													ls_date_time_stamp, &
//													dw_freezer_inv_position_report) Then 

If Not iu_ws_pas_share.nf_pasp49fr(istr_error_info, &
													ls_input, &
													ls_date_time_stamp, &
													dw_freezer_inv_position_report) Then 
	SetRedraw(True)
	Return False
End If

SetRedraw (False)

ll_rec_count = dw_freezer_inv_position_report.RowCount()

dw_freezer_inv_position_report.SetItem(1, "back_color", il_selectedcolor)
dw_freezer_inv_position_report.SetItem(1, "text_color", il_selectedtextcolor)

dw_freezer_inv_position_report.Sort()
dw_freezer_inv_position_report.GroupCalc()


ldt_datetime = DateTime(Date(String(mid(ls_date_time_stamp, 1,10))), &
					Time(String(mid(ls_date_time_stamp, 12,5)))) 

dw_freezer_inv_position_report.SetItem(1,"boxes_or_loads", &
		dw_freezer_inv_position_header.GetItemString(1,"boxes_or_loads"))

If dw_freezer_inv_position_header.GetItemString(1,"boxes_or_loads") = 'B' Then 
	dw_freezer_inv_position_report.Modify("inv_1_comp.Format='###,##0' " + &
			"inv_2_comp.Format='###,##0' " + &
			"inv_3_comp.Format='###,##0' " + &
			"inv_4_comp.Format='###,##0' " + &
			"inv_5_comp.Format='###,##0' " + &
			"inv_6_comp.Format='###,##0' " + &
			"inv_7_comp.Format='###,##0' " + &
			"inv_total_comp.Format='###,##0' " + &
			"prod_current_ship_comp.Format='###,##0' " + &
			"sales_comp_1.Format='###,##0' " + &
			"sales_comp_2.Format='###,##0' " + &
			"sales_comp_3.Format='###,##0' " + &
			"sales_comp_4.Format='###,##0' " + &
			"sales_comp_5.Format='###,##0' " + &
			"sales_comp_6.Format='###,##0' " + &
			"sales_comp_7.Format='###,##0' " + &
			"sales_comp_8.Format='###,##0' " + &
			"sales_comp_9.Format='###,##0' " + &
			"inventory_sum_comp.Format='###,##0' " + &
			"production_sum_comp.Format='###,##0' " + &
			"ship_sum_comp.Format='###,##0' ")
	tab_1.tp_inventory.dw_inventory.Modify("inv_1_comp.Format='###,##0' " + &
			"inv_2_comp.Format='###,##0' " + &
			"inv_3_comp.Format='###,##0' " + &
			"inv_4_comp.Format='###,##0' " + &
			"inv_5_comp.Format='###,##0' " + &
			"inv_6_comp.Format='###,##0' " + &
			"inv_7_comp.Format='###,##0' " + &
			"inv_total_comp.Format='###,##0' " + &
			"sum_total_comp.Format='###,##0' ")
	tab_1.tp_production.dw_production.Modify("prod_current_ship_comp.Format='###,##0' " + &
			"prod_sum_ship_comp.Format='###,##0' ")
	tab_1.tp_sales.dw_sales.Modify("sales_comp_1.Format='###,##0' " + &
			"sales_comp_2.Format='###,##0' " + &
			"sales_comp_3.Format='###,##0' " + &
			"sales_comp_4.Format='###,##0' " + &
			"sales_comp_5.Format='###,##0' " + &
			"sales_comp_6.Format='###,##0' " + &
			"sales_comp_7.Format='###,##0' " + &
			"sales_comp_8.Format='###,##0' " + &
			"sales_comp_9.Format='###,##0' ")
Else
	dw_freezer_inv_position_report.Modify("inv_1_comp.Format='###,##0.0' " + &
			"inv_2_comp.Format='###,##0.0' " + &
			"inv_3_comp.Format='###,##0.0' " + &
			"inv_4_comp.Format='###,##0.0' " + &
			"inv_5_comp.Format='###,##0.0' " + &
			"inv_6_comp.Format='###,##0.0' " + &
			"inv_7_comp.Format='###,##0.0' " + &
			"inv_total_comp.Format='###,##0.0' " + &
			"prod_current_ship_comp.Format='###,##0.0' " + &
			"sales_comp_1.Format='###,##0.0' " + &
			"sales_comp_2.Format='###,##0.0' " + &
			"sales_comp_3.Format='###,##0.0' " + &
			"sales_comp_4.Format='###,##0.0' " + &
			"sales_comp_5.Format='###,##0.0' " + &
			"sales_comp_6.Format='###,##0.0' " + &
			"sales_comp_7.Format='###,##0.0' " + &
			"sales_comp_8.Format='###,##0.0' " + &
			"sales_comp_9.Format='###,##0.0' " + &
			"inventory_sum_comp.Format='###,##0.0' " + &
			"production_sum_comp.Format='###,##0.0' " + &
			"ship_sum_comp.Format='###,##0.0' ")
	tab_1.tp_inventory.dw_inventory.Modify("inv_1_comp.Format='###,##0.0' " + &
			"inv_2_comp.Format='###,##0.0' " + &
			"inv_3_comp.Format='###,##0.0' " + &
			"inv_4_comp.Format='###,##0.0' " + &
			"inv_5_comp.Format='###,##0.0' " + &
			"inv_6_comp.Format='###,##0.0' " + &
			"inv_7_comp.Format='###,##0.0' " + &
			"inv_total_comp.Format='###,##0.0' " + &
			"sum_total_comp.Format='###,##0.0' ")
	tab_1.tp_production.dw_production.Modify("prod_current_ship_comp.Format='###,##0.0' " + &
			"prod_sum_ship_comp.Format='###,##0.0' ")
	tab_1.tp_sales.dw_sales.Modify("sales_comp_1.Format='###,##0.0' " + &
			"sales_comp_2.Format='###,##0.0' " + &
			"sales_comp_3.Format='###,##0.0' " + &
			"sales_comp_4.Format='###,##0.0' " + &
			"sales_comp_5.Format='###,##0.0' " + &
			"sales_comp_6.Format='###,##0.0' " + &
			"sales_comp_7.Format='###,##0.0' " + &
			"sales_comp_8.Format='###,##0.0' " + &
			"sales_comp_9.Format='###,##0.0' ")
End If

dw_freezer_inv_position_header.SetItem(1,"last_updated", ldt_datetime)
dw_freezer_inv_position_report.SetItem(1,"plant_code", ls_plant_code)
dw_freezer_inv_position_report.SetItem(1,"plant_descr", ls_plant_descr)
dw_freezer_inv_position_report.SetItem(1,"division_code", ls_division_code)
dw_freezer_inv_position_report.SetItem(1,"division_descr", ls_division_descr)
dw_freezer_inv_position_report.SetItem(1,"last_updated", ldt_datetime)

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

SetRedraw(True)

Return (True)
end function

event resize;call super::resize;constant integer li_tab_x		= 20
constant integer li_tab_y		= 230
  
 integer li_dw_x		= 70
 integer li_dw_y		= 360


if il_BorderPaddingWidth > li_dw_x Then
   li_dw_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_dw_y Then
   li_dw_y = il_BorderPaddingHeight
End If

  
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tp_inventory.dw_inventory.width = newwidth - li_dw_x
tab_1.tp_inventory.dw_inventory.height = newheight - li_dw_y

tab_1.tp_production.dw_production.width = newwidth - li_dw_x
tab_1.tp_production.dw_production.height = newheight - li_dw_y

tab_1.tp_sales.dw_sales.width = newwidth - li_dw_x
tab_1.tp_sales.dw_sales.height = newheight - li_dw_y


end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')

//iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end event

event ue_postopen;call super::ue_postopen;Environment			le_env

iu_pas203 = create u_pas203
iu_ws_pas_share = Create u_ws_pas_share


If Message.ReturnValue = -1 Then Close(This)

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "frzinvpos"
istr_error_info.se_user_id				= sqlca.userid


GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

dw_freezer_inv_position_report.ShareData( tab_1.tp_inventory.dw_inventory)
dw_freezer_inv_position_report.ShareData( tab_1.tp_production.dw_production)
dw_freezer_inv_position_report.ShareData( tab_1.tp_sales.dw_sales)

Choose Case tab_1.SelectedTab
	Case 1
		idw_top_dw = tab_1.tp_inventory.dw_inventory
	Case 2
		idw_top_dw = tab_1.tp_production.dw_production
	Case 3
		idw_top_dw = tab_1.tp_sales.dw_sales
	Case 4
		idw_top_dw = tab_1.tp_inventory.dw_inventory	
	Case Else
		idw_top_dw = tab_1.tp_inventory.dw_inventory
		tab_1.SelectedTab = 1
End Choose
 

This.PostEvent('ue_query')
end event

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
end on

on w_pas_freezer_inv_position.create
int iCurrent
call super::create
this.dw_product_status_window=create dw_product_status_window
this.dw_freezer_inv_position_report=create dw_freezer_inv_position_report
this.dw_division=create dw_division
this.tab_1=create tab_1
this.dw_freezer_inv_position_header=create dw_freezer_inv_position_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_status_window
this.Control[iCurrent+2]=this.dw_freezer_inv_position_report
this.Control[iCurrent+3]=this.dw_division
this.Control[iCurrent+4]=this.tab_1
this.Control[iCurrent+5]=this.dw_freezer_inv_position_header
end on

on w_pas_freezer_inv_position.destroy
call super::destroy
destroy(this.dw_product_status_window)
destroy(this.dw_freezer_inv_position_report)
destroy(this.dw_division)
destroy(this.tab_1)
destroy(this.dw_freezer_inv_position_header)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')

iw_frame.im_menu.mf_enable('m_print')

end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

Destroy iu_ws_pas_share
end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

If dw_freezer_inv_position_header.GetItemString(1, 'neg_selection') = "Y" Then
	dw_freezer_inv_position_report.Modify("inventory_sum.Visible = 0 " + &
													"ship_sum.Visible = 0")
End if

dw_freezer_inv_position_report.print()

dw_freezer_inv_position_report.Modify("inventory_sum.Visible = 1 " + &
													"ship_sum.Visible = 1")

end event

event ue_get_data(string as_value);call super::ue_get_data;Choose Case as_value
	Case "product_status"
		message.StringParm = ''
	Case "selected"
		message.StringParm = is_selected_status_array
 	Case "dddwxpos"
		message.StringParm = string(ii_dddw_x)
	case "dddwypos"
		message.StringParm = string(ii_dddw_x)
End choose


end event

type dw_product_status_window from datawindow within w_pas_freezer_inv_position
boolean visible = false
integer x = 1614
integer y = 216
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "d_product_status_2"
boolean border = false
boolean livescroll = true
end type

event constructor;//ib_updateable = False
InsertRow(0)
end event

type dw_freezer_inv_position_report from u_netwise_dw within w_pas_freezer_inv_position
boolean visible = false
integer x = 549
integer y = 8
integer width = 1623
integer height = 780
integer taborder = 0
string dataobject = "d_pas_freezer_inv_position_report"
end type

on constructor;call u_netwise_dw::constructor;ib_updateable = False

end on

type dw_division from u_division within w_pas_freezer_inv_position
integer y = 104
integer taborder = 10
end type

on constructor;call u_division::constructor;This.disable()
end on

type tab_1 from tab within w_pas_freezer_inv_position
event create ( )
event destroy ( )
integer x = 5
integer y = 220
integer width = 2853
integer height = 1160
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tp_inventory tp_inventory
tp_production tp_production
tp_sales tp_sales
end type

on tab_1.create
this.tp_inventory=create tp_inventory
this.tp_production=create tp_production
this.tp_sales=create tp_sales
this.Control[]={this.tp_inventory,&
this.tp_production,&
this.tp_sales}
end on

on tab_1.destroy
destroy(this.tp_inventory)
destroy(this.tp_production)
destroy(this.tp_sales)
end on

event selectionchanged;String 	ls_firstrow

If IsValid(idw_top_dw) Then
	ls_firstrow = idw_top_dw.Describe("DataWindow.VerticalScrollPosition")
	// This should synchronize the two datawindows' scrolling
	Choose Case newindex
	 	Case 1   // Switched to Inventory
			idw_top_dw = This.tp_inventory.dw_inventory
		Case 2   // Switched to current
			idw_top_dw = This.tp_production.dw_production
		Case 3   // Switched to sales
			idw_top_dw = This.tp_sales.dw_sales
	End Choose
	idw_top_dw.Modify("DataWindow.VerticalScrollPosition = '" + ls_firstrow + "'")	
End If
return 0  // Allow the tabpage selection to change


end event

type tp_inventory from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2816
integer height = 1044
long backcolor = 12632256
string text = "Available Inventory"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
dw_inventory dw_inventory
end type

on tp_inventory.create
this.dw_inventory=create dw_inventory
this.Control[]={this.dw_inventory}
end on

on tp_inventory.destroy
destroy(this.dw_inventory)
end on

type dw_inventory from u_netwise_dw within tp_inventory
integer width = 2816
integer height = 1028
integer taborder = 2
string dataobject = "d_pas_freezer_inv_position_inv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;iw_Parent = w_pas_sales_inv_position

This.Postevent("ue_postconstructor")
end event

event scrollvertical;call super::scrollvertical;idw_top_dw = This
end event

type tp_production from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2816
integer height = 1044
long backcolor = 12632256
string text = "Current Ship"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_production dw_production
end type

on tp_production.create
this.dw_production=create dw_production
this.Control[]={this.dw_production}
end on

on tp_production.destroy
destroy(this.dw_production)
end on

type dw_production from u_netwise_dw within tp_production
integer width = 2775
integer height = 1028
integer taborder = 2
string dataobject = "d_pas_freezer_inv_position_production"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;iw_Parent = w_pas_sales_inv_position

This.Postevent("ue_postconstructor")
end event

event scrollvertical;call super::scrollvertical;idw_top_dw = This
end event

type tp_sales from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2816
integer height = 1044
long backcolor = 12632256
string text = "Sales"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_sales dw_sales
end type

on tp_sales.create
this.dw_sales=create dw_sales
this.Control[]={this.dw_sales}
end on

on tp_sales.destroy
destroy(this.dw_sales)
end on

type dw_sales from u_netwise_dw within tp_sales
integer width = 2811
integer height = 1036
integer taborder = 3
string dataobject = "d_pas_freezer_inv_position_sales"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;iw_Parent = w_pas_sales_inv_position

This.Postevent("ue_postconstructor")
end event

event scrollvertical;call super::scrollvertical;idw_top_dw = This
end event

type dw_freezer_inv_position_header from u_base_dw within w_pas_freezer_inv_position
integer x = 18
integer width = 2715
integer height = 220
integer taborder = 40
string dataobject = "d_pas_freezer_inv_position_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;Integer		li_Counter

IF This.GetColumnName() = "neg_selection" Then
	wf_filter()
End If
If dwo.Name = "boxes_or_loads" Then
	dw_freezer_inv_position_report.SetItem(1,"boxes_or_loads", data)
	SetPointer(HourGlass!)
	If data = 'L' Then
		dw_freezer_inv_position_report.Modify("inv_1_comp.Format='###,##0.0' " + &
			"inv_2_comp.Format='###,##0.0' " + &
			"inv_3_comp.Format='###,##0.0' " + &
			"inv_4_comp.Format='###,##0.0' " + &
			"inv_5_comp.Format='###,##0.0' " + &
			"inv_6_comp.Format='###,##0.0' " + &
			"inv_7_comp.Format='###,##0.0' " + &
			"inv_total_comp.Format='###,##0.0' " + &
			"prod_current_ship_comp.Format='###,##0.0' " + &
			"sales_comp_1.Format='###,##0.0' " + &
			"sales_comp_2.Format='###,##0.0' " + &
			"sales_comp_3.Format='###,##0.0' " + &
			"sales_comp_4.Format='###,##0.0' " + &
			"sales_comp_5.Format='###,##0.0' " + &
			"sales_comp_6.Format='###,##0.0' " + &
			"sales_comp_7.Format='###,##0.0' " + &
			"sales_comp_8.Format='###,##0.0' " + &
			"sales_comp_9.Format='###,##0.0' " + &
			"inventory_sum_comp.Format='###,##0.0' " + &
			"production_sum_comp.Format='###,##0.0' " + &
			"ship_sum_comp.Format='###,##0.0' ")
		tab_1.tp_inventory.dw_inventory.Modify("inv_1_comp.Format='###,##0.0' " + &
			"inv_2_comp.Format='###,##0.0' " + &
			"inv_3_comp.Format='###,##0.0' " + &
			"inv_4_comp.Format='###,##0.0' " + &
			"inv_5_comp.Format='###,##0.0' " + &
			"inv_6_comp.Format='###,##0.0' " + &
			"inv_7_comp.Format='###,##0.0' " + &
			"inv_total_comp.Format='###,##0.0' " + &
			"sum_total_comp.Format='###,##0.0' ")
		tab_1.tp_production.dw_production.Modify("prod_current_ship_comp.Format='###,##0.0' " + &
			"prod_sum_ship_comp.Format='###,##0.0' ")
		tab_1.tp_sales.dw_sales.Modify("sales_comp_1.Format='###,##0.0' " + &
			"sales_comp_2.Format='###,##0.0' " + &
			"sales_comp_3.Format='###,##0.0' " + &
			"sales_comp_4.Format='###,##0.0' " + &
			"sales_comp_5.Format='###,##0.0' " + &
			"sales_comp_6.Format='###,##0.0' " + &
			"sales_comp_7.Format='###,##0.0' " + &
			"sales_comp_8.Format='###,##0.0' " + &
			"sales_comp_9.Format='###,##0.0' ")
	Else
		dw_freezer_inv_position_report.Modify("inv_1_comp.Format='###,##0' " + &
			"inv_2_comp.Format='###,##0' " + &
			"inv_3_comp.Format='###,##0' " + &
			"inv_4_comp.Format='###,##0' " + &
			"inv_5_comp.Format='###,##0' " + &
			"inv_6_comp.Format='###,##0' " + &
			"inv_7_comp.Format='###,##0' " + &
			"inv_total_comp.Format='###,##0' " + &
			"prod_current_ship_comp.Format='###,##0' " + &
			"sales_comp_1.Format='###,##0' " + &
			"sales_comp_2.Format='###,##0' " + &
			"sales_comp_3.Format='###,##0' " + &
			"sales_comp_4.Format='###,##0' " + &
			"sales_comp_5.Format='###,##0' " + &
			"sales_comp_6.Format='###,##0' " + &
			"sales_comp_7.Format='###,##0' " + &
			"sales_comp_8.Format='###,##0' " + &
			"sales_comp_9.Format='###,##0' " + &
			"inventory_sum_comp.Format='###,##0' " + &
			"production_sum_comp.Format='###,##0' " + &
			"ship_sum_comp.Format='###,##0' ")
		tab_1.tp_inventory.dw_inventory.Modify("inv_1_comp.Format='###,##0' " + &
			"inv_2_comp.Format='###,##0' " + &
			"inv_3_comp.Format='###,##0' " + &
			"inv_4_comp.Format='###,##0' " + &
			"inv_5_comp.Format='###,##0' " + &
			"inv_6_comp.Format='###,##0' " + &
			"inv_7_comp.Format='###,##0' " + &
			"inv_total_comp.Format='###,##0' " + &
			"sum_total_comp.Format='###,##0' ")
		tab_1.tp_production.dw_production.Modify("prod_current_ship_comp.Format='###,##0' " + &
			"prod_sum_ship_comp.Format='###,##0' ")
		tab_1.tp_sales.dw_sales.Modify("sales_comp_1.Format='###,##0' " + &
			"sales_comp_2.Format='###,##0' " + &
			"sales_comp_3.Format='###,##0' " + &
			"sales_comp_4.Format='###,##0' " + &
			"sales_comp_5.Format='###,##0' " + &
			"sales_comp_6.Format='###,##0' " + &
			"sales_comp_7.Format='###,##0' " + &
			"sales_comp_8.Format='###,##0' " + &
			"sales_comp_9.Format='###,##0' ")
	End If
End If
end event

on constructor;call u_base_dw::constructor;ib_updateable = False
InsertRow(0)
end on

