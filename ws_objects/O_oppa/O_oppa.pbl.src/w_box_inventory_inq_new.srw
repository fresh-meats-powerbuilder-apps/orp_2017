﻿$PBExportHeader$w_box_inventory_inq_new.srw
forward
global type w_box_inventory_inq_new from w_netwise_response
end type
type dw_uom_code from u_uom_code within w_box_inventory_inq_new
end type
type uo_groups from u_group_list within w_box_inventory_inq_new
end type
type cbx_division from u_netwise_checkbox within w_box_inventory_inq_new
end type
type dw_division from u_netwise_dw within w_box_inventory_inq_new
end type
type dw_start_date from u_netwise_dw within w_box_inventory_inq_new
end type
type rb_quantity from radiobutton within w_box_inventory_inq_new
end type
type rb_weight from radiobutton within w_box_inventory_inq_new
end type
type rb_total_on_hand from radiobutton within w_box_inventory_inq_new
end type
type rb_ready_to_ship_date from radiobutton within w_box_inventory_inq_new
end type
type rb_seven_days from radiobutton within w_box_inventory_inq_new
end type
type rb_eight_days from radiobutton within w_box_inventory_inq_new
end type
type gb_group_weight from groupbox within w_box_inventory_inq_new
end type
type gb_inv_position from groupbox within w_box_inventory_inq_new
end type
type gb_number_of_days from groupbox within w_box_inventory_inq_new
end type
end forward

global type w_box_inventory_inq_new from w_netwise_response
integer x = 151
integer y = 460
integer width = 2263
integer height = 2608
long backcolor = 67108864
dw_uom_code dw_uom_code
uo_groups uo_groups
cbx_division cbx_division
dw_division dw_division
dw_start_date dw_start_date
rb_quantity rb_quantity
rb_weight rb_weight
rb_total_on_hand rb_total_on_hand
rb_ready_to_ship_date rb_ready_to_ship_date
rb_seven_days rb_seven_days
rb_eight_days rb_eight_days
gb_group_weight gb_group_weight
gb_inv_position gb_inv_position
gb_number_of_days gb_number_of_days
end type
global w_box_inventory_inq_new w_box_inventory_inq_new

type variables
Boolean			ib_valid_to_close

w_box_inventory_new		iw_parent

end variables

event open;call super::open;String				ls_division


iw_parent = Message.PowerObjectParm


This.Title = iw_parent.Title + " Inquire"

// ibdkdld 12/12/02 add groups object 
//ole_groups.object.GroupType(1)
//ole_Groups.object.LoadObject()

uo_groups.uf_load_groups('L')






end event

on close;call w_netwise_response::close;If Not ib_valid_to_close Then
	Message.StringParm = ""
End if
end on

on w_box_inventory_inq_new.create
int iCurrent
call super::create
this.dw_uom_code=create dw_uom_code
this.uo_groups=create uo_groups
this.cbx_division=create cbx_division
this.dw_division=create dw_division
this.dw_start_date=create dw_start_date
this.rb_quantity=create rb_quantity
this.rb_weight=create rb_weight
this.rb_total_on_hand=create rb_total_on_hand
this.rb_ready_to_ship_date=create rb_ready_to_ship_date
this.rb_seven_days=create rb_seven_days
this.rb_eight_days=create rb_eight_days
this.gb_group_weight=create gb_group_weight
this.gb_inv_position=create gb_inv_position
this.gb_number_of_days=create gb_number_of_days
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_uom_code
this.Control[iCurrent+2]=this.uo_groups
this.Control[iCurrent+3]=this.cbx_division
this.Control[iCurrent+4]=this.dw_division
this.Control[iCurrent+5]=this.dw_start_date
this.Control[iCurrent+6]=this.rb_quantity
this.Control[iCurrent+7]=this.rb_weight
this.Control[iCurrent+8]=this.rb_total_on_hand
this.Control[iCurrent+9]=this.rb_ready_to_ship_date
this.Control[iCurrent+10]=this.rb_seven_days
this.Control[iCurrent+11]=this.rb_eight_days
this.Control[iCurrent+12]=this.gb_group_weight
this.Control[iCurrent+13]=this.gb_inv_position
this.Control[iCurrent+14]=this.gb_number_of_days
end on

on w_box_inventory_inq_new.destroy
call super::destroy
destroy(this.dw_uom_code)
destroy(this.uo_groups)
destroy(this.cbx_division)
destroy(this.dw_division)
destroy(this.dw_start_date)
destroy(this.rb_quantity)
destroy(this.rb_weight)
destroy(this.rb_total_on_hand)
destroy(this.rb_ready_to_ship_date)
destroy(this.rb_seven_days)
destroy(this.rb_eight_days)
destroy(this.gb_group_weight)
destroy(this.gb_inv_position)
destroy(this.gb_number_of_days)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String					ls_selected_divisions, &
						ls_uom_code, ls_owner, ls_group_id, ls_group_desc 
							
Long					ll_division_rowcount, ll_row		

Integer				li_div_count
		
u_String_Functions		lu_string_functions

If dw_division.AcceptText() = -1 Then 
	dw_division.SetFocus()
	return
End if

If dw_uom_code.AcceptText() = -1 Then
	dw_uom_code.SetFocus()
	Return
End If

If dw_start_date.AcceptText() = -1 Then
	dw_start_date.SetFocus()
	Return
End If

ls_selected_divisions = ''
if cbx_division.Checked = True Then
	li_div_count = 0
	ll_division_rowcount = dw_division.RowCount()
	For ll_row = 1 to ll_division_rowcount
		If dw_division.IsSelected(ll_row) Then
			if len(ls_selected_divisions) = 0 Then
				ls_selected_divisions = mid(dw_division.GetItemString(ll_row, "type_code"),1,2) 
				SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastDivision", ls_selected_divisions)				
			else
				ls_selected_divisions += ',' + mid(dw_division.GetItemString(ll_row, "type_code"),1,2) 
			end if
			li_div_count ++
		End If	
		If li_div_count > 10 Then
			iw_frame.SetMicroHelp("No more than 10 divisions can be selected, or unselect division checkbox for all divisions")
			Return
		End If
	Next	
	if li_div_count = 0 Then
		iw_frame.SetMicroHelp("At least 1 division code must be selected")
		return
	End If
Else
	ls_selected_divisions = 'ALL'
End If

if dw_start_date.GetItemDate(1,"start_date") < today() Then
	iw_frame.SetMicroHelp("Start date must be today or greater")
	dw_start_date.SetFocus()
	return	
end If	
	
ls_uom_code = dw_uom_code.uf_get_uom_code()
If lu_string_functions.nf_IsEmpty(ls_uom_code) Then
	iw_frame.SetMicroHelp("UOM Code is a required field")
	dw_uom_code.SetFocus()
	return
End if
ls_owner = uo_groups.uf_get_owner()
ls_group_id = string(uo_groups.uf_get_sel_id(ls_group_id))
uo_groups.uf_get_sel_desc(ls_group_desc)

iw_parent.event ue_set_data("division_list", ls_selected_divisions)
iw_parent.event ue_set_data("UOM Code", ls_uom_code)
iw_parent.Event ue_set_data('group_system',  ls_owner)
iw_parent.Event ue_set_data('group_description', ls_group_desc)
iw_parent.Event ue_set_data('groupid', ls_group_id)
iw_parent.Event ue_set_data('start_date', String(dw_start_date.GetItemDate(1, "start_date"), "yyyy-mm-dd"))

if cbx_division.Checked Then
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtAllDivsSelected", "N")
else
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtAllDivsSelected", "Y")
end if

If rb_quantity.Checked Then
	iw_parent.Event ue_set_data('display_uom', 'Q')
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtDisplayUOM", "Q")
Else 
	iw_parent.Event ue_set_data('display_uom', 'W')
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtDisplayUOM", "W")
End If

If rb_total_on_hand.Checked Then
	iw_parent.Event ue_set_data('inv_position', 'T')
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtPosition", "T")
Else 
	iw_parent.Event ue_set_data('inv_position', 'S')
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtPosition", "S")
End If

If rb_seven_days.Checked Then
	iw_parent.Event ue_set_data('number_of_days', '7')
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtNumberDays", "7")
Else 
	iw_parent.Event ue_set_data('number_of_days', '8')
	SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InvtNumberDays", "8")
End If
	
ib_valid_to_close = True
CloseWithReturn(This, "True")
end event

event ue_postopen;call super::ue_postopen;String		ls_Division, ls_find_string, ls_division_list, ls_days_checked, ls_display_UOM, ls_inv_position, ls_all_divs
Integer	li_Found
Boolean	lb_first_selected_division

ls_all_divs = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "InvtAllDivsSelected", "N")
If ls_all_divs = 'Y' Then
	cbx_division.Checked = False
	dw_division.Visible = False
Else
	iw_parent.Event ue_get_data("division_list")
	If Message.StringParm = '' Then
		ls_Division = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastDivision", "  ")
		ls_find_string = "type_code = " + "'" + ls_Division + "'"
		li_Found = dw_division.Find(ls_find_string,1,dw_division.RowCount())
		if li_Found > 0 then
			dw_division.SelectRow(li_Found, True)
			dw_division.ScrollToRow(li_Found)
		End If
		cbx_division.Checked = True
	Else
		If Message.StringParm = 'ALL,' Then
			cbx_division.Checked = False
			dw_division.Visible = False
		Else	
			ls_division_list = Message.Stringparm
			lb_first_selected_division = True
			Do Until (ls_division_list = '')
				ls_division = iw_frame.iu_string.nf_gettoken(ls_division_list, ',')
				ls_find_string = "type_code = " + "'" + ls_Division + "'"
				li_Found = dw_division.Find(ls_find_string,1,dw_division.RowCount())
				If li_Found > 0 Then
					dw_division.SelectRow(li_Found, True)
				End If
				if lb_first_selected_division Then
					dw_division.ScrollToRow(li_Found)
					lb_first_selected_division = False
				End If
			Loop	
			cbx_division.Checked = True
		End If
	End If
End If

iw_parent.Event ue_get_data("number_of_days")
If Message.StringParm > ' ' Then
	If Message.StringParm = "8" Then
		rb_eight_days.Checked = True
	Else	
		rb_seven_days.Checked = True
	End If
Else
	ls_days_checked = ProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "InvtNumberDays", " ")
	if ls_days_checked = '8' Then
		rb_eight_days.Checked = True
	Else	
		rb_seven_days.Checked = True
	End If	
End If


iw_parent.Event ue_get_data("display_uom")
If Message.StringParm > ' ' then
	If Message.StringParm = "W" Then
		rb_weight.Checked = True
	Else	
		rb_quantity.Checked = True
	End If
Else
	ls_display_uom = ProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "InvtDisplayUOM", " ")
	If ls_display_UOM = "W" Then
		rb_weight.Checked = True
	Else	
		rb_quantity.Checked = True
	End If		
End If

iw_parent.Event ue_get_data("inv_position")
If Message.StringParm > ' ' Then
	If Message.StringParm = "T" then
		rb_total_on_hand.Checked = True
	Else	
		rb_ready_to_ship_date.Checked = True
	End If
Else
	ls_inv_position = ProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "InvtPosition", " ")
	If ls_inv_position = "T" then
		rb_total_on_hand.Checked = True
	Else	
		rb_ready_to_ship_date.Checked = True
	End If	
End If

iw_parent.Event ue_get_data("start_date")
dw_start_date.SetItem(1, "start_date", Date(Message.StringParm))
end event

type cb_base_help from w_netwise_response`cb_base_help within w_box_inventory_inq_new
integer x = 1376
integer y = 2388
integer taborder = 60
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_box_inventory_inq_new
integer x = 1079
integer y = 2388
integer taborder = 50
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_box_inventory_inq_new
integer x = 782
integer y = 2388
integer taborder = 40
end type

type dw_uom_code from u_uom_code within w_box_inventory_inq_new
integer x = 128
integer y = 628
integer width = 814
integer height = 104
integer taborder = 20
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(true)
end event

type uo_groups from u_group_list within w_box_inventory_inq_new
integer x = 146
integer y = 884
integer taborder = 30
boolean bringtotop = true
end type

on uo_groups.destroy
call u_group_list::destroy
end on

type cbx_division from u_netwise_checkbox within w_box_inventory_inq_new
integer x = 119
integer y = 36
integer width = 302
boolean bringtotop = true
string text = "Division:"
end type

event clicked;call super::clicked;If This.Checked = True Then
	dw_division.Visible = True
Else
	dw_division.Visible = False
End If
end event

type dw_division from u_netwise_dw within w_box_inventory_inq_new
integer x = 466
integer y = 44
integer width = 1714
integer height = 528
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_multi_division"
boolean vscrollbar = true
end type

event constructor;call super::constructor;This.SetTransObject(SQLCA)
This.Retrieve()
is_selection = '2'

end event

type dw_start_date from u_netwise_dw within w_box_inventory_inq_new
integer x = 101
integer y = 748
integer width = 654
integer height = 104
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_start_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type rb_quantity from radiobutton within w_box_inventory_inq_new
integer x = 215
integer y = 1528
integer width = 544
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Quantity"
end type

type rb_weight from radiobutton within w_box_inventory_inq_new
integer x = 210
integer y = 1616
integer width = 699
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Weight in Thousands"
end type

type rb_total_on_hand from radiobutton within w_box_inventory_inq_new
integer x = 206
integer y = 1828
integer width = 489
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Total on Hand"
end type

type rb_ready_to_ship_date from radiobutton within w_box_inventory_inq_new
integer x = 201
integer y = 1900
integer width = 562
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ready to Ship Date"
end type

type rb_seven_days from radiobutton within w_box_inventory_inq_new
integer x = 201
integer y = 2100
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "7"
end type

type rb_eight_days from radiobutton within w_box_inventory_inq_new
integer x = 197
integer y = 2184
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "8"
end type

type gb_group_weight from groupbox within w_box_inventory_inq_new
integer x = 146
integer y = 1460
integer width = 795
integer height = 256
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Display UOM"
end type

type gb_inv_position from groupbox within w_box_inventory_inq_new
integer x = 142
integer y = 1736
integer width = 631
integer height = 272
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Inv Position"
end type

type gb_number_of_days from groupbox within w_box_inventory_inq_new
integer x = 155
integer y = 2024
integer width = 475
integer height = 284
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Number of Days"
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
03w_box_inventory_inq_new.bin 
2800000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000007baba7a001d21e8000000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f3000000007baba7a001d21e807baba7a001d21e8000000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
26ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
13w_box_inventory_inq_new.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
