﻿$PBExportHeader$u_product_state.sru
$PBExportComments$User Object for Product State
forward
global type u_product_state from datawindow
end type
end forward

global type u_product_state from datawindow
integer width = 928
integer height = 72
integer taborder = 10
string dataobject = "d_product_state"
boolean border = false
boolean livescroll = true
end type
global u_product_state u_product_state

forward prototypes
public function long uf_enable (boolean ab_enable)
public function string uf_get_product_state ()
public subroutine uf_set_product_state (string as_product_state)
public function string uf_get_product_state_desc ()
end prototypes

public function long uf_enable (boolean ab_enable);string	ls_temp

If ab_enable Then
// The following line gave us a wierd error for some unknow reason please do not use it
//	This.object.product_state.Background.Color = '16777215'
	this.setitem(1,"enable_product_state","T")

//	This.object.product_state.Protect = 0
Else
// The following line gave us a wierd error for some unknow reason please do not use it
//	This.object.product_state.Background.Color = '12632256'
	this.setitem(1,"enable_product_state","F")
//	This.object.product_state.Protect = '1'
End If

Return 1
end function

public function string uf_get_product_state ();return This.GetItemString(1, "product_state")
end function

public subroutine uf_set_product_state (string as_product_state);This.SetItem(1,"product_state",as_product_state)
end subroutine

public function string uf_get_product_state_desc ();Datawindowchild			ldwc_temp

String						ls_prod_state

Long							ll_findrow

u_string_functions		lu_string


ls_prod_state = This.GetItemString(1, 'product_state')

If lu_string.nf_IsEmpty(ls_prod_state) Then Return ''

This.GetChild('product_state', ldwc_temp)
ll_findrow = ldwc_temp.Find('type_code = "' + ls_prod_state + '"', 1, ldwc_temp.RowCount())
If ll_findrow <= 0 Then Return ''

Return ldwc_temp.GetItemString(ll_findrow, 'type_short_desc')


end function

event constructor;DataWindowChild		ldwc_type


This.GetChild("product_state", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTATE")
ldwc_type.SetSort("type_code")
ldwc_type.Sort()

This.InsertRow(0)
end event

on u_product_state.create
end on

on u_product_state.destroy
end on

