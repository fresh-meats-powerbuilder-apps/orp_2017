﻿$PBExportHeader$w_res_util_inq_resp.srw
$PBExportComments$Inquire resonse window for reservation utilation windows
forward
global type w_res_util_inq_resp from w_base_response_ext
end type
type dw_res_util_inq from u_netwise_dw within w_res_util_inq_resp
end type
end forward

global type w_res_util_inq_resp from w_base_response_ext
int X=14
int Y=117
int Width=2140
int Height=881
boolean TitleBar=true
string Title=""
long BackColor=12632256
dw_res_util_inq dw_res_util_inq
end type
global w_res_util_inq_resp w_res_util_inq_resp

type variables
Boolean		ib_GotTSR

Window		iw_parent

u_cfm002		iu_cfm002
end variables

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_customer

String				ls_modify_string,&
						ls_salesman, &
						ls_salesman_code, &
						ls_visible, &
						ls_rtn, &
						ls_customer

u_string_functions		lu_string_functions			

Choose Case iw_parent.ClassName()

Case	"w_resutl_tsr_customer"
	dw_res_util_inq.Modify('all_customer.visible = 1 ' + &
									'white_rec.visible = 0 ' + &
									'gray_rec.visible = 0 ' + &
									'black_rec.visible = 0 ' + &
									'bgray_rec.visible = 0 ')

	ls_rtn = dw_res_util_inq.Describe('all_reservation.visible')
	lu_string_functions.nf_ParseLeftRight(ls_rtn, "~t", ls_rtn, ls_visible)
	ls_visible = Left(ls_visible, (len(ls_visible) - 1))
	IF ls_visible = '1' then
		dw_res_util_inq.Modify('all_reservation.visible = 0 ' + &
										'reservation_number.visible = 0 ' + &
										'reservation_number_t.visible = 0')
	End IF

	ls_rtn = dw_res_util_inq.Describe('all_products.visible')
	lu_string_functions.nf_ParseLeftRight(ls_rtn, "~t", ls_rtn, ls_visible)
	ls_visible = Left(ls_visible, (len(ls_visible) - 1))
	IF ls_visible = '1' then
		dw_res_util_inq.Modify('all_product.visible = 0 ' + &
										'product_code.visible = 0 ' + &
										'product_code_t.visible = 0')
	End IF

	dw_res_util_inq.SetItem( 1, "all_customer", "N")
	dw_res_util_inq.SetItem( 1, "all_reservation", "Y")
	dw_res_util_inq.object.all_customer.Protect = 0 
	
Case	"w_resutl_custres"

		dw_res_util_inq.Modify('all_customer.visible = 0 ' + &
										'white_rec.visible = 1 ' + &
										'gray_rec.visible = 1 ' + &
										'black_rec.visible = 1 ' + &
										'bgray_rec.visible = 1 ')

	ls_rtn = dw_res_util_inq.Describe('all_reservation.visible')
	lu_string_functions.nf_ParseLeftRight(ls_rtn, "~t", ls_rtn, ls_visible)
	ls_visible = Left(ls_visible, (len(ls_visible) - 1))
	IF ls_visible = '0' then
		dw_res_util_inq.Modify('all_reservation.visible = 1 ' + &
										'reservation_number.visible = 1 ' + &
										'reservation_number_t.visible = 1')
	End IF

	ls_rtn = dw_res_util_inq.Describe('all_products.visible')
	lu_string_functions.nf_ParseLeftRight(ls_rtn, "~t", ls_rtn, ls_visible)
	ls_visible = Left(ls_visible, (len(ls_visible) - 1))
	IF ls_visible = '1' then
		dw_res_util_inq.Modify('all_products.visible = 0 ' + &
										'product_code.visible = 0 ' + &
										'product_code_t.visible = 0')
	End IF

	dw_res_util_inq.SetItem( 1, "all_customer", "N")
	dw_res_util_inq.SetItem( 1, "all_reservation","N")
	dw_res_util_inq.SetItem( 1, "all_products", "Y")
	dw_res_util_inq.object.all_customer.Protect = 1

Case	"w_resutl_res_product"
		dw_res_util_inq.Modify('all_customer.visible = 0 ' + &
										'white_rec.visible = 1 ' + &
										'gray_rec.visible = 1 ' + &
										'black_rec.visible = 1 ' + &
										'bgray_rec.visible = 1 ')

	ls_rtn = dw_res_util_inq.Describe('all_reservation.visible')
	lu_string_functions.nf_ParseLeftRight(ls_rtn, "~t", ls_rtn, ls_visible)
	ls_visible = Left(ls_visible, (len(ls_visible) - 1))

	IF ls_visible = '0' then
		dw_res_util_inq.Modify('all_reservation.visible = 1 ' + &
										'reservation_number.visible = 1 ' + &
										'reservation_number_t.visible = 1 ')
	End IF
	ls_rtn = dw_res_util_inq.Describe('all_products.visible')
	lu_string_functions.nf_ParseLeftRight(ls_rtn, "~t", ls_rtn, ls_visible)
	ls_visible = Left(ls_visible, (len(ls_visible) - 1))

	IF ls_visible = '0' then
		dw_res_util_inq.Modify('all_products.visible = 1 ' + &
									'product_code.visible = 1 ' + &
									'product_code_t.visible = 1 ')
	End IF
	//sem
	If dw_res_util_inq.GetItemString(1, 'all_customer') <> "R" Then
		dw_res_util_inq.SetItem( 1, "all_customer", "N")
	End If
		dw_res_util_inq.SetItem( 1, "all_reservation", "N")
		dw_res_util_inq.SetItem( 1, "all_products", "N")
		dw_res_util_inq.object.all_customer.Protect = 1
END Choose

Message.StringParm = ''
//dw_res_util_inq.SetRedraw(False)

iw_parent.TriggerEvent("ue_GetRetrievalData")
ls_modify_String = Message.StringParm
IF Not iw_frame.iu_string.nf_AmIEmpty( ls_modify_String) Then 
	dw_res_util_inq.Reset()
	dw_res_util_inq.ImportString(ls_modify_string)
	ls_salesman_code = dw_res_util_inq.GetItemString(1, 'tsr_code')
Else
	dw_res_util_inq.SetItem( 1, "month", Month(Today()))
	dw_res_util_inq.SetItem( 1, "year", Year(Today()))
	dw_res_util_inq.SetItem( 1, "week", 'A')
	dw_res_util_inq.SetItem( 1, "util_percent",100)
	message.stringparm = ""
	message.TriggerEvent("ue_getsmancode")
	ls_salesman_code = message.stringparm
	dw_res_util_inq.SetItem( 1, 'tsr_code', ls_salesman_code)
END IF

If Not lu_string_functions.nf_IsEmpty(ls_salesman_code) Then
	SELECT 	salesman.smanname
	INTO 		:ls_salesman
	FROM 		salesman  
	WHERE 	salesman.smancode = :ls_salesman_code AND salesman.smantype = 's';
	dw_res_util_inq.object.smanname.text =  ls_salesman
End If

// Go Get Customer Data
dw_res_util_inq.GetChild('billto_id', ldwc_customer)
iw_frame.iu_project_functions.ids_billtos.ShareData(ldwc_customer)

dw_res_util_inq.GetChild('Corp_id', ldwc_customer)
iw_frame.iu_project_functions.ids_corps.ShareData(ldwc_customer)

dw_res_util_inq.GetChild('customer_id', ldwc_customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

ls_customer = ldwc_customer.Describe("datawindow.data")

dw_res_util_inq.SetItem(1, 'location', message.is_smanlocation)

IF dw_res_util_inq.GetItemString(1, "all_customer") = "Y" Then 
	dw_res_util_inq.SetItem(1, "customer_id", "")
End IF

IF dw_res_util_inq.GetItemString(1, "all_reservation") = "Y" Then 
	dw_res_util_inq.SetItem(1, "reservation_number", "")
	dw_res_util_inq.object.reservation_number.Protect= 1
	dw_res_util_inq.object.reservation_number.Background.Color = 12632256
End If

IF dw_res_util_inq.GetItemString(1, "all_products") = "Y" Then 
	dw_res_util_inq.SetItem(1, "product_code", "")
	dw_res_util_inq.object.product_code.Protect= 1
	dw_res_util_inq.object.product_code.Background.Color= 12632256
End If

dw_res_util_inq.SetColumn('week')
dw_res_util_inq.SetFocus()
//dw_res_util_inq.SetRedraw(True)

ls_customer = ldwc_customer.Describe("datawindow.data")
Return
end event

event open;call super::open;
iw_Parent = Message.PowerObjectParm

This.Title = iw_Parent.Title + " Inquire"
  


end event

on w_res_util_inq_resp.create
int iCurrent
call w_base_response_ext::create
this.dw_res_util_inq=create dw_res_util_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_res_util_inq
end on

on w_res_util_inq_resp.destroy
call w_base_response_ext::destroy
destroy(this.dw_res_util_inq)
end on

event ue_base_ok;call super::ue_base_ok;DataWindowChild		ldwc_temp

String	ls_temp

u_string_functions		lu_string_functions			

If dw_res_util_inq.AcceptText() < 0 Then Return

IF lu_string_functions.nf_IsEmpty(dw_res_util_inq.GetItemString( 1, "tsr_code")) Then
	MessageBox("TSR Required", "You must specify a TSR")
	Return
END IF
	
Choose Case iw_parent.ClassName()

Case	"w_resutl_custres"
	IF dw_res_util_inq.GetItemString( 1, "All_Reservation") = 'N' AND &
		lu_string_functions.nf_IsEmpty( dw_res_util_inq.GetItemString( 1, "Reservation_number")) Then
		MessageBox(	"Input Reservation Number", "You are required " + &
			"to specify a Reservation or check All Reservations.")
	Return
	ELSE
		IF	dw_res_util_inq.GetItemString( 1, "All_Reservation") = 'Y' AND &
				lu_string_functions.nf_IsEmpty( dw_res_util_inq.GetItemString( 1, "Customer_id" )) &
				Then
			MessageBox(	"Input Customer ID", "You are required to " + &
					"specify a Customer ID, If you check All Reservations.")
			Return
		END IF
	END IF

Case	"w_resutl_tsr_customer"
	IF dw_res_util_inq.GetItemString( 1, "all_customer") = 'N' AND &
			lu_string_functions.nf_IsEmpty(dw_res_util_inq.GetItemString( 1, "customer_id")) Then
		MessageBox("Input customer", "You are required to specify a " + &
				"customer or check All customers.")
		Return
	End if 

Case	"w_resutl_res_product"
	IF dw_res_util_inq.GetItemString( 1, "all_reservation") = 'N' AND&
			lu_string_functions.nf_IsEmpty( dw_res_util_inq.GetItemString( 1, "reservation_number")) Then
		MessageBox(	"Input Reservation Number", "You are required to " + &
				"specify a Reservation or check All Reservations.")
		Return
	END IF

	IF dw_res_util_inq.GetItemString( 1, "all_reservation") = 'Y' AND &
			lu_string_functions.nf_IsEmpty( dw_res_util_inq.GetItemString( 1, "customer_id")) Then
		MessageBox(	"Input Customer Id.", "You are required to " + &
				"specify a Customer Id.")
		Return
	END IF

	IF dw_res_util_inq.GetItemString( 1, "all_Products") = 'N' AND&
			lu_string_functions.nf_IsEmpty( dw_res_util_inq.GetItemString( 1, "Product_code")) Then
		MessageBox(	"Input Product Code", "You are required to specify " + &
				"a Product or check All Products")
		Return
	END IF

END CHOOSE

dw_res_util_inq.GetChild("customer_id", ldwc_temp)
ls_temp = ldwc_temp.Describe("datawindow.data")

ls_temp = dw_res_util_inq.Describe("DataWindow.Data")
CloseWithReturn(This, dw_res_util_inq.Describe("DataWindow.Data"))
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, "Abort")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_res_util_inq_resp
int X=1198
int Y=617
int Width=298
string FaceName="Arial"
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_res_util_inq_resp
int X=865
int Y=617
int Width=298
string Text="Cancel"
string FaceName="Arial"
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_res_util_inq_resp
int X=535
int Y=617
int Width=298
string Text="OK"
string FaceName="Arial"
end type

type dw_res_util_inq from u_netwise_dw within w_res_util_inq_resp
event ue_dwndropdown pbm_dwndropdown
int X=10
int Y=25
int Width=2067
int Height=565
int TabOrder=10
string DataObject="d_res_util_inq"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event itemchanged;call super::itemchanged;Long					ll_row, &
						ll_rowfound

STRING				ls_WindowName,&
						ls_ColumnName, &
						ls_findexpression

DataWindowChild	ldw_childdw

u_string_functions		lu_string_functions

ls_WindowName = Parent.iw_parent.ClassName()
ls_ColumnName = dwo.name

Choose Case ls_WindowName
	Case	"w_resutl_custres"
		IF ls_ColumnName = "all_reservation" Then
			IF data = 'N' Then
				dw_res_util_inq.SetItem( 1, "Customer_id", "")
			Else
				dw_res_util_inq.SetItem( 1, "Customer_id", "")
				IF ls_ColumnName = "all_customer" Then
					dw_res_util_inq.SetItem( 1, "Customer_id", "")
				END IF
			END IF
		END IF
	Case	"w_resutl_tsr_customer"
		IF ls_ColumnName = "all_customer" Then
			dw_res_util_inq.SetItem( 1, "Customer_id", "")
		END IF
	Case	"w_resutl_res_product"
		IF ls_ColumnName = "all_reservation" Then
			IF data = 'N' Then
				dw_res_util_inq.SetItem( 1, "Customer_id", "")
			Else
				dw_res_util_inq.SetItem( 1, "Customer_id", "")
				IF ls_ColumnName = "all_customer" Then
					dw_res_util_inq.SetItem( 1, "Customer_id", "")
				END IF
			END IF
		END IF
END CHOOSE

Choose Case ls_ColumnName 
Case "tsr_code"
		This.GetChild('tsr_code', ldw_childdw)
		ls_FindExpression = "smancode = ~"" + Trim(data) + "~""
		ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
		If  ll_RowFound <= 0 Then 
			MessageBox('Invalid Data', data + ' is an invalid SalesMan Code')
			This.SelectText(1, Len(data))
			RETURN	1
		ELSE
			This.object.smanname.text = ldw_childdw.GetItemString(ll_rowfound, 'smanname')
		END IF
Case "month"
	IF Integer(data) < 1 or Integer(data) > 12 Then
		MessageBox("Month", "The Month must be a valid Month from 1 to 12.")
		Return 1
	End IF	
Case "year"
	IF Integer(data) < 1900 or Integer(data) > 3000 then 
		MessageBox("Year", "The Year must be a valid Year from 1900 to 3000")
		Return 1
	End IF
Case "util_percent"
	IF Long(data) < 0 or Long(data) > 100 Then
		MessageBox("Percent Utilized", "The Percent Utilized must be " + &
				"between 1 and 100.")
		Return 1
	End IF
Case "all_reservation" 
	IF data = 'Y' Then
		dw_res_util_inq.SetItem( 1, "reservation_number", "")
		dw_res_util_inq.object.reservation_number.Protect = 1 
		dw_res_util_inq.object.reservation_number.BackGround.Color = 12632256
	Else
		dw_res_util_inq.SetItem( 1, "reservation_number", "")
		dw_res_util_inq.object.reservation_number.Protect = 0 
		dw_res_util_inq.object.reservation_number.BackGround.Color = 16777215
	END IF
Case  "all_products" 
	IF data = 'Y' Then
		dw_res_util_inq.SetItem( 1, "product_code", "")
		dw_res_util_inq.object.product_code.Protect = 1 
		dw_res_util_inq.object.product_code.BackGround.Color = 12632256
	Else
		dw_res_util_inq.SetItem( 1, "product_code", "")
				dw_res_util_inq.object.product_code.Protect = 0 
				dw_res_util_inq.object.product_code.BackGround.Color = 16777215
	END IF
CASE 'customer_type'
	This.SetItem(1, 'customer_id', "")
Case 'corp_id'
	If lu_string_functions.nf_IsEmpty(data) Then return 
	Select	count(*)
	into		:ll_row
	From		corpcust
	Where		corpcust.corp_id = :data
	Using		SQLCA;
	if ll_row < 1 then
		This.SelectText(1, Len(data))
		dw_res_util_inq.SetFocus()
		iw_frame.SetMicroHelp('This is a Invalid Customer.')
		Return 1
	End If
Case 'billto_id'
	If lu_string_functions.nf_IsEmpty(data) Then return 
	Select	count(*)
	into		:ll_row
	From		billtocust
	Where		billtocust.bill_to_id = :data
	Using		SQLCA;
	if ll_row < 1 then
		This.SelectText(1, Len(data))
		dw_res_util_inq.SetFocus()
		iw_frame.SetMicroHelp('This is a Invalid Customer.')
		Return 1
	End If	
Case 'customer_id'
	If lu_string_functions.nf_IsEmpty(data) Then return 
	Select	count(*)
	into		:ll_row
	From		customers
	Where		customers.customer_id = :data
	Using		SQLCA;
	if ll_row < 1 then
		This.SelectText(1, Len(data))
		dw_res_util_inq.SetFocus()
		iw_frame.SetMicroHelp('This is a Invalid Customer.')
		Return 1
	End If
End Choose
iw_frame.SetMicroHelp('Ready')
end event

event constructor;call super::constructor;DataWindowChild		ldwc_temp

String					ls_location

u_string_functions		lu_string_functions

dw_res_util_inq.GetChild( "tsr_code",ldwc_temp) 

ls_location = message.is_smanlocation

IF Not iw_frame.iu_project_functions.nf_issalesmgr() Then
	If Not lu_string_functions.nf_IsEmpty(ls_location) Then
		iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, ls_location)
	End If
Else	
	iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, '')
End IF
This.InsertRow(0)
end event

event itemerror;call super::itemerror;Return 1

end event

