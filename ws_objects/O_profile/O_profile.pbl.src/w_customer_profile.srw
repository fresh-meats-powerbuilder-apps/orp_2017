﻿$PBExportHeader$w_customer_profile.srw
forward
global type w_customer_profile from w_base_sheet_ext
end type
type tab_detail from tab within w_customer_profile
end type
type tp_service_center from userobject within tab_detail
end type
type dw_service_center_detail from u_base_dw within tp_service_center
end type
type tp_service_center from userobject within tab_detail
dw_service_center_detail dw_service_center_detail
end type
type tp_contact from userobject within tab_detail
end type
type pb_new from picturebutton within tp_contact
end type
type pb_last from picturebutton within tp_contact
end type
type pb_next from picturebutton within tp_contact
end type
type pb_previous from picturebutton within tp_contact
end type
type pb_first from picturebutton within tp_contact
end type
type dw_contact from u_base_dw_ext within tp_contact
end type
type tp_contact from userobject within tab_detail
pb_new pb_new
pb_last pb_last
pb_next pb_next
pb_previous pb_previous
pb_first pb_first
dw_contact dw_contact
end type
type tp_location from userobject within tab_detail
end type
type dw_location from u_base_dw_ext within tp_location
end type
type dw_include_exclude_indicator from u_base_dw_ext within tp_location
end type
type tp_location from userobject within tab_detail
dw_location dw_location
dw_include_exclude_indicator dw_include_exclude_indicator
end type
type tp_carrier from userobject within tab_detail
end type
type dw_carrier from u_base_dw_ext within tp_carrier
end type
type tp_carrier from userobject within tab_detail
dw_carrier dw_carrier
end type
type tp_prodsub from userobject within tab_detail
end type
type dw_product from u_base_dw_ext within tp_prodsub
end type
type tp_prodsub from userobject within tab_detail
dw_product dw_product
end type
type tp_product_age from userobject within tab_detail
end type
type dw_product_age from u_base_dw_ext within tp_product_age
end type
type tp_product_age from userobject within tab_detail
dw_product_age dw_product_age
end type
type tp_temp_recorder from userobject within tab_detail
end type
type dw_temp_recorder_dtl from u_base_dw_ext within tp_temp_recorder
end type
type dw_temp_recorder_hdr from u_base_dw_ext within tp_temp_recorder
end type
type tp_temp_recorder from userobject within tab_detail
dw_temp_recorder_dtl dw_temp_recorder_dtl
dw_temp_recorder_hdr dw_temp_recorder_hdr
end type
type tp_product_cat from userobject within tab_detail
end type
type dw_product_category from u_base_dw_ext within tp_product_cat
end type
type tp_product_cat from userobject within tab_detail
dw_product_category dw_product_category
end type
type tp_category from userobject within tab_detail
end type
type dw_category from u_base_dw_ext within tp_category
end type
type tp_category from userobject within tab_detail
dw_category dw_category
end type
type tp_temp_settings from userobject within tab_detail
end type
type dw_temp_settings from u_base_dw_ext within tp_temp_settings
end type
type tp_temp_settings from userobject within tab_detail
dw_temp_settings dw_temp_settings
end type
type tp_loading_platform from userobject within tab_detail
end type
type dw_loading_platform from u_base_dw_ext within tp_loading_platform
end type
type tp_loading_platform from userobject within tab_detail
dw_loading_platform dw_loading_platform
end type
type tp_programs from userobject within tab_detail
end type
type dw_programs from u_base_dw_ext within tp_programs
end type
type tp_programs from userobject within tab_detail
dw_programs dw_programs
end type
type tab_detail from tab within w_customer_profile
tp_service_center tp_service_center
tp_contact tp_contact
tp_location tp_location
tp_carrier tp_carrier
tp_prodsub tp_prodsub
tp_product_age tp_product_age
tp_temp_recorder tp_temp_recorder
tp_product_cat tp_product_cat
tp_category tp_category
tp_temp_settings tp_temp_settings
tp_loading_platform tp_loading_platform
tp_programs tp_programs
end type
type dw_header from u_base_dw_ext within w_customer_profile
end type
end forward

global type w_customer_profile from w_base_sheet_ext
integer x = 59
integer y = 32
integer width = 4873
integer height = 1956
string title = "Customer Profile"
event ue_getdddw pbm_custom02
event ue_set_excl_plants ( string as_data_item,  string as_value )
tab_detail tab_detail
dw_header dw_header
end type
global w_customer_profile w_customer_profile

type variables
GraphicObject		itp_ActiveTab
s_error		istr_error_info

u_cfm001		iu_cfm001

u_orp001		iu_orp001

u_ws_orp1		iu_ws_orp1
u_ws_orp2       iu_ws_orp2
u_ws_orp3       iu_ws_orp3
u_ws_orp5       iu_ws_orp5

Datawindowchild 	idwc_from_date, &
						idwc_to_date, &
						idwc_from_day, &
						idwc_to_day
						
// pjm 08/15/2014 added for excluded plants 
private:
window iw_this
string is_selected_plants, is_description, is_message
integer ii_dddw_x, &
		  ii_dddw_y
long	il_selected_row, il_max_excl_row=0, il_xpos, il_ypos
boolean ib_modified, ib_open, ib_contact = false
// pjm 08/15/2014 added for new service cfmc36br
datastore	ids_exclude_plants, &
				ids_product_category, &
				ids_prod_cat_cool_code_label
// pjm 09/14/2014 added for new 'D' String
string is_prod_category="", is_cool_code=""

integer ii_cont_id, row_count

w_tooltip_expanded	iw_tooltip


		  

 

end variables

forward prototypes
public function boolean wf_getcarrierdata (ref datawindowchild adwc_carrier)
public subroutine wf_delete ()
public subroutine wf_insertrow (datawindow adw_name, long al_row)
public subroutine wf_filenew ()
public subroutine wf_filldddw (ref datawindowchild ldw_childdw)
public function boolean wf_checkrights ()
public function boolean wf_retrieve ()
public subroutine wf_initialize_temp_row (datawindow adw_name, long al_row)
public function integer wf_validate_month_day (datawindow adw_name, long al_row)
public function string wf_get_desc (string data)
public function boolean wf_update ()
public subroutine wf_delete_excluded_plants (integer ai_row)
public function long wf_get_max_excl_row ()
public subroutine wf_set_max_excl_row (long al_max_excl_row)
public subroutine wf_process_cust_excl_string (ref string as_cust_excl_string)
public subroutine wf_get_selected_plants ()
public subroutine wf_set_prod_cat ()
public subroutine wf_set_label_excl_plant_descs ()
public subroutine wf_set_selected_plants ()
public subroutine wf_process_cool_code_string (string as_cool_code_str)
public function integer wf_fill_labels_dddw ()
public function integer wf_fill_cool_code_dddw ()
public subroutine wf_build_update_string (ref string as_input_string)
public function integer wf_check_loading_platform_update ()
public function integer wf_check_loading_platform_delete ()
public function boolean wf_check_dup_programs ()
public function boolean wf_contact_has_multiples (string as_contact_type)
end prototypes

on ue_getdddw;call w_base_sheet_ext::ue_getdddw;DataWindowChild			ldwc_temp


dw_header.GetChild('customer_id', ldwc_temp)
Message.PowerObjectParm = ldwc_temp
end on

event ue_set_excl_plants(string as_data_item, string as_value);choose case as_data_item
	case "selected"
		is_selected_plants = as_value
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
end choose
end event

public function boolean wf_getcarrierdata (ref datawindowchild adwc_carrier);integer                 li_rtn
								
u_otr002						lu_otr002

string                  ls_CarrierInfo, &
                        ls_CarrierType, &
		               	ls_CarrierStatus, &
               			ls_Refetch_CarrierCode

s_error                 lstr_Error_Info

IF Date(ProfileString(iw_frame.is_userini, "System Settings", &
			"CarrierDate", String(RelativeDate(Today(), -1),  "mm/dd/yy"))) < Today() &
			Or Not FileExists(iw_frame.is_WorkingDir + "carrier.dbf") THEN

   ls_Refetch_CarrierCode = Space(4)
 	ls_CarrierInfo = Space(2101)
	ls_CarrierType = " "
   ls_CarrierStatus = " "
  
	lu_otr002 = Create u_otr002

//   li_rtn = lu_otr002.nf_otrt22ar(ls_CarrierType, ls_CarrierStatus, &
//            ls_CarrierInfo, ls_Refetch_CarrierCode, lstr_Error_Info,0)
   
// Check the return code from the above function call
	If li_rtn <> 0 Then
 	//	li_RPCrtn = Message.nf_CheckRPCError(li_rtn, lstr_ErrorInfo, 0)
	End If

	li_rtn = adwc_carrier.ImportString(trim(ls_CarrierInfo))

	Do While li_rtn = 100
		ls_CarrierInfo= Space(2101)
	
		ls_Refetch_CarrierCode = &
		adwc_carrier.GetItemString(adwc_carrier.RowCount(), "carrier_code")
//
//		li_rtn = lu_otr002.nf_otrt22ar(ls_CarrierType, ls_CarrierStatus, &
//               ls_CarrierInfo, ls_Refetch_CarrierCode, lstr_Error_Info,0)
 
		li_rtn = adwc_carrier.ImportString(Trim(ls_carrierInfo))

	Loop

	If adwc_carrier.RowCount() > 0 Then
		// Write out todays date to the file
		// Don't write anything if there is no data
		li_rtn = SetProfileString(iw_frame.is_userini, "System Settings", &
							"CarrierDate", String(Today(),"MM/DD/YY"))
		adwc_carrier.SaveAs(iw_frame.is_WorkingDir + "carrier.dbf",DBASE3!,TRUE)
	End if

	Destroy lu_otr002

ELSE
// Import the file from local disk
	   adwc_carrier.ImportFile(iw_frame.is_WorkingDir+"carrier.dbf")
END IF

adwc_carrier.Sort()

iw_frame.SetMicroHelp(iw_frame.ia_Application.MicroHelpDefault)

return true

 
end function

public subroutine wf_delete ();if IsValid(w_locations_popup) then close(w_locations_popup)

tab_detail.PostEvent("ue_delete")
end subroutine

public subroutine wf_insertrow (datawindow adw_name, long al_row);Long						ll_new_row
u_project_functions	lu_project_functions

ll_new_row = adw_name.InsertRow(al_row)
if adw_name.DataObject = 'd_customer_profile_product_sub' then
	if lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() or &
			lu_project_functions.nf_issvcpresident() then
		adw_name.SetItem(ll_new_row, "accepted_ind", "Y")
	else
		adw_name.SetItem(ll_new_row, "accepted_ind", "N")
	end if
end if

//if adw_name.DataObject <> 'd_temp_recorder_dtl' then
//	adw_name.SetItem(ll_new_row, "effective_date", RelativeDate(Today(), 1))
//end if
//
//adw_name.SetItem(ll_New_Row, 'update_flag', 'N')
adw_name.ScrollToRow(ll_New_Row)
adw_name.SetColumn(1)

end subroutine

public subroutine wf_filenew ();MessageBox("NEW", "If you want to setup a new customer profile please inquire")
end subroutine

public subroutine wf_filldddw (ref datawindowchild ldw_childdw);IF ldw_childdw.RowCount()	> 0 THEN RETURN
ldw_childdw.SetTransObject(SQLCA)
ldw_childdw.Retrieve()
end subroutine

public function boolean wf_checkrights ();string					lstrCustomersLocation,lstrCustomerID
u_project_functions	lu_project_functions
		
if lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
	RETURN true
else
	lstrCustomerID = dw_header.getitemstring(1,"customer_id")
	if isnull(lstrCustomerID) then
		RETURN false	
	else
		
		SELECT sales_location
		INTO :lstrCustomersLocation
		FROM customer_defaults  
		WHERE customer_id = :lstrCustomerID    
		  AND sales_location = :Message.is_smanlocation
		USING SQLCA ;
		
		if isnull(lstrCustomersLocation) then	
			MessageBox ("Warning", "Your location does not match the customers location.~r~Save not performed")
			//MessageBox("Warning", "Your location does not match the orders location.~r~nYou may not look at its detail")
			RETURN false
		else
			if lstrCustomersLocation = '' Then
				MessageBox ("Warning", "Your location does not match the customers location.~r~Save not performed")
				//MessageBox("Warning", "Your location does not match the orders location.~r~nYou may not look at its detail")
				RETURN false
			else
				RETURN true
			end if
		end if
	end if
end if
end function

public function boolean wf_retrieve ();String						ls_return
u_string_functions		lu_string_functions

This.TriggerEvent(CloseQuery!)

IF Message.ReturnValue = 1 Then
	Return False
ELSE
	OpenWithParm(w_customer_profile_inq, dw_header.Describe("DATAwINDOW.dATA"))
	IF message.StringParm = 'ABORT' Then 
		Return False
	ELSE
		ls_return = Message.StringParm
		If lu_string_functions.nf_isempty(ls_return) Then return False
		dw_header.Reset()
		tab_detail.tp_prodsub.dw_product.Reset()
		
		//tab_detail.tp_contact.ole_cust_contact.object.Clear()
		tab_detail.tp_contact.dw_contact.Reset()
		//tab_detail.tp_contact.ole_cust_contact.object.ContactType = ""
	 	//tab_detail.tp_contact.dw_contact.object.ContactType = ""
		
		tab_detail.tp_location.dw_location.Reset()
		tab_detail.tp_carrier.dw_carrier.Reset()
		tab_detail.tp_service_center.dw_service_center_detail.reset()
		tab_detail.tp_loading_platform.dw_loading_platform.reset()		
	
		dw_header.ImportString(ls_return)
		
		if lu_string_functions.nf_IsEmpty(dw_header.getitemstring(1,'division')) then
			dw_header.object.division.visible = 0
			dw_header.object.t_service_center_header.visible = 0
			//temp
	//		tab_detail.tp_contact.ole_cust_contact.visible = False
			tab_detail.tp_contact.dw_contact.visible = False
			
		else
			dw_header.object.division.visible = 1
			dw_header.object.t_service_center_header.visible = 1
			tab_detail.tp_service_center.enabled = true	
	//		tab_detail.tp_contact.ole_cust_contact.visible = True
			tab_detail.tp_contact.visible = True
			if lu_string_functions.nf_IsEmpty(dw_header.getitemstring(1,'contact_type')) then
	//			tab_detail.tp_contact.ole_cust_contact.visible = False
	//			tab_detail.tp_contact.visible = False
				tab_detail.tp_contact.dw_contact.visible = False
			else
	//			tab_detail.tp_contact.ole_cust_contact.visible = True
				tab_detail.tp_contact.dw_contact.visible = True
			end if

		end if
		
		if (tab_detail.selectedtab = 1 or tab_detail.selectedtab = 2) &
		and lu_string_functions.nf_IsEmpty(dw_header.getitemstring(1,'division')) then
			tab_detail.SelectTab(3)
		end if

		if dw_header.getitemstring(1,"customer_type") = "C" or dw_header.getitemstring(1,"customer_type") = "B" then
			tab_detail.selecttab(3)
		end if


		tab_detail.TriggerEvent("ue_retrieve")
		
		return True
	END IF
End IF
end function

public subroutine wf_initialize_temp_row (datawindow adw_name, long al_row);Long						ll_new_row
u_project_functions	lu_project_functions

adw_name.SetItem(al_row, 'update_flag', 'A')
adw_name.SetItem(al_row, 'complex_code', '')
adw_name.SetItem(al_row, 'prod_state', '')
adw_name.SetItem(al_row, 'setting_type', '')

end subroutine

public function integer wf_validate_month_day (datawindow adw_name, long al_row);Boolean					lb_error
Integer					li_dup_row, ll_rowCount, li_loopCount, & 
						   li_from_month, li_from_day, li_from_year, & 
							li_to_month,   li_to_day,   li_to_year, &
							li_from_month_dup, li_from_day_dup, li_from_year_dup, & 
							li_to_month_dup,   li_to_day_dup,   li_to_year_dup, &
							li_curr_year
Date                 ld_from, ld_to, ld_to_current	, ld_from_dup, ld_to_dup, ld_to_current_dup				
String					ls_Find



li_curr_year = Year (Today() )

u_project_functions	lu_project_functions

li_dup_row = 0
ls_Find = "complex_code = '" + adw_name.GetItemString(al_row, 'complex_code') + &
					"' and prod_state = '" + adw_name.GetItemString(al_row, 'prod_state') + &
					"' and setting_type = '" + adw_name.GetItemString(al_row, 'setting_type')+ "'"

ll_RowCount = adw_name.RowCount()
If al_row < ll_rowCount Then
	li_loopCount = 0
	
	//start date parsing for current record
	li_from_month = integer(adw_name.GetitemString(al_row, 'from_month'))
	li_from_day = integer(adw_name.GetitemString(al_row, 'from_day'))
	li_from_year = li_curr_year
	li_to_month = integer(adw_name.GetitemString(al_row, 'to_month'))
	li_to_day = integer(adw_name.GetitemString(al_row, 'to_day'))
	
	//if from > to, the year is next year else its current year
	if(li_from_month > li_to_month) then
		li_to_year = li_curr_year + 1
	else
		li_to_year = li_curr_year
	end if
	
	ld_from = DATE(string(li_from_month) + "/" + string(li_from_day) + "/" +  string(li_from_year))
	ld_to = date(string(li_to_month) + "/" + string(li_to_day) + "/" + string(li_to_year))
	ld_to_current = date(string(li_to_month) + "/" + string(li_to_day) + "/" + string(li_curr_year))

	do while li_loopCount < ll_rowCount
		li_loopCount = li_loopCount + 1
		
		// see if this record has duplicates
		li_dup_row = adw_name.Find(ls_Find , li_loopCount, ll_RowCount)

		if li_dup_row = 0 then // if not then return
			return 0
		end if
		
		li_loopCount = li_dup_row // if one is found, we will move our looper to its position so we can start on the one after it next iteration
		if(li_loopCount <> al_row) then //only validate against rows that aren't itself
			// start date parsing for next record (the duplicate)
			li_from_month_dup = integer(adw_name.GetitemString(li_dup_row, 'from_month'))
			li_from_day_dup = integer(adw_name.GetitemString(li_dup_row, 'from_day'))
			li_from_year_dup = li_curr_year
			li_to_month_dup = integer(adw_name.GetitemString(li_dup_row, 'to_month'))
			li_to_day_dup = integer(adw_name.GetitemString(li_dup_row, 'to_day'))
			
			//if from > to, the to year is next year else its current year
			if(li_from_month_dup > li_to_month_dup) then
				li_to_year_dup = li_curr_year + 1
			else
				li_to_year_dup = li_curr_year
			end if
						
			
			ld_from_dup = DATE(string(li_from_month_dup) + "/" + string(li_from_day_dup) + "/" + string(li_from_year_dup))
			ld_to_dup = date(string(li_to_month_dup) + "/" + string(li_to_day_dup) + "/" + string(li_to_year_dup))
			ld_to_current_dup = date(string(li_to_month_dup) + "/" + string(li_to_day_dup) + "/" + string(li_curr_year))

			int li_invalid = 0

			// validate the dates
			select 
				// dup row crosses year
				case 
					// dup row crosses year
					when (cast(:ld_to_dup as datetime) > cast(:ld_to_current_dup as datetime) AND cast(:ld_to as datetime) = cast(:ld_to_current as datetime)) then
						case when cast(:ld_to as datetime) <= cast(:ld_to_current_dup as datetime) then 1 
							  when cast(:ld_to as datetime) >= cast(:ld_from_dup as datetime) then 3 
							  when cast(:ld_from as datetime) >= cast(:ld_from_dup as datetime) then 2 
							  when cast(:ld_from as datetime) <= cast(:ld_to_current_dup as datetime) then 4 else 0 					
						 end
					// neither rolls years
					WHEN (cast(:ld_to as datetime) = cast(:ld_to_current as datetime) AND cast(:ld_to_dup as datetime) = cast(:ld_to_current_dup as datetime)) then
						case  when (cast(:ld_from as datetime)     between cast(:ld_from_dup as datetime) and cast(:ld_to_dup as datetime)) then 5
								when (cast(:ld_to as datetime)       between cast(:ld_from_dup as datetime) and cast(:ld_to_dup as datetime)) then 6
								when (cast(:ld_from_dup as datetime) between cast(:ld_from as datetime)     and cast(:ld_to as datetime)) then 7
								when (cast(:ld_to_dup as datetime)   between cast(:ld_from as datetime)     and cast(:ld_to as datetime)) then 8 else 0
						end
					// both roll years
					WHEN (cast(:ld_to as datetime) > cast(:ld_to_current as datetime) AND cast(:ld_to_dup as datetime) > cast(:ld_to_current_dup as datetime)) then
						case  when (cast(:ld_to as datetime) >= cast(:ld_to_dup as datetime)) then 9
								when (cast(:ld_to as datetime) >= cast(:ld_from_dup as datetime)) then 10
								when (cast(:ld_from as datetime) <= cast(:ld_from_dup as datetime)) then 11
								when (cast(:ld_from as datetime) <= cast(:ld_to_dup as datetime)) then 12 else 0
						end
					// only original row rolls years
					when (cast(:ld_to as datetime) > cast(:ld_to_current as datetime) AND cast(:ld_to_dup as datetime) = cast(:ld_to_current_dup as datetime)) then
						case  when (cast(:ld_to_current as datetime) >= cast(:ld_from_dup as datetime)) then 13
								when (cast(:ld_to_current as datetime) >= cast(:ld_to_dup as datetime)) then 14
								when (cast(:ld_from as datetime) between cast(:ld_from_dup as datetime) and cast(:ld_to_dup as datetime)) then 15 else 0
						end
				end
					
			into :li_invalid 
			FROM sku_products 
				
			USING SQLCA;

			IF(SQLCA.SQLCode > 0) THEN
				adw_name.SelectRow(li_dup_row, True)
				iw_Frame.SetMicroHelp("SQL error validating this row. " + string(SQLCA.SQLCode))
				return -1
			END IF
			
			if(li_invalid > 0) then
				adw_name.SelectRow(al_row, True)
				adw_name.SelectRow(li_dup_row, True)
				iw_Frame.SetMicroHelp("Dates cannot overlap.")
				return -1
			end if
			
		end if
   Loop 
End If

return 0



/*Long						ll_month, &
							ll_day, &
							ll_rowCount, &
							ll_to_month, &
							ll_to_day, &
							ll_from_month, &
							ll_from_day, &
							ll_dup_to_month, &
							ll_dup_to_day, &
							ll_dup_from_month, &
							ll_dup_from_day 
							
String					ls_Find
							

u_project_functions	lu_project_functions


ls_Find = "complex_code = '" + adw_name.GetItemString(al_row, 'complex_code') + &
					"' and prod_state = '" + adw_name.GetItemString(al_row, 'prod_state') + &
					"' and setting_type = '" + adw_name.GetItemString(al_row, 'setting_type')+ "'"

li_dup_row = 0

if al_row > 1 then
	li_dup_row = adw_name.Find(ls_Find , 1, al_row - 1) 
End if

If li_dup_row = 0 Then
	ll_RowCount = adw_name.RowCount()
	If al_row < ll_rowCount Then
		li_dup_row = adw_name.Find(ls_Find , al_row + 1, ll_RowCount)
	End If
End If

If li_dup_row = 0 Then
	return 0
Else
	
	ll_from_month = Long(adw_name.GetitemString(al_row, 'from_month'))
	ll_from_day = Long(adw_name.GetitemString(al_row, 'from_day')) 
	ll_to_month = Long(adw_name.GetitemString(al_row, 'to_month'))
	ll_to_day = Long(adw_name.GetitemString(al_row, 'to_day'))
	
	ll_dup_from_month = Long(adw_name.GetitemString(li_dup_row, 'from_month'))
	ll_dup_from_day = Long(adw_name.GetitemString(li_dup_row, 'from_day'))
	ll_dup_to_month = Long(adw_name.GetitemString(li_dup_row, 'to_month'))
	ll_dup_to_day = Long(adw_name.GetitemString(li_dup_row, 'to_day'))
	
// if the from month matches on both rows check the day, if the day on the changed row is > then the dup row
// the dates overlap.
	if ll_from_month = ll_dup_from_month Then
		if long(ll_from_day) > long(ll_dup_from_day) Then
			lb_error = True
		End If
	else
// if the from month matches the to month check the day, if the day on the changed row is < then the dup row
// the dates overlap.
		if ll_from_month = ll_dup_to_month Then
			if Long(ll_from_day) < Long(ll_dup_to_day) Then
				lb_error = True
			end if
		end if
	end if

// if the to month matches on both rows check the day, if the day on the changed row is < then the dup row
// the dates overlap.
	if ll_to_month = ll_dup_to_month Then
		if long(ll_to_day) < long(ll_dup_to_day) Then
			lb_error = True
		End If
	else
// if the to month matches the from month check the day, if the day on the changed row is > then the dup row
// the dates overlap.
		if ll_to_month = ll_dup_from_month Then
			if Long(ll_to_day) > Long(ll_dup_from_day) Then
				lb_error = True
			end if
		end if
	end if

//if the to month on the dup line is less than the from month, compare the dates between the from 
// month and the end of the year and the beginning of the year to the to month.
	if ll_dup_to_month < ll_dup_from_month Then
		if (ll_from_month > ll_dup_from_month and ll_from_month <= 12) or &  
			(ll_from_month >= 1 and ll_from_month < ll_dup_to_month) Then 
			lb_error = True
		else
			if (ll_to_month > ll_dup_from_month and ll_to_month <= 12) or &
				(ll_to_month >= 1 and ll_to_month < ll_dup_from_month) Then
				lb_error = True
			end if
		end if
	else
		if ll_from_month > ll_dup_from_month and ll_from_month < ll_dup_to_month Then
			lb_error = True
		else
			if ll_to_month > ll_dup_from_month and ll_to_month < ll_dup_to_month Then
				lb_error = True
			end if
		end if
	end if
	
	if ll_to_month = ll_dup_to_month and &
	   ll_to_day = ll_dup_to_day and &
		ll_from_month = ll_dup_from_month and &
		ll_from_day = ll_dup_from_day Then
		 lb_error = True
	end if
	
	if lb_error Then
		adw_name.SelectRow(al_row, True)
		adw_name.SelectRow(li_dup_row, True)
		iw_Frame.SetMicroHelp("Dates cannot overlap.")
		return -1
	Else
		return 0
	End if

End if*/
//
//
end function

public function string wf_get_desc (string data);string ls_type_desc
	
	SELECT tutltypes.type_desc
	INTO :ls_type_desc
	FROM tutltypes
	WHERE tutltypes.record_type = 'COOLCODE'
	  AND tutltypes.type_code   = :data
	USING SQLCA ;

return ls_type_desc
end function

public function boolean wf_update ();boolean lb_rtn
Integer	li_rtn

if IsValid(w_locations_popup) then close(w_locations_popup)

if not wf_checkrights() then
	return false
else
	li_rtn = tab_detail.TriggerEvent('ue_update')
	
	Return lb_rtn
	 
end if


//Choose case is_selection
		//case 10
			
	//end choose

end function

public subroutine wf_delete_excluded_plants (integer ai_row);// pjm 09/02/2014 added for excluded plants
long ll_found_row, ll_row, ll_excl_row

ll_found_row = ids_exclude_plants.Find("excl_row = " + string(ai_row),1,ids_exclude_plants.RowCount())
If ll_found_row > 0 Then
	For ll_row = ids_exclude_plants.RowCount() to ll_found_row Step -1
		ll_excl_row = ids_exclude_plants.GetItemNumber(ll_row,"excl_row")
		If ll_excl_row = ai_row Then
			ids_exclude_plants.DeleteRow(ll_row)
		ElseIf ll_excl_row > ai_row Then  // have to change row number
			ids_exclude_plants.SetItem(ll_row,"excl_row", ll_excl_row - 1)
		End If
	Next
End If
		
		
		
end subroutine

public function long wf_get_max_excl_row ();il_max_excl_row++

Return il_max_excl_row
end function

public subroutine wf_set_max_excl_row (long al_max_excl_row);il_max_excl_row = al_max_excl_row
end subroutine

public subroutine wf_process_cust_excl_string (ref string as_cust_excl_string);string ls_cust_excl_string, ls_row, ls_plant
long ll_row
int  li_row, li_count

u_string_functions lu_string

ids_exclude_plants.Reset()
Do While Len(Trim(as_cust_excl_string)) > 0
	ls_cust_excl_string = Trim(lu_string.nf_GetToken(as_cust_excl_string,","))
	// first, get the row number
	ls_row = Trim(lu_string.nf_GetToken(ls_cust_excl_string,"~t"))
	li_row = Integer(ls_row)
	
	Do 
		ls_plant = Trim(lu_string.nf_GetToken(ls_cust_excl_string,"~t"))
		ls_plant = Left(ls_plant,3)  // check for linefeed
		ll_row = ids_exclude_plants.InsertRow(0)
		ids_exclude_plants.SetItem(ll_row,"excl_row",li_row)
		ids_exclude_plants.SetItem(ll_row,"excl_plant_id",ls_plant)
		ids_exclude_plants.SetItem(ll_row,"update_flag","O")
		// pjm 09/03/2014  Set the row in the cool codes dw
		tab_detail.tp_product_cat.dw_product_category.SetItem(li_row,"excl_plant_row", li_row)
	Loop While Len(Trim(ls_cust_excl_string)) > 0
	li_count++
Loop
// pjm 09/03/2014 set max excl plant row
wf_set_max_excl_row(Long(li_row))
end subroutine

public subroutine wf_get_selected_plants ();//pjm 08/18/2014 added for excluded plants
long				ll_count, ll_excl_row
integer			li_windowx, li_windowy, li_height, li_height_H
Application					la_Application

is_selected_plants = ""
 
long ll_row, ll_found_row, ll_last_row
string ls_excl_plant, ls_find_string
 
// pjm 09/02 change for new exc_row column
//for ll_row = 1 to ids_exclude_plants.RowCount()
//	ll_excl_row = ids_exclude_plants.GetItemNumber(ll_row,"excl_row")
//	If ll_excl_row = il_selected_row Then
//		If ids_exclude_plants.getitemstring(ll_row, "update_flag") = 'D' Then Continue
//		ls_excl_plant = ids_exclude_plants.getitemstring(ll_row, "excl_plant_id")
//		if isnull(ls_plant)  then
//			is_selected_plants += " " + "~r~n"
//		else
//			is_selected_plants += ls_excl_plant + "~r~n"
//		end if
//	end if
//next
ll_excl_row = tab_detail.tp_product_cat.dw_product_category.GetItemNumber(il_selected_row,"excl_plant_row")
If Isnull(ll_excl_row) or ll_excl_row = 0 Then  // need to set the new column
	tab_detail.tp_product_cat.dw_product_category.SetItem(il_selected_row,"excl_plant_row",wf_get_max_excl_row())
	is_selected_plants = " " + "~r~n"
Else
	ls_find_string = "excl_row = " + String(ll_excl_row) + " and update_flag <> 'D' "
	ll_found_row = ids_exclude_plants.Find(ls_find_string,1,ids_exclude_plants.RowCount() )
	ll_last_row = ids_exclude_plants.RowCount()													 
	If ll_found_row > 0 Then
		Do
			ls_excl_plant = ids_exclude_plants.getitemstring(ll_found_row, "excl_plant_id")
			if isnull(ls_excl_plant)  then
				is_selected_plants += " " + "~r~n"
			else
				is_selected_plants += ls_excl_plant + "~r~n"
			end if
			ll_found_row++
			If ll_found_row > ll_last_row Then Exit
			ll_found_row = ids_exclude_plants.Find(ls_find_string, ll_found_row,ids_exclude_plants.RowCount() )
		Loop While ll_found_row > 0 
	End If
End If

// find opening x/y positions for "Multi select drop down"
la_Application = GetApplication()
Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose

li_windowx = iw_frame.width
li_windowy = iw_frame.height

if (iw_frame.width - (this.x + integer(tab_detail.tp_product_cat.dw_product_category.X) + integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.X))) > 1179 then
	ii_dddw_x = integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.X) + 38 + this.x + tab_detail.x
else
	ii_dddw_x = iw_frame.width - 1179 - 38
	if ii_dddw_x < 0 then ii_dddw_x = integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.X) + 38 + this.x + tab_detail.x
end if
if (iw_frame.height - (this.y + tab_detail.y +integer(tab_detail.tp_product_cat.dw_product_category.y) + ((integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.y) + &
				integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.height) + 8) * &
				(il_selected_row - (integer(tab_detail.tp_product_cat.dw_product_category.Object.DataWindow.FirstRowOnPage) - 1)) + 440 + li_height)) > (676 + 200)) then
	ii_dddw_y = this.y + tab_detail.y + integer(tab_detail.tp_product_cat.dw_product_category.y) + ((integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.y) + &
				integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.height) + 8) * &
				(il_selected_row - (integer(tab_detail.tp_product_cat.dw_product_category.Object.DataWindow.FirstRowOnPage) - 1))) + 440 + li_height
else
	ii_dddw_y = (integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.y) + 288 + this.y + tab_detail.y + (((integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.y) + &
				integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.height) + 8)) * &
				(il_selected_row - (integer(tab_detail.tp_product_cat.dw_product_category.Object.DataWindow.FirstRowOnPage) - 1))))  - 676 + li_height
	if ii_dddw_y < 0 then ii_dddw_y = this.y + tab_detail.y + integer(tab_detail.tp_product_cat.dw_product_category.y) + ((integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.y) + &
				integer(tab_detail.tp_product_cat.dw_product_category.Object.excl_plant_id.height) + 8) * &
				(il_selected_row - (integer(tab_detail.tp_product_cat.dw_product_category.Object.DataWindow.FirstRowOnPage) - 1))) + 440  + li_height
end if
 
end subroutine

public subroutine wf_set_prod_cat ();// pjm 08/18/2014 - Set the product category dw from the datastore
long ll_row, ll_row2, ll_row_count, ll_found_row
string ls_type_desc, ls_type_code
//datawindowchild ldwc_child


ll_row_count = ids_product_category.RowCount()
For ll_row = 1 to ll_row_count
	ll_row2 = tab_detail.tp_product_cat.dw_product_category.InsertRow(0)
	tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row2, "prod_category", &
		ids_product_category.getitemstring(ll_row, "prod_category") )
	ls_type_code = ids_product_category.getitemstring(ll_row, "cool_code") 
	tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row2, "cool_code", &
		ls_type_code )
	 	  
	ls_type_code = Trim(ls_type_code)
	
//	SELECT tutltypes.type_desc
//	INTO :ls_type_desc
//	FROM tutltypes
//	WHERE tutltypes.record_type = 'COOLCODE'
//	  AND tutltypes.type_code   = :ls_type_code
//	USING SQLCA ;
	INT li_ret
// pjm 09/17/2014 - now use the datastore
//	li_ret = tab_detail.tp_product_cat.dw_product_category.GetChild('cool_code', ldwc_child)
//	ll_found_row = ldwc_child.Find("type_code = '" + ls_type_code + "'",1,ldwc_child.RowCount())
//	If ll_found_row > 0 Then
//		ls_type_desc = ldwc_child.GetItemString(ll_found_row,"type_desc")
//	Else
//		ls_type_desc = ""
//	End If
	ll_found_row = ids_prod_cat_cool_code_label.Find("cool_code = '" + ls_type_code + "'",1, &
	                                ids_prod_cat_cool_code_label.RowCount() )
	If ll_found_row > 0 Then
		ls_type_desc = ids_prod_cat_cool_code_label.object.cool_code_desc[ll_found_row]
	Else
		ls_type_desc = ""
	End If
	
	tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row2, "cool_desc", ls_type_desc)
	
	
	tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row2, "label_id", &
		ids_product_category.getitemstring(ll_row, "label_id") )
	tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row2, "update_flag", &
		ids_product_category.getitemstring(ll_row, "update_flag") )

Next

	

end subroutine

public subroutine wf_set_label_excl_plant_descs ();// Set the label description and excluded plant description
long ll_row, ll_lbl_row, ll_excl_row, ll_last_row, ll_row2
string ls_label_id, ls_label_desc, ls_excl_plant, ls_plant_desc, ls_plant_descs, ls_find_string
boolean lb_first_plant
  
datawindowchild ldwc_child

// pjm 09/03/2014 - set the sort for the datastore
int li_ret
li_ret = ids_exclude_plants.SetSort("excl_row A, excl_plant_id A")
li_ret = ids_exclude_plants.Sort()

//tab_detail.tp_product_cat.dw_product_category.GetChild('label_id', ldwc_child)

For ll_row = 1 to tab_detail.tp_product_cat.dw_product_category.RowCount()
	ls_plant_descs=""
	// Set the label description
	// pjm 09/17/2014 - now using the datastore.
//	ls_label_id = tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row,'label_id')
//	ll_last_row = ldwc_child.RowCount()
//	ll_lbl_row = ldwc_child.Find("label_id = '" + ls_label_id + "'",1,ll_last_row)
//	If ll_lbl_row > 0 Then
//		ls_label_desc = ldwc_child.GetItemString(ll_lbl_row,"label_text")
//		tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row, "label_desc", ls_label_desc)
//	End If
	ls_label_id = tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row,'label_id')
	ll_lbl_row = ids_prod_cat_cool_code_label.Find("label_id = '" + ls_label_id + "'",1,ids_prod_cat_cool_code_label.RowCount())
	If ll_lbl_row > 0 Then
		ls_label_desc = ids_prod_cat_cool_code_label.GetItemString(ll_lbl_row,"label_desc")
		tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row, "label_desc", ls_label_desc)
	End If
	// Set the plant description

	ll_last_row = ids_exclude_plants.RowCount()
	// pjm 09/03/2014 changed for new column
	ll_excl_row = tab_detail.tp_product_cat.dw_product_category.GetItemNumber(ll_row,'excl_plant_row')
	If IsNull(ll_excl_row) or ll_excl_row = 0 Then Continue
	
	lb_first_plant = true
	ls_find_string = "excl_row = " + string(ll_excl_row)
	ll_row2 = ids_exclude_plants.Find(ls_find_string,1,ll_last_row)
	Do while ll_row2 > 0
		ls_excl_plant = ids_exclude_plants.GetItemString(ll_row2,"excl_plant_id")

		If Pos(ls_excl_plant,char(10)) > 0 Then // check for linefeed	
		   ls_excl_plant = Left(ls_excl_plant, 3)
		End If
		
		// get the plant description
		SELECT  
		  locations.location_name into :ls_plant_desc
		FROM locations
		WHERE locations.location_code = :ls_excl_plant
		using sqlca ;
		
		If sqlca.sqlcode = 0 Then
			ls_plant_descs = ls_plant_descs + ls_plant_desc + ","	 
		End If 
	
		If lb_first_plant Then // set the plant id
				tab_detail.tp_product_cat.dw_product_category.SetItem( &
	              ll_row,"excl_plant_id",ls_excl_plant )
					  lb_first_plant = false
		End If
		ll_row2++
		If ll_row2 > ll_last_row Then	Exit
		ll_row2 = ids_exclude_plants.Find(ls_find_string, ll_row2,ll_last_row)
	Loop 
	// set the plant description in the dw
	tab_detail.tp_product_cat.dw_product_category.SetItem(ll_row,"excl_plant_desc",Left(ls_plant_descs,40) )
Next					  
end subroutine

public subroutine wf_set_selected_plants ();// pjm 08/15/2014  added for excluded plants
long					ll_excl_row, ll_row, ll_found_row, ll_new_excl_row
String				ls_excl_plant, ls_update_flag, ls_old_update_flag, ls_excl_plant_str, ls_plant_desc, ls_plant_descs
integer           li_pos, li_row

u_string_functions	lu_string

if ib_modified then
	// first, find and delete old rows
//	For ll_row = ids_exclude_plants.RowCount() to 1 STEP -1
//		 ll_excl_row = ids_exclude_plants.GetItemNumber(ll_row,"excl_row")
//		 If ll_excl_row = il_selected_row Then
//			 ids_exclude_plants.DeleteRow(ll_row)
//		 End If
//	Next
   // pjm 09/03/2014 added for new excl_plant_row column in dw
	ids_exclude_plants.SetSort("")
	ids_exclude_plants.Sort()
	ll_excl_row = tab_detail.tp_product_cat.dw_product_category.GetItemNumber(il_selected_row,"excl_plant_row")
	// pjm end
	Do While Len(Trim(is_selected_plants)) > 0 
 		ls_excl_plant_str = Trim(lu_string.nf_GetToken(is_selected_plants, "~r~n"))
		ls_excl_plant = Trim(lu_string.nf_GetToken(ls_excl_plant_str, "~t"))
		ls_update_flag = Trim(lu_string.nf_GetToken(ls_excl_plant_str, "~t"))
		// Find if row already exists
		// pjm 09/03/2014 changed for new dw column
		ll_found_row = ids_exclude_plants.Find("excl_row = " + string(ll_excl_row) + " and excl_plant_id = ~"" + ls_excl_plant + "~"",1,ids_exclude_plants.RowCount())
		If ll_found_row > 0 Then
			ls_old_update_flag = ids_exclude_plants.GetItemString(ll_found_row,"update_flag")
			If IsNull(ls_old_update_flag) Then ls_old_update_flag = "O"
			If ls_old_update_flag = "O" Then
				ids_exclude_plants.SetItem(ll_found_row,"update_flag",ls_update_flag)
			ElseIf ls_old_update_flag <> ls_update_flag Then
				ids_exclude_plants.DeleteRow(ll_found_row)
			End If
			Continue
//		Else
//			//MessageBox("Error","could not find " + ls_excl_plant)
		End If
		ll_row = ids_exclude_plants.InsertRow(0)		
		// pjm 09/03/2014 changed for new column
		// ids_exclude_plants.SetItem(ll_row,"excl_row",il_selected_row)
		ls_excl_plant = Right(ls_excl_plant,Len(ls_excl_plant) - Pos(ls_excl_plant,char(10))) // check for linefeed
		ids_exclude_plants.SetItem(ll_row,"excl_row",ll_excl_row)
		ids_exclude_plants.SetItem(ll_row,"excl_plant_id",ls_excl_plant)
		ids_exclude_plants.SetItem(ll_row,"update_flag",ls_update_flag)
		
	Loop 

	
end if
// set the datawindow with the first plant
int li_ret
li_ret = ids_exclude_plants.SetSort("excl_row A, excl_plant_id A")
li_ret = ids_exclude_plants.Sort()
ll_found_row = ids_exclude_plants.Find("excl_row = " + string(ll_excl_row) + "and update_flag <> 'D'",1,ids_exclude_plants.RowCount())
If ll_found_row > 0 Then
	ls_excl_plant = ids_exclude_plants.GetItemString(ll_found_row,"excl_plant_id")
Else
	ls_excl_plant = ''
End If
tab_detail.tp_product_cat.dw_product_category.SetItem( &
	  il_selected_row,"excl_plant_id",ls_excl_plant )
If ll_found_row > 0 Then
// get the plant descriptions for the selected rows
   Do
		SELECT  
		  locations.location_name into :ls_plant_desc
		FROM locations
		WHERE locations.location_code = :ls_excl_plant
		using sqlca ;
		
		If sqlca.sqlcode = 0 Then
			ls_plant_descs = ls_plant_descs + ls_plant_desc + ","
		End If
	
		ll_found_row ++
		If ll_found_row > ids_exclude_plants.RowCount() Then Exit
		ll_found_row = ids_exclude_plants.Find("excl_row = " + string(ll_excl_row) + "and update_flag <> 'D'",ll_found_row,ids_exclude_plants.RowCount())
		If ll_found_row > 0 Then
			ls_excl_plant = ids_exclude_plants.GetItemString(ll_found_row,"excl_plant_id")
		End If
	Loop While ll_found_row > 0
End If
// set the plant description in the dw
tab_detail.tp_product_cat.dw_product_category.SetItem( &
				  il_selected_row,"excl_plant_desc",Left(ls_plant_descs,40) )
ib_open = false
end subroutine

public subroutine wf_process_cool_code_string (string as_cool_code_str);// pjm 08/13/2014 This function will process the cool codes string and label info
//                for the dropdown datawindows for the cool codes and labels.
// pjm 09/16/2014 - added category for processing cool codes, now store everything in datastore
string ls_cool_code, &
		 ls_cool_code_line, &
		 ls_cool_desc, &
		 ls_label_id, &
		 ls_label_desc, &
		 ls_category, &
		 ls_values, &
		 ls_junk, &
		 ls_nl = '~n', &
		 ls_ret

  datawindowchild ldwc_labels, ldwc_cool_codes

long ll_rows, ll_row, ll_len

u_string_functions	lu_string

int iasc, li_ret

//li_ret = tab_detail.tp_product_cat.dw_product_category.GetChild('cool_code', ldwc_cool_codes)
//li_ret = tab_detail.tp_product_cat.dw_product_category.GetChild('label_id', ldwc_labels)
//li_ret = ldwc_labels.Reset()
//li_ret = ldwc_cool_codes.Reset()
ids_prod_cat_cool_code_label.Reset()

Do While Len(Trim(as_cool_code_str)) > 0
	   ls_cool_code_line = Trim(lu_string.nf_GetToken(as_cool_code_str, '~r'))
		ls_category = lu_string.nf_GetToken(ls_cool_code_line, '~t') // 09/16/2014
    	ls_cool_code = Trim(lu_string.nf_GetToken(ls_cool_code_line, '~t'))
		ls_label_id = lu_string.nf_GetToken(ls_cool_code_line, '~t')
		ls_label_id = Right(ls_label_id, Len(ls_label_id) - Pos(ls_label_id,char(10)))  // check for linefeed
		If Len(ls_cool_code) > 0 and ls_cool_code <> ls_nl and ls_cool_code <> ' ' Then  
			ls_cool_desc = Trim(lu_string.nf_GetToken(ls_cool_code_line, '~t'))
		End If
		
		If Len(ls_label_id) > 0 and ls_label_id <> ls_nl and ls_label_id <> ' ' Then  
			iasc = asc(ls_label_id)
			ls_label_desc = Trim(lu_string.nf_GetToken(ls_cool_code_line, '~t'))
		End If
				
		// read unique codes into dropdowns
//		If Len(ls_cool_code) > 0 and ls_cool_code <> ls_nl and ls_cool_code <> ' ' Then  
//			ls_cool_code = Right(ls_cool_code,Len(ls_cool_code) - Pos(ls_cool_code,char(10)))  // check for linefeed
//			ll_row = ldwc_cool_codes.Find("type_code = '" + ls_cool_code + "'",1,ldwc_cool_codes.RowCount() )
//			If ll_row > 0 Then  // already there, continue
//			Else
//				ll_row = ldwc_cool_codes.InsertRow(0)
//				ldwc_cool_codes.SetItem(ll_row, "type_code", ls_cool_code)
//				ldwc_cool_codes.SetItem(ll_row, "type_desc", ls_cool_desc)
//			End If
//		End If
		// read unique data into datastore
		string ls_find
		ls_cool_code = Right(ls_cool_code,Len(ls_cool_code) - Pos(ls_cool_code,char(10)))
		ls_category = Right(ls_category,Len(ls_category) - Pos(ls_category,char(10)))
		ls_find = "prod_category = ~"" + ls_category + "~" and cool_code = ~"" + &
		          ls_cool_code + "~" and label_id = ~"" + ls_label_id + "~""
		ll_row = ids_prod_cat_cool_code_label.Find(ls_find, &
																 1,ids_prod_cat_cool_code_label.RowCount() ) 
		If ll_row > 0 Then  // already there, continue
		Else
			ll_row = ids_prod_cat_cool_code_label.InsertRow(0)
			ids_prod_cat_cool_code_label.SetItem(ll_row,"prod_category",ls_category)
			ids_prod_cat_cool_code_label.SetItem(ll_row,"cool_code",ls_cool_code)
			ids_prod_cat_cool_code_label.SetItem(ll_row,"label_id",ls_label_id)
			ids_prod_cat_cool_code_label.SetItem(ll_row,"cool_code_desc",ls_cool_desc)
			ids_prod_cat_cool_code_label.SetItem(ll_row,"label_desc",ls_label_desc)
		End If
		// order by description pjm 09/09/2014
//		ldwc_cool_codes.SetSort("type_desc A")
//		ldwc_cool_codes.Sort()
//		
//		If Len(Trim(ls_label_id)) > 0 and ls_label_id <> ls_nl Then  
//			ll_row = ldwc_labels.Find("label_id = " + ls_label_id,1,ldwc_labels.RowCount() )
//			If ll_row > 0 Then  // already there, continue
//			Else
//				ll_row = ldwc_labels.InsertRow(0)
//				ldwc_labels.SetItem(ll_row, "label_id", ls_label_id)
//				ldwc_labels.SetItem(ll_row, "label_text", ls_label_desc)
//				ldwc_labels.SetItem(ll_row, "cool_code", ls_cool_code)
//			End If
//		End If
Loop
gw_netwise_frame.SetMicroHelp(String(ids_prod_cat_cool_code_label.RowCount()) + " rows inserted") // testing
end subroutine

public function integer wf_fill_labels_dddw ();datawindowchild ldwc_labels
integer 	li_ret
long		ll_row, ll_rows, ll_dddw_row
string	ls_label_id, ls_label_desc

li_ret = tab_detail.tp_product_cat.dw_product_category.GetChild('label_id', ldwc_labels)
li_ret = ldwc_labels.Reset()

li_ret = ids_prod_cat_cool_code_label.SetSort("prod_category A, label_desc A")
li_ret = ids_prod_cat_cool_code_label.Sort()
If Len(Trim(is_cool_code)) > 0 Then
	li_ret = ids_prod_cat_cool_code_label.SetFilter("prod_category = ~"" + is_prod_category + "~" and " + &
	         "cool_code = ~"" + is_cool_code + "~"")
Else
	li_ret = ids_prod_cat_cool_code_label.SetFilter("prod_category = ~"" + is_prod_category + "~"")
End If	
li_ret = ids_prod_cat_cool_code_label.Filter()
ll_rows = ids_prod_cat_cool_code_label.RowCount()

For ll_row = 1 to ll_rows
	ls_label_id = ids_prod_cat_cool_code_label.object.label_id[ll_row]
	ls_label_desc = ids_prod_cat_cool_code_label.object.label_desc[ll_row]
	ll_dddw_row = ldwc_labels.InsertRow(0)
	ldwc_labels.SetItem(ll_row, "label_id", ls_label_id)
	ldwc_labels.SetItem(ll_row, "label_text", ls_label_desc)
Next

return li_ret
end function

public function integer wf_fill_cool_code_dddw ();datawindowchild ldwc_cool_codes
integer 	li_ret
long		ll_row, ll_rows, ll_dddw_row
string	ls_cool_code, ls_cool_desc, ls_save_desc

li_ret = tab_detail.tp_product_cat.dw_product_category.GetChild('cool_code', ldwc_cool_codes)
li_ret = ldwc_cool_codes.Reset()

li_ret = ids_prod_cat_cool_code_label.SetSort("prod_category A, cool_code_desc A")
li_ret = ids_prod_cat_cool_code_label.Sort()
li_ret = ids_prod_cat_cool_code_label.SetFilter("prod_category = ~"" + is_prod_category + "~"")
li_ret = ids_prod_cat_cool_code_label.Filter()
ll_rows = ids_prod_cat_cool_code_label.RowCount()

For ll_row = 1 to ll_rows
	ls_cool_code = ids_prod_cat_cool_code_label.object.cool_code[ll_row]
	ls_cool_desc = ids_prod_cat_cool_code_label.object.cool_code_desc[ll_row]
	If ls_cool_desc <> ls_save_desc Then
		ll_dddw_row = ldwc_cool_codes.InsertRow(0)
		ldwc_cool_codes.SetItem(ll_dddw_row, "type_code", ls_cool_code)
		ldwc_cool_codes.SetItem(ll_dddw_row, "type_desc", ls_cool_desc)
	End If
	ls_save_desc = ls_cool_desc
Next

return li_ret








end function

public subroutine wf_build_update_string (ref string as_input_string);// pjm 10/19/2014 Build update string for service 36br
long ll_row, ll_RowCount, ll_excl_row, ll_row2
string ls_update_flag, ls_excl_update_flag, ls_ehdr

ll_RowCount = tab_detail.tp_product_cat.dw_product_category.RowCount()
For ll_row = 1 to ll_RowCount
	// check for blank line:
	If IsNull(tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row, 'prod_category')) Then Return
	
	ls_update_flag = tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row, 'update_flag')
	If IsNull(ls_update_flag) Then ls_update_flag = ' '
	ls_ehdr = "~r~nE~t"
	If ls_update_flag <> '' AND ls_update_flag <> ' ' Then  
		as_input_string = as_input_string + "M~t" + tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row, 'prod_category') + "~t" + &
						tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row, 'cool_code') + "~t" + &
						tab_detail.tp_product_cat.dw_product_category.GetItemString(ll_row, 'label_id')  + "~t" + &
						ls_update_flag 
	  If ls_update_flag <> 'D' Then
			
			// check if there are any excluded plants for the row
			string ls_find
			long ll_excl_plant_row
			// pjm 09/03/2014 changed for new column
			ll_excl_plant_row = tab_detail.tp_product_cat.dw_product_category.GetItemNumber(ll_row,"excl_plant_row")
			If IsNull(ll_excl_plant_row) Or ll_excl_plant_row = 0 Then 
				as_input_string = as_input_string + "~r~n"
				Continue
			End If
			ls_find = 'excl_row = ' + String(ll_excl_plant_row)
			ll_excl_row = ids_exclude_plants.Find(ls_find, 1, ids_exclude_plants.RowCount() )
			If ll_excl_row > 0 Then
				For ll_row2 = ll_excl_row to ids_exclude_plants.RowCount()
					 If ids_exclude_plants.GetItemNumber(ll_row2,"excl_row") = ll_excl_plant_row Then
						 ls_excl_update_flag = ids_exclude_plants.GetItemString(ll_row2,"update_flag")
						 If IsNull(ls_excl_update_flag) Then ls_excl_update_flag = "O"
						 If ls_excl_update_flag <> "O" Then
							If Len(ls_ehdr) > 0 Then
								as_input_string = as_input_string + ls_ehdr
								ls_ehdr = ''
							End If
						 	as_input_string = as_input_string + ids_exclude_plants.GetItemString(ll_row2,"excl_plant_id") + "~t" &
							                  + ls_excl_update_flag + "~t"
						 End If
					 End If
				 Next
				// replace last tab with formfeed
				as_input_string = Left(as_input_string,Len(as_input_string) - 1) + "~r~n"
			Else
				as_input_string = as_input_string + "~r~n"	
			End If
		Else
			as_input_string = as_input_string + "~r~n"
		End If
	End If
Next
 
end subroutine

public function integer wf_check_loading_platform_update ();Integer li_fresh_count, &
		   li_frozen_count, &
		   li_count
Long ll_RowCount

String ls_product_state, &
			ls_loading_platform, &
			ls_temp
			
ll_rowcount = tab_detail.tp_loading_platform.dw_loading_platform.RowCount ( )

// check to make sure there is at least 1 fresh and 1 frozen loading platform specificed that is not a trackable loading platform like CHEP or SMART
// do not allow update or delete if this minimum requirement is not met
li_fresh_count = 0
li_frozen_count = 0
For li_count = 1 to ll_RowCount - 1
	ls_product_state = Trim(tab_detail.tp_loading_platform.dw_loading_platform.GetItemString(li_count, 'product_state'))
	ls_loading_platform = Trim(tab_detail.tp_loading_platform.dw_loading_platform.GetItemString(li_count, 'loading_platform'))
	
	SELECT type_code
		into :ls_temp
		FROM TUTLTYPES
		WHERE record_type = 'DEFLDINS'
		  AND type_code = :ls_loading_platform
		  USING SQLCA ;
		  
	Choose Case ls_product_state
		Case '1' //Fresh
			If Not IsNull(ls_temp) Then
				li_fresh_count ++
			End If
		Case '2' //Frozen
			If Not IsNull(ls_temp) Then
				li_frozen_count ++
			End If		
	End Choose	
Next

If li_fresh_count < 1 or li_frozen_count < 1 Then
	MessageBox("Error", "At least 1 default Fresh loading platform and 1 default Frozen loading platform is required.")
	tab_detail.tp_loading_platform.dw_loading_platform.SetFocus()
	tab_detail.tp_loading_platform.dw_loading_platform.SetRedraw(True)
	Return -1
End If

Return 0
end function

public function integer wf_check_loading_platform_delete ();Integer li_fresh_count, &
		   li_frozen_count, &
		   li_count
Long ll_RowCount

String ls_product_state, &
			ls_loading_platform, &
			ls_temp
			
ll_rowcount = tab_detail.tp_loading_platform.dw_loading_platform.RowCount ( )

// check to make sure there is at least 1 fresh and 1 frozen loading platform specificed that is not a trackable loading platform like CHEP or SMART
// do not allow update or delete if this minimum requirement is not met
li_fresh_count = 0
li_frozen_count = 0
For li_count = 1 to ll_RowCount
	If (Not tab_detail.tp_loading_platform.dw_loading_platform.IsSelected(li_count)) Then
		ls_product_state = Trim(tab_detail.tp_loading_platform.dw_loading_platform.GetItemString(li_count, 'product_state'))
		ls_loading_platform = Trim(tab_detail.tp_loading_platform.dw_loading_platform.GetItemString(li_count, 'loading_platform'))
	
		SELECT type_code
			into :ls_temp
			FROM TUTLTYPES
			WHERE record_type = 'DEFLDINS'
			  AND type_code = :ls_loading_platform
			  USING SQLCA ;
			  
		Choose Case ls_product_state
			Case '1' //Fresh
				If (Not IsNull(ls_temp)) and (Len(Trim(ls_temp)) > 0) Then
					li_fresh_count ++
				End If
			Case '2' //Frozen
				If (Not IsNull(ls_temp)) and (Len(Trim(ls_temp)) > 0) Then
					li_frozen_count ++
				End If		
		End Choose	
	End If
Next

If li_fresh_count < 1 or li_frozen_count < 1 Then
	MessageBox("Error", "At least 1 default Fresh loading platform and 1 default Frozen loading platform is required.")
	tab_detail.tp_loading_platform.dw_loading_platform.SetFocus()
	tab_detail.tp_loading_platform.dw_loading_platform.SetRedraw(True)
	Return -1
End If

Return 0
end function

public function boolean wf_check_dup_programs ();Long		ll_RowCount, ll_counter, ll_rtn

String		ls_find_string

ll_Rowcount = tab_detail.tp_programs.dw_programs.RowCount()

ll_counter = 1

For ll_counter =1 to ll_RowCount
	ls_find_string = "program_id = " + string(  tab_detail.tp_programs.dw_programs.GetItemNumber(ll_counter, "program_id"))
	
	CHOOSE CASE ll_counter
		CASE 1
			ll_rtn =  tab_detail.tp_programs.dw_programs.Find  &
				( ls_find_string, ll_counter + 1, ll_Rowcount)
		CASE 2 to (ll_Rowcount - 1)
			ll_rtn =  tab_detail.tp_programs.dw_programs.Find ( ls_find_string, ll_counter - 1, 1)
			If ll_rtn = 0 Then
				ll_rtn =  tab_detail.tp_programs.dw_programs.Find  &
					(ls_find_string, ll_counter + 1, ll_Rowcount)
				End If
		CASE ll_Rowcount
			ll_rtn =  tab_detail.tp_programs.dw_programs.Find ( ls_find_string, ll_counter - 1, 1)
	END CHOOSE
	
	If ll_rtn > 0 Then
		MessageBox("Duplicate Product", "There are duplicate programs for this customer")
		Return False
	End If
Next

Return True


end function

public function boolean wf_contact_has_multiples (string as_contact_type);String	ls_type_desc

SELECT tutltypes.type_desc
INTO :ls_type_desc
FROM tutltypes
WHERE tutltypes.record_type = 'CONTMULT'
  AND tutltypes.type_code   = :as_contact_type
USING SQLCA ;

If SQLCA.SQLCode = 100 then
	Return False
Else
	Return True
End If

end function

event ue_postopen;call super::ue_postopen;DataWindowChild			ldwc_customer, &
								ldwc_city, &
								ldwc_name, &
								ldwc_temp
	
u_project_functions		lu_proj_func
	
iu_Cfm001 = Create u_cfm001 
If Message.ReturnValue = -1 Then
	Close(This)
	return
End if

iu_orp001 = Create u_orp001 
iu_ws_orp1 = Create u_ws_orp1
iu_ws_orp2 = Create u_ws_orp2
iu_ws_orp5 = Create u_ws_orp5

If Message.ReturnValue = -1 Then
	Close(This)
	return
End if

iw_frame.iu_project_functions.nf_set_s_error( istr_error_info)
istr_error_info.se_window_name = 'w_customer_profile'

// pjm 10/15/2014 added for new service cfmc36br
ids_exclude_plants = create datastore
ids_product_category = create datastore
ids_prod_cat_cool_code_label = create datastore
ids_exclude_plants.dataobject = 'd_excl_plants'
ids_product_category.dataobject = 'd_product_category2'
ids_prod_cat_cool_code_label.dataobject = 'd_prod_cat_cool_code_label'
iw_this = this

IF NOT wf_Retrieve() Then 
	CLOSE( This)
else
	if lu_proj_func.nf_issalesmgr() or lu_proj_func.nf_isschedulermgr() then
		tab_detail.tp_product_age.dw_product_age.object.minageondelivery.Protect = 0
		tab_detail.tp_product_age.dw_product_age.object.maxageondelivery.Protect = 0
		tab_detail.tp_product_age.dw_product_age.object.minageondelivery.Background.Color = 16777215
		tab_detail.tp_product_age.dw_product_age.object.maxageondelivery.Background.Color = 16777215
		
	end if
end if

// Don't add any code here!

//minageondelivery






end event

event activate;call super::activate;Integer		li_selected_tab

u_project_functions		lu_project_functions

nvuo_pa_business_rules 	nvuo_pa

iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_save')

li_selected_tab = tab_detail.SelectedTab


Choose case li_selected_tab
	case 8
		If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
			iw_frame.im_menu.mf_Enable('m_delete')
		Else	
			iw_frame.im_menu.mf_Disable('m_delete')
		End If
	case 9
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_CATAGORY_TAB','MODIFY') Then
			iw_frame.im_menu.mf_Disable('m_save')
		Else
			iw_frame.im_menu.mf_enable('m_save')
		End If
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_CATAGORY_TAB','DELETE') Then
			iw_frame.im_menu.mf_Disable('m_delete')
		else
			iw_frame.im_menu.mf_Enable('m_delete')
		End If		
	case 10
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_TEMP_TAB','MODIFY') Then
			iw_frame.im_menu.mf_Disable('m_save')
		Else
			iw_frame.im_menu.mf_enable('m_save')
		End If
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_TEMP_TAB','DELETE') Then
			iw_frame.im_menu.mf_Disable('m_delete')
		else
			iw_frame.im_menu.mf_Enable('m_delete')
		End If
	case 12
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_PROGRAMS_TAB','MODIFY') Then
			iw_frame.im_menu.mf_Disable('m_save')
		End If
		iw_frame.im_menu.mf_Disable('m_delete')
End choose

end event

event close;call super::close;If IsValid(iu_cfm001) Then
	Destroy iu_cfm001
End if

If IsValid(iu_orp001) Then
	Destroy iu_orp001
End if

If IsValid(iu_ws_orp1) Then
	Destroy iu_ws_orp1
End if

If IsValid(iu_ws_orp2) Then
	Destroy iu_ws_orp2
End if

If IsValid(iu_ws_orp3) Then
	Destroy iu_ws_orp3
End if

If IsValid(iu_ws_orp5) Then
	Destroy iu_ws_orp5
End if

// pjm 08/20/2014 added for new columns
If IsValid(ids_exclude_plants) Then
	Destroy ids_exclude_plants  
End If

If IsValid(ids_product_category) Then
	Destroy ids_product_category  
End If
end event

event deactivate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_save')
end event

on w_customer_profile.create
int iCurrent
call super::create
this.tab_detail=create tab_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_customer_profile.destroy
call super::destroy
destroy(this.tab_detail)
destroy(this.dw_header)
end on

event closequery;call super::closequery;////*** IBDKEEM *** 
//
//integer	li_prompt_onsave,	li_data_modified,	li_selected_tab, li_index
//u_string_functions		lu_string_functions
//
////gets the settings values
//gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(li_prompt_onsave)
//IF gw_base_frame.ib_exit and li_prompt_onsave = 0 THEN RETURN
//
//// Do a security check for save and do nothing if the user does not have priviliges
//IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN
//
//li_data_modified = This.wf_modified( )
//
//if li_data_modified = -1 then
//	// Problem with the AcceptText() function against a datawindow.
//	// The item error will handle setting focus to the correct column. 
//	Message.ReturnValue = 1
//	Return
//end if
//
//
//if li_data_modified = 1 or tab_detail.tp_contact.ole_cust_contact.object.RowDataChanged then
//		// Changes were made to one of the datawindows
//	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
//
//		CASE 1	// Save Changes
//			if lu_string_functions.nf_IsEmpty(dw_header.getitemstring(1,'division')) then
//				li_index = 3
//			else
//				li_index = 1
//			end if
//			
//			for li_selected_tab = li_index to 5
//				Yield( )
//				
//				tab_detail.SelectTab(li_selected_tab)
//				
//				If This.wf_update() = FALSE Then				
//					Message.ReturnValue = 1	 // Update failed - do not close window
//				end if	
//			next 
//			
//		CASE 2	// Do not save changes
//
//		CASE 3	// Cancel the closing of window
//			Message.ReturnValue = 1
//	END CHOOSE
//else
//	// No modifications were made to any datawindows on this sheet...closing of sheet allowed
//end if
//
//Return
//
////*** END IBDKEEM ***
end event

event ue_getdata;call super::ue_getdata;// pjm 08/15/2014 - added for w_location_popup
Choose case as_stringvalue	
	case "division_code"
		Message.Stringparm = dw_header.GetItemString(1,"division")
	case "plant"
		Message.Stringparm = ''
	case "selected"
		Message.Stringparm = is_selected_plants
	case "dddwxpos"
		Message.Stringparm = String(ii_dddw_x)
	case "dddwypos"
		Message.Stringparm = String(ii_dddw_y)
	case "updateheader"
		Message.Stringparm = "false"
	case "updatedetail"
		Message.Stringparm = "true"
	case else
		Message.Stringparm =''


End Choose	
end event

event ue_get_data;call super::ue_get_data;//
Choose case as_value
	case 'ToolTip'
		Message.StringParm = is_description
	Case 'CurrRow'
		Message.StringParm = '1'
	Case 'CurrCol' 
		Message.StringParm = '1'
	Case "xpos"
		Message.StringParm = string(il_xpos)
	Case "ypos"
		Message.StringParm = string(il_ypos)
end choose
end event

type tab_detail from tab within w_customer_profile
event type integer ue_retrieve ( integer index )
event type integer ue_delete ( integer index )
event type integer ue_update ( integer index )
event create ( )
event destroy ( )
event type integer ue_getrowcount ( integer someinteger )
integer x = 14
integer y = 316
integer width = 4768
integer height = 1476
integer taborder = 10
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tp_service_center tp_service_center
tp_contact tp_contact
tp_location tp_location
tp_carrier tp_carrier
tp_prodsub tp_prodsub
tp_product_age tp_product_age
tp_temp_recorder tp_temp_recorder
tp_product_cat tp_product_cat
tp_category tp_category
tp_temp_settings tp_temp_settings
tp_loading_platform tp_loading_platform
tp_programs tp_programs
end type

event ue_retrieve;itp_activetab.PostEvent("ue_retrieve")
Return 1
end event

event ue_delete;itp_activetab.PostEvent("ue_delete")
Return 1
end event

event type integer ue_update(integer index);Integer		li_rtn


if index = 10 then
	//if wf_validate_temps("dw_temp_settings") = False Then
//			Return -1
//	End if
end if

li_rtn = itp_activetab.TriggerEvent("ue_update")

If li_rtn < 0 then
	return -1
Else
	Return 0
End IF
end event

on tab_detail.create
this.tp_service_center=create tp_service_center
this.tp_contact=create tp_contact
this.tp_location=create tp_location
this.tp_carrier=create tp_carrier
this.tp_prodsub=create tp_prodsub
this.tp_product_age=create tp_product_age
this.tp_temp_recorder=create tp_temp_recorder
this.tp_product_cat=create tp_product_cat
this.tp_category=create tp_category
this.tp_temp_settings=create tp_temp_settings
this.tp_loading_platform=create tp_loading_platform
this.tp_programs=create tp_programs
this.Control[]={this.tp_service_center,&
this.tp_contact,&
this.tp_location,&
this.tp_carrier,&
this.tp_prodsub,&
this.tp_product_age,&
this.tp_temp_recorder,&
this.tp_product_cat,&
this.tp_category,&
this.tp_temp_settings,&
this.tp_loading_platform,&
this.tp_programs}
end on

on tab_detail.destroy
destroy(this.tp_service_center)
destroy(this.tp_contact)
destroy(this.tp_location)
destroy(this.tp_carrier)
destroy(this.tp_prodsub)
destroy(this.tp_product_age)
destroy(this.tp_temp_recorder)
destroy(this.tp_product_cat)
destroy(this.tp_category)
destroy(this.tp_temp_settings)
destroy(this.tp_loading_platform)
destroy(this.tp_programs)
end on

event selectionchanging;u_project_functions	lu_project_functions

nvuo_pa_business_rules 	nvuo_pa

iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_Enable('m_delete')

Choose case newindex
	case 8
//If newindex = 8 Then
		If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
			iw_frame.im_menu.mf_Enable('m_delete')
		Else	
			iw_frame.im_menu.mf_Disable('m_delete')
		End If
	case 9
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_CATAGORY_TAB','MODIFY') Then
			iw_frame.im_menu.mf_Disable('m_save')
		End If
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_CATAGORY_TAB','DELETE') Then
			iw_frame.im_menu.mf_Disable('m_delete')
		End If	
	case 10
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_TEMP_TAB','MODIFY') Then
			iw_frame.im_menu.mf_Disable('m_save')
		End If
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_TEMP_TAB','DELETE') Then
			iw_frame.im_menu.mf_Disable('m_delete')
		End If
	case 12
		If not nvuo_pa.uf_check_security('M_CUSTOMER_PROFILE_PROGRAMS_TAB','MODIFY') Then
			iw_frame.im_menu.mf_Disable('m_save')
		End If
		iw_frame.im_menu.mf_Disable('m_delete')
End choose

end event

event selectionchanged;Integer				li_date

DataWindowChild	ldwc_plant, &
						ldwc_plant_descr, &
						ldwc_complex, &
						ldwc_complex_descr, &
						ldwc_category
						
u_project_functions	lu_project_functions
datawindowchild		ldw_DDDWChild


itp_activetab = This.Control[newindex]

IF  itp_activetab.Event Trigger dynamic ue_getrowcount(1)  < 1 Then
	itp_activetab.PostEvent("ue_retrieve")
	Choose case newindex
		case 1

			
//			tp_service_center.dw_service_center_detail.GetChild('divisioncode', ldw_DDDWChild)
//			wf_filldddw(ldw_DDDWChild)
//			tp_service_center.dw_service_center_detail.GetChild('divisiondesc', ldw_DDDWChild)
//			wf_filldddw(ldw_DDDWChild)
		case 3
			tp_location.dw_location.GetChild('location_code', ldwc_plant)
			If ldwc_plant.RowCount() < 1 Then
				tp_location.dw_location.GetChild('location_name', ldwc_plant_descr)
				lu_project_functions.nf_GetLocations(ldwc_plant, '')
				lu_project_functions.nf_GetLocations(ldwc_plant_descr, '')
//				ldwc_plant.ShareData(ldwc_plant_descr)
			End IF
	
			tp_location.dw_location.GetChild('complex_code', ldwc_complex)
			If ldwc_complex.RowCount() < 1 Then
				tp_location.dw_location.GetChild('complex_name', ldwc_complex_descr)
				lu_project_functions.nf_gettutltype(ldwc_complex, 'COMPLEX')
				ldwc_complex.ShareData(ldwc_complex_descr)
			End IF
		case 9
			tp_category.dw_category.GetChild('prod_category', ldwc_category)
	end choose
END IF
end event

type tp_service_center from userobject within tab_detail
event ue_retrieve ( )
event ue_delete ( )
event ue_update ( )
event type long ue_getrowcount ( integer ai_number )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Service~r~nCenter"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
dw_service_center_detail dw_service_center_detail
end type

event ue_retrieve();Boolean	lb_ret

String	ls_input, &
			ls_output, &
			ls_customer, &
			ls_short_name, &
			ls_return
			
datawindowchild ldwc_Temp
			
nvuo_pa_business_rules 	nvuo_pa			
			
u_string_functions		lu_string_functions
u_project_functions		lu_project_functions

istr_error_info.se_event_name = "ue_retrieve of uo_tab_Service_center"

if dw_header.rowcount() = 0 then return

if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the Service Center Tab.")
	return
end if

IF dw_header.getitemstring(1,"customer_type") = "B" then
	messagebox("Required", "Can only inquire on Ship To Customers for the Service Center Tab.")
	return
end if

If dw_header.getitemstring(1,"customer_type") = "C" then
	messagebox("Required", "Can only inquire on Ship To Customers for the Service Center Tab.")
	return
end if
//dmk added another delimiter for dom exporter
//slh added another delimiter - SR25459
ls_input = "I" + '~t' +  dw_header.getitemstring(1,"customer_id") + '~t' +   &
								 dw_header.getitemstring(1,"division") + '~t'+ ' ' + '~t' &
				+ ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' &
				+ ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' &
				+ ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' &
				+ ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' &
				+ ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' + '~t' &
				+ ' ' + '~t' + ' ' + '~t' + ' ' + '~t' + ' ' +  '~t' + ' ' + '~r~n'

SetPointer(HourGlass!)

dw_service_center_detail.Reset()

//lb_ret = iu_cfm001.nf_cfmc28br_service_center_inq(istr_error_info, &
//											ls_input, &
//											ls_output)

lb_ret = iu_ws_orp2.nf_cfmc28fr(ls_input, &
											ls_output,istr_error_info)

//messagebox("For Steven Evans", ls_output)


If lb_ret Then
	If lu_string_functions.nf_isempty(ls_output) Then
		dw_service_center_detail.insertrow(0)
		dw_service_center_detail.setitem(1,'divisioncode', dw_header.getitemstring(1,"division"))	
		dw_service_center_detail.setitem(1,'divisiondesc', dw_header.getitemstring(1,"division"))
	else
		dw_service_center_detail.SetRedraw(False)
		dw_service_center_detail.ImportString(ls_output)
		
		dw_service_center_detail.setitem(1,'perishdirectname', &
								trim(dw_service_center_detail.getitemstring(1,"perishdirectname")))
		dw_service_center_detail.setitem(1,'meatdirectname', &
								trim(dw_service_center_detail.getitemstring(1,"meatdirectname")))
		dw_service_center_detail.setitem(1,'buyername', &
								trim(dw_service_center_detail.getitemstring(1,"buyername")))
		dw_service_center_detail.setitem(1,'assistantbuyername', &
								trim(dw_service_center_detail.getitemstring(1,"assistantbuyername")))
		dw_service_center_detail.setitem(1,'reciever', &
								trim(dw_service_center_detail.getitemstring(1,"reciever")))
		dw_service_center_detail.setitem(1,'companypresident', &
								trim(dw_service_center_detail.getitemstring(1,"companypresident")))
	
		IF dw_service_center_detail.getitemstring(1,"protocaltype") = " " then
			dw_service_center_detail.setitem(1,"protocaltype","")
		end if
		dw_service_center_detail.setitem(1, 'divisiondesc', dw_service_center_detail.getitemstring(1,"divisioncode"))
		dw_service_center_detail.setitem(1,'tsrname', dw_service_center_detail.getitemstring(1,"tsrid"))
		dw_service_center_detail.setitem(1,'fieldsalesconsultantname',  &
											  dw_service_center_detail.getitemstring(1,"fieldsalesconsultantid"))
		dw_service_center_detail.ResetUpdate()
		dw_service_center_detail.SetFocus()
		dw_service_center_detail.SetRedraw(True)
		
	end if
End if

If dw_service_center_detail.GetItemString(1, "store_door") = "Y" Then
	dw_service_center_detail.object.store_door_charge_type.Visible = True
	dw_service_center_detail.object.store_door_label_t.Visible = True
Else
	dw_service_center_detail.object.store_door_charge_type.Visible = False
	dw_service_center_detail.object.store_door_label_t.Visible = False
End If

If dw_service_center_detail.GetItemString(1, "max_box_weight_ind") = "Y" Then
	dw_service_center_detail.object.max_box_weight.Visible = True
Else
	dw_service_center_detail.object.max_box_weight.Visible = False
End If

If nvuo_pa.uf_check_security('W_CUSTOMER_PROFILE_WEIGHT_FIELDS','MODIFY') Then
	dw_service_center_detail.object.max_box_weight_ind.Protect = 0 
	dw_service_center_detail.object.max_box_weight_ind.background.color = 16777215
	dw_service_center_detail.object.max_box_weight.Protect = 0 
	dw_service_center_detail.object.max_box_weight.background.color = 16777215	
	dw_service_center_detail.object.ordered_by_weight_allowed.Protect = 0 
	dw_service_center_detail.object.ordered_by_weight_allowed.background.color = 16777215
	dw_service_center_detail.object.ordered_by_weight_default.Protect = 0 
	dw_service_center_detail.object.ordered_by_weight_default.background.color = 16777215		
Else
	dw_service_center_detail.object.max_box_weight_ind.Protect = 1 
	dw_service_center_detail.object.max_box_weight_ind.background.color = 12632256
	dw_service_center_detail.object.max_box_weight.Protect = 1
	dw_service_center_detail.object.max_box_weight.background.color = 12632256	
	dw_service_center_detail.object.ordered_by_weight_allowed.Protect = 1 
	dw_service_center_detail.object.ordered_by_weight_allowed.background.color = 12632256
	dw_service_center_detail.object.ordered_by_weight_default.Protect = 1
	dw_service_center_detail.object.ordered_by_weight_default.background.color = 12632256	
End If 

If dw_service_center_detail.GetItemString(1, "order_sourcing_allowed") = 'N' Then
	dw_service_center_detail.SetItem(1, "order_splitting_allowed", "N")
	dw_service_center_detail.SetItem(1, "line_splitting_allowed", "N")
	dw_service_center_detail.object.order_splitting_allowed.Protect = True
	dw_service_center_detail.object.line_splitting_allowed.Protect = True
	dw_service_center_detail.object.splitting_uom.Protect = True
	dw_service_center_detail.object.order_splitting_allowed.background.color = 12632256
	dw_service_center_detail.object.line_splitting_allowed.background.color = 12632256
	dw_service_center_detail.object.splitting_uom.background.color = 12632256
Else	
	dw_service_center_detail.object.order_splitting_allowed.Protect = False
	dw_service_center_detail.object.order_splitting_allowed.background.color = 16777215	
	If dw_service_center_detail.GetItemString(1, "order_splitting_allowed") = 'N' Then
		dw_service_center_detail.SetItem(1, "line_splitting_allowed", "N")
		dw_service_center_detail.object.line_splitting_allowed.Protect = True
		dw_service_center_detail.object.splitting_uom.Protect = True
		dw_service_center_detail.object.line_splitting_allowed.background.color = 12632256
		dw_service_center_detail.object.splitting_uom.background.color = 12632256			
	Else	
		dw_service_center_detail.object.line_splitting_allowed.Protect = False
		dw_service_center_detail.object.line_splitting_allowed.background.color = 16777215		
		If dw_service_center_detail.GetItemString(1, "line_splitting_allowed") = 'N' Then
			dw_service_center_detail.object.splitting_uom.Protect = True
			dw_service_center_detail.object.splitting_uom.background.color = 12632256			
		Else	
			dw_service_center_detail.object.splitting_uom.Protect = False
			dw_service_center_detail.object.splitting_uom.background.color = 16777215		
		End If
	End If
End If

If lu_project_functions.nf_issalesmgr() then
	dw_service_center_detail.object.fieldsalesconsultantid.Protect = False
	dw_service_center_detail.object.fieldsalesconsultantid.background.color = 16777215	
Else
	dw_service_center_detail.object.fieldsalesconsultantid.Protect = True
	dw_service_center_detail.object.fieldsalesconsultantid.background.color = 12632256	
End If

//SLH - SR25459
If dw_service_center_detail.GetItemString(1, "microtest") = "Y" Then
	dw_service_center_detail.object.micro_test_ship_type.Visible = True
	dw_service_center_detail.object.mtst_t.Visible = True
Else
	dw_service_center_detail.object.micro_test_ship_type.Visible = False
	dw_service_center_detail.object.mtst_t.Visible = False
End If

dw_service_center_detail.ResetUpdate()

end event

event ue_delete;Messagebox("Informational","Delete is not allowed on this tab.")


end event

event ue_update();/****************************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/29/2006  SR3802        New colums "frz_chep_ind"," frz_loading_instr" has been added.

******************************************************************************************************/


Boolean	lb_ret

Int		li_count
	
Long		ll_rowCount, &
			ll_row, &
			ll_rowfound

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_FindExpression, &
			ls_output
			
Decimal{2}	ld_max_box_weight, &
				ld_price_range_days

u_string_functions	lu_string_functions
datawindowchild		ldw_childdw

this.dw_service_center_detail.AcceptText()
dw_service_center_detail.AcceptText()
if dw_service_center_detail.ModifiedCount() < 1 then
	iw_frame.SetMicroHelp("No Update necessary.")
	return
end if

if (Trim(this.dw_service_center_detail.GetItemString(1, "max_box_weight_ind")) = 'Y') then
	ld_max_box_weight = this.dw_service_center_detail.GetItemNumber(1, "max_box_weight")
	if (ld_max_box_weight <= 0.00) or (ld_max_box_weight > 999.99) then
		MessageBox("Max Box Weight Error", "Max box weight must be greater than 0.00 and less than or equal to 999.99")
		Return
	end if
else
	this.dw_service_center_detail.SetItem(1, "max_box_weight", 0.00)
end if

if (this.dw_service_center_detail.GetItemString(1, "ordered_by_weight_allowed") = 'N') then
	if (this.dw_service_center_detail.GetItemString(1, "ordered_by_weight_default") <> 'Q') then
		MessageBox("Ordered by Weight Default Error", "Ordered by Weight default must be Q Quantity if Ordered by Weight Allowed = N")
		Return
	end if
end if

ld_price_range_days = this.dw_service_center_detail.GetItemNumber(1, "price_range_days")
if (ld_price_range_days < 0.00) or (ld_price_range_days > 30.00) then
		MessageBox("Price Range Days Error", "Price Range Days must be between 0 and 30")
		Return
end if

ls_update = "U" + '~t' + dw_header.getitemstring(1,"customer_id") + '~t' 


SetPointer(HourGlass!)

dw_service_center_detail.SetRedraw(False)

dw_service_center_detail.GetChild('divisioncode', ldw_ChildDW)
ll_rowfound	=	ldw_ChildDW.RowCount()
ls_FindExpression = "type_code = '" + dw_service_center_detail.GetItemString(1, 'divisioncode') + "'"
ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
If  ll_RowFound <= 0 Then 
	iw_frame.SetMicroHelp(dw_service_center_detail.GetItemString(1, 'divisioncode') + ' is an invalid division')
	dw_service_center_detail.SetColumn('divisioncode')
	dw_service_center_detail.SetFocus()
	dw_service_center_detail.SelectText(1, Len(dw_service_center_detail.GetItemString(1, 'divisioncode')))
	return
END IF

ls_update += dw_service_center_detail.getitemstring(1,'divisioncode') + '~t'

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'customertelephonenumber')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'customertelephonenumber') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'customeremailaddress')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'customeremailaddress') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'perishdirectname')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'perishdirectname') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'meatdirectname')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'meatdirectname') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'buyername')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'buyername') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'assistantbuyername')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'assistantbuyername') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'tsrid')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'tsrid') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'fieldsalesconsultantid')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'fieldsalesconsultantid') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'markettype')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'markettype') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'labellist')) Then
	ls_update += "N" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'labellist') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'calllist')) Then
	ls_update += "N" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'calllist') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'ccalllist')) Then
	ls_update += "N" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'ccalllist') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'pricingmenuoptions')) Then
	ls_update += "D" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'pricingmenuoptions') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'forwardwarehouse')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'forwardwarehouse') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'unitofmeasure')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'unitofmeasure') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'protocaltype')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'protocaltype') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'microtest')) Then
	ls_update += "N" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'microtest') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'filler4')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'filler4') + '~t'
End if
//Start changes for SR3802
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'filler3')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'filler3') + '~t'
End if
//End changes for SR3802
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'substitution')) Then
	ls_update += "N" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'substitution') + '~t'
End if
If lu_string_functions.nf_IsEmpty(string(dw_service_center_detail.GetItemnumber(1, 'apptleadhours'))) Then
	ls_update += "0" + '~t'
ELSE
	ls_update += string(dw_service_center_detail.getitemnumber(1,'apptleadhours')) + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'reciever')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'reciever') + '~t'
End if
If lu_string_functions.nf_IsEmpty(string(dw_service_center_detail.GetItemnumber(1, 'unloadinghr'))) Then
	ls_update += "0" + '~t'
ELSE
	ls_update += string(dw_service_center_detail.getitemnumber(1,'unloadinghr')) + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'companypresident')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'companypresident') + '~t'
End if

// replace old backorder claim indicator with space

ls_update += " " + '~t'

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'liccab')) Then
	ls_update += "N" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'liccab') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'customershortname')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'customershortname') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'ExpFrtForward')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'ExpFrtForward') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'RecvTypeInd')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'RecvTypeInd') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'recvCustPhone')) Then
	ls_update += " " + '~r~n'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'recvCustPhone') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'alternaterecvcontact')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'alternaterecvcontact') + '~t'
End if
If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'prodagecodetype')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'prodagecodetype') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'recieverfax')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'recieverfax') + '~t'
End if

ls_update += dw_service_center_detail.getitemstring(1,'qoutedaymon') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'qoutedaytue') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'qoutedaywed') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'qoutedaythu') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'qoutedayfri') + '~t'

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'op_ship_plant')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'op_ship_plant') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'delvdatetype')) Then
	ls_update += " " + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'delvdatetype') + '~t'
End if

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemstring(1, 'delvdatechgdys')) Then
	ls_update += " 0" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'delvdatechgdys') + '~t'
End if
// ibdkdld aug/27/2002 Default loading platform 
ls_update += dw_service_center_detail.getitemstring(1,'filler1') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'filler2') + '~t' //Added for SR3802
ls_update += dw_service_center_detail.getitemstring(1,'dom_exporter_ind') + '~t' //Added for SR5571 dmk
ls_update += dw_service_center_detail.getitemstring(1,'mcool_conf_ind') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'star_ranch_status') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'purch_product_status_ind') + '~t'//Added 12-12 jac

ls_update += dw_service_center_detail.getitemstring(1,'store_door') + '~t'//Added for SR21384 scs
ls_update += dw_service_center_detail.getitemstring(1,'store_door_charge_type') + '~t'//Added for SR21384 scs
ls_update += dw_service_center_detail.getitemstring(1,'deck_code') + '~t'//Added for SR21384 scs
ls_update += Trim(dw_service_center_detail.getitemstring(1,'max_box_weight_ind')) + '~t'//Added for SR21384 scs
ls_update += string(dw_service_center_detail.getitemnumber(1,'max_box_weight')) + '~t'//Added for SR21384 scs
ls_update += dw_service_center_detail.getitemstring(1,'ordered_by_weight_allowed') + '~t'//Added for SR21384 scs
ls_update += dw_service_center_detail.getitemstring(1,'ordered_by_weight_default') + '~t'//Added for SR5571 scs
ls_update += dw_service_center_detail.getitemstring(1,'order_sourcing_allowed') + '~t'//Added for SR22505 scs
ls_update += dw_service_center_detail.getitemstring(1,'order_splitting_allowed') + '~t'//Added for SR22505 scs
ls_update += dw_service_center_detail.getitemstring(1,'line_splitting_allowed') + '~t'//Added for SR22505 scs
ls_update += dw_service_center_detail.getitemstring(1,'splitting_uom') + '~t'//Added for SR22505 scs
ls_update += string(dw_service_center_detail.getitemnumber(1,'price_range_days')) + '~t'//Added for WR22650 dmk

If lu_string_functions.nf_IsEmpty(dw_service_center_detail.GetItemString(1, 'micro_test_ship_type')) Then  //Added for SR25459 slh
	ls_update += "S" + '~t'
ELSE
	ls_update += dw_service_center_detail.getitemstring(1,'micro_test_ship_type') + '~t'
End if

ls_update += dw_service_center_detail.getitemstring(1,'run_unit_cont_ind') + '~t'
ls_update += dw_service_center_detail.getitemstring(1,'manifest_reqd') + '~t'

//messagebox("DATA TO RPC",ls_update)
istr_error_info.se_event_name = "ue_update of uo_tab_service_center"

//lb_ret = iu_cfm001.nf_cfmc28br_service_center_inq(istr_error_info, &
//											ls_update, &
//											ls_output)

lb_ret = iu_ws_orp2.nf_cfmc28fr(ls_update, &
											ls_output,istr_error_info)

If lb_ret Then
	If lu_string_functions.nf_isempty(ls_output) Then
		dw_service_center_detail.insertrow(0)
		dw_service_center_detail.setitem(1,'divisioncode', dw_header.getitemstring(1,"division"))	
		dw_service_center_detail.setitem(1,'divisiondesc', dw_header.getitemstring(1,"division"))
	else
		dw_service_center_detail.ImportString(ls_output)
		dw_service_center_detail.setitem(1, 'divisiondesc', dw_service_center_detail.getitemstring(1,"divisioncode"))
		dw_service_center_detail.setitem(1,'tsrname', dw_service_center_detail.getitemstring(1,"tsrid"))
		dw_service_center_detail.setitem(1,'fieldsalesconsultantname',  &
											  dw_service_center_detail.getitemstring(1,"fieldsalesconsultantid"))
		dw_service_center_detail.ResetUpdate()
		dw_service_center_detail.SetFocus()
	end if
ELSE
	dw_service_center_detail.SetRedraw(True)
	Return 
End if

//SLH - SR25459
If dw_service_center_detail.GetItemString(1, "microtest") = "Y" Then
	dw_service_center_detail.object.micro_test_ship_type.Visible = True
	dw_service_center_detail.object.mtst_t.Visible = True
Else
	dw_service_center_detail.object.micro_test_ship_type.Visible = False
	dw_service_center_detail.object.mtst_t.Visible = False
End If

dw_service_center_detail.ResetUpdate()
dw_service_center_detail.SetRedraw(True)



end event

event ue_getrowcount;Long ll_RowCount
ll_RowCount = This.dw_service_center_detail.RowCount()
Return ll_RowCount

end event

on tp_service_center.create
this.dw_service_center_detail=create dw_service_center_detail
this.Control[]={this.dw_service_center_detail}
end on

on tp_service_center.destroy
destroy(this.dw_service_center_detail)
end on

type dw_service_center_detail from u_base_dw within tp_service_center
event ue_dwndropdown pbm_dwndropdown
integer y = 12
integer width = 3022
integer height = 1276
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_service_center_detail"
boolean border = false
end type

event ue_dwndropdown;/****************************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/29/2006  SR3802        New colums "frz_chep_ind"," frz_loading_instr" has been added.

******************************************************************************************************/


DataWindowChild	ldwc_temp

u_project_functions	lu_project_functions

Choose Case This.GetColumnName()
	Case 'tsrid'
		This.GetChild('tsrid', ldwc_Temp)
		If ldwc_temp.RowCount() <= 1 Then
			lu_project_functions.nf_gettsrs(ldwc_temp, &
					message.is_smanlocation)
		End If
		This.GetChild('tsrname', ldwc_Temp)
		If ldwc_temp.RowCount() <= 1 Then
			lu_project_functions.nf_gettsrs(ldwc_temp, &
					message.is_smanlocation)
		End If
	case 'divisioncode'
		this.getchild('divisioncode', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
		this.getchild('divisiondesc', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
	case 'fieldsalesconsultantid'
		This.GetChild('fieldsalesconsultantid', ldwc_Temp)
		If ldwc_temp.RowCount() <= 1 Then
			lu_project_functions.nf_gettsrs(ldwc_temp, &
					message.is_smanlocation)
		End If
		This.GetChild('fieldsalesconsultantname', ldwc_Temp)
		If ldwc_temp.RowCount() <= 1 Then
			lu_project_functions.nf_gettsrs(ldwc_temp, &
					message.is_smanlocation)
		End If	
	case 'markettype'
		this.getchild('markettype', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
	case 'unitofmeasure'
		this.getchild('unitofmeasure', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
	case 'forwardwarehouse'
		dw_service_center_detail.getchild('forwardwarehouse', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
	case 'op_ship_plant'
		dw_service_center_detail.getchild('op_ship_plant', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
	case 'protocaltype'
		dw_service_center_detail.getchild('protocaltype', ldwc_temp)
		If ldwc_temp.RowCount() <= 1 Then
			wf_filldddw(ldwc_temp)
		end if
//	case 'loadmethod'
//		dw_service_center_detail.getchild('loadmethod', ldwc_temp)
//		If ldwc_temp.RowCount() <= 1 Then
//			wf_filldddw(ldwc_temp)
// Remove per Deb T 09/19/02 ibdkdld
			//ibdkdld Chep Ind 09/18/02
			// The following two lines are to alliviate two rows being selected
			// in the dropdown then filter out the Chep row
//			ldwc_temp.SetFilter("")
//			ldwc_temp.Filter( )
//			ldwc_temp.SetFilter("type_code >'1'")
//			ldwc_temp.Filter( )
//		Else
//			wf_filldddw(ldwc_temp)
//			//ibdkdld Chep Ind 09/18/02
//			// The following two lines are to alliviate two rows being selected
//			// in the dropdown then filter out the Chep row
//			ldwc_temp.SetFilter("")
//			ldwc_temp.Filter( )
//			ldwc_temp.SetFilter("type_code >'1'")
//			ldwc_temp.Filter( )
		//end if
	//Start changes for SR3802
//	case 'frz_loading_instr'
//		dw_service_center_detail.getchild('frz_loading_instr', ldwc_temp)
//		If ldwc_temp.RowCount() <= 1 Then
//			wf_filldddw(ldwc_temp)
//		end if
		//End changes for SR3802
End Choose





end event

event itemchanged;call super::itemchanged;string		ls_FindExpression
long			ll_rowfound, ll_row
datawindowchild	ldw_childdw, &
                  ldwc_temp
						  
decimal{2}	ld_max_box_weight

u_project_functions		lu_project_functions

data = TRIM(data)

choose case dwo.name
	case "divisioncode"
		this.setitem(1, "divisiondesc", data)

	case "tsrid"
		this.setitem(1, "tsrname", data)

	case "fieldsalesconsultantid"
		this.setitem(1, "fieldsalesconsultantname", data)

	CASE "protocaltype"
		this.getchild('protocaltype', ldw_childdw)
		If Len(Trim(data)) > 0 Then 

			ll_row = ldw_childdw.Find('Trim(type_code) = "' + data + '"', 1, ldw_childdw.RowCount())
			If ll_row <= 0 Then
				iw_frame.SetMicroHelp(data + " is an Invalid Protocal Type")
				This.SetFocus()
				This.SelectText(1, Len(data))
				return 1
			Else
				iw_frame.SetMicroHelp("Ready")
			End if
		end if
		//12-12 jac
//		CASE "purch_product_status_ind"
//          This.GetChild('purch_product_status_ind', ldwc_temp)
//          If ldwc_temp.RowCount() <= 1 Then
//	          lu_project_functions.nf_gettutltype(ldwc_temp, 'PPSTATUS')
//          End IF
      //

	CASE "store_door"
		If data = "Y" then
			This.object.store_door_charge_type.Visible = True
			This.object.store_door_label_t.Visible = True
		Else
			This.object.store_door_charge_type.Visible = False
			This.object.store_door_label_t.Visible = False
		End If

	CASE "max_box_weight_ind"
		If data = "Y" then	
			This.object.max_box_weight.Visible = True
		Else
			This.object.max_box_weight.Visible = False
		End If		
		
	CASE "ordered_by_weight_allowed"
		If data = "N" then	
			This.SetItem(1, "ordered_by_weight_default", "Q")
		End If
		
	CASE "order_sourcing_allowed"
		If data = "N" then	
			This.SetItem(1, "order_splitting_allowed", "N")
			This.SetItem(1, "line_splitting_allowed", "N")
			This.object.order_splitting_allowed.Protect = True
			This.object.line_splitting_allowed.Protect = True
			This.object.splitting_uom.Protect = True
			this.object.order_splitting_allowed.background.color = 12632256
			this.object.line_splitting_allowed.background.color = 12632256
			this.object.splitting_uom.background.color = 12632256
		Else	
			This.object.order_splitting_allowed.Protect = False
			this.object.order_splitting_allowed.background.color = 16777215
		End If	
		
	CASE "order_splitting_allowed"
		If data = "N" then	
			This.SetItem(1, "line_splitting_allowed", "N")
			This.object.line_splitting_allowed.Protect = True
			This.object.splitting_uom.Protect = True
			this.object.line_splitting_allowed.background.color = 12632256
			this.object.splitting_uom.background.color = 12632256			
		Else	
			This.object.line_splitting_allowed.Protect = False
			this.object.line_splitting_allowed.background.color = 16777215			
		End If		
		
	CASE "line_splitting_allowed"
		If data = "N" then	
			This.object.splitting_uom.Protect = True
			this.object.splitting_uom.background.color = 12632256			
		Else	
			This.object.splitting_uom.Protect = False
			this.object.splitting_uom.background.color = 16777215			
		End If		
	
	CASE "price_range_days"
		IF integer(data) > 30 or integer(data) < 0 Then
			iw_frame.SetMicroHelp("Price Range Days must be between 0 and 30")
		Else
			iw_frame.SetMicroHelp("Ready")
		End if		
		
	Case "microtest"
		If data = 'Y' then
			This.object.micro_test_ship_type.Visible = True
			This.object.mtst_t.Visible = True
			This.Setitem( 1, "micro_test_ship_type", "H")
		Else
			This.object.micro_test_ship_type.Visible = False
			This.object.mtst_t.Visible = False
		End If
			
end choose
end event

event constructor;call super::constructor;/****************************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/29/2006  SR3802        New colums "frz_chep_ind"," frz_loading_instr" has been added.
Claudia Zarate  21/06/2018                     loadmethod, frz_loading_instr, chep_ind, frz_chep_ind were deleted.

******************************************************************************************************/

datawindowchild ldwc_Temp, &
                ldwc_purch_prod_status
string                     ls_groupid

string                     ls_classname
string							ls_setting
boolean                    lb_rtn
string	ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access	
			
u_string_functions		lu_string_functions
u_project_functions		lu_project_functions

ib_updateable = TRUE

ls_classname = mid(UPPER(this.ClassName()),1,31) + "_PPS"
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)

/*IF isValid(iw_frame) AND IsValid(iw_frame.iu_netwise_data) Then
	ls_groupid = iw_frame.iu_netwise_data.is_groupid	
else  
	ls_groupid = ''			
end if*/

dw_service_center_detail.GetChild('tsrid', ldwc_Temp)

If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_temp, &
			message.is_smanlocation)
End If

dw_service_center_detail.GetChild('tsrname', ldwc_Temp)
If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_temp, &
			message.is_smanlocation)
End If

dw_service_center_detail.getchild('divisioncode', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.getchild('divisiondesc', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.GetChild('fieldsalesconsultantid', ldwc_Temp)
If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_temp, &
			message.is_smanlocation)
End If

dw_service_center_detail.GetChild('fieldsalesconsultantname', ldwc_Temp)
If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_temp, &
			message.is_smanlocation)
End If

dw_service_center_detail.getchild('markettype', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.getchild('unitofmeasure', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.getchild('forwardwarehouse', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.getchild('op_ship_plant', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.getchild('protocaltype', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

//dw_service_center_detail.getchild('loadmethod', ldwc_temp)
//If ldwc_temp.RowCount() <= 1 Then
//	wf_filldddw(ldwc_temp)
//end if

dw_service_center_detail.getchild('prodagecodetype', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

dw_service_center_detail.getchild('delvdatetype', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

//Start changes for SR3802
//dw_service_center_detail.getchild('frz_loading_instr', ldwc_temp)
//If ldwc_temp.RowCount() <= 1 Then
//	wf_filldddw(ldwc_temp)
//end if
//End chages for SR3802

//12-12 jac
This.GetChild('purch_product_status_ind', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettutltype(ldwc_temp, 'PPSTATUS')
End IF
//

//SR25459 SLH
This.GetChild('micro_test_ship_type', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettutltype(ldwc_temp, 'MICROTYP')
End IF

This.GetChild('store_door', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('store_door_charge_type', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('deck_code', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('max_box_weight_ind', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('ordered_by_weight_allowed', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('ordered_by_weight_default', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('order_sourcing_allowed', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('order_splitting_allowed', ldwc_temp) 
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('line_splitting_allowed', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('splitting_uom', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('run_unit_cont_ind', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF

This.GetChild('manifest_reqd', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
End IF


//if ls_groupid <> '' and (ls_groupid = "111" or ls_groupid = "102") then
if(lb_rtn AND (ls_Add_Access = 'Y' or ls_Mod_Access = 'Y')) then
	//do nothing
else
	this.object.purch_product_status_ind.protect = 1
	this.object.purch_product_status_ind.color = 12632256
end if

if	lu_project_functions.nf_issalesmgr() then
	this.object.liccab.protect = 0
	this.object.liccab.background.color = 16777215
else
	this.object.liccab.protect = 1
	this.object.liccab.background.color = 12632256	
end if

if	lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
	this.object.apptleadhours.protect = 0
	this.object.prodagecodetype.protect = 0
	this.object.forwardwarehouse.protect = 0
	this.object.op_ship_plant.protect = 0
	this.object.apptleadhours.background.color = 16777215
	this.object.prodagecodetype.background.color = 16777215
	this.object.forwardwarehouse.background.color = 16777215
	this.object.op_ship_plant.background.color = 16777215
else
	this.object.apptleadhours.protect = 1
	this.object.prodagecodetype.protect = 1
	this.object.forwardwarehouse.protect = 1
	this.object.op_ship_plant.protect = 1
	this.object.apptleadhours.background.color = 12632256
	this.object.prodagecodetype.background.color = 12632256
	this.object.forwardwarehouse.background.color = 12632256
	this.object.op_ship_plant.background.color = 12632256
end if

//if	lu_project_functions.nf_issalesmgr() THEN
//	this.object.loadmethod.protect = 0
//else
//	this.object.loadmethod.protect = 1
//end if

//always unprotect load method 

//this.object.loadmethod.protect = 0

IF isValid(iw_frame) then
	IF IsValid(iw_frame.iu_netwise_data) Then
		If (iw_frame.iu_netwise_data.is_groupid = '102' or iw_frame.iu_netwise_data.is_groupid = '111') then	
			//this.object.chep_ind.protect = 0
			//this.object.chep_ind.background.color = 16777215
			//this.object.frz_chep_ind.protect = 0
			//this.object.frz_chep_ind.background.color = 16777215
			this.object.micro_test_ship_type.protect = 0
			this.object.micro_test_ship_type.background.color = 16777215
		else
			//this.object.chep_ind.protect = 1
			//this.object.chep_ind.background.color = 12632256
			//this.object.frz_chep_ind.protect = 1
			//this.object.frz_chep_ind.background.color = 12632256
			this.object.micro_test_ship_type.protect = 1
			this.object.micro_test_ship_type.background.color = 12632256
		end if
	else
		//this.object.chep_ind.protect = 1
		//this.object.chep_ind.background.color = 12632256
		//this.object.frz_chep_ind.protect = 1
		//this.object.frz_chep_ind.background.color = 12632256
		this.object.micro_test_ship_type.protect = 1
		this.object.micro_test_ship_type.background.color = 12632256
	end if
else
	//this.object.chep_ind.protect = 1
	//this.object.chep_ind.background.color = 12632256
	//this.object.frz_chep_ind.protect = 1
	//this.object.frz_chep_ind.background.color = 12632256
	this.object.micro_test_ship_type.protect = 1
	this.object.micro_test_ship_type.background.color = 12632256
end if
	
	
	
	
	
	
	

end event

type tp_contact from userobject within tab_detail
event type long ue_getrowcount ( integer ai_number )
event ue_retrieve ( )
event ue_update ( )
event ue_delete ( )
event ue_mousemove pbm_mousemove
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Contact"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
pb_new pb_new
pb_last pb_last
pb_next pb_next
pb_previous pb_previous
pb_first pb_first
dw_contact dw_contact
end type

event type long ue_getrowcount(integer ai_number);//if ole_cust_contact.object.CustomerID <> dw_header.getitemstring(1,"customer_id") &
// 	or ole_cust_contact.object.CustomerType <> dw_header.getitemstring(1,"customer_type") &
//  		or ole_cust_contact.object.DivisionCode <> dw_header.getitemstring(1,"division") then
//	Return 0 //This.ole_contact.RowCount()
//else
//	Return 1
//end if
//
Long ll_RowCount
ll_RowCount = This.dw_contact.RowCount()
Return ll_RowCount
//return 0



end event

event ue_retrieve();Boolean	lb_ret
string ls_division 
string ls_contact_type 
string ls_input_string, ls_output_string, cont_id
Integer	li_rtn, id = 0001

istr_error_info.se_event_name = "ue_retrieve of uo_tab_Customer_Contact"
//DataWindowChild ldwc_temp

if dw_header.rowcount() = 0 then return

ls_division = dw_header.getitemstring(1,"division")
ls_contact_type = dw_header.getitemstring(1,"contact_type")

	pb_new.visible = false
	pb_last.visible = false
	pb_next.visible = false
	pb_previous.visible = false
	pb_first.visible = false

if isnull(ls_division) then
//	ole_cust_contact.visible = false
	messagebox("Required", "Division is required for the Customer Contact Tab.")
//	pb_new.visible = false
//	pb_last.visible = false
//	pb_next.visible = false
//	pb_previous.visible = false
//	pb_first.visible = false
	return
end if

if isnull(ls_contact_type) then
//	ole_cust_contact.visible = false
	messagebox("Required", "Contact type is required for the Customer Contact Tab.")
//	pb_new.visible = false
//	pb_last.visible = false
//	pb_next.visible = false
//	pb_previous.visible = false
//	pb_first.visible = false
	return
end if


//ole_cust_contact.object.ModifyRights = true
//dw_contact.object.ModifyRights = true
//ole_cust_contact.object.AddRights = true
//dw_contact.object.AddRights = true

//if ls_contact_type = 'RECEIVER' THEN
//	if ls_division = '00' then
//		ole_cust_contact.object.ModifyRights = false
//		ole_cust_contact.object.AddRights = false
	// jenifer bauer says sales still uses receiver information 
	//	ole_cust_contact.visible = false
	//messagebox("Invalid", "Contact type RECEIVER is valid for division 00 only.")
//	iw_Frame.SetMicroHelp( "Contact type RECEIVER is valid for division 00 only.")
//	return
//	end if
//end if
		
//ole_cust_contact.visible = true 
  dw_contact.visible = true

If wf_contact_has_multiples(ls_contact_type) Then
	pb_new.visible = true
	pb_last.visible = true
	pb_next.visible = true
	pb_previous.visible = true
	pb_first.visible = true
else
     pb_new.visible = false
	pb_last.visible = false
	pb_next.visible = false
	pb_previous.visible = false
	pb_first.visible = false
end if


cont_id = string(id)
If Len(cont_id) = 1 then
	cont_id = "000" + cont_id
end if
	
 
 ls_input_string = dw_header.getitemstring(1, "customer_id") + '~t' + dw_header.getitemstring(1, "customer_type") + '~t' + dw_header.getitemstring(1, "division") + '~t' + dw_header.getitemstring(1, "contact_type") + '~t' + cont_id
 ls_output_string = Space(1000)
 dw_contact.Reset()
//ole_cust_contact.object.CurrentIndex = 0
//ole_cust_contact.object.CustomerID = dw_header.getitemstring(1,"customer_id")
//ole_cust_contact.object.CustomerType = dw_header.getitemstring(1,"customer_type") 
//ole_cust_contact.object.DivisionCode =	dw_header.getitemstring(1,"division") 
//ole_cust_contact.object.ContactType =	dw_header.getitemstring(1,"contact_type")
//ole_cust_contact.object.Inquire

lb_ret = iu_ws_orp5.nf_cfmc32fr(ls_input_string, ls_output_string, istr_error_info)


dw_contact.SetRedraw(False)
dw_contact.Reset()
li_rtn = dw_contact.ImportString(ls_output_string)
If li_rtn = 1 then 
	dw_contact.ResetUpdate()
	//wf_insertrow(dw_contact, 0)
	dw_contact.SetFocus()
	//dw_contact.SetRedraw(True)
else
	If li_rtn = -1  then
		dw_contact.InsertRow(0)
	else
		gw_netwise_frame.SetMicroHelp('Error message')
	end if
end if
dw_contact.SetRedraw(True)

dw_contact.Object.t_type.Text = ls_contact_type
dw_contact.Object.t_primaryalt.Text = 'Primary'

ii_cont_id = Integer(dw_contact.getitemstring(1, "contact_id"))
row_count = Integer(dw_contact.getitemstring(1, "row_count"))
//end if



end event

event ue_update();istr_error_info.se_event_name = "ue_retrieve of uo_tab_Customer_Contact"
string ls_input_string, ls_header_string
Boolean lb_ret


if gw_base_frame.im_base_menu.m_file.m_save.enabled = false then
	return
end if

if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the Customer Contact Tab.")
	return
end if
if isnull(dw_header.getitemstring(1,"contact_type")) then
	messagebox("Required", "Contact type is required for the Customer Contact Tab.")
	return
end if


dw_contact.AcceptText() 
if isnull(dw_contact.GetItemString(1, "first_name")) then 
	dw_contact.SetItem(1, "first_name", "") 
end if
if isnull(dw_contact.getitemstring(1, "last_name")) then 
	dw_contact.SetItem(1, "last_name", "") 
end if
if isnull(dw_contact.getitemstring(1, "contact_title")) then
	dw_contact.SetItem(1, "contact_title", "") 
end if
if isnull(dw_contact.getitemstring(1, "phone_1")) then
	dw_contact.SetItem(1, "phone_1", "") 
end if
if isnull(dw_contact.getitemstring(1, "phone_2")) then 
	dw_contact.SetItem(1, "phone_2", "")
end if
if isnull(dw_contact.getitemstring(1, "speed_no")) then
	dw_contact.SetItem(1, "speed_no", "") 
end if
if isnull(dw_contact.getitemstring(1, "fax_numer")) then 
	dw_contact.SetItem(1, "fax_numer", "") 
end if
if isnull(dw_contact.getitemstring(1, "contact_mail")) then 
	dw_contact.SetItem(1, "contact_mail", "") 
end if
if isnull(dw_contact.getitemstring(1, "contact_comments")) then 
	dw_contact.SetItem(1, "contact_comments", "") 
end if
if isnull(dw_contact.getitemstring(1, "alt_contact_name")) then 
	dw_contact.SetItem(1, "alt_contact_name", "") 
end if
if isnull(dw_contact.getitemstring(1, "contact_id")) then 
	dw_contact.SetItem(1, "contact_id", "") 
end if


ls_input_string = "U" + '~t' +   dw_header.getitemstring(1, "customer_id") + '~t' + dw_header.getitemstring(1, "customer_type") + '~t' + dw_header.getitemstring(1, "division") + '~t' + dw_header.getitemstring(1, "contact_type") + '~t' + dw_contact.getitemstring(1, "first_name") + '~t' + dw_contact.getitemstring(1, "last_name") + '~t' + dw_contact.getitemstring(1, "contact_title") + '~t' + dw_contact.getitemstring(1, "phone_1") + '~t' + dw_contact.getitemstring(1, "phone_2") + '~t' + dw_contact.getitemstring(1, "speed_no") +'~t' + dw_contact.getitemstring(1, "fax_numer") + '~t' + dw_contact.getitemstring(1, "contact_mail") + '~t' + dw_contact.getitemstring(1, "contact_comments") + '~t' + dw_contact.getitemstring(1, "alt_contact_name") + '~t' + dw_contact.getitemstring(1, "contact_id")
lb_ret = iu_ws_orp5.nf_cfmc33fr(ls_input_string, istr_error_info)

If lb_ret then
	gw_netwise_frame.SetMicroHelp('Update Successful')
end if

dw_contact.ResetUpdate()
	

//if lb_ret then
//	dw_contact.SetRedraw(False)
//	dw_contact.ImportSintrgi

//Return lb_ret

//if lb_ret = true then 
//	This.TriggerEvent("ue_retrive")
//	gw_netwise_frame.SetMicroHelp('Update successful')
//end if

//ole_cust_contact.object.Save

end event

event ue_delete;Messagebox("Informational","Delete is not allowed on this tab.")
end event

event ue_mousemove;Close(iw_tooltip)
end event

event constructor;string	ls_server_suffix

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_customer_profile"
istr_error_info.se_user_id 		= sqlca.userid

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" )

//ole_cust_contact.object.UserID = sqlca.userid
//ole_cust_contact.object.Password = sqlca.DBpass
//ole_cust_contact.object.SighonSystem = ls_server_suffix
//
//ole_cust_contact.object.DeleteRights = false //gw_base_frame.im_base_menu.m_file.m_save.enabled



//this.dw_contact = create dw_contact


end event

on tp_contact.create
this.pb_new=create pb_new
this.pb_last=create pb_last
this.pb_next=create pb_next
this.pb_previous=create pb_previous
this.pb_first=create pb_first
this.dw_contact=create dw_contact
this.Control[]={this.pb_new,&
this.pb_last,&
this.pb_next,&
this.pb_previous,&
this.pb_first,&
this.dw_contact}
end on

on tp_contact.destroy
destroy(this.pb_new)
destroy(this.pb_last)
destroy(this.pb_next)
destroy(this.pb_previous)
destroy(this.pb_first)
destroy(this.dw_contact)
end on

type pb_new from picturebutton within tp_contact
event ue_mousemove pbm_mousemove
event ue_get_data ( string as_value )
integer x = 1248
integer y = 1136
integer width = 110
integer height = 96
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string picturename = "new.bmp"
alignment htextalign = left!
end type

event ue_mousemove;//s

//if ib_contact = false then 
//	ib_contact = true
//else 
//	ib_contact = false 
//end if
//
//if  ib_contact = true then
	il_xpos = gw_netwise_frame.PointerX() + 50
	il_ypos = gw_netwise_frame.PointerY() + 50

	is_description =   '99' + "~t" + '99' + "~t" + 'Add new contact' + '~r~n' 
	openwithparm(iw_tooltip, iw_this, iw_frame)
	is_description = ""
//else
//	Close(iw_ToolTip)
//end if 
//
end event

event clicked;dw_contact.Reset()
dw_contact.InsertRow(0)
dw_contact.Object.t_primaryalt.Text = 'New'




end event

event help;//il_xpos = gw_netwise_frame.PointerX() + 50
//il_ypos = gw_netwise_frame.PointerY() + 50
//
//is_description =  'Add new contact'
//openwithparm(iw_tooltip, parent, iw_frame)
//is_description = ""
end event

type pb_last from picturebutton within tp_contact
integer x = 1079
integer y = 1136
integer width = 110
integer height = 96
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string picturename = "LAST1.BMP"
alignment htextalign = left!
end type

event clicked;string cont_id, ls_input_string, ls_output_string, max
integer li_rtn
boolean lb_ret

	max = dw_contact.getitemstring(1, "row_count")

 	ls_input_string = dw_header.getitemstring(1, "customer_id") + '~t' + dw_header.getitemstring(1, "customer_type") + '~t' + dw_header.getitemstring(1, "division") + '~t' + dw_header.getitemstring(1, "contact_type") + '~t' + max
 	ls_output_string = Space(1000)
 	dw_contact.Reset()


	lb_ret = iu_ws_orp5.nf_cfmc32fr(ls_input_string, ls_output_string, istr_error_info)


	//dw_contact.SetRedraw(False)
	dw_contact.Reset()
	li_rtn = dw_contact.ImportString(ls_output_string)
	If li_rtn = 1 then 
		dw_contact.ResetUpdate()
		dw_contact.SetFocus()
		dw_contact.SetRedraw(True)
	else
		If li_rtn < 1  then
			dw_contact.InsertRow(0)
		else
			gw_netwise_frame.SetMicroHelp('Error message')
		end if
	end if
	dw_contact.Object.t_type.Text = dw_header.getitemstring(1, "contact_type")
	dw_contact.Object.t_primaryalt.Text = 'Alternate'
	ii_cont_id = Integer(dw_contact.getitemstring(1, "contact_id"))
end event

type pb_next from picturebutton within tp_contact
integer x = 905
integer y = 1136
integer width = 114
integer height = 96
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string picturename = "NEXT1.BMP"
alignment htextalign = left!
end type

event clicked;string cont_id, ls_input_string, ls_output_string, max
integer li_rtn
boolean lb_ret
integer count

max = dw_contact.getitemstring(1, "row_count")


count = Integer(max)

if ii_cont_id < count then
	ii_cont_id = ii_cont_id + 1

	cont_id = string(ii_cont_id)
	If Len(cont_id) = 1 then
		cont_id = "000" + cont_id
	else 
		if Len(cont_id) = 2 then 
			cont_id = "00" + cont_id
		else 
			if Len(cont_id) = 3 then
				cont_id = "0" + cont_id
			end if
		end if
	end if

	 ls_input_string = dw_header.getitemstring(1, "customer_id") + '~t' + dw_header.getitemstring(1, "customer_type") + '~t' + dw_header.getitemstring(1, "division") + '~t' + dw_header.getitemstring(1, "contact_type") + '~t' + cont_id
 	ls_output_string = Space(1000)
 	dw_contact.Reset()


	lb_ret = iu_ws_orp5.nf_cfmc32fr(ls_input_string, ls_output_string, istr_error_info)


	dw_contact.SetRedraw(False)
	dw_contact.Reset()
	li_rtn = dw_contact.ImportString(ls_output_string)
	If li_rtn = 1 then 
		dw_contact.ResetUpdate()
		//wf_insertrow(dw_contact, 0)
		dw_contact.SetFocus()
		dw_contact.SetRedraw(True)
	else
		If li_rtn < 1  then
			dw_contact.InsertRow(0)
		else
			gw_netwise_frame.SetMicroHelp('Error message')
		end if
	end if
	

	dw_contact.Object.t_type.Text = dw_header.getitemstring(1, "contact_type")
	dw_contact.Object.t_primaryalt.Text = 'Alternate'
ii_cont_id = Integer(dw_contact.getitemstring(1, "contact_id"))
end if


end event

type pb_previous from picturebutton within tp_contact
integer x = 736
integer y = 1136
integer width = 110
integer height = 96
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string picturename = "PRIOR1.BMP"
alignment htextalign = left!
end type

event clicked;string cont_id, ls_input_string, ls_output_string
integer li_rtn
boolean lb_ret
if ii_cont_id > 1 then
	dw_contact.Object.t_primaryalt.Text = 'Alternate'
	
	ii_cont_id = ii_cont_id - 1
	
	if ii_cont_id = 1 then
		dw_contact.Object.t_primaryalt.Text = 'Primary'
	end if

	cont_id = string(ii_cont_id)
	If Len(cont_id) = 1 then
		cont_id = "000" + cont_id
	else 
		if Len(cont_id) = 2 then 
			cont_id = "00" + cont_id
		else 
			if Len(cont_id) = 3 then
				cont_id = "0" + cont_id
			end if
		end if
	end if

 	ls_input_string = dw_header.getitemstring(1, "customer_id") + '~t' + dw_header.getitemstring(1, "customer_type") + '~t' + dw_header.getitemstring(1, "division") + '~t' + dw_header.getitemstring(1, "contact_type") + '~t' + cont_id
 	ls_output_string = Space(1000)
 	dw_contact.Reset()


	lb_ret = iu_ws_orp5.nf_cfmc32fr(ls_input_string, ls_output_string, istr_error_info)


	dw_contact.SetRedraw(False)
	dw_contact.Reset()
	li_rtn = dw_contact.ImportString(ls_output_string)
	If li_rtn = 1 then 
		dw_contact.ResetUpdate()
		//wf_insertrow(dw_contact, 0)
		dw_contact.SetFocus()
		dw_contact.SetRedraw(True)
	else
		If li_rtn < 1  then
			dw_contact.InsertRow(0)
		else
			gw_netwise_frame.SetMicroHelp('Error message')
		end if
	end if
	dw_contact.Object.t_type.Text = dw_header.getitemstring(1, "contact_type")
	ii_cont_id = Integer(dw_contact.getitemstring(1, "contact_id"))
else
//	if ii_cont_id = 1 then
//		dw_contact.Object.t_primaryalt.Text = 'Primary'
//	else 
//			
//			dw_contact.Object.t_primaryalt.Text = 'Alternate'
//	end if
end if



end event

type pb_first from picturebutton within tp_contact
integer x = 558
integer y = 1136
integer width = 119
integer height = 96
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string picturename = "FIRST1.BMP"
alignment htextalign = left!
end type

event clicked;string cont_id, ls_input_string, ls_output_string
integer li_rtn
boolean lb_ret


 	ls_input_string = dw_header.getitemstring(1, "customer_id") + '~t' + dw_header.getitemstring(1, "customer_type") + '~t' + dw_header.getitemstring(1, "division") + '~t' + dw_header.getitemstring(1, "contact_type") + '~t' + "0001"
 	ls_output_string = Space(1000)
 	dw_contact.Reset()


	lb_ret = iu_ws_orp5.nf_cfmc32fr(ls_input_string, ls_output_string, istr_error_info)


	//dw_contact.SetRedraw(False)
	dw_contact.Reset()
	li_rtn = dw_contact.ImportString(ls_output_string)
	If li_rtn = 1 then 
		dw_contact.ResetUpdate()
		//wf_insertrow(dw_contact, 0)
		dw_contact.SetFocus()
		dw_contact.SetRedraw(True)
	else
		If li_rtn < 1  then
			dw_contact.InsertRow(0)
		else
			gw_netwise_frame.SetMicroHelp('Error message')
		end if
	end if
	dw_contact.Object.t_type.Text = dw_header.getitemstring(1, "contact_type")
	dw_contact.Object.t_primaryalt.Text = 'Primary'

	ii_cont_id = Integer(dw_contact.getitemstring(1, "contact_id"))

end event

type dw_contact from u_base_dw_ext within tp_contact
integer y = 108
integer width = 1719
integer height = 976
integer taborder = 11
string dataobject = "d_contact"
boolean border = false
end type

event constructor;call super::constructor;//IF This.RowCount() = 0 Then 
This.InsertRow(0)
ib_updateable = TRUE
end event

type tp_location from userobject within tab_detail
event ue_retrieve ( )
event ue_delete ( )
event type integer ue_update ( integer ai_rtn )
event create ( )
event destroy ( )
event type long ue_getrowcount ( integer somenumber )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Plant Include~r~n/Exclude"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_location dw_location
dw_include_exclude_indicator dw_include_exclude_indicator
end type

event ue_retrieve();Boolean	lb_ret

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_temp
long 		ll_rowcount


ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return 

ls_header += '~tI~r~n'
istr_error_info.se_event_name = "ue_retrieve of uo_tab_location"

SetPointer(HourGlass!)
//lb_ret = iu_cfm001.nf_location_preference(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)

lb_ret=iu_ws_orp2.nf_cfmc09fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)	

lb_ret = true

If lb_ret Then
	dw_Location.SetRedraw(False)
	dw_location.reset()
	dw_location.ImportString(ls_inquire)
	If dw_location.rowcount() > 0 Then
		ls_temp = dw_location.getitemstring(1,'accepted_status')
	else
		ls_temp = 'N'
	end if
	If dw_include_exclude_indicator.rowcount() = 0 then
	   dw_include_exclude_indicator.InsertRow (0)
	end if
	
	dw_include_exclude_indicator.SetItem(1,'include_exlcude',ls_temp)
	dw_include_exclude_indicator.ResetUpdate ( )
	ll_rowcount=dw_location.rowcount()
	dw_location.ResetUpdate()
	wf_insertrow(dw_location, 0)
	dw_location.SetFocus()
	dw_location.SetRedraw(True)
End if


end event

event ue_delete();Boolean	lb_ret

Int	li_counter

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_update



ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)

// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return 
SetPointer(HourGlass!)

ls_header += '~tU~r~n'
istr_error_info.se_event_name = "ue_delete of uo_tab_location"

ll_RowCount = dw_location.RowCount()

Do
	ll_row = dw_location.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_location.SetRedraw(False)

// need to delete New! rows
// also delete NewModified! rows that are selected
dw_location.DeleteRow(ll_RowCount)
ll_RowCount = dw_location.RowCount()
//For li_counter = 1 to ll_RowCount
//	If dw_location.GetItemString(li_counter, 'update_flag') = 'N' And &
//			dw_location.IsSelected(li_counter) Then
//		dw_location.DeleteRow(li_counter)
//		ll_rowcount --
//	End If
//Next


dw_location.SetFilter('IsSelected() And Not IsRowNew()')
dw_location.Filter()
ll_RowCount = dw_location.RowCount()
For li_counter = 1 to ll_RowCount
	dw_location.SetItem(li_counter, 'update_flag', 'D')
Next

ls_Update = dw_location.Describe('DataWindow.Data')

dw_location.SetFilter('')
dw_location.Filter()

dw_location.SetRedraw(True)

//lb_ret = iu_cfm001.nf_location_preference(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)

lb_ret=iu_ws_orp2.nf_cfmc09fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)	

If lb_ret Then
	dw_location.SetRedraw(False)
	
	dw_location.SetFilter('Not IsSelected()')
	dw_location.Filter()
	dw_location.RowsDiscard(1, dw_location.FilteredCount(), Filter!)

	dw_location.SetFilter('')
	dw_location.Sort()

	dw_location.ResetUpdate()
	wf_InsertRow(dw_location, 0)
	dw_location.SetRedraw(True)
End if

dw_location.SetFocus()
end event

event type integer ue_update(integer ai_rtn);Boolean	lb_ret

Int		li_count

Long		ll_RowCount, &
			ll_row, &
			ll_count

String	ls_header, &
			ls_inquire, &
			ls_update
			
u_string_functions	lu_string_functions			

IF Upper(Message.nf_get_app_id()) <> 'ORP' Then
	// To restrict this Tab
	 iw_frame.SetMicroHelp("This Tab is currently available to OP only.")
	Return -1
END IF

ll_rowcount = dw_location.RowCount ( )

FOR ll_count = 1 TO ll_rowcount
 	dw_location.SetItem (ll_count,'accepted_status',dw_include_exclude_indicator.GetItemstring(1,'include_exlcude'))
NEXT

If dw_location.RowCount() = 0 Then Return -1
If dw_location.AcceptText() = -1 Then return -1

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return -1
ls_header += '~tU~r~n'

SetPointer(HourGlass!)
ll_RowCount = dw_location.RowCount()

dw_location.SetRedraw(False)

// need to delete New! rows (but not NewModified!)
dw_location.DeleteRow(ll_RowCount)
ll_RowCount = dw_location.RowCount()

// Validate data here
Do 
	ll_row = dw_location.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_location.GetItemString(ll_row, 'location_name')) Then
			MessageBox("Location", "Location is a required field")
			dw_location.SetColumn('location_name')
			dw_location.SetRow(ll_row)
			dw_location.SetFocus()
			dw_location.SetRedraw(True)
			return -1
		End if
		If dw_location.GetItemString(ll_Row, 'update_flag') = 'N' Then
			dw_location.SetItem(ll_Row, "update_flag", "A")
		Else
			dw_location.SetItem(ll_Row, "update_flag", "U")
		End If
	End if

Loop While ll_row > 0 and ll_row < (ll_RowCount)		


ls_Update = lu_string_functions.nf_BuildUpdateString ( dw_location )

If lu_string_functions.nf_IsEmpty(ls_Update) Then 
	wf_insertrow(dw_location, 0)
	dw_location.SetRedraw(True)
	return -1
End if

istr_error_info.se_event_name = "ue_update of uo_tab_location"

//lb_ret = iu_cfm001.nf_location_preference(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)
											
lb_ret=iu_ws_orp2.nf_cfmc09fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)									

If not lb_ret Then
	dw_location.SetRedraw(True)
	Return -1
End if

// This is done like this because we needed to make a quick change.
For li_count = 1 To dw_location.RowCount()
	dw_location.SetItem(li_count, "update_flag", " ")
Next
dw_location.ResetUpdate()
wf_insertrow(dw_location, 0)

dw_location.SetRedraw(True)
Return 0

end event

on tp_location.create
this.dw_location=create dw_location
this.dw_include_exclude_indicator=create dw_include_exclude_indicator
this.Control[]={this.dw_location,&
this.dw_include_exclude_indicator}
end on

on tp_location.destroy
destroy(this.dw_location)
destroy(this.dw_include_exclude_indicator)
end on

event ue_getrowcount;Long ll_RowCount
ll_RowCount = This.dw_location.RowCount()
Return ll_RowCount
end event

type dw_location from u_base_dw_ext within tp_location
event clicked pbm_dwnlbuttonclk
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
event itemerror pbm_dwnitemvalidationerror
event ue_postconstructor pbm_custom55
integer x = 23
integer y = 84
integer width = 2459
integer height = 1196
integer taborder = 31
string dataobject = "d_customer_profile_location"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;call super::clicked;////  This code overrides ancestor script
////
//// Verify if a valid row was clicked on
//If row < 1 Then
//	Return
//ELSE
//	Call Super::Clicked
//End If


end event

event constructor;call super::constructor;is_selection = '4'
ib_updateable = TRUE

end event

event itemchanged;call super::itemchanged;Boolean	lb_error

Long	ll_CurrentRow, &
		ll_RowCount

String	ls_Find, &
			ls_GetText, &
			ls_location_type

//ll_CurrentRow = This.GetRow()
//ls_GetText = This.GetText()
//

ll_CurrentRow = row

Choose Case This.GetColumnName()
Case "location_type"
	This.SetItem(ll_CurrentRow, "complex_code", "")
	
Case "location_code", "complex_code"
	Select Location_type 
			into :ls_location_type
			from locations
			WHERE locations.location_code = :data
			or locations.complex_code = :data
			USING SQLCA;
			
	If SQLCA.SQLCode = 100 then
		iw_Frame.SetMicroHelp("Invalid Location")
		return 1
	end if
	
	This.Setitem(ll_CurrentRow, "Location_type", ls_location_type)
	
	ls_Find = "location_type = '" + This.GetItemString(ll_CurrentRow, 'location_type') + &
					"' and location_name = '" + data + "'"
	If This.Find(ls_Find , 1, ll_CurrentRow - 1) > 0 Then
		lb_error = True
	End if

	ll_RowCount = This.RowCount()
	If ll_CurrentRow < ll_rowCount Then
		If This.Find(ls_Find , ll_CurrentRow + 1, ll_RowCount) > 0 Then
			lb_error = True
		End if
	End If

	If lb_Error Then
		iw_Frame.SetMicroHelp("Duplicate Rows are not allowed")
		return 1
	else
		iw_Frame.SetMicroHelp("Ready")
	End if

Case 'effective_date'
	If Date(ls_GetText) < Today() Then
		MessageBox("Effective Date", "The Effective Date must be greater" + &
				" than Today.")
		Return 1
	End If
End Choose
If ll_CurrentRow = This.RowCount() Then
	This.SelectRow ( ll_currentrow, False )
	wf_insertrow(This, 0)
End if


end event

event itemerror;call super::itemerror;return 1
end event

type dw_include_exclude_indicator from u_base_dw_ext within tp_location
integer y = 16
integer width = 576
integer height = 68
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_include_exclude_indicator"
boolean border = false
end type

type tp_carrier from userobject within tab_detail
event ue_retrieve ( )
event ue_delete ( )
event ue_update ( )
event create ( )
event destroy ( )
boolean visible = false
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
boolean enabled = false
long backcolor = 12632256
string text = "Carrier"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_carrier dw_carrier
end type

event ue_retrieve();Boolean	lb_ret

String	ls_header, &
			ls_inquire, &
			ls_update


ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return 

istr_error_info.se_event_name = "ue_retrieve of uo_tab_location"

ls_header += '~tI~r~n'

SetPointer(HourGlass!)
//lb_ret = iu_cfm001.nf_carrier_preference(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)
											
lb_ret = iu_ws_orp2.nf_cfmc05fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)

If lb_ret Then
	dw_carrier.SetRedraw(False)
	dw_carrier.ImportString(ls_inquire)
	wf_insertrow(dw_carrier, 0)
	dw_carrier.ResetUpdate()
	dw_carrier.SetFocus()
	dw_carrier.SetRedraw(True)
End if


end event

event ue_delete();Boolean	lb_ret

Int	li_counter

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_update



ls_header = dw_header.Describe("DataWindow.Data")
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return 
SetPointer(HourGlass!)

ls_header += '~tU~r~n'
istr_error_info.se_event_name = "ue_delete of uo_tab_carrier"

ll_RowCount = dw_carrier.RowCount()

Do
	ll_row = dw_carrier.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_carrier.SetRedraw(False)

// need to delete New! rows
// also delete NewModified! rows that are selected
dw_carrier.DeleteRow(ll_RowCount)
ll_RowCount = dw_carrier.RowCount()
For li_counter = 1 to ll_RowCount
	If dw_carrier.GetItemString(li_counter, 'update_flag') = 'N' And &
			dw_carrier.IsSelected(li_counter) Then
		dw_carrier.DeleteRow(li_counter)
	End If
Next


dw_carrier.SetFilter('IsSelected() And Not IsRowNew()')
dw_carrier.Filter()
ll_RowCount = dw_carrier.RowCount()
For li_counter = 1 to ll_RowCount
	dw_carrier.SetItem(li_counter, 'update_flag', 'D')
Next

ls_Update = dw_carrier.Describe('DataWindow.Data')

dw_carrier.SetFilter('')
dw_carrier.Filter()

dw_carrier.SetRedraw(True)

//lb_ret = iu_cfm001.nf_carrier_preference(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)

lb_ret = iu_ws_orp2.nf_cfmc05fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)


If lb_ret Then
	dw_carrier.SetRedraw(False)
	
	dw_carrier.SetFilter('Not IsSelected()')
	dw_carrier.Filter()
	dw_carrier.RowsDiscard(1, dw_carrier.FilteredCount(), Filter!)

	dw_carrier.SetFilter('')
	dw_carrier.Sort()

	wf_InsertRow(dw_carrier, 0)

	dw_carrier.ResetUpdate()
	dw_carrier.SetRedraw(True)
End if

dw_carrier.SetFocus()
end event

event ue_update();//Boolean	lb_ret
//
//Int		li_counter, &
//			li_count
//
//Long		ll_RowCount, &
//			ll_row
//
//String	ls_header, &
//			ls_inquire, &
//			ls_update
//
//
//If dw_carrier.RowCount() = 0 Then Return
//If dw_carrier.AcceptText() = -1 Then return
//
//ls_header = dw_header.Describe("DataWindow.Data")
//// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
//If IsNull(ls_Header) or Len(ls_header) < 9 Then return 
//ls_header += '~tU~r~n'
//
//SetPointer(HourGlass!)
//ll_RowCount = dw_Carrier.RowCount()
//
//dw_carrier.SetRedraw(False)
//
//// need to delete New! rows (but not NewModified!)
//dw_carrier.DeleteRow(ll_RowCount)
//ll_RowCount = dw_Carrier.RowCount()
//
//// Validate data here
//Do 
//	ll_row = dw_carrier.GetNextModified(ll_row, Primary!)
//	If ll_row > 0 Then
//		// Make sure all fields are filled in
//		If f_IsEmpty(dw_carrier.GetItemString(ll_row, 'division_code')) Then
//			MessageBox("Division", "Division is a required field")
//			dw_carrier.SetColumn('division_code')
//			dw_carrier.SetRow(ll_row)
//			dw_carrier.SetFocus()
//			return
//		End if
//
//		If f_IsEmpty(dw_carrier.GetItemString(ll_row, 'carrier_code')) Then
//			MessageBox("Carrier", "Carrier is a required field")
//			dw_carrier.SetColumn('carrier_code')
//			dw_carrier.SetRow(ll_row)
//			dw_carrier.SetFocus()
//			return
//		End if
//		If dw_carrier.GetItemString(ll_Row, 'update_flag') = 'N' Then
//			dw_carrier.SetItem(ll_Row, "update_flag", "A")
//		Else
//			dw_carrier.SetItem(ll_Row, "update_flag", "U")
//		End If
//	End if
//Loop While ll_row > 0 and ll_row < ll_RowCount		
//
//ls_Update = Parent.wf_BuildUpdateString(dw_carrier)
//
//If f_IsEmpty(ls_Update) Then 
//	wf_insertrow(dw_carrier, 0)
//	dw_carrier.SetRedraw(True)
//	return 
//End if
//
//istr_error_info.se_event_name = "ue_update of uo_tab_location"
//
//lb_ret = iu_cfm001.nf_carrier_preference(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)
//
//If lb_ret Then
//	dw_carrier.SetRedraw(False)
//// This is done like this because we needed to make a quick change.
//	For li_count = 1 To dw_carrier.RowCount()
//		dw_carrier.SetItem(li_count, "update_flag", " ")
//	Next
//	dw_carrier.SetRedraw(True)
//	dw_carrier.ResetUpdate()
//End if
//
//If Parent.igo_ActiveTab = This Then wf_insertrow(dw_carrier, 0)
//dw_carrier.SetRedraw(True)
//
end event

on tp_carrier.create
this.dw_carrier=create dw_carrier
this.Control[]={this.dw_carrier}
end on

on tp_carrier.destroy
destroy(this.dw_carrier)
end on

type dw_carrier from u_base_dw_ext within tp_carrier
event clicked pbm_dwnlbuttonclk
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
event itemerror pbm_dwnitemvalidationerror
event ue_postconstructor pbm_custom55
integer x = 5
integer y = 12
integer width = 2432
integer height = 1276
integer taborder = 72
boolean bringtotop = true
string dataobject = "d_customer_profile_carrier"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;call super::clicked;// Verify if a valid row was clicked on
If row < 1 Then
	Return
ELSE
	Call Super::Clicked
End If


end event

on constructor;call u_base_dw_ext::constructor;is_selection = '4'
ib_updateable = TRUE

end on

event itemchanged;call super::itemchanged;Boolean	lb_error
Long		ll_CurrentRow, &
			ll_RowCount

String	ls_Find, &
			ls_GetText, &
			ls_ColumnName


ls_GetText = This.GetText()
ll_CurrentRow = This.GetRow()
ls_ColumnName = This.GetColumnName()


Choose Case ls_ColumnName
Case 'carrier_code' 
	ls_Find = "division_descr = '" + This.GetItemString(ll_CurrentRow, 'division_code') + &
					"' and carrier_descr = '" + ls_GetText + "'"

Case 'division_code' 
	ls_Find = "division_descr = '" + ls_GetText + "' and carrier_descr = '" + &
				This.GetItemString(ll_CurrentRow, 'carrier_code') + "'"

Case 'effective_date'
	If Date(ls_GetText) < Today() Then
		MessageBox("Effective Date", "The Effective Date must be greater" + &
				" than Today.")
		Return 1
	End If
	Return
Case Else 
	Return
End Choose	

If This.Find(ls_Find , 1, ll_CurrentRow - 1) > 0 Then
	lb_error = True
End if

ll_RowCount = This.RowCount()
If ll_CurrentRow < ll_rowCount Then
	If This.Find(ls_Find , ll_CurrentRow + 1, ll_RowCount) > 0 Then
		lb_error = True
	End if
End If

If lb_Error Then
	This.SelectText(1, Len(ls_GetText))
	iw_Frame.SetMicroHelp("Duplicate Rows are not allowed")
		return 1
End if

If ll_CurrentRow = This.RowCount() Then
	wf_insertrow(This, 0)
End if
end event

event itemerror;call super::itemerror;return 1
end event

event ue_postconstructor;call super::ue_postconstructor;//DataWindowChild	ldwc_division, &
//						ldwc_division_descr, &
//						ldwc_carrier, &
//						ldwc_carrier_descr
//
//ib_updateable = True
//
//This.GetChild("division_code", ldwc_division)
//This.GetChild("division_descr", ldwc_division_descr)
//
//Message.iw_utldata.wf_gettutltype("DIVCODE", ldwc_division)
//ldwc_division.ShareData(ldwc_division_descr)
//ldwc_division.Sort()
//
//This.GetChild("carrier_code", ldwc_carrier)
//This.GetChild("carrier_descr", ldwc_carrier_descr)
//
//wf_GetCarrierData(ldwc_carrier)
//ldwc_carrier.ShareData(ldwc_carrier_descr)
//ldwc_carrier.Sort()
//
//is_selection = '3'
end event

type tp_prodsub from userobject within tab_detail
event ue_retrieve ( )
event ue_delete ( )
event type integer ue_update ( integer nzhz )
event create ( )
event destroy ( )
event type long ue_getrowcount ( integer somevalue )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Prod Sub"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
string powertiptext = "Add Vailid Product Subs"
dw_product dw_product
end type

event ue_retrieve();Boolean	lb_ret

String	ls_header, &
			ls_inquire, &
			ls_update
			
ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return 

istr_error_info.se_event_name = "ue_retrieve of uo_tab_product"

ls_header += '~tI~r~n'

SetPointer(HourGlass!)
dw_product.Reset()
//lb_ret = iu_cfm001.nf_product_substitution(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)
											
lb_ret = iu_ws_orp2.nf_cfmc01fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)
											
If lb_ret Then
	dw_product.SetRedraw(False)
	dw_product.ImportString(ls_inquire)
	wf_insertrow(dw_product, 0)
	dw_product.ResetUpdate()
	dw_product.SetFocus()
	dw_product.SetRedraw(True)
End if


end event

event ue_delete();Boolean	lb_ret

Int	li_counter

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_update

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return 
SetPointer(HourGlass!)

ls_header += '~tU~r~n'
istr_error_info.se_event_name = "ue_delete of uo_tab_product"

ll_RowCount = dw_product.RowCount()

Do
	ll_row = dw_product.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then 
	iw_frame.SetMicroHelp("select a row to delete")
	return
END IF

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_product.SetRedraw(False)

// need to delete New! rows
// also delete NewModified! rows that are selected
dw_product.DeleteRow(ll_RowCount)
//ll_RowCount = dw_product.RowCount()
//For li_counter = 1 to ll_RowCount
//	If dw_product.GetItemString(li_counter, 'update_flag') = 'N' And &
//			dw_product.IsSelected(li_counter) Then
//		dw_product.DeleteRow(li_counter)
//		ll_rowcount --
//	End If
//Next


dw_product.SetFilter('IsSelected() And Not IsRowNew()')
dw_product.Filter()
ll_RowCount = dw_product.RowCount()
For li_counter = 1 to ll_RowCount
	dw_product.SetItem(li_counter, 'update_flag', 'D')
Next

ls_Update = dw_product.Describe('DataWindow.Data')

dw_product.SetFilter('')
dw_product.Filter()

dw_product.SetRedraw(True)

//lb_ret = iu_cfm001.nf_product_substitution(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)

lb_ret = iu_ws_orp2.nf_cfmc01fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)

If lb_ret Then
	dw_product.SetRedraw(False)
	
	dw_product.SetFilter('Not IsSelected()')
	dw_product.Filter()
	dw_product.RowsDiscard(1, dw_product.FilteredCount(), Filter!)

	dw_product.SetFilter('')
	dw_product.Sort()

	wf_InsertRow(dw_product, 0)

	dw_product.ResetUpdate()
	dw_product.SetRedraw(True)
End if

dw_product.SetFocus()
end event

event type integer ue_update(integer nzhz);Boolean	lb_ret

Int		li_count
	
Long		ll_rowCount, &
			ll_row, &
			ll_count

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_product_Code
			
u_string_functions	lu_string_functions			

IF Upper(Message.nf_get_app_id()) <> 'ORP' Then
	// To restrict this Tab
	 iw_frame.SetMicroHelp("This Tab is currently available to OP only.")
	Return -1
END IF

If dw_product.RowCount() = 0 Then Return -1
If dw_product.AcceptText() = -1 Then return -1

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,9)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return -1
ls_header += '~tU~r~n'

SetPointer(HourGlass!)

dw_product.SetRedraw(False)

// need to delete New! rows (but not NewModified!)
ll_RowCount = dw_product.RowCount()
dw_product.DeleteRow(ll_RowCount)
ll_RowCount = dw_product.RowCount()

// Validate data here
Do  
	ll_row = dw_product.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_product.GetItemString(ll_row, 'sku_product_code')) Then
			MessageBox("Sku Product Code", "Sku Product Code is a required field")
			dw_product.SetColumn('sku_product_code')
			dw_product.SetRow(ll_row)
			dw_product.SetFocus()
			dw_product.SetRedraw(True)
			return -1
		else
			ls_product_Code = dw_product.GetItemString(ll_row, 'sku_product_code') + &
									space(10 - len(dw_product.GetItemString(ll_row, 'sku_product_code')))
			Select Count(*) 
			into :ll_count
			from sku_products
			WHERE sku_products.sku_product_code = :ls_Product_code
			USING SQLCA;
			IF ll_Count < 1 AND SQLCA.SQLCode = 0 then 
				iw_Frame.SetMicroHelp("Invalid/Inactive product code")
				dw_product.SetColumn('sku_product_code')
				dw_product.SetRow(ll_row)
				dw_product.SetFocus()
				dw_product.SetRedraw(True)
				Return -1
			END IF
		End if

		If lu_string_functions.nf_IsEmpty(dw_product.GetItemString(ll_row, 'sub_product_code')) Then
			MessageBox("Sub Product Code", "Sub Product Code is a required field")
			dw_product.SetColumn('sub_product_code')
			dw_product.SetRow(ll_row)
			dw_product.SetFocus()
			dw_product.SetRedraw(True)
			return -1
		else
			ls_product_Code = dw_product.GetItemString(ll_row, 'sub_product_code') + &
									space(10 - len(dw_product.GetItemString(ll_row, 'sub_product_code')))
			Select Count(*) 
			into :ll_count
			from sku_products
			WHERE sku_products.sku_product_code = :ls_Product_code
			USING SQLCA;
			IF ll_Count < 1 AND SQLCA.SQLCode = 0 then 
				iw_Frame.SetMicroHelp("Invalid/Inactive sub product code")
				dw_product.SetColumn('sub_product_code')
				dw_product.SetRow(ll_row)
				dw_product.SetFocus()
				dw_product.SetRedraw(True)
				Return -1
			end if
		End if
	End if
Loop While ll_row > 0 and ll_row < ll_RowCount		

ls_Update = lu_string_functions.nf_BuildUpdateString(dw_product)


If lu_string_functions.nf_IsEmpty(ls_Update) Then 
	wf_insertrow(dw_product, 0)
	dw_product.SetRedraw(True)
	return -1
End if

istr_error_info.se_event_name = "ue_update of uo_tab_product"

//lb_ret = iu_cfm001.nf_product_substitution(istr_error_info, &
//											ls_header, &
//											ls_inquire, &
//											ls_update)

lb_ret = iu_ws_orp2.nf_cfmc01fr(ls_header, &
											ls_inquire, &
											ls_update, istr_error_info)						

If Not lb_ret Then
	dw_product.SetRedraw(True)
	Return -1
End if

// This is done like this because we needed to make a quick change.
For li_count = 1 To dw_product.RowCount()
	dw_product.SetItem(li_count, "update_flag", " ")
Next
wf_insertrow(dw_product, 0)
dw_product.ResetUpdate()
dw_product.SetRedraw(True)
Return 0

end event

on tp_prodsub.create
this.dw_product=create dw_product
this.Control[]={this.dw_product}
end on

on tp_prodsub.destroy
destroy(this.dw_product)
end on

event ue_getrowcount;Long ll_RowCount
ll_RowCount = This.dw_product.RowCount()
Return ll_RowCount
end event

type dw_product from u_base_dw_ext within tp_prodsub
event clicked pbm_dwnlbuttonclk
event constructor pbm_constructor
event itemchanged pbm_dwnitemchange
event itemerror pbm_dwnitemvalidationerror
event itemfocuschanged pbm_dwnitemchangefocus
event rowfocuschanged pbm_dwnrowchange
integer y = 36
integer width = 2629
integer height = 1244
integer taborder = 71
string dataobject = "d_customer_profile_product_sub"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;//  This code overrides ancestor script
//
// Verify if a valid row was clicked on
If row < 1 Then
	Return
ELSE
	Call Super::Clicked
End If


end event

event constructor;u_project_functions	lu_project_functions

// pjm 09/10/2014 the accepted ind dropdown is now dynamic
datawindowchild ldwc_temp

This.getchild('accepted_ind', ldwc_temp)

If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)  
end if

is_selection = '4'
ib_updateable = TRUE

if lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() or &
		lu_project_functions.nf_issvcpresident() then
	This.object.accepted_ind.Protect=0
	This.Modify ( "accepted_ind.Background.Color='16777215'" ) 
else
	This.object.accepted_ind.Protect=1
	This.Modify ( "accepted_ind.Background.Color='12632256'" ) 
end if


end event

event itemchanged;call super::itemchanged;Integer						li_return

Long							ll_CurrentRow, &
								ll_RowCount, &
								ll_tab, &
								ll_tab2

String						ls_output, &
								ls_GetText, &
								ls_ProductDescription, &
								ls_sub_product, &
								ls_sub_product_code, &
								ls_sub_product_descr, &
								ls_sequence_number, &
								ls_accepted_ind, &
								ls_effective_date, &
								ls_update_flag, &
								ls_importstring, &
								ls_product
								
u_string_functions		lu_string_functions

ll_CurrentRow = This.GetRow()
ls_GetText = This.GetText()

Choose Case This.GetColumnName() 
	Case 'sku_product_code'

		If lu_string_functions.nf_isempty(ls_GetText) Then
			ls_ProductDescription = ''
			This.SetItem(row, "sku_short_descr", ls_ProductDescription)
			Return
		else
			ls_product = ls_GetText
			ls_product = TRIM(ls_product) + space(10 - len(TRIM(ls_product)))
			SELECT short_description   
	   		INTO :ls_ProductDescription
	   		FROM sku_products  
	 			WHERE sku_product_code = :ls_product;  
			This.SetItem(row, "sku_short_descr", ls_ProductDescription)	 
			This.SetItem(row, "update_flag", "A")
		End IF
		
	Case 'sub_product_code'

		If lu_string_functions.nf_isempty(ls_GetText) Then
			ls_ProductDescription = ''
			This.SetItem(row, "sub_product_descr", ls_ProductDescription)
			Return
		else
			ls_product = ls_GetText
			ls_product = TRIM(ls_product) + space(10 - len(TRIM(ls_product)))
			SELECT short_description   
	   		INTO :ls_ProductDescription
	   		FROM sku_products  
	 			WHERE sku_product_code = :ls_product;  
			This.SetItem(row, "sub_product_descr", ls_ProductDescription)
		End IF	
		If This.GetITemString(row, "update_flag") = ' ' then
			This.SetItem(row, "update_flag", "U")
		end if
			
	Case 'effective_date'
		If Date(ls_GetText) <= Today() Then
			MessageBox("Effective Date", "The Effective Date must be greater" + &
				" than Today.")
			Return 1
		else
			If This.GetITemString(row, "update_flag") = ' ' then
				This.SetItem(row, "update_flag", "U")
			end if
		end if
		
	Case 'accepted_ind'
		If This.GetITemString(row, "update_flag") = ' ' then
			This.SetItem(row, "update_flag", "U")
		end if
		
End Choose	

If ll_CurrentRow = This.RowCount() Then
	wf_insertrow(This, 0)
End if
end event

event itemerror;call super::itemerror;return 1
end event

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;
//String	ls_visible
//
//
//If This.GetColumnName() = 'accepted_ind' Then
//	ls_visible = '1'
//Else
//	ls_visible = '0'
//End if
//
//This.Modify('r_accept.visible = ' + ls_visible)


end on

on rowfocuschanged;call u_base_dw_ext::rowfocuschanged;This.Modify("current_row.text = '" + String(This.GetRow()) + "'")
end on

event rbuttondown;call super::rbuttondown;Integer						li_return, &
								li_temp

Boolean						lb_insert_flag

Long							ll_RowCount, &
								ll_Rowfound, &
								ll_tab, &
								ll_tab2

String						ls_output, &
								ls_GetText, &
								ls_ProductDescription, &
								ls_sub_product, &
								ls_sub_product_code, &
								ls_sub_product_descr, &
								ls_sequence_number, &
								ls_accepted_ind, &
								ls_effective_date, &
								ls_update_flag, &
								ls_importstring
								
u_string_functions		lu_string_functions								
								
if row <1 then
	return -1
end if

This.AcceptText()
ll_RowCount = This.RowCount()
ls_GetText = This.GetItemString(row,"sku_product_code")
	
If lu_string_functions.nf_isempty(ls_GetText) Then
	Return -1
End IF

iw_frame.SetMicroHelp("Wait.. Inquiring on product " + ls_GetText)

IF Not IsValid( iu_orp001 ) THEN
	iu_orp001	=  CREATE u_orp001
END IF

IF Not IsValid( iu_ws_orp3 ) THEN
	iu_ws_orp3	=  CREATE u_ws_orp3
END IF

istr_error_info.se_function_name = ''
istr_error_info.se_event_name = 'ItemChanged dw_product'
istr_error_info.se_procedure_name = 'orpo66ar'
//li_return = iu_orp001.nf_orpo66ar_getsubs( istr_Error_Info, &
//								ls_GetText, ls_output )
								
li_return = iu_ws_orp3.uf_orpo66fr_getsubs( istr_Error_Info, &
								ls_GetText, ls_output )								
 
IF li_return <> 0 Or ls_output = '' THEN
	This.SetItem( This.GetRow(), This.GetColumn() + 1, &
				ls_ProductDescription )	
	return 1
END IF

OpenWithParm(w_customer_sub_products, ls_output)
ls_sub_product = message.StringParm

IF ls_sub_product = "" Then 
	MessageBox("Substitution Products", "There were no Substitution" + &
		" Products Selected.  You must Select a Substitution Product.")
	Return 1
ElseIf ls_sub_product = "Abort" Then
	Return 1
End IF

This.SetRedraw(False)

ll_tab = pos(ls_output, "~t", 1) + 1
ll_tab2 = pos(ls_output, "~r", 1)
ls_ProductDescription = mid(ls_output, ll_tab, (ll_tab2 - ll_tab))
ls_sequence_number = "0"
ls_accepted_ind = "N"
ls_effective_date = String(RelativeDate(Today(), 1), "mm/dd/yyyy")
ls_update_flag = "A"

This.DeleteRow(row)

//ll_CurrentRow -= 1
Do While Len(ls_sub_product) > 0
	ls_sub_product_code = lu_string_functions.nf_gettoken(ls_sub_product, "~t")
	ls_sub_product_descr = lu_string_functions.nf_gettoken(ls_sub_product, "~r")
//	If This.Find("sub_product_code = '" + ls_sub_product_code + "' And sku_product_code '" + &
//			this.getitemstring(row,'sku_product_code') + "'" &
//			,1, This.RowCount()) = 0 Then
		ls_importstring += ls_GetText + '~t' + &
								ls_ProductDescription + '~t' + &
								ls_sub_product_code + '~t' + &
								ls_sub_product_descr + '~t' + &
								ls_sequence_number + '~t' + &
								ls_accepted_ind + '~t' + &
								ls_effective_date + '~t' + &
								ls_update_flag + "~r~n"
		ll_RowCount += 1
//	Else
//		MessageBox("Substitution Product", ls_sub_product_code + &
//				" is a Duplicate Substitution Product Code for " + this.getitemstring(row,'sku_product_code') &
//				+ ".  It will not be Added.")  
//	End If
Loop 

This.ImportString(ls_importstring) 

ll_RowCount = This.RowCount()

for li_temp = 1 to ll_Rowcount
	if lu_string_functions.nf_isempty(This.GetItemString(li_temp,"sku_product_code")) then
		This.DeleteRow(li_temp)
		ll_Rowcount --
	end if
next

wf_insertrow(This,0)

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
This.SetRedraw(True)


end event

type tp_product_age from userobject within tab_detail
event ue_retrieve ( )
event ue_delete ( )
event ue_update ( )
event type long ue_getrowcount ( integer somevalue )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Product Age~r~nOn Delivery"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_product_age dw_product_age
end type

event ue_retrieve();Boolean	lb_ret

String	ls_input, &
			ls_output, &
			ls_customer, &
			ls_short_name
			
datawindowchild ldwc_Temp
			
u_string_functions		lu_string_functions
u_project_functions		lu_project_functions

istr_error_info.se_event_name = "ue_retrieve of tp_product_age"

ls_input = dw_header.getitemstring(1,"customer_id") + '~t' +   &
								 dw_header.getitemstring(1,"customer_type") + '~rn'

SetPointer(HourGlass!)

dw_product_age.Reset()



//lb_ret = iu_cfm001.nf_cfmc25br_product_age_inq(istr_error_info, &
//											ls_input, &
//											ls_output)

lb_ret = iu_ws_orp2.nf_cfmc25fr(ls_input, &
											ls_output, istr_error_info)

If lb_ret Then
	If lu_string_functions.nf_isempty(ls_output) Then
		Messagebox("ERROR","No data found.")
		return
	else
		dw_product_age.SetRedraw(False)
		dw_product_age.ImportString(ls_output)
		dw_product_age.ResetUpdate()
		dw_product_age.SetFocus()
		dw_product_age.SetRedraw(True)
	end if
End if

end event

event ue_delete;return

end event

event ue_update();Boolean	lb_ret

Int		li_count
	
Long		ll_rowCount, &
			ll_row, &
			ll_rowfound

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_output
			
u_string_functions	lu_string_functions
datawindowchild		ldw_childdw


ls_inquire = dw_header.getitemstring(1,"customer_id") + '~t' +   &
								 dw_header.getitemstring(1,"customer_type") + '~t'
ls_update = ""

this.dw_product_age.accepttext()

SetPointer(HourGlass!)

dw_product_age.SetRedraw(False)

ll_RowCount = dw_product_age.rowcount()
ll_row = 0

Do 
	ll_row = dw_product_age.GetNextModified(ll_row, Primary!)
	if ll_row > 0 then
		ls_update += dw_product_age.getitemstring(ll_row,'groupage') + '~t' &
			+ dw_product_age.getitemstring(ll_row,'minageondelivery') + '~t' &
			+ dw_product_age.getitemstring(ll_row,'maxageondelivery') + '~r~n'
	end if
	
Loop While ll_row > 0 and ll_row < ll_RowCount	

istr_error_info.se_event_name = "ue_update of tp_product_age"

If lu_string_functions.nf_isempty(ls_update) Then
	iw_frame.setmicrohelp("No Update needed.")
	lb_ret = false
	dw_product_age.SetRedraw(True)
else
	ls_update = ls_inquire + ls_update
//	lb_ret = iu_cfm001.nf_cfmc27br_product_age_update(istr_error_info, &
//												ls_update, &
//												ls_output)
    
	 lb_ret = iu_ws_orp2.nf_cfmc27fr(ls_update, &
												ls_output,istr_error_info)
	 
end if

If Not lb_ret Then
	dw_product_age.SetRedraw(True)
	Return 
End if
tp_product_age.TriggerEvent("ue_retrieve")
dw_product_age.ResetUpdate()
dw_product_age.SetRedraw(True)

end event

event ue_getrowcount;Long ll_RowCount
ll_RowCount = This.dw_product_age.RowCount()
Return ll_RowCount
end event

on tp_product_age.create
this.dw_product_age=create dw_product_age
this.Control[]={this.dw_product_age}
end on

on tp_product_age.destroy
destroy(this.dw_product_age)
end on

type dw_product_age from u_base_dw_ext within tp_product_age
integer x = 9
integer y = 4
integer width = 1627
integer height = 892
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_product_age_group_detail"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;//this.insertrow(0)
ib_updateable = TRUE
end event

event doubleclicked;call super::doubleclicked;string		ls_groupage

if row > 0 and dwo.name = "groupdescription" then
	
	ls_groupage = this.getitemstring(row,"groupage")
	
	OpenWithParm(w_productpopup, ls_groupage)
	
end if

end event

event itemchanged;call super::itemchanged;//choose case dwo.name
//	case "minageondelivery"
//		if this.getitemstring(row,"maxageondelivery") < data then
//			this.setitem(row,"maxageondelivery",data)
////			iw_frame.setmicrohelp("Min days has to be less than or equal to Max days.")
//////			this.SetColumn('minageondelivery')
//////			this.SetRow(row)
//////			this.SetFocus()
////			return 1
//		else
//			iw_frame.setmicrohelp("Ready")
//		end if
//		
//	case "maxageondelivery"
//		if data < this.getitemstring(row,"minageondelivery") then
//			this.setitem(row,"minageondelivery",data)
////			iw_frame.setmicrohelp("Max days has to be greater than or equal to Min days.")
//////			this.SetColumn('maxageondelivery')
//////			this.SetRow(row)
//////			this.SetFocus()
////			return 1
//		else
//			iw_frame.setmicrohelp("Ready")
//		end if
//		
//end choose
end event

type tp_temp_recorder from userobject within tab_detail
event type long ue_getrowcount ( integer somevalue )
event ue_retrieve ( )
event ue_update ( )
event ue_delete ( )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Temp~r~nRecorder"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_temp_recorder_dtl dw_temp_recorder_dtl
dw_temp_recorder_hdr dw_temp_recorder_hdr
end type

event type long ue_getrowcount(integer somevalue);Long ll_RowCount
ll_RowCount = This.dw_temp_recorder_dtl.RowCount()
Return ll_RowCount
end event

event ue_retrieve();Integer	li_ret

String	ls_header, &
			ls_input, &
			ls_output, &
			ls_detail, &			
			ls_recorder_string
			
Long		ll_row
			
DataWindowChild	ldwc_temp			
			
if dw_header.rowcount() = 0 then return

if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the Temp Recorder Tab.")
	return
end if

IF dw_header.getitemstring(1,"customer_type") = "B" then
	messagebox("Required", "Can only inquire on Ship To Customers for the Temp Recorder Tab.")
	return
end if

If dw_header.getitemstring(1,"customer_type") = "C" then
	messagebox("Required", "Can only inquire on Ship To Customers for the Temp Recorder Tab.")
	return
end if


ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
istr_error_info.se_event_name = "ue_retrieve of uo_tab_temp_rec"

ls_header += "~tI~t" + dw_temp_recorder_hdr.getitemstring(1,'product_state') + "~r~n"

SetPointer(HourGlass!)
//dw_temp_recorder.Reset()
//li_ret = iu_cfm001.nf_inq_upd_temp_recorder(istr_error_info, &
//											ls_header, &
//											ls_input, &
//											ls_output, &
//											ls_recorder_string)

dw_temp_recorder_hdr.Reset()
dw_temp_recorder_dtl.Reset()
li_ret = iu_ws_orp2.nf_cfmc34fr(istr_error_info, ls_header, ls_input, ls_output, ls_detail, ls_recorder_string)

If li_ret = 0 Then
//	dw_temp_recorder.SetRedraw(False)
//	dw_temp_recorder.ImportString(ls_output)
//	dw_temp_recorder.ResetUpdate()
//	dw_temp_recorder.SetFocus()
//	dw_temp_recorder.SetRedraw(True)
//	
//	dw_temp_recorder.getchild('fresh_rec_type', ldwc_temp)
//	If ldwc_temp.RowCount() <= 1 Then
//		ldwc_temp.ImportString(ls_recorder_string)
//	End if
//	
//	dw_temp_recorder.getchild('froz_rec_type', ldwc_temp)
//	If ldwc_temp.RowCount() <= 1 Then
//		ldwc_temp.ImportString(ls_recorder_string)
//	End if
//	
//	If dw_temp_recorder.GetItemString(1, "fresh_rec_rules") = 'LB' Then
//		dw_temp_recorder.Object.fresh_qty_text_t.Text = "Pounds:"
//	Else
//		dw_temp_recorder.Object.fresh_qty_text_t.Text = "Recorders:"
//	End If
//	
//	If dw_temp_recorder.GetItemString(1, "froz_rec_rules") = 'LB' Then
//		dw_temp_recorder.Object.froz_qty_text_t.Text = "Pounds:"
//	Else
//		dw_temp_recorder.Object.froz_qty_text_t.Text = "Recorders:"
//	End If
//	
//	dw_temp_recorder.SetFocus()
//	dw_temp_recorder.SetRedraw(True)
	
//new logic
	dw_temp_recorder_hdr.SetRedraw(False)
	dw_temp_recorder_hdr.ImportString(ls_output)
	dw_temp_recorder_hdr.ResetUpdate()
	dw_temp_recorder_hdr.SetFocus()
	dw_temp_recorder_hdr.SetRedraw(True)
	
	dw_temp_recorder_hdr.getchild('rec_type', ldwc_temp)
	If ldwc_temp.RowCount() <= 1 Then
		ldwc_temp.ImportString(ls_recorder_string)
	End if
	
	If dw_temp_recorder_hdr.GetItemString(1, "rec_rules") = 'LB' Then
		dw_temp_recorder_hdr.Object.rec_qty_t.Text = "Pounds:"
	Else
		dw_temp_recorder_hdr.Object.rec_qty_t.Text = "Recorders:"
	End If
	
	If dw_temp_recorder_hdr.GetItemString(1, "rec_required") = 'Y' Then
		dw_temp_recorder_dtl.enabled = true
	Else
		dw_temp_recorder_dtl.enabled = false
	End If
	
	dw_temp_recorder_hdr.SetFocus()
	dw_temp_recorder_hdr.SetRedraw(True)
	
	
	dw_temp_recorder_dtl.SetRedraw(False)
	dw_temp_recorder_dtl.ImportString(ls_detail)
//	dw_temp_recorder_dtl.insertrow(0)
	wf_insertrow(dw_temp_recorder_dtl, 0)
	dw_temp_recorder_dtl.ResetUpdate()
//	dw_temp_recorder_dtl.SetFocus()
	dw_temp_recorder_dtl.SetRedraw(True)
End if

//test
//dw_temp_recorder_hdr.insertrow(0)
//dw_temp_recorder_dtl.insertrow(0)

end event

event ue_update();String	ls_header, &
			ls_input, &
			ls_output, &
			ls_detail, &
			ls_recorder_string, &
			ls_update
			
Integer	li_ret

Long		ll_rowcount, &
			ll_row
			
u_string_functions	lu_string_functions				

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
istr_error_info.se_event_name = "ue_retrieve of uo_tab_temp_rec"

dw_temp_recorder_dtl.AcceptText()
dw_temp_recorder_hdr.AcceptText()

ls_header += '~tU~t' + dw_temp_recorder_hdr.getitemstring(1,'product_state') + '~r~n'

If lu_string_functions.nf_IsEmpty(dw_temp_recorder_hdr.GetItemString(1,"product_state")) Then
	MessageBox("Product State Required","A Product State is Required")
	Return
Else
	If Trim(dw_temp_recorder_hdr.GetItemString(1,"rec_required")) = 'Y' Then
		If lu_string_functions.nf_IsEmpty(dw_temp_recorder_hdr.GetItemString(1,"rec_type")) Then
			MessageBox("Recorder Type Required","A Recorder Type is Required")
			Return
		End If
		If lu_string_functions.nf_IsEmpty(dw_temp_recorder_hdr.GetItemString(1,"rec_rules")) Then
			MessageBox("Recorder Rule Required","A Recorder Rule is Required")
			Return
		End If
		If Trim(dw_temp_recorder_hdr.GetItemString(1,"rec_rules")) = 'LB' Then
			If dw_temp_recorder_hdr.GetItemNumber(1,"rec_qty") = 0 Then
				MessageBox("Recorder Pounds Required","Recorder Pounds is Required")
				Return
			End If
		End If
		If dw_temp_recorder_hdr.GetItemNumber(1,"rec_max_qty") = 0 Then
			If (Trim(dw_temp_recorder_hdr.GetItemString(1,"rec_rules")) = 'OR') or &
				(Trim(dw_temp_recorder_hdr.GetItemString(1,"rec_rules")) = 'ST') Then
	//			Continue
			Else
				MessageBox("Maximum Recorders Required","Maximum Number of Recorders is Required")
				Return
			End If
		End If
	End If
End If
		
ls_input = dw_temp_recorder_hdr.Describe("DataWindow.Data") + '~r~n'

//Detail
ll_RowCount = dw_temp_recorder_dtl.RowCount()

// Validate data here
Do 
	ll_row = dw_temp_recorder_dtl.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_temp_recorder_dtl.GetItemString(ll_row, 'type_code')) Then
			MessageBox("Type Code", "Type Code is a required field")
			dw_temp_recorder_dtl.SetColumn('type_code')
			dw_temp_recorder_dtl.SetRow(ll_row)
			dw_temp_recorder_dtl.SetFocus()
			dw_temp_recorder_dtl.SetRedraw(True)
			return
		Else
			If dw_temp_recorder_dtl.GetItemString(ll_row, 'type_code') = 'P' &
			And lu_string_functions.nf_IsEmpty(dw_temp_recorder_dtl.GetItemString(ll_row, 'product_code')) Then
				MessageBox("Product Code", "Product Code is a required field")
				dw_temp_recorder_dtl.SetColumn('product_code')
				dw_temp_recorder_dtl.SetRow(ll_row)
				dw_temp_recorder_dtl.SetFocus()
				dw_temp_recorder_dtl.SetRedraw(True)
				return
			End If
			If dw_temp_recorder_dtl.GetItemString(ll_row, 'type_code') = 'G' &
			And lu_string_functions.nf_IsEmpty(dw_temp_recorder_dtl.GetItemString(ll_row, 'group_code')) Then
				MessageBox("Group Code", "Group Code is a required field")
				dw_temp_recorder_dtl.SetColumn('group_code')
				dw_temp_recorder_dtl.SetRow(ll_row)
				dw_temp_recorder_dtl.SetFocus()
				dw_temp_recorder_dtl.SetRedraw(True)
				return
			End If
		End if
		If dw_temp_recorder_dtl.GetItemString(ll_Row, 'update_flag') = 'N' Then
			dw_temp_recorder_dtl.SetItem(ll_Row, "update_flag", "A")
//		Else
//			dw_location.SetItem(ll_Row, "update_flag", "U")
		End If
	End if

Loop While ll_row > 0 and ll_row < (ll_RowCount - 1)		

// need to delete New! rows (but not NewModified!)
ll_RowCount = dw_temp_recorder_dtl.RowCount()
dw_temp_recorder_dtl.DeleteRow(ll_RowCount)

ls_Update = lu_string_functions.nf_BuildUpdateString ( dw_temp_recorder_dtl )

SetPointer(HourGlass!)

ls_detail = ls_Update

li_ret = iu_ws_orp2.nf_cfmc34fr(istr_error_info, ls_header, ls_input, ls_output, ls_detail, ls_recorder_string)

If (li_ret = 0) Then
	itp_activetab.PostEvent("ue_retrieve")
	iw_frame.SetMicroHelp("Update Successful.")
End if

Return
end event

event ue_delete();Boolean	lb_ret, lb_hasUpdates

Int	li_counter, &
		li_ret

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_input, &
			ls_detail, &
			ls_output, &
			ls_recorder_string
			
ll_RowCount = dw_temp_recorder_dtl.RowCount()

Do
	ll_row = dw_temp_recorder_dtl.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then 
	If MessageBox("Delete", "Temp Recorder charged will be deleted at header level, Continue?", Question!, YesNo!, 2) = 2 Then 
		return
	else
		ls_header = dw_header.Describe("DataWindow.Data")
		ls_header = mid(ls_header,1,13)
		ls_header +=  '~tD~t' + dw_temp_recorder_hdr.getitemstring(1,'product_state') + '~r~n'

		li_ret = iu_ws_orp2.nf_cfmc34fr(istr_error_info, ls_header, ls_input, ls_output, ls_detail, ls_recorder_string)
		
		If li_ret = 0 Then
			itp_activetab.PostEvent("ue_retrieve")
			gw_netwise_frame.SetMicroHelp('Delete Successful')	
		end if
		return
	end if
end if

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_temp_recorder_dtl.SetRedraw(False)

ll_RowCount = dw_temp_recorder_dtl.RowCount()
dw_temp_recorder_dtl.DeleteRow(ll_RowCount)
lb_hasUpdates = false	// look for updates. if they have any, stop processing.
For li_counter = 1 to ll_RowCount
	if dw_temp_recorder_dtl.IsSelected(li_counter) Then
		If dw_temp_recorder_dtl.GetItemString(li_counter, 'update_flag') = 'A' Then
			dw_temp_recorder_dtl.DeleteRow(li_counter) // allow delete, record doesnt exist in db. remove from grid
		else 
			If dw_temp_recorder_dtl.GetItemString(li_counter, 'update_flag') = 'U' then
				lb_hasUpdates = true
			end if
		End if
	End If
Next

if(lb_hasUpdates) then 
	MessageBox("Delete Error", "You have pending changes. You must save before deleting rows.")
	dw_temp_recorder_dtl.InsertRow(0)
	ll_RowCount = dw_temp_recorder_dtl.RowCount()
     dw_temp_recorder_dtl.SetItem(ll_RowCount, 'update_flag', 'A')
	dw_temp_recorder_dtl.uf_ChangeRowStatus( ll_rowcount, NotModified!)
	dw_temp_recorder_dtl.SetRedraw(True)
else
	ls_header = dw_header.Describe("DataWindow.Data")
	ls_header = mid(ls_header,1,13)
	ls_header +=  '~tU~t' + dw_temp_recorder_hdr.getitemstring(1,'product_state') + '~r~n'
	
	dw_temp_recorder_dtl.SetFilter('IsSelected() And Not IsRowNew()')
	dw_temp_recorder_dtl.Filter()
	ll_RowCount = dw_temp_recorder_dtl.RowCount()

	For li_counter = 1 to ll_RowCount
			dw_temp_recorder_dtl.SetItem(li_counter, 'update_flag', 'D')
	Next

	ll_row = 0
	Do
		ll_row = dw_temp_recorder_dtl.GetNextModified(ll_row, Primary!)
		if ll_row > 0 then
			ls_detail += dw_temp_recorder_dtl.GetItemString(ll_row, 'type_code') + '~t' &
					 + dw_temp_recorder_dtl.GetItemString(ll_row, 'product_code') + '~t' &
				  + trim(dw_temp_recorder_dtl.GetItemString(ll_row, 'group_code')) + '~t' & 
				  + dw_temp_recorder_dtl.GetItemString(ll_row, 'update_flag') +  '~r~n'
		end if
	Loop While ll_row > 0 and ll_row < ll_RowCount
	
	li_ret = iu_ws_orp2.nf_cfmc34fr(istr_error_info, ls_header, ls_input, ls_output, ls_detail, ls_recorder_string)
	
	If (li_ret = 0) Then
		itp_activetab.PostEvent("ue_retrieve")
		gw_netwise_frame.SetMicroHelp('Delete Successful')	
	End if
end if
dw_temp_recorder_dtl.SetRedraw(True)
end event

on tp_temp_recorder.create
this.dw_temp_recorder_dtl=create dw_temp_recorder_dtl
this.dw_temp_recorder_hdr=create dw_temp_recorder_hdr
this.Control[]={this.dw_temp_recorder_dtl,&
this.dw_temp_recorder_hdr}
end on

on tp_temp_recorder.destroy
destroy(this.dw_temp_recorder_dtl)
destroy(this.dw_temp_recorder_hdr)
end on

type dw_temp_recorder_dtl from u_base_dw_ext within tp_temp_recorder
integer x = 59
integer y = 340
integer width = 1422
integer height = 908
integer taborder = 11
string dataobject = "d_temp_recorder_dtl"
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;Long		ll_CurrentRow, ll_New_Row

Choose case dwo.name
	Case "type_code"	
		If data = 'P' Then
			This.SetItem(row, "group_code","")
			This.SetColumn("product_code")
		End If
		If data = "G" Then
			This.SetItem(row, "product_code","")
			This.SetColumn("group_code")
		End If
End Choose

ll_CurrentRow = This.GetRow()

If ll_CurrentRow = This.RowCount() Then
//	wf_insertrow(This, 0)
	ll_New_Row = This.InsertRow(0)
	This.SetItem(ll_New_Row, 'update_flag', 'N')
End if

data = Trim(data)
end event

event clicked;call super::clicked;If row = 0 Then return
If (dwo.Name = 'type_code' or dwo.Name = 'product_code' or dwo.Name = 'group_code') Then return

IF row > 0 Then
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If
end event

event constructor;call super::constructor;ib_updateable = TRUE
end event

type dw_temp_recorder_hdr from u_base_dw_ext within tp_temp_recorder
integer x = 59
integer y = 48
integer width = 4155
integer height = 260
integer taborder = 11
string dataobject = "d_temp_recorder_hdr"
end type

event constructor;call super::constructor;datawindowchild ldwc_Temp

ib_updateable = TRUE

This.getchild('product_state', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

This.getchild('rec_required', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

This.getchild('rec_charged', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if

This.getchild('rec_rules', ldwc_temp)
If ldwc_temp.RowCount() <= 1 Then
	wf_filldddw(ldwc_temp)
end if


end event

event itemchanged;call super::itemchanged;data = Trim(data)

Choose case dwo.name
	Case "rec_rules"
		If data = 'LB' Then
			This.Object.rec_qty_t.Text = "Pounds:"
		Else
			This.Object.rec_qty_t.Text = "Recorders:"
		End If
	Case "rec_required"	
		If data = 'N' Then
			This.SetItem(1, "rec_charged","Y")
			This.SetItem(1, "rec_type","")
			This.SetItem(1, "rec_rules","")
			This.SetItem(1, "rec_qty",0)
			This.SetItem(1, "rec_max_qty",0)
			dw_temp_recorder_dtl.enabled = false
		Else	
			dw_temp_recorder_dtl.enabled = true
		End If
	Case "product_state" 
		itp_activetab.PostEvent("ue_retrieve")
End Choose


end event

type tp_product_cat from userobject within tab_detail
event ue_retrieve ( )
event type long ue_getrowcount ( integer somenumber )
event type integer ue_update ( )
event type integer ue_delete ( )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "MCOOL"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_product_category dw_product_category
end type

event ue_retrieve();Integer	li_ret

String	ls_header, &
			ls_input_string, &
			ls_cool_string, &
			ls_prod_cat_string, &
			ls_cust_excl_string, &
			ls_temp
long 		ll_row

u_project_functions	lu_project_functions

if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the MCOOL Tab.")
	return
end if


ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
// Minimum len of ls_header should be 13 chars. 7 for cust_id, tab, cust_type, tab, div, tab

ls_header +=  '~t' + 'I' + '~t'    // inquire option //

istr_error_info.se_event_name = "ue_retrieve of tab_prod_cat"

SetPointer(HourGlass!)

// pjm 08/11/2014 change service to cfmc36br.
//li_ret = iu_cfm001.nf_cfmc35br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_input_string, &
//											ls_cool_string, &
//											ls_prod_cat_string)
//
//li_ret = iu_cfm001.nf_cfmc36br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_input_string, &
//											ls_cool_string, &
//											ls_prod_cat_string, &
//											ls_cust_excl_string)
											
li_ret = iu_ws_orp2.nf_cfmc36fr(istr_error_info, &
											ls_header, &
											ls_input_string, &
											ls_cool_string, &
											ls_prod_cat_string, &
											ls_cust_excl_string)											
											
If li_ret = 0 Then
	//gf_cool_code(ls_cool_string,25)  // testing
	dw_product_category.Reset()
	ids_product_category.Reset()
	ids_product_category.ImportString(ls_prod_cat_string)

	If li_ret = 0 then
		// pjm 08/11/2014 added label ids and exclusive plants
		wf_process_cool_code_string(ls_cool_string)
	//	messagebox("Exclude string",ls_cust_excl_string) // testing
	   wf_set_prod_cat()
		wf_process_cust_excl_string(ls_cust_excl_string)
		wf_set_label_excl_plant_descs()
		gw_netwise_frame.SetMicroHelp('Inquire Successful')	
		ll_row = dw_product_category.InsertRow(0)
		dw_product_category.ResetUpdate()    
		dw_product_category.SetRow(ll_row)
	End If
End If

//If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
//	iw_frame.im_menu.mf_Enable('m_delete')
//Else	
//	iw_frame.im_menu.mf_Disable('m_delete')
//End If


SetPointer(Arrow!)


end event

event type long ue_getrowcount(integer somenumber);Return 0
end event

event type integer ue_update();Integer	li_ret

String	ls_header, &
			ls_input_string, &
			ls_cool_string, &
			ls_prod_cat_string, &
			ls_cust_excl_string, &
			ls_temp, &
			ls_cool_code, &
			ls_label_id
			
long 		ll_rowcount, &
			ll_row

datawindowchild ldwc_labels

u_string_functions	lu_string_functions

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
// Minimum len of ls_header should be 13 chars. 7 for cust_id, tab, cust_type, tab, div, tab

//If IsNull(ls_Header) or Len(ls_header) < 11 Then return 

ls_header += 'U' + '~t'    // inquire option //

istr_error_info.se_event_name = "ue_update of tab_prod_cat"

SetPointer(HourGlass!)

// need to delete New! rows (but not NewModified!)
//dw_product_category.DeleteRow(ll_RowCount)
ll_RowCount = dw_product_category.RowCount()

// Validate data here
Do 
	ll_row = dw_product_category.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_product_category.GetItemString(ll_row, 'prod_category')) Then
			MessageBox("Product Category", "Product Category is a required field")
			dw_product_category.SetColumn('prod_category')
			dw_product_category.SetRow(ll_row)
			dw_product_category.SetFocus()
			dw_product_category.SetRedraw(True)
			return -1
		End if
		If lu_string_functions.nf_IsEmpty(dw_product_category.GetItemString(ll_row, 'cool_code')) Then
			MessageBox("Cool Code", "Cool Code is a required field")
			is_prod_category = dw_product_category.GetItemString(ll_row,'prod_category')
			wf_fill_cool_code_dddw()
			dw_product_category.SetColumn('cool_code')
			dw_product_category.SetRow(ll_row)
			dw_product_category.SetFocus()
			dw_product_category.SetRedraw(True)
			return -1
		End if
		// pjm 08/20/2014 added for new column label_id
		If lu_string_functions.nf_IsEmpty(dw_product_category.GetItemString(ll_row, 'label_id')) Then
			MessageBox("Label ID", "Label ID is a required field")
			is_prod_category = dw_product_category.GetItemString(ll_row,'prod_category')
			is_cool_code = dw_product_category.GetItemString(ll_row,'cool_code')
			wf_fill_labels_dddw()
			dw_product_category.SetColumn('label_id')
			dw_product_category.SetRow(ll_row)
			dw_product_category.SetFocus()
			dw_product_category.SetRedraw(True)
			return -1
		End if		
		ls_cool_code = Trim(dw_product_category.GetItemString(ll_row,"cool_code"))
		ls_label_id = Trim(dw_product_category.GetItemString(ll_row,"label_id"))
		If Len(ls_label_id) > 0 Then
			dw_product_category.GetChild('label_id', ldwc_labels)
			li_ret = ldwc_labels.SetFilter("cool_code = '" + ls_cool_code + &
						"' and label_id = '" + ls_label_id + "'" )
			li_ret = ldwc_labels.Filter()
			If ldwc_labels.RowCount() < 0 Then
				MessageBox("Cool Code", "Label ID does not match Cool Code")
				is_prod_category = dw_product_category.GetItemString(ll_row,'prod_category')
				is_cool_code = dw_product_category.GetItemString(ll_row,'cool_code')
				wf_fill_labels_dddw()
				dw_product_category.SetColumn('cool_code')
				dw_product_category.SetRow(ll_row)
				dw_product_category.SetFocus()
				dw_product_category.SetRedraw(True)
				return -1
			End If
		End If
		If dw_product_category.GetItemString(ll_Row, 'update_flag') = 'A' Then
			dw_product_category.SetItem(ll_Row, "update_flag", "A")
		Else
			dw_product_category.SetItem(ll_Row, "update_flag", "U")
		End If
	End if

Loop While ll_row > 0 and ll_row < (ll_RowCount)		


// pjm 08/19/2014 - changed for service 36br
//ls_input_string = lu_string_functions.nf_BuildUpdateString ( dw_product_category )
wf_build_update_string(ls_input_string)
 
//return 1 // test
If lu_string_functions.nf_IsEmpty(ls_input_string) Then 
	dw_product_category.SetRedraw(True)
	gw_netwise_frame.SetMicroHelp('No lines found for update')
	return -1
End if

// pjm 08/11/2014 change service to cfmc36br.
//li_ret = iu_cfm001.nf_cfmc35br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_input_string, &
//											ls_cool_string, &
//											ls_prod_cat_string)
//

//li_ret = iu_cfm001.nf_cfmc36br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_input_string, &
//											ls_cool_string, &
//											ls_prod_cat_string, &
//											ls_cust_excl_string)				
											
li_ret = iu_ws_orp2.nf_cfmc36fr(istr_error_info, &
											ls_header, &
											ls_input_string, &
											ls_cool_string, &
											ls_prod_cat_string, &
											ls_cust_excl_string)
											
If li_ret = 0 then
	
//	ll_RowCount = dw_product_category.RowCount()
//
//	For ll_row = 1 to ll_RowCount
//		dw_product_category.SetItem(ll_row,"update_flag", ' ')
//	Next
//	
//	// delete last empty row before sort
//	dw_product_category.DeleteRow(ll_RowCount)
//
//	dw_product_category.SetFilter('')
//	dw_product_category.SetSort("prod_category A, cool_code A")
//	dw_product_category.Sort()
//	
//	// insert row at end after sort
//	dw_product_category.InsertRow(0)


	This.TriggerEvent("ue_retrieve")

	gw_netwise_frame.SetMicroHelp('Update Successful')	
End If

SetPointer(Arrow!)

Return li_ret
end event

event type integer ue_delete();Int	li_counter, &
		li_ret

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_cool_string, &
			ls_cust_excl_string, &
			ls_prod_cat_string, &
			ls_cool_code, &
			ls_label_id

datawindowchild ldwc_labels

u_string_functions	lu_string_functions

u_project_functions	lu_project_functions

If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
	// do nothing -- continue
Else	
	RETURN 1
End If

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
// Minimum len of ls_header should be 13 chars. 7 for cust_id, tab, cust_type, tab, div, tab

//If IsNull(ls_Header) or Len(ls_header) < 11 Then return 

ls_header += 'U' + '~t'    // inquire option //

istr_error_info.se_event_name = "ue_delete of tab_prod_cat"

SetPointer(HourGlass!)

ll_RowCount = dw_product_category.RowCount()

Do
	ll_row = dw_product_category.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return 1

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return 1

// Validate data here
Do 
	ll_row = dw_product_category.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_product_category.GetItemString(ll_row, 'prod_category')) Then
			MessageBox("Product Category", "Product Category is a required field")
			dw_product_category.SetColumn('prod_category')
			dw_product_category.SetRow(ll_row)
			dw_product_category.SetFocus()
			dw_product_category.SetRedraw(True)
			return -1
		End if
		If lu_string_functions.nf_IsEmpty(dw_product_category.GetItemString(ll_row, 'cool_code')) Then
			MessageBox("Cool Code", "Cool Code is a required field")
			is_prod_category = dw_product_category.GetItemString(ll_row,'prod_category')
			wf_fill_cool_code_dddw()
			dw_product_category.SetColumn('cool_code')
			dw_product_category.SetRow(ll_row)
			dw_product_category.SetFocus()
			dw_product_category.SetRedraw(True)
			return -1
		End if
		// pjm 08/20/2014 added for new column label_id
		If lu_string_functions.nf_IsEmpty(dw_product_category.GetItemString(ll_row, 'label_id')) Then
			MessageBox("Label ID", "Label ID is a required field")
			is_prod_category = dw_product_category.GetItemString(ll_row,'prod_category')
			is_cool_code = dw_product_category.GetItemString(ll_row,'cool_code')
			wf_fill_labels_dddw()
			dw_product_category.SetColumn('label_id')
			dw_product_category.SetRow(ll_row)
			dw_product_category.SetFocus()
			dw_product_category.SetRedraw(True)
			return -1
		End if		
		ls_cool_code = Trim(dw_product_category.GetItemString(ll_row,"cool_code"))
		ls_label_id = Trim(dw_product_category.GetItemString(ll_row,"label_id"))
		If Len(ls_label_id) > 0 Then
			dw_product_category.GetChild('label_id', ldwc_labels)
			li_ret = ldwc_labels.SetFilter("cool_code = '" + ls_cool_code + &
						"' and label_id = '" + ls_label_id + "'" )
			li_ret = ldwc_labels.Filter()
			If ldwc_labels.RowCount() < 0 Then
				MessageBox("Cool Code", "Label ID does not match Cool Code")
				is_prod_category = dw_product_category.GetItemString(ll_row,'prod_category')
				is_cool_code = dw_product_category.GetItemString(ll_row,'cool_code')
				wf_fill_labels_dddw()
				dw_product_category.SetColumn('cool_code')
				dw_product_category.SetRow(ll_row)
				dw_product_category.SetFocus()
				dw_product_category.SetRedraw(True)
				return -1
			End If
		End If
		If dw_product_category.GetItemString(ll_Row, 'update_flag') = 'A' Then
			dw_product_category.SetItem(ll_Row, "update_flag", "A")
		Else
			dw_product_category.SetItem(ll_Row, "update_flag", "U")
		End If
	End if

Loop While ll_row > 0 and ll_row < (ll_RowCount)		

dw_product_category.SetRedraw(False)

// need to delete New! rows
// also delete NewModified! rows that are selected
dw_product_category.DeleteRow(ll_RowCount)
ll_RowCount = dw_product_category.RowCount()
For li_counter = 1 to ll_RowCount
	// pjm 09/02/2014 added for excluded plants
	// IF dw_product_category.IsSelected(li_counter) Then wf_delete_excluded_plants(li_counter)
	// end pjm
	If dw_product_category.GetItemString(li_counter, 'update_flag') = 'A' And &
			dw_product_category.IsSelected(li_counter) Then
		dw_product_category.DeleteRow(li_counter)
		// pjm 09/02/2014 added for excluded plants
		// wf_delete_excluded_plants(li_counter)
		ll_rowcount --
	End If
Next

dw_product_category.SetFilter('IsSelected() And Not IsRowNew()')
//dw_product_category.SetFilter('IsSelected()')
dw_product_category.Filter()
ll_RowCount = dw_product_category.RowCount()

For li_counter = 1 to ll_RowCount
	dw_product_category.SetItem(li_counter, 'update_flag', 'D')
	ls_update = ls_update + 'M~t'+ dw_product_category.GetItemString(li_counter, 'prod_category') + "~t" + &
	            dw_product_category.GetItemString(li_counter, 'cool_code') + "~t" + &
					dw_product_category.GetItemString(li_counter, 'label_id') + "~t" + &
					"D~r~n"
Next

//ls_Update = dw_product_category.Describe('DataWindow.Data')  // testing

dw_product_category.SetFilter('')
dw_product_category.Filter()

dw_product_category.SetRedraw(True)

// pjm 08/11/2014 change service to cfmc36br.
//li_ret = iu_cfm001.nf_cfmc35br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_Update, &
//											ls_cool_string, &
//											ls_prod_cat_string)
//
// Only want to call if something to delete
If Len(Trim(ls_update)) > 0 Then
 
//	li_ret = iu_cfm001.nf_cfmc36br_inq_upd_tcfmiprd(istr_error_info, &
//												ls_header, &
//												ls_Update, &
//												ls_cool_string, &
//												ls_prod_cat_string, &
//												ls_cust_excl_string)		

	li_ret = iu_ws_orp2.nf_cfmc36fr(istr_error_info, &
												ls_header, &
												ls_Update, &
												ls_cool_string, &
												ls_prod_cat_string, &
												ls_cust_excl_string)

End If

If li_ret = 0 Then
	dw_product_category.SetRedraw(False)
	
	dw_product_category.SetFilter('Not IsSelected()')
	dw_product_category.Filter()
	dw_product_category.RowsDiscard(1, dw_product_category.FilteredCount(), Filter!)

	dw_product_category.SetFilter('')
	dw_product_category.SetSort("prod_category A, cool_code A")
	dw_product_category.Sort()

	dw_product_category.ResetUpdate()
//	wf_InsertRow(dw_product_category, 0)
	ll_row = dw_product_category.InsertRow(0)
	dw_product_category.SetRedraw(True)
	If li_ret = 0 then
		gw_netwise_frame.SetMicroHelp('Delete Successful')	
	End If
End if

dw_product_category.SetFocus()

Return 1
end event

on tp_product_cat.create
this.dw_product_category=create dw_product_category
this.Control[]={this.dw_product_category}
end on

on tp_product_cat.destroy
destroy(this.dw_product_category)
end on

type dw_product_category from u_base_dw_ext within tp_product_cat
integer x = 14
integer y = 44
integer width = 4645
integer height = 1244
integer taborder = 11
string dataobject = "d_product_category_coolcodes"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

u_project_functions	lu_project_functions

is_selection = '4'
ib_updateable = TRUE

This.GetChild('prod_category', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'PRODCAT ')

This.getchild('cool_code', ldwc_temp)

If ldwc_temp.RowCount() <= 1 Then
//	wf_filldddw(ldwc_temp)  pjm 08/14/2014
end if
end event

event itemchanged;String	ls_type_desc, ls_label_desc, ls_plant_desc, ls_plant_id, ls_prod_cat
datawindowchild ldwc_child
Long ll_row

If row = This.RowCount() Then
	ll_row = This.InsertRow(0)
End if

Choose Case dwo.name
	// pjm 09/16/2014 - added for new 'D' String
	Case "prod_category"
		is_prod_category  = Trim(data)
		wf_fill_cool_code_dddw()
		//wf_fill_labels_dddw()		
		This.SetItem(row, "label_id",'')
		This.SetItem(row, "label_desc",'')
		This.SetItem(row, "cool_code",'')
		This.SetItem(row, "cool_desc",'')
	Case "cool_code"
		is_prod_category = Trim(data)
		This.GetChild('cool_code', ldwc_child)
		ll_row = ldwc_child.Find("type_code = '" + data + "'",1,ldwc_child.RowCount())
		If ll_row > 0 Then
			ls_type_desc = ldwc_child.GetItemString(ll_row,"type_desc")
			This.SetItem(row, "cool_desc", ls_type_desc)
		End If
	
		This.SetItem(row, "cool_desc", ls_type_desc)
		This.SetItem(row, "label_id",'')  // pjm 09/18/2014
		This.SetItem(row, "label_desc",'')
		// pjm 08/20/2014 for new update
		this.SetItem(row, "update_flag", "A")

	// pjm 08/20/2014 added for labels
	Case 'label_id'
		This.GetChild('label_id', ldwc_child)
		ll_row = ldwc_child.Find("label_id = '" + data + "'",1,ldwc_child.RowCount())
		If ll_row > 0 Then
			ls_label_desc = ldwc_child.GetItemString(ll_row,"label_text")
			This.SetItem(row, "label_desc", ls_label_desc)
		End If

		this.SetItem(row, "update_flag", "A")
	
End Choose



end event

event clicked;call super::clicked;string ls_category 
datawindowchild ldwc_labels
integer li_ret
string ls_prod_category, ls_cool_code

If row > 0 Then
	Choose case dwo.name
		case 'excl_plant_id'
			il_selected_row = row
			ib_open = true
			wf_get_selected_plants()
			OpenWithParm(w_locations_popup, iw_this)
		case 'cool_code'
			// pjm 09/16/2014 - changed for new 'D' String
			ls_prod_category = Trim(this.GetItemString(row,"prod_category"))
			If Len(ls_prod_category) = 0 Then
				iw_frame.SetMicroHelp("You Must Select a Product Category first!")
				Return -1
			End If
			ls_cool_code = Trim(this.GetItemString(row,"cool_code"))
			If ls_cool_code <> is_cool_code Then
				is_cool_code = ls_cool_code
				wf_fill_cool_code_dddw()
			End If
		case 'label_id'
			// pjm 09/16/2014 - changed for new 'D' String
			ls_prod_category = Trim(this.GetItemString(row,"prod_category"))
			If Len(ls_prod_category) = 0 Then
				iw_frame.SetMicroHelp("You Must Select a Product Category first!")
				Return -1
			End If
			ls_cool_code = Trim(this.GetItemString(row,"cool_code"))
			If Len(ls_cool_code) = 0 Then
				iw_frame.SetMicroHelp("You Must Select a Cool Code first!")
				Return -1
			End If
			If ls_prod_category <> is_prod_category Then				
				is_prod_category = ls_prod_category
				is_cool_code = ls_cool_code
				wf_fill_cool_code_dddw()
				wf_fill_labels_dddw()
			ElseIf ls_cool_code <> is_cool_code Then
				is_cool_code = ls_cool_code
				wf_fill_labels_dddw()
			End If			
//			ls_cool_code = Trim(this.GetItemString(row,"cool_code"))
//			If IsNull(ls_cool_code) Then
//				MessageBox("Label ID", "You Must Select a Cool Code first!")
//				Return -1
//			End If
//			this.GetChild('label_id', ldwc_labels)
//			li_ret = ldwc_labels.SetFilter("cool_code = '" + ls_cool_code + "'")
//			li_ret = ldwc_labels.Filter(
		case else
			if IsValid(w_locations_popup) then close(w_locations_popup)
	End Choose
End If
end event

event ue_update;call super::ue_update;// pjm 08/15/2014   Added for excluded plants, do we need this?
If ib_open then 
	If isvalid(w_locations_popup) Then close(w_locations_popup) 
End If
end event

type tp_category from userobject within tab_detail
event ue_delete ( )
event ue_retrieve ( )
event type integer ue_update ( )
event type long ue_getrowcount ( integer somenumber )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Category"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_category dw_category
end type

event ue_delete();Int	li_counter, &
		li_ret

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_cool_string, &
			ls_cust_excl_string, &
			ls_prod_cat_string



u_string_functions	lu_string_functions

u_project_functions	lu_project_functions

If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
	// do nothing -- continue
Else	
	RETURN 
End If

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)

ls_header += 'U' + '~t'    // inquire option //

istr_error_info.se_event_name = "ue_delete of tab_category"

SetPointer(HourGlass!)

ll_RowCount = dw_category.RowCount()

Do
	ll_row = dw_category.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_category.SetRedraw(False)

// need to delete New! rows
// also delete NewModified! rows that are selected
dw_category.DeleteRow(ll_RowCount)
ll_RowCount = dw_category.RowCount()
For li_counter = 1 to ll_RowCount
	If dw_category.GetItemString(li_counter, 'update_flag') = 'A' And &
			dw_category.IsSelected(li_counter) Then
		dw_category.DeleteRow(li_counter)
		ll_rowcount --
	End If
Next

dw_category.SetFilter('IsSelected() And Not IsRowNew()')

dw_category.Filter()
ll_RowCount = dw_category.RowCount()
For li_counter = 1 to ll_RowCount
	dw_category.SetItem(li_counter, 'update_flag', 'D')
Next

ls_Update = dw_category.Describe('DataWindow.Data')

dw_category.SetFilter('')
dw_category.Filter()

dw_category.SetRedraw(True)

//li_ret = iu_cfm001.nf_cfmc35br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_Update, &
//											ls_cool_string, &
//											ls_prod_cat_string)
li_ret = iu_ws_orp2.nf_cfmc35fr(istr_error_info, ls_header, ls_Update, ls_cool_string, ls_prod_cat_string)
 
If li_ret = 0 Then
	dw_category.SetRedraw(False)
	
	dw_category.SetFilter('Not IsSelected()')
	dw_category.Filter()
	dw_category.RowsDiscard(1, dw_category.FilteredCount(), Filter!)

	dw_category.SetFilter('')
	dw_category.SetSort("prod_category A")
	dw_category.Sort()

	dw_category.ResetUpdate()

	dw_category.InsertRow(0)
	dw_category.SetRedraw(True)
	If li_ret = 0 then
		gw_netwise_frame.SetMicroHelp('Delete Successful')	
	End If
End if

dw_category.SetFocus()
end event

event ue_retrieve();Integer	li_ret

String	ls_header, &
			ls_input_string, &
			ls_cool_string, &
			ls_prod_cat_string, &
			ls_cust_excl_string, &
			ls_temp
long 		ll_rowcount

u_project_functions	lu_project_functions

if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the Product Category Tab.")
	return 
end if


ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
// Minimum len of ls_header should be 13 chars. 7 for cust_id, tab, cust_type, tab, div, tab

ls_header += 'I' + '~t'    // inquire option //

istr_error_info.se_event_name = "ue_retrieve of tab_category"

SetPointer(HourGlass!)

//li_ret = iu_cfm001.nf_cfmc35br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_input_string, &
//											ls_cool_string, &
//											ls_prod_cat_string)

li_ret = iu_ws_orp2.nf_cfmc35fr(istr_error_info, ls_header, ls_input_string, ls_cool_string, ls_prod_cat_string)

If li_ret = 0 Then
	dw_category.Reset()
	dw_category.ImportString(ls_prod_cat_string)
	If li_ret = 0 then
		gw_netwise_frame.SetMicroHelp('Inquire Successful')	
		dw_category.InsertRow(0)
		dw_category.ResetUpdate()
	End If
End If

//If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
//	iw_frame.im_menu.mf_Enable('m_delete')
//Else	
//	iw_frame.im_menu.mf_Disable('m_delete')
//End If


SetPointer(Arrow!)


end event

event type integer ue_update();Integer	li_ret

String	ls_header, &
			ls_input_string, &
			ls_cool_string, &
			ls_prod_cat_string, &
			ls_temp
			
long 		ll_rowcount, &
			ll_row

u_string_functions	lu_string_functions

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)

ls_header += 'U' + '~t'    // inquire option //

istr_error_info.se_event_name = "ue_update of tab_category"

SetPointer(HourGlass!)

// need to delete New! rows (but not NewModified!)

ll_RowCount = dw_category.RowCount()

// Validate data here
Do 
	ll_row = dw_category.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_category.GetItemString(ll_row, 'prod_category')) Then
			MessageBox("Category", "Category is a required field")
			dw_category.SetColumn('prod_category')
			dw_category.SetRow(ll_row)
			dw_category.SetFocus()
			dw_category.SetRedraw(True)
			return -1
		End if
		If dw_category.GetItemString(ll_Row, 'update_flag') = 'A' Then
			dw_category.SetItem(ll_Row, "update_flag", "A")
		Else
			dw_category.SetItem(ll_Row, "update_flag", "U")
		End If
	End if

Loop While ll_row > 0 and ll_row < (ll_RowCount)		

ls_input_string = lu_string_functions.nf_BuildUpdateString ( dw_category )

If lu_string_functions.nf_IsEmpty(ls_input_string) Then 
	dw_category.SetRedraw(True)
	gw_netwise_frame.SetMicroHelp('No lines found for update')
	return -1
End if

//li_ret = iu_cfm001.nf_cfmc35br_inq_upd_tcfmiprd(istr_error_info, &
//											ls_header, &
//											ls_input_string, &
//											ls_cool_string, &
//											ls_prod_cat_string)

li_ret = iu_ws_orp2.nf_cfmc35fr(istr_error_info, ls_header, ls_input_string, ls_cool_string, ls_prod_cat_string)
											
If li_ret = 0 then

	This.TriggerEvent("ue_retrieve")

	gw_netwise_frame.SetMicroHelp('Update Successful')	
End If

SetPointer(Arrow!)

Return li_ret
end event

event type long ue_getrowcount(integer somenumber);Return 0
//Long ll_RowCount
//ll_RowCount = This.dw_category.RowCount()
//Return ll_RowCount
end event

on tp_category.create
this.dw_category=create dw_category
this.Control[]={this.dw_category}
end on

on tp_category.destroy
destroy(this.dw_category)
end on

type dw_category from u_base_dw_ext within tp_category
integer x = 55
integer y = 76
integer height = 1204
integer taborder = 11
string dataobject = "d_assign_prod_category"
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

u_project_functions	lu_project_functions

is_selection = '4'
ib_updateable = TRUE

This.GetChild('prod_category', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'TSKUCATG')

This.GetChild('inc_exc_ind', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'TSKUCTIN')


end event

event itemchanged;call super::itemchanged;string		ls_category, &
				ls_type_short_desc, &
				ls_inc_exc_ind


If row = This.RowCount() Then
	This.InsertRow(0)
End if

If dwo.name = "inc_exc_ind" Then
	
	ls_category = dw_category.GetItemString(Row, 'prod_category')
	
	SELECT substring(tutltypes.type_short_desc,1,1)
	INTO :ls_type_short_desc
	FROM tutltypes
	WHERE tutltypes.record_type = 'TSKUCATG'
	  AND tutltypes.type_code   = :ls_category
	USING SQLCA ;
	
	ls_inc_exc_ind = data
	ls_inc_exc_ind = Mid(ls_inc_exc_ind, 1, 1)

	
	If (ls_inc_exc_ind = 'I') or (ls_inc_exc_ind = 'O') Then
		If ls_type_short_desc = 'N' Then
			MessageBox("I/E Indicator", "Category is not set up for Include or Include Only")
			dw_category.Setitem(Row, 'inc_exc_ind', 'E       ')
			dw_category.SetColumn('inc_exc_ind')
			dw_category.SetRow(row)
			dw_category.SetFocus()
			dw_category.SetRedraw(True)
			return 2
		End if
	End if
End If
end event

type tp_temp_settings from userobject within tab_detail
event ue_retrieve ( )
event type long ue_getrowcount ( integer ai_rowcount )
event ue_delete ( )
event type integer ue_update ( integer ai_rtn )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Temp~r~nSettings"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_temp_settings dw_temp_settings
end type

event ue_retrieve();Integer	li_ret

String	ls_header, &
			ls_output_string, &
			ls_temp
			
long 		ll_rowcount

u_project_functions	lu_project_functions


if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the Temperature Setting tab.")
	return
end if


ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)

istr_error_info.se_event_name = "ue_retrieve of tab_temp_settings"

SetPointer(HourGlass!)

//li_ret = iu_orp001.nf_orpo25br_cust_temp_settings_inq(istr_error_info, &
//											ls_header, &
//											ls_output_string)
											
li_ret = iu_ws_orp1.nf_orpo25fr(istr_error_info, &
										ls_header, &
										ls_output_string)
											

If li_ret = 0 Then
	dw_temp_settings.Reset()
	dw_temp_settings.ImportString(ls_output_string)
	gw_netwise_frame.SetMicroHelp('Inquire Successful')	
	ll_rowcount = dw_temp_settings.RowCount ( )
	dw_temp_settings.InsertRow(0)
	wf_initialize_temp_row(dw_temp_settings, ll_RowCount + 1)
	dw_temp_settings.uf_ChangeRowStatus( ll_RowCount + 1, NotModified!)
	dw_temp_settings.SetColumn("complex_code")
	dw_temp_settings.ResetUpdate()
	dw_temp_settings.SetFocus()
End If

//If lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() then
//	iw_frame.im_menu.mf_Enable('m_delete')
//Else	
//	iw_frame.im_menu.mf_Disable('m_delete')
//End If


SetPointer(Arrow!)


end event

event ue_getrowcount;Long ll_RowCount
ll_RowCount = This.dw_temp_settings.RowCount()
Return ll_RowCount
end event

event ue_delete();Boolean	lb_ret, lb_hasUpdates

Int	li_counter, &
		li_ret

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_filter, &
			ls_output_string

ll_RowCount = dw_temp_settings.RowCount()

Do
	ll_row = dw_temp_settings.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_temp_settings.SetRedraw(False)

ll_RowCount = dw_temp_settings.RowCount()
dw_temp_settings.DeleteRow(ll_RowCount)
lb_hasUpdates = false	// look for updates. if they have any, stop processing.
For li_counter = 1 to ll_RowCount
	if dw_temp_settings.IsSelected(li_counter) Then
		If dw_temp_settings.GetItemString(li_counter, 'update_flag') = 'A' Then
			dw_temp_settings.DeleteRow(li_counter) // allow delete, record doesnt exist in db. remove from grid
		else 
			If dw_temp_settings.GetItemString(li_counter, 'update_flag') = 'U' then
				lb_hasUpdates = true
			end if
		End if
	End If
Next

if(lb_hasUpdates) then 
	MessageBox("Delete Error", "You have pending changes. You must save before deleting rows.")
	dw_temp_settings.InsertRow(0)
	ll_RowCount = dw_temp_settings.RowCount()
	wf_initialize_temp_row(dw_temp_settings, ll_RowCount)
	dw_temp_settings.uf_ChangeRowStatus( ll_rowcount, NotModified!)
	dw_temp_settings.SetRedraw(True)
else
	ls_header = dw_header.Describe("DataWindow.Data")
	ls_header = mid(ls_header,1,13)
	ls_header += '~tU~r~n'
	
	dw_temp_settings.SetFilter('IsSelected() And Not IsRowNew()')
	dw_temp_settings.Filter()
	ll_RowCount = dw_temp_settings.RowCount()


	For li_counter = 1 to ll_RowCount
			dw_temp_settings.SetItem(li_counter, 'update_flag', 'D')
	Next
	
	ls_Update = dw_temp_settings.Describe('DataWindow.Data')
	//li_ret = iu_orp001.nf_orpo26br_cust_temp_settings_upd(istr_error_info, &
	//											ls_update, &
	//											ls_header, &
	//											ls_output_string)
												
	li_ret = iu_ws_orp2.nf_orpo26fr(ls_header, &
												ls_update, &
												ls_output_string, &
												istr_error_info)
	
	If li_ret = 0 Then
		dw_temp_settings.SetRedraw(False)
		
		dw_temp_settings.SetFilter('Not IsSelected()')
		dw_temp_settings.Filter()
		dw_temp_settings.RowsDiscard(1, dw_temp_settings.FilteredCount(), Filter!)
	
		dw_temp_settings.SetFilter('')
		dw_temp_settings.SetSort("complex_code, prod_state, setting_type")
		dw_temp_settings.Sort()
	
		dw_temp_settings.ResetUpdate()
	
		dw_temp_settings.InsertRow(0)
		ll_RowCount = dw_temp_settings.RowCount()
		wf_initialize_temp_row(dw_temp_settings, ll_RowCount)
		dw_temp_settings.uf_ChangeRowStatus( ll_rowcount, NotModified!)
		dw_temp_settings.SetRedraw(True)
	
		gw_netwise_frame.SetMicroHelp('Delete Successful')	
	End if
end if
dw_temp_settings.SetRedraw(True)
end event

event type integer ue_update(integer ai_rtn);date		ldt_from_date
Int		li_count, &
			li_ret, &
			li_day

Long		ll_RowCount, &
			ll_row, &
			ll_count, &
			ll_temp

String	ls_header, &
			ls_inquire, &
			ls_update, &
			ls_output_string, &
			ls_update_date, &
			ls_find, &
			ls_update_flag
			
			
u_string_functions	lu_string_functions			

IF Upper(Message.nf_get_app_id()) <> 'ORP' Then
	// To restrict this Tab
	 iw_frame.SetMicroHelp("This Tab is currently available to OP only.")
	Return -1
END IF

//dw_temp_settings.SetSort("complex_code D, prod_state D, setting_type D, from_month D")
//dw_temp_settings.Sort()

ll_rowcount = dw_temp_settings.RowCount ( )

If dw_temp_settings.RowCount() = 0 Then Return -1
If dw_temp_settings.AcceptText() = -1 Then return -1

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
// Minimum len of ls_header should be 9 chars.  7 for cust_id, tab, cust_type
If IsNull(ls_Header) or Len(ls_header) < 9 Then return -1
ls_header += '~tU~r~n'

SetPointer(HourGlass!)

integer i
for i = 1 to ll_RowCount
	dw_temp_settings.SelectRow(i, false)
next

// if the dropdown for the day fields is not populated then fill full of all 31 days.
li_day = 31
If idwc_from_day.RowCount() < 1 Then
	dw_temp_settings.GetChild('from_day', idwc_from_day)
	Do While li_day > 0
		idwc_from_day.InsertRow(1)
		if li_day > 9 Then
			idwc_from_day.SetItem(1,1,String(li_day))
		Else 
			idwc_from_day.SetItem(1,1,Fill("0" + String(li_day),2))
		End If
		li_day = li_day - 1
	Loop
end if

li_day = 31
If idwc_to_day.RowCount() < 1 Then
	dw_temp_settings.GetChild('to_day', idwc_to_day)
	Do While li_day > 0
		idwc_to_day.InsertRow(1)
		if li_day > 9 Then
			idwc_to_day.SetItem(1,1,String(li_day))
		Else 
			idwc_to_day.SetItem(1,1,Fill("0" + String(li_day),2))
		End If
		li_day = li_day - 1
	Loop
end if

dw_temp_settings.SetRedraw(False)


ll_RowCount = dw_temp_settings.RowCount()

// Validate data here
Do 
	ll_row = dw_temp_settings.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Mke sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'complex_code')) Then
			MessageBox("Complex Code", "Complex Code is a required field")
			dw_temp_settings.SetColumn('complex_code')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'prod_state')) Then
			MessageBox("Product State", "Product State is a required field")
			dw_temp_settings.SetColumn('prod_state')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'setting_type')) Then
			MessageBox("Setting Type", "Setting Type is a required field")
			dw_temp_settings.SetColumn('setting_type')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'from_month')) Then
			MessageBox("From Month", "From Month is a required field")
			dw_temp_settings.SetColumn('from_month')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		/*AJG - the options in the drop downs are restricted. this validation is not necessary
		        as only valid options are presented to the user
		ls_Find = "two_char_date = '" + dw_temp_settings.GetItemString(ll_row, 'from_month') + "'"
		If idwc_from_date.Find(ls_Find , 1, idwc_from_date.RowCount()) > 0 Then
			//do nothing
		Else
			MessageBox("From Month", "Invalid From Month")
			dw_temp_settings.SetColumn('from_month')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End If*/
			
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'from_day')) Then
			MessageBox("From Day", "From Day is a required field")
			dw_temp_settings.SetColumn('from_day')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		
		/*AJG - the options in the drop downs are restricted. this validation is not necessary
		        as only valid options are presented to the user
		ls_Find = "two_char_date = '" + dw_temp_settings.GetItemString(ll_row, 'from_day') + "'"
		If idwc_from_day.Find(ls_Find , 1, idwc_from_day.RowCount()) > 0 Then
			//do nothing
		Else
			MessageBox("From Day", "Invalid From Day")
			dw_temp_settings.SetColumn('from_day')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End If*/
		
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'to_month')) Then
			MessageBox("To Month", "To Month is a required field")
			dw_temp_settings.SetColumn('to_month')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		
		/*AJG - the options in the drop downs are restricted. this validation is not necessary
		        as only valid options are presented to the user
		ls_Find = "two_char_date = '" + dw_temp_settings.GetItemString(ll_row, 'to_month') + "'"
		If idwc_to_date.Find(ls_Find , 1, idwc_to_date.RowCount()) > 0 Then
			//do nothing
		Else
			MessageBox("To Month", "Invalid to Month")
			dw_temp_settings.SetColumn('to_month')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End If*/
		
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, 'to_day')) Then
			MessageBox("To Day", "To Day is a required field")
			dw_temp_settings.SetColumn('to_day')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
		
		/*AJG - the options in the drop downs are restricted. this validation is not necessary
		        as only valid options are presented to the user
		ls_Find = "two_char_date = '" + dw_temp_settings.GetItemString(ll_row, 'to_day') + "'"
		If idwc_to_day.Find(ls_Find , 1, idwc_to_day.RowCount()) > 0 Then
			//do nothing
		Else
			MessageBox("To Day", "Invalid To Day")
			dw_temp_settings.SetColumn('to_day')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End If*/
		
		ll_temp = (dw_temp_settings.GetItemNumber(ll_row, 'temperature'))
		If ll_temp < -99 or ll_temp > 99 Then
			MessageBox("Temperature", "Temperature must be between -99 and 99")
			dw_temp_settings.SetColumn('temperature')
			dw_temp_settings.SetRow(ll_row)
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			return -1
		End if
				
		ll_temp = (dw_temp_settings.GetItemNumber(ll_row, 'min_boxes'))
		If dw_temp_settings.GetItemString(ll_row, 'setting_type') = 'DEF' Then
		// do nothing
		Else 
			If ll_temp < 0 or ll_temp > 99999 Then
				MessageBox("Min Boxes", "Min Boxes must be between 0 and 99999")
				dw_temp_settings.SetColumn('min_boxes')
				dw_temp_settings.SetRow(ll_row)
				dw_temp_settings.SetFocus()
				dw_temp_settings.SetRedraw(True)
				return -1
			End if
		End If
		
		if wf_validate_month_day(dw_temp_settings, ll_row) < 0 Then
			dw_temp_settings.SetFocus()
			dw_temp_settings.SetRedraw(True)
			Return -1
		End if
		
		ls_update_flag = dw_temp_settings.GetItemString(ll_row, "update_flag")
		If lu_string_functions.nf_IsEmpty(dw_temp_settings.GetItemString(ll_row, "update_flag")) Then
			dw_temp_settings.SetItem(ll_Row, "update_flag", "U")
		End If
	End if
Loop While ll_row > 0 and ll_row < (ll_RowCount)		

ls_Update = lu_string_functions.nf_BuildUpdateString ( dw_temp_settings )

istr_error_info.se_event_name = "ue_update of uo_tab_temp_settings"

//li_ret = iu_orp001.nf_orpo26br_cust_temp_settings_upd(istr_error_info, &
//											ls_update, &
//											ls_header, &
//											ls_output_string)
											
li_ret = iu_ws_orp2.nf_orpo26fr(ls_header, &
								           ls_update, &
										  ls_output_string, &
										  istr_error_info)

If li_ret < 0 Then
	dw_temp_settings.SetRedraw(True)
	Return -1
End if

For li_count = 1 To dw_temp_settings.RowCount()
	if li_count = ll_rowCount Then 
		// do nothing
	else
		if dw_temp_settings.GetItemString(li_count, "update_flag") = 'A' Then 
			dw_temp_settings.SetItem(li_count, "last_update_date", today())
			dw_temp_settings.SetItem(li_count, "last_update_time", now())
			dw_temp_settings.Setitem(li_count, "last_update_userid", Message.nf_getuserid())
		end if
	end if	
	dw_temp_settings.SetItem(li_count, "update_flag", " ")
	dw_temp_settings.SelectRow(li_count, FALSE)
Next


wf_initialize_temp_row(dw_temp_settings, ll_RowCount)
tp_temp_settings.TriggerEvent("ue_retrieve")
gw_netwise_frame.SetMicroHelp('Update Successful')
dw_temp_settings.ResetUpdate()
dw_temp_settings.SetRedraw(True)
Return 0





end event

on tp_temp_settings.create
this.dw_temp_settings=create dw_temp_settings
this.Control[]={this.dw_temp_settings}
end on

on tp_temp_settings.destroy
destroy(this.dw_temp_settings)
end on

type dw_temp_settings from u_base_dw_ext within tp_temp_settings
integer x = 14
integer y = 28
integer width = 3305
integer height = 1252
integer taborder = 11
string dataobject = "d_temp_settings"
end type

event itemchanged;call super::itemchanged;Integer 				li_day
Long					ll_currentrow
String				ls_find

data = trim(data)

DataWindowChild	ldwc_two_char_date


ll_CurrentRow = row

choose case dwo.name
	case "from_month"
		This.SetItem(row,"from_day", ' ')
		if data = '02' then 
			li_day = 29
		else 
		if data = '01' or data = '03' or data = '05' or data = '07' or data = '08' or data = '10' or data = '12' Then
			li_day = 31
		else 
			li_day = 30
		end if
		end if
		This.GetChild('from_day', idwc_from_day)
		idwc_from_day.Reset()
		If idwc_from_day.RowCount() < 1 Then
			This.GetChild('from_day', idwc_from_day)
			Do While li_day > 0
				idwc_from_day.InsertRow(1)
				if li_day > 9 Then
					idwc_from_day.SetItem(1,1,String(li_day))
				Else 
					idwc_from_day.SetItem(1,1,Fill("0" + String(li_day),2))
				End If
				li_day = li_day - 1
			Loop
		end if
		//		
	case "to_month"
		This.SetItem(row,"to_day", ' ')
		if data = '02' then
			li_day = 29
		else 
		if data = '01' or data = '03' or data = '05' or data = '07' or data = '08' or data = '10' or data = '12' Then
			li_day = 31
		else 
			li_day = 30
		end if
		end if
		This.GetChild('to_day', idwc_to_day)
		idwc_to_day.Reset()
		If idwc_to_day.RowCount() < 1 Then
			This.GetChild('to_day', idwc_to_day)
			Do While li_day > 0
				idwc_to_day.InsertRow(1)
				if li_day > 9 Then
					idwc_to_day.SetItem(1,1,String(li_day))
				Else
					idwc_to_day.SetItem(1,1,Fill("0" + String(li_day),2))
				End If
				li_day = li_day - 1
			Loop
		end if
	case "setting_type"
			if data = 'DFT' Then
				This.SetItem(row, "min_boxes_flag", 'H')
			else
				This.SetItem(row, "min_boxes_flag", ' ')
			End if
//		
end choose

If ll_CurrentRow = This.RowCount() Then
	This.SelectRow ( ll_currentrow, False )
	This.InsertRow(0)
	wf_initialize_temp_row(dw_temp_settings, ll_currentrow + 1)
	This.uf_ChangeRowStatus( ll_currentrow + 1, NotModified!)
End if

end event

event constructor;call super::constructor;Integer						li_date

String 						ls_all

DataWindowChild			ldwc_complex, &
								ldwc_product_state, &
								ldwc_setting_type, &
								ldwc_setting_type_descr, &
								ldwc_product_state_descr, &
								ldwc_complex_descr

u_project_functions		lu_project_functions


is_selection = '10'
ib_updateable = TRUE
ls_all = "ALL"

This.GetChild('complex_code', ldwc_complex)
If ldwc_complex.RowCount() < 1 Then
	This.GetChild('complex_name', ldwc_complex_descr)
	lu_project_functions.nf_gettutltype(ldwc_complex, 'COMPLEX')
	ldwc_complex.ShareData(ldwc_complex_descr)
	ldwc_complex.InsertRow(1)
	ldwc_complex.SetItem(1,1,ls_all)
	ldwc_complex.SetItem(1,2,ls_all)

End IF
			
This.GetChild('prod_state', ldwc_product_state)
If ldwc_product_state.RowCount() < 1 Then
	This.GetChild('prod_state', ldwc_product_state_descr)
	lu_project_functions.nf_gettutltype(ldwc_product_state, 'PRDSTAT')
	ldwc_product_state.ShareData(ldwc_product_state_descr)
End IF
			
This.GetChild('setting_type', ldwc_setting_type)
If ldwc_setting_type.RowCount() < 1 Then
	This.GetChild('setting_type', ldwc_setting_type_descr)
	lu_project_functions.nf_gettutltype(ldwc_setting_type, 'OPTMPTYP')
	ldwc_setting_type.ShareData(ldwc_setting_type_descr)
End IF
			
This.GetChild('from_month', idwc_from_date)
If idwc_from_date.RowCount() < 1 Then
	This.GetChild('from_month', idwc_from_date)
	li_date = 12
	Do While li_date > 0
		
		idwc_from_date.InsertRow(1)
		if li_date > 9 Then
			idwc_from_date.SetItem(1,1,String(li_date))
		else
			idwc_from_date.SetItem(1,1,Fill("0" + String(li_date),2))
		end if
		li_date = li_date - 1
	Loop
End IF
			
This.GetChild('to_month', idwc_to_date)
If idwc_to_date.RowCount() < 1 Then
	This.GetChild('to_month', idwc_to_date)
	li_date = 12
	Do While li_date > 0
		idwc_to_date.InsertRow(1)
		if li_date > 9 Then
			idwc_to_date.SetItem(1,1,String(li_date))
		else
			idwc_to_date.SetItem(1,1,Fill("0" + String(li_date),2))
		end if
			li_date = li_date - 1
		Loop
End IF
			
end event

event clicked;call super::clicked;Integer 				li_day
Long					ll_currentrow
String				ls_find, &
						ls_month

DataWindowChild	ldwc_two_char_date


ll_CurrentRow = row

choose case dwo.name
	case "from_day"
		ls_month = This.GetItemString(row,"from_month")
		if ls_month = '02' then 
			li_day = 29
		else 
		if ls_month  = '01' or ls_month  = '03' or ls_month  = '05' or ls_month  = '07' or ls_month  = '08' or ls_month  = '10' or ls_month  = '12' Then
			li_day = 31
		else 
			li_day = 30
		end if
		end if
		This.GetChild('from_day', idwc_from_day)
		idwc_from_day.Reset()
		If idwc_from_day.RowCount() < 1 Then
			This.GetChild('from_day', idwc_from_day)
			Do While li_day > 0
				idwc_from_day.InsertRow(1)
				if li_day > 9 Then
					idwc_from_day.SetItem(1,1,String(li_day))
				Else 
					idwc_from_day.SetItem(1,1,Fill("0" + String(li_day),2))
				End If
				li_day = li_day - 1
			Loop
		end if
		//		
	case "to_day"
		ls_month = This.GetItemString(row,"to_month")
		if ls_month  = '02' then
			li_day = 29
		else 
		if ls_month  = '01' or ls_month  = '03' or ls_month  = '05' or ls_month  = '07' or ls_month  = '08' or ls_month  = '10' or ls_month  = '12' Then
			li_day = 31
		else 
			li_day = 30
		end if
		end if
		This.GetChild('to_day', idwc_to_day)
		idwc_to_day.Reset()
		If idwc_to_day.RowCount() < 1 Then
			This.GetChild('to_day', idwc_to_day)
			Do While li_day > 0
				idwc_to_day.InsertRow(1)
				if li_day > 9 Then
					idwc_to_day.SetItem(1,1,String(li_day))
				Else
					idwc_to_day.SetItem(1,1,Fill("0" + String(li_day),2))
				End If
				li_day = li_day - 1
			Loop
		end if
		
end choose

If row = 0 Then return
If Right(dwo.Name, 2) = '_t' Then return

IF row > 0 Then
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If



end event

type tp_loading_platform from userobject within tab_detail
event ue_retrieve ( )
event type integer ue_update ( )
event type long ue_getrowcount ( integer somenumber )
event ue_delete ( )
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Loading~r~nPlatform"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_loading_platform dw_loading_platform
end type

event ue_retrieve();Boolean	lb_rtn

String	ls_header, &
			ls_input_string, &
			ls_output_string, &
			ls_temp
			
long 		ll_rowcount

u_project_functions	lu_project_functions

if isnull(dw_header.getitemstring(1,"division")) then
	messagebox("Required", "Division is required for the Loading Platform Tab.")
	return
end if	

ls_input_string = " "

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
ls_header +=  'I' + '~t'  // Inquire option

istr_error_info.se_event_name = "ue_retrieve of tab_loading_platform"

SetPointer(HourGlass!)
											
lb_rtn = iu_ws_orp5.nf_cfmc37fr(ls_header, &
												ls_input_string, &
												ls_output_string, &
												istr_error_info)
											

If lb_rtn Then
		dw_loading_platform.Reset()
		dw_loading_platform.ImportString(ls_output_string)
		gw_netwise_frame.SetMicroHelp('Inquire Successful')	
		ll_rowcount = dw_loading_platform.RowCount ( )
		dw_loading_platform.InsertRow(0)
		dw_loading_platform.SetItem(ll_RowCount + 1, 'update_flag', 'A')
		dw_loading_platform.uf_ChangeRowStatus( ll_RowCount + 1, NotModified!)
		dw_loading_platform.SetColumn("priority")
		dw_loading_platform.ResetUpdate()
		dw_loading_platform.SetFocus()
	Else
		dw_loading_platform.SetRedraw( true)
End If


end event

event type integer ue_update();Int		li_count, &
          li_priority_rows_found, &
		li_loading_platform_rows_found, &
		li_rtn

Long		ll_RowCount, &
			ll_row, &
			ll_count, &
			ll_temp, &
			ll_mod_priority, ll_priority

String	ls_header, &
			ls_inquire, &
			ls_input_string, &
			ls_output_string, &
			ls_update_date, &
			ls_find, &
			ls_update_flag, &
			ls_mod_loading_platform, ls_loading_platform, &
			ls_mod_product_state, ls_product_state
			
Boolean lb_rtn			
			
			
u_string_functions	lu_string_functions			

ll_rowcount = dw_loading_platform.RowCount ( )

If dw_loading_platform.RowCount() = 0 Then Return -1
If dw_loading_platform.AcceptText() = -1 Then return -1

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,13)
ls_header +=  'U' + '~t'  // Inquire option

SetPointer(HourGlass!)

integer i
for i = 1 to ll_RowCount
	dw_loading_platform.SelectRow(i, false)
next

dw_loading_platform.SetRedraw(False)

// Validate data here
Do 
	ll_row = dw_loading_platform.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		
		If lu_string_functions.nf_IsEmpty(dw_loading_platform.GetItemString(ll_row, 'product_state')) Then
			MessageBox("Product State", "Product State is a required field")
			dw_loading_platform.SetColumn('product_state')
			dw_loading_platform.SetRow(ll_row)
			dw_loading_platform.SetFocus()
			dw_loading_platform.SetRedraw(True)
			return -1
		End if
		
		If lu_string_functions.nf_IsEmpty(dw_loading_platform.GetItemString(ll_row, 'loading_platform')) Then
			MessageBox("Loading Platform", "Loading Platform is a required field")
			dw_loading_platform.SetColumn('loading_platform')
			dw_loading_platform.SetRow(ll_row)
			dw_loading_platform.SetFocus()
			dw_loading_platform.SetRedraw(True)
			return -1
		End if
		
		ll_temp = (dw_loading_platform.GetItemNumber(ll_row, 'priority'))
		If ll_temp <= 0 or ll_temp > 9999 Then
			MessageBox("Priority", "Priority must be between 0 and 9999")
			dw_loading_platform.SetColumn('priority')
			dw_loading_platform.SetRow(ll_row)
			dw_loading_platform.SetFocus()
			dw_loading_platform.SetRedraw(True)
			return -1
		End if
	End if
Loop While ll_row > 0 and ll_row < ll_RowCount		

// check modified rows to see if duplicated...
// more than 1 row with the same Product State and Priority is NOT allowed
// more than 1 row with the same Product State and Loading Platform is NOT allowed
ll_row = 0
li_priority_rows_found = 0
li_loading_platform_rows_found = 0

Do
	ll_row = dw_loading_platform.GetNextModified(ll_row, Primary!)
	
	if ll_row > 0 then
		// modified row data values 
		ls_mod_product_state = Trim(dw_loading_platform.GetItemString(ll_row, 'product_state'))
		ll_mod_priority = dw_loading_platform.GetItemNumber(ll_row, 'priority')
		ls_mod_loading_platform = Trim(dw_loading_platform.GetItemString(ll_row, 'loading_platform'))
		
		li_priority_rows_found = 0
		li_loading_platform_rows_found = 0
				
		For li_count = 1 to ll_RowCount - 1
				ls_product_state = Trim(dw_loading_platform.GetItemString(li_count, 'product_state'))
				ll_priority = dw_loading_platform.GetItemNumber(li_count, 'priority')
				ls_loading_platform = Trim(dw_loading_platform.GetItemString(li_count, 'loading_platform'))
				If IsNull(ls_product_state) Then
					// do nothing - continue
				Else
					If (ls_mod_product_state = ls_product_state and ll_mod_priority = ll_priority) Then
						li_priority_rows_found ++
						If li_priority_rows_found > 1 Then
							MessageBox("Duplicate", "Duplicate Product State and Priority")
							dw_loading_platform.SetFocus()
							dw_loading_platform.SetRedraw(True)		
							Return -1
						End If
					End If
					If (ls_mod_product_state = ls_product_state and ls_mod_loading_platform = ls_loading_platform) Then
						li_loading_platform_rows_found ++
							If li_loading_platform_rows_found > 1 Then
								MessageBox("Duplicate", "Duplicate Product State and Loading Platform.")
								dw_loading_platform.SetFocus()
								dw_loading_platform.SetRedraw(True)			
								Return -1
							End If					
					End If
				End If
		Next
	end if
	
Loop While ll_row > 0 and ll_row < ll_RowCount

// check to make sure there is at least 1 fresh and 1 frozen loading platform specificed that is not a trackable loading platform like CHEP or SMART
li_rtn = wf_check_loading_platform_update()
If li_rtn < 0 Then
	return -1
End If

// all validations and checks for this tab passed - set the modified rows for updating
ll_row = 0
Do 
	ll_row = dw_loading_platform.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
ls_update_flag = dw_loading_platform.GetItemString(ll_row, "update_flag")
		If lu_string_functions.nf_IsEmpty(dw_loading_platform.GetItemString(ll_row, "update_flag")) Then
			dw_loading_platform.SetItem(ll_Row, "update_flag", "U")
		End If
	End if
Loop While ll_row > 0 and ll_row < ll_RowCount		

ll_row = 0
Do
	ll_row = dw_loading_platform.GetNextModified(ll_row, Primary!)
	if ll_row > 0 then
		ls_input_string += trim(dw_loading_platform.GetItemString(ll_row, 'product_state')) + '~t' &
		       + trim(dw_loading_platform.GetItemString(ll_row, 'loading_platform')) + '~t' &
			  + string(dw_loading_platform.GetItemNumber(ll_row, 'priority')) + '~t' & 
			  + trim(dw_loading_platform.GetItemString(ll_row, 'update_flag')) +  '~r~n'
	end if
Loop While ll_row > 0 and ll_row < ll_RowCount
		
istr_error_info.se_event_name = "ue_update of tab_loading_platform"
											
lb_rtn = iu_ws_orp5.nf_cfmc37fr(ls_header, &
												ls_input_string, &
												ls_output_string, &
												istr_error_info)

If Not lb_rtn Then
	dw_loading_platform.SetRedraw(True)
	Return -1
End if

tp_loading_platform.TriggerEvent("ue_retrieve")
gw_netwise_frame.SetMicroHelp('Update Successful')
dw_loading_platform.ResetUpdate()
dw_loading_platform.SetRedraw(True)
Return 0





end event

event type long ue_getrowcount(integer somenumber);Long ll_RowCount
ll_RowCount = This.dw_loading_platform.RowCount()
Return ll_RowCount

end event

event ue_delete();Boolean	lb_ret, lb_hasUpdates

Int	li_counter, &
		li_ret

Long	ll_RowCount, &
		ll_row

String	ls_header, &
			ls_inquire, &
			ls_input_string, &
			ls_filter, &
			ls_output_string

ll_RowCount = dw_loading_platform.RowCount()

Do
	ll_row = dw_loading_platform.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return


dw_loading_platform.SetRedraw(False)

ll_RowCount = dw_loading_platform.RowCount()
dw_loading_platform.DeleteRow(ll_RowCount)
lb_hasUpdates = false	// look for updates. if they have any, stop processing.
For li_counter = 1 to ll_RowCount
	if dw_loading_platform.IsSelected(li_counter) Then
		If dw_loading_platform.GetItemString(li_counter, 'update_flag') = 'A' Then
			dw_loading_platform.DeleteRow(li_counter) // allow delete, record doesnt exist in db. remove from grid
		else 
			If dw_loading_platform.GetItemString(li_counter, 'update_flag') = 'U' then
				lb_hasUpdates = true
			end if
		End if
	End If
Next

if(lb_hasUpdates) then 
	MessageBox("Delete Error", "You have pending changes. You must save before deleting rows.")
	dw_loading_platform.InsertRow(0)
	ll_RowCount = dw_loading_platform.RowCount()
     dw_loading_platform.SetItem(ll_RowCount, 'update_flag', 'A')
	dw_loading_platform.uf_ChangeRowStatus( ll_rowcount, NotModified!)
	dw_loading_platform.SetRedraw(True)
else
	// check to make sure there is at least 1 fresh and 1 frozen loading platform specificed that is not a trackable loading platform like CHEP or SMART
	li_ret = wf_check_loading_platform_delete()
	
	If li_ret < 0 Then
		dw_loading_platform.InsertRow(0)
		ll_RowCount = dw_loading_platform.RowCount()
	     dw_loading_platform.SetItem(ll_RowCount, 'update_flag', 'A')
		dw_loading_platform.uf_ChangeRowStatus( ll_rowcount, NotModified!)
		dw_loading_platform.SetRedraw(True)
	Else
	
		ls_header = dw_header.Describe("DataWindow.Data")
		ls_header = mid(ls_header,1,13)
		ls_header +=  'U' + '~t'  // Inquire option
		
		dw_loading_platform.SetFilter('IsSelected() And Not IsRowNew()')
		dw_loading_platform.Filter()
		ll_RowCount = dw_loading_platform.RowCount()
	
		For li_counter = 1 to ll_RowCount
				dw_loading_platform.SetItem(li_counter, 'update_flag', 'D')
		Next
	
		ll_row = 0
		Do
			ll_row = dw_loading_platform.GetNextModified(ll_row, Primary!)
			if ll_row > 0 then
				ls_input_string += dw_loading_platform.GetItemString(ll_row, 'product_state') + '~t' &
				       + dw_loading_platform.GetItemString(ll_row, 'loading_platform') + '~t' &
					  + string(dw_loading_platform.GetItemNumber(ll_row, 'priority')) + '~t' & 
					  + dw_loading_platform.GetItemString(ll_row, 'update_flag') +  '~r~n'
			end if
		Loop While ll_row > 0 and ll_row < ll_RowCount
													
		lb_ret = iu_ws_orp5.nf_cfmc37fr(ls_header, &
													ls_input_string, &
													ls_output_string, &
													istr_error_info)
		
		If lb_ret Then
			dw_loading_platform.SetRedraw(False)
			
			dw_loading_platform.SetFilter('Not IsSelected()')
			dw_loading_platform.Filter()
			dw_loading_platform.RowsDiscard(1, dw_loading_platform.FilteredCount(), Filter!)
		
			dw_loading_platform.SetFilter('')
			dw_loading_platform.SetSort("product_state, priority")
			dw_loading_platform.Sort()
		
			dw_loading_platform.ResetUpdate()
		
			dw_loading_platform.InsertRow(0)
			ll_RowCount = dw_loading_platform.RowCount()
	          dw_loading_platform.SetItem(ll_RowCount, 'update_flag', 'A')
			dw_loading_platform.uf_ChangeRowStatus( ll_rowcount, NotModified!)
			dw_loading_platform.SetRedraw(True)
		
			gw_netwise_frame.SetMicroHelp('Delete Successful')	
		End if
	End if
end if
dw_loading_platform.SetRedraw(True)
end event

on tp_loading_platform.create
this.dw_loading_platform=create dw_loading_platform
this.Control[]={this.dw_loading_platform}
end on

on tp_loading_platform.destroy
destroy(this.dw_loading_platform)
end on

type dw_loading_platform from u_base_dw_ext within tp_loading_platform
integer x = 23
integer y = 12
integer width = 2574
integer height = 1252
integer taborder = 11
string dataobject = "d_loading_platform"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild			ldwc_product_state, &
								ldwc_product_state_descr, &
								ldwc_loading_platform, &
								ldwc_temp

u_project_functions	lu_project_functions

is_selection = '11'
ib_updateable = TRUE

This.GetChild('product_state', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'PRDSTAT')

This.GetChild('loading_platform', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'LOADINST ')


end event

event itemchanged;call super::itemchanged;Long ll_row

If row = This.RowCount() Then
	This.SelectRow(row, False)
	This.InsertRow(0)
	This.SetItem(row + 1, 'update_flag', 'A')
	This.uf_ChangeRowStatus( row + 1, NotModified!)	
End if

end event

event clicked;call super::clicked;If row = 0 Then return
If Right(dwo.Name, 2) = '_t' Then return

IF row > 0 Then
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If
end event

type tp_programs from userobject within tab_detail
event type long ue_retrieve ( )
event type long ue_getrowcount ( integer ai_some_value )
event type long ue_update ( )
string tag = "Programs"
integer x = 18
integer y = 176
integer width = 4731
integer height = 1284
long backcolor = 12632256
string text = "Programs"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_programs dw_programs
end type

event type long ue_retrieve();Integer	li_ret

String	ls_header, &
		ls_customer_programs_out, &
		ls_programs_out
		
Long	ll_rowcount		
		
u_project_functions	lu_project_functions

DataWindowChild	ldwc_temp

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,10)

dw_programs.Reset()

IF dw_header.getitemstring(1,"customer_type") = "B" then
	messagebox("Required", "Can only inqire on Ship To Customers for the Programs Tab.")
	return 1
end if

If dw_header.getitemstring(1,"customer_type") = "C" then
	messagebox("Required", "Can only inquire on Ship To Customers for the Programs Tab.")
	return 1
end if

istr_error_info.se_event_name = "ue_retrieve of customer programs"

SetPointer(HourGlass!)

IF Not IsValid( iu_ws_orp3) THEN
	iu_ws_orp3	=  CREATE u_ws_orp3
END IF

li_ret = iu_ws_orp3.uf_orpo22gr_customer_programs_inq(istr_error_info, &
				ls_header, &
				ls_customer_programs_out, &
				ls_programs_out)
				
dw_programs.GetChild("program_id", ldwc_temp)
ldwc_temp.Reset()
ldwc_temp.ImportString(ls_programs_out)

If li_ret = 0 Then
	dw_programs.Reset()
	dw_programs.ImportString(ls_customer_programs_out)
//	gw_netwise_frame.SetMicroHelp('Inquire Successful')	
	ll_rowcount = dw_programs.RowCount ( )
	dw_programs.InsertRow(0)
//	wf_initialize_temp_row(dw_temp_settings, ll_RowCount + 1)
	dw_programs.uf_ChangeRowStatus( ll_RowCount + 1, NotModified!)
	dw_programs.SetColumn("status")
	dw_programs.ResetUpdate()
	dw_programs.SetFocus()
End If

SetPointer(Arrow!)

If is_message > '' Then
	gw_netwise_frame.SetMicroHelp(is_message)
	is_message = ''
End If

Return li_ret



end event

event type long ue_getrowcount(integer ai_some_value);Return 0
end event

event type long ue_update();String		ls_header, &
			ls_update_string, &
			ls_out

Long 		ll_RowCount, &
			ll_row, &
			ll_ret
			
u_string_functions	lu_string_functions			

ls_header = dw_header.Describe("DataWindow.Data")
ls_header = mid(ls_header,1,10)

istr_error_info.se_event_name =  "ue_update of customer programs"

If Not wf_check_dup_programs() Then Return 1

SetPointer(HourGlass!)

// need to delete New! rows (but not NewModified!)

ll_RowCount = dw_programs.RowCount()

// Validate data here
Do 
	ll_row = dw_programs.GetNextModified(ll_row, Primary!)
	If ll_row > 0 Then
		// Make sure all fields are filled in
		If lu_string_functions.nf_IsEmpty(String(dw_programs.GetItemNumber(ll_row, 'program_id'))) Then
			MessageBox("Program ID", "Program ID is a required field")
			dw_programs.SetColumn('status')
			dw_programs.SetRow(ll_row)
			dw_programs.SetFocus()
			dw_programs.SetRedraw(True)
			return -1
		End if
		ls_update_string += ls_header + String(dw_programs.GetItemNumber(ll_row, "program_id")) + "~t" + &
							dw_programs.GetItemString(ll_row, "status") +  "~t" + &
							dw_programs.GetItemString(ll_row, "update_flag") + "~r~n" 
	End if
					
Loop While ll_row > 0 and ll_row < (ll_RowCount)		

istr_error_info.se_event_name = "ue_update of tp_programs"

ll_ret = iu_ws_orp3.uf_orpo23gr_cust_programs_update(istr_error_info, &
				ls_update_string, &
				ls_out)  

if ll_ret = 0 Then
	dw_programs.ResetUpdate()
	is_message = iw_frame.wf_Getmicrohelp()
	tab_detail.TriggerEvent("ue_retrieve")
End If

end event

on tp_programs.create
this.dw_programs=create dw_programs
this.Control[]={this.dw_programs}
end on

on tp_programs.destroy
destroy(this.dw_programs)
end on

type dw_programs from u_base_dw_ext within tp_programs
integer x = 41
integer y = 32
integer width = 4402
integer height = 1256
integer taborder = 11
string dataobject = "d_customer_programs"
boolean vscrollbar = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild("SPECIES", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

This.GetChild("STATUS", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()
end event

event itemchanged;call super::itemchanged;DataWindowchild	ldwc_temp

Long	ll_ret, &
		ll_RowCount

If row = This.RowCount() Then
	This.InsertRow(0)
End if

If This.GetItemString(row, "last_update_userid") > "        " Then
	This.SetItem(row, "update_flag", "U")
Else	
	This.SetItem(row, "update_flag", "I")
End If

Choose Case dwo.name
	Case "program_id"
		This.GetChild("program_id", ldwc_temp)
		ll_Rowcount = ldwc_temp.Rowcount()
		ll_ret = ldwc_temp.Find("program_id = " + String(data), 1, ll_RowCount)
		if ll_ret > 0 then
			this.SetItem(row, "status", ldwc_temp.GetitemString(ll_ret, "status"))
			this.SetItem(row, "species", ldwc_temp.GetitemString(ll_ret, "species"))
		End If
	
End Choose
end event

type dw_header from u_base_dw_ext within w_customer_profile
integer x = 9
integer width = 2747
integer height = 316
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_cust_profile_header"
string icon = "Application5!"
end type

