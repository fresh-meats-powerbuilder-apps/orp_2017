﻿$PBExportHeader$w_checkcustomers_inq.srw
forward
global type w_checkcustomers_inq from w_base_response_ext
end type
type cbx_1 from checkbox within w_checkcustomers_inq
end type
end forward

global type w_checkcustomers_inq from w_base_response_ext
int Width=828
int Height=501
boolean TitleBar=true
string Title="View Customer Data Inquire"
long BackColor=12632256
cbx_1 cbx_1
end type
global w_checkcustomers_inq w_checkcustomers_inq

event ue_base_ok;call super::ue_base_ok;IF cbx_1.Checked Then
	CloseWithReturn(This,"Checked")
ELSE
	CloseWithReturn(This,"UnChecked")
END IF
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWIthReturn(This,"Cancel")
end event

on w_checkcustomers_inq.create
int iCurrent
call w_base_response_ext::create
this.cbx_1=create cbx_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cbx_1
end on

on w_checkcustomers_inq.destroy
call w_base_response_ext::destroy
destroy(this.cbx_1)
end on

type cb_base_help from w_base_response_ext`cb_base_help within w_checkcustomers_inq
int X=517
int Y=293
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_checkcustomers_inq
int X=517
int Y=169
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_checkcustomers_inq
int X=517
int Y=49
end type

type cbx_1 from checkbox within w_checkcustomers_inq
int X=14
int Y=53
int Width=467
int Height=77
boolean BringToTop=true
string Text="Location Specific"
boolean LeftText=true
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

