﻿$PBExportHeader$u_errorcontext.sru
forward
global type u_errorcontext from u_abstracterrorcontext
end type
end forward

global type u_errorcontext from u_abstracterrorcontext
end type
global u_errorcontext u_errorcontext

type variables
Private:
String	is_Message
Long	il_ReturnCode = 0
String	is_HashKey[]
Any	ia_HashValue[]
Long	il_HashCount = 0
end variables

forward prototypes
public function boolean uf_issuccessful ()
public function boolean uf_initialize ()
public function boolean uf_setreturncode (long al_returncode)
public function long uf_getreturncode ()
public function boolean uf_set (string as_key, string as_value)
public function boolean uf_set (string as_key, long al_value)
public function boolean uf_set (string as_key, integer ai_value)
public function boolean uf_set (string as_key, datetime adt_value)
public function boolean uf_set (string as_key, real ar_value)
public function boolean uf_set (string as_key, boolean ab_value)
public function boolean uf_set (string as_key, decimal adc_value)
public function boolean uf_get (string as_key, ref string as_value)
public function boolean uf_get (string as_key, ref long al_value)
public function boolean uf_get (string as_key, ref integer ai_value)
public function boolean uf_get (string as_key, ref datetime adt_value)
public function boolean uf_get (string as_key, ref real ar_value)
public function boolean uf_get (string as_key, ref boolean ab_value)
public function boolean uf_get (string as_key, ref decimal adc_value)
protected function long uf_findkey (string as_key)
public function boolean uf_appendtext (string as_message)
public function boolean uf_settext (string as_message)
public function string uf_gettext ()
public function boolean uf_getall (ref string as_values)
end prototypes

public function boolean uf_issuccessful ();
/* --------------------------------------------------------

<DESC>	Test whether a given error context
			represents an error condition.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	This function returns true if the
			return code set by uf_SetReturnCode() is
			zero and false otherwise.</USAGE>
-------------------------------------------------------- */
Return (This.uf_GetReturnCode() = 0)
		
end function

public function boolean uf_initialize ();
/* --------------------------------------------------------

<DESC>	Initialize the error context.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	This function resets the hash table used by
			the uf_Set() and uf_Get() methods.  It also
			resets the message text used by the
			uf_SetText(), uf_AppendText(), and uf_GetText()
			methods, and it resets the return code
			used by the uf_SetReturnCode(), 
			uf_GetReturnCode(), and uf_IsSuccessful()
			functions.</USAGE>
-------------------------------------------------------- */
Any		la_null[]
String	ls_null[]

ia_hashvalue = la_null
is_hashkey = ls_null
il_hashcount = 0
is_Message = ""
il_ReturnCode = 0
Return True
end function

public function boolean uf_setreturncode (long al_returncode);
/* --------------------------------------------------------

<DESC>	Set the return code for the error context.</DESC>

<ARGS>	al_returncode: Return code to set</ARGS>
			
<USAGE>	Set the return code to a nonzero value to make
			the uf_IsSuccessful() call return false.</USAGE>
-------------------------------------------------------- */
il_returncode = al_returncode
Return True
end function

public function long uf_getreturncode ();
/* --------------------------------------------------------

<DESC>	Get the return code.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Use this function to obtain the return code set
			by uf_SetReturnCode().
			</USAGE>
-------------------------------------------------------- */
Return il_returncode
end function

public function boolean uf_set (string as_key, string as_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			as_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= as_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = as_value
	Return False
End If

end function

public function boolean uf_set (string as_key, long al_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			al_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= al_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = al_value
	Return False
End If

end function

public function boolean uf_set (string as_key, integer ai_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			ai_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= ai_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = ai_value
	Return False
End If

end function

public function boolean uf_set (string as_key, datetime adt_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			adt_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= adt_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = adt_value
	Return False
End If

end function

public function boolean uf_set (string as_key, real ar_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			ar_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= ar_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = ar_value
	Return False
End If

end function

public function boolean uf_set (string as_key, boolean ab_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			ab_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= ab_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = ab_value
	Return False
End If

end function

public function boolean uf_set (string as_key, decimal adc_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			adc_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			This function returns true if the key was
			previously set with a value, otherwise false.</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	ia_hashvalue[ll_index]	= adc_value
	Return True
Else
	il_hashcount++
	is_hashkey[il_hashcount] = as_key
	ia_hashvalue[il_hashcount] = adc_value
	Return False
End If

end function

public function boolean uf_get (string as_key, ref string as_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			as_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "string".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "string" Then
		as_value = String(ia_hashvalue[ll_index])
		Return True
	End If
End If

Return False

end function

public function boolean uf_get (string as_key, ref long al_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			al_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "long".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "long" Then
		al_value = ia_hashvalue[ll_index]
		Return True
	End If
End If

Return False

end function

public function boolean uf_get (string as_key, ref integer ai_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			ai_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "integer".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "integer" Then
		ai_value = ia_hashvalue[ll_index]
		Return True
	End If
End If

Return False

end function

public function boolean uf_get (string as_key, ref datetime adt_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			adt_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "datetime".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "datetime" Then
		adt_value = ia_hashvalue[ll_index]
		Return True
	End If
End If

Return False

end function

public function boolean uf_get (string as_key, ref real ar_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			ar_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "real".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "real" Then
		ar_value = ia_hashvalue[ll_index]
		Return True
	End If
End If

Return False

end function

public function boolean uf_get (string as_key, ref boolean ab_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			ab_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "boolean".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "boolean" Then
		ab_value = ia_hashvalue[ll_index]
		Return True
	End If
End If

Return False

end function

public function boolean uf_get (string as_key, ref decimal adc_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			adc_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			This function returns false if the key is not
			found or if the ClassName() of the key's value is
			not "decimal".</USAGE>
-------------------------------------------------------- */
Long	ll_index

ll_index = uf_findkey(as_key)

If ll_index > 0 Then
	If ClassName(ia_hashvalue[ll_index]) = "decimal" Then
		adc_value = ia_hashvalue[ll_index]
		Return True
	End If
End If

Return False

end function

protected function long uf_findkey (string as_key);
/* --------------------------------------------------------

<DESC>	Find the specified key in the hash array.</DESC>

<ARGS>	as_key: The key to query for a value</ARGS>
			
<USAGE>	Use this function to obtain the array index for
			the specified key, or zero if it's not found.
			This function is protected.
			</USAGE>
-------------------------------------------------------- */
Long	ll_counter

For ll_counter = 1 to il_hashcount 
	If Not IsNull(is_hashkey[ll_counter]) Then
		If Lower(Trim(is_hashkey[ll_counter])) = Lower(Trim(as_key)) Then
			Return ll_counter
		End If
	End If
Next
Return 0
end function

public function boolean uf_appendtext (string as_message);
/* --------------------------------------------------------

<DESC>	Append a message text to the context.</DESC>

<ARGS>	as_message: The text to append to the message.</ARGS>
			
<USAGE>	Use this function to append additional information
			about any error context.  This function adds a
			CR/LF between successive calls.</USAGE>
-------------------------------------------------------- */
If Len(Trim(is_message)) = 0 Then
	is_message = as_message
Else
	is_message += "~r~n" + as_message
End If
Return True
end function

public function boolean uf_settext (string as_message);
/* --------------------------------------------------------

<DESC>	Set the message text for the context.</DESC>

<ARGS>	as_message: The text to set the message to.</ARGS>
			
<USAGE>	Use this function to set information
			about the error context.</USAGE>
-------------------------------------------------------- */
is_message = as_message
Return True
end function

public function string uf_gettext ();
/* --------------------------------------------------------

<DESC>	Get the message text for the context.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Use this function to get information
			about the error context.</USAGE>
-------------------------------------------------------- */
Return is_message
end function

public function boolean uf_getall (ref string as_values);
/* --------------------------------------------------------

<DESC>	Retrieve all the hash values.</DESC>

<ARGS>	as_values: The variable to receive the key/value pairs.</ARGS>
			
<USAGE>	Use this function to obtain a tab-delimited
			string with all key/value pairs set on this
			object by uf_Set() functions.</USAGE>
-------------------------------------------------------- */
Integer	li_counter

as_values = ""

For li_counter = 1 to il_hashcount
	as_values += is_hashkey[li_counter] + "~t" + String(ia_hashvalue[li_counter]) + "~r~n"
Next

as_values += "Return Code" + "~t" + String(il_returncode) + "~r~n"

Return True
end function

on u_errorcontext.create
TriggerEvent( this, "constructor" )
end on

on u_errorcontext.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;
/* --------------------------------------------------------
u_errorcontext

<OBJECT>	This object's purpose is to hold information
			relating to possible error or notification
			conditions.</OBJECT>
			
<USAGE>	Use this object to contain information about
			events which may need user notification.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */

end event

