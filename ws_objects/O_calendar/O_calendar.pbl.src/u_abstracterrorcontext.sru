﻿$PBExportHeader$u_abstracterrorcontext.sru
forward
global type u_abstracterrorcontext from nonvisualobject
end type
end forward

global type u_abstracterrorcontext from nonvisualobject
end type
global u_abstracterrorcontext u_abstracterrorcontext

forward prototypes
public function boolean uf_issuccessful ()
public function boolean uf_initialize ()
public function boolean uf_setreturncode (long al_returncode)
public function long uf_getreturncode ()
public function boolean uf_set (string as_key, string as_value)
public function boolean uf_set (string as_key, long al_value)
public function boolean uf_set (string as_key, integer ai_value)
public function boolean uf_set (string as_key, datetime adt_value)
public function boolean uf_set (string as_key, real ar_value)
public function boolean uf_set (string as_key, boolean ab_value)
public function boolean uf_set (string as_key, decimal adc_value)
public function boolean uf_get (string as_key, ref string as_value)
public function boolean uf_get (string as_key, ref long al_value)
public function boolean uf_get (string as_key, ref integer ai_value)
public function boolean uf_get (string as_key, ref datetime adt_value)
public function boolean uf_get (string as_key, ref real ar_value)
public function boolean uf_get (string as_key, ref boolean ab_value)
public function boolean uf_get (string as_key, ref decimal adc_value)
public function boolean uf_appendtext (string as_message)
public function boolean uf_settext (string as_message)
public function string uf_gettext ()
public function boolean uf_getall (ref string as_values)
end prototypes

public function boolean uf_issuccessful ();
/* --------------------------------------------------------

<DESC>	Test whether a given error context
			represents an error condition.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	This function returns true if the
			return code set by uf_SetReturnCode() is
			zero and false otherwise.</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_initialize ();
/* --------------------------------------------------------

<DESC>	Initialize the error context.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	This function resets the hash table used by
			the uf_Set() and uf_Get() methods.  It also
			resets the message text used by the
			uf_SetText(), uf_AppendText(), and uf_GetText()
			methods, and it resets the return code
			used by the uf_SetReturnCode(), 
			uf_GetReturnCode(), and uf_IsSuccessful()
			functions.</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_setreturncode (long al_returncode);
/* --------------------------------------------------------

<DESC>	Set the return code for the error context.</DESC>

<ARGS>	al_returncode: Return code to set</ARGS>
			
<USAGE>	Set the return code to a nonzero value to make
			the uf_IsSuccessful() call return false.</USAGE>
-------------------------------------------------------- */
Return False
end function

public function long uf_getreturncode ();
/* --------------------------------------------------------

<DESC>	Get the return code.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Use this function to obtain the return code set
			by uf_SetReturnCode().
			</USAGE>
-------------------------------------------------------- */
Return 0
end function

public function boolean uf_set (string as_key, string as_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			as_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_set (string as_key, long al_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			al_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_set (string as_key, integer ai_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			ai_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_set (string as_key, datetime adt_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			adt_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_set (string as_key, real ar_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			ar_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_set (string as_key, boolean ab_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			ab_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_set (string as_key, decimal adc_value);
/* --------------------------------------------------------

<DESC>	Set a hash value.</DESC>

<ARGS>	as_key: The key to set a value for
			adc_value: The value to set for the key</ARGS>
			
<USAGE>	Use this function to set a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref string as_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			as_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref long al_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			al_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref integer ai_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			ai_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref datetime adt_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			adt_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref real ar_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			ar_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref boolean ab_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			ab_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_get (string as_key, ref decimal adc_value);
/* --------------------------------------------------------

<DESC>	Get a hash value.</DESC>

<ARGS>	as_key: The key to query for a value
			adc_value: The variable to receive the hash value</ARGS>
			
<USAGE>	Use this function to get a value for a specified key.
			</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_appendtext (string as_message);
/* --------------------------------------------------------

<DESC>	Append a message text to the context.</DESC>

<ARGS>	as_message: The text to append to the message.</ARGS>
			
<USAGE>	Use this function to append additional information
			about any error context.</USAGE>
-------------------------------------------------------- */
Return False
end function

public function boolean uf_settext (string as_message);
/* --------------------------------------------------------

<DESC>	Set the message text for the context.</DESC>

<ARGS>	as_message: The text to set the message to.</ARGS>
			
<USAGE>	Use this function to set information
			about any error context.</USAGE>
-------------------------------------------------------- */
Return False
end function

public function string uf_gettext ();
/* --------------------------------------------------------

<DESC>	Get the message text for the context.</DESC>

<ARGS>	</ARGS>
			
<USAGE>	Use this function to get information
			about any error context.</USAGE>
-------------------------------------------------------- */
Return ""
end function

public function boolean uf_getall (ref string as_values);
/* --------------------------------------------------------

<DESC>	Retrieve all the hash values.</DESC>

<ARGS>	as_values: The variable to receive the key/value pairs.</ARGS>
			
<USAGE>	Use this function to obtain a tab-delimited
			string with all key/value pairs set on this
			object by uf_Set() functions.</USAGE>
-------------------------------------------------------- */
Return False
end function

on u_abstracterrorcontext.create
TriggerEvent( this, "constructor" )
end on

on u_abstracterrorcontext.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_abstracterrorcontext

<OBJECT>	This object's interface is to hold information
			relating to possible error or notification
			conditions.</OBJECT>
			
<USAGE>	Concrete descendants of this object should
			implement these methods.</USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */

end event

