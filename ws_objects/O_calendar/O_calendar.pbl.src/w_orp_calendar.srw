﻿$PBExportHeader$w_orp_calendar.srw
forward
global type w_orp_calendar from window
end type
type st_3 from statictext within w_orp_calendar
end type
type st_2 from statictext within w_orp_calendar
end type
type st_1 from statictext within w_orp_calendar
end type
type dw_calendar from datawindow within w_orp_calendar
end type
type cb_cancel from commandbutton within w_orp_calendar
end type
type cb_previous from commandbutton within w_orp_calendar
end type
type cb_next from commandbutton within w_orp_calendar
end type
type oval_begin from oval within w_orp_calendar
end type
type r_1 from rectangle within w_orp_calendar
end type
type rr_1 from roundrectangle within w_orp_calendar
end type
end forward

global type w_orp_calendar from window
integer x = 823
integer y = 356
integer width = 832
integer height = 1076
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = response!
st_3 st_3
st_2 st_2
st_1 st_1
dw_calendar dw_calendar
cb_cancel cb_cancel
cb_previous cb_previous
cb_next cb_next
oval_begin oval_begin
r_1 r_1
rr_1 rr_1
end type
global w_orp_calendar w_orp_calendar

type variables
Private:
u_AbstractCalendar	iu_Calendar
u_AbstractParameterStack	iu_ParameterStack
end variables

on w_orp_calendar.create
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.dw_calendar=create dw_calendar
this.cb_cancel=create cb_cancel
this.cb_previous=create cb_previous
this.cb_next=create cb_next
this.oval_begin=create oval_begin
this.r_1=create r_1
this.rr_1=create rr_1
this.Control[]={this.st_3,&
this.st_2,&
this.st_1,&
this.dw_calendar,&
this.cb_cancel,&
this.cb_previous,&
this.cb_next,&
this.oval_begin,&
this.r_1,&
this.rr_1}
end on

on w_orp_calendar.destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_calendar)
destroy(this.cb_cancel)
destroy(this.cb_previous)
destroy(this.cb_next)
destroy(this.oval_begin)
destroy(this.r_1)
destroy(this.rr_1)
end on

event open;/* --------------------------------------------------------
w_calendar

<OBJECT>	This object is a utility to display a calendar.
			The calendar-related processing is encapsulated in an object
			inherited from u_AbstractCalendar (currently u_calendar).
			</OBJECT>
			
<USAGE>	Open this response window with a parameter stack consisting of 
			the following objects pushed onto the stack in the given order:
			<UL>
			<LI>u_AbstractClassFactory:  Class factory</LI>
			<LI>String:  String representation of the initial date. 
			If it is not a valid date, Today() will be used.</LI>
			<LI>String:  DataWindow object to use</LI>
			<LI>u_AbstractErrorContext: Error context</LI>
			</UL>
			The window returns a parameter stack consisting of the
			following objects to be popped off the stack in the given
			order:
			<UL>
			<LI>u_AbstractErrorContext: Error context</LI>
			<LI>String: String representation of the chosen date.</LI>
			</UL>
			If the error context has a nonzero return code, the
			string representation of the chosen date is not on the
			parameter stack.
			</USAGE>

<AUTH>	Jim Weier	</AUTH>

--------------------------------------------------------- */
u_AbstractErrorContext		lu_ErrorContext
u_AbstractClassFactory		lu_ClassFactory

Integer	li_ret

String	ls_ObjectName,&
			ls_InitDate

iu_ParameterStack = Message.PowerObjectParm

Long	ll_NewX, &
		ll_NewY, &
		ll_FrameX, &
		ll_FrameY
		
ll_FrameX = gw_netwise_frame.X
ll_FrameY = gw_netwise_frame.Y

ll_NewX = gw_netwise_frame.PointerX()
ll_NewY = gw_netwise_frame.PointerY()

li_ret = This.Move(ll_FrameX + ll_NewX, ll_FrameY + ll_NewY)

IF Not iu_ParameterStack.uf_Pop("u_AbstractErrorContext", lu_ErrorContext) Then 
	lu_ErrorContext = Create Using "u_ErrorContext"
	lu_ErrorContext.uf_Initialize()
	lu_ErrorContext.uf_SetReturnCode(-1)
	lu_ErrorContext.uf_AppendText("Missing Parameter: Error Context")
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not iu_ParameterStack.uf_Pop("String", ls_ObjectName) Then
	lu_ErrorContext.uf_AppendText("Missing Parameter: DataObject")
	lu_ErrorContext.uf_SetReturnCode(-2)
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not iu_ParameterStack.uf_Pop("String", ls_InitDate) Then
	lu_ErrorContext.uf_AppendText("Missing Parameter: Initial Date")
	lu_ErrorContext.uf_SetReturnCode(-3)
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not iu_ParameterStack.uf_Pop("u_AbstractClassFactory", lu_ClassFactory) Then
	lu_ErrorContext.uf_AppendText("Missing Parameter: Class Factory")
	lu_ErrorContext.uf_SetReturnCode(-4)
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
END IF

IF Not IsDate(ls_InitDate) Then
	ls_InitDate = String(Today(),"mm/dd/yyyy")
else
	Choose CASE DaysAfter(Today(), date(ls_InitDate)) 
	
	CASE -18000 to 18000
	// Date is within range so leave alone
	CASE ELSE
		ls_InitDate = String(Today(),"mm/dd/yyyy")
	END CHOOSE	
end if	

lu_ClassFactory.uf_GetObject("u_calendar", iu_calendar, lu_ErrorContext) 
If Not IsValid(iu_calendar) Then
	lu_ErrorContext.uf_SetReturnCode(-5)
	lu_ErrorContext.uf_AppendText("Can't get calendar object")
	iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
	CloseWithReturn(This, iu_ParameterStack)
	Return
ELSE
	IF Not iu_Calendar.uf_Initialize(lu_ClassFactory, dw_calendar, ls_ObjectName, &
										ls_InitDate, lu_ErrorContext)	Then
		lu_ErrorContext.uf_AppendText("Can't initialize calendar object")
		lu_ErrorContext.uf_SetReturnCode(-6)
		iu_ParameterStack.uf_push("u_AbstractErrorContext", lu_ErrorContext)
		CloseWithReturn(This, iu_ParameterStack)
		Return
	ELSE
		This.Title = iu_Calendar.uf_Display()
	END IF
END IF

Return

end event

event key;
/* --------------------------------------------------------

<DESC>	Calls a function on the calendar user object 
			depending on the key the user presses. The keys 
			handled are:
			<UL>
			<LI>KeyPageUp!</LI>
			<LI>KeyPageDown!</LI>
			<LI>KeyLeftArrow!</LI>
			<LI>KeyUpArrow!</LI>
			<LI>KeyRightArrow!</LI>
			<LI>KeyDownArrow!</LI>
			<LI>KeyEnter!</LI>
			</UL>
			If the user presses KeyEnter!, the date chosen is
			pushed onto the parameter stack as a string (after
			a successful error context) and the window closes.
</DESC>

-------------------------------------------------------- */
String	ls_Date
u_AbstractErrorContext	lu_ErrorContext

Choose Case key
	Case	KeyPageUp!
		This.Title = iu_Calendar.uf_KeyPageUp()
	Case	KeyPageDown!
		This.Title = iu_Calendar.uf_KeyPageDown()		
	Case	KeyLeftArrow!
		This.Title = iu_Calendar.uf_KeyLeftArrow()
	Case	KeyUpArrow!
		This.Title = iu_Calendar.uf_KeyUpArrow()	
	Case	KeyRightArrow!
		This.Title = iu_Calendar.uf_KeyRightArrow()
	Case	KeyDownArrow!
		This.Title = iu_Calendar.uf_KeyDownArrow()
	Case KeyEnter!
		ls_Date = iu_calendar.uf_KeyEnter()
		lu_ErrorContext = CREATE USING "u_ErrorContext"
		lu_ErrorContext.uf_Initialize()
		lu_ErrorContext.uf_SetReturnCode(0)
		iu_parameterstack.uf_Push("String",ls_Date)
		iu_parameterstack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)
		CloseWithReturn(This, iu_parameterstack)
	CASE ELSE
End Choose
		
end event

type st_3 from statictext within w_orp_calendar
integer x = 137
integer y = 892
integer width = 411
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean enabled = false
string text = "Next Contract Due"
boolean focusrectangle = false
end type

type st_2 from statictext within w_orp_calendar
integer x = 142
integer y = 824
integer width = 361
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean enabled = false
string text = "Begin Ship Date"
boolean focusrectangle = false
end type

type st_1 from statictext within w_orp_calendar
integer x = 146
integer y = 756
integer width = 325
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean enabled = false
string text = "End Ship Date"
boolean focusrectangle = false
end type

type dw_calendar from datawindow within w_orp_calendar
integer x = 37
integer y = 136
integer width = 695
integer height = 484
integer taborder = 30
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;This.InsertRow(0)
end event

event clicked;
/* --------------------------------------------------------

<DESC>	Happens when user clicks on the DataWindow control.

Usage:	If a valid date is returned by the calendar object the string
			representation of the date is pushed onto the stack and the
			window is closed.
			
</DESC>
-------------------------------------------------------- */

String	ls_Date
u_AbstractErrorContext lu_ErrorContext

if Row < 1 then Return

ls_Date = iu_calendar.uf_Clicked(row, String(dwo.Name))
IF NOT IsDate(ls_Date) Then return

lu_ErrorContext = CREATE USING "u_ErrorContext"
lu_ErrorContext.uf_Initialize()
lu_ErrorContext.uf_SetReturnCode(0)
iu_ParameterStack.uf_Push("String", ls_Date)
iu_parameterstack.uf_Push("u_AbstractErrorContext", lu_ErrorContext)
CloseWithReturn(Parent,iu_ParameterStack)

end event

type cb_cancel from commandbutton within w_orp_calendar
integer x = 37
integer y = 624
integer width = 690
integer height = 96
integer taborder = 30
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Cancel"
boolean cancel = true
end type

event clicked;

/* --------------------------------------------------------

<DESC>	Calls uf_Cancel() on the calendar UserObject.
			Pushes the string that represents cancel to the calling 
			window onto the stack and closes the window.
</DESC>

-------------------------------------------------------- */
String	ls_CancelReturn
u_AbstractErrorContext lu_ErrorContext

ls_CancelReturn  = iu_calendar.uf_Cancel()

lu_ErrorContext = CREATE USING "u_ErrorContext"
lu_ErrorContext.uf_Initialize()
lu_ErrorContext.uf_SetReturnCode(0)
iu_ParameterStack.uf_Push("String", ls_CancelReturn)
iu_ParameterStack.uf_Push("u_AbstractErrorContext", lu_ErrorContext)
CloseWithReturn(Parent, iu_parameterstack)
end event

type cb_previous from commandbutton within w_orp_calendar
integer x = 37
integer y = 36
integer width = 343
integer height = 96
integer taborder = 20
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<<&Previous"
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Calls uf_Previous() on the calendar UserObject.
			Sets the title appropriately.
			</DESC>
-------------------------------------------------------- */
Parent.Title = iu_calendar.uf_Previous()
end event

type cb_next from commandbutton within w_orp_calendar
integer x = 375
integer y = 36
integer width = 343
integer height = 96
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Next>>"
end type

event clicked;
/* --------------------------------------------------------

<DESC>	Calls uf_next() on the calendar UserObject.
			Sets the title appropriately.
			
</DESC>
-------------------------------------------------------- */
Parent.Title = iu_calendar.uf_Next()
end event

type oval_begin from oval within w_orp_calendar
long linecolor = 255
integer linethickness = 4
long fillcolor = 33554431
integer x = 41
integer y = 760
integer width = 69
integer height = 48
end type

type r_1 from rectangle within w_orp_calendar
long linecolor = 32768
integer linethickness = 4
long fillcolor = 33554431
integer x = 41
integer y = 828
integer width = 64
integer height = 52
end type

type rr_1 from roundrectangle within w_orp_calendar
long linecolor = 16711680
integer linethickness = 8
long fillcolor = 33554431
integer x = 37
integer y = 896
integer width = 69
integer height = 48
integer cornerheight = 40
integer cornerwidth = 46
end type

