﻿$PBExportHeader$u_abstractclassfactory.sru
forward
global type u_abstractclassfactory from nonvisualobject
end type
end forward

global type u_abstractclassfactory from nonvisualobject
end type
global u_abstractclassfactory u_abstractclassfactory

forward prototypes
public function boolean uf_initialize (u_abstractobjectlist ao_theobjectlist, connection acon_theconnection)
public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject)
public subroutine uf_getresponsewindow (string as_theclassname, ref u_abstractparameterstack ao_theparamstack)
public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject, ref u_abstracterrorcontext ao_theerrorcontext)
private subroutine uf_appenderrormessage (string as_theMessage, ref u_abstracterrorcontext ao_theErrorContext)
private subroutine uf_seterrorreturnvalue (long al_theValue, ref u_abstracterrorcontext ao_theErrorContext)
public function boolean uf_setconnection (ref connection au_connection)
public function Boolean uf_setarrangeopen (ArrangeOpen aen_ArrangeOpen)
end prototypes

public function boolean uf_initialize (u_abstractobjectlist ao_theobjectlist, connection acon_theconnection);
/* --------------------------------------------------------

<DESC>	Initialize the ClassFactory.</DESC>

<ARGS>	ao_theobjectlist: Object List
			acon_theconnection: Connection</ARGS>
			
<USAGE>	Create an object list and call this function
			to associate it with this ClassFactory.  Pass
			NULL for the acon_TheConnection object for
			non-distributed applications. This function
			returns true on success, false otherwise.</USAGE>
			
-------------------------------------------------------- */
Return True
end function

public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject);
/* --------------------------------------------------------

<DESC>	Create an object in memory or obtain a reference.</DESC>

<ARGS>	as_TheClassName: Class of the object desired
			ao_TheObject: Variable to hold the reference obtained.
			</ARGS>
			
<USAGE>	Call this function to obtain a reference to a
			long-lived object, or to create a new instance of
			a short-lived object. An object is short-lived unless
			it is specified in the object list associated with
			this ClassFactory to be long-lived.  An object
			is instantiated on the local client unless it
			is specified otherwise in the object list associated
			with this ClassFactory.  The argument
			as_TheClassName is used to look up the entry in the 
			object list.  If as_TheClassName
			begins with the letter "w", the object is
			assumed to be a window and is opened as a 
			sheet window, unless specified otherwise in the
			object list associated with this ClassFactory.
			This function returns true if the object has
			been initialized (only for long-lived objects)
			and false otherwise.			</USAGE>
			
-------------------------------------------------------- */
Return True
end function

public subroutine uf_getresponsewindow (string as_theclassname, ref u_abstractparameterstack ao_theparamstack);
/* --------------------------------------------------------

<DESC>	Open a response window.</DESC>

<ARGS>	as_TheClassName: Class name of the window desired
			ao_TheParamStack: ParameterStack to pass to the window
			</ARGS>
			
<USAGE>	Call this function to open a response window. 
			The parameter stack passed to this function is
			passed to the response window in an OpenWithParm()
			call.  The response window should retrieve the
			parameter stack from Message.PowerObjectParm in
			its open event. The window should set 
			Message.PowerObjectParm to a ParameterStack before
			calling CloseWithReturn().</USAGE>
			
-------------------------------------------------------- */
end subroutine

public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject, ref u_abstracterrorcontext ao_theerrorcontext);
/* --------------------------------------------------------

<DESC>	Create an object in memory or obtain a reference.</DESC>

<ARGS>	as_TheClassName: Class of the object desired
			ao_TheObject: Variable to hold the reference obtained.
			ao_TheErrorContext: Error context
			</ARGS>
			
<USAGE>	Call this function to obtain a reference to a
			long-lived object, or to create a new instance of
			a short-lived object. An object is short-lived unless
			it is specified in the object list associated with
			this ClassFactory to be long-lived.  An object
			is instantiated on the local client unless it
			is specified otherwise in the object list associated
			with this ClassFactory.  The argument
			as_TheClassName is used to look up the entry in the 
			object list.  Errors are reported in the
			passed-in error context.  If as_TheClassName
			begins with the letter "w", the object is
			assumed to be a window and is opened as a 
			sheet window, unless specified otherwise in the
			object list associated with this ClassFactory.
			This function returns true if the object has
			been initialized (only for long-lived objects)
			and false otherwise.</USAGE>
			
-------------------------------------------------------- */
Return True
end function

private subroutine uf_appenderrormessage (string as_theMessage, ref u_abstracterrorcontext ao_theErrorContext);
end subroutine

private subroutine uf_seterrorreturnvalue (long al_theValue, ref u_abstracterrorcontext ao_theErrorContext);
end subroutine

public function boolean uf_setconnection (ref connection au_connection);
/* --------------------------------------------------------

<DESC>	Set the connection object.</DESC>

<ARGS>	au_connection: Connection</ARGS>
			
<USAGE>	After initializing the class factory with a
			null connection object (or no connection object),
			call this function with a valid, connected
			connection object as a parameter.  After that,
			future calls to the class factory can use the
			connection object to instantiate distributed
			objects.</USAGE>
			
-------------------------------------------------------- */
Return True
end function

public function Boolean uf_setarrangeopen (ArrangeOpen aen_ArrangeOpen);Return False
end function

on u_abstractclassfactory.create
TriggerEvent( this, "constructor" )
end on

on u_abstractclassfactory.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;
/* --------------------------------------------------------
u_abstractclassfactory

<OBJECT>	This interface handles memory management and
			object lifetimes.  Additionally, it may be
			able to manage distributed services.</OBJECT>
			
<USAGE>	Concrete descendants of this interface should
			implement these methods. </USAGE>

<AUTH>	Conrad Engel	</AUTH>

--------------------------------------------------------- */

end event

