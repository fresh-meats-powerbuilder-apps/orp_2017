﻿$PBExportHeader$u_abstractparameterstack.sru
forward
global type u_abstractparameterstack from nonvisualobject
end type
end forward

global type u_abstractparameterstack from nonvisualobject
end type
global u_abstractparameterstack u_abstractparameterstack

forward prototypes
public function boolean uf_initialize ()
public function boolean uf_push (string as_theClassName, powerobject ao_theParm)
public function boolean uf_push (string as_theClassName, string as_theParm)
public function boolean uf_push (string as_theClassName, long al_theParm)
public function boolean uf_push (string as_theClassName, integer ai_theParm)
public function boolean uf_push (string as_theClassName, datetime adt_theParm)
public function boolean uf_push (string as_theClassName, real ar_theParm)
public function boolean uf_push (string as_theClassName, decimal adc_theParm)
public function boolean uf_push (string as_theClassname, boolean ab_theParm)
public function boolean uf_pop (string as_theClassName, ref powerobject ao_theParm)
public function boolean uf_pop (string as_theClassName, ref string as_theParm)
public function boolean uf_pop (string as_theClassName, ref long al_theParm)
public function boolean uf_pop (string as_theClassName, ref integer ai_theParm)
public function boolean uf_pop (string as_theClassName, ref datetime adt_theParm)
public function boolean uf_pop (string as_theClassName, ref real ar_theParm)
public function boolean uf_pop (string as_theClassName, ref decimal adc_theParm)
public function boolean uf_pop (string as_theClassName, ref boolean ab_theParm)
public function boolean uf_pop (string as_theclassname, ref date ad_theparm)
public function boolean uf_push (string as_theclassname, date ad_theparm)
end prototypes

public function boolean uf_initialize ();return true
end function

public function boolean uf_push (string as_theClassName, powerobject ao_theParm);return True
end function

public function boolean uf_push (string as_theClassName, string as_theParm);return true
end function

public function boolean uf_push (string as_theClassName, long al_theParm);return True
end function

public function boolean uf_push (string as_theClassName, integer ai_theParm);Return True
end function

public function boolean uf_push (string as_theClassName, datetime adt_theParm);Return True
end function

public function boolean uf_push (string as_theClassName, real ar_theParm);Return True
end function

public function boolean uf_push (string as_theClassName, decimal adc_theParm);Return True
end function

public function boolean uf_push (string as_theClassname, boolean ab_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref powerobject ao_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref string as_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref long al_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref integer ai_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref datetime adt_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref real ar_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref decimal adc_theParm);Return True
end function

public function boolean uf_pop (string as_theClassName, ref boolean ab_theParm);Return True
end function

public function boolean uf_pop (string as_theclassname, ref date ad_theparm);Return True
end function

public function boolean uf_push (string as_theclassname, date ad_theparm);Return True
end function

on u_abstractparameterstack.create
TriggerEvent( this, "constructor" )
end on

on u_abstractparameterstack.destroy
TriggerEvent( this, "destructor" )
end on

