﻿$PBExportHeader$u_gpo_functions.sru
forward
global type u_gpo_functions from nonvisualobject
end type
end forward

global type u_gpo_functions from nonvisualobject
end type
global u_gpo_functions u_gpo_functions

type variables

end variables

forward prototypes
public function integer nf_getgpomonth (date gpodate)
public function integer nf_getgpoweek (date gpodate)
public function integer nf_getgpoyear (date gpodate)
public function string nf_getgpodate (date gpo_date)
public function date nf_get_end_shipdate (date gpo_date)
public function integer nf_gpo_week (integer ai_month, long al_year)
public function date nf_get_contract_due_date (date gpo_date)
public function date nf_monthstartdate (date gpo_date)
public function date nf_get_begin_shipdate (date gpo_date)
end prototypes

public function integer nf_getgpomonth (date gpodate);u_String_Functions	lu_StringFunctions
String	ls_Temp_String,&
			ls_Token
			
ls_Temp_String = nf_GetGPODate(gpodate)
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
Return Integer(ls_Token)
end function

public function integer nf_getgpoweek (date gpodate);u_String_Functions	lu_StringFunctions
String	ls_Temp_String,&
			ls_Token
			
ls_Temp_String = nf_GetGPODate(gpodate)
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
Return Integer(ls_Token)
end function

public function integer nf_getgpoyear (date gpodate);u_String_Functions	lu_StringFunctions
String	ls_Temp_String,&
			ls_Token
			
ls_Temp_String = nf_GetGPODate(gpodate)
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
ls_Token = lu_StringFunctions.nf_GetToken(ls_Temp_String,"~t")
ls_Token = ls_Temp_String
Return Integer(ls_Token)
end function

public function string nf_getgpodate (date gpo_date);Date		ld_MonthStart

Integer	li_Number_OF_Weeks,&
			li_Comparemonth = 12,&
			li_CompareYear = 1998,&
			li_NumberofWeeks = 0,&
			li_GpoMonth,&
			li_GPOYear

String	ls_1stOfMonth
ld_MonthStart = Date("12/28/1998") 
li_GPOMonth = Month(gpo_Date) 
li_GPOYear = Year(gpo_Date)
// GPO Dates are calculated from Janauary 1999 Start Date (December 28, 1998)
IF gpo_date >= ld_MonthStart  then
	do While ((li_GPOMonth <> li_CompareMonth) OR ( li_GPOYear <> li_CompareYear))
		if li_CompareMonth = 12 then
			li_CompareMonth = 1
			li_CompareYear += 1
		Else
			li_CompareMonth += 1
		End if
		li_NumberOfWeeks += nf_gpo_week(li_CompareMonth,li_CompareYear)
	Loop 
Else
	do While (Month(gpo_Date) <> li_CompareMonth) and (Year(gpo_Date) <> li_CompareYear)
		if li_CompareMonth = 1 then
			li_CompareMonth = 12
			li_CompareYear -= 1
		Else
			li_CompareMonth -= 1
		End if
		li_NumberOfWeeks -= nf_gpo_week(li_CompareMonth,li_CompareYear)
	Loop 

End if
ld_MonthStart = RelativeDate(LD_MonthStart,li_NumberOfWeeks*7)
Choose Case True
	Case ld_MonthStart <= gpo_date and RelativeDate(ld_MonthStart,+6 ) >= gpo_date
		Return "1~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))
	Case	RelativeDate(ld_MonthStart,+7) <= gpo_date and RelativeDate(ld_MonthStart,+13) >= gpo_date 
		Return "2~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))
	Case	RelativeDate(ld_MonthStart,+14) <= gpo_date and RelativeDate(ld_MonthStart,+20) >= gpo_date 
		Return "3~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))	
	Case	RelativeDate(ld_MonthStart,+21) <= gpo_date and RelativeDate(ld_MonthStart,+27) >=gpo_date 
		Return "4~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))	
	Case	RelativeDate(ld_MonthStart,+28) <= gpo_date and RelativeDate(ld_MonthStart,+34) >=gpo_date 
		IF li_Number_OF_Weeks = 5 Then
			Return "5~t"+String(Month(gpo_date))	
		ELSE
			IF Month(gpo_date) = 12 Then
				Return "1~t01"+"~t"+String(Year(gpo_date)+1)
			ELSE
				Return "1~t"+String(Month(gpo_date)+1)+"~t"+String(Year(gpo_date))	
			END IF
		END IF
	Case Else
		Return "1~t"+String(Month(gpo_date))+"~t"+String(Year(gpo_date))
End Choose

end function

public function date nf_get_end_shipdate (date gpo_date);// End Shipdate is Day Prior to Next Months StartDate
Date		ld_Begin_ShipDate

Integer	li_Number_of_Weeks,&
			li_month,&
			li_Year
			
			
ld_Begin_ShipDate = nf_get_Begin_ShipDate(gpo_date)

Return RelativeDate(ld_Begin_SHipDate,-1)

end function

public function integer nf_gpo_week (integer ai_month, long al_year);//Determines the NUMBer of GPO weeks per month

Integer		li_week_number
Boolean		lb_Tyson_calendar

if al_year >= 2001 then
	if al_year = 2001 then
		if ai_month > 9 then
			lb_Tyson_calendar = TRUE
		else
			lb_Tyson_calendar = FALSE
		end if 
	else
		lb_Tyson_calendar = TRUE
	end if
else
	lb_Tyson_calendar = FALSE
end if

if lb_Tyson_calendar then
	
	Choose Case ai_month
	Case 1
		li_week_number = 4
	Case 2
		li_week_number = 5
	Case 3
		li_week_number = 4
	Case 4
		li_week_number = 4
	Case 5
		li_week_number = 5
	Case 6
		li_week_number = 4
	Case 7
		li_week_number = 4
	Case 8
		li_week_number = 5
	Case 9
		if (al_year = 2004) or (al_year = 2009) or (al_year = 2015) or (al_year = 2020) or (al_year = 2026) then
			li_week_number = 5
		else
			li_week_number = 4
		end if
	Case 10
		li_week_number = 4
	Case 11
		li_week_number = 5
	Case 12
		li_week_number = 4
	End Choose

else
	
	Choose Case ai_month
	Case 1
		li_week_number = 4
	Case 2
		li_week_number = 4
	Case 3
		li_week_number = 5
	Case 4
		li_week_number = 4
	Case 5
		li_week_number = 4
	Case 6
		li_week_number = 5
	Case 7
		li_week_number = 4
	Case 8
		li_week_number = 4
	Case 9
		li_week_number = 5
	Case 10
		If DayNumber(date(string(String(al_year - 1) + "-12-31"))) = 6 Then 
			li_week_number = 5 
		Else
			li_week_number = 4
		End If
	Case 11
		li_week_number = 4
	Case 12
		li_week_number = 5
	End Choose
	
end if

Return li_week_number
end function

public function date nf_get_contract_due_date (date gpo_date);// ******************************************************************************************
// Contract Due Date is the Wednesday after 2 weeks prior of the Start of the Next Month
// For Example if Next months Start Date is on the 28th the Monday  2 weeks Prior would be 
// the 14th So that Next Wednesday is the 16th -
// Contract Due Date for the Next Month is the 16th
// ******************************************************************************************
Date		ld_Begin_ShipDate

Integer	li_Number_of_Weeks,&
			li_month,&
			li_Year
			
			
ld_Begin_ShipDate = nf_get_Begin_ShipDate(gpo_date)
if ld_Begin_ShipDate >= date('2003-09-27') then
	Return RelativeDate(ld_Begin_ShipDate, -11)
else
	Return RelativeDate(ld_Begin_ShipDate,- 12)
end if
end function

public function date nf_monthstartdate (date gpo_date);Date		ld_MonthStart

Integer	li_Number_OF_Weeks,&
			li_Comparemonth = 12,&
			li_CompareYear = 1998,&
			li_NumberofWeeks = 0,&
			li_GpoMonth,&
			li_GPOYear

String	ls_1stOfMonth
ld_MonthStart = Date("12/28/1998") 
li_GPOMonth = Month(gpo_Date) 
li_GPOYear = Year(gpo_Date)
// GPO Dates are calculated from Janauary 1999 Start Date (December 28, 1998)
IF gpo_date >= ld_MonthStart  then
	do While ((li_GPOMonth <> li_CompareMonth) OR ( li_GPOYear <> li_CompareYear))
		if li_CompareMonth = 12 then
			li_CompareMonth = 1
			li_CompareYear += 1
		Else
			li_CompareMonth += 1
		End if
		li_NumberOfWeeks += nf_gpo_week(li_CompareMonth,li_CompareYear)
	Loop 
Else
	do While (Month(gpo_Date) <> li_CompareMonth) and (Year(gpo_Date) <> li_CompareYear)
		if li_CompareMonth = 1 then
			li_CompareMonth = 12
			li_CompareYear -= 1
		Else
			li_CompareMonth -= 1
		End if
		li_NumberOfWeeks -= nf_gpo_week(li_CompareMonth,li_CompareYear)
	Loop 

End if
ld_MonthStart = RelativeDate(LD_MonthStart,li_NumberOfWeeks*7)

if ld_MonthStart >= date('2003-09-29') then
	ld_MonthStart = RelativeDate(ld_MonthStart, -1)
end if
	
Return ld_MonthStart
end function

public function date nf_get_begin_shipdate (date gpo_date);// Gets the begin ShipDate of the Month in the Date Sent
Date		ld_MonthStart

ld_MonthStart = nf_MonthStartDate(gpo_date)

Return ld_MonthStart

end function

on u_gpo_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_gpo_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

