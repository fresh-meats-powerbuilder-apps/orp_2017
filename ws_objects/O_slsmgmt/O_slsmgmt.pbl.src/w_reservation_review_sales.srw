﻿$PBExportHeader$w_reservation_review_sales.srw
$PBExportComments$Reservation Review for Sales Management
forward
global type w_reservation_review_sales from w_netwise_sheet
end type
type cb_next from commandbutton within w_reservation_review_sales
end type
type cb_prev from commandbutton within w_reservation_review_sales
end type
type st_endofqueue from statictext within w_reservation_review_sales
end type
type st_noqueue from statictext within w_reservation_review_sales
end type
type tab_1 from tab within w_reservation_review_sales
end type
type tabpage_1 from u_reservation_review_tab within tab_1
end type
type tabpage_1 from u_reservation_review_tab within tab_1
end type
type tabpage_2 from u_reservation_review_tab within tab_1
end type
type tabpage_2 from u_reservation_review_tab within tab_1
end type
type tabpage_3 from u_reservation_review_tab within tab_1
end type
type tabpage_3 from u_reservation_review_tab within tab_1
end type
type tabpage_4 from u_reservation_review_tab within tab_1
end type
type tabpage_4 from u_reservation_review_tab within tab_1
end type
type tabpage_5 from u_reservation_review_tab within tab_1
end type
type tabpage_5 from u_reservation_review_tab within tab_1
end type
type tab_1 from tab within w_reservation_review_sales
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_reservation_review_sales from w_netwise_sheet
integer width = 2917
integer height = 1512
string title = "Reservation Review"
long backcolor = 12632256
event ue_complete_order ( )
event ue_cascadeplant ( )
event type string ue_getdata ( double word_parm,  string string_parm )
cb_next cb_next
cb_prev cb_prev
st_endofqueue st_endofqueue
st_noqueue st_noqueue
tab_1 tab_1
end type
global w_reservation_review_sales w_reservation_review_sales

type variables
DataStore	ids_res_list
DataStore	ids_customer
DataStore	ids_salespeople
String		is_inquireData
u_orp001		iu_orp001
u_orp002		iu_orp002

u_ws_orp3		iu_ws_orp3

s_error		istr_error_info

u_reservation_review_tab	iu_tabs[]

Integer		ii_start_res

end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_loadsalespeople ()
public subroutine wf_select_tab (integer ai_tab)
public function boolean wf_update ()
public subroutine wf_remove_reservation ()
public subroutine wf_show_reservations (integer ai_start_res)
end prototypes

event ue_complete_order;call super::ue_complete_order;Long	ll_Last_Selected_tab
ll_Last_Selected_tab = tab_1.SelectedTab
If IsValid(iu_tabs[ ll_Last_Selected_tab]) Then 
	iu_tabs[ ll_Last_Selected_tab].uf_completeorder()
End If
IF ids_res_list.RowCount() = 0 Then 
	MessageBox( "Queue Empty","All Reservations have been completed.")
	st_noqueue.Visible = FALSE
	st_endofqueue.Visible = True
	This.SetRedraw( True)
End If
end event

event ue_cascadeplant;call super::ue_cascadeplant;iu_tabs[tab_1.SelectedTab].uf_cascadeplants()
end event

event ue_getdata;call super::ue_getdata;String	ls_Return

Choose Case string_Parm
	Case 'inquire'
		ls_Return = This.Tab_1.Control[Tab_1.SelectedTab].Text
	Case Else
		ls_Return = ''
End Choose
Return ls_Return
end event

public function boolean wf_retrieve ();Long	ll_result, &
		ll_row_count

String	ls_startres, &
			ls_detail_info, &
			ls_option, &
			ls_customers, &
			ls_division, &
			ls_inquire_data, &
			ls_gpo_date, ls_detail_error, ls_fields_in_error, ls_fieldserror
			

Integer	li_counter

ll_result = w_base_sheet::EVENT CloseQuery( )
If ll_result > 0 then return false

OpenWithParm(w_reservation_review_inq, This)
is_InquireData = Message.StringParm

If Lower(Left(is_InquireData,6)) = "cancel" Then
	return False
End if

SetPointer(HourGlass!)
istr_error_info.se_event_name = 'wf_retrieve'

// is_InquireData is:
// reservation number + gpo_date + "M" + division
ls_inquire_data = is_InquireData
ls_StartRes = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')
ls_gpo_date = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')
ls_option = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')		// Should be "M"
ls_division = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')
If Not iw_frame.iu_string.nf_IsEmpty(ls_StartRes) Then
	// If they entered a reservation, only inquire on that
	ls_detail_info = ls_StartRes 
Else
	ls_detail_info = ''
End If
ls_detail_info += '~t~t~t~t'
ls_detail_info += '~t' + ls_division + '~t' + String(Date(ls_gpo_date),"mm/dd/yyyy")

//iu_orp001.nf_orpo62ar(istr_error_info, &
//								ls_detail_info, &
//								ls_option)
								
iu_ws_orp3.uf_orpo62fr(istr_error_info, &
								ls_detail_info, &
								ls_option)								

ids_res_list.Reset()
ll_row_count = ids_res_list.ImportString(ls_detail_info)

//
if ids_res_list.RowCount() > 0 then 
	for li_Counter = 1 to ids_res_list.RowCount()
		ls_detail_error = Mid(ids_res_list.GetItemString(li_Counter, 'detail_errors'), 10, 1)
		if ls_detail_error = 'V'  then
			ls_fields_in_error = ids_res_list.GetItemString(1, 'fields_in_error')
			ls_fieldserror = Replace(ls_fields_in_error, 8, 1, 'V')
			ids_res_list.SetItem(1, 'fields_in_error', ls_fieldserror)
		end if 
	next
end if 
//


// Build a string to import to ids_customer.
ls_Customers = ''
For li_Counter = 1  to ids_res_list.RowCount()
	ls_Customers += ids_res_list.GetItemString(li_Counter, 'customer_id') + '~t' + &
						ids_res_list.GetItemString(li_Counter, 'customer_name') + '~t' + &
						ids_res_list.GetItemString(li_Counter, 'city') + '~t' + &
						ids_res_list.GetItemString(li_Counter, 'state') + '~r~n'
Next
	
ids_res_list.SetRow(1)

ids_customer.Reset()
ids_customer.ImportString(ls_Customers)


For li_counter = 1 to UpperBound(iu_tabs)
	iu_tabs[li_counter].Visible = False
Next
wf_show_reservations(1)

//For li_counter = 1 to li_max_num
//	Choose Case True
//		Case li_counter > UpperBound(iu_tabs)
//			If tab_1.OpenTab(iu_tabs[li_counter],0) > 0 Then
//				ll_Number_of_Tabs = UpperBound( tab_1.Control)
//				tab_1.Control[ll_Number_of_Tabs+1] = iu_tabs[li_counter]
//			END IF	
//		Case Not IsValid(iu_tabs[li_counter])
//			If tab_1.OpenTab(iu_tabs[li_counter],0) > 0 Then
//				ll_Number_of_Tabs = UpperBound( tab_1.Control)
//				tab_1.Control[ll_Number_of_Tabs+1] = iu_tabs[li_counter]
//			END IF	
//		Case Else
//			iu_tabs[li_counter].ib_retrieved = FALSE
//	End Choose
//	iu_tabs[li_counter].Text = ids_res_list.GetItemString(li_counter,"order_id")
//	ls_tab_title = Trim(ids_res_list.GetItemString(li_counter,"customer_name"))
//	If Not iw_frame.iu_string.nf_IsEmpty(ls_tab_title) Then
//		iu_tabs[li_counter].PowerTipText = ls_tab_title
//	Else
//		iu_tabs[li_counter].PowerTipText = Trim(ids_res_list.GetItemString(li_counter,"customer_id"))
//	End If
//	iu_tabs[li_counter].is_order_status = ids_res_list.GetItemString(li_counter,"order_status")
//Next
//
//For li_counter = UpperBound(iu_tabs) To ids_res_list.RowCount() + 1 Step -1
//	If IsValid(iu_tabs[li_counter]) Then 
//		tab_1.CloseTab(iu_tabs[li_counter])
//	End If
//Next

This.SetRedraw(True)
If ids_res_list.RowCount() > 0 Then 
	This.POST wf_select_tab(1)
	st_noqueue.Visible = False
	st_endofqueue.Visible = False
Else
	st_noqueue.Visible = True	
	st_endofqueue.Visible = False
End if

return True

end function

public subroutine wf_loadsalespeople ();Integer	li_RecCount 
String	ls_salespeople, ls_smancode, ls_sman_type
u_utl001	lu_utl001

iw_frame.SetMicroHelp( "Loading Sales People")

IF Date(ProfileString(iw_frame.is_UserIni, "Reservation Review", "Salesperson Date", &
		String( RelativeDate( Today(), -1)))) < Today() OR &
		NOT(FILEEXISTS(iw_Frame.is_workingdir+"salespep.dbf"))	THEN

	lu_utl001 = Create u_utl001
	
	Do 
   	ls_salespeople = Space(19601)
		
		istr_error_info.se_event_name = 'uf_loadSalesPeople'
//stan meng 9/3 parmaters changed on utl001
//		lu_utl001.nf_utlu05ar( istr_Error_Info, ls_salespeople, li_RecCount)
      lu_utl001.nf_utlu05ar( istr_Error_Info, ls_salespeople, li_RecCount, ls_smancode, ls_sman_type)
		
		If ids_salespeople.ImportString(TRIM(ls_salespeople)) < 1 Then
			MessageBox("Sales People", "Problem populating Sales People Drop Down")
		Return
	End If
	Loop While li_RecCount = 350
	
	Destroy lu_utl001

	IF ids_salespeople.RowCount() > 0 THEN
		ids_salespeople.Saveas(iw_frame.is_workingDir+"SALESPEP.dbf",DBASE3!,TRUE)
		SetProfileString(iw_frame.is_UserIni, "Reservation Review", "Salesperson Date", &
								String(Today()))
	END IF

ELSE
   ids_salespeople.ImportFile( iw_frame.is_workingdir+"salespep.dbf")
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end subroutine

public subroutine wf_select_tab (integer ai_tab);If ai_tab = tab_1.SelectedTab Then
	tab_1.SelectTab(ai_tab)
	iu_tabs[ai_tab].uf_selected()
Else
	tab_1.SelectTab(ai_tab)
End If
end subroutine

public function boolean wf_update ();If IsValid(iu_tabs[tab_1.SelectedTab]) Then return iu_tabs[tab_1.SelectedTab].uf_update()
end function

public subroutine wf_remove_reservation ();Integer	li_selected, &
			li_counter, &
			li_upperbound

//Remove the accepted order from the queue
li_selected = tab_1.SelectedTab
li_upperbound = UpperBound(iu_tabs)
ids_res_list.DeleteRow(li_selected + ii_start_res - 1)
If ids_res_list.RowCount() = 0 Then
	iu_tabs[li_selected].Visible = False
	st_noqueue.Visible = True
	st_endofqueue.Visible = False
	return
End If
wf_show_reservations(ii_start_res)

//Now select the next visible tab
Boolean lb_done = FALSE
li_counter = li_selected
Do While li_counter <= li_upperbound And Not lb_done
	If iu_tabs[li_counter].Visible Then
		iu_tabs[li_counter].ib_retrieved = FALSE
		tab_1.SelectTab(li_counter)
		iu_tabs[li_counter].uf_selected()
		lb_done = TRUE
	End If
	li_counter ++
Loop
li_counter = 1
Do While li_counter < li_selected And Not lb_done
	If iu_tabs[li_counter].Visible Then
		iu_tabs[li_counter].ib_retrieved = FALSE
		tab_1.SelectTab(li_counter)
		iu_tabs[li_counter].uf_selected()
		lb_done = TRUE
	End If
	li_counter ++
Loop
If Not lb_done Then
	st_endofqueue.Visible = True	
	st_noqueue.Visible = False
	return	
End If

st_endofqueue.Visible = False
st_noqueue.Visible = False
return



end subroutine

public subroutine wf_show_reservations (integer ai_start_res);Integer	li_counter
String	ls_tab_title
If ai_start_res + UpperBound(iu_tabs) > ids_res_list.RowCount() Then
	ai_start_res = (ids_res_list.RowCount() - UpperBound(iu_tabs)) + 1
End If
If ai_start_res < 1 Then ai_start_res = 1
ii_start_res = ai_start_res
For li_counter = 1 to UpperBound(iu_tabs)
	iu_tabs[li_counter].Visible = FALSE
Next
For li_counter = ai_start_res to ids_res_list.RowCount()
	If li_counter - ai_start_res + 1 <= UpperBound(iu_tabs) Then
		iu_tabs[li_counter - ai_start_res + 1].Visible = TRUE	
		iu_tabs[li_counter - ai_start_res + 1].ib_retrieved = FALSE
		iu_tabs[li_counter - ai_start_res + 1].Text = ids_res_list.GetItemString(li_counter,"order_id")
		iu_tabs[li_counter - ai_start_res + 1].id_gpo_date = ids_res_list.GetItemDate(li_counter,"gpo_date")
		ls_tab_title = Trim(ids_res_list.GetItemString(li_counter,"customer_name"))
		If Not iw_frame.iu_string.nf_IsEmpty(ls_tab_title) Then
			iu_tabs[li_counter - ai_start_res + 1].PowerTipText = ls_tab_title
		Else
			iu_tabs[li_counter - ai_start_res + 1].PowerTipText = Trim(ids_res_list.GetItemString(li_counter,"customer_id"))
		End If
		iu_tabs[li_counter - ai_start_res + 1].is_order_status = ids_res_list.GetItemString(li_counter,"order_status")
	End If
Next

If ai_start_res + UpperBound(iu_tabs) < ids_res_list.RowCount() Then
	cb_next.Visible = True
Else
	cb_next.Visible = False
End If
If ai_start_res > 1 Then
	cb_prev.Visible = True
Else
	cb_prev.Visible = False
End If
end subroutine

event ue_postopen;call super::ue_postopen;String	ls_system_id

ids_res_list  = Create DataStore
ids_res_list.DataObject = "d_reservation_list"
ids_customer = Create DataStore
ids_customer.DataObject = "d_customer_data"
ids_salespeople = Create DataStore
ids_salespeople.DataObject = "d_sales_people"

//This.wf_loadsalespeople()

iu_orp001 = Create u_orp001
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_orp002 = Create u_orp002
If Message.ReturnValue = -1 Then
	Close(This)
	Return
end if

iu_ws_orp3 = Create u_ws_orp3
If Message.ReturnValue = -1 Then
	Close(This)
	Return
end if

ls_system_id = Trim(Message.nf_Get_App_ID())

istr_error_info.se_app_name = ls_system_id 
istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_window_name = "Res Revu"

Integer li_counter
For li_counter = 1 to UpperBound(tab_1.Control)
	Choose Case li_counter
		Case 1
			iu_tabs[li_counter] = tab_1.tabpage_1
		Case 2
			iu_tabs[li_counter] = tab_1.tabpage_2
		Case 3
			iu_tabs[li_counter] = tab_1.tabpage_3
		Case 4
			iu_tabs[li_counter] = tab_1.tabpage_4
		Case 5
			iu_tabs[li_counter] = tab_1.tabpage_5
		Case Else
	End Choose			
Next

This.wf_retrieve()
end event

on w_reservation_review_sales.create
int iCurrent
call super::create
this.cb_next=create cb_next
this.cb_prev=create cb_prev
this.st_endofqueue=create st_endofqueue
this.st_noqueue=create st_noqueue
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_next
this.Control[iCurrent+2]=this.cb_prev
this.Control[iCurrent+3]=this.st_endofqueue
this.Control[iCurrent+4]=this.st_noqueue
this.Control[iCurrent+5]=this.tab_1
end on

on w_reservation_review_sales.destroy
call super::destroy
destroy(this.cb_next)
destroy(this.cb_prev)
destroy(this.st_endofqueue)
destroy(this.st_noqueue)
destroy(this.tab_1)
end on

event close;call super::close;If IsValid(ids_customer) Then Destroy ids_customer
If IsValid(ids_res_list) Then Destroy ids_res_list

If IsValid(iu_orp001) Then
	Destroy iu_orp001
End If

If IsValid(iu_orp002) Then
	Destroy iu_orp002
End If
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')  
iw_frame.im_menu.mf_Enable('m_previous')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_completeorder')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_generatesales')

end event

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')  
iw_frame.im_menu.mf_Disable('m_previous')
iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_completeorder')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_generatesales')
end event

event closequery;Integer	li_counter

For li_counter = 1 to UpperBound(iu_tabs)
	If IsValid(iu_tabs[li_counter]) Then
		If iu_tabs[li_counter].ib_modified Then
			tab_1.SelectTab(li_counter)
			Choose Case MessageBox(This.Title, &
								"Do you want to save changes?", Question!, YesNoCancel!)
				Case 1
					If Not iu_tabs[li_counter].uf_update() Then Return 1
					iu_tabs[li_counter].ib_modified = False
				Case 2
					iu_tabs[li_counter].ib_modified = False
				Case 3
					Return 1
			End Choose
		End If
	End If
Next

Return 0
end event

type cb_next from commandbutton within w_reservation_review_sales
boolean visible = false
integer x = 2629
integer y = 12
integer width = 247
integer height = 68
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Next"
end type

event clicked;wf_show_reservations(ii_start_res + UpperBound(iu_tabs))
iu_tabs[tab_1.SelectedTab].ib_retrieved = False
Parent.Post wf_select_tab(tab_1.SelectedTab)

end event

type cb_prev from commandbutton within w_reservation_review_sales
boolean visible = false
integer x = 2377
integer y = 12
integer width = 247
integer height = 68
integer taborder = 3
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Previous"
end type

event clicked;wf_show_reservations(ii_start_res - UpperBound(iu_tabs))
iu_tabs[tab_1.SelectedTab].ib_retrieved = False
Parent.Post wf_select_tab(tab_1.SelectedTab)

end event

type st_endofqueue from statictext within w_reservation_review_sales
boolean visible = false
integer x = 233
integer y = 420
integer width = 2405
integer height = 508
integer textsize = -24
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "You have reached the end of the queue. Please  inquire to check the queue."
alignment alignment = center!
boolean focusrectangle = false
end type

type st_noqueue from statictext within w_reservation_review_sales
boolean visible = false
integer x = 466
integer y = 536
integer width = 1979
integer height = 164
integer textsize = -24
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "No reservations in the queue."
boolean focusrectangle = false
end type

type tab_1 from tab within w_reservation_review_sales
integer x = 18
integer width = 2862
integer height = 1404
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
boolean powertips = true
boolean boldselectedtext = true
boolean createondemand = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

event rightclicked;If index <= UpperBound(iu_tabs) And index > 0 Then
	If IsValid(iu_tabs[index]) Then
		iu_tabs[index].ib_retrieved = False
		This.SelectTab(index)
		iu_tabs[index].uf_selected()
	End If
End If
end event

event selectionchanged;If newindex <= UpperBound(iu_tabs) Then
	If IsValid(iu_tabs[newindex]) Then iu_tabs[newindex].post uf_selected()
End If
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

type tabpage_1 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2825
integer height = 1372
long tabtextcolor = 0
long picturemaskcolor = 0
end type

type tabpage_2 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2825
integer height = 1372
long tabtextcolor = 0
long picturemaskcolor = 0
end type

type tabpage_3 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2825
integer height = 1372
long tabtextcolor = 0
long picturemaskcolor = 0
end type

type tabpage_4 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2825
integer height = 1372
long tabtextcolor = 0
long picturemaskcolor = 0
end type

type tabpage_5 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2825
integer height = 1372
long tabtextcolor = 0
long picturemaskcolor = 0
end type

