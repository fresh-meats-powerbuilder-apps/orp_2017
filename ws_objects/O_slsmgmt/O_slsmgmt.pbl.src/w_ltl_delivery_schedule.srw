﻿$PBExportHeader$w_ltl_delivery_schedule.srw
forward
global type w_ltl_delivery_schedule from w_base_sheet_ext
end type
type dw_delivery_schedule from u_base_dw_ext within w_ltl_delivery_schedule
end type
end forward

global type w_ltl_delivery_schedule from w_base_sheet_ext
integer width = 2917
integer height = 1032
string title = "LTL Delivery Schedule"
dw_delivery_schedule dw_delivery_schedule
end type
global w_ltl_delivery_schedule w_ltl_delivery_schedule

type variables
u_orp001		iu_orp001

u_ws_orp4       iu_ws_orp4

s_error	istr_error_info
end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_update ();String			ls_input_string, &
					ls_output_string
					
Long				ll_row

Integer			li_rtn

If dw_delivery_schedule.AcceptText() < 1 Then Return False

ll_row = dw_delivery_schedule.GetNextModified(0, primary!)
If ll_row = 0 Then
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End If

ls_input_string = 'U' + '~t'
ls_output_string = '' 


Do 
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'state_abbrev') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'state_desc') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'sunday_ind') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'monday_ind') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'tuesday_ind') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'wednesday_ind') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'thursday_ind') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'friday_ind') + '~t'
	ls_input_string += dw_delivery_schedule.GetItemString( &
	   ll_row, 'saturday_ind') + '~r~n'
		
		
	ll_row = dw_delivery_schedule.GetNextModified(ll_row, primary!)
	
Loop While ll_row > 0

//li_rtn = iu_orp001.nf_orpo05cr_inq_upd_ltl_sched(istr_error_info, &
//																		ls_input_string, &
//																		ls_output_string)
									
li_rtn = iu_ws_orp4.nf_orpo05gr(istr_error_info, &
												ls_input_string, &
												ls_output_string)


iw_frame.SetMicroHelp('Modification Successful')
dw_delivery_schedule.ResetUpdate()

Return True
end function

public function boolean wf_retrieve ();String	ls_input_string, &
			ls_output_string
		
Integer	li_rtn		

u_string_functions	lu_string_functions 
			
This.TriggerEvent('closequery')

istr_error_info.se_event_name = "wf_retrieve"			  

ls_input_string = 'I'

//li_rtn = iu_orp001.nf_orpo05cr_inq_upd_ltl_sched(istr_error_info, &
//																		ls_input_string, &
//																		ls_output_string)
									
li_rtn = iu_ws_orp4.nf_orpo05gr(istr_error_info, &
												ls_input_string, &
												ls_output_string)


dw_delivery_schedule.Reset()
dw_delivery_schedule.Importstring(ls_output_string)
dw_delivery_schedule.ResetUpdate()

Return True
end function

on w_ltl_delivery_schedule.create
int iCurrent
call super::create
this.dw_delivery_schedule=create dw_delivery_schedule
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_delivery_schedule
end on

on w_ltl_delivery_schedule.destroy
call super::destroy
destroy(this.dw_delivery_schedule)
end on

event ue_postopen;call super::ue_postopen;iu_orp001 = CREATE u_orp001

iu_ws_orp4 = CREATE u_ws_orp4

wf_retrieve()



end event

event close;call super::close;If IsValid( iu_Orp001) Then Destroy( iu_orp001)

If IsValid( iu_ws_orp4) Then Destroy( iu_ws_orp4)



end event

event resize;call super::resize;//dw_delivery_schedule.Resize(this.width - dw_delivery_schedule.x - 60, this.height - dw_delivery_schedule.y - 150)

long       ll_x = 60,  ll_y = 150

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_delivery_schedule.Resize(this.width - dw_delivery_schedule.x - ll_x, this.height - dw_delivery_schedule.y - ll_y)



end event

type dw_delivery_schedule from u_base_dw_ext within w_ltl_delivery_schedule
integer x = 14
integer y = 12
integer width = 2816
integer taborder = 10
string dataobject = "d_ltl_delivery_schedule"
boolean hscrollbar = true
boolean vscrollbar = true
end type

