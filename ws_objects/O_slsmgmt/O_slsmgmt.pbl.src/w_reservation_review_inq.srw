﻿$PBExportHeader$w_reservation_review_inq.srw
$PBExportComments$Reservation review Inquire window
forward
global type w_reservation_review_inq from w_netwise_response
end type
type dw_inquire from u_netwise_dw within w_reservation_review_inq
end type
type dw_division from u_division within w_reservation_review_inq
end type
end forward

global type w_reservation_review_inq from w_netwise_response
int Width=1691
int Height=552
long BackColor=12632256
dw_inquire dw_inquire
dw_division dw_division
end type
global w_reservation_review_inq w_reservation_review_inq

type variables
Boolean		ib_ValidToClose

Window		iw_Parent
end variables

event open;call super::open;String ls_temp,ls_parent
u_string_functions lu_string

iw_Parent = Message.PowerObjectParm
If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if
If iw_parent.title = "Reservation Movement" Then
	dw_division.Hide()
Else
	dw_division.Show()
End If
 
This.Title = iw_Parent.title + " Inquire"
end event

on close;call w_netwise_response::close;If Not ib_ValidToClose Then
	Message.StringParm = ''
End if
end on

event ue_postopen;call super::ue_postopen;String		ls_data

w_reservation_review_sales lw_parent
lw_parent = iw_parent

ls_data = lw_parent.is_InquireData
If iw_frame.iu_string.nf_IsEmpty(ls_data) Then return

// ls_data is:
// starting reservation + "~t" + gpo date + "~t" + "S" + "~t" + division
Long ll_pos
ll_pos = iw_frame.iu_string.nf_npos(ls_data,"~t",1,3)

dw_division.uf_set_division(Mid(ls_data,ll_pos + 1))
dw_inquire.Reset()
dw_inquire.ImportString(Left(ls_data,ll_pos - 1))

Boolean lb_temp
lb_temp =  iw_frame.iu_string.nf_IsEmpty(Left(ls_data,Pos(ls_data,'~t')-1))
If lb_temp Then
	dw_inquire.Modify("gpo_date.Protect = '1' gpo_date.Background.Color = '12632256'")
Else
	dw_inquire.Modify("gpo_date.Protect = '0' gpo_date.Background.Color = '16777215'")
End If
dw_inquire.SetFocus()
end event

on w_reservation_review_inq.create
int iCurrent
call super::create
this.dw_inquire=create dw_inquire
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_inquire
this.Control[iCurrent+2]=this.dw_division
end on

on w_reservation_review_inq.destroy
call super::destroy
destroy(this.dw_inquire)
destroy(this.dw_division)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;If dw_inquire.AcceptText() = -1 Then return 

If dw_division.AcceptText() = -1 Then return

If Not iw_frame.iu_string.nf_isempty(dw_inquire.GetItemString(1,"reference_number")) and &
	iw_frame.iu_string.nf_isempty(String(dw_inquire.GetItemDate(1,"gpo_date"))) Then
	iw_frame.SetMicroHelp("GPO Date is required when reservation number is specified")
	dw_inquire.SetColumn("gpo_date")
	dw_inquire.SelectText(1,10000)
	dw_inquire.SetFocus()
	return
End If

ib_ValidToClose = True
CloseWithReturn(This, dw_inquire.Describe("DataWindow.Data") + "~t" + dw_division.uf_get_division())
end event

type cb_base_help from w_netwise_response`cb_base_help within w_reservation_review_inq
int X=1330
int Y=332
int TabOrder=50
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_reservation_review_inq
int X=1024
int Y=332
int TabOrder=40
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_reservation_review_inq
int X=718
int Y=332
int TabOrder=30
end type

type dw_inquire from u_netwise_dw within w_reservation_review_inq
int X=27
int Y=16
int Width=1376
int Height=184
int TabOrder=10
boolean BringToTop=true
string DataObject="d_reservation_review_inq"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event itemchanged;call super::itemchanged;String	ls_res


If This.GetColumnName() = 'reference_number' Then
	ls_res = Fill('0', 7 - Len(Trim(This.GetText()))) + This.GetText()
	This.SetText(ls_res)
End if

Boolean lb_temp
If dwo.Name = "reference_number" Then
	lb_temp =  iw_frame.iu_string.nf_IsEmpty(data)
	If lb_temp Then
		This.Modify("gpo_date.Protect = '1' gpo_date.Background.Color = '12632256'")
		Date	ld_null
		SetNull(ld_null)
		This.SetItem(1,"gpo_date",ld_null)
	Else
		This.Modify("gpo_date.Protect = '0' gpo_date.Background.Color = '16777215'")
	End If
End If
end event

on constructor;call u_netwise_dw::constructor;This.InsertRow(0)
end on

event itemfocuschanged;call super::itemfocuschanged;Choose Case This.GetColumnName()
Case 'reference_number'
	SetMicroHelp(This.Object.reference_number_t.Text + " is an optional field")
End Choose

This.SelectText(1, Len(This.GetText()))
end event

event getfocus;call super::getfocus;Choose Case This.GetColumnName()
Case 'reference_number'
	SetMicroHelp(This.Object.reference_number_t.Text + " is an optional field")
End Choose

This.SelectText(1, Len(This.GetText()))

end event

event clicked;call super::clicked;This.AcceptText()

end event

event losefocus;This.AcceptText()

end event

type dw_division from u_division within w_reservation_review_inq
int X=27
int Y=224
int TabOrder=20
end type

event constructor;call super::constructor;ib_required = False
This.Reset()
This.SetItem(This.InsertRow(0),"division_code","  ")
end event

