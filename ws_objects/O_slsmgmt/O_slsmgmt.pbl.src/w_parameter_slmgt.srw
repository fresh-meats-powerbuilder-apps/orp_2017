﻿$PBExportHeader$w_parameter_slmgt.srw
forward
global type w_parameter_slmgt from w_base_sheet_ext
end type
type dw_main from u_base_dw_ext within w_parameter_slmgt
end type
end forward

global type w_parameter_slmgt from w_base_sheet_ext
integer width = 937
integer height = 1132
string title = "Parameters"
event ue_retrieve pbm_custom01
event ue_update pbm_custom02
dw_main dw_main
end type
global w_parameter_slmgt w_parameter_slmgt

type variables
u_orp001			iu_orp001
s_error			istr_error_info
w_parameters		iw_parameters
u_ws_orp3			iu_ws_orp3

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

event ue_retrieve;INTEGER	li_Rtn

STRING	ls_parameter_info

//li_Rtn = iu_orp001.nf_orpo52ar(istr_error_info, "I", ls_parameter_info)
li_Rtn = iu_ws_orp3.uf_orpo52fr(istr_error_info, "I", ls_parameter_info)

CHOOSE CASE li_Rtn
CASE 0
	dw_main.Reset()
	dw_main.ImportString(ls_parameter_info)
END CHOOSE

end event

event ue_update;INTEGER	li_Rtn
Long		ll_min, ll_max
STRING	ls_parameter_info

li_Rtn = dw_main.AcceptText()
CHOOSE CASE li_Rtn
CASE 1
CASE ELSE
	RETURN
END CHOOSE

ll_min = dw_main.GetItemNumber(1, 'shtg_mindays')
ll_max = dw_main.GetItemNumber(1, 'shtg_maxdays')
If ll_min > ll_max Then
	MessageBox("Input Error","Min days can not be greater than Max days")
	iw_Frame.SetMicroHelp("No Update Was Made")
	Return
End If

ls_parameter_info = String(dw_main.GetItemNumber(1, 'so_gen_fill_percentage'), '000') + '~t'
ls_parameter_info += String(dw_main.GetItemNumber(1, 'shortage_queue_min_boxes'), '00') + '~t'
ls_parameter_info += String(dw_main.GetItemNumber(1, 'shtg_mindays'), '000') + '~t'
ls_parameter_info += String(dw_main.GetItemNumber(1, 'shtg_maxdays'), '000') + '~t'
ls_parameter_info += String(dw_main.GetItemNumber(1, 'shtg_hours'), '000') + '~t'
ls_parameter_info += String(dw_main.GetItemNumber(1, 'days_for_release'), '00') + '~t'
ls_parameter_info += String(dw_main.GetItemNumber(1, 'peelable_days_for_release'), '00') + '~t'

//ls_parameter_info = dw_main.Describe("DataWindow.Data")

//li_Rtn = iu_orp001.nf_orpo52ar(istr_error_info, "U", ls_parameter_info)
li_Rtn = iu_ws_orp3.uf_orpo52fr(istr_error_info, "U", ls_parameter_info)



end event

public function boolean wf_retrieve ();This.TriggerEvent("ue_retrieve")
RETURN TRUE
end function

public function boolean wf_update ();This.TriggerEvent("ue_update")
RETURN TRUE
end function

event ue_postopen;call super::ue_postopen;iu_orp001 = CREATE u_orp001
iu_ws_orp3 = CREATE u_ws_orp3

TriggerEvent("ue_retrieve")


end event

on w_parameter_slmgt.create
int iCurrent
call super::create
this.dw_main=create dw_main
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
end on

on w_parameter_slmgt.destroy
call super::destroy
destroy(this.dw_main)
end on

event close;call super::close;Destroy (iu_orp001)
Destroy (iu_ws_orp3)

end event

type dw_main from u_base_dw_ext within w_parameter_slmgt
integer x = 14
integer y = 44
integer width = 855
integer height = 980
string dataobject = "d_parameter_slmgt"
boolean border = false
end type

