﻿$PBExportHeader$w_edi_prod_detail_inq.srw
forward
global type w_edi_prod_detail_inq from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_edi_prod_detail_inq
end type
type dw_edi_prod_detail_inq from u_base_dw_ext within w_edi_prod_detail_inq
end type
end forward

global type w_edi_prod_detail_inq from w_base_response
integer width = 2432
integer height = 444
string title = "Customer Product Cross Reference Detail Inquire"
boolean controlmenu = false
long backcolor = 12632256
cb_browse cb_browse
dw_edi_prod_detail_inq dw_edi_prod_detail_inq
end type
global w_edi_prod_detail_inq w_edi_prod_detail_inq

type variables
w_base_sheet_ext		iw_parentwindow
w_base_sheet	iw_parent
String			is_openstring
Boolean	ib_valid_return
DataWindowChild	idwc_customer

u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_prod_detail_inq.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
this.dw_edi_prod_detail_inq=create dw_edi_prod_detail_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
this.Control[iCurrent+2]=this.dw_edi_prod_detail_inq
end on

on w_edi_prod_detail_inq.destroy
call super::destroy
destroy(this.cb_browse)
destroy(this.dw_edi_prod_detail_inq)
end on

event open;call super::open;datawindowchild			ldwc_temp

								
String						ls_data
								
iw_parentwindow = Message.PowerObjectParm
ls_data = Message.StringParm

If dw_edi_prod_detail_inq.ImportString(ls_data) < 1 Then 
	dw_edi_prod_detail_inq.InsertRow(0)
End If
end event

event ue_base_ok;string 	ls_customer_id, &
			ls_customer_name, &
			ls_value, &
			ls_temp, ls_desc
long  	ll_row
			
Date	ldt_temp

u_string_functions	lu_string_functions

dw_edi_prod_detail_inq.AcceptText()

ls_value = dw_edi_prod_detail_inq.GetItemString(1, 'customer_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('Customer Id is a required field.')
	Return
End If	


ll_row = idwc_customer.find("customer_id = '" +  ls_value + "'", 1, idwc_customer.Rowcount() + 1)
//	
if ll_row > 0 Then
	ls_customer_name = dw_edi_prod_detail_inq.getitemstring(1, 'customer_name')
	SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_value)
else
	ls_customer_name = ' '
end if

//	ls_desc = idwc_customer.getitemstring(ll_row, "customer_name")
//	SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_value)
//else
//		iw_frame.setmicrohelp("Invalid EDI Customer")
//		SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)
//		return 
//end if



	ls_customer_id = dw_edi_prod_detail_inq.getitemstring(1, 'customer_id')
	

	iw_parentwindow.Event ue_set_data('customer_id', ls_customer_id)

//ls_customer_name = dw_edi_prod_detail_inq.getitemstring(1, 'customer_name')

	iw_parentwindow.Event ue_set_data('customer_name', ls_customer_name)
	

ib_valid_return = True

CloseWithReturn(This, "OK")

end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;DataWindowChild	ldwc_customer
Integer				li_rtn
String				ls_output_string

iu_orp001 = CREATE u_orp001
iu_ws_orp1 = CREATE u_ws_orp1


gw_netwise_frame.SetMicroHelp( "Ready")

String				ls_customer_id, &
						ls_customer_name, &						
						ls_start_date, &
						ls_end_date, ls_text, &
						ls_desc
long 					ll_row

u_string_functions	lu_string_functions



istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_orp001.nf_orpo06br_inq_edi_customer(istr_error_info, &
//																ls_output_string)
																
li_rtn = iu_ws_orp1.nf_orpo06fr(istr_error_info, &
											ls_output_string)															
										
										

dw_edi_prod_detail_inq.getchild( "customer_id", idwc_customer)
li_rtn = idwc_customer.importstring(ls_output_string)

ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer","")
if ls_text > ' ' then
	ll_row = idwc_customer.find("customer_id = '" +  ls_text + "'", 1, idwc_customer.Rowcount() + 1)
	dw_edi_prod_detail_inq.setitem(1, "customer_id", ls_text)
	ls_desc = idwc_customer.getitemstring(ll_row, "customer_name")
	dw_edi_prod_detail_inq.setitem(1, "customer_name", ls_desc)
	dw_edi_prod_detail_inq.SetColumn("customer_id")
end if


end event

type cb_base_help from w_base_response`cb_base_help within w_edi_prod_detail_inq
boolean visible = false
integer x = 992
integer y = 256
integer taborder = 40
boolean enabled = false
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_edi_prod_detail_inq
integer x = 530
integer y = 176
integer taborder = 50
end type

type cb_base_ok from w_base_response`cb_base_ok within w_edi_prod_detail_inq
integer x = 183
integer y = 176
integer taborder = 30
end type

type cb_browse from u_base_commandbutton_ext within w_edi_prod_detail_inq
boolean visible = false
integer x = 2126
integer y = 396
integer height = 108
integer taborder = 10
string text = "&Browse"
end type

type dw_edi_prod_detail_inq from u_base_dw_ext within w_edi_prod_detail_inq
integer x = 27
integer y = 16
integer width = 1947
integer height = 128
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_edi_prod_detail_inq"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
String				ls_ColName, &
						ls_customer_id, &
						ls_desc, &
						ls_text
Long					ll_row
u_string_functions		lu_string



ls_ColName = GetColumnName()
If ls_ColName = "customer_id" Then
	ls_customer_id = data
	ll_row = idwc_customer.find("customer_id = '" +  ls_customer_id + "'", 1, idwc_customer.Rowcount() + 1)

	if ll_row > 0 Then
		ls_desc = idwc_customer.getitemstring(ll_row, "customer_name")
		SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)
		dw_edi_prod_detail_inq.setitem(1, "customer_name", ls_desc)
	end if
end if




end event

event constructor;call super::constructor;string 			ls_text

ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer","")



end event

event itemerror;call super::itemerror;Return 1
end event

