﻿$PBExportHeader$w_edi_excp_detail_inq.srw
forward
global type w_edi_excp_detail_inq from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_edi_excp_detail_inq
end type
type dw_edi_prod_detail_inq from u_base_dw_ext within w_edi_excp_detail_inq
end type
type dw_edi_excp_detail_inq from datawindow within w_edi_excp_detail_inq
end type
end forward

global type w_edi_excp_detail_inq from w_base_response
integer width = 1655
integer height = 792
string title = "Customer Product Cross Reference Exception Inquire"
boolean controlmenu = false
long backcolor = 12632256
cb_browse cb_browse
dw_edi_prod_detail_inq dw_edi_prod_detail_inq
dw_edi_excp_detail_inq dw_edi_excp_detail_inq
end type
global w_edi_excp_detail_inq w_edi_excp_detail_inq

type variables
w_base_sheet_ext		iw_parentwindow
w_base_sheet	iw_parent
String			is_openstring
Boolean	ib_valid_return
DataWindowChild	idwc_customer

u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_excp_detail_inq.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
this.dw_edi_prod_detail_inq=create dw_edi_prod_detail_inq
this.dw_edi_excp_detail_inq=create dw_edi_excp_detail_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
this.Control[iCurrent+2]=this.dw_edi_prod_detail_inq
this.Control[iCurrent+3]=this.dw_edi_excp_detail_inq
end on

on w_edi_excp_detail_inq.destroy
call super::destroy
destroy(this.cb_browse)
destroy(this.dw_edi_prod_detail_inq)
destroy(this.dw_edi_excp_detail_inq)
end on

event open;call super::open;datawindowchild			ldwc_temp

								
String						ls_data
								
iw_parentwindow = Message.PowerObjectParm
ls_data = Message.StringParm


If dw_edi_excp_detail_inq.RowCount() = 0 Then
	dw_edi_excp_detail_inq.InsertRow(0)
End if


If dw_edi_prod_detail_inq.RowCount() = 0 Then
	dw_edi_prod_detail_inq.InsertRow(0)
End if

end event

event ue_base_ok;string 	ls_customer_id, &
			ls_excp_customer_id, &
			ls_customer_name, &
			ls_excp_customer_name, &
			ls_value, &
			ls_temp, &
			ls_choice, ls_desc
long 		ll_row
			
Date	ldt_temp

u_string_functions	lu_string_functions

dw_edi_prod_detail_inq.AcceptText()
dw_edi_excp_detail_inq.AcceptText()

ls_choice = dw_edi_excp_detail_inq.GetItemString(1,'inq_on_excp')

ls_value = dw_edi_prod_detail_inq.GetItemString(1, 'customer_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('EDI Customer Id is a required field.')
	Return
End If	


ll_row = idwc_customer.find("customer_id = '" +  ls_value + "'", 1, idwc_customer.Rowcount() + 1)
	


if ll_row > 0 Then
	ls_customer_name = idwc_customer.getitemstring(ll_row, "customer_name")
	SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_value)
else
	ls_customer_name = ' '
end if


ls_customer_id = dw_edi_prod_detail_inq.getitemstring(1, 'customer_id')

iw_parentwindow.Event ue_set_data('customer_id', ls_customer_id)

//ls_customer_name = dw_edi_prod_detail_inq.getitemstring(1, 'customer_name')

iw_parentwindow.Event ue_set_data('customer_name', ls_customer_name)


if ls_choice = 'Y' then
	ls_excp_customer_id = dw_edi_excp_detail_inq.GetItemString(1, 'excp_customer_id')
	If lu_string_functions.nf_isempty(ls_excp_customer_id) Then
		iw_frame.SetMicroHelp('Customer Id is a required field.')
	Return
	End If	
	iw_parentwindow.Event ue_set_data('excp_customer_id', ls_excp_customer_id)
else
	iw_parentwindow.Event ue_set_data('excp_customer_id', ' ')
	iw_parentwindow.Event ue_set_data('excp_customer_name', ' ')
end if	

if ls_excp_customer_id > ' '	 then
//this.SetTransObject(SQLCA)
			SELECT customers.short_name
				INTO :ls_excp_customer_name
				FROM customers
				WHERE customers.customer_id = :ls_excp_customer_id
				USING SQLCA ;
			IF isnull(ls_excp_customer_name) THEN
				ls_excp_customer_name = ' '
			END IF 
	iw_parentwindow.Event ue_set_data('excp_customer_name', ls_excp_customer_name)
end if

ib_valid_return = True

CloseWithReturn(This, "OK")

end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")



end event

event ue_postopen;DataWindowChild	ldwc_customer
Integer				li_rtn
String				ls_output_string, &
						ls_choice

iu_orp001 = CREATE u_orp001
iu_ws_orp1 = CREATE u_ws_orp1

String				ls_customer_id, &
						ls_customer_name, &						
						ls_start_date, &
						ls_end_date, ls_text, &
						ls_desc
long					ll_row

u_string_functions	lu_string_functions


istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_orp001.nf_orpo06br_inq_edi_customer(istr_error_info, &
//																ls_output_string)
																
li_rtn = iu_ws_orp1.nf_orpo06fr(istr_error_info, &
											ls_output_string)																
										
dw_edi_prod_detail_inq.getchild( "customer_id", idwc_customer)
li_rtn = idwc_customer.importstring(ls_output_string)	
	
ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer","")
if ls_text > ' ' then
	ll_row = idwc_customer.find("customer_id = '" +  ls_text + "'", 1, idwc_customer.Rowcount() + 1)
	dw_edi_prod_detail_inq.setitem(1, "customer_id", ls_text)
	ls_desc = idwc_customer.getitemstring(ll_row, "customer_name")
	dw_edi_prod_detail_inq.setitem(1, "customer_name", ls_desc)
end if

iw_parentwindow.Event ue_get_data("excp_customer_id")
ls_customer_id = message.stringparm
if ls_customer_id > ' ' or IsNull(ls_customer_id) Then
	dw_edi_excp_detail_inq.setitem(1, "inq_on_excp", 'Y')
	dw_edi_excp_detail_inq.object.excp_customer_id.Visible = true
	dw_edi_excp_detail_inq.object.excp_customer_type.Visible = true
else
	dw_edi_excp_detail_inq.setitem(1, "inq_on_excp", 'N')
	dw_edi_excp_detail_inq.object.excp_customer_id.Visible = false
	dw_edi_excp_detail_inq.object.excp_customer_type.Visible = false
end if
 
dw_edi_prod_detail_inq.SetFocus()
dw_edi_prod_detail_inq.setrow(1)
dw_edi_prod_detail_inq.SetColumn(1)
end event

type cb_base_help from w_base_response`cb_base_help within w_edi_excp_detail_inq
boolean visible = false
integer x = 992
integer y = 256
integer taborder = 50
boolean enabled = false
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_edi_excp_detail_inq
integer x = 1111
integer y = 416
integer taborder = 40
end type

type cb_base_ok from w_base_response`cb_base_ok within w_edi_excp_detail_inq
integer x = 613
integer y = 416
integer taborder = 30
end type

type cb_browse from u_base_commandbutton_ext within w_edi_excp_detail_inq
boolean visible = false
integer x = 2126
integer y = 396
integer height = 108
integer taborder = 10
string text = "&Browse"
end type

type dw_edi_prod_detail_inq from u_base_dw_ext within w_edi_excp_detail_inq
integer x = 553
integer y = 24
integer width = 946
integer height = 116
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_edi_prod_detail_inq"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
String				ls_ColName, &
						ls_customer_id, &
						ls_desc
Long					ll_row
u_string_functions		lu_string


ls_ColName = GetColumnName()

If ls_ColName = "customer_id" Then
	ls_customer_id = data
	ll_row = idwc_customer.find("customer_id = '" +  ls_customer_id + "'", 1, idwc_customer.Rowcount() + 1)

	if ll_row > 0 Then
		ls_desc = idwc_customer.getitemstring(ll_row, "customer_name")
		SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)
		dw_edi_prod_detail_inq.setitem(1, "customer_name", ls_desc)
	end if
end if


end event

event constructor;call super::constructor;string 			ls_text

ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer","")

dw_edi_prod_detail_inq.SetItem(1, "customer_id", ls_text)
idwc_customer.SetItem(1, "customer_id", ls_text)



end event

type dw_edi_excp_detail_inq from datawindow within w_edi_excp_detail_inq
integer x = 55
integer y = 188
integer width = 1445
integer height = 212
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_edi_excp_detail_inq"
boolean border = false
boolean livescroll = true
end type

event itemchanged;string			ls_choice

Choose Case This.GetColumnName()
Case 'inq_on_excp'
	If data = "Y" Then
		this.object.excp_customer_id.Visible = true
		this.object.excp_customer_type.Visible = true
		dw_edi_excp_detail_inq.SetItem(1,'inq_on_excp', data)
	else	
		this.object.excp_customer_id.Visible = false
		this.object.excp_customer_type.Visible = false
		dw_edi_excp_detail_inq.SetItem(1,'inq_on_excp', data)
	End If
End Choose



end event

event constructor;DataWindowChild ldwc_temp

String			ls_choice, ls_excp_customer_id

This.InsertRow(0)

This.GetChild('excp_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)



end event

