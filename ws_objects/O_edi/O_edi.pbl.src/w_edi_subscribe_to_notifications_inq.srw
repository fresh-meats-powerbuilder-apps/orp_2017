﻿$PBExportHeader$w_edi_subscribe_to_notifications_inq.srw
forward
global type w_edi_subscribe_to_notifications_inq from w_base_response_ext
end type
type dw_subscribe_to_notifications_inq from u_base_dw_ext within w_edi_subscribe_to_notifications_inq
end type
end forward

global type w_edi_subscribe_to_notifications_inq from w_base_response_ext
integer width = 2683
integer height = 412
string title = "Subscribe to Notifications Inquiry"
long backcolor = 12632256
dw_subscribe_to_notifications_inq dw_subscribe_to_notifications_inq
end type
global w_edi_subscribe_to_notifications_inq w_edi_subscribe_to_notifications_inq

type variables
w_base_sheet_ext		iw_parentwindow
w_base_sheet	iw_parent
String			is_openstring
Boolean	ib_valid_return
DataWindowChild	idwc_document

//u_orp001		iu_orp001
u_ws_orp1		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_subscribe_to_notifications_inq.create
int iCurrent
call super::create
this.dw_subscribe_to_notifications_inq=create dw_subscribe_to_notifications_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_subscribe_to_notifications_inq
end on

on w_edi_subscribe_to_notifications_inq.destroy
call super::destroy
destroy(this.dw_subscribe_to_notifications_inq)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,'CANCEL')
end event

event ue_base_ok;call super::ue_base_ok;
//string ls_notification

//ls_notification =dw_subscribe_to_notifications_inq.GetItemString(1, "notification_id")
//CloseWithReturn(This,ls_notification)

string 	ls_doc_id, &
			ls_doc_desc, &
			ls_value, &
			ls_temp, ls_desc
long  	ll_row
			
Date	ldt_temp

u_string_functions	lu_string_functions


dw_subscribe_to_notifications_inq.getchild( "notification_id", idwc_document)

dw_subscribe_to_notifications_inq.AcceptText()

ls_value = dw_subscribe_to_notifications_inq.GetItemString(1, 'notification_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('Document Id is a required field.')
	Return
End If	


ll_row = idwc_document.find("edi_doc_id = '" +  ls_value + "'", 1, idwc_document.Rowcount() + 1)

ls_doc_id = dw_subscribe_to_notifications_inq.getitemstring(1, 'notification_id')
	

iw_parentwindow.Event ue_set_data('notification_id', ls_doc_id)

ls_doc_desc = idwc_document.getitemstring(ll_row, 'edi_doc_desc')

iw_parentwindow.Event ue_set_data('doc_desc', ls_doc_desc)
	
ib_valid_return = True

CloseWithReturn(This, "OK")





end event

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_document
Integer				li_rtn
String				ls_output_string, &
						ls_input_string, &
						ls_edi_doc_id


iu_ws_orp1 = Create u_ws_orp1

u_string_functions		lu_string


istr_error_info.se_event_name = "wf_retrieve"	


// format for detail
   // call to RPC needed to populate the dw_notifications_detail
	
ls_input_string =  'H' + '~t' + &
    					'ORP32' + '~r~n'
						 
 				
 li_rtn = iu_ws_orp1.nf_orpo09fr(istr_error_info, &
											ls_input_string, &
											ls_output_string)						
										

dw_subscribe_to_notifications_inq.getchild( "notification_id", ldwc_document)
dw_subscribe_to_notifications_inq.getchild( "document_id", ldwc_document)

li_rtn = ldwc_document.importstring(ls_output_string)

//iw_parentwindow.Event ue_get_data('edi_doc_id')
//ls_edi_doc_id = Message.StringParm
//If Not lu_string.nf_IsEmpty(ls_edi_doc_id) Then 
	//dw_edi_assign_functions_to_cust_inq.SetItem(1, "edi_doc_id", ls_edi_doc_id)
//End If

end event

event open;call super::open;datawindowchild			ldwc_temp

								
String						ls_data
								
iw_parentwindow = Message.PowerObjectParm
ls_data = Message.StringParm

If dw_subscribe_to_notifications_inq.ImportString(ls_data) < 1 Then 
	dw_subscribe_to_notifications_inq.InsertRow(0)
End If
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_edi_subscribe_to_notifications_inq
boolean visible = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_edi_subscribe_to_notifications_inq
integer x = 750
integer y = 188
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_edi_subscribe_to_notifications_inq
integer x = 457
integer y = 188
end type

type cb_browse from w_base_response_ext`cb_browse within w_edi_subscribe_to_notifications_inq
end type

type dw_subscribe_to_notifications_inq from u_base_dw_ext within w_edi_subscribe_to_notifications_inq
integer y = 32
integer width = 2523
integer height = 96
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_edi_subscribe_to_notifications_inq"
boolean border = false
end type

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
String				ls_ColName, &
						ls_doc_id, &
						ls_doc_desc, &
						ls_text
Long					ll_row
u_string_functions		lu_string



ls_ColName = GetColumnName()
If ls_ColName = "notification_id" Then
	ls_doc_id = data
	ll_row = idwc_document.find("notification_id = '" +  ls_doc_id + "'", 1, idwc_document.Rowcount() + 1)

	if ll_row > 0 Then
		ls_doc_desc = idwc_document.getitemstring(ll_row, "document_id")
//		SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)
		dw_subscribe_to_notifications_inq.setitem(1, "document_id", ls_doc_desc)
	end if
end if

end event

