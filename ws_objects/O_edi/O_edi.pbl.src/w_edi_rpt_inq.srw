﻿$PBExportHeader$w_edi_rpt_inq.srw
forward
global type w_edi_rpt_inq from w_base_sheet_ext
end type
type cb_cancel from u_base_commandbutton_ext within w_edi_rpt_inq
end type
type cb_ok from u_base_commandbutton_ext within w_edi_rpt_inq
end type
type dw_edi_rpt_inq from u_base_dw_ext within w_edi_rpt_inq
end type
end forward

global type w_edi_rpt_inq from w_base_sheet_ext
integer width = 1778
integer height = 1212
string title = "EDI Product Cross Reference Report Inquire"
cb_cancel cb_cancel
cb_ok cb_ok
dw_edi_rpt_inq dw_edi_rpt_inq
end type
global w_edi_rpt_inq w_edi_rpt_inq

type variables
u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_rpt_inq.create
int iCurrent
call super::create
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.dw_edi_rpt_inq=create dw_edi_rpt_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_cancel
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.dw_edi_rpt_inq
end on

on w_edi_rpt_inq.destroy
call super::destroy
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.dw_edi_rpt_inq)
end on

event ue_postopen;call super::ue_postopen;
DataWindowChild	ldwc_customer
Integer				li_rtn
String				ls_output_string

iu_orp001 = CREATE u_orp001
iu_ws_orp1 = CREATE u_ws_orp1

istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_orp001.nf_orpo06br_inq_edi_customer(istr_error_info, &
//																ls_output_string)

li_rtn = iu_ws_orp1.nf_orpo06fr(istr_error_info, &
											ls_output_string)										
										

dw_edi_rpt_inq.getchild( "customer_id", ldwc_customer)
li_rtn = ldwc_customer.importstring(ls_output_string)

dw_edi_rpt_inq.SetITem(1,"product_status","A")

end event

event close;call super::close;If IsValid( iu_orp001) Then Destroy( iu_orp001)
destroy iu_ws_orp1
end event

type cb_cancel from u_base_commandbutton_ext within w_edi_rpt_inq
integer x = 1079
integer y = 932
integer width = 256
integer height = 108
integer taborder = 30
integer textsize = -9
integer weight = 400
string facename = "MS Sans Serif"
string text = "Cancel"
end type

event clicked;call super::clicked;Close (Parent)
end event

type cb_ok from u_base_commandbutton_ext within w_edi_rpt_inq
integer x = 439
integer y = 932
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 400
string facename = "MS Sans Serif"
string text = "OK"
end type

event clicked;call super::clicked;String 	ls_input_string
Integer	li_rtn


istr_error_info.se_event_name = "OK-clicked"			 

dw_edi_rpt_inq.AcceptText()

ls_input_string = dw_edi_rpt_inq.Describe("DataWindow.Data")


//li_rtn = iu_orp001.nf_orpo11br_init_edi_prod_rpts(istr_error_info, &
//																ls_input_string)
																
li_rtn = iu_ws_orp1.nf_orpo11fr(istr_error_info, &
											ls_input_string)																
										


end event

type dw_edi_rpt_inq from u_base_dw_ext within w_edi_rpt_inq
integer x = 50
integer y = 28
integer width = 1618
integer taborder = 10
string dataobject = "d_edi_rpt_inq"
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)

this.object.product_code_char.Visible = false
end event

event itemchanged;call super::itemchanged;Choose Case This.GetColumnName()
Case 'sort_option'
	If data = "G" Then
		this.object.product_code_char.Visible = true
	else	
		this.object.product_code_char.Visible = false
	End If

End Choose

end event

