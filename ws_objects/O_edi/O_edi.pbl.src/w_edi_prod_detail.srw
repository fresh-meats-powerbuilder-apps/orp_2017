﻿$PBExportHeader$w_edi_prod_detail.srw
$PBExportComments$Edi Customer Detail Prodcut Update
forward
global type w_edi_prod_detail from w_netwise_sheet
end type
type dw_cust_product_filter from u_base_dw_ext within w_edi_prod_detail
end type
type dw_edi_prod_scroll_prodct from u_base_dw_ext within w_edi_prod_detail
end type
type dw_edi_prod_detail_choice from u_base_dw_ext within w_edi_prod_detail
end type
type dw_edi_prod_detail_header from u_base_dw_ext within w_edi_prod_detail
end type
type dw_edi_prod_detail from u_base_dw_ext within w_edi_prod_detail
end type
end forward

global type w_edi_prod_detail from w_netwise_sheet
integer width = 3145
integer height = 1796
string title = "Customer Product Cross Reference Detail"
boolean maxbox = false
long backcolor = 12632256
event ue_getcustomerid pbm_custom63
event ue_systemcommand pbm_syscommand
event ue_open_order_confirmation ( )
event ue_getdata ( string as_stringvalue )
event ue_pa_summary ( )
event ue_print_report ( )
event ue_excp_detail ( )
dw_cust_product_filter dw_cust_product_filter
dw_edi_prod_scroll_prodct dw_edi_prod_scroll_prodct
dw_edi_prod_detail_choice dw_edi_prod_detail_choice
dw_edi_prod_detail_header dw_edi_prod_detail_header
dw_edi_prod_detail dw_edi_prod_detail
end type
global w_edi_prod_detail w_edi_prod_detail

type variables
s_error		istr_error_info

u_orp003		iu_orp003
u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

long		il_selected_rows

String		is_MicroHelp, &
		is_customer_inq, &
		is_update_string, &
		is_columnname, &
		is_filter

Boolean  		ib_modified = false, &
		ib_updating , &
		ib_reinquire, &
		ib_retrieve, &
		ib_updateable
	
end variables

forward prototypes
public subroutine wf_delete ()
public subroutine wf_rightclick ()
public subroutine wf_print_edi_report (string as_customer_id, string as_product_status, string as_sort_option, string as_prod_char)
public function boolean wf_addrow ()
public function boolean wf_unstring_output (string as_output_string)
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
public function boolean wf_update_modify (long al_row, character ac_status_ind)
public function boolean wf_retrieve ()
end prototypes

on ue_getcustomerid;call w_netwise_sheet::ue_getcustomerid;Message.StringParm = ''
end on

event ue_print_report();string		ls_customer_id


ls_customer_id = dw_edi_prod_detail_header.GetItemString( 1, "customer_id") 


wf_print_edi_report(ls_customer_id,' ', 'C', ' ')
end event

event ue_excp_detail();String					ls_customer_id, &
							ls_customer_name, ls_string
							
Window 					lw_Open

u_string_functions	lu_string_functions

ls_customer_id = dw_edi_prod_detail_header.GetItemString( 1, "customer_id") 
ls_customer_name = dw_edi_prod_detail_header.GetItemString( 1, "customer_name")
Event ue_set_data('customer_id', ls_customer_id)
Event ue_set_data('customer_name', ls_customer_name)

ls_string = dw_edi_prod_detail_header.GetItemString(1, "customer_id") &
									+"~t"+ dw_edi_prod_detail_header.GetItemString(1, "customer_name")
							

OpenSheetWithParm(lw_open, ls_string, "w_edi_excp_detail", iw_frame, 0, iw_frame.im_menu.iao_arrangeopen)
end event

public subroutine wf_delete ();long	ll_deleted_row

ll_deleted_row = dw_edi_prod_detail.getselectedrow(0)


DO WHILE ll_deleted_row > 0
	dw_edi_prod_detail.deleterow(ll_deleted_row)
	ll_deleted_row = dw_edi_prod_detail.getselectedrow(ll_deleted_row)
LOOP


end subroutine

public subroutine wf_rightclick ();Constant Date ld_date = Today() 

m_edi_prod_popup NewMenu

u_project_functions lu_project_functions

if lu_project_functions.nf_isscheduler() then return

NewMenu = CREATE m_edi_prod_popup
NewMenu.m_file.m_customerexceptions.Enabled = TRUE 

NewMenu.M_file.m_edi_cross_ref_report.enabled = TRUE

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu
end subroutine

public subroutine wf_print_edi_report (string as_customer_id, string as_product_status, string as_sort_option, string as_prod_char);String 	ls_input_string
Integer	li_rtn


ls_input_string = as_customer_id + '~t' + as_product_status + '~t' + &
				as_sort_option + '~t' +as_prod_char + '~r~n'


//li_rtn = iu_orp001.nf_orpo11br_init_edi_prod_rpts(istr_error_info, &
//																ls_input_string)
																
li_rtn = iu_ws_orp1.nf_orpo11fr(istr_error_info, &
											ls_input_string)										
end subroutine

public function boolean wf_addrow ();Long		ll_value


ll_value = dw_edi_prod_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

dw_edi_prod_detail.insertrow(0)

ll_value = ll_value + 1
dw_edi_prod_detail.SetItem(ll_value, "product_code", ' ')
dw_edi_prod_detail.SetColumn("product_code")
dw_edi_prod_detail.SetFocus()
dw_edi_prod_detail.SetItem(ll_value, "start_date", today())
dw_edi_prod_detail.uf_changerowstatus(ll_value, New!)
Return TRUE
end function

public function boolean wf_unstring_output (string as_output_string);Date					ldt_start_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows
						

String				ls_cust_product, &
						ls_fm_product, &
						ls_customer_id, &
						ls_searchstring, &
						ls_temp, &
						ls_start_date
						

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_edi_prod_detail.RowCount()

dw_edi_prod_detail.SelectRow(0,False)

//find the row that matches the fm product, cust product and start date

ll_nbrrows = dw_edi_prod_detail.RowCount()
ls_customer_id = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_fm_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_cust_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_start_date = lu_string_functions.nf_gettoken(as_output_string, '~t')

ls_SearchString = 	"product_code = '"+ ls_fm_product +&
							"' and cust_prod_code = '" + ls_cust_product + "'" +&
							" and start_date = date('" + ls_start_date+ "')" 
	
ll_rtn = dw_edi_prod_detail.Find  &
					( ls_SearchString, 1, ll_nbrrows)
		
If ll_rtn > 0 Then
	dw_edi_prod_detail.SetRedraw(False)
	dw_edi_prod_detail.ScrollToRow(ll_rtn)
	dw_edi_prod_detail.SetRow(ll_rtn)
	dw_edi_prod_detail.SelectRow(ll_rtn, True)
	dw_edi_prod_detail.SetRedraw(True)
End If

as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public function boolean wf_update ();String	ls_input, &
			ls_output_string_in, &
			ls_output_string, &
			ls_header_string, &
			ls_output_string_returned, &
			ls_MicroHelp, &
			ls_customer_id, &
			ls_product_code, &
			ls_default_ind, &
			ls_cust_prod_code, &
			ls_dept_code, &
			ls_upc_code, &
 			ls_searchstring, &
			ls_cust_prod

Date		ldt_start_date, &
			ldt_end_date, &
			ldt_found_start_date, &
			ldt_found_end_date
			
Integer	li_rtn, &
			li_count
			
Long		ll_rtn
			
			
			
Long		ll_rowcount, &
			ll_pos, &
			ll_col, &
			ll_row, &
			ll_modrows, &
			ll_delrows

char		lc_status_ind

Boolean	lb_error_found

u_string_functions	lu_string_functions

if dw_edi_prod_detail.AcceptText() = -1 then
	Return False
end if

SetPointer(HourGlass!)
This.SetRedraw(false)

is_update_string = ''

ll_modrows = dw_edi_prod_detail.modifiedcount()
ll_delrows = dw_edi_prod_detail.deletedcount()

ls_header_string = 'P' + '~t' + &
			  dw_edi_prod_detail_header.GetItemString( 1, "customer_id") + '~h7F' 
			  
// get all deleted records from buffer
For li_count = 1 to ll_delrows
	lc_status_ind = 'D'
	
	if not this.wf_update_modify(li_count, lc_status_ind) then
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if

Next

// get all new & modified rows from buffer
ll_row = 0

for li_count = 1 to ll_modrows

	ll_Row = dw_edi_prod_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then 
			This.SetRedraw(True) 
			Return False
		end if	
	END IF
	
//LOOP
	choose case dw_edi_prod_detail.getitemstatus(ll_row, 0, primary!)
		case newmodified!
			lc_status_ind = 'A'
		case datamodified!
			lc_status_ind = 'M'
	end choose
	
	if not this.wf_update_modify(ll_row, lc_status_ind) then
		ib_updating = false
		return false
	end if
next

ls_output_string_in = is_update_string
istr_error_info.se_function_name = "wf_update"


//  mainframe call
//li_rtn = iu_orp001.nf_orpo08br_edi_cust_prod_det_upd (istr_error_info, ls_output_string_in, ls_output_string, ls_header_string)
li_rtn = iu_ws_orp1.nf_orpo08fr(istr_error_info, ls_output_string_in, ls_output_string, ls_header_string)																			
if li_rtn <> 0 then
	if ls_output_string > '' Then
		wf_unstring_output(ls_output_string)
	End If
	This.SetRedraw(True)
	SetPointer(Arrow!)																		
	return false
end if

dw_edi_prod_detail.ResetUpdate()
ib_ReInquire = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")
dw_edi_prod_detail.SetColumn("product_code")

ls_customer_id = dw_edi_prod_detail_header.GetItemString( 1, "customer_id")
SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)

dw_edi_prod_detail.SetFocus()
dw_edi_prod_detail.SetReDraw(True)

Return True

end function

public function boolean wf_validate (long al_row);string	ls_customer_id, &
			ls_product_code, &
			ls_default_ind, &
			ls_cust_prod_code, &
			ls_dept_code, &
			ls_upc_code, &
			ls_prev_cust_prod, &
			ls_searchstring
		
Date		ldt_start_date, &
			ldt_end_date
			
			
Long		ll_rtn, ll_value
			
			
Long		ll_modrows

ll_modrows = dw_edi_prod_detail.modifiedcount()

ls_product_code = dw_edi_prod_detail.getitemstring(al_row, "product_code")

	if iw_frame.iu_string.nf_isempty(ls_product_code) then
		iw_frame.setmicrohelp("Product Code is a required field.")
		dw_edi_prod_detail.SelectRow(0, False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetColumn("product_code")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	ls_default_ind = dw_edi_prod_detail.getitemstring(al_row, "default_ind")
	if isNull(ls_default_ind) then
		dw_edi_prod_detail.setitem(al_row, "default_ind", ' ')
	end if
	
	ls_prev_cust_prod = dw_edi_prod_detail.getitemstring(al_row, "prev_cust_prod")
	if isNull(ls_prev_cust_prod) then
		dw_edi_prod_detail.setitem(al_row, "prev_cust_prod", ' ')
	end if
	
	
	ls_cust_prod_code = dw_edi_prod_detail.getitemstring(al_row, "cust_prod_code")
	IF IsNull(ls_cust_prod_code) or Len(trim(ls_cust_prod_code)) = 0 Then 
//	if iw_frame.iu_string.nf_isempty(ls_cust_prod_code) then
		iw_frame.setmicrohelp("Customer Product Code is a required field.")
		dw_edi_prod_detail.SelectRow(0, False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetColumn("cust_prod_code")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	ls_dept_code = dw_edi_prod_detail.getitemstring(al_row, "dept_code")
	if isNull(ls_dept_code) then
		dw_edi_prod_detail.setitem(al_row, "dept_code", ' ')
	end if
	
	ls_upc_code = dw_edi_prod_detail.getitemstring(al_row, "upc_code")
	if isNull(ls_upc_code) then
		dw_edi_prod_detail.setitem(al_row, "upc_code", ' ')
	end if
	
	ldt_start_date = dw_edi_prod_detail.getItemDate(al_row, "start_date")
 	if isnull(ldt_start_date) then
		iw_frame.setmicrohelp("Start Date is a required field.")
		This.SetRedraw(False)
		dw_edi_prod_detail.SelectRow(0, False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetColumn("start_date")
		dw_edi_prod_detail.SetFocus()
		ib_updating = false
		This.SetRedraw(True)
		return false		
	end if
	
	
//	choose case dw_edi_prod_detail.getitemstatus(al_row, 0, primary!)
//		case newmodified!
//			if ldt_start_date < today() then
//				iw_frame.setmicrohelp("Start Date must be equal to or greater than current date.")
//				This.SetRedraw(False)
//				dw_edi_prod_detail.ScrollToRow(al_row)
//				dw_edi_prod_detail.SelectRow(al_row, True)
//				dw_edi_prod_detail.SetColumn("start_date")
//				dw_edi_prod_detail.SetFocus()
//				ib_updating = false
//				This.SetRedraw(True)
//				return false		
//			end if
//	end choose
		
	ldt_end_date = dw_edi_prod_detail.getItemDate(al_row, "end_date")
 	if isnull(ldt_end_date) then
		iw_frame.setmicrohelp("End Date is a required field.")
		This.SetRedraw(False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SetColumn("end_date")
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetFocus()
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	if ldt_start_date > ldt_end_date then
		iw_frame.setmicrohelp("Start Date must be less than End Date.")
		This.SetRedraw(False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetColumn("start_date")
		dw_edi_prod_detail.SetFocus()
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	If ls_default_ind = 'Y' then
	
		ls_SearchString	= "cust_prod_code = '" + ls_cust_prod_code + &
							"' and default_ind = '" + ls_default_ind + "'" 
							
	// Find a matching row excluding the current row.
	ll_rtn = iw_frame.iu_string.nf_compare_dates(al_row, ls_SearchString, dw_edi_prod_detail) 

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "Only one row can be set as the default with same Customer Product Code.")
		dw_edi_prod_detail.SetRedraw(False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SetColumn("start_date")
		dw_edi_prod_detail.SetRow(al_row)
		dw_edi_prod_detail.SelectRow(ll_rtn, True)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetRedraw(True)
		Return False
	End If
End if


	If ls_default_ind = 'Y' then
	
		ls_SearchString	= "product_code = '" + ls_product_code + &
							"' and default_ind = '" + ls_default_ind + "'" 
							
	// Find a matching row excluding the current row.
	ll_rtn = iw_frame.iu_string.nf_compare_dates(al_row, ls_SearchString, dw_edi_prod_detail) 

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "Only one row can be set as the default with same FM Product Code.")
		dw_edi_prod_detail.SetRedraw(False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SetColumn("start_date")
		dw_edi_prod_detail.SetRow(al_row)
		dw_edi_prod_detail.SelectRow(ll_rtn, True)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetRedraw(True)
		Return False
	End If
End if

	ls_SearchString	= "product_code = '" + ls_product_code + &
							"' and cust_prod_code = '" + ls_cust_prod_code	+  "'"
						

// Find a matching row excluding the current row.
	ll_rtn = iw_frame.iu_string.nf_compare_dates(al_row, ls_SearchString, dw_edi_prod_detail) 

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate FM Products and Customer Product with overlapping dates.")
		dw_edi_prod_detail.SetRedraw(False)
		dw_edi_prod_detail.SelectRow(0, False)
		dw_edi_prod_detail.ScrollToRow(al_row)
		dw_edi_prod_detail.SetColumn("start_date")
		dw_edi_prod_detail.SetRow(al_row)
		dw_edi_prod_detail.SelectRow(ll_rtn, True)
		dw_edi_prod_detail.SelectRow(al_row, True)
		dw_edi_prod_detail.SetRedraw(True)
		Return False
	End If
Return True
end function

public function boolean wf_update_modify (long al_row, character ac_status_ind);dwbuffer	ldwb_buffer
string	ls_start_date, ls_prev_start_date, &
			ls_end_date, ls_product_code, ls_default_ind, &
			ls_cust_prod_code, ls_dept_code, ls_upc_code, ls_prev_cust_prod
			
Long		ll_rtn, &
			ll_nbrrows
			
ll_nbrrows = dw_edi_prod_detail.RowCount()

if ac_status_ind = 'D' then
	ldwb_buffer = delete!
else
	ldwb_buffer = primary!
end if

ls_start_date = String(dw_edi_prod_detail.GetItemdate(al_row, "start_date", ldwb_buffer, false),'yyyy-mm-dd')
ls_end_date = String(dw_edi_prod_detail.GetItemdate(al_row, "end_date", ldwb_buffer, false),'yyyy-mm-dd')
ls_product_code = trim(dw_edi_prod_detail.getitemstring (al_row, "product_code",  ldwb_buffer, false))
ls_default_ind = dw_edi_prod_detail.getitemstring (al_row, "default_ind",  ldwb_buffer, false)
ls_cust_prod_code = trim(dw_edi_prod_detail.getitemstring (al_row, "cust_prod_code",  ldwb_buffer, false))
ls_dept_code = dw_edi_prod_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false)
ls_upc_code = dw_edi_prod_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false)
ls_prev_cust_prod = dw_edi_prod_detail.getitemstring (al_row, "prev_cust_prod",  ldwb_buffer, false)
ls_prev_start_date = String(dw_edi_prod_detail.GetItemdate(al_row, "prev_start_date", ldwb_buffer, false),'yyyy-mm-dd')

is_update_string += &
	trim(dw_edi_prod_detail.getitemstring (al_row, "product_code",  ldwb_buffer, false)) + "~t" + &
	dw_edi_prod_detail.getitemstring (al_row, "default_ind",  ldwb_buffer, false) + "~t" + &
	trim(dw_edi_prod_detail.getitemstring (al_row, "cust_prod_code",  ldwb_buffer, false)) + "~t" + &
	dw_edi_prod_detail.getitemstring (al_row, "dept_code",  ldwb_buffer, false) + "~t" + &
	dw_edi_prod_detail.getitemstring (al_row, "upc_code",  ldwb_buffer, false) + "~t" + &
	String(dw_edi_prod_detail.GetItemdate(al_row, "start_date", ldwb_buffer, false),'yyyy-mm-dd') + "~t" + &
	String(dw_edi_prod_detail.GetItemdate(al_row, "end_date", ldwb_buffer, false),'yyyy-mm-dd') + "~t" + &
	dw_edi_prod_detail.getitemstring (al_row, "prev_cust_prod",  ldwb_buffer, false) + "~t" + &
	String(dw_edi_prod_detail.GetItemdate(al_row, "prev_start_date", ldwb_buffer, false),'yyyy-mm-dd') + "~t" + &
	ac_status_ind + "~r~n"
	
return true

end function

public function boolean wf_retrieve ();Boolean	lb_show_message
String		ls_input, &
				ls_output_string, &
				ls_row_option, &
				ls_cust_prod_filter	
				
Integer		li_rtn

Long		ll_value


TriggerEvent("closequery")

ls_input = Message.StringParm
If Not ib_ReInquire Then
	OpenWithParm(w_edi_prod_detail_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_input = 'P' + '~t' + &
			  dw_edi_prod_detail_header.GetItemString( 1, "customer_id") + '~t' + &
			  ' ' + '~r~n'
			  
istr_error_info.se_function_name = "wf_retrieve"

dw_edi_prod_detail.Reset()
//li_rtn = iu_orp001.nf_orpo07br_edi_cust_prod_det_inq (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp1.nf_orpo07fr(istr_error_info, ls_input, ls_output_string)
ib_retrieve = True
dw_edi_prod_detail.ImportString(ls_output_string)
dw_edi_prod_detail.ResetUpdate()

if li_rtn = 0 Then
	ls_row_option = dw_edi_prod_detail_choice.GetItemString(1, "choice")
	If ls_row_option = 'C' Then
		is_filter = "(end_date > RelativeDate(Today(), -1)) and (product_code > ' ')" 
	Else
		is_filter = "(product_code > ' ')" 
	End If
	dw_cust_product_filter.AcceptText()
	ls_cust_prod_filter = dw_cust_product_filter.GetItemString(1, "cust_product_code") 
	If ls_cust_prod_filter > "" Then
		is_filter += " and (cust_prod_code like " + "'" + trim(ls_cust_prod_filter) + "%')"
	End If
	dw_edi_prod_detail.SetFilter(is_filter)
	dw_edi_prod_detail.Filter()
End if

ll_value = dw_edi_prod_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

dw_edi_prod_detail.ResetUpdate()
dw_edi_prod_detail_choice.ResetUpdate()

IF ll_value > 0 THEN
	dw_edi_prod_detail.SetFocus()
	dw_edi_prod_detail.ScrollToRow(1)
	dw_edi_prod_detail.SetColumn( "product_code" )
	dw_edi_prod_detail.TriggerEvent("RowFocusChanged")
END IF

dw_edi_prod_detail.SetSort("product_code, start_date, end_date, default_ind")
dw_edi_prod_detail.Sort()
dw_edi_prod_detail_choice.ResetUpdate()
dw_edi_prod_detail.ResetUpdate()
dw_edi_prod_detail.SetReDraw(True)
This.SetRedraw(True)
dw_edi_prod_detail.insertrow(0)

ll_value = ll_value + 1
dw_edi_prod_detail.SetItem(ll_value, "product_code", ' ')
dw_edi_prod_detail.SetColumn("product_code")
dw_edi_prod_detail.SetFocus()
dw_edi_prod_detail.SetItem(ll_value, "start_date", today())
dw_edi_prod_detail.ResetUpdate()
dw_edi_prod_detail.uf_changerowstatus(ll_value, New!)
ib_retrieve = False

Return TRUE
end function

on w_edi_prod_detail.create
int iCurrent
call super::create
this.dw_cust_product_filter=create dw_cust_product_filter
this.dw_edi_prod_scroll_prodct=create dw_edi_prod_scroll_prodct
this.dw_edi_prod_detail_choice=create dw_edi_prod_detail_choice
this.dw_edi_prod_detail_header=create dw_edi_prod_detail_header
this.dw_edi_prod_detail=create dw_edi_prod_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cust_product_filter
this.Control[iCurrent+2]=this.dw_edi_prod_scroll_prodct
this.Control[iCurrent+3]=this.dw_edi_prod_detail_choice
this.Control[iCurrent+4]=this.dw_edi_prod_detail_header
this.Control[iCurrent+5]=this.dw_edi_prod_detail
end on

on w_edi_prod_detail.destroy
call super::destroy
destroy(this.dw_cust_product_filter)
destroy(this.dw_edi_prod_scroll_prodct)
destroy(this.dw_edi_prod_detail_choice)
destroy(this.dw_edi_prod_detail_header)
destroy(this.dw_edi_prod_detail)
end on

event ue_query;call super::ue_query;IF wf_Retrieve() = false Then
		This.SetRedraw( TRUE)
		CLOSE(THIS)
		RETURN
END IF
end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")

gw_netwise_frame.SetMicroHelp( "Ready")



end event

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_delete")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")

end event

event deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")

end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
destroy iu_ws_orp1
end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp1 = Create u_ws_orp1

String	ls_ret, &
			ls_message, &
			ls_customer_id, &
			ls_customer_name
		
u_string_functions		lu_string_functions	
			

//is_initial_reference = Message.StringParm

ls_ret = dw_edi_prod_detail_header.Modify('excp_customer_id.visible = 0 ' + &
									'excp_customer_name.visible = 0 ' + &
										'excp_customer_id_t.visible = 0')


//iw_this = this




end event

event ue_set_data;choose case as_data_item
	case "customer_id"
		dw_edi_prod_detail_header.setItem(1, 'customer_id', as_value)
	case "customer_name"
		dw_edi_prod_detail_header.setItem(1, 'customer_name', as_value)
end choose
end event

event ue_get_data;Choose Case as_value
	Case 'customer_id'
		message.StringParm = dw_edi_prod_detail_header.getitemstring (1, "customer_id")
	Case 'customer_name'
		message.StringParm = dw_edi_prod_detail_header.getitemstring (1, "customer_name")	
End choose

end event

type dw_cust_product_filter from u_base_dw_ext within w_edi_prod_detail
integer x = 1573
integer y = 352
integer width = 2011
integer height = 96
integer taborder = 40
string dataobject = "d_edi_cust_prod_filter"
boolean border = false
end type

event editchanged;call super::editchanged;String	ls_filter

Long		ll_position

ll_position = pos(is_filter, "(cust_prod_code like")

If ll_position = 0 Then  //  string does not contain filter on cust_prod_code
	ls_filter = is_filter + " and (cust_prod_code like " + "'" + trim(data) + "%')"
	is_filter = ls_filter
Else
	If ll_position = 1 Then  // Filter on cust product only
		ls_filter = "(cust_prod_code like " + "'" + trim(data) + "%')"
		is_filter = ls_filter
	Else				// Keep filter on other fields	
		ls_filter = mid(is_filter, 1, ll_position - 1)
		ls_filter = ls_filter + "(cust_prod_code like " + "'" + trim(data) + "%')"
		is_filter = ls_filter
	End If
End If

dw_edi_prod_detail.SetFilter(is_filter)
dw_edi_prod_detail.Filter()


end event

event constructor;call super::constructor;If this.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_edi_prod_scroll_prodct from u_base_dw_ext within w_edi_prod_detail
integer y = 356
integer width = 1225
integer height = 96
integer taborder = 40
string dataobject = "d_edi_prod_scroll_product"
boolean border = false
boolean ib_selecttext = false
end type

event editchanged;call super::editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row

ll_row = dw_edi_prod_detail.Find("product_code >= '" + data + "'",1,dw_edi_prod_detail.RowCount()+1)

If ll_row > 0 Then 
	dw_edi_prod_detail.ScrollToRow(ll_row)
	dw_edi_prod_detail.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_edi_prod_detail.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_edi_prod_detail.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_edi_prod_detail.SetRedraw(False)
	dw_edi_prod_detail.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_edi_prod_detail.ScrollToRow(ll_row)
	dw_edi_prod_detail.SetRow(ll_row + 1)
	dw_edi_prod_detail.SetRedraw(True)
End If
end event

event constructor;call super::constructor;This.InsertRow(0)
ib_updateable = False
end event

type dw_edi_prod_detail_choice from u_base_dw_ext within w_edi_prod_detail
integer x = 1600
integer y = 12
integer width = 626
integer height = 220
integer taborder = 30
string dataobject = "d_edi_prod_detail_choice"
boolean border = false
end type

event itemchanged;call super::itemchanged;String		ls_row_option, &
				ls_cust_prod_filter

Date			ld_date
Long			ll_value


dw_edi_prod_detail.SetReDraw(False)
ls_row_option = data
ld_date = today()
If ls_row_option = 'C' Then
	dw_edi_prod_detail.SetFilter("")
	dw_edi_prod_detail.Filter()
	is_filter = "(end_date > RelativeDate(Today(), -1)) and (product_code > ' ')" 
Else
	dw_edi_prod_detail.SetFilter("")
	dw_edi_prod_detail.Filter()
	is_filter = "(product_code > ' ')" 
End If
dw_cust_product_filter.AcceptText()
ls_cust_prod_filter = dw_cust_product_filter.GetItemString(1, "cust_product_code") 
If ls_cust_prod_filter > "" Then
	is_filter += " and (cust_prod_code like " + "'" + trim(ls_cust_prod_filter) + "%')"
End If

dw_edi_prod_detail.SetFilter(is_filter)
dw_edi_prod_detail.Filter()


dw_edi_prod_detail.SELECTROW(dw_edi_prod_detail.GETROW(),false)
ll_value = dw_edi_prod_detail.RowCount()
dw_edi_prod_detail.AcceptText()
dw_edi_prod_detail.SetSort("product_code, start_date, end_date, default_ind")
dw_edi_prod_detail.Sort()
dw_edi_prod_detail.GroupCalc()
dw_edi_prod_detail.insertrow(0)


ll_value = ll_value + 1
dw_edi_prod_detail.SetItem(ll_value, "start_date", today())
dw_edi_prod_detail.SetItem(ll_value, "product_code", ' ')
dw_edi_prod_detail.SetColumn("product_code")
dw_edi_prod_detail.SetFocus()
dw_edi_prod_detail.ResetUpdate()
dw_edi_prod_detail.uf_changerowstatus(ll_value, New!)
dw_edi_prod_detail.SetReDraw(True)
end event

type dw_edi_prod_detail_header from u_base_dw_ext within w_edi_prod_detail
integer y = 12
integer width = 1577
integer height = 364
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_edi_prod_detail_header"
boolean border = false
end type

event constructor;call super::constructor;dw_edi_prod_detail_header.InsertRow(0)
dw_edi_prod_detail_choice.InsertRow(0)

end event

type dw_edi_prod_detail from u_base_dw_ext within w_edi_prod_detail
integer y = 464
integer width = 2587
integer height = 1120
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_edi_prod_detail"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;is_selection = '2'
ib_updateable = true
ib_NewRowOnChange = False

end event

event itemchanged;call super::itemchanged;string				ls_temp, &
						ls_product_code

long					ll_rowfound, &
						ll_rowcount

Choose Case dwo.name


	case "product_code"
		ll_rowcount = dw_edi_prod_detail.RowCount()
		ls_product_code = data
		if ls_product_code > ' ' then
			wf_addrow()
		end if
	
end choose




end event

event itemerror;call super::itemerror;return 1
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event itemfocuschanged;call super::itemfocuschanged;if ib_retrieve Then Return 1
is_columnname = dwo.name

if is_columnname = 'cust_prod_code' then
	This.SetText(Trim(this.GetText()))
	This.SelectText(1,len(trim(this.GetText())))
end if


end event

