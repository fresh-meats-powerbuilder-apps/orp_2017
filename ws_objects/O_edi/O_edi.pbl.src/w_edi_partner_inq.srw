﻿$PBExportHeader$w_edi_partner_inq.srw
forward
global type w_edi_partner_inq from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_edi_partner_inq
end type
type dw_edi_partner_inq from u_base_dw_ext within w_edi_partner_inq
end type
end forward

global type w_edi_partner_inq from w_base_response
integer x = 1075
integer y = 485
integer width = 1449
integer height = 708
string title = "Inquire Product Cross Reference Customer"
boolean controlmenu = false
long backcolor = 12632256
cb_browse cb_browse
dw_edi_partner_inq dw_edi_partner_inq
end type
global w_edi_partner_inq w_edi_partner_inq

type variables
w_base_sheet_ext		iw_parentwindow
//w_base_sheet	iw_parent

DataWindowChild	idwc_customer

u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_partner_inq.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
this.dw_edi_partner_inq=create dw_edi_partner_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
this.Control[iCurrent+2]=this.dw_edi_partner_inq
end on

on w_edi_partner_inq.destroy
call super::destroy
destroy(this.cb_browse)
destroy(this.dw_edi_partner_inq)
end on

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;DataWindowChild	ldwc_customer
Integer				li_rtn
String				ls_output_string

iu_orp001 = CREATE u_orp001
iu_ws_orp1 = CREATE u_ws_orp1
	  

//li_rtn = iu_orp001.nf_orpo06br_inq_edi_customer(istr_error_info, &
//																ls_output_string)
																
li_rtn = iu_ws_orp1.nf_orpo06fr(istr_error_info, &
											ls_output_string)																
										

dw_edi_partner_inq.getchild( "customer_id", ldwc_customer)
li_rtn = ldwc_customer.importstring(ls_output_string)
end event

event close;call super::close;If IsValid( iu_orp001) Then Destroy( iu_orp001)

DESTROY iu_ws_orp1
end event

event ue_base_ok;call super::ue_base_ok;String 	ls_input, &
	ls_output_string
Integer	li_rtn


istr_error_info.se_event_name = "OK-clicked"			 


dw_edi_partner_inq.AcceptText()

ls_input = 'C' + '~t' + &
			  dw_edi_partner_inq.GetItemString( 1, "customer_id") + '~r~n'
			  

//li_rtn = iu_orp001.nf_orpo07br_edi_cust_prod_det_inq (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp1.nf_orpo07fr(istr_error_info, ls_input, ls_output_string)
dw_edi_partner_inq.setitem(1, "partner_id", ls_output_string)

dw_edi_partner_inq.ResetUpdate()

																
										


end event

event activate;call super::activate;gw_base_frame.im_base_Menu.M_File.M_New.Enabled = FALSE
gw_base_frame.im_base_Menu.M_file.m_Delete.Enabled = FALSE
gw_base_frame.im_base_Menu.M_file.M_Inquire.Enabled = FALSE
gw_base_frame.im_base_Menu.M_file.M_Save.Enabled = FALSE
end event

event deactivate;call super::deactivate;gw_base_frame.im_base_Menu.M_File.M_New.Enabled = TRUE
gw_base_frame.im_base_Menu.M_file.m_Delete.Enabled = TRUE
gw_base_frame.im_base_Menu.M_file.M_Inquire.Enabled = TRUE
gw_base_frame.im_base_Menu.M_file.M_Save.Enabled = TRUE
end event

type cb_base_help from w_base_response`cb_base_help within w_edi_partner_inq
boolean visible = false
integer x = 992
integer y = 256
integer taborder = 40
boolean enabled = false
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_edi_partner_inq
integer x = 526
integer y = 416
integer taborder = 50
end type

event cb_base_cancel::clicked;call super::clicked;Close (Parent)
end event

type cb_base_ok from w_base_response`cb_base_ok within w_edi_partner_inq
integer x = 206
integer y = 416
integer taborder = 30
end type

type cb_browse from u_base_commandbutton_ext within w_edi_partner_inq
boolean visible = false
integer x = 2126
integer y = 396
integer height = 108
integer taborder = 10
string text = "&Browse"
end type

type dw_edi_partner_inq from u_base_dw_ext within w_edi_partner_inq
integer x = 27
integer y = 16
integer width = 1362
integer height = 364
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_edi_partner_inq"
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;dw_edi_partner_inq.setitem(1, "partner_id", ' ')
end event

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)

end event

event itemerror;call super::itemerror;Return 1
end event

