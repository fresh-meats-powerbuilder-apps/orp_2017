﻿$PBExportHeader$w_edi_cust_inactivate.srw
forward
global type w_edi_cust_inactivate from w_base_sheet_ext
end type
type cbx_1 from checkbox within w_edi_cust_inactivate
end type
type cb_cancel from u_base_commandbutton_ext within w_edi_cust_inactivate
end type
type cb_ok from u_base_commandbutton_ext within w_edi_cust_inactivate
end type
type dw_edi_cust_inactivate from u_base_dw_ext within w_edi_cust_inactivate
end type
end forward

global type w_edi_cust_inactivate from w_base_sheet_ext
integer width = 1166
integer height = 608
string title = "EDI Product Cross Reference Mass Inactivation"
cbx_1 cbx_1
cb_cancel cb_cancel
cb_ok cb_ok
dw_edi_cust_inactivate dw_edi_cust_inactivate
end type
global w_edi_cust_inactivate w_edi_cust_inactivate

type variables
u_orp001		iu_orp001
u_ws_orp1		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_cust_inactivate.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.dw_edi_cust_inactivate=create dw_edi_cust_inactivate
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.dw_edi_cust_inactivate
end on

on w_edi_cust_inactivate.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.dw_edi_cust_inactivate)
end on

event ue_postopen;call super::ue_postopen;
DataWindowChild	ldwc_customer
Integer				li_rtn
String				ls_output_string


iu_orp001 = CREATE u_orp001
iu_ws_orp1 = CREATE u_ws_orp1


istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_orp001.nf_orpo06br_inq_edi_customer(istr_error_info, &
//																ls_output_string)
																
li_rtn = iu_ws_orp1.nf_orpo06fr(istr_error_info, &
											ls_output_string)																
										

dw_edi_cust_inactivate.getchild( "customer_id", ldwc_customer)
li_rtn = ldwc_customer.importstring(ls_output_string)

cbx_1.Checked = False

end event

event close;call super::close;If IsValid( iu_orp001) Then Destroy( iu_orp001)
DESTROY iu_ws_orp1
end event

type cbx_1 from checkbox within w_edi_cust_inactivate
integer x = 302
integer y = 188
integer width = 695
integer height = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Exception Rows Only"
end type

type cb_cancel from u_base_commandbutton_ext within w_edi_cust_inactivate
integer x = 768
integer y = 340
integer width = 256
integer height = 108
integer taborder = 30
string text = "Cancel"
end type

event clicked;call super::clicked;Close (Parent)
end event

type cb_ok from u_base_commandbutton_ext within w_edi_cust_inactivate
integer x = 242
integer y = 344
integer height = 108
integer taborder = 20
string text = "OK"
end type

event clicked;call super::clicked;String 	ls_input_string, &
			ls_output_string
Integer	li_rtn


istr_error_info.se_event_name = "OK-clicked"			 

dw_edi_cust_inactivate.AcceptText()

ls_input_string = dw_edi_cust_inactivate.Describe("DataWindow.Data")

If MessageBox("Inactivation Confirmation", "This will inactivate product conversions for " + ls_input_string + ". " + &
				"Do you want to continue?", Question!, YesNo!, 1) = 2 Then
	return
End if

if cbx_1.Checked = TRUE Then
	ls_input_string += "~t" + "Y" + "~r~n"
Else
	ls_input_string += "~t" + "N" + "~r~n"
End if
//li_rtn = iu_orp001.nf_orpo12br_edi_cust_prod_inact(istr_error_info, &
//																ls_input_string, &
//																ls_output_string)
																
li_rtn = iu_ws_orp1.nf_orpo12fr(istr_error_info, &
											ls_input_string, &
											ls_output_string)																
										


end event

type dw_edi_cust_inactivate from u_base_dw_ext within w_edi_cust_inactivate
integer x = 5
integer y = 28
integer width = 978
integer height = 144
integer taborder = 10
string dataobject = "d_edi_cust_inactivate"
boolean border = false
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)
end event

event itemchanged;call super::itemchanged;Choose Case This.GetColumnName()
Case 'sort_option'
	If data = "G" Then
		this.object.product_code_char.Visible = true
	else	
		this.object.product_code_char.Visible = false
	End If

End Choose

end event

