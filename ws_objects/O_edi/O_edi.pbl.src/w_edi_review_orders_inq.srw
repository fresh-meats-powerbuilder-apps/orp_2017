﻿$PBExportHeader$w_edi_review_orders_inq.srw
forward
global type w_edi_review_orders_inq from w_base_response_ext
end type
type dw_edi_ship_dates from u_base_dw_ext within w_edi_review_orders_inq
end type
type dw_edi_order_line_choice from u_base_dw_ext within w_edi_review_orders_inq
end type
type dw_edi_review_orders_inq from u_base_dw_ext within w_edi_review_orders_inq
end type
end forward

global type w_edi_review_orders_inq from w_base_response_ext
integer width = 2231
integer height = 1740
string title = "EDI Review Orders Inquire"
long backcolor = 12632256
dw_edi_ship_dates dw_edi_ship_dates
dw_edi_order_line_choice dw_edi_order_line_choice
dw_edi_review_orders_inq dw_edi_review_orders_inq
end type
global w_edi_review_orders_inq w_edi_review_orders_inq

type variables
w_base_sheet_ext		iw_parentwindow
w_base_sheet	iw_parent
String			is_openstring
Boolean	ib_valid_return, &
			ib_updateable
DataWindowChild	idwc_customer

u_orp001		iu_orp001

s_error	istr_error_info
end variables

on w_edi_review_orders_inq.create
int iCurrent
call super::create
this.dw_edi_ship_dates=create dw_edi_ship_dates
this.dw_edi_order_line_choice=create dw_edi_order_line_choice
this.dw_edi_review_orders_inq=create dw_edi_review_orders_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_edi_ship_dates
this.Control[iCurrent+2]=this.dw_edi_order_line_choice
this.Control[iCurrent+3]=this.dw_edi_review_orders_inq
end on

on w_edi_review_orders_inq.destroy
call super::destroy
destroy(this.dw_edi_ship_dates)
destroy(this.dw_edi_order_line_choice)
destroy(this.dw_edi_review_orders_inq)
end on

event open;call super::open;//datawindowchild			ldwc_temp
//
//								
//String						ls_data
//								
iw_parent = Message.PowerObjectParm
If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if
//ls_data = Message.StringParm
//
//
//If dw_edi_review_orders_inq.RowCount() = 0 Then
//	dw_edi_review_orders_inq.InsertRow(0)
//End if
//
//

//

//

DataWindowChild	ldwc_customer
//is_openstring = message.stringparm
//iw_parent = Message.PowerObjectParm

dw_edi_review_orders_inq.GetChild( "customer_id", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_edi_review_orders_inq.GetChild( "customer_Name", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_edi_review_orders_inq.GetChild( "customer_type", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

//dw_edi_review_orders_inq.GetChild( "customer_id_City", ldwc_Customer)
//iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)
//
dw_edi_review_orders_inq.getchild( "div_code", ldwc_customer)
iw_frame.iu_project_functions.nf_gettutltype(ldwc_customer, "divcode")
//
ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', '  ')
ldwc_customer.SetItem(1, 'type_desc', 'ALL DIVISIONS')

If dw_edi_review_orders_inq.RowCount() = 0 Then
	dw_edi_review_orders_inq.InsertRow(0)
End if


If dw_edi_order_line_choice.RowCount() = 0 Then
	dw_edi_order_line_choice.InsertRow(0)
End if

If dw_edi_ship_dates.RowCount() = 0 Then
	dw_edi_ship_dates.InsertRow(0)
End if

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")

end event

event ue_base_ok;call super::ue_base_ok;string 	ls_customer_id, &
			ls_ship_date_ind, &
			ls_delv_date_ind, &
			ls_customer_ind, &
			ls_sales_order_ind, &
			ls_customer_name, &
			ls_customer_type, &
			ls_value, &
			ls_temp, &
			ls_choice, ls_desc, &
			ls_line_option, &
			ls_from_date, &
			ls_to_date
long 		ll_row
			
Date	ldt_temp, &
		ldt_from_date, &
		ldt_end_date

u_string_functions	lu_string_functions

dw_edi_review_orders_inq.AcceptText()
dw_edi_order_line_choice.AcceptText()
dw_edi_ship_dates.AcceptText()

//ls_choice = dw_edi_excp_detail_inq.GetItemString(1,'inq_on_excp')
ls_customer_ind =  dw_edi_review_orders_inq.GetItemString(1, 'customer_ind')
ls_sales_order_ind =  dw_edi_review_orders_inq.GetItemString(1, 'sales_order_ind')
ls_line_option = dw_edi_order_line_choice.GetItemString(1, 'choice')
if ls_customer_ind = 'Y' then
	ls_customer_type = dw_edi_review_orders_inq.GetItemString(1, 'customer_type')
	If lu_string_functions.nf_isempty(ls_customer_type) Then
		iw_frame.SetMicroHelp('Customer Type is a required field.')
		Return
	else
		iw_parent.Event ue_set_data('customer_type', ls_customer_type)
	End If	

	ls_value = dw_edi_review_orders_inq.GetItemString(1, 'customer_id')
	If lu_string_functions.nf_isempty(ls_value) Then
		iw_frame.SetMicroHelp('Customer Id is a required field.')
		Return
	else
		iw_parent.Event ue_set_data('customer_id', ls_value)
	End If	
	
	if ls_customer_type = 'S' then
			SELECT customers.short_name
				INTO :ls_customer_name
				FROM customers
				WHERE customers.customer_id = :ls_value
				USING SQLCA ;
			IF isnull(ls_customer_name) THEN
				ls_customer_name = ' '
			END IF 
			iw_parent.Event ue_set_data('customer_name', ls_customer_name)
	end if
	
	if ls_customer_type = 'B' then
			SELECT billtocust.name
				INTO :ls_customer_name
				FROM billtocust
				WHERE billtocust.bill_to_id = :ls_value
				USING SQLCA ;
			IF isnull(ls_customer_name) THEN
				ls_customer_name = ' '
			END IF 
			iw_parent.Event ue_set_data('customer_name', ls_customer_name)
	end if
	
	if ls_customer_type = 'C' then
			SELECT corpcust.name
				INTO :ls_customer_name
				FROM corpcust
				WHERE corpcust.corp_id = :ls_value
				USING SQLCA ;
			IF isnull(ls_customer_name) THEN
				ls_customer_name = ' '
			END IF 
			iw_parent.Event ue_set_data('customer_name', ls_customer_name)
	end if
	iw_parent.Event ue_set_data('customer_type', ls_customer_type)
	
	ls_value = dw_edi_review_orders_inq.GetItemString(1, 'div_code')
	If lu_string_functions.nf_isempty(ls_value) Then
		ls_value = 'AL'
		iw_parent.Event ue_set_data('div_code', ls_value)
	else
		iw_parent.Event ue_set_data('div_code', ls_value)
	End If		
	iw_parent.Event ue_set_data('sales_order_id', ' ')
end if

ls_ship_date_ind = dw_edi_ship_dates.GetItemString(1, 'ship_date_ind')
ls_delv_date_ind = dw_edi_ship_dates.GetItemString(1, 'delv_date_ind')



if ls_customer_ind = 'Y' then
	if ls_ship_date_ind = 'Y' then
		ls_from_date = String(dw_edi_ship_dates.GetItemDate(1, "ship_from_date"),'mm-dd-yyyy')
		ls_to_date = String(dw_edi_ship_dates.GetItemDate(1, "ship_to_date"),'mm-dd-yyyy')
		if dw_edi_ship_dates.GetItemDate(1, "ship_from_date") > dw_edi_ship_dates.GetItemDate(1, "ship_to_date") then
			iw_frame.SetMicroHelp('Please enter a Ship To Date greater than a Ship From Date.')
			This.SetRedraw(False)
			dw_edi_ship_dates.SetColumn("ship_to_date")
			dw_edi_ship_dates.SetFocus()
			This.SetRedraw(True)
			Return
		end if
		iw_parent.Event ue_set_data('date_option', 'S')
		iw_parent.Event ue_Set_Data('from_date', ls_from_date)		
		iw_parent.Event ue_Set_Data('to_date', ls_to_date)
		iw_parent.Event ue_set_data('inquire_option', 'C')
	else
		if ls_delv_date_ind = 'Y' then
			ls_from_date = String(dw_edi_ship_dates.GetItemDate(1, "delv_from_date"),'mm-dd-yyyy')
			ls_to_date = String(dw_edi_ship_dates.GetItemDate(1, "delv_to_date"),'mm-dd-yyyy')
			if dw_edi_ship_dates.GetItemDate(1, "delv_from_date") > dw_edi_ship_dates.GetItemDate(1, "delv_to_date") then
				iw_frame.SetMicroHelp('Please enter a Delivery To Date greater than a Delivery From Date.')
				This.SetRedraw(False)
				dw_edi_ship_dates.SetColumn("delv_to_date")
				dw_edi_ship_dates.SetFocus()
				This.SetRedraw(True)
				Return
			end if
			iw_parent.Event ue_set_data('date_option', 'D')
			iw_parent.Event ue_Set_Data('from_date', ls_from_date)		
			iw_parent.Event ue_Set_Data('to_date', ls_to_date)
				iw_parent.Event ue_set_data('inquire_option', 'C')
		else
			iw_frame.SetMicroHelp('Ship or Delivery Date Option is required.')
			Return
		end if
	end if	
end if

if ls_sales_order_ind = 'Y' then
	ls_value = dw_edi_review_orders_inq.GetItemString(1, 'sales_order_id')
	If lu_string_functions.nf_isempty(ls_value) Then
		iw_frame.SetMicroHelp('Sales Order is a required field.')
		Return
	else
		iw_parent.Event ue_set_data('sales_order_id', ls_value)
		iw_parent.Event ue_set_data('inquire_option', 'O')
		iw_parent.Event ue_set_data('date_option', ' ')
		iw_parent.Event ue_set_data('customer_type', ' ')
		iw_parent.Event ue_set_data('customer_name', ' ')
		iw_parent.Event ue_set_data('customer_id', ' ')
		iw_parent.Event ue_set_data('div_code', ' ')
	End If	
end if


iw_parent.Event ue_set_data('line_option', ls_line_option)
   

ib_valid_return = True

CloseWithReturn(This, "OK")

end event

event ue_postopen;call super::ue_postopen;integer					li_rtn
string					ls_output, &
							ls_customer_ind, &
							ls_sales_order_ind, &
							ls_value, &
							ls_date, &
							ls_date_option, &
							ls_customer_type
							
u_string_functions	lu_string

DataWindowChild		ldwc_temp

dw_edi_review_orders_inq.AcceptText()


dw_edi_review_orders_inq.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_Temp)

dw_edi_review_orders_inq.GetChild('billto_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_billtos.ShareData(ldwc_Temp)

dw_edi_review_orders_inq.GetChild('corporate_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_corps.ShareData(ldwc_Temp)


SetMicroHelp("Ready")
//iw_parentwindow.Event ue_get_data("customer_ind")
//ls_customer_ind = message.stringparm
//iw_parentwindow.Event ue_get_data("sales_order_ind")
//ls_customer_ind = message.stringparm
//if ls_customer_ind = 'Y' and ls_sales_order_ind = 'Y' Then
//	iw_frame.setmicrohelp("Select either customer or sales order.")
//	return 
//end if
//	dw_edi_excp_detail_inq.setitem(1, "inq_on_excp", 'Y')
//	dw_edi_excp_detail_inq.object.excp_customer_id.Visible = true
//	dw_edi_excp_detail_inq.object.excp_customer_type.Visible = true
//else
//	dw_edi_excp_detail_inq.setitem(1, "inq_on_excp", 'N')
//	dw_edi_excp_detail_inq.object.excp_customer_id.Visible = false
//	dw_edi_excp_detail_inq.object.excp_customer_type.Visible = false
//end if

//iw_parent.Event ue_Get_Data('customer_id')
//ls_value = Message.StringParm
//If Not lu_string.nf_IsEmpty(ls_value) Then 
//	dw_edi_review_orders_inq.SetItem(1, "customer_id", ls_value)
//End If
//
iw_parent.Event ue_get_data('customer_type')
ls_customer_type = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_customer_type) Then 
	dw_edi_review_orders_inq.SetItem(1, "customer_type", ls_customer_type)
End If

iw_parent.Event ue_get_data('customer_id')
ls_value = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_value) Then 
	dw_edi_review_orders_inq.SetItem(1, "customer_id", ls_value)
End If

iw_parent.Event ue_get_data('customer_name')
ls_value = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_value) Then 
	dw_edi_review_orders_inq.SetItem(1, "customer_name", ls_value)
End If

iw_parent.Event ue_get_data('div_code')
ls_value = Message.StringParm
if ls_value = 'AL' then
	ls_value = '  '
end if
If Not lu_string.nf_IsEmpty(ls_value) Then 
	dw_edi_review_orders_inq.SetItem(1, "div_code", ls_value)
End If

iw_parent.Event ue_get_data('inquire_option')
ls_value = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_value) Then 
	if ls_value = 'C' then
		dw_edi_review_orders_inq.SetItem(1, "customer_ind", 'Y')
		dw_edi_review_orders_inq.SetItem(1, "sales_order_ind", 'N')
		dw_edi_ship_dates.visible = True
		if ls_customer_type = 'S' then
			dw_edi_review_orders_inq.Object.corporate_customer_id.Visible = False
			dw_edi_review_orders_inq.Object.billto_customer_id.Visible = False
			dw_edi_review_orders_inq.Object.customer_id.Visible = True
		end if
		if ls_customer_type = 'B' then
			dw_edi_review_orders_inq.Object.corporate_customer_id.Visible = False
			dw_edi_review_orders_inq.Object.billto_customer_id.Visible = True
			dw_edi_review_orders_inq.Object.customer_id.Visible = False
		end if
		if ls_customer_type = 'C' then
			dw_edi_review_orders_inq.Object.corporate_customer_id.Visible = True
			dw_edi_review_orders_inq.Object.billto_customer_id.Visible = False
			dw_edi_review_orders_inq.Object.customer_id.Visible = False
		end if
	else
		dw_edi_review_orders_inq.SetItem(1, "sales_order_ind", 'Y')
		dw_edi_review_orders_inq.SetItem(1, "customer_ind", 'N')
		dw_edi_ship_dates.visible = False
		dw_edi_review_orders_inq.Object.corporate_customer_id.Visible = 0
		dw_edi_review_orders_inq.Object.billto_customer_id.Visible = 0
		dw_edi_review_orders_inq.Object.customer_id.Visible = 0
	End If
end if

iw_parent.Event ue_get_data('sales_order_id')
ls_value = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_value) Then 
	dw_edi_review_orders_inq.SetItem(1, "sales_order_id", ls_value)
End If

iw_parent.Event ue_get_data('line_option')
ls_value = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_value) Then 
	dw_edi_order_line_choice.SetItem(1, "choice", ls_value)
End If

iw_parent.Event ue_get_data('date_option')
ls_date_option = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_date_option) Then 
	if ls_date_option = 'S' then
		dw_edi_ship_dates.SetItem(1, "ship_date_ind", 'Y')
		dw_edi_ship_dates.SetItem(1, "delv_date_ind", 'N')		
	else	
		if ls_date_option = 'D' then
			dw_edi_ship_dates.SetItem(1, "delv_date_ind", 'Y')
			dw_edi_ship_dates.SetItem(1, "ship_date_ind", 'N')
		End If
	end if
end if
	
//if ls_date_option = 'S' then
	iw_parent.Event ue_get_data('from_date')
	ls_date = Message.StringParm
	if Not lu_string.nf_IsEmpty(ls_date) or ls_date = '00/00/0000' Then 
		dw_edi_ship_dates.SetItem(1, "ship_from_date", date(ls_date))
	else
		dw_edi_ship_dates.SetItem(1, "ship_from_date", Today())
	End if
	iw_parent.Event ue_get_data('to_date')
	ls_date = Message.StringParm
	if Not lu_string.nf_IsEmpty(ls_date) Then 
		dw_edi_ship_dates.SetItem(1, "ship_to_date", date(ls_date))
	else
		dw_edi_ship_dates.SetItem(1, "ship_to_date", Today())	
	End if
//end if

//if ls_date_option = 'D' then
	iw_parent.Event ue_get_data('from_date')
	ls_date = Message.StringParm
	if Not lu_string.nf_IsEmpty(ls_date) Then 
		dw_edi_ship_dates.SetItem(1, "delv_from_date", date(ls_date))
	else
		dw_edi_ship_dates.SetItem(1, "delv_from_date", Today())	
	End if
	iw_parent.Event ue_get_data('to_date')
	ls_date = Message.StringParm
	if Not lu_string.nf_IsEmpty(ls_date) Then 
		dw_edi_ship_dates.SetItem(1, "delv_to_date", date(ls_date))
	else
		dw_edi_ship_dates.SetItem(1, "delv_to_date", Today())		
	End if
//end if

//dw_edi_review_orders_inq.SetFocus()
//dw_edi_review_orders_inq.setrow(1)
//dw_edi_review_orders_inq.SetColumn(1)
end event

event close;call super::close;//If Not ib_valid_return Then
//	Message.StringParm = ""
//Else
//	Message.StringParm = 'OK'
//End if
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_edi_review_orders_inq
boolean visible = false
integer x = 512
integer y = 1216
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_edi_review_orders_inq
integer x = 805
integer y = 1472
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_edi_review_orders_inq
integer x = 293
integer y = 1472
end type

type cb_browse from w_base_response_ext`cb_browse within w_edi_review_orders_inq
end type

type dw_edi_ship_dates from u_base_dw_ext within w_edi_review_orders_inq
integer x = 73
integer y = 992
integer width = 1207
integer height = 448
integer taborder = 11
boolean bringtotop = true
string title = "none"
string dataobject = "d_edi_from_to_dates"
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

event clicked;call super::clicked;Long		ll_clicked_row

String	ls_update_ind, &
			ls_plant_code, &
			ls_column_name

ll_clicked_row = row
If ll_clicked_row < 1 Then return

ls_column_name = dwo.name
if ls_column_name = 'ship_date_ind' then
	This.SetItem(1, 'delv_date_ind', 'N')
end if

if ls_column_name = 'delv_date_ind' then
	This.SetItem(1, 'ship_date_ind', 'N')
end if

end event

type dw_edi_order_line_choice from u_base_dw_ext within w_edi_review_orders_inq
integer x = 73
integer y = 660
integer width = 768
integer height = 288
integer taborder = 21
boolean bringtotop = true
string title = "none"
string dataobject = "d_edi_order_line_choice"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_edi_review_orders_inq from u_base_dw_ext within w_edi_review_orders_inq
integer x = 73
integer y = 32
integer width = 1317
integer height = 608
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_edi_orders_inq"
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;string ls_customer_type

Choose Case dwo.name
Case 'customer_type'
	CHOOSE CASE data
		CASE 'C'
			This.Setitem(1, 'corporate_customer_id', "")
			This.Object.corporate_customer_id.Visible = 1
			This.Object.billto_customer_id.Visible = 0
			This.Object.customer_id.Visible = 0
		CASE 'B'
			This.Setitem(1, 'billto_customer_id', "")
			This.Object.corporate_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 1
			This.Object.customer_id.Visible = 0
		CASE 'S'
			This.Setitem(1, 'customer_id', "")
			This.Object.corporate_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 0
			This.Object.customer_id.Visible = 1
	END CHOOSE
	
	CASE 'customer_id'
		IF TRIM(data) > '' THEN
//			SetItem(1, 'specific_orders_for_specific_customers', 'Y')
		ELSE
//			SetItem(1, 'specific_orders_for_specific_customers', 'N')
		END IF
	CASE 'sales_order_ind'	
		CHOOSE CASE data
			case 'Y'
				dw_edi_ship_dates.visible = False
				This.Object.corporate_customer_id.Visible = False
				This.Object.billto_customer_id.Visible = False
				This.Object.customer_id.Visible = False
			case 'N'
				dw_edi_ship_dates.visible = True
				ls_customer_type = dw_edi_review_orders_inq.GetItemString(1, 'customer_type')
				//iw_parent.Event ue_get_data('customer_type')
				if ls_customer_type = 'S' then 
					This.Object.corporate_customer_id.Visible = False
					This.Object.billto_customer_id.Visible = False
					This.Object.customer_id.Visible = True
					This.SetItem(1, 'customer_ind', 'Y')
				end if
				if ls_customer_type = 'B' then 
					This.Object.corporate_customer_id.Visible = False
					This.Object.billto_customer_id.Visible = True
					This.Object.customer_id.Visible = False
				end if
				if ls_customer_type = 'C' then 
					This.Object.corporate_customer_id.Visible = True
					This.Object.billto_customer_id.Visible = False
					This.Object.customer_id.Visible = False
				end if
		end choose
		

	CASE 'customer_ind'	
		CHOOSE CASE data
			case 'N'
				dw_edi_ship_dates.visible = False
				This.Object.corporate_customer_id.Visible = False
				This.Object.billto_customer_id.Visible = False
				This.Object.customer_id.Visible = False
				This.SetItem(1, 'sales_order_ind', 'Y')
			case 'Y'
				dw_edi_ship_dates.visible = True
				ls_customer_type = dw_edi_review_orders_inq.GetItemString(1, 'customer_type')
				//iw_parent.Event ue_get_data('customer_type')
				if ls_customer_type = 'S' then 
					This.Object.corporate_customer_id.Visible = False
					This.Object.billto_customer_id.Visible = False
					This.Object.customer_id.Visible = True
				end if
				if ls_customer_type = 'B' then 
					This.Object.corporate_customer_id.Visible = False
					This.Object.billto_customer_id.Visible = True
					This.Object.customer_id.Visible = False
				end if
				if ls_customer_type = 'C' then 
					This.Object.corporate_customer_id.Visible = True
					This.Object.billto_customer_id.Visible = False
					This.Object.customer_id.Visible = False
				end if
		end choose	
	END CHOOSE

end event

event constructor;call super::constructor;string 			ls_text

ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer","")

dw_edi_review_orders_inq.SetItem(1, "customer_id", ls_text)
idwc_customer.SetItem(1, "customer_id", ls_text)



end event

event clicked;call super::clicked;Long		ll_clicked_row

String	ls_update_ind, &
			ls_plant_code, &
			ls_column_name

ll_clicked_row = row
If ll_clicked_row < 1 Then return

ls_column_name = dwo.name
if ls_column_name = 'sales_order_ind' then
	This.SetItem(1, 'customer_ind', 'N')
	dw_edi_ship_dates.visible = False
//	iw_parent.object.dw_header.SetItemn(1, 'date_option', 'N')
end if

if ls_column_name = 'customer_ind' then
	This.SetItem(1, 'sales_order_ind', 'N')
	dw_edi_ship_dates.visible = True
end if

	
				
end event

