﻿$PBExportHeader$w_edi_assign_functions_detail.srw
$PBExportComments$Edi Customer Detail Prodcut Update
forward
global type w_edi_assign_functions_detail from w_base_sheet_ext
end type
type dw_scroll_to_cust from u_base_dw_ext within w_edi_assign_functions_detail
end type
type dw_detail from u_base_dw_ext within w_edi_assign_functions_detail
end type
type dw_header from u_base_dw_ext within w_edi_assign_functions_detail
end type
end forward

global type w_edi_assign_functions_detail from w_base_sheet_ext
integer width = 3173
integer height = 1792
string title = "Assign EDI Functions to Customer"
boolean maxbox = false
dw_scroll_to_cust dw_scroll_to_cust
dw_detail dw_detail
dw_header dw_header
end type
global w_edi_assign_functions_detail w_edi_assign_functions_detail

type variables

s_error		istr_error_info


u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

long		il_selected_rows, &
 			il_product_seq

INT		ii_row_count

String	is_customer_inq, &
			is_update_string, &
			is_columnname, &
			is_colname

Boolean  ib_modified = false, &
			ib_updating , &
			ib_reinquire, &
			ib_retrieve, &
			ib_updateable
			
DataStore ids_sales_person

end variables

forward prototypes
public function boolean wf_unstring_output (string as_output_string)
public subroutine wf_delete ()
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
end prototypes

public function boolean wf_unstring_output (string as_output_string);Date					ldt_start_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows, &
						ll_sequence_number
						

String				ls_cust_product, &
						ls_fm_product, &
						ls_customer_id, &
						ls_searchstring, &
						ls_system, &
						ls_start_date, &
						ls_output_string, &
						ls_sequence_number
integer				li_sequence_number
						

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_detail.RowCount()

dw_detail.SelectRow(0,False)

////find the row that matches the fm product, cust product and start date
//
//ll_nbrrows = dw_detail.RowCount()
//ls_sequence_number = string(lu_string_functions.nf_gettoken(as_output_string, '~t'))
//ls_system = lu_string_functions.nf_gettoken(as_output_string, '~t')
////ls_fm_product = lu_string_functions.nf_gettoken(ls_output_string, '~t')
////ls_cust_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
////ls_start_date = lu_string_functions.nf_gettoken(as_output_string, '~t')
////
//							
//ls_SearchString = 	"sequence_number = " + ls_sequence_number + &
//									" and system = '" + ls_system + "'"
////	
//ll_rtn = dw_detail.Find  &
//					( ls_SearchString, 1, ll_nbrrows)
////		
//If ll_rtn > 0 Then
//	dw_detail.SetRedraw(False)
//	dw_detail.ScrollToRow(ll_rtn)
//	dw_detail.SetRow(ll_rtn)
//	dw_detail.SelectRow(ll_rtn, True)
//	dw_detail.SetRedraw(True)
//End If
////
//ls_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public subroutine wf_delete ();long	ll_deleted_row

ll_deleted_row = dw_detail.getselectedrow(0)


DO WHILE ll_deleted_row > 0
	dw_detail.deleterow(ll_deleted_row)
	ll_deleted_row = dw_detail.getselectedrow(ll_deleted_row)
LOOP
end subroutine

public function boolean wf_update ();String	ls_input, &
			ls_update_string, &
			ls_output_string_in, &
			ls_output_string, &
			ls_header_string, &
			ls_rpc_detail, &
			ls_MicroHelp, &
			ls_customer_id, &
			ls_update_flag


Integer	li_rtn, &
			li_count
			
Long		ll_rtn
			
			
			
Long		ll_rowcount, &
			ll_pos, &
			ll_col, &
			ll_row, &
			ll_modrows, &
			ll_6th_row
			

char		lc_status_ind

Boolean	lb_error_found

u_string_functions	lu_string_functions

dw_detail.SetReDraw(False)

if dw_detail.AcceptText() = -1 then
	Return False
end if

SetPointer(HourGlass!)
dw_detail.SetRedraw(false)

is_update_string = ''
ll_rowcount = dw_detail.rowcount()
ll_modrows = dw_detail.modifiedcount()


ll_row = 0

for li_count = 1 to ll_modrows
ll_Row = dw_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then 
			dw_detail.SetRedraw(True) 
			Return False
		end if	
	END IF
next


ll_row = 0

for li_count = 1 to ll_rowcount
	ls_customer_id = dw_detail.GetItemString(li_count,"customer_id")
	IF ls_customer_id > ' ' THEN	
	else
		dw_detail.setitem(li_count, "update_flag", ' ')
	end if
next

ls_update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_detail)

if IsNull(ls_update_string) or Len(trim(ls_update_string)) = 0 then
	iw_frame.SetMicroHelp("No rows selected for Update")
	dw_detail.SetRedraw(True) 
	dw_detail.ResetUpdate()	
	return false
end if
	






istr_error_info.se_event_name = "wf_update"
//sap  istr_error_info.se_procedure_name = "u_orp201.orpo10br_edi_cust_prod_exc_upd"
istr_error_info.se_message = Space(71)


//
//	li_rtn =  iu_orp001.nf_orpo10br_edi_cust_prod_exc_upd (istr_error_info, & 
	//										ls_update_string ) 
											
		li_rtn =  iu_ws_orp1.nf_orpo10fr(istr_error_info, & 
											ls_update_string ) 										
											
	if li_rtn <> 0 then
//		if ls_output_string > '' Then
//			wf_unstring_output(ls_output_string)
//		End If
		dw_detail.SetRedraw(True)
		SetPointer(Arrow!)																		
		return false
	end if
//	if li_rtn <> 0 Then 
//		Return False 
//	End if
//

//Loop While Not lu_string_functions.nf_IsEmpty( ls_Update_string)



dw_detail.ResetUpdate()
ib_ReInquire = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")

dw_detail.SetFocus()
dw_detail.SetReDraw(True)


Return True

end function

public function boolean wf_validate (long al_row);int			li_rc				

long			ll_RowCount, &
				ll_find_row, &
				ll_sequence_number, &
				ll_rtn, &
				ll_nbrrows, &
				ll_current_row, &
				ll_delv_date_range

string		ls_temp, &
				ls_doc_id, &
				ls_delv_date_range, &
				ls_customer_id, &
				ls_searchstring, &
				ls_buyer_date_ind, &
				ls_tsr, &
				ls_customer_type
				
Boolean		lb_changed
				
ll_nbrrows = dw_detail.RowCount()
ll_current_row = al_row

ls_customer_id = dw_detail.getitemstring(al_row, "customer_id")
ls_customer_type = dw_detail.getitemstring(al_row, "customer_id")
if ls_customer_id = ' ' then
	iw_frame.SetMicroHelp(" Customer ID cannot be blank")
	dw_detail.SetRedraw(False)
	dw_detail.ScrollToRow(al_row)
	dw_detail.SetColumn("customer_id")
	dw_detail.SetRow(al_row)
	dw_detail.SelectRow(ll_rtn, True)
	dw_detail.SelectRow(al_row, True)
	dw_detail.SetRedraw(True)
Return False
End If

If ll_nbrrows > 1 Then
ls_SearchString = "customer_id = "
ls_Searchstring += "'" + ls_customer_id + "'"
ls_Searchstring += " and customer_type = " 
ls_Searchstring += "'" + ls_customer_type + "'"

CHOOSE CASE al_row 
		CASE 1
			ll_rtn = dw_detail.Find  &
					( ls_SearchString, al_row + 1, ll_nbrrows)
		CASE 2 to (ll_nbrrows - 1)
			ll_rtn = dw_detail.Find ( ls_SearchString, al_row - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_detail.Find  &
				(ls_SearchString, al_row + 1, ll_nbrrows)
		CASE ll_nbrrows 
			ll_rtn = dw_detail.Find ( ls_SearchString, al_row - 1, 1)
	END CHOOSE
	if ll_rtn > 0 then
		iw_frame.SetMicroHelp(" Customer and Type already exists for this Doc ID")
		dw_detail.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetColumn("customer_Id")
		dw_detail.SetRow(al_row)
		dw_detail.SelectRow(ll_rtn, True)
		dw_detail.SelectRow(al_row, True)
		dw_detail.SetRedraw(True)
		Return False
	end if
end if

ls_doc_id = dw_detail.GetItemString(al_row, "document_id")
if ls_doc_id = 'EDI850PO  ' then
	ls_delv_date_range = string(dw_detail.GetItemNumber(al_row, "delv_date_range"))
	ll_delv_date_range = dw_detail.GetItemNumber(al_row, "delv_date_range")
	if isNull(ls_delv_date_range) or ll_delv_date_range < 0 then
		iw_frame.SetMicroHelp(" Delivery Date Range cannot be less than 0")
		dw_detail.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetColumn("delv_date_range")
		dw_detail.SetRow(al_row)
		dw_detail.SelectRow(al_row, True)
		dw_detail.SetRedraw(True)
	Return False	
	end if

end if
//10-12 jac
	if ls_doc_id = 'EDI850BL  ' then
	ls_buyer_date_ind = string(dw_detail.GetItemString(al_row, "buyer_date_ind"))
//	ll_buyer_date_ind = dw_detail.GetItemString(al_row, "buyer_date_ind")
	if isNull(ls_buyer_date_ind) then
		iw_frame.SetMicroHelp(" Buyer Date Indicator cannot be spaces")
		dw_detail.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetColumn("buyer_date_ind")
		dw_detail.SetRow(al_row)
		dw_detail.SelectRow(al_row, True)
		dw_detail.SetRedraw(True)
	   Return False	
   end if
	ls_tsr = string(dw_detail.GetItemString(al_row, "tsr"))
		if isNull(ls_tsr) then
		iw_frame.SetMicroHelp(" TSR cannot be spaces")
		dw_detail.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetColumn("tsr")
		dw_detail.SetRow(al_row)
		dw_detail.SelectRow(al_row, True)
		dw_detail.SetRedraw(True)
	   Return False	
   end if
//	
end if

dw_detail.SelectRow(0, False)
	
Return True
end function

public function boolean wf_retrieve ();Boolean	lb_show_message
String		ls_input, &
				ls_output_string, &
				ls_row_option, &
				ls_filter, &
				ls_edi_doc_id, &
				ls_edi_doc_desc, &
				ls_customer_id
				
Integer		li_rtn

Long		ll_value


TriggerEvent("closequery")

ls_input = Message.StringParm

If Not ib_ReInquire Then
	OpenWithParm(w_edi_assign_functions_to_cust_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_edi_doc_id = dw_header.GetItemString( 1, "edi_doc_id")
If IsNull(ls_edi_doc_id) Then
	ls_edi_doc_id = ""
End If


ls_input =  'D' + '~t' + &
				'EDI' + '~t' + &
    			ls_edi_doc_id + '~r~n'
//			  
istr_error_info.se_function_name = "wf_retrieve"
//
dw_detail.Reset()
//li_rtn = iu_orp001.nf_orpo09br_edi_cust_prod_exc_inq (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp1.nf_orpo09fr(istr_error_info, ls_input, ls_output_string)
ib_retrieve = True
dw_detail.ImportString(ls_output_string)
dw_detail.ResetUpdate()

ll_value = dw_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if
SetMicroHelp(String(ll_value) + " rows retrieved")
IF ll_value > 0 THEN
	dw_detail.SetSort("customer_Id, tsr")
	dw_detail.Sort()
//	dw_detail.SetFocus()
	
END IF


ll_value = dw_detail.RowCount()
if ll_value > 0 then
	ls_customer_id = dw_detail.getitemstring(ll_value, "customer_id")
	if ls_customer_id > ' ' then
		wf_addrow()
	end if
else
	wf_addrow()
end if


dw_detail.ScrollToRow(ll_value + 1)
dw_detail.SetColumn( "customer_id" )
dw_detail.TriggerEvent("RowFocusChanged")

dw_detail.ResetUpdate()
dw_detail.SetReDraw(True)
This.SetRedraw(True)

Return TRUE
end function

public function boolean wf_addrow ();Long		ll_value
String	ls_doc_id

u_project_functions	lu_project_functions

If not lu_project_functions.nf_issalesmgr() Then
	return True
end if

ll_value = dw_detail.RowCount()
If ll_value < 0 Then 
	ll_value = 0
End if

ls_doc_id = dw_header.GetItemString(1, "edi_doc_id")
dw_detail.insertrow(0)
ll_value = ll_value + 1
dw_detail.SetItem(ll_value, "customer_id", ' ')
dw_detail.SetItem(ll_value, "update_flag", 'A')
dw_detail.SetItem(ll_value, "document_id", ls_doc_id)
dw_detail.SetItem(ll_value, "system_id", 'EDI')
dw_detail.SetItem(ll_value, "delv_date_range", 0)
//10-12 jac
dw_detail.SetItem(ll_value, "buyer_date_ind", 'S')
dw_detail.SetItem(ll_value, "full_load_ind", 'N')

//add option to bypass a reservation 
dw_detail.SetItem(ll_value, "bypass_res_ind", 'N')

//
dw_detail.SetColumn("customer_id")
//dw_detail.SetFocus()
dw_detail.uf_changerowstatus(ll_value, New!)

Return TRUE
end function

on w_edi_assign_functions_detail.create
int iCurrent
call super::create
this.dw_scroll_to_cust=create dw_scroll_to_cust
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scroll_to_cust
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_header
end on

on w_edi_assign_functions_detail.destroy
call super::destroy
destroy(this.dw_scroll_to_cust)
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event activate;call super::activate;
iw_frame.im_menu.mf_enable("m_deleterow")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")


u_project_functions	lu_project_functions

If lu_project_functions.nf_issalesmgr() Then
	iw_frame.im_menu.mf_enable("m_save")
	iw_frame.im_menu.mf_enable("m_delete")
Else
	iw_frame.im_menu.mf_disable("m_save")
	iw_frame.im_menu.mf_disable("m_delete")
End IF


end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
Destroy iu_ws_orp1

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")
end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")


end event

event ue_query;call super::ue_query;IF wf_Retrieve() = false Then
		This.SetRedraw( TRUE)
		CLOSE(THIS)
		RETURN
END IF
end event

event ue_set_data;call super::ue_set_data;choose case as_data_item
	case "edi_doc_id"
		dw_header.setItem(1, 'edi_doc_id', as_value)
	case "edi_doc_desc"
		dw_header.setItem(1, 'edi_doc_desc', as_value)
end choose
end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp1 = Create u_ws_orp1

dw_detail.InsertRow(0)
			






end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'edi_doc_id'
		message.StringParm = dw_header.getitemstring (1, "edi_doc_id")
	Case 'edi_doc_desc'
		message.StringParm = dw_header.getitemstring (1, "edi_doc_desc")
End choose
end event

type dw_scroll_to_cust from u_base_dw_ext within w_edi_assign_functions_detail
integer x = 37
integer y = 320
integer width = 1134
integer height = 96
integer taborder = 10
string dataobject = "d_edi_assign_scroll_customer"
boolean border = false
end type

event constructor;call super::constructor;dw_scroll_to_cust.InsertRow(0)

end event

event editchanged;call super::editchanged;Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
Integer 	li_counter


ll_row_count = dw_detail.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return 

Do While li_counter <= ll_Row_count 
	dw_detail.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop

if data > ' ' then
else
	return
end if

ll_row = dw_detail.GetSelectedRow(row)
if ll_row > 0 then
	dw_detail.SelectRow(ll_row, False)
end if
ll_row = dw_detail.Find("customer_id >= '" + data + "'",1,dw_detail.RowCount()+1)

If ll_row > 0 Then 
	dw_detail.SetRedraw(False)
	dw_detail.ScrollToRow(ll_row)
	dw_detail.SelectRow(ll_row, True)
	dw_detail.SetRow(ll_row + 1)
	dw_detail.SetRedraw(True)
End If

ll_first_row = Long(dw_detail.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_detail.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_detail.SetRedraw(False)
	dw_detail.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_detail.ScrollToRow(ll_row)
	dw_detail.SetRow(ll_row + 1)
	dw_detail.SetRedraw(True)
End If
end event

type dw_detail from u_base_dw_ext within w_edi_assign_functions_detail
event ue_set_hspscroll ( )
integer y = 448
integer width = 2743
integer height = 896
integer taborder = 20
string dataobject = "d_edi_assign_functions_detail"
boolean vscrollbar = true
boolean resizable = true
boolean border = false
boolean livescroll = true
end type

event ue_set_hspscroll();Int	li_position, &
		li_position2


if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	li_position = Integer(This.Object.system.X) + &
						Integer(This.Object.system.Width) + 15
	li_position2 = Integer(This.Object.system.X) + &
						Integer(This.Object.system.Width) + 15
	
	This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
	This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
end if
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus
int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row, &
				ll_boxes, ll_wgt, &
				ll_prod_avg_wgt, & 
			 	ll_sequence_number, &
				ll_avg_wgt, ll_prev_avg_wgt, &
				ll_nbrrows, &
				ll_rtn

string		ls_GetText, &
				ls_tem, ls_temp, &
				ls_searchstring, &
				ls_customer_name, &
				ls_customer_id, &
				ls_customer_type
				
Boolean		lb_changed
				
Date			ldt_temp	
//
is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0
ll_nbrrows = dw_detail.RowCount()
CHOOSE CASE is_ColName
	CASE "customer_id"
		ls_customer_type = This.GetItemString(row, "customer_type")
		CHOOSE CASE ls_customer_type
			CASE 'S'
				If ls_GetText > '  ' then
					SELECT customers.short_name
						INTO :ls_customer_name
						FROM customers
						WHERE customers.customer_id = :ls_GetText
						USING SQLCA ;
					IF isnull(ls_customer_name) THEN
						ls_customer_name = ' '
					END IF 
					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
				End if
			CASE 'B'
				If ls_GetText > '  ' then
					SELECT billtocust.name
						INTO :ls_customer_name
						FROM billtocust
						WHERE billtocust.bill_to_id = :ls_GetText
						USING SQLCA ;
					IF isnull(ls_customer_name) THEN
						ls_customer_name = ' '
					END IF 
					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
				End if	
			CASE 'C'
				If ls_GetText > '  ' then
					SELECT corpcust.name
						INTO :ls_customer_name
						FROM corpcust
						WHERE corpcust.corp_id = :ls_GetText
						USING SQLCA ;
					IF isnull(ls_customer_name) THEN
						ls_customer_name = ' '
					END IF 
					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
				End if				
				
		END CHOOSE		
		
	CASE "customer_type"
		ls_customer_type = data
		ls_customer_id = This.GetItemString(row, "customer_id")
		
		CHOOSE CASE ls_customer_type
			CASE 'S'
				If ls_GetText > '  ' then
					SELECT customers.short_name
						INTO :ls_customer_name
						FROM customers
						WHERE customers.customer_id = :ls_customer_id
						USING SQLCA ;
					IF isnull(ls_customer_name) THEN
						ls_customer_name = ' '
					END IF 
					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
				End if
			CASE 'B'
				If ls_GetText > '  ' then
					SELECT billtocust.name
						INTO :ls_customer_name
						FROM billtocust
						WHERE billtocust.bill_to_id = :ls_customer_id
						USING SQLCA ;
					IF isnull(ls_customer_name) THEN
						ls_customer_name = ' '
					END IF 
					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
				End if	
			CASE 'C'
				If ls_GetText > '  ' then
					SELECT corpcust.name
						INTO :ls_customer_name
						FROM corpcust
						WHERE corpcust.corp_id = :ls_customer_id
						USING SQLCA ;
					IF isnull(ls_customer_name) THEN
						ls_customer_name = ' '
					END IF 
					dw_detail.SetItem(ll_source_row, "customer_name", ls_customer_name)
				End if				
				
		END CHOOSE				
END CHOOSE
		   
//			iw_frame.SetMicroHelp("Sequnce Number must be a number")
//			this.setfocus( )
//			this.setcolumn("sequence_number") 
//			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
//			return 1
//		End if
//		if Integer(ls_GetText) < 0 then
//			iw_frame.SetMicroHelp("Sequnce Number must be a greater than zero")
//			this.setfocus( )
//			this.setcolumn("sequence_number") 
//			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
//			return 1
//		End if
//END CHOOSE
//
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
			This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF
////
parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
dw_detail.AcceptText()
ll_rowcount = dw_detail.RowCount()
ls_customer_id = dw_detail.getitemstring(ll_rowcount, "customer_id")
if ls_customer_id > ' ' then
	wf_addrow()
end if

//
return 0
end event

event constructor;call super::constructor;is_selection = '2'
ib_updateable = true
ib_NewRowOnChange = False

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild ldwc_temp

Long		ll_value
String	ls_customer_id

This.InsertRow(0)

This.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)



ids_sales_person = CREATE DATASTORE
ids_sales_person.DataObject = 'd_sales_people'
//ls_Location = message.is_smanlocation
ids_sales_person.SetTransObject(SQLCA)
ids_sales_person.Retrieve( '000', '999')

This.GetChild('tsr', ldwc_temp)
ids_sales_person.ShareData( ldwc_temp)


//Return TRUE

end event

event itemfocuschanged;call super::itemfocuschanged;if ib_retrieve Then Return 1
is_columnname = dwo.name

if is_columnname = 'customer_id' then
	This.SetText(Trim(this.GetText()))
	This.SelectText(1,len(trim(this.GetText())))
end if

end event

type dw_header from u_base_dw_ext within w_edi_assign_functions_detail
integer x = 37
integer width = 1682
integer height = 192
integer taborder = 0
string dataobject = "d_edi_assign_functions_detail_header"
boolean border = false
end type

event constructor;call super::constructor;dw_header.InsertRow(0)
end event

