﻿$PBExportHeader$w_edi_assign_functions_to_cust_inq.srw
forward
global type w_edi_assign_functions_to_cust_inq from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_edi_assign_functions_to_cust_inq
end type
type dw_edi_assign_functions_to_cust_inq from u_base_dw_ext within w_edi_assign_functions_to_cust_inq
end type
end forward

global type w_edi_assign_functions_to_cust_inq from w_base_response
integer width = 2432
integer height = 444
string title = "Assign Functions to Customer Inquire"
boolean controlmenu = false
long backcolor = 12632256
cb_browse cb_browse
dw_edi_assign_functions_to_cust_inq dw_edi_assign_functions_to_cust_inq
end type
global w_edi_assign_functions_to_cust_inq w_edi_assign_functions_to_cust_inq

type variables
w_base_sheet_ext		iw_parentwindow
w_base_sheet	iw_parent
String			is_openstring
Boolean	ib_valid_return
DataWindowChild	idwc_document

u_orp001		iu_orp001
u_ws_orp1		iu_ws_orp1

s_error	istr_error_info
end variables

on w_edi_assign_functions_to_cust_inq.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
this.dw_edi_assign_functions_to_cust_inq=create dw_edi_assign_functions_to_cust_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
this.Control[iCurrent+2]=this.dw_edi_assign_functions_to_cust_inq
end on

on w_edi_assign_functions_to_cust_inq.destroy
call super::destroy
destroy(this.cb_browse)
destroy(this.dw_edi_assign_functions_to_cust_inq)
end on

event open;call super::open;datawindowchild			ldwc_temp

								
String						ls_data
								
iw_parentwindow = Message.PowerObjectParm
ls_data = Message.StringParm

If dw_edi_assign_functions_to_cust_inq.ImportString(ls_data) < 1 Then 
	dw_edi_assign_functions_to_cust_inq.InsertRow(0)
End If
end event

event ue_base_ok;string 	ls_doc_id, &
			ls_doc_desc, &
			ls_value, &
			ls_temp, ls_desc
long  	ll_row
			
Date	ldt_temp

u_string_functions	lu_string_functions


dw_edi_assign_functions_to_cust_inq.getchild( "edi_doc_id", idwc_document)

dw_edi_assign_functions_to_cust_inq.AcceptText()

ls_value = dw_edi_assign_functions_to_cust_inq.GetItemString(1, 'edi_doc_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('Document Id is a required field.')
	Return
End If	


ll_row = idwc_document.find("edi_doc_id = '" +  ls_value + "'", 1, idwc_document.Rowcount() + 1)

ls_doc_id = dw_edi_assign_functions_to_cust_inq.getitemstring(1, 'edi_doc_id')
	

iw_parentwindow.Event ue_set_data('edi_doc_id', ls_doc_id)

ls_doc_desc = idwc_document.getitemstring(ll_row, 'edi_doc_desc')

iw_parentwindow.Event ue_set_data('edi_doc_desc', ls_doc_desc)
	
ib_valid_return = True

CloseWithReturn(This, "OK")

end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;
DataWindowChild	ldwc_document
Integer				li_rtn
String				ls_output_string, &
						ls_input_string, &
						ls_edi_doc_id

iu_orp001 = CREATE u_orp001
iu_ws_orp1 = Create u_ws_orp1

u_string_functions		lu_string

//Environment					le_env


istr_error_info.se_event_name = "wf_retrieve"	

ls_input_string =  'H' + '~t' + &
    					'EDI' + '~r~n'
						 
 //li_rtn = iu_orp001.nf_orpo09br_edi_cust_prod_exc_inq (istr_error_info, &
//															ls_input_string, &
//															ls_output_string)
						
 li_rtn = iu_ws_orp1.nf_orpo09fr(istr_error_info, &
											ls_input_string, &
											ls_output_string)						
										

dw_edi_assign_functions_to_cust_inq.getchild( "edi_doc_id", ldwc_document)
dw_edi_assign_functions_to_cust_inq.getchild( "edi_doc_desc", ldwc_document)
li_rtn = ldwc_document.importstring(ls_output_string)

iw_parentwindow.Event ue_get_data('edi_doc_id')
ls_edi_doc_id = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_edi_doc_id) Then 
	dw_edi_assign_functions_to_cust_inq.SetItem(1, "edi_doc_id", ls_edi_doc_id)
End If





end event

type cb_base_help from w_base_response`cb_base_help within w_edi_assign_functions_to_cust_inq
boolean visible = false
integer x = 992
integer y = 256
integer taborder = 40
boolean enabled = false
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_edi_assign_functions_to_cust_inq
integer x = 530
integer y = 176
integer taborder = 50
end type

type cb_base_ok from w_base_response`cb_base_ok within w_edi_assign_functions_to_cust_inq
integer x = 183
integer y = 176
integer taborder = 30
end type

type cb_browse from u_base_commandbutton_ext within w_edi_assign_functions_to_cust_inq
boolean visible = false
integer x = 2126
integer y = 396
integer height = 108
integer taborder = 10
string text = "&Browse"
end type

type dw_edi_assign_functions_to_cust_inq from u_base_dw_ext within w_edi_assign_functions_to_cust_inq
integer width = 2414
integer height = 128
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_edi_assign_function_to_cust_inq"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
String				ls_ColName, &
						ls_doc_id, &
						ls_doc_desc, &
						ls_text
Long					ll_row
u_string_functions		lu_string



ls_ColName = GetColumnName()
If ls_ColName = "edi_doc_id" Then
	ls_doc_id = data
	ll_row = idwc_document.find("edi_doc_id = '" +  ls_doc_id + "'", 1, idwc_document.Rowcount() + 1)

	if ll_row > 0 Then
		ls_doc_desc = idwc_document.getitemstring(ll_row, "edi_doc_desc")
//		SetProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer",ls_customer_id)
		dw_edi_assign_functions_to_cust_inq.setitem(1, "edi_doc_desc", ls_doc_desc)
	end if
end if




end event

event constructor;call super::constructor;string 			ls_text

ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastedicustomer","")



end event

event itemerror;call super::itemerror;Return 1
end event

