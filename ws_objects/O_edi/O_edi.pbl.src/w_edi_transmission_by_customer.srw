﻿$PBExportHeader$w_edi_transmission_by_customer.srw
forward
global type w_edi_transmission_by_customer from w_base_sheet_ext
end type
type dw_error_select from u_base_dw_ext within w_edi_transmission_by_customer
end type
type dw_header from u_base_dw_ext within w_edi_transmission_by_customer
end type
type dw_detail from u_base_dw_ext within w_edi_transmission_by_customer
end type
end forward

global type w_edi_transmission_by_customer from w_base_sheet_ext
integer width = 2999
integer height = 1860
string title = "EDI Transmission by Customer"
dw_error_select dw_error_select
dw_header dw_header
dw_detail dw_detail
end type
global w_edi_transmission_by_customer w_edi_transmission_by_customer

type variables
Boolean  	ib_reinquire

s_error		istr_error_info

String		is_selected_data

u_ws_orp3	iu_ws_orp3
end variables

forward prototypes
public subroutine wf_filter ()
public subroutine wf_resend ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_filter ();dw_error_select.AcceptText()

If dw_error_select.GetItemString(1, "error_ind") = "A" Then
	dw_detail.SetFilter('')
Else
	dw_detail.SetFilter("transaction_status = 'ERROR'")
End If

dw_detail.Filter()

end subroutine

public subroutine wf_resend ();String	ls_header_string, &
			ls_detail_string
			
Integer	li_rtn			
			
ls_header_string = dw_header.Describe("DataWindow.Data")

ls_detail_string = is_selected_data

//li_rtn = iu_orp003.nf_orpo12cr_edi_trans_by_cust_upd(istr_error_info, &
//		ls_header_string, ls_detail_string)

li_rtn = iu_ws_orp3.uf_orpo12gr_edi_trans_by_cust_upd(istr_error_info, &
		ls_header_string, ls_detail_string)
end subroutine

public function boolean wf_retrieve ();String	ls_header_string_in, &
			ls_header_string_out, &
			ls_detail_string, &
			ls_temp
			
Integer	li_rtn			

TriggerEvent("closequery")

If Not ib_ReInquire Then
	OpenWithParm(w_edi_transmission_by_customer_inq, This)
Else
	ib_ReInquire = False
End if

If Message.StringParm = "Cancel" Then Return False

ls_header_string_in = dw_header.Describe("DataWindow.Data")

//li_rtn = iu_orp003.nf_orpo11cr_edi_trans_by_cust_inq(istr_error_info, &
//		ls_header_string_in, ls_header_string_out, ls_detail_string) 

li_rtn = iu_ws_orp3.uf_orpo11gr_edi_trans_by_cust_inq(istr_error_info, &
		ls_header_string_in, ls_header_string_out, ls_detail_string) 

If li_rtn = 0 Then
	dw_header.Reset()
	dw_header.ImportString(ls_header_string_out)
	ls_temp = dw_header.GetItemString(1, "customer_name")
	If mid(dw_header.GetItemString(1, "customer_name"),1,3) = 'ALL' Then
		dw_header.SetItem(1, "customer_id", 'ALL')
	End If
	dw_detail.Reset()
	dw_detail.ImportString(ls_detail_string)

	If dw_error_select.GetItemString(1, "error_ind") = "A" Then
		dw_detail.SetFilter('')
	Else
		dw_detail.SetFilter("transaction_status = 'ERROR'")
	End If	
	dw_detail.Filter()
End If

Return True
end function

on w_edi_transmission_by_customer.create
int iCurrent
call super::create
this.dw_error_select=create dw_error_select
this.dw_header=create dw_header
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_error_select
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.dw_detail
end on

on w_edi_transmission_by_customer.destroy
call super::destroy
destroy(this.dw_error_select)
destroy(this.dw_header)
destroy(this.dw_detail)
end on

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")
end event

event ue_query;call super::ue_query;wf_Retrieve()
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'customer_id'
		message.StringParm = dw_header.getitemstring (1, "customer_id")		
	Case 'from_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'from_date'), 'mm/dd/yyyy')
	Case 'to_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'to_date'), 'mm/dd/yyyy')	
	Case 'transmission_type'
		message.StringParm = dw_header.getitemstring (1, "transmission_type")	
End choose
end event

event open;call super::open;iu_orp003 = Create u_orp003
iu_ws_orp3 = Create u_ws_orp3
end event

event close;call super::close;If IsValid(iu_orp003) Then Destroy iu_orp003
If IsValid(iu_ws_orp3) Then Destroy iu_ws_orp3
end event

event ue_set_data;call super::ue_set_data;choose case as_data_item
	case "customer_id"
		dw_header.setItem(1, 'customer_id', as_value)
	case "customer_type"
		dw_header.setItem(1, 'customer_type', as_value)	
	case "customer_name"
		dw_header.setItem(1, 'customer_name', as_value)		
	Case 'from_date'
		dw_header.SetItem(1, 'from_date', Date(as_value))
	Case 'to_date'
		dw_header.SetItem(1, 'to_date', Date(as_value))
	Case 'transmission_type'
		dw_header.SetItem(1, 'transmission_type', as_value)
end choose
end event

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_delete")
iw_frame.im_menu.mf_enable("m_deleterow")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")
end event

type dw_error_select from u_base_dw_ext within w_edi_transmission_by_customer
integer x = 2377
integer width = 549
integer height = 224
integer taborder = 20
string dataobject = "d_edi_transmission_by_cust_error_select"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;If data = "A" Then
	dw_detail.SetFilter('')
Else
	dw_detail.SetFilter("transaction_status = 'ERROR'")
End If	
dw_detail.Filter()
end event

type dw_header from u_base_dw_ext within w_edi_transmission_by_customer
integer x = 37
integer y = 32
integer width = 2377
integer height = 320
integer taborder = 10
string dataobject = "d_edi_transmission_by_cust_header"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_detail from u_base_dw_ext within w_edi_transmission_by_customer
integer x = 37
integer y = 384
integer width = 2853
integer height = 1312
integer taborder = 10
string dataobject = "d_edi_transmission_by_cust_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event constructor;call super::constructor;is_selection= "2"
end event

event doubleclicked;call super::doubleclicked;String	ls_so_id, &
			ls_transmission_type

Window	lw_detail

u_string_functions	lu_string_functions

If dwo.name = 'document_id' Then
	ls_transmission_type = Trim(This.GetItemString(row, "transmission_type"))
	If (ls_transmission_type = 'ORDER CONF') or (ls_transmission_type = 'ASN') Then
		SetPointer(HourGlass!)
		ls_so_id = mid(This.GetItemString( Row, 'document_id'), 1, 7) 
		If lu_string_functions.nf_IsEmpty(ls_so_id) Then return
		OpenSheetWithParm(lw_detail, ls_so_id, 'w_sales_order_detail', &
								iw_frame, 0, iw_Frame.im_menu.iao_ArrangeOpen)
	End If
End if
end event

event clicked;call super::clicked;If row < 1 Then Return

If (This.GetItemString(row, "transmission_type") = 'EDI CONF') or &
		(This.GetItemString(row, "transmission_type") = 'ASN') Then
	This.SelectRow (row, NOT This.IsSelected( row))
End If
end event

event rbuttondown;call super::rbuttondown;DataStore	lds_selected

m_resend_edi_popup NewMenu

If row < 1 Then Return

lds_selected	=	Create DataStore
lds_selected.DataObject		=	'd_edi_transmission_by_cust_detail'

If (This.GetItemString(row, "transmission_type") = 'EDI CONF') or &
		(This.GetItemString(row, "transmission_type") = 'ASN') Then
	This.SelectRow (row, TRUE)
End If

If This.GetSelectedRow(0) > 0 Then
	lds_selected.Object.Data =	This.Object.Data.Selected
Else
	Return	
End If

is_selected_data = lds_selected.Describe("Datawindow.Data")

NewMenu = CREATE m_resend_edi_popup

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu

end event

