﻿$PBExportHeader$orpo03sr_programinterface.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterface from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterface from nonvisualobject
end type

type variables
    orpo03sr_ProgramInterfaceOrpo03ci orpo03ci
    orpo03sr_ProgramInterfaceOrpo03pg orpo03pg
    orpo03sr_ProgramInterfaceOrpo03in orpo03in
    orpo03sr_ProgramInterfaceOrpo03ot orpo03ot
    boolean channel
end variables

on orpo03sr_ProgramInterface.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterface.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

