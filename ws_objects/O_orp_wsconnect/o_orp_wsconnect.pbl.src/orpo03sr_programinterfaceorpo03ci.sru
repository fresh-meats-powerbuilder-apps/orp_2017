﻿$PBExportHeader$orpo03sr_programinterfaceorpo03ci.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03ci from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03ci from nonvisualobject
end type

type variables
    orpo03sr_ProgramInterfaceOrpo03ciOrp000sr_cics_container orp000sr_cics_container
    boolean structuredContainer
end variables

on orpo03sr_ProgramInterfaceOrpo03ci.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03ci.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

