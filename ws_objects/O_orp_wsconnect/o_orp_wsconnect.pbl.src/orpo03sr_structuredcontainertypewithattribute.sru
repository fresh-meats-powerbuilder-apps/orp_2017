﻿$PBExportHeader$orpo03sr_structuredcontainertypewithattribute.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_structuredContainerTypeWithAttribute from nonvisualobject
    end type
end forward

global type orpo03sr_structuredContainerTypeWithAttribute from nonvisualobject
end type

type variables
    boolean structuredContainer
end variables

on orpo03sr_structuredContainerTypeWithAttribute.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_structuredContainerTypeWithAttribute.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

