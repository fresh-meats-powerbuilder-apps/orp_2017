﻿$PBExportHeader$orpo03sr_programinterfaceorpo03ot1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03ot1 from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03ot1 from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on orpo03sr_ProgramInterfaceOrpo03ot1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03ot1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

