﻿$PBExportHeader$orpo03sr_programinterfaceorpo03ciorp000sr_cics_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03ciOrp000sr_cics_container from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03ciOrp000sr_cics_container from nonvisualobject
end type

type variables
    string orp000sr_req_tranid
    string orp000sr_req_program
    string orp000sr_req_userid
    string orp000sr_req_password
end variables

on orpo03sr_ProgramInterfaceOrpo03ciOrp000sr_cics_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03ciOrp000sr_cics_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

