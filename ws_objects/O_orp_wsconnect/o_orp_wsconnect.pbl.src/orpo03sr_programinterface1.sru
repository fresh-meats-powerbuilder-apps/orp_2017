﻿$PBExportHeader$orpo03sr_programinterface1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterface1 from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterface1 from nonvisualobject
end type

type variables
    orpo03sr_ProgramInterfaceOrpo03ci1 orpo03ci
    orpo03sr_ProgramInterfaceOrpo03pg1 orpo03pg
    orpo03sr_ProgramInterfaceOrpo03in1 orpo03in
    orpo03sr_ProgramInterfaceOrpo03ot1 orpo03ot
    boolean channel
end variables

on orpo03sr_ProgramInterface1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterface1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

