﻿$PBExportHeader$smas01sr_programinterfacesmas01cisma000sr_cics_container1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterfaceSmas01ciSma000sr_cics_container1 from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterfaceSmas01ciSma000sr_cics_container1 from nonvisualobject
end type

type variables
    string sma000sr_req_tranid
    string sma000sr_req_program
    string sma000sr_req_userid
    string sma000sr_req_password
end variables

on smas01sr_ProgramInterfaceSmas01ciSma000sr_cics_container1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterfaceSmas01ciSma000sr_cics_container1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

