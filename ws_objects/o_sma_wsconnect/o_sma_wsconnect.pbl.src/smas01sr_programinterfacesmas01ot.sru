﻿$PBExportHeader$smas01sr_programinterfacesmas01ot.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type smas01sr_ProgramInterfaceSmas01ot from nonvisualobject
    end type
end forward

global type smas01sr_ProgramInterfaceSmas01ot from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on smas01sr_ProgramInterfaceSmas01ot.create
call super::create
TriggerEvent( this, "constructor" )
end on

on smas01sr_ProgramInterfaceSmas01ot.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

