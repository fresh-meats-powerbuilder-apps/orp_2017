﻿$PBExportHeader$u_plant.sru
$PBExportComments$Plant user object that passes retrieval arguments to the datawindows.
forward
global type u_plant from u_netwise_dw
end type
end forward

global type u_plant from u_netwise_dw
int Width=1504
int Height=88
string DataObject="d_pas_plant_local_retrieval"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean LiveScroll=true
event ue_revisions ( )
end type
global u_plant u_plant

type variables
datawindowchild idddw_child

Boolean	ib_protected = FALSE
end variables

forward prototypes
public function string nf_get_plant_code ()
public function string uf_get_plant_descr ()
public function integer uf_set_plant_code (string as_plant_code)
public function string uf_get_plant_code ()
public subroutine disable ()
public subroutine enable ()
end prototypes

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: Rev#01
**   PROJECT NUMBER:  Support
**   DATE:				 January 2000            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Needed to change to only populate with the plants based on there GroupID
**							 added Group*** in tutltype, changed this object to use two new datawindows
**                    The control being d_pas_plant_local_retrieval, and the child datawindow being
**                    d_pas_u_plant_dddw_local_retrieval and changed the constructer to pass							
**                    the GroupId needed. 
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public function string nf_get_plant_code ();return This.GetItemString(1, "location_code")
end function

public function string uf_get_plant_descr ();return Trim(This.GetItemString(1, "location_name"))
end function

public function integer uf_set_plant_code (string as_plant_code);Long	ll_row, &
		ll_row_count

String	ls_text

This.GetChild("location_code",idddw_child)

ls_text = 'Trim(location_code) = "' + as_plant_code + '"'
ll_row_count = idddw_child.RowCount()
ll_row = idddw_child.Find(ls_text, 1, ll_row_count + 1)
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(Trim(as_plant_code) + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(as_plant_code))
	return 1
Else
	This.SetItem(1,"location_code",as_plant_code)
	This.SetItem(1,"location_name",idddw_child.GetItemString(ll_row,"location_name"))
	This.SetFocus()
	This.SelectText(1, Len(as_plant_code))
	iw_frame.SetMicroHelp("Ready")
End if

return 0
end function

public function string uf_get_plant_code ();return This.GetItemString(1, "location_code")
end function

public subroutine disable ();This.Modify("location_code.Background.Color = 12632256 location_code.Protect = 1 " + &
				"location_code.Pointer = 'Arrow!'")

end subroutine

public subroutine enable ();This.Modify("location_code.Background.Color = 16777215 location_code.Protect = 0 " + &
				"location_code.Pointer = 'Beam!'")

end subroutine

event itemchanged;call super::itemchanged;Long	ll_row

If Len(Trim(data)) = 0 Then 
	// User entered an empty plant code -- set fields to blank
	// The last row is an empty row
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	This.ScrollToRow(This.RowCount())
	This.SetItem(1,"location_code","")
	This.SetItem(1,"location_name","")
	return 
End if

// Find the entered plant code in the table -- don't count the last row, it's blank
ll_row = idddw_child.Find('location_code = "' + Trim(data) + '"', 1, idddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(data + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
Else
	This.SetItem(1, 'location_name', idddw_child.GetItemString(ll_row, 'location_name'))
	This.SetFocus()
	This.SelectText(1, Len(data))
	SetProfileString( iw_frame.is_UserINI, "Pas", "Lastplant",Trim(data))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

return 
end event

event getfocus;call super::getfocus;If This.GetColumnName() = "location_code" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

event constructor;call super::constructor;//REV#01 added ls_group_id to pass to the retrieve
nvuo_pa_business_rules 	nvuo_pa
String 						ls_group_id

ls_group_id = 'GROUP' + iw_frame.iu_netwise_data.is_groupid 
//Rev#01 check if there are any user groups
nvuo_pa.uf_check_user_group(ls_group_id)

ib_updateable = False

This.GetChild('location_code', idddw_child)
idddw_child.SetTransObject(SQLCA)
idddw_child.Retrieve(ls_group_id)
// Add a blank row at the end of the table
idddw_child.InsertRow(0)

String ls_text

This.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastplant","")
This.SetItem( 1, "location_code", ls_text)

Long ll_row
ll_row = idddw_child.Find('location_code = "' + Trim(ls_text) + '"', 1, idddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(ls_text + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
Else
	This.SetItem(1, 'location_name', idddw_child.GetItemString(ll_row, 'location_name'))
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

end event

event itemerror;call super::itemerror;Return 1
end event

event ue_postconstructor;call super::ue_postconstructor;If ib_protected Then
	This.Modify("location_code.Protect='1' location_code.Background.Color='12632256'")
End If
end event

