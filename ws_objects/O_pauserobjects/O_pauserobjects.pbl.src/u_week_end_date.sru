﻿$PBExportHeader$u_week_end_date.sru
forward
global type u_week_end_date from datawindow
end type
end forward

global type u_week_end_date from datawindow
int Width=1042
int Height=100
int TabOrder=1
string DataObject="d_week_end_date"
boolean Border=false
boolean LiveScroll=true
event ue_postconstructor ( )
end type
global u_week_end_date u_week_end_date

type variables
String		Is_week_end_type
end variables

forward prototypes
public function long uf_enable (boolean ab_enable)
public function long uf_set_week_end_date (date adt_week_end_date)
public function integer uf_modified ()
public function date uf_get_week_end_date ()
public subroutine uf_set_week_end_type (string as_week_end_type)
public function date uf_get_last_week_end_date ()
public subroutine uf_weekly_pa_type_format (ref string as_first_week_end_date, ref integer ai_week_end_dates_to_display)
public subroutine uf_daily_pa_type_format (ref string as_first_week_end_date, ref integer ai_week_end_dates_to_display)
end prototypes

event ue_postconstructor;Integer					li_count, &
							li_rc, &
							li_number_of_dates
							
DataWindowChild		ldwc_week_end

Date						ldt_week_end[]

String					ls_week_end

// WEEK END TYPE IS USED TO DETERMINE THE RIGHT FORMAT FOR THIS USER OBJECT
// IF IT IS EQUAL TO 'W' THE FORMAT WILL BE LIKE WEEKLY PA REPORT ELSE
// IT WILL WORK LIKE WEEKLY SOURCE(WHICH AT THIS TIME DOESN'T SEND TYPE).

CHOOSE CASE is_week_end_type
	CASE  "W"
		uf_weekly_pa_type_format(ls_week_end,li_Number_of_dates)
	CASE ELSE
		uf_daily_pa_type_format(ls_week_end,li_Number_of_dates)
END CHOOSE

ldt_week_end[1] = date(ls_week_end)

For li_count = 2 to li_number_of_dates
	ldt_week_end[li_count] = RelativeDate(ldt_week_end[li_count - 1], 7)
	ls_week_end += "~r~n" + String(ldt_week_end[li_count], "mm/dd/yyyy")
Next

li_rc	=	This.GetChild( "week_end_date", ldwc_week_end )

ldwc_week_end.Reset()
li_rc = ldwc_week_end.ImportString(ls_week_end)

end event

public function long uf_enable (boolean ab_enable);If ab_enable Then
	This.object.week_end_date.Background.Color = 16777215
	This.object.week_end_date.Protect = 0
Else
	This.object.week_end_date.Background.Color = 12632256
	This.object.week_end_date.Protect = 1
End If

Return 1
end function

public function long uf_set_week_end_date (date adt_week_end_date);Long	ll_row, &
		ll_row_count

String	ls_text

This.SetItem(1,"week_end_date",adt_week_end_date)
This.SetFocus()
This.SelectText(1, Len(String(adt_week_end_date)) + 5)

return 0
end function

public function integer uf_modified ();Return 0
end function

public function date uf_get_week_end_date ();Date				ldt_week_end_date

If This.AcceptText() = -1 then 
	This.SetFocus()
	SetNull(ldt_week_end_date)
	return ldt_week_end_date
End if

ldt_week_end_date = This.GetItemDate(1, 'week_end_date')

Return ldt_week_end_date
end function

public subroutine uf_set_week_end_type (string as_week_end_type);is_week_end_type = as_week_end_type
end subroutine

public function date uf_get_last_week_end_date ();DataWindowChild ldwc_week_end_child


This.GetChild ('week_end_date', ldwc_week_end_child)

Return ldwc_week_end_child.GetItemDate(ldwc_week_end_child.rowcount(), 'start_date')



end function

public subroutine uf_weekly_pa_type_format (ref string as_first_week_end_date, ref integer ai_week_end_dates_to_display);// THIS FORMAT IS USED FOR THE CURRENT WEEEKLY PA WINDOW.

Integer			li_number_of_dates, &
					li_day_num

String			ls_PARange, &
					ls_week_end

Date				ldt_week_end



	li_day_num = DayNumber(Today())
	IF li_day_num = 1 Then                           
		ldt_week_end = Today()
	else
		ldt_week_end = RelativeDate(Today(), 8 - li_day_num)
	end If	
 	as_first_week_end_date += String(ldt_week_end, "mm/dd/yyyy")


	 SELECT tutltypes.type_short_desc  
    INTO :ls_parange  
    FROM tutltypes  
   WHERE ( tutltypes.type_code = 'PA RANGE' ) AND  
         ( tutltypes.record_type = 'PA RANGE' )   ;
   ai_week_end_dates_to_display += (Integer(ls_PARange) / 7) 
		
   SELECT tutltypes.type_short_desc  
    INTO :ls_parange  
    FROM tutltypes  
   WHERE ( tutltypes.type_code = 'Weekly' ) AND  
         ( tutltypes.record_type = 'PA RANGE' )  ;
   ai_week_end_dates_to_display += (Integer(ls_parange))			

end subroutine

public subroutine uf_daily_pa_type_format (ref string as_first_week_end_date, ref integer ai_week_end_dates_to_display);// THIS FORMAT IS USED FOR THE CURRENT WEEKLY SOURCE WINDOW

Integer			li_day_num
					
String			ls_PARange
					
Date				ldt_week_end


	li_day_num = DayNumber(Today())
	
	IF li_day_num = 1 Then                           
		ldt_week_end = RelativeDate(Today(), - 7 ) 
	else
		ldt_week_end = RelativeDate(Today(), 1 - DayNumber(Today()))
	end If
	
	as_first_week_end_date += String(ldt_week_end, "mm/dd/yyyy")


	 SELECT tutltypes.type_short_desc  
    INTO :ls_parange  
    FROM tutltypes  
   WHERE ( tutltypes.type_code = 'PA RANGE' ) AND  
         ( tutltypes.record_type = 'PA RANGE' )   ;
   ai_week_end_dates_to_display += (Integer(ls_PARange) / 7 + 2) 
		

end subroutine

event constructor;This.Event Post ue_postconstructor()
end event

