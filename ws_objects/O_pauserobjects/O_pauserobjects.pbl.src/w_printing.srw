﻿$PBExportHeader$w_printing.srw
forward
global type w_printing from Window
end type
type cb_cancel from commandbutton within w_printing
end type
type st_page_page from statictext within w_printing
end type
type st_2 from statictext within w_printing
end type
type st_page_max from statictext within w_printing
end type
type st_1 from statictext within w_printing
end type
end forward

global type w_printing from Window
int X=823
int Y=360
int Width=1472
int Height=368
boolean TitleBar=true
string Title="Printing . . ."
long BackColor=12632256
WindowType WindowType=popup!
event ue_setdata ( string as_dataitem,  string as_datavalue )
event ue_getdata ( string as_dataitem,  ref string as_datavalue )
cb_cancel cb_cancel
st_page_page st_page_page
st_2 st_2
st_page_max st_page_max
st_1 st_1
end type
global w_printing w_printing

type variables
String			is_cancel
end variables

event ue_setdata;Choose Case as_dataitem
	Case 'pagesmax'
		st_page_max.text = as_datavalue
	Case 'pagenumber'
		st_page_page.text = as_datavalue
End Choose

end event

event ue_getdata;Choose Case as_dataitem
	Case 'cancel'
		as_datavalue = is_cancel
End Choose
end event

on w_printing.create
this.cb_cancel=create cb_cancel
this.st_page_page=create st_page_page
this.st_2=create st_2
this.st_page_max=create st_page_max
this.st_1=create st_1
this.Control[]={this.cb_cancel,&
this.st_page_page,&
this.st_2,&
this.st_page_max,&
this.st_1}
end on

on w_printing.destroy
destroy(this.cb_cancel)
destroy(this.st_page_page)
destroy(this.st_2)
destroy(this.st_page_max)
destroy(this.st_1)
end on

event open;String						ls_printer_name

//u_print_functions			lu_print
//
//
//lu_print = Create u_print_functions
//
//ls_printer_name = lu_print.uf_get_default_printer()
//This.Title = 'Print:  ' + ls_printer_name

is_cancel = 'no'
end event

type cb_cancel from commandbutton within w_printing
int X=603
int Y=152
int Width=242
int Height=100
int TabOrder=10
string Text="Cancel"
boolean Default=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;is_cancel = 'yes'
end event

type st_page_page from statictext within w_printing
int X=690
int Y=32
int Width=114
int Height=68
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_printing
int X=805
int Y=32
int Width=78
int Height=68
boolean Enabled=false
string Text="of"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_page_max from statictext within w_printing
int X=896
int Y=32
int Width=114
int Height=68
boolean Enabled=false
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_printing
int X=215
int Y=32
int Width=480
int Height=68
boolean Enabled=false
string Text="Preparing page "
Alignment Alignment=Right!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

