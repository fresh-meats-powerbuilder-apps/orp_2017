﻿$PBExportHeader$u_plant_type.sru
forward
global type u_plant_type from datawindow
end type
end forward

global type u_plant_type from datawindow
int Width=1326
int Height=89
int TabOrder=1
string DataObject="d_plant_type_inq"
boolean Border=false
end type
global u_plant_type u_plant_type

forward prototypes
public function long uf_set_plant_type (String as_plant_type)
public function long uf_enable (boolean ab_enable)
public function long uf_modified ()
public function string uf_get_plant_type ()
public function datawindowchild uf_get_child ()
public function string uf_get_desc ()
end prototypes

public function long uf_set_plant_type (String as_plant_type);Long	ll_row, &
		ll_row_count

String	ls_text

This.SetItem(1,"Plant_type",as_plant_type)
This.SetFocus()
This.SelectText(1, Len(as_plant_type) + 5)

return 0
end function

public function long uf_enable (boolean ab_enable);If ab_enable Then
	This.object.plant_type.Background.Color = 16777215
	This.object.plant_type.Protect = 0
Else
	This.object.plant_type.Background.Color = 12632256
	This.object.plant_type.Protect = 1
End If

Return 1
end function

public function long uf_modified ();Return 0
end function

public function string uf_get_plant_type ();String			 			ls_plant_type

u_string_functions		lu_string


If This.AcceptText() = -1 then 
	This.SetFocus()
	return ''
End if

ls_plant_type = This.GetItemString(1, 'plant_type')

If lu_string.nf_IsEmpty(ls_plant_type) Then
	ls_plant_type = ''
End if

Return ls_plant_type

end function

public function datawindowchild uf_get_child ();DataWindowChild		ldwc_temp


This.GetChild('plant_type', ldwc_temp)

Return ldwc_temp
end function

public function string uf_get_desc ();Datawindowchild			ldwc_temp

String						ls_plant_type

Long							ll_findrow

u_string_functions		lu_string


ls_plant_type = This.GetItemString(1, 'plant_type')

If lu_string.nf_IsEmpty(ls_plant_type) Then Return ''

This.GetChild('plant_type', ldwc_temp)
ll_findrow = ldwc_temp.Find('type_short_desc = "' + ls_plant_type + '"', 1, ldwc_temp.RowCount())
If ll_findrow <= 0 Then Return ''

Return ldwc_temp.GetItemString(ll_findrow, 'type_desc')

end function

event constructor;DataWindowChild		ldwc_type


This.GetChild("plant_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PLTGROUP")

end event

