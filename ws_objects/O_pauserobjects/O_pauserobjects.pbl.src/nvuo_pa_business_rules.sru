﻿$PBExportHeader$nvuo_pa_business_rules.sru
$PBExportComments$NonVisual Pa User Object
forward
global type nvuo_pa_business_rules from nonvisualobject
end type
end forward

global type nvuo_pa_business_rules from nonvisualobject autoinstantiate
end type

type variables
/*This code needs to be here for regeneration of this object
for now until I get this fixed.  Remove the code after
regeneration*/
//w_netwise_frame	iw_frame
end variables

forward prototypes
public function boolean uf_check_pa_date (date adt_date, ref string as_message_date)
public function integer uf_check_pa_daily_range ()
public function time uf_check_min_shift_start_time (string as_shift)
public function long uf_check_5_char_time (ref string as_time)
public function long uf_check_max_chain_speed ()
public subroutine uf_check_user_group (ref string as_user_group)
public function boolean uf_check_pasldtyp (string as_plant)
public subroutine uf_retrieve_plan_tran_options (ref string as_options)
public function integer uf_check_parange_dayofinv ()
public function boolean uf_check_parange_dayofinv_date (date adt_process_date, date adt_production_date, ref string as_message_date)
public subroutine uf_retrieve_periods (ref string as_periods)
public function integer uf_check_number_of_weeks_out ()
public function long uf_check_pa_date_ext (date adt_date, ref string as_message_date)
public function boolean uf_check_security (string as_window_name, string as_rights)
end prototypes

public function boolean uf_check_pa_date (date adt_date, ref string as_message_date);/* This function accepts a date and determines if the date falls within the daily PA range.
	If it doesn't it will return a string date that can be used to inform the user of the 
	furthest date out they can inquire on.                          Author: David Deal  */     

Date 		ldt_pa_date

Int		li_pa_range


li_pa_range = This.uf_check_pa_daily_range()

ldt_pa_date = RelativeDate(Today(), li_pa_range - 1 )			
If adt_date > ldt_pa_date or adt_date < Today() Then
	as_message_date = string(ldt_pa_date,'mm/dd/yyyy')
	Return False
End If

Return True
end function

public function integer uf_check_pa_daily_range ();/* This function returns the daily pa range as currently set in tutltypes.
	                                                                    Author: David Deal  */     

Int		li_pa_range


Select 	tutltypes.type_short_desc
Into		:li_pa_range
From  	tutltypes
Where 	(tutltypes.type_code = 'PA RANGE') AND
			(tutltypes.record_type = 'PA RANGE');
			
			
Return li_pa_range
end function

public function time uf_check_min_shift_start_time (string as_shift);/* This function receives a shift then returns the earliest time that the shift should start
as currently set in tutltypes.
	                                                                    Author: David Deal  */     

Time		lt_shift_time
String	ls_shift_time


Select 	tutltypes.type_short_desc
Into		:ls_shift_time
From  	tutltypes
Where 	(tutltypes.type_code = :as_shift) AND
			(tutltypes.record_type = 'shsttime');
			
lt_shift_time =  Time(ls_shift_time)

Return lt_shift_time
end function

public function long uf_check_5_char_time (ref string as_time);/* This function accepts a 4 or 5 char string and determines if the string is in the proper
	format for a time field. It requires that if the user enters a ":"  it must be in the
	third position of the string.  If the length of the string isn't at least 4 char
	long the function will fail.  If any other non numeric char is in the string the function
	will fail.  If the string sent is 4 char long and has no : in it, the : will be inserted 
	into the string to represent a valid time.
	
	Return Codes:	0 = String Time OK   (The argument string is also sent back by ref) 
						1 = Invalid Length
						2 = Invalid Format
						3 = Invalid Time
																							 Author: David Deal  */
   
Long 		ll_count = 1, &
			ll_num = 1, &
			ll_len

String 	ls_temp, &
			ls_compare_byte
			
Boolean	lb_invalid

Time		lt_time



ll_len = Len(as_time)
If ll_len < 4  Then
	Beep(1)
	Return 1
End If

DO WHILE ll_count < ll_len
	ls_temp = Right(as_time,ll_count)
	ls_compare_byte = Left(ls_temp,1)
	If IsNumber (ls_compare_byte) Then
		ll_count ++
		ll_num ++
		Continue
	Else
		If ls_compare_byte = ':' Then
			If ll_count <> 3 Then
				lb_invalid = True
				Exit
			End If
		Else
			lb_invalid = True
			Exit
		End If
	End If
	ll_count ++
LOOP

If ll_num = 5 Then
	lb_invalid = True
End if
 
If lb_invalid Then
	Beep(1)
	Return 2
End If	

If ll_len = 4 and ll_num = 4 Then
	ls_temp = left(as_time, 2) + ':' + Right(as_time, 2)
Else
	ls_temp = as_time
End If

IF Not ls_temp = '0:00' And Not ls_temp = '00:00' Then
	lt_time = Time(ls_temp)
	If lt_time = 00:00:00.000000 Then
		Beep(1)
		Return 3
	End IF
End If

as_time = ls_temp

Return 0
	
end function

public function long uf_check_max_chain_speed ();/* This function returns the max chain speed as currently set in tutltypes.
	                                                                    Author: David Deal  */     

Long		ll_pa_range


Select 	tutltypes.type_short_desc
Into		:ll_pa_range
From  	tutltypes
Where 	(tutltypes.type_code = 'm') AND
			(tutltypes.record_type = 'chainspd');
			
			
Return ll_pa_range
end function

public subroutine uf_check_user_group (ref string as_user_group);/* This function takes the argument passed to see if it is in tutltypes if it is it passes back the original value
else it returns a default setting.
	                                                                    Author: David Deal  */     

String	ls_temp


Select 	tutltypes.type_short_desc
Into		:ls_temp
From  	tutltypes
Where 	(tutltypes.record_type = :as_user_group) 
using     sqlca;

If sqlca.sqlcode = 100 Then
	as_user_group = 'NOGROUP'
ELSEIF sqlca.SQLCode > 0 Then
	MessageBox("Database Error", &
		sqlca.SQLErrText, Exclamation!)
End If		

end subroutine

public function boolean uf_check_pasldtyp (string as_plant);/* This function returns true if the description for code(the argument plant) has a 'L' in the first byte False if it doesn't.
	                                                                    Author: David Deal  */     

String	ls_value

Select 	tutltypes.type_short_desc
Into		:ls_value
From  	tutltypes
Where 	(tutltypes.type_code = :as_plant ) AND
			(tutltypes.record_type = 'PASLDTYP');
			
If Left(ls_value,1) = 'L' Then
	Return True
Else
	Return False
End If

end function

public subroutine uf_retrieve_plan_tran_options (ref string as_options);/* This function returns all of the Planned Transfer Options as currently set in tutltypes.
	                                                                    Author: David Deal  */     

// Declare the option_cur. 
as_options = ''
DECLARE option_cur CURSOR FOR
SELECT tutltypes.type_short_desc
FROM tutltypes
Where 	(tutltypes.type_code = 'PTRANOPT') AND
			(tutltypes.record_type = 'PTRANOPT');
			
// Declare a destination variable for the options.
string	ls_temp 
// Open The Cursor.
OPEN option_cur;
// Fetch the first row from the result set.
FETCH option_cur INTO :ls_temp;
// Loop through result set until exhausted.
DO WHILE sqlca.sqlcode = 0
	// Build the return string.
	as_options += ls_temp + '~r~n'
	// Fetch the next row from the result set.
	FETCH option_cur INTO :ls_temp;
LOOP
// All done, so close the cursor.
CLOSE option_cur;			

end subroutine

public function integer uf_check_parange_dayofinv ();/* This function returns the number of days to hold inventory as currently set in tutltypes.
	                                                                    Author: David Deal  */     

Int		li_DAYOFINV


Select 	tutltypes.type_short_desc
Into		:li_DAYOFINV
From  	tutltypes
Where 	(tutltypes.type_code = 'DAYOFINV') AND
			(tutltypes.record_type = 'PA RANGE');
			
			
Return li_DAYOFINV
end function

public function boolean uf_check_parange_dayofinv_date (date adt_process_date, date adt_production_date, ref string as_message_date);/* This function accepts a Process date and determines if the Production date sent falls within the
	range of dayofinv.
	If it doesn't it will return a string date that can be used to inform the user of the 
	min date they can use.                                                    Author: David Deal  */     

Date 		ldt_pa_range_dayofinv_date

Int		li_pa_range_dayofinv


li_pa_range_dayofinv = This.uf_check_parange_dayofinv()
li_pa_range_dayofinv = li_pa_range_dayofinv * -1

ldt_pa_range_dayofinv_date = RelativeDate(adt_production_date, li_pa_range_dayofinv )

If adt_process_date < ldt_pa_range_dayofinv_date Then
	as_message_date = string(ldt_pa_range_dayofinv_date,'mm/dd/yyyy')
	Return False
End If

Return True
end function

public subroutine uf_retrieve_periods (ref string as_periods);/* This function returns all of the Periods as currently set in tutltypes.
	                                                                    Author: David Deal  */     

// Declare the option_cur. 
as_periods = ''
DECLARE option_cur CURSOR FOR
SELECT tutltypes.type_code
FROM tutltypes
Where 	(tutltypes.record_type = 'PAPERIOD');
			
// Declare a destination variable for the options.
string	ls_temp 
// Open The Cursor.
OPEN option_cur;
// Fetch the first row from the result set.
FETCH option_cur INTO :ls_temp;
// Loop through result set until exhausted.
DO WHILE sqlca.sqlcode = 0
	// Build the return string.
	as_periods += ls_temp + '~r~n'
	// Fetch the next row from the result set.
	FETCH option_cur INTO :ls_temp;
LOOP
// All done, so close the cursor.
CLOSE option_cur;			

end subroutine

public function integer uf_check_number_of_weeks_out ();/* This function returns the number of weeks out in the future for pa,
	currently set in tutltypes.
	                                                                    Author: David Deal  */     

Int		li_pa_range


Select 	tutltypes.type_short_desc
Into		:li_pa_range
From  	tutltypes
Where 	(tutltypes.type_code = 'WEEKSUM') AND
			(tutltypes.record_type = 'PA RANGE');
			
			
Return li_pa_range
end function

public function long uf_check_pa_date_ext (date adt_date, ref string as_message_date);/* This function accepts a date and determines if the date falls within the daily PA range.
	If it is OK.  If the date passed is beyond the Pa range then validate that it is a 
	Sunday(weekending date) if it is ok else it will return a string date that can be used to 
	inform the user that the date sent is not a valide date. 
	
	Return Value
	0 = Valid PA Range Date. 
	1 = Valid Ext PA Weekend Date.
	2 = Within ext pa max date Set the control to the next sunday date.
	3 = InValid EXT PA Weekend Date and invalid PA Range Date.    	
	4 = past ext pa set the control to the max Sunday date.
	//ibdkdld rev#01 add return
	5 = Ext dates not turned on set to max daily date
																		07/02 Author: David Deal  */     

Date 		ldt_pa_date,ldt_max_sunday,ldt_set_sunday

Int		li_pa_range,li_weeks_out,li_day_number


li_pa_range = This.uf_check_pa_daily_range()
li_weeks_out = This.uf_check_number_of_weeks_out()

ldt_pa_date = RelativeDate(Today(), li_pa_range - 1 )

//dld Revision 1 add if
if li_weeks_out = 0 and adt_date > ldt_Pa_date then 
	as_message_date = string(ldt_Pa_date,'mm/dd/yyyy')
	return 5	
End IF
//dld Revision 1 change if
//If adt_date < ldt_pa_date and adt_date > Today() Then Return 0
If (adt_date < ldt_pa_date or adt_date = ldt_pa_date)  and (adt_date > Today() or adt_date = Today()) Then Return 0

IF adt_date > ldt_pa_date Then
	/*  
		determine the greatest sunday date
	*/
//dld
//	li_weeks_out = This.uf_check_number_of_weeks_out()
	li_day_number = daynumber(ldt_pa_date)
	choose case True
		case li_day_number = 1 //sunday
				ldt_max_sunday = RelativeDate(ldt_pa_date, li_weeks_out * 7)
		case li_day_number = 2 
				ldt_max_sunday = RelativeDate(ldt_pa_date, (li_weeks_out * 7) - 1)
		case li_day_number = 3 
				ldt_max_sunday = RelativeDate(ldt_pa_date, (li_weeks_out * 7) - 2)
		case li_day_number = 4 
				ldt_max_sunday = RelativeDate(ldt_pa_date, (li_weeks_out * 7) - 3)
		case li_day_number = 5 
				ldt_max_sunday = RelativeDate(ldt_pa_date, (li_weeks_out * 7) - 4)
		case li_day_number = 6 
				ldt_max_sunday = RelativeDate(ldt_pa_date, (li_weeks_out * 7) - 5)
		case li_day_number = 7 
				ldt_max_sunday = RelativeDate(ldt_pa_date, (li_weeks_out * 7) - 6)
	end choose		
			
	li_day_number = daynumber(adt_date)
	
	If li_day_number = 1 and adt_date <= ldt_max_sunday Then
		Return 1
	Else
		/*  
			determine next sunday date 
		*/
		choose case True
			//ibdkdld bug sunday 9/30	
			case li_day_number = 1 
					ldt_set_sunday = RelativeDate(adt_date,0	)
			case li_day_number = 2 
					ldt_set_sunday = RelativeDate(adt_date, + 6)
			case li_day_number = 3 
					ldt_set_sunday = RelativeDate(adt_date, + 5)
			case li_day_number = 4 
					ldt_set_sunday = RelativeDate(adt_date, + 4)
			case li_day_number = 5 
					ldt_set_sunday = RelativeDate(adt_date, + 3)
			case li_day_number = 6 
					ldt_set_sunday = RelativeDate(adt_date, + 2)
			case li_day_number = 7 
					ldt_set_sunday = RelativeDate(adt_date, + 1)
		end choose		
		/*  
			IF date within the bounds then use next sunday date to  
		*/
		If ldt_set_sunday <= ldt_max_sunday Then
			as_message_date = string(ldt_set_sunday,'mm/dd/yyyy')
			return 2
		Else
			as_message_date = string(ldt_max_sunday,'mm/dd/yyyy')
			Return 4
		End If
	End if
Else
	as_message_date = string(ldt_pa_date,'mm/dd/yyyy')
	Return 3
End If


end function

public function boolean uf_check_security (string as_window_name, string as_rights);/* This function returns a boolean seeing if the user has rights to the window passed and the process passed.
	                                                                    Author: David Deal  */     


String ls_result,ls_groupid

ls_groupid = iw_frame.iu_netwise_data.is_groupid

CHOOSE CASE upper(as_rights)
	CASE "ADD"
		Select 	group_profile.add_auth
			Into		:ls_result	
			From  	group_profile
			Where 	(group_profile.group_id = :ls_groupid) AND
						(group_profile.window_name = :as_window_name);
				
			If SQLCA.SQLCode = 0 Then
				If ls_result = "N" Then Return False
			Else
				Return False
			End if
	CASE "MODIFY"
		Select 	group_profile.modify_auth
			Into		:ls_result	
			From  	group_profile
			Where 	(group_profile.group_id = :ls_groupid) AND
						(group_profile.window_name = :as_window_name);
					
			If SQLCA.SQLCode = 0 Then 
				If ls_result = "N" Then Return False
			Else
				Return False
			End if
	CASE "DELETE"
		Select 	group_profile.delete_auth
			Into		:ls_result	
			From  	group_profile
			Where 	(group_profile.group_id = :ls_groupid) AND
						(group_profile.window_name = :as_window_name);
					
			If SQLCA.SQLCode = 0 Then
				If ls_result = "N" Then Return False
			Else
				Return False
			End if
END CHOOSE

Return True
end function

on nvuo_pa_business_rules.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvuo_pa_business_rules.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

