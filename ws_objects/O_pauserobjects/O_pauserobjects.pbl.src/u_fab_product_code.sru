﻿$PBExportHeader$u_fab_product_code.sru
$PBExportComments$UO to get product description from product code
forward
global type u_fab_product_code from u_base_dw_ext
end type
end forward

global type u_fab_product_code from u_base_dw_ext
integer width = 1623
integer height = 308
string dataobject = "d_fab_product_code"
boolean border = false
boolean ib_db_reconnectable = true
integer ii_retry_count = 1
end type
global u_fab_product_code u_fab_product_code

type variables
Boolean		ib_required = TRUE
Boolean		ib_error_occurred = FALSE

Private:
datawindowchild 			idddw_child_state, &
								idddw_child_status
nvuo_fab_product_code	invuo_fab_product_code
s_error						istr_error_info


end variables

forward prototypes
public function string uf_get_product_state ()
public function string uf_get_product_desc ()
public function string uf_get_product_code ()
public function boolean uf_set_product_code (string as_product_code)
public function string uf_get_product_state_desc ()
public function boolean uf_product (string as_product)
public function boolean uf_set_product_desc (string as_product_desc)
public function boolean uf_set_product_state_desc (string as_product_desc)
public function boolean uf_set_product_code (string as_product_code, string as_product_descr)
public function boolean uf_set_product_state (string as_product_state, string as_product_state_desc)
public subroutine uf_disable ()
public function boolean uf_validate_rmt ()
public function string uf_get_product_status ()
public function boolean uf_set_product_status (string as_product_status)
public subroutine uf_enable_status (boolean ab_enable)
public subroutine uf_enable (boolean ab_enable)
public subroutine uf_enable_state (boolean ab_enable)
public function string uf_get_product_status_desc ()
public function boolean uf_set_product_status (string as_product_status, string as_product_status_desc)
public function boolean uf_set_product_status_desc (string as_product_status_desc)
public function integer uf_importstring (string as_product_code, string as_product_desc, string as_product_state, string as_product_state_desc, string as_product_status, string as_product_status_desc, boolean ab_resetdw)
public function string uf_exportstring ()
public function integer uf_importstring (string as_product_code, string as_product_desc, string as_product_state, string as_product_state_desc, boolean ab_resetdw)
public function boolean uf_set_product_state (string as_product_state)
public function boolean uf_validate ()
end prototypes

public function string uf_get_product_state ();string		ls_product_state

AcceptText()

ls_product_state = Trim(GetItemString( 1, "product_state" ))

IF IsNull( ls_product_state ) THEN ls_product_state = ""

Return( ls_product_state )

end function

public function string uf_get_product_desc ();string		ls_product_desc

AcceptText()

ls_product_desc = GetItemString( 1, "fab_product_description" )

IF IsNull( ls_product_desc ) THEN ls_product_desc = ""

Return( ls_product_desc )
end function

public function string uf_get_product_code ();string		ls_product_code

AcceptText()

ls_product_code = GetItemString( 1, "fab_product_code" )

IF IsNull( ls_product_code ) THEN ls_product_code = ""

Return( ls_product_code )
end function

public function boolean uf_set_product_code (string as_product_code);Return uf_set_product_code(as_product_code,'')

end function

public function string uf_get_product_state_desc ();string		ls_product_state

AcceptText()

ls_product_state = GetItemString( 1, "product_state_description" )

IF IsNull( ls_product_state ) THEN ls_product_state = ""

Return(ls_product_state)



end function

public function boolean uf_product (string as_product);Boolean			lb_return
			
					
lb_return	= invuo_fab_product_code.uf_check_product(as_product)

If not lb_return then
	If	invuo_fab_product_code.ib_error_occurred then
		iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
	Else
		iw_frame.SetMicroHelp(as_product + " is an invalid Product Code")
	End If
End If
	
Return lb_return
end function

public function boolean uf_set_product_desc (string as_product_desc);Return (SetItem( 1, "fab_product_description", as_product_desc) = 1)
end function

public function boolean uf_set_product_state_desc (string as_product_desc);Return(SetItem( 1, "product_state_description", as_product_desc) = 1)

end function

public function boolean uf_set_product_code (string as_product_code, string as_product_descr);integer		li_rc

li_rc = SetItem( 1, "fab_product_code", as_product_code)

If li_rc = 1 Then
	Return uf_set_product_desc(as_product_descr)
End If

Return (li_rc = 1)

end function

public function boolean uf_set_product_state (string as_product_state, string as_product_state_desc);Integer li_rtn
li_rtn = SetItem( 1, "product_state", as_product_state )

if li_rtn = 1 then
	Return uf_set_product_state_desc(as_product_state_desc)
end if

Return(li_rtn = 1)
end function

public subroutine uf_disable ();this.uf_enable(false)
return
end subroutine

public function boolean uf_validate_rmt ();String	ls_product, &
			ls_product_state, &
			ls_product_status

if AcceptText() <> 1 then return false

ls_product = this.uf_get_product_code( )
If iw_frame.iu_string.nf_IsEmpty(ls_product) Then
	iw_frame.SetMicroHelp("Product is a required field")
	this.setfocus( )
	this.setcolumn("fab_product_code") 
	return false
End if

ls_product_state = this.uf_get_product_state( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	this.setfocus( )
	this.setcolumn("product_state") 
	return false
End if
	
if not invuo_fab_product_code.uf_check_product_state_rmt(ls_product, Trim(ls_product_state)) then	
	iw_Frame.SetMicroHelp(ls_product_state + " is an Invalid Product State for this Product")
	this.setfocus( )
	this.setcolumn("product_state") 
	This.SelectText(1, Len(ls_product_state))	
	return false
end if
//1-13 jac
ls_product_status = this.uf_get_product_status( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_status) Then
	iw_frame.SetMicroHelp("Product Status is a required field")
	this.setfocus( )
	this.setcolumn("product_status") 
	return false
End if
//
Return true
end function

public function string uf_get_product_status ();string		ls_product_status

AcceptText()

ls_product_status = Trim(GetItemString( 1, "product_status" ))

IF IsNull( ls_product_status ) THEN ls_product_status = ""

Return( ls_product_status )
end function

public function boolean uf_set_product_status (string as_product_status);Integer 	li_rtn
Long 		ll_row

li_rtn = SetItem( 1, "product_status", Trim(as_product_status))

// Find the entered plant code in the table -- don't count the last row, it's blank
ll_row = idddw_child_status.Find('type_code = "' + Trim(as_product_status) + '"', 1, idddw_child_status.RowCount())
If ll_row > 0 Then
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	uf_set_product_status_desc(idddw_child_status.GetItemString(ll_row, 'type_short_desc'))
End if

Return(li_rtn = 1)
end function

public subroutine uf_enable_status (boolean ab_enable);//16777215, 67108864
If ab_enable Then
	Modify("product_status.Protect='0' product_status.Background.Color='16777215'")
Else
	Modify("product_status.Protect='1' product_status.Background.Color='67108864'")
End If

end subroutine

public subroutine uf_enable (boolean ab_enable);//16777215, 67108864

ib_Updateable = ab_enable 

If ab_enable Then
	Modify("fab_product_code.Protect='0' fab_product_code.Background.Color='16777215'")
	Modify("product_state.Protect='0' product_state.Background.Color='16777215'")
	Modify("product_status.Protect='0' product_status.Background.Color='16777215'")

	//this.setitem(1,"enable_product_state","T")
	//this.setitem(1,"enable_product_code","T")
Else
	Modify("fab_product_code.Protect='1' fab_product_code.Background.Color='67108864'")
	Modify("product_state.Protect='1' product_state.Background.Color='67108864'")
	Modify("product_status.Protect='1' product_status.Background.Color='67108864'")
	
	//this.setitem(1,"enable_product_state","F")
	//this.setitem(1,"enable_product_code","F")
End If

return
end subroutine

public subroutine uf_enable_state (boolean ab_enable);//16777215, 67108864
If ab_enable Then
	Modify("product_state.Protect='0' product_state.Background.Color='16777215'")
Else
	Modify("product_state.Protect='1' product_state.Background.Color='67108864'")
End If


end subroutine

public function string uf_get_product_status_desc ();string		ls_product_status_desc

AcceptText()

ls_product_status_desc = GetItemString( 1, "product_status_description" )

IF IsNull( ls_product_status_desc ) THEN ls_product_status_desc = ""

Return(ls_product_status_desc)



end function

public function boolean uf_set_product_status (string as_product_status, string as_product_status_desc);Integer li_rtn
li_rtn = SetItem( 1, "product_status", as_product_status )

if li_rtn = 1 then
	Return uf_set_product_status_desc(as_product_status_desc)
end if

Return(li_rtn = 1)
end function

public function boolean uf_set_product_status_desc (string as_product_status_desc);Return(SetItem( 1, "product_status_description", as_product_status_desc) = 1)

end function

public function integer uf_importstring (string as_product_code, string as_product_desc, string as_product_state, string as_product_state_desc, string as_product_status, string as_product_status_desc, boolean ab_resetdw);return uf_importstring(as_product_code + '~t' + as_product_desc + '~t' + &
					 	  		as_product_state + '~t' + as_product_state_desc + '~t' + &
								as_product_status + '~t' + as_product_status_desc, ab_resetdw)
					 
end function

public function string uf_exportstring ();return (uf_get_product_code() + '~t' + uf_get_product_desc() + '~t' + &
		  uf_get_product_state() + '~t' + uf_get_product_state_desc() + '~t' + &
		  uf_get_product_status() + '~t' + uf_get_product_status_desc() )


end function

public function integer uf_importstring (string as_product_code, string as_product_desc, string as_product_state, string as_product_state_desc, boolean ab_resetdw);return uf_importstring(as_product_code + '~t' + as_product_desc + '~t' + &
					 	  		as_product_state + '~t' + as_product_state_desc, ab_resetdw)
					 
end function

public function boolean uf_set_product_state (string as_product_state);Integer 	li_rtn
Long 		ll_row

li_rtn = SetItem( 1, "product_state", Trim(as_product_state))

// Find the entered plant code in the table -- don't count the last row, it's blank
ll_row = idddw_child_state.Find('type_code = "' + Trim(as_product_state) + '"', 1, idddw_child_state.RowCount())
If ll_row > 0 Then
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	uf_set_product_state_desc(idddw_child_state.GetItemString(ll_row, 'type_short_desc'))
End if

Return(li_rtn = 1)
end function

public function boolean uf_validate ();String	ls_product, &
			ls_product_state, &
			ls_product_status
			

if AcceptText() <> 1 then return false

ls_product = this.uf_get_product_code( )
If iw_frame.iu_string.nf_IsEmpty(ls_product) Then
	iw_frame.SetMicroHelp("Product is a required field")
	this.setfocus( )
	this.setcolumn("fab_product_code") 
	return false
End if

ls_product_state = this.uf_get_product_state( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	this.setfocus( )
	this.setcolumn("product_state") 
	return false
End if

ls_product_status = this.uf_get_product_status( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_status) Then
	iw_frame.SetMicroHelp("Product Status is a required field")
	this.setfocus( )
	this.setcolumn("product_status") 
	return false
End if

	
if not invuo_fab_product_code.uf_check_product_state(ls_product, Trim(ls_product_state)) then	
	iw_Frame.SetMicroHelp(ls_product_state + " is an Invalid Product State for this Product")
	this.setfocus( )
	this.setcolumn("product_state") 
	This.SelectText(1, Len(ls_product_state))	
	return false
end if

Return true
end function

event getfocus;call super::getfocus;String	ls_GetText 


ls_GetText = Trim(This.GetText())

This.SelectText(1, Len(ls_GetText))

end event

event ue_postconstructor;call super::ue_postconstructor;//this.uf_enable(ib_updateable)
end event

event itemchanged;call super::itemchanged;String	ls_product_code, &
			ls_product_state, &
			ls_product_state_desc, &
			ls_product_status_desc, &
			ls_color, &
			ls_product_status
			
Boolean	lb_fresh_ind, &
			lb_frozen_ind			
			
Long		ll_row

If ib_Updateable Then
	choose case dwo.name
		case "fab_product_code"	
			ls_product_code = data
			IF Len(Trim(ls_product_code)) <> 0 THEN
				If uf_product( ls_product_code ) Then
					uf_set_product_desc(invuo_fab_product_code.uf_get_product_description( ))
					iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				Else
					This.SelectText(1, Len(ls_product_Code))
					Return 1
				END IF
			Else
				This.SelectText(1, Len(ls_product_Code))
				uf_set_product_desc("")
				If ib_required Then
					iw_frame.SetMicroHelp("Product Code is a required field")
					Return 1
				End If
			END IF
			
			ls_product_state = GetItemString( 1, "product_state" )
			IF IsNull( ls_product_state ) THEN ls_product_state = ""
			If Len(ls_product_state) = 0 Then
				ls_product_state = '1'
				SetItem( 1, "product_state", ls_product_state)
				// Find the entered plant code in the table -- don't count the last row, it's blank
				ll_row = idddw_child_state.Find('type_code = "' + Trim(ls_product_state) + '"', 1, idddw_child_state.RowCount())
				If ll_row <= 0 Then
				Else
					iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
					ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
					uf_set_product_state_desc(ls_product_state_desc)
				End if
			End If
			
			ls_product_status = GetItemString( 1, "product_status" )
			IF IsNull( ls_product_status ) THEN ls_product_status = ""
			If Len(ls_product_status) = 0 Then
				ls_product_status = 'G'
				SetItem( 1, "product_status", ls_product_status)
				// Find the entered plant code in the table -- don't count the last row, it's blank
				ll_row = idddw_child_status.Find('type_code = "' + Trim(ls_product_status) + '"', 1, idddw_child_status.RowCount())
				If ll_row <= 0 Then
				Else
					iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
					ls_product_status_desc = idddw_child_status.GetItemString(ll_row, 'type_short_desc')
					uf_set_product_status_desc(ls_product_status_desc)
				End if
			End If
			
			
		case "product_state" 
			
			ls_product_state = data
						
			If Len(Trim(ls_product_state)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"product_state","")
				uf_set_product_state_desc("")
				return 
			End if
			
			// Find the entered plant code in the table -- don't count the last row, it's blank
			ll_row = idddw_child_state.Find('type_code = "' + Trim(ls_product_state) + '"', 1, idddw_child_state.RowCount())
			If ll_row <= 0 Then
				iw_Frame.SetMicroHelp(ls_product_state + " is an Invalid Product State")
	
				This.SetFocus()
				This.SelectText(1, Len(ls_product_state))
				
				return 1
			Else
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
				uf_set_product_state_desc(ls_product_state_desc)
				If Trim(ls_product_state) = '3' Then
					This.Modify("product_state_description.Background.Color='65535'")
				Else
					If Trim(ls_product_state) = '4' Then
						This.Modify("product_state_description.Background.Color='65280'")
					End if
				End if
				SetProfileString( iw_frame.is_UserINI, "Pas", "LastProductState",Trim(data))
				This.SetFocus()
				This.SelectText(1, Len(ls_product_state))
			End if
			
			
		case "product_status" 
			
			ls_product_status = data
			uf_set_product_status(ls_product_status)
						
			If Len(Trim(ls_product_status)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"product_status","")
				uf_set_product_status_desc("")
				return 
			End if
			
			// Find the entered plant code in the table -- don't count the last row, it's blank
			ll_row = idddw_child_status.Find('type_code = "' + Trim(ls_product_status) + '"', 1, idddw_child_status.RowCount())
			If ll_row <= 0 Then
				iw_Frame.SetMicroHelp(ls_product_status + " is an Invalid Product Status")
	
				This.SetFocus()
				This.SelectText(1, Len(ls_product_status))
				
				return 1
			Else
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				ls_product_status_desc = idddw_child_status.GetItemString(ll_row, 'type_short_desc')
				uf_set_product_status_desc(ls_product_status_desc)
				If Trim(ls_product_status) = 'R' Then
					This.Modify("product_status_description.Background.Color='255'")
				Else
					If Trim(ls_product_status) = 'M' Then
						This.Modify("product_status_description.Background.Color='16711935'")
					End if
				End if
				SetProfileString( iw_frame.is_UserINI, "Pas", "LastProductStatus",Trim(data))
				This.SetFocus()
				This.SelectText(1, Len(ls_product_status))
			End if			
	end choose	
End If

Return 0
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, Len(Trim(This.GetText())))

end event

event itemerror;call super::itemerror;Return 1
end event

on u_fab_product_code.create
call super::create
end on

on u_fab_product_code.destroy
call super::destroy
end on

event ue_retrieve;call super::ue_retrieve;string ls_Last_State, &
		ls_Last_Status

This.GetChild("product_state", idddw_child_state)

idddw_child_state.SetTransObject(SQLCA)
idddw_child_state.Retrieve("PRDSTATE")
idddw_child_state.SetSort("type_code")
idddw_child_state.Sort()
//idddw_child.InsertRow(0)
This.InsertRow(0)


ls_Last_State = ProfileString( iw_frame.is_UserINI, "Pas", "LastProductState","1")
this.uf_set_product_state(ls_Last_State)


This.GetChild("product_status", idddw_child_status)

idddw_child_status.SetTransObject(SQLCA)
idddw_child_status.Retrieve("PRODSTAT")
idddw_child_status.SetSort("type_code")
idddw_child_status.Sort()
//idddw_child.InsertRow(0)
//This.InsertRow(0)


ls_Last_Status = ProfileString( iw_frame.is_UserINI, "Pas", "LastProductStatus","G")
this.uf_set_product_status(ls_Last_Status)


end event

