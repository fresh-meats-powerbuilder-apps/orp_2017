﻿$PBExportHeader$w_pa-summary.srw
$PBExportComments$PA_Summary Window
forward
global type w_pa-summary from w_base_sheet_ext
end type
type dw_pa-summary_header from u_base_dw_ext within w_pa-summary
end type
type dw_pa_summary_plants from u_base_dw_ext within w_pa-summary
end type
type dw_pa-summary_detail from u_base_dw_ext within w_pa-summary
end type
end forward

global type w_pa-summary from w_base_sheet_ext
integer x = 663
integer y = 348
integer width = 3328
integer height = 2096
string title = "Product Availability Summary"
event ue_re-retrieve ( )
event type integer ue_reinquire ( )
dw_pa-summary_header dw_pa-summary_header
dw_pa_summary_plants dw_pa_summary_plants
dw_pa-summary_detail dw_pa-summary_detail
end type
global w_pa-summary w_pa-summary

type prototypes

end prototypes

type variables
Boolean	ib_ShowInquire

m_PA-Summary_Pop-Ups im_Pop-Ups

INTEGER	ii_Max_Rows = 50

String	is_passedValue, &
	is_file_desc, &
	is_description, &
	is_openstring, &
	is_prod_data, &
	is_save_row, &
	is_save_col

Long	il_xpos, &
	il_ypos, &
	il_save_col, &
	il_col, &
	il_save_row
	

application 	ia_apptool

//u_orp002		iu_orp002
//u_orp003		iu_orp003
u_orp204  	iu_orp204

u_ws_orp2   iu_ws_orp2
u_ws_orp4   iu_ws_orp4

w_tooltip_expanded	iw_tooltip

datastore		ids_prod_data

Window			iw_This
end variables

forward prototypes
public function boolean wf_update ()
public function integer wf_getplantdata (ref string as_plantdata)
public function integer wf_putdetaildata (ref string as_detaildata)
public function integer wf_putplantdata (ref string as_plantdata)
public function integer wf_getdetaildata (ref string as_detaildata)
public function boolean wf_retrieve ()
public function boolean wf_next ()
public function boolean wf_previous ()
public subroutine wf_fileopen (string ac_seqnum, string as_availabilitydate)
public subroutine wf_filenew ()
public function integer wf_updat ()
end prototypes

event ue_re-retrieve();INT		li_rtn,& 
			li_index, &
			li_rowcount

CHAR		lc_ProcessOption, &			
			lc_AvailableDate[10]			
			
Any		lc_todays_date,&
			lc_date_available

STRING	ls_PlantData, &
			ls_DetailData, &
			ls_Availability_Date,&
			ls_Test_Date, &
			ls_descr_out, &
			ls_page_descr, & 
			ls_date_type_ind, &
			ls_customer_id, & 
			ls_detail_line, &
			ls_prod_data
						
			
BOOLEAN	lb_Modified = FALSE
s_Error	lstr_Error

dw_pa-summary_header.AcceptText()
dw_pa-summary_detail.AcceptText()
dw_pa_summary_plants.AcceptText()

dw_pa-summary_header.SetRedraw(FALSE)
dw_pa-summary_detail.SetRedraw(FALSE)
dw_pa_summary_plants.SetRedraw(FALSE)

lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Retriev"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

ls_page_descr = dw_pa-summary_header.GetItemString(1, "seq_num")
 
// 9-12 jac lc_AvailableDate = STRING(dw_pa-summary_header.GetItemDate(1, "reinquire_date"), "YYYY/MM/DD")
lc_AvailableDate = STRING(dw_pa-summary_header.GetItemDate(1, "availability_date"), "YYYY/MM/DD")
lc_date_available = string(lc_AvailableDate)

lc_todays_date = string(today(),"YYYY/MM/DD")

if (lc_date_available < lc_todays_date) then

	this.SetMicroHelp("Can not inquire on date Earlier than today.")
else
	lc_ProcessOption = "P"
	IF wf_GetPlantData(ls_PlantData) <> 0 THEN 
		dw_pa-summary_header.SetRedraw(True)
		dw_pa-summary_detail.SetRedraw(True)
		dw_pa_summary_plants.SetRedraw(True)
	End IF
	IF wf_GetDetailData(ls_DetailData) <> 0 THEN 
		dw_pa-summary_header.SetRedraw(True)
		dw_pa-summary_detail.SetRedraw(True)
		dw_pa_summary_plants.SetRedraw(True)
	End IF
	
 	//9-12 jac     lc_AvailableDate = STRING(dw_pa-summary_header.GetItemDate(1, "reinquire_date"), "mm/dd/yyyy")
	lc_AvailableDate = STRING(dw_pa-summary_header.GetItemDate(1, "availability_date"), "mm/dd/yyyy")
		
	lc_date_available = string(lc_AvailableDate)
	lc_todays_date = string(today(),"mm/dd/yyyy")
	
	//added 9-12 jac
	ls_date_type_ind = dw_pa-summary_header.GetItemString( 1, "date_type_ind")
	ls_customer_id = dw_pa-summary_header.GetItemString( 1, "customer_id")
	
	If IsNull(ls_date_type_ind) Then
		ls_date_type_ind = " "
	End If
	If IsNull(ls_customer_id) Then
		ls_customer_id = "        "
	End If

		
	//added 11-12 jac
   /* 04-13 ajg, date type is passed in now if show inquire = false, no need to set it.
	if ib_ShowInquire = FALSE then
      ls_date_type_ind = 'A'
   end if
	*/
	
	//added 11-12 jac
	ls_detail_line = dw_pa-summary_header.GetItemString( 1, "discounted_ind") + '~t'	
	
	////// Call RPC orpo38ar
//	li_Rtn = iu_orp003.nf_orpo38ar(lstr_Error, lc_ProcessOption, &
//										lc_AvailableDate, ls_page_descr, &
//										ls_PlantData, ls_DetailData, ls_descr_out, &
//										ls_customer_id, ls_date_type_ind, ls_detail_line, ls_prod_data)
//									

	li_Rtn = iu_ws_orp2.nf_orpo38fr(lc_ProcessOption, &
									lc_AvailableDate, ls_page_descr, &
									ls_PlantData, ls_DetailData, ls_descr_out, &
									ls_customer_id, ls_date_type_ind, ls_detail_line, ls_prod_data, lstr_Error)
									

	IF li_Rtn = 0 or li_Rtn = 5 THEN
		This.wf_PutDetailData(ls_DetailData)
		This.wf_PutPlantData(ls_PlantData)
	   ls_Availability_Date = lc_AvailableDate
		is_prod_data = ls_prod_data
		
		dw_pa-summary_header.SetItem(1, "last_seq_num", ls_descr_out)
		dw_pa-summary_header.SetItem(1, "availability_date", DATE(ls_Availability_Date))
    //added 9-12 jac
	   dw_pa-summary_header.SetItem(1, "customer_id", ls_customer_id)
   //
		dw_pa-summary_header.SetRedraw(TRUE)
		dw_pa-summary_detail.SetRedraw(TRUE)
		dw_pa_summary_plants.SetRedraw(TRUE)

		dw_pa-summary_detail.SetFocus()
	END IF
	
	dw_pa-summary_detail.SetRow(1)
	dw_pa-summary_detail.SetColumn('product_code')
	dw_pa-summary_detail.SetFocus()
END IF

dw_pa-summary_header.SetRedraw(TRUE)
dw_pa-summary_detail.SetRedraw(TRUE)
dw_pa_summary_plants.SetRedraw(TRUE)
end event

event ue_reinquire;call super::ue_reinquire;This.TriggerEvent("ue_re-retrieve")
Return 1
end event

public function boolean wf_update ();STRING	ls_FileName

Openwithparm(w_PA-Summary_Save-As, This)
ls_FileName = Message.StringParm
IF ls_FileName = "A" THEN RETURN FALSE	
dw_pa-summary_header.SetItem(1, "seq_num", ls_FileName)
dw_pa-summary_header.SetItem(1, "last_seq_num", ls_FileName)
wf_updat()
RETURN TRUE
end function

public function integer wf_getplantdata (ref string as_plantdata);dw_pa_summary_plants.AcceptText()
as_PlantData = dw_pa_summary_plants.Describe("DataWindow.Data")
RETURN 0

end function

public function integer wf_putdetaildata (ref string as_detaildata);INTEGER	li_Rows

STRING	ls_Null

SetNull(ls_Null)

dw_PA-Summary_Detail.Reset()
dw_PA-Summary_Detail.ImportString(as_detaildata)
li_Rows = dw_PA-Summary_Detail.RowCount()

	IF li_Rows < ii_max_rows AND li_Rows > 0 THEN 
		CHOOSE CASE TRIM(dw_PA-Summary_Detail.GetItemString(li_Rows, "product_code"))
			CASE ls_null, ""
			CASE ELSE	
				dw_PA-Summary_Detail.InsertRow(0)
		END CHOOSE
	END IF
	dw_pa-summary_detail.uf_changerowstatus ( 0, NotModified! )

RETURN 0

end function

public function integer wf_putplantdata (ref string as_plantdata);IF Len( as_plantdata) > 0 THEN
	dw_PA_Summary_Plants.Reset()
	dw_PA_Summary_Plants.ImportString(as_PlantData)
	dw_PA_Summary_Plants.uf_changerowstatus ( 0, NotModified! )
END IF 

RETURN 0
	
	


end function

public function integer wf_getdetaildata (ref string as_detaildata);dw_pa-summary_detail.AcceptText()
as_DetailData = dw_PA-Summary_Detail.Describe("DataWindow.Data")
RETURN 0



end function

public function boolean wf_retrieve ();//IF ib_ShowInquire then 
//	Open(w_pa_summary_open)
//	CHOOSE CASE Message.StringParm
//		CASE "CANCEL"
//			dw_pa-summary_detail.SetFocus()
//			RETURN FALSE
//	END CHOOSE
//END IF
//

string    ls_input 


If ib_ShowInquire Then
	OpenWithParm(w_pa_summary_open, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false	
Else	
	ib_ShowInquire = False
End if

IF Message.StringParm = "Cancel" then return False


This.PostEvent("ue_query",0,"")	

RETURN TRUE


end function

public function boolean wf_next ();Date	ld_avail_date

//9-12 jac ld_avail_date = dw_pa-summary_header.GetItemDate(1, "reinquire_date")
ld_avail_date = dw_pa-summary_header.GetItemDate(1, "availability_date")
ld_avail_date = RelativeDate(ld_avail_date,1)
//9-12 jac dw_pa-summary_header.SetItem(1, "reinquire_date", ld_avail_date)

dw_pa-summary_header.SetItem(1, "availability_date", ld_avail_date)

//Return This.PostEvent("ue_query",0,"")
Return This.PostEvent("ue_re-retrieve", 0, "")
end function

public function boolean wf_previous ();String	ls_today,&
			ls_temp

Date	ld_avail_date

//9-12 jac ld_avail_date = dw_pa-summary_header.GetItemDate(1, "reinquire_date")
ld_avail_date = dw_pa-summary_header.GetItemDate(1, "availability_date")
ld_avail_date = RelativeDate(ld_avail_date,-1)

ls_temp = STRING(ld_avail_date, "YYYY/MM/DD")
ls_today = STRING(Today(), "YYYY/MM/DD")

IF ls_temp < ls_today THEN
	MessageBox("WRONG DATE", "Can't Inquire on a Date Prior To Today's Date")
	Return false
END IF

//9-12 jac dw_pa-summary_header.SetItem(1, "reinquire_date", ld_avail_date)
dw_pa-summary_header.SetItem(1, "availability_date", ld_avail_date)

//Return This.PostEvent("ue_query", 0, "")
Return This.PostEvent("ue_re-retrieve", 0, "")
end function

public subroutine wf_fileopen (string ac_seqnum, string as_availabilitydate);//	This.SetRedraw(FALSE)

	STRING	ls_Test

	
	//9-12 jac dw_pa-summary_header.SetItem(1, "reinquire_date", DATE(as_AvailabilityDate))
	dw_pa-summary_header.SetItem(1, "availability_date", DATE(as_AvailabilityDate))
	dw_pa-summary_header.SetItem(1, "seq_num", ac_SeqNum)
		
	wf_Retrieve()
//	This.SetRedraw(TRUE)









end subroutine

public subroutine wf_filenew ();	This.SetRedraw(FALSE)

	dw_pa-summary_header.Reset()
	dw_pa_summary_plants.Reset()
	dw_pa-summary_detail.Reset()

	dw_pa-summary_header.InsertRow(0)
	dw_pa_summary_plants.InsertRow(0)
	dw_pa-summary_detail.InsertRow(0)

	dw_pa-summary_header.SetItem(1, "seq_num", "00")
	dw_pa-summary_header.SetItem(1, "last_seq_num", "00")
	dw_pa-summary_header.SetItem(1, "availability_date", Today())
//9-12 jac	dw_pa-summary_header.SetItem(1, "reinquire_date", Today())
	dw_pa-summary_header.uf_changerowstatus(1, NotModified!)
	dw_pa-summary_detail.uf_changerowstatus(1, NotModified!)
	dw_pa_summary_plants.uf_changerowstatus(1, NotModified!)

	dw_pa_summary_plants.SetFocus()
	dw_pa_summary_plants.SetColumn(1)

	This.SetRedraw(TRUE)


end subroutine

public function integer wf_updat ();INT		li_rtn, &
			li_counter

CHAR		lc_AvailableDate[10], &
			lc_ProcessOption

STRING	ls_PlantData, &
			ls_DetailData, &
			ls_descr_out, &
			ls_page_descr_in, &
			ls_save_prod
			
long     ll_RowCount

s_Error	lstr_Error

lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Updat"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

ls_page_descr_in = dw_pa-summary_header.GetItemString(1, "seq_num" + '~t' + "description")

lc_AvailableDate = STRING(dw_pa-summary_header.GetItemDate(1, "availability_date"), "mm/dd/yyyy")

IF	ls_page_descr_in = "00" THEN
	MESSAGEBOX("Operator Error","You must select a sequence number.")
	dw_pa-summary_header.SetFocus()
	RETURN -1
ELSE
	lc_ProcessOption = "S"
	IF wf_GetPlantData(ls_PlantData) <> 0 THEN 
		dw_pa-summary_header.SetFocus()
		RETURN -1
	END IF

	IF wf_GetDetailData(ls_DetailData) <> 0 THEN 
		dw_pa-summary_header.SetFocus()
		RETURN -1
	END IF
END IF

dw_pa-summary_header.Enabled = FALSE
dw_pa-summary_detail.Enabled = FALSE
dw_pa_summary_plants.Enabled = FALSE

IF NOt IsValid(iu_orp204) Then iu_orp204 = Create u_orp204
										
li_Rtn = iu_ws_orp4.nf_orpo81fr(lstr_Error, lc_ProcessOption, lc_AvailableDate,  ls_page_descr_in, ls_descr_out, ls_PlantData, ls_DetailData, SQLCA.userid)
										
ll_RowCount = dw_pa-summary_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	ls_save_prod = dw_pa-summary_detail.GetItemString(li_Counter, "save_prod_ind")
	if ls_save_prod = '99' then
		dw_pa-summary_detail.SetItem(li_Counter, 'save_prod_ind', li_counter)
	END IF	
Next
										
//This.PostEvent("ue_reinquire")
	
dw_pa-summary_header.SetItem(1, "last_seq_num", ls_descr_out)

dw_pa-summary_header.Enabled = TRUE
dw_pa-summary_detail.Enabled = TRUE
dw_pa_summary_plants.Enabled = TRUE
dw_pa-summary_header.SetFocus()

RETURN li_Rtn
end function

event ue_postopen;call super::ue_postopen;BOOLEAN	lb_CloseOpen

String	ls_Date,&
			ls_Plant, ls_Customer, ls_Temp
			
string  ls_date_type_ind, ls_date_tag, ls_column_name, ls_discounted_ind, ls_Customer_name
			
u_string_functions lu_string_functions

lb_CloseOpen = TRUE

IF Not IsValid( iu_orp003) Then iu_orp003 = Create u_orp003

If Message.ReturnValue = -1 Then
	Close(This)
End If

im_Pop-Ups = CREATE m_PA-Summary_Pop-Ups

iu_ws_orp2 = CREATE u_ws_orp2
iu_ws_orp4 = Create u_ws_orp4


wf_FileNew()
IF Not lu_string_functions.nf_isempty ( is_passedValue ) Then
	ls_Temp = is_passedValue

	lu_string_functions.nf_parseleftright ( ls_Temp, "~t", ls_Date, ls_Temp)
	lu_string_functions.nf_parseleftright ( ls_temp, "~t", ls_Plant, ls_Temp)
	lu_string_functions.nf_parseleftright ( ls_temp, "~t", ls_Customer, ls_date_type_ind)

	if not lu_string_functions.nf_isempty(ls_Customer) then
		dw_pa-summary_Header.SetItem(1,"customer_id", ls_Customer)
		
		SELECT customers.short_name
		INTO :ls_Customer_Name
		FROM customers
		WHERE customers.customer_id = :ls_Customer
		USING SQLCA;
																					
		If SQLCA.SQLCode = 100 then
			 iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
			 Return 
		ELSE
			dw_pa-summary_Header.setitem(1,'customer_name', ls_Customer_name)
		end if
		
	end if

 	dw_pa_summary_plants.SetItem(1,"plant_code_1", ls_plant)
	//9-12 jac	dw_pa-summary_header.SetItem(1,"reinquire_date", Date(ls_date))
	
	dw_pa-summary_Header.SetItem(1,"date_type_ind", ls_date_type_ind)
	dw_pa-summary_Header.SetItem(1,"availability_date", Date(ls_date))
	dw_pa-summary_detail.SetFocus()
	ib_ShowInquire = FALSE
ELSE
	ib_ShowInquire = TRUE
   wf_retrieve()
END IF

iw_this	=	This


iw_frame.im_menu.mf_enable("m_previous")
iw_frame.im_menu.mf_enable("m_next")

//added 9-12 jac

ls_date_type_ind = dw_pa-summary_header.GetItemString( 1, "date_type_ind")
If IsNull(ls_date_type_ind) Then
	ls_date_type_ind = ""
End If
//added 11-12 jac
ls_discounted_ind = dw_pa-summary_header.GetItemString( 1, "discounted_ind")
If IsNull(ls_discounted_ind) Then
	ls_discounted_ind = ""
End If
//
ls_column_name = 'availability_date_t'
if ls_date_type_ind = 'A' then
	ls_date_tag = 'Availability Date:'
else 
	ls_date_tag = 'Delivery Date:'
end if

dw_pa-summary_header.Modify(ls_column_name + '.Expression="' + ls_date_tag + '"')

ls_date_type_ind = dw_pa-summary_header.GetItemString( 1, "date_type_ind")
If IsNull(ls_date_type_ind) Then
	ls_date_type_ind = "D"
End If

if ls_date_type_ind = 'A' then
dw_pa-summary_header.object.availability_date_t.text = 'Availability Date:'
dw_pa-summary_header.object.customer_tag.visible = False
dw_pa-summary_header.object.customer_id.visible = False
dw_pa-summary_header.object.customer_name.visible = False
else 
dw_pa-summary_header.object.availability_date_t.text = 'Delivery Date:'
dw_pa-summary_header.object.customer_tag.visible = True
dw_pa-summary_header.object.customer_id.visible = True
dw_pa-summary_header.object.customer_name.visible = True
end if


end event

event activate;call super::activate;dw_pa-summary_detail.Enabled = TRUE
dw_pa_summary_plants.Enabled = TRUE

iw_frame.im_menu.mf_enable("m_previous")
iw_frame.im_menu.mf_enable("m_next")

end event

on clicked;call w_base_sheet_ext::clicked;//send(Handle(this), 132, 0, 13631733)
//send(handle(this), 20, handle(this),  33554432)
//send(handle(this), 512, 0,  5963959)
//
//open(w_test)
end on

event deactivate;dw_pa-summary_detail.Enabled = FALSE
dw_pa_summary_plants.Enabled = FALSE
if IsValid(iw_ToolTip) Then Close(iw_ToolTip)

iw_frame.im_menu.mf_Disable("m_previous")
iw_frame.im_menu.mf_Disable("m_next")

end event

event close;call super::close;Destroy( iu_orp003)
Destroy(iu_ws_orp2)
Destroy( iu_orp204)
end event

on w_pa-summary.create
int iCurrent
call super::create
this.dw_pa-summary_header=create dw_pa-summary_header
this.dw_pa_summary_plants=create dw_pa_summary_plants
this.dw_pa-summary_detail=create dw_pa-summary_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pa-summary_header
this.Control[iCurrent+2]=this.dw_pa_summary_plants
this.Control[iCurrent+3]=this.dw_pa-summary_detail
end on

on w_pa-summary.destroy
call super::destroy
destroy(this.dw_pa-summary_header)
destroy(this.dw_pa_summary_plants)
destroy(this.dw_pa-summary_detail)
end on

event ue_query;call super::ue_query;INT		li_rtn

CHAR		lc_ProcessOption, &        
			lc_AvailableDate[10]

STRING	ls_PlantData, &
			ls_DetailData, &
			ls_Availability_Date,&
			ls_Test_Date, &
			ls_descr_out, &
			ls_page_descr, &
			ls_procedure_name, &
			ls_today, &			
		   ls_date_type_ind, &
			ls_customer_id, &
			ls_detail_line, &
			ls_prod_data
	

BOOLEAN	lb_Modified = FALSE

s_Error	lstr_Error

dw_pa-summary_header.AcceptText()
dw_pa-summary_detail.AcceptText()
dw_pa_summary_plants.AcceptText()

dw_pa-summary_header.SetRedraw(FALSE)
dw_pa-summary_detail.SetRedraw(FALSE)
dw_pa_summary_plants.SetRedraw(FALSE)

lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Retriev"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

ls_page_descr = dw_pa-summary_header.GetItemString(1, "seq_num")

//9-12 jac lc_AvailableDate = STRING(dw_pa-summary_header.&
//		GetItemDate(1, "reinquire_date"), "YYYY/MM/DD")
lc_AvailableDate = STRING(dw_pa-summary_header.&
		GetItemDate(1, "availability_date"), "YYYY/MM/DD")

		
ls_today = STRING(Today(), "YYYY/MM/DD")

IF lc_AvailableDate < ls_today THEN
	MessageBox("WRONG DATE", "Can't Inquire on a Date Prior To Today's Date")
	Return
END IF		

IF	ls_page_descr = "00" THEN
	lc_ProcessOption = "P"
	IF wf_GetPlantData(ls_PlantData) <> 0 THEN 
		dw_pa-summary_header.SetRedraw(True)
		dw_pa-summary_detail.SetRedraw(True)
		dw_pa_summary_plants.SetRedraw(True)
		RETURN -1
	End IF
	IF wf_GetDetailData(ls_DetailData) <> 0 THEN 
		dw_pa-summary_header.SetRedraw(True)
		dw_pa-summary_detail.SetRedraw(True)
		dw_pa_summary_plants.SetRedraw(True)
		RETURN -1
	End IF
ELSE
	lc_ProcessOption = "D"
END IF	


		
//9-12 jac lc_AvailableDate = STRING(dw_pa-summary_header.&
//		GetItemDate(1, "reinquire_date"), "MM/DD/YYYY")
lc_AvailableDate = STRING(dw_pa-summary_header.&
		GetItemDate(1, "availability_date"), "MM/DD/YYYY")
		
ls_today = STRING(Today(), "MM/DD/YYYY")

//added 9-12 jac

  ls_date_type_ind = dw_pa-summary_header.GetItemString( 1, "date_type_ind")
  If IsNull(ls_date_type_ind) Then
	ls_date_type_ind = " "
End If
  ls_customer_id = dw_pa-summary_header.GetItemString( 1, "customer_id")
  If IsNull(ls_customer_id) Then
	 ls_customer_id = "        "
End If

//added 11-12 jac
if ib_ShowInquire = FALSE then
   ls_date_type_ind = 'A'
end if
//added 11-12 jac
ls_detail_line = dw_pa-summary_header.GetItemString( 1, "discounted_ind") + '~t'	
////// Call RPC orpo38ar
//li_Rtn = iu_orp003.nf_orpo38ar(lstr_Error, lc_ProcessOption, &
//										lc_AvailableDate, ls_page_descr, &
//										ls_PlantData, ls_DetailData, ls_descr_out, &										
//										ls_customer_id, ls_date_type_ind, ls_detail_line, &
//										ls_prod_data)

li_Rtn = iu_ws_orp2.nf_orpo38fr(lc_ProcessOption, &
									lc_AvailableDate, ls_page_descr, &
									ls_PlantData, ls_DetailData, ls_descr_out, &
									ls_customer_id, ls_date_type_ind, ls_detail_line, ls_prod_data, lstr_Error)


IF li_Rtn = 0 or li_Rtn = 5 THEN	
	This.wf_PutDetailData(ls_DetailData)
	This.wf_PutPlantData(ls_PlantData)
  	ls_Availability_Date = lc_AvailableDate
	is_prod_data = ls_prod_data
	
	dw_pa-summary_header.SetItem(1, "last_seq_num", ls_descr_out)
	dw_pa-summary_header.SetItem(1, "availability_date", DATE(ls_Availability_Date))
   //added 9-12 jac
	dw_pa-summary_header.SetItem(1, "customer_id", ls_customer_id)
   //
	dw_pa-summary_header.SetRedraw(TRUE)
	dw_pa-summary_detail.SetRedraw(TRUE)
	dw_pa_summary_plants.SetRedraw(TRUE)

	dw_pa-summary_detail.SetFocus()
	
	RETURN 0
END IF
dw_pa-summary_detail.SetRow(1)
dw_pa-summary_detail.SetColumn('product_code')
dw_pa-summary_detail.SetFocus()

dw_pa-summary_header.SetRedraw(TRUE)
dw_pa-summary_detail.SetRedraw(TRUE)
dw_pa_summary_plants.SetRedraw(TRUE)

RETURN -1
end event

event open;call super::open;is_passedValue = Message.StringPArm
end event

event ue_get_data;
			

CHOOSE CASE as_value
	CASE	'seq_num'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'seq_num')
	CASE  'last_seq_num'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'last_seq_num')
//added 8-12  jac
	CASE	'customer_id'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'customer_id')
	CASE  'customer_name'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'customer_name')
	CASE  'date_type_ind'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'date_type_ind')
	CASE	'availability_date'
		Message.StringParm = String(dw_pa-summary_header.GetItemDate(1, 'availability_date'), 'mm/dd/yyyy')	
	CASE  'description'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'description')
//added 11-12 jac
	CASE  'discounted_ind'
		Message.StringParm = dw_pa-summary_header.GetItemString(1, 'discounted_ind')
//
CASE 'is_file_desc'
		Message.StringParm = is_file_desc
	CASE 'ToolTip'
		Message.StringParm = is_description
	CASE 'CurrRow'
		Message.StringParm = is_save_row
	CASE 'CurrCol'
		Message.StringParm = is_save_col
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
END CHOOSE





end event

event resize;call super::resize;Long	ll_x = 20, &
		ll_y = 125


if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_pa-summary_detail.Resize(this.width - dw_pa-summary_detail.x - ll_x, this.height - dw_pa-summary_detail.y - ll_y)
//dw_pa-summary_detail.Resize(this.width - dw_pa-summary_detail.x - 20, this.height - dw_pa-summary_detail.y - 125)

end event

event ue_set_data(string as_data_item, string as_value);call super::ue_set_data;//added 9-12 jac
  
  string  ls_date_type_ind


choose case as_data_item
	case "customer_id"
		dw_pa-summary_header.setItem(1, 'customer_id', as_value)
	case "customer_name"
		dw_pa-summary_header.setItem(1, 'customer_name', as_value)
	case "date_type_ind"
		dw_pa-summary_header.setItem(1, 'date_type_ind', as_value)
	case "availability_date"
		dw_pa-summary_header.setItem(1, 'availability_date', Date(as_value))
	
	case "descripition"
		dw_pa-summary_header.setItem(1, 'description', as_value)	
	case "seq_num"
		dw_pa-summary_header.setItem(1, 'seq_num', as_value)
	case "last_seq_num"
		dw_pa-summary_header.setItem(1, 'last_seq_num', as_value)
//added 11-12 jac		
	case "discounted_ind"
		dw_pa-summary_header.setItem(1, 'discounted_ind', as_value)
end choose







end event

type dw_pa-summary_header from u_base_dw_ext within w_pa-summary
event ue_lbuttondown pbm_lbuttondown
event ue_vcr_previous pbm_custom40
event ue_vcr_next pbm_custom41
integer x = 105
integer y = 16
integer width = 2496
integer height = 308
integer taborder = 10
boolean enabled = false
string dataobject = "d_pa-summary_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;String	ls_Date
//9-12 jac If This.GetColumnName() = 'reinquire_date' Then
If This.GetColumnName() = 'availability_date' Then
	ls_Date = This.GetText()
	IF Not ISDATE( ls_Date) Then
		MessageBox("Invalid Date", ls_Date+ " is not a Valid Date. Please enter a valid date")
		This.SetFocus()
		return 1
		Return
	END IF
END IF
end event

event itemerror;call super::itemerror;RETURN 1
end event

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_pa_summary_plants from u_base_dw_ext within w_pa-summary
event ue_delete pbm_custom30
event ue_insert pbm_custom31
event ue_dropdown pbm_dwndropdown
integer x = 201
integer y = 352
integer width = 2798
integer height = 96
integer taborder = 20
string dataobject = "d_pa_summary_plants"
boolean border = false
end type

event ue_delete;call super::ue_delete;INT	li_Columns, &
		li_Column, &
		li_CurColumn

STRING	ls_NullString

li_CurColumn = This.GetColumn()
SETNULL(ls_NullString)

This.SetItem(1, li_Columns, ls_NullString)
This.SetRedraw(FALSE)

li_Columns = INTEGER(This.Describe("DataWindow.Column.Count"))

FOR li_Column = li_CurColumn TO li_Columns - 1  
	This.SetItem(1, li_Column, This.GetItemString(1, li_Column + 1))
NEXT

IF li_CurColumn = 11 THEN
	This.SetColumn(10)
ELSE
	This.SetColumn(11)
END IF
 
This.SetColumn(li_CurColumn)
This.SetRedraw(TRUE)
end event

event ue_insert;call super::ue_insert;This.SetRedraw(FALSE)

INT	li_Columns, &
		li_Column, &
		li_Rtn, &
		li_CurColumn

STRING	ls_NullString

li_Columns = INTEGER(This.Describe("DataWindow.Column.Count"))
SETNULL(ls_NullString)
li_CurColumn = This.GetColumn()

FOR li_Column = li_Columns TO li_CurColumn + 1 STEP -1
	This.SetItem(1, li_Column, This.GetItemString(1, li_Column - 1))
NEXT
	
This.SetItem(1, li_CurColumn, ls_NullString)
IF li_CurColumn = 11 THEN
	This.SetColumn(10)
ELSE
	This.SetColumn(11)
END IF
This.SetColumn(li_CurColumn)
This.SetRedraw(TRUE)
end event

event rbuttondown;call super::rbuttondown;STRING ls_ColumnName

ls_ColumnName = MID(This.GetObjectAtPointer(), 1, POS(This.GetObjectAtPointer(), "	"))

IF This.Describe(ls_ColumnName + ".ID") <> "!" THEN
	This.SetColumn(ls_ColumnName)
END IF

//Parent.im_Pop-Ups.m_Column.PopMenu(iw_Frame.PointerX(), iw_Frame.PointerY())
Parent.im_Pop-Ups.m_row.enabled = False
Parent.im_Pop-Ups.m_column.enabled = True
Parent.im_Pop-Ups.PopMenu(iw_Frame.PointerX(), iw_Frame.PointerY())
end event

event ue_keydown;call super::ue_keydown;CHOOSE CASE TRUE
	CASE KEYDOWN(keyDelete!)
		This.PostEvent("ue_delete")

	CASE KEYDOWN(keyInsert!)
		This.PostEvent("ue_insert")
END CHOOSE
end event

event itemchanged;call super::itemchanged;Long			ll_RowFound

String 		ls_FindExpression , &
				ls_columnname

datawindowchild	ldw_childdw

u_string_functions	lu_string_functions

ls_ColumnName = dwo.Name

If lu_string_functions.nf_IsEmpty(data) Then 
	dw_pa-summary_detail.TriggerEvent('ue_delete_column')
	Return
End If

This.GetChild('plant_code_1', ldw_ChildDW)
ll_rowfound	=	ldw_ChildDW.RowCount()
ls_FindExpression = "location_code = ~"" + Trim(data) + "~""
ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
If  ll_RowFound <= 0 Then 
	MessageBox('Invalid Data', data + ' is an invalid plant code')
	This.SelectText(1, Len(data))
	RETURN	1
END IF

end event

event constructor;call super::constructor;DataWindowChild	ldwc_DropDown

This.GetChild('plant_code_1', ldwc_DropDown)

IF ldwc_DropDown.RowCount() < 1 then
	ldwc_DropDown.SetTransObject( SQLCA)
	ldwc_DropDown.Retrieve()
END IF	
end event

event ue_dwndropdown;call super::ue_dwndropdown;DataWindowChild	ldwc_DropDown

This.GetChild(This.GetColumnName(), ldwc_DropDown)

IF ldwc_DropDown.RowCount() < 1 then
	ldwc_DropDown.SetTransObject( SQLCA)
	ldwc_DropDown.Retrieve()
END IF	
end event

event itemerror;call super::itemerror;RETURN 1
end event

type dw_pa-summary_detail from u_base_dw_ext within w_pa-summary
event ue_insert pbm_custom25
event ue_delete pbm_custom26
event ue_delete_column ( )
integer x = 37
integer y = 448
integer width = 3008
integer height = 1212
integer taborder = 30
string dataobject = "d_pa-summary_detail"
boolean vscrollbar = true
boolean border = false
end type

event ue_insert;call super::ue_insert;IF This.RowCount() < ii_max_rows THEN 
	This.InsertRow(This.GetRow())
End IF
end event

event ue_delete;Long		ll_row, &
			ll_pos, &
			ll_RowCount, &
			ll_delete_count
		
string  	ls_prod, &
			ls_save_prod

INT		li_counter


This.SetRedraw(FALSE)
ll_RowCount = dw_pa-summary_detail.RowCount()	

ll_row = This.GetRow()
ll_delete_count = ll_row


IF ll_row <> This.RowCount() THEN
	ls_prod = dw_pa-summary_detail.GetItemString(ll_row, "product_code")
	This.DeleteRow(ll_row)
	ll_delete_count = ll_delete_count + 1
	ll_pos = pos(ls_prod, "*")
	if ll_pos > 0 then
		li_counter = ll_row 
		do
//			ls_prod = dw_pa-summary_detail.GetItemString(li_counter, "product_code")
			ls_save_prod = dw_pa-summary_detail.GetItemString(li_Counter, "save_prod_ind")
			if ls_save_prod = '00' then
				This.DeleteRow(li_Counter)
				ll_delete_count = ll_delete_count + 1
			else
				ll_delete_count = 31
			end if
//			li_counter = li_counter + 1
		loop while ll_delete_count < 31
	END IF
END IF

IF ll_row = ii_max_rows THEN
	This.DeleteRow(ii_max_rows)
	This.InsertRow(0)
END IF

ll_RowCount = dw_pa-summary_detail.RowCount()	
if ll_RowCount < 31 then
	ll_RowCount = ll_RowCount + 1
	do
		This.InsertRow(ll_RowCount)
		ll_RowCount = ll_RowCount + 1
	loop while ll_RowCount < 31
end if
		
This.SetRedraw(TRUE)

end event

event ue_delete_column;call super::ue_delete_column;Integer			li_curcolumn

String			ls_array[]

Long				ll_rowcount


li_curcolumn = dw_pa_summary_plants.GetColumn()
ll_rowcount = This.RowCount()
ls_array[1] = ''
Choose Case li_curcolumn
	Case 1
		This.object.boxes_1.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_1.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_1.current[1,ll_rowcount] = ls_array[]
	Case 2
		This.object.boxes_2.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_2.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_2.current[1,ll_rowcount] = ls_array[]
	Case 3
		This.object.boxes_3.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_3.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_3.current[1,ll_rowcount] = ls_array[]
	Case 4
		This.object.boxes_4.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_4.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_4.current[1,ll_rowcount] = ls_array[]
	Case 5
		This.object.boxes_5.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_5.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_5.current[1,ll_rowcount] = ls_array[]
	Case 6
		This.object.boxes_6.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_6.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_6.current[1,ll_rowcount] = ls_array[]
	Case 7
		This.object.boxes_7.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_7.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_7.current[1,ll_rowcount] = ls_array[]
	Case 8
		This.object.boxes_8.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_8.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_8.current[1,ll_rowcount] = ls_array[]
	Case 9
		This.object.boxes_9.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_9.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_9.current[1,ll_rowcount] = ls_array[]
	Case 10
		This.object.boxes_10.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_10.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_10.current[1,ll_rowcount] = ls_array[]
	Case 11
		This.object.boxes_11.current[1,ll_rowcount] = ls_array[]
		This.object.indicator_11.current[1,ll_rowcount] = ls_array[]
		This.object.rslv_ind_11.current[1,ll_rowcount] = ls_array[]
End choose
end event

event editchanged;call super::editchanged;CHOOSE CASE This.GetColumnName()
CASE "product_code"
	IF Len(This.GetText()) > 9  AND This.GetText() > "        " THEN This.SetColumn("age_code")
END CHOOSE
end event

event itemchanged;call super::itemchanged;Long	ll_Row, ll_RowCount, ll_pos

String	ls_product, ls_description, &
			ls_product_first, ls_save_prod, &
			ls_business_rule, ls_discounted_ind

u_project_functions lu_project_functions
u_string_functions	lu_stringFunction


ll_Row = row
ll_RowCount = This.RowCount()

//IF ll_Row = ii_max_rows THEN RETURN
IF ll_Row = ll_RowCount AND ll_Row < ii_max_rows THEN ll_RowCount  = This.InsertRow(0)

ls_discounted_ind = dw_pa-summary_header.GetItemString( 1, "discounted_ind")

CHOOSE CASE Lower(dwo.Name)
	CASE "age_code"
	CASE "product_code"
		If ls_discounted_ind = 'Y' Then
			This.SetItem( ll_Row, 'age_Code', 'F')
		Else
			This.SetItem( ll_Row, 'age_Code',lu_project_functions.nf_get_age_code(Data))
		End if
		ls_save_prod = dw_pa-summary_detail.GetItemString(row, "save_prod_ind")
		if ls_save_prod = '00' or isnull(ls_save_prod)  then
			dw_pa-summary_detail.SetItem(row, "save_prod_ind", "99")
		end if	
		if ls_save_prod = '  ' then
			dw_pa-summary_detail.SetItem(row, "save_prod_ind", "99")
		end if
		ll_pos = pos(data, "*")
		if ll_pos > 0 then
			ls_business_rule = This.GetItemString(row,"Business_rule")
			if lu_stringFunction.nf_ISEmpty(ls_Business_Rule) then
				ls_Business_Rule = Fill('V',41)
			else
				ls_Business_Rule = 'V' + Mid(ls_Business_rule,2)
			end if	
			This.SetItem(row,"Business_Rule",ls_Business_Rule)
			iw_frame.SetMicroHelp(" ")
		end if
		CASE ELSE
END CHOOSE
//sap
//IF row > 0 AND ll_row <> row THEN
//	IF dwo.name = "product_code" THEN
//		ls_product = dw_pa-summary_detail.GetItemString(row, "product_code")
//		ls_product = TRIM(ls_product) + Space(10 - len(Trim(ls_product)))
//		ls_product_first = left(ls_product,1)
//	end if
//end if
//sap

IF row > 0 AND ll_row <> row THEN
	IF dwo.name = "product_code" THEN
		ls_product = dw_pa-summary_detail.GetItemString(row, "product_code")
		ls_product = TRIM(ls_product) + Space(10 - len(Trim(ls_product)))
		Select Short_description
			INTO :ls_description
			FROM sku_products
			WHERE sku_product_code = :ls_product;
			IF SQLCA.sqlcode = 100 THEN
				ls_save_prod = dw_pa-summary_detail.GetItemString(row, "save_prod_ind")
				if ls_save_prod = '99' then
					dw_pa-summary_detail.SetItem(row, "save_prod_ind", "00")
				end if	
				Return 1
				dw_pa-summary_detail.SetFocus()
			End IF
	End IF
End IF
end event

event constructor;call super::constructor;This.SelectRow(This.GetRow(), False)
ib_firstcolumnonnextrow = True

end event

event rbuttondown;call super::rbuttondown;Parent.im_Pop-Ups.m_row.enabled = True
Parent.im_Pop-Ups.m_Column.enabled = False
Parent.im_Pop-Ups.PopMenu(iw_Frame.PointerX(), iw_Frame.PointerY())

//Parent.im_Pop-Ups.m_row.PopMenu(iw_Frame.PointerX(), iw_Frame.PointerY())
end event

event itemerror;call super::itemerror;Return 1
end event

event ue_setbusinessrule;call super::ue_setbusinessrule;String	ls_Business_Rule, ls_product
long		ll_pos
u_string_functions	lu_stringFunction

ls_business_rule = This.GetItemString(row,"Business_rule")
ls_product = This.GetItemString(row, "product_code")
if lu_stringFunction.nf_ISEmpty(ls_Business_Rule) then ls_Business_Rule = Fill('V',41)
ll_pos = pos(ls_product, "*")
if ll_pos = 0 then
	ls_Business_Rule = value + Mid(ls_Business_rule,2)
else
	iw_frame.SetMicroHelp(" ")
end if
This.SetItem(row,"Business_Rule",ls_Business_Rule)

Return 0

end event

event ue_mousemove;call super::ue_mousemove;String	ls_product, &	
			ls_boxes
					
long ll_desc_row_count, &
		ll_desc_row

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50
IF ia_apptool.ToolBarTips = TRUE THEN
	If row > 0   then
		choose case dwo.name 
			case 'product_code'
				il_col = 1
			case 'age_code'
				il_col = 2
			case 'boxes_1'
				il_col = 3
			case 'boxes_2'
				il_col = 4
			case 'boxes_3'
				il_col = 5
			case 'boxes_4'
				il_col = 6
			case 'boxes_5'
				il_col = 7
			case 'boxes_6'
				il_col = 8
			case 'boxes_7'
				il_col = 9
			case 'boxes_8'
				il_col = 10
			case 'boxes_9'
				il_col = 11
			case 'boxes_10'
				il_col = 12
			case 'boxes_11'
				il_col = 13
			case 'boxes_12'
				il_col = 14
			case 'boxes_13'
				il_col = 15
		end choose		
				
		If ((row <> il_save_row) or (il_col <> il_save_col)) Then
			choose case dwo.name
				case 'product_code'		
					ls_product = dw_pa-summary_detail.GetItemString(row, "product_code")
					ls_product = TRIM(ls_product) + space(10 - len(TRIM(ls_product)))
					IF IsValid(iw_tooltip) Then
						close(iw_tooltip)
					END IF
					is_save_row = string(row)
					il_save_col = 1
					is_save_col = '1'	
					il_save_row = Row
					
					SELECT short_description   
						INTO :is_description
						FROM sku_products  
						WHERE sku_product_code = :ls_product;   
					if len(trim(is_description)) > 0 then 
						is_description =  '99' + "~t" + '99' + "~t" + is_description + '~r~n'
						openwithparm(iw_tooltip, parent, iw_frame)
						is_description = ""
					ELSE
						iw_frame.SetMicroHelp("Ready")
					END IF
				//		
				case 'age_code'		
					IF IsValid(iw_tooltip) Then
						close(iw_tooltip)
					END IF
					is_save_row = string(row)
					il_save_col = 2
					is_save_col = ''	
					il_save_row = Row
					
					iw_frame.SetMicroHelp("Ready")
				//		
				
				case 'boxes_1'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_1")
						IF IsValid(iw_tooltip) Then
								close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 3
						is_save_col = '1'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End if
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//		
				case 'boxes_2'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_2")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 4
						is_save_col = '2'	
						il_save_row = Row						
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//		
				case 'boxes_3'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_3")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF
						is_save_row = string(row)
						il_save_col = 5
						is_save_col = '3'
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""							
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_4'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_4")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 6
						is_save_col = '4'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_5'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_5")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF		
						is_save_row = string(row)
						il_save_col = 7
						is_save_col = '5'	
						il_save_row = Row							
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_6'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_6")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 8
						is_save_col = '6'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_7'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_7")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 9
						is_save_col = '7'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_8'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_8")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 10
						is_save_col = '8'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_9'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_9")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF
						is_save_row = string(row)
						il_save_col = 11
						is_save_col = '9'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_10'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_10")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF
						is_save_row = string(row)
						il_save_col = 12
						is_save_col = '10'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_11'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_11")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 13
						is_save_col = '11'	
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_12'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_12")
						IF IsValid(iw_tooltip) Then
							close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 14
						is_save_col = '12'
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if
			//	
				case 'boxes_13'
						ls_boxes = dw_pa-summary_detail.GetItemString(row, "boxes_13")
						IF IsValid(iw_tooltip) Then
								close(iw_tooltip)
						END IF	
						is_save_row = string(row)
						il_save_col = 15
						is_save_col = '13'
						il_save_row = Row
						if len(trim(ls_boxes)) > 0 and IsNumber(ls_boxes) then // and ls_boxes > '0' then
							is_description = is_prod_data
							If Len(is_description) > 0 Then  // pjm 08/06/2014 added error checking
								openwithparm(iw_tooltip, parent, iw_frame)
							End If
							is_description = ""	  
						else
							iw_frame.SetMicroHelp("Ready")
						end if	
			END choose	
		ELSE	
			IF IsValid(iw_tooltip)  AND ((row <> il_save_row) or (il_save_col <> il_col)) Then
				close(iw_tooltip)
			END IF
		END IF
  end if
end if
end event

