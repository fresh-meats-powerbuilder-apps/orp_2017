﻿$PBExportHeader$w_avail_sum_inq.srw
forward
global type w_avail_sum_inq from w_base_response_ext
end type
type dw_inquire from u_base_dw_ext within w_avail_sum_inq
end type
type uo_groups from u_group_list within w_avail_sum_inq
end type
end forward

global type w_avail_sum_inq from w_base_response_ext
integer width = 1371
integer height = 976
long backcolor = 67108864
dw_inquire dw_inquire
uo_groups uo_groups
end type
global w_avail_sum_inq w_avail_sum_inq

type variables
w_avail_sum	iw_parent
end variables

event ue_base_ok;Date			ldt_inq_date, &
				ldt_days_out
				
Integer		li_parange, li_rtn

string  		ls_owner, &
				ls_prod_group_id, &
				ls_prod_group_desc
				
u_string_functions	lu_strings


dw_inquire.AcceptText()

ldt_inq_date = dw_inquire.GetItemDate(1, 'inquire_date')

iw_parent.Event ue_get_data('pa range')
li_parange = Integer(Message.StringParm)
ldt_days_out = RelativeDate(Today(), li_parange)

If ldt_inq_date < Today() Or ldt_inq_date > ldt_days_out Then
	iw_frame.SetMicroHelp('Inquire Date must be after today and before ' + &
			String(RelativeDate(ldt_days_out, 1), 'mm/dd/yyyy'))
	dw_inquire.SetFocus()
	Return
End If

// ibdkdld ls_owner = uo_div_group_options.uf_get_owner()
//ls_owner = ole_groups.object.systemname()
ls_owner = uo_groups.uf_get_owner()
If lu_strings.nf_IsEmpty(ls_owner) Then
	iw_frame.SetMicroHelp("Product System is a required field")
	return
End If

//li_rtn = uo_div_group_options.uf_get_sel_id(ls_prod_group_id)
//ls_prod_group_id = string(ole_groups.object.groupID()
ls_prod_group_id = string(uo_groups.uf_get_sel_id(ls_prod_group_id))
If lu_strings.nf_isempty(ls_prod_group_id) Then
	iw_frame.SetMicroHelp("Product Group is a required field")
	Return
else
	//ls_prod_group_desc = ole_groups.object.Groupdescription()
	uo_groups.uf_get_sel_desc(ls_prod_group_desc)
End if

iw_parent.event ue_set_data('GroupID',ls_prod_group_id)
iw_parent.event ue_set_data('GroupDesc',ls_prod_group_desc)
iw_parent.event ue_set_data('group_owner',ls_owner)

CloseWithReturn(This, String(ldt_inq_date, 'mm/dd/yyyy'))
end event

event open;call super::open;iw_parent = message.powerobjectparm

This.Title = iw_parent.Title + " Inquire"

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, '')
end event

event ue_postopen;String		ls_inquire_date, &
				ls_owner, &
				ls_group
long			ll_group


dw_inquire.InsertRow(0)
// ibdkdld added new control removed old control 12/27/02
//uo_div_group_options.uf_initialize(1, sqlca.userid, sqlca.dbpass)
//ole_groups.object.GroupType(3)
//ole_Groups.object.LoadObject()
uo_groups.uf_load_groups('P')


iw_parent.Event ue_get_data('inquire date')
ls_inquire_date = Message.StringParm
dw_inquire.SetItem(1, 'inquire_date', Date(ls_inquire_date))

//iw_parent.event ue_get_data('group_owner')
//ls_owner = Message.StringParm
//if not isnull(ls_owner) then
//	uo_div_group_options.uf_set_owner(ls_owner)
//	iw_parent.event ue_get_data('group_id')
//	ls_group = Message.StringParm
//	ll_group = long(ls_group)
//	uo_div_group_options.uf_select_group(ls_owner, ll_group)
//end if
dw_inquire.SetFocus()
end event

on w_avail_sum_inq.create
int iCurrent
call super::create
this.dw_inquire=create dw_inquire
this.uo_groups=create uo_groups
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_inquire
this.Control[iCurrent+2]=this.uo_groups
end on

on w_avail_sum_inq.destroy
call super::destroy
destroy(this.dw_inquire)
destroy(this.uo_groups)
end on

type cb_base_help from w_base_response_ext`cb_base_help within w_avail_sum_inq
integer x = 782
integer y = 736
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_avail_sum_inq
integer x = 453
integer y = 736
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_avail_sum_inq
integer x = 123
integer y = 736
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_avail_sum_inq
integer taborder = 0
end type

type dw_inquire from u_base_dw_ext within w_avail_sum_inq
integer x = 41
integer y = 32
integer width = 727
integer height = 104
integer taborder = 10
string dataobject = "d_avail_sum"
boolean border = false
end type

type uo_groups from u_group_list within w_avail_sum_inq
integer y = 132
integer taborder = 30
boolean bringtotop = true
end type

on uo_groups.destroy
call u_group_list::destroy
end on


Start of PowerBuilder Binary Data Section : Do NOT Edit
05w_avail_sum_inq.bin 
2000000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000d3ebf87001c2b04e00000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000d3ebf87001c2b04ed3ebf87001c2b04e000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
21ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d700073006e006c0020006e006f002000670070005b006d00620064005f00650064007800650063006500740075005d006500720000006d00650074006f006800650074006f0069006c006b006e00740073007200610020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f006500640064006100690076006500730000005d00650072006f006d00650074006f0068006c0074006e00690073006b006f00740020007000200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006e00750064006100690076006500730000005d00650072006f006d006500740065007200750071007300650020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006500720075007100730065005d007400720000006d00650074006f00730065006e00650020006400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006f007000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
15w_avail_sum_inq.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
