﻿$PBExportHeader$w_pa-summary_save-as.srw
$PBExportComments$File-Save Response Window For PA-Summary
forward
global type w_pa-summary_save-as from w_base_response_ext
end type
type dw_filenumber from u_base_dw_ext within w_pa-summary_save-as
end type
type st_1 from statictext within w_pa-summary_save-as
end type
end forward

global type w_pa-summary_save-as from w_base_response_ext
integer x = 818
integer y = 432
integer width = 1902
integer height = 376
string title = "Save As"
long backcolor = 12632256
dw_filenumber dw_filenumber
st_1 st_1
end type
global w_pa-summary_save-as w_pa-summary_save-as

type variables
s_error		istr_error_info

u_orp204		iu_orp204
u_ws_orp4		iu_ws_orp4

s_error			istr_error

w_base_sheet	iw_parent
end variables

on w_pa-summary_save-as.create
int iCurrent
call super::create
this.dw_filenumber=create dw_filenumber
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_filenumber
this.Control[iCurrent+2]=this.st_1
end on

on w_pa-summary_save-as.destroy
call super::destroy
destroy(this.dw_filenumber)
destroy(this.st_1)
end on

event open;call super::open;iw_parent = Message.PowerObjectParm
end event

event ue_postopen;iw_parent.event ue_get_data('seq_num')
dw_filenumber.SetItem(1, 'seq_num', Message.stringparm)
iw_parent.event ue_get_data('last_seq_num')
if Message.stringparm = 'Current' then
	dw_filenumber.SetItem(1, 'description', '       ')
else
	dw_filenumber.SetItem(1, 'description', Message.stringparm)
end if
dw_filenumber.SetColumn('seq_num')
end event

event close;call super::close;Destroy(iu_orp204)
Destroy(iu_ws_orp4)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pa-summary_save-as
boolean visible = false
integer x = 1129
integer y = 288
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pa-summary_save-as
integer x = 1600
integer y = 160
end type

event cb_base_cancel::clicked;call super::clicked;CloseWithReturn(Parent, "A")

end event

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pa-summary_save-as
integer x = 1600
integer y = 32
end type

event cb_base_ok::clicked;call super::clicked;DataWindowChild	ldwc_temp

string 				ls_info_out, &
						ls_temp, &
						ls_seq_num, &
						ls_desc

Integer	li_message

Long 	ll_rowcount

u_string_functions	lu_string

dw_filenumber.AcceptText()
ls_seq_num = dw_filenumber.GetItemString(1, 'seq_num')

dw_filenumber.GetChild('seq_num', ldwc_temp) 
ll_rowcount = ldwc_temp.RowCount()

ls_temp = "file_number = '" + ls_seq_num + "'"
IF ldwc_temp.Find(ls_temp, 1, ldwc_temp.rowcount()) > 0 THEN
	li_message = MessageBox('File Override', 'Do you want to override this file', question!, yesno!)
	IF li_message = 2 THEN 
		dw_filenumber.SetFocus()
		Return
	END IF
END IF

ls_desc = dw_filenumber.GetItemString(1, 'description')
IF lu_string.nf_IsEmpty(ls_desc) Then
	dw_filenumber.SetItem(1,'description', "")
END IF
ls_info_out = ls_seq_num + '~t' + dw_filenumber.GetItemString(1, 'description')
CloseWithReturn(Parent, ls_info_out)
end event

type cb_browse from w_base_response_ext`cb_browse within w_pa-summary_save-as
end type

type dw_filenumber from u_base_dw_ext within w_pa-summary_save-as
event constructor pbm_constructor
integer x = 18
integer y = 92
integer width = 1577
integer height = 172
string dataobject = "d_pa_summary_saveas"
boolean border = false
end type

event constructor;call super::constructor;InsertRow(0)

end event

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_Temp

Long	ll_Row

This.GetChild("seq_num", ldwc_Temp)
ll_Row = ldwc_Temp.Find("file_number='" +Data+ "'", 1, ldwc_temp.RowCount())
if ll_Row > 0 Then
	This.SetItem(1, "description", ldwc_temp.GetItemString(ll_row, "file_description"))
End if

end event

event ue_postconstructor;call super::ue_postconstructor;Char	lc_ProcessOption, &
		lc_AvailableDate[10]			

DataWindowChild	ldwc_file_number, &
						ldwc_file_descr

Integer	li_Rtn

String	ls_description_out, &
			ls_plantdata, &
			ls_detaildata, &
			ls_descr_out, &
			ls_page_descr
			
s_Error	lstr_Error

lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Retriev"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")			

lc_ProcessOption = "Q"
lc_AvailableDate = ""
ls_page_descr = ""
ls_PlantData = ""
ls_DetailData = ""
ls_descr_out = ""

IF NOt IsValid(iu_orp204) Then iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4
////call rpc 
//li_Rtn = iu_orp204.nf_orpo81br_pa_summary(istr_Error, lc_ProcessOption, &
//										lc_AvailableDate, ls_page_descr, &
//										ls_PlantData, ls_DetailData, ls_descr_out)			

li_Rtn = iu_ws_orp4.nf_orpo81fr(istr_Error, lc_ProcessOption, lc_AvailableDate, ls_page_descr, ls_descr_out, ls_PlantData, ls_DetailData, SQLCA.userid)

This.GetChild('seq_num', ldwc_file_number)
IF NOT ISVAlid(ldwc_file_number)Then
	MessageBox("","seq_number not valid")
	return
END IF
This.SetFocus()

This.GetChild('description', ldwc_file_descr)
IF NOT ISVAlid(ldwc_file_descr)Then
	MessageBox("","description not valid")
	return
END IF

ldwc_file_number.ImportString(ls_descr_out)
ldwc_file_number.ShareData(ldwc_file_descr)
end event

type st_1 from statictext within w_pa-summary_save-as
integer x = 1102
integer y = 20
integer width = 430
integer height = 76
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = " ** Unused File"
boolean focusrectangle = false
end type

