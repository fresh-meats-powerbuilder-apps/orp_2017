﻿$PBExportHeader$w_avail_sum.srw
forward
global type w_avail_sum from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_avail_sum
end type
type ole_spreadsheet from olecustomcontrol within w_avail_sum
end type
type st_1 from statictext within w_avail_sum
end type
end forward

global type w_avail_sum from w_base_sheet_ext
string title = "Product Availability by Group"
long backcolor = 67108864
dw_header dw_header
ole_spreadsheet ole_spreadsheet
st_1 st_1
end type
global w_avail_sum w_avail_sum

type variables
Integer		ii_PARange, &
		ii_current

u_pas203		iu_pas203
u_ws_pas_share	iu_ws_pas_share

string		is_groupid, &
		is_groupdesc, &
		is_group_owner


end variables

forward prototypes
public function string wf_lefttrim (string as_trimleft)
public function boolean wf_retrieve ()
end prototypes

public function string wf_lefttrim (string as_trimleft);if right(as_trimleft,1) = " " then
	as_trimleft = left(as_trimleft, len(as_trimleft) - 1)
	as_trimleft = wf_lefttrim(as_trimleft)
end if

return as_trimleft
end function

public function boolean wf_retrieve ();Integer			li_count

String			ls_heading, &
					ls_out_data, &
					ls_plant, &
					ls_input

Long				ll_row, &
					ll_col, &
					ll_loop, &
					ll_pos, &
					ll_temp
					
S_error			lstr_error_info

u_string_functions	lu_string_functions

openwithparm(w_avail_sum_inq, This)
ls_input = Message.StringParm

If ls_input = '' Then Return False

dw_header.SetItem(1, 'inquire_date', Date(ls_input))

This.SetRedraw( False)

ole_spreadsheet.object.EnableProtection = False

ole_spreadsheet.object.ClearRange(1, 1, ole_spreadsheet.object.MaxRow, &
		ole_spreadsheet.object.MaxCol, 3)

ls_input += '~t' + is_groupid

st_1.text = is_groupdesc + " Product Availability"

iu_ws_pas_share.nf_pasp58fr( &
			lstr_error_info, &
			ls_Input, &
			ls_Out_Data)


If lu_string_functions.nf_IsEmpty(ls_out_data) Then
//	ole_spreadsheet.Visible = False
	This.SetRedraw(True)
	Return False
End IF

ole_spreadsheet.object.SheetName(1,st_1.text)


ls_heading = lu_string_functions.nf_GetToken(ls_out_data, '~r~n')

//Set up the Data
ole_spreadsheet.object.SetTabbedText(3, 1, Ref ll_row, Ref ll_col, &
		False, ls_out_data)
ll_row ++
ll_row ++

ole_spreadsheet.object.MaxRow = ll_row
ole_spreadsheet.object.MaxCol = ll_col
ole_spreadsheet.object.BackColor = 12632256

ole_spreadsheet.object.SetSelection(1, 2, ll_row, 2)
ole_spreadsheet.object.SetAlignment(7, False, 3, 0)   


ll_loop = 1
ole_spreadsheet.object.TextRC(1, ll_Loop, "Product")   
ole_spreadsheet.object.TextRC(2, ll_Loop, "Code")   
ole_spreadsheet.object.SetSelection(1, ll_loop, 1, ll_loop)
ole_spreadsheet.object.SetAlignment(7, False, 3, 0)   

ll_loop ++
ole_spreadsheet.object.TextRC(2, ll_Loop, "Age")   
ole_spreadsheet.object.SetSelection(2, ll_loop, 2, ll_loop)
ole_spreadsheet.object.SetAlignment(7, False, 3, 0)   


For li_count = 3 to ll_col 
	ll_Loop++
	ls_plant = Trim(lu_string_functions.nf_GetToken(ls_heading, "~t"))
	ole_spreadsheet.object.TextRC(2, ll_Loop, Trim(ls_plant))   
	ole_spreadsheet.object.SetSelection(2, ll_Loop, 2, ll_Loop)
	ole_spreadsheet.object.SetAlignment(7, True, 3, 0)
Next

ll_pos = (ll_loop / 2) + 2
ole_spreadsheet.object.TextRC(1, ll_pos, "Plant")   

ole_spreadsheet.object.SetColWidth(1, 1, 3000, False)
ole_spreadsheet.object.SetColWidth(2, 2, 1000, False)
ole_spreadsheet.object.SetColWidth(3, ll_col, 1700, False)

ole_spreadsheet.object.FixedRow = 1
ole_spreadsheet.object.FixedRows = 2
ole_spreadsheet.object.FixedCols = 1
ole_spreadsheet.object.FixedCols = 2
ole_spreadsheet.object.SetSelection(3, 1, ll_row, ll_col)
ole_spreadsheet.object.SetBorder(1,1,1,1,1,0,0,0,0,0,0)
ole_spreadsheet.object.SetProtection(True, False)
ole_spreadsheet.object.EnableProtection(True)
ole_spreadsheet.object.SetSelection(1,1,1,1)

ole_spreadsheet.object.SetActiveCell(3, 1)

This.SetRedraw( True)

ole_spreadsheet.SetFocus()

Return True
end function

event ue_get_data;Choose Case as_value
	Case 'pa range'
		Message.StringParm =  String(ii_parange)
	Case 'inquire date'
		Message.StringPArm =  String(dw_header.GetItemDate(1, 'inquire_date'), 'mm/dd/yyyy')
	Case 'group_id'
		Message.StringPArm = left(is_groupid, len(is_groupid) - 2)
	Case 'group_owner'
		Message.StringPArm = is_group_owner
	Case 'GroupDesc'
		Message.StringPArm = is_groupdesc
	Case Else
		Message.StringParm = ''
End Choose



end event

event open;call super::open;String		ls_PARange


If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	Return
End if

ii_PARange = Integer(ls_PARange)

IF Not IsValid( iu_Pas203) Then
   iu_Pas203 = Create u_Pas203
End IF

iu_ws_pas_share = Create u_ws_pas_share

If Message.ReturnValue = -1 Then
	Close(This)
End If


end event

on w_avail_sum.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.ole_spreadsheet=create ole_spreadsheet
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.ole_spreadsheet
this.Control[iCurrent+3]=this.st_1
end on

on w_avail_sum.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.ole_spreadsheet)
destroy(this.st_1)
end on

event ue_postopen;call super::ue_postopen;wf_retrieve()
end event

event close;call super::close;Destroy( iu_Pas203)
Destroy iu_ws_pas_share
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_Enable('m_print')

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_disable('m_print')

end event

event ue_fileprint;call super::ue_fileprint;
If Not ib_print_ok Then Return
//ibdkdld 12/27/02
ole_spreadsheet.object.PrintHeader = '&CInquire Date ' + String( &
		dw_header.GetItemDate(1, 'inquire_date'), 'mm/dd/yyyy') + '~r~n' + &
		st_1.text //' &C&A' + '~r~n'
//ole_spreadsheet.object.PrintHeader = '&LInquire Date ' + String( &
//		dw_header.GetItemDate(1, 'inquire_date'), 'mm/dd/yyyy') + &
//		' &C&A' 
//ole_spreadsheet.object.SheetName(1, is_groupdesc + ' Product Availability')
ole_spreadsheet.object.FilePrint(False)
Return 
end event

event ue_set_data;u_string_functions	lu_strings
string					ls_groupdesc

choose case as_data_item
	case 'GroupID'
		is_groupid = as_value
	case 'GroupDesc'
		ls_groupdesc = left(as_value, len(as_value) - 2)
		ls_groupdesc = wf_lefttrim(left(ls_groupdesc, len(ls_groupdesc) - 2))
		is_groupdesc = ls_groupdesc
	case 'group_owner'
		is_group_owner = as_value
end choose
end event

event resize;call super::resize;ole_spreadsheet.Resize(this.width - ole_spreadsheet.x - il_BorderPaddingWidth, &
								this.height - ole_spreadsheet.y - il_BorderPaddingHeight)
end event

type dw_header from u_base_dw_ext within w_avail_sum
integer x = 18
integer y = 20
integer width = 745
integer height = 176
string dataobject = "d_avail_sum"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
This.SetItem(1, 'inquire_date', RelativeDate(Today(), 7 - DayNumber(Today())))
This.object.inquire_date.protect = 1
This.object.inquire_date.BackGround.Color = 67108864//12632256

end event

type ole_spreadsheet from olecustomcontrol within w_avail_sum
event click ( long nrow,  long ncol )
event dblclick ( long nrow,  long ncol )
event canceledit ( )
event selchange ( )
event startedit ( ref string editstring,  ref integer cancel )
event endedit ( ref string editstring,  ref integer cancel )
event startrecalc ( )
event endrecalc ( )
event topleftchanged ( )
event objclick ( ref string objname,  long objid )
event objdblclick ( ref string objname,  long objid )
event rclick ( long nrow,  long ncol )
event rdblclick ( long nrow,  long ncol )
event objvaluechanged ( ref string objname,  long objid )
event modified ( )
event mousedown ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mouseup ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event mousemove ( integer button,  integer shift,  long ocx_x,  long ocx_y )
event objgotfocus ( ref string objname,  long objid )
event objlostfocus ( ref string objname,  long objid )
event validationfailed ( ref string pentry,  long nsheet,  long nrow,  long ncol,  ref string pshowmessage,  ref integer paction )
event keypress ( ref integer keyascii )
event keydown ( ref integer keycode,  integer shift )
event keyup ( ref integer keycode,  integer shift )
event ue_postconstuctor ( )
integer x = 5
integer y = 224
integer width = 2821
integer height = 1100
integer taborder = 2
boolean bringtotop = true
long backcolor = 67108864
string binarykey = "w_avail_sum.win"
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
end type

event constructor;//ole_spreadsheet.object.SheetName(1, 'CAB Product Availability')
ole_spreadsheet.object.AllowDesigner = False
ole_spreadsheet.object.ShowColHeading = False
ole_spreadsheet.object.ShowRowHeading = False
ole_spreadsheet.object.PrintNoColor = True
ole_spreadsheet.object.ShowSelections = 0
ole_spreadsheet.object.ShowHScrollBar = 1
ole_spreadsheet.object.ShowVScrollBar = 1

end event

type st_1 from statictext within w_avail_sum
integer x = 9
integer y = 112
integer width = 2807
integer height = 96
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "CAB Product Availability"
alignment alignment = center!
boolean focusrectangle = false
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Ew_avail_sum.bin 
2400001600e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000007fffffffe00000004000000050000000600000008fffffffe00000009fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000005d3cbb0001d24f1e0000000300000c000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000540000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000020000063100000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004042badc511ce5e58415210b601004853000000005d3cbb0001d24f1e5d3cbb0001d24f1e00000000000000000000000000000001fffffffe000000030000000400000005000000060000000700000008000000090000000a0000000b0000000c0000000d0000000e0000000f000000100000001100000012000000130000001400000015000000160000001700000018000000190000001afffffffe0000001c0000001d0000001e0000001f000000200000002100000022000000230000002400000025000000260000002700000028000000290000002a0000002b0000002c0000002d0000002e0000002ffffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
2Dffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f004300790070006900720068006700200074006300280020002900390031003500390056002000730069006100750020006c006f00430070006d006e006f006e0065007300740020002c006e0049002e006300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fffe00020206042badc511ce5e58415210b60100485300000001fb8f0821101b01640008ed8413c72e2b000000300000060100000008000001000000004800000101000000500000010200000058000001030000006000000104000000680000010500000070000001060000007c000000000000057800000003000100000000000300003fc50000000300001c6c0000000300000060000000020000000100000008000000010000000000000041000004f300090000ee000505000000040014ef000000000000000000ffffff00ffffffff3dfff3ff00000012000000000000380001000000310258000000c814907fff00000000010500000061697241c814316cff0000000002bc7f0000000072410500316c61690200c814907fff00000000010500000061697241c814316cff0002000002bc7f0000000072410500316c61690000c814907fff00000000010500000061697241051a1e6c24221700232c2322295f302322285c3b2c2322245c302323061f1e2924221c00232c2322295f302365525b3b285c5d64232224223023232c201e295c221d00072c2322242e302323295f303022285c3b2c2322242e302323295c30300008251e2224222223232c2330302e305b3b295f5d6465522422285c232c2322302e30231e295c3032002a352422285f23202a223023232c5f3b295f22242228285c202a23232c233b295c302422285f22202a22295f222d40285f3b2c1e295f5f29002923202a283023232c5f3b295f5c202a28232c2328295c30232a285f3b222d22205f3b295f295f4028002c3d1e22285f3a202a222423232c2330302e305f3b295f22242228285c202a23232c2330302e305f3b295c222422282d22202a5f3f3f22285f3b291e295f4031002b34202a285f23232c2330302e305f3b295f5c202a28232c2328302e30233b295c30202a285f3f222d223b295f3f5f40285f0005ed2900000000000003ec0014e000f5000000c00020ff0000002000000000e00000000000011420fff5000020c0c400000000000000000114e000f5000000c0c420ff0000002000000000e00000000000021420fff5000020c0c400000000000000000214e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e00000000000001420fff5000020c0c400000000000000000014e000f5000000c0c420ff0000002000000000e000000000000014200001000020c00000000000000000000514e000f5000800c0c820ff0000002000000000e00000000600051420fff5000020c0c800000000000000000514e000f5000c00c0c820ff0000002000000000e00000000a00051420fff5000020c0c800000000000000000514e000f5000d00c0c820ff0000002000000000e00000000000041412fff0000020c04800000000000000000009850068530600317465650500090a010d001000640c0010000011d2f1a9fc3f50624d2a00015f012b000000012500018c00ff81000100031404c11541260261500708262065670000835026000084000000003fe8000000000027e80000000000283f0000000000293ff000000000a13ff0006400012201000100060001000000000000000000e00000000000003fe00000000000013f000000000000000018f70000ccff009200ffffff00c0c0c03fff00ff0000000000000000000010f200000000000000000000000015f6ffff150015004000ff00030f1d060000000000010000000000000a3e0000000002360000000064a0000099006400000a092600000008000000000000000100010600000009006c6966006d616e6501030065000c0000735f00006b636f74706f727001040073000c00006f620000726564726c7974730101006500090000655f00006e65747802007874090000015f000000657478650079746e00000105000000086e70706100656d6100000100000000097265765f6e6f6973000000000000000000000000000000000001000000003fc500001c6c0000006000010001010101010101010101010101010101010004f3000900000000050500000004ee14ef00000000000000000000ffff0000fffffffffff3ffff0000123d0000000000380000000000000258000100c814317fff000000000190000000006972410514316c61000000c802bc7fff00000000410500006c61697200c814317fff000200000190000000006972410514316c61000200c802bc7fff00000000410500006c61697200c814317fff00000000019000000000697241051a1e6c6122170005
202c2322245f302323285c3b29232224223023232c1f1e295c221c00062c2322245f302323525b3b295c5d64652224222823232c231e295c301d000720232224223023232c5f30302e285c3b29232224223023232c5c30302e08251e2924222200232c2322302e30233b295f30006f00430074006e006e00650073007400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000001b00000520000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000006465525b22285c5d2c2322242e302323295c3030002a351e22285f32202a222423232c233b295f302422285f5c202a22232c2328295c302322285f3b202a22245f222d22285f3b291e295f402900292c202a285f23232c233b295f30202a285f2c23285c5c302323285f3b292d22202a3b295f225f40285f2c3d1e29285f3a002a222422232c2320302e30233b295f302422285f5c202a22232c2328302e30233b295c302422285f22202a223f3f222d5f3b295f295f4028002b341e2a285f31232c2320302e30233b295f30202a285f2c23285c2e302323295c30302a285f3b222d2220295f3f3f40285f3b05ed295f000000000003ec0014e00000000000000020fff5000020c00000000000000000000114e0fff5000020c0c420000000000000000014e0000000000001c420fff5000020c00000000000000000000214e0fff5000020c0c420000000000000000014e0000000000002c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e0fff5000020c0c420000000000000000014e0000000000000c420fff5000020c00000000000000000000014e00001000020c00020000000000000000014e0000000080005c820fff5000020c00000000000000000000514e0fff5000620c0c820000000000000000014e00000000c0005c820fff5000020c00000000000000000000514e0fff5000a20c0c820000000000000000014e00000000d0005c820fff5000020c00000000000000000000414e0fff0000020c04812000000000000000009850000530600007465656800090a310d001005640c000100001100f1a9fc1050624dd200015f3f2b00002a012500018c00ff00000100011404c181412602035007081520656761008350260000840000000026e80000000000273f0000000000283fe800000000293ff000000000003ff00000000122a1000100640001000100000006000000000000000000003fe00000000000013fe00000000000000000f7000000ff009218ffffffccc0c0c000ff00ff000000003f000000000010f200000000000000000000000000f6ffff000015001500ff00150f1d06400000000301000000000000003e0000000002360a00000000a0000000006400640a09269901ff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Ew_avail_sum.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
