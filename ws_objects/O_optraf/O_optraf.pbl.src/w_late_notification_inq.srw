﻿$PBExportHeader$w_late_notification_inq.srw
$PBExportComments$Inquire Response window for sales late notification
forward
global type w_late_notification_inq from w_base_response_ext
end type
type st_1 from statictext within w_late_notification_inq
end type
type st_2 from statictext within w_late_notification_inq
end type
type dw_sales_people from u_base_dw_ext within w_late_notification_inq
end type
type dw_sales_peoplehidden from u_base_dw_ext within w_late_notification_inq
end type
type em_1 from editmask within w_late_notification_inq
end type
type dw_salespersonbydiv from u_base_dw_ext within w_late_notification_inq
end type
end forward

global type w_late_notification_inq from w_base_response_ext
integer x = 23
integer y = 108
integer width = 1595
integer height = 1040
string title = "Late Load Notification Inquire"
long backcolor = 12632256
st_1 st_1
st_2 st_2
dw_sales_people dw_sales_people
dw_sales_peoplehidden dw_sales_peoplehidden
em_1 em_1
dw_salespersonbydiv dw_salespersonbydiv
end type
global w_late_notification_inq w_late_notification_inq

type variables
Boolean		ib_Canc

u_orp204		iu_orp204
u_ws_orp4		iu_ws_orp4
end variables

forward prototypes
public function character wf_get_late_permissions ()
end prototypes

public function character wf_get_late_permissions ();Int		li_rtn

string		lc_late_permissions

s_error	s_error_info

s_error_info.se_event_name = "wf_get_late_permissions"
s_error_info.se_window_name = "w_late_notification"
s_error_info.se_procedure_name = "orpo94br_late_load_permissions"
s_error_info.se_user_id = Message.nf_GetUserid()
s_error_info.se_message = Space(71)

li_rtn = iu_ws_orp4.nf_orpo94fr(s_error_info,lc_late_permissions)

Return lc_late_permissions

end function

event closequery;Long	ll_RowCount,&
		ll_Loop_Count
String ls_WinPath,ls_filename		
u_sdkcalls	lu_sdkCalls


lu_sdkcalls = Create u_sdkcalls
		
This.SetRedraw(False)
If Not ib_canc Then
	dw_sales_people.SetFilter( "IsSelected()")
	dw_sales_people.Filter()
	// added change for terminal server don't use working dir use windows dir 
	// Get the fullpath to the windows dir ibdkdld
	ls_WinPath = lu_sdkcalls.nf_getwindowsdirectory()
	ls_fileName = ls_WinPath+"\select.dbf"
	dw_sales_people.SaveAS( ls_fileName, Dbase3!, True)
//	dw_sales_people.SaveAS( iw_frame.is_workingDir+"select.dbf", Dbase3!, True)
	Message.StringParm = "I~t"+em_1.Text+"~t"
	ll_RowCount = dw_sales_people.RowCount()
	IF ll_RowCount < 1 Then
		MessageBox("Select a TSR", "You must select at least one TSR")
		dw_sales_people.SetFilter( "")
		dw_sales_people.Filter()
		dw_Sales_People.Sort()
		This.SetRedraw(True)
		Message.ReturnValue =1 
		Return
	ELSE
		Message.ReturnValue = 0 
	END IF
	FOR ll_Loop_Count = 1 to ll_RowCount
		Message.StringParm += dw_sales_people.GetItemString( ll_Loop_Count, "smancode") +"~t"
	Next
end if
This.SetRedraw(true)


end event

event ue_postopen;String	ls_fileName,&
			ls_FindString, ls_WinPath

Long		ll_NumRows,&
			ll_Loop_Count,&
			ll_RowFound
u_sdkcalls	lu_sdkCalls


lu_sdkcalls = Create u_sdkcalls

em_1.Text = Trim(Message.is_smanlocation)
em_1.DisplayOnly = True
em_1.Enabled = False

IF wf_get_late_permissions() = 'D' then
	dw_salespersonbydiv.SetTransObject( SQLCA)
	dw_salespersonbydiv.Retrieve(Message.is_Smandivision)
	dw_sales_people.Reset()
	dw_sales_people.ImportString(dw_salespersonbydiv.Object.DataWindow.Data) 
	ll_NumRows = dw_sales_people.Rowcount()
	For ll_Loop_Count = 1 to ll_NumRows
		dw_Sales_people.SelectRow( ll_Loop_Count,True)
	NEXT
ELSE
	
	dw_sales_people.SetTransObject( SQLCA)
	dw_Sales_People.Retrieve(Message.is_SmanLocation,Message.is_SmanLocation)
	// added change for terminal server don't use working dir use windows dir 
	// Get the fullpath to the windows dir ibdkdld
	ls_WinPath = lu_sdkcalls.nf_getwindowsdirectory()
	ls_fileName = ls_WinPath+"\select.dbf"
//	ls_fileName = iw_frame.is_workingDir+"select.dbf"
	dw_sales_peoplehidden.Reset()
	IF FileExists( ls_FileNAme) Then ll_NumRows = dw_sales_peoplehidden.ImportFile( ls_fileName)
	ls_FileName = dw_sales_peoplehidden.Describe("DataWindow.Data")
	For ll_Loop_Count = 1 to ll_NumRows
		ls_FindString = "smancode = '" + dw_sales_peoplehidden.GetItemString( ll_Loop_Count, "smancode")+"'"
		ll_RowFound = dw_sales_people.Find( ls_FindString, 1, dw_sales_people.RowCount())
		IF ll_RowFound > 0 Then
			dw_Sales_people.SelectRow( ll_RowFound,True)
		END IF
	NEXT
End If	
	


end event

on w_late_notification_inq.create
int iCurrent
call super::create
this.st_1=create st_1
this.st_2=create st_2
this.dw_sales_people=create dw_sales_people
this.dw_sales_peoplehidden=create dw_sales_peoplehidden
this.em_1=create em_1
this.dw_salespersonbydiv=create dw_salespersonbydiv
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.dw_sales_people
this.Control[iCurrent+4]=this.dw_sales_peoplehidden
this.Control[iCurrent+5]=this.em_1
this.Control[iCurrent+6]=this.dw_salespersonbydiv
end on

on w_late_notification_inq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.st_2)
destroy(this.dw_sales_people)
destroy(this.dw_sales_peoplehidden)
destroy(this.em_1)
destroy(this.dw_salespersonbydiv)
end on

event open;call super::open;IF Not IsValid(iu_orp204) Then
	iu_orp204 = Create u_orp204
END IF
iu_ws_orp4 = Create u_ws_orp4
end event

event close;Destroy ( iu_orp204)
Destroy (iu_ws_orp4)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_late_notification_inq
integer x = 846
integer y = 812
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_late_notification_inq
integer x = 558
integer y = 812
integer taborder = 50
end type

event cb_base_cancel::clicked;call super::clicked;ib_canc = TRUE
CloseWithReturn( Parent, "Abort")
end event

type cb_base_ok from w_base_response_ext`cb_base_ok within w_late_notification_inq
integer x = 274
integer y = 812
end type

event cb_base_ok::clicked;call super::clicked;Close(Parent)
end event

type cb_browse from w_base_response_ext`cb_browse within w_late_notification_inq
end type

type st_1 from statictext within w_late_notification_inq
integer x = 247
integer y = 96
integer width = 443
integer height = 72
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Sales Location"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_late_notification_inq
integer x = 718
integer y = 96
integer width = 247
integer height = 72
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "TSR"
boolean focusrectangle = false
end type

type dw_sales_people from u_base_dw_ext within w_late_notification_inq
integer x = 727
integer y = 176
integer width = 658
integer height = 572
integer taborder = 70
string dataobject = "d_sales_people_traf"
boolean vscrollbar = true
end type

on constructor;call u_base_dw_ext::constructor;This.is_selection = "4"
end on

type dw_sales_peoplehidden from u_base_dw_ext within w_late_notification_inq
boolean visible = false
integer x = 1417
integer y = 180
integer width = 1234
integer taborder = 30
string dataobject = "d_sales_people"
end type

type em_1 from editmask within w_late_notification_inq
integer x = 251
integer y = 184
integer width = 407
integer height = 100
integer taborder = 60
integer textsize = -10
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "###"
string displaydata = ""
end type

type dw_salespersonbydiv from u_base_dw_ext within w_late_notification_inq
boolean visible = false
integer x = 233
integer y = 1076
integer width = 1842
integer height = 732
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_salespersonbydiv_traf"
boolean border = false
end type

