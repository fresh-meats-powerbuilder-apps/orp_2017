﻿$PBExportHeader$w_late_notification.srw
$PBExportComments$Main window of sales late notification
forward
global type w_late_notification from w_base_sheet_ext
end type
type dw_late_load_notify from u_base_dw_ext within w_late_notification
end type
end forward

global type w_late_notification from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2939
integer height = 1548
string title = "Late Load Notification"
dw_late_load_notify dw_late_load_notify
end type
global w_late_notification w_late_notification

type variables
u_orp001		iu_orp001
u_ws_orp3		iu_ws_orp3

String		is_Open_Parm

Boolean		ib_first_time
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_delete ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();Open( w_late_notification_inq)
is_open_parm = Message.StringParm
IF is_open_parm = "Abort" Then
	if ib_first_time = true then
		ib_first_time = false
		Close (This)
	end if
	Return False
ELSE
	ib_first_time = false
	Return This.PostEvent( "Ue_Query", 0, is_open_parm)
END IF

end function

public subroutine wf_delete ();Long	ll_RowCount,&
		ll_LoopCount

String ls_InputString

int		li_rtn

S_Error	lstr_error_info
IF MessageBox("Confirm Delete", "Are you sure you want to delete selected records",Question!,YesNo!,2 )  =2 Then Return
dw_late_load_notify.SetRedraw( False)
dw_late_load_notify.SetFilter("Delete_button  = 'Y'")
dw_late_load_notify.Filter()

ll_RowCount = dw_late_load_notify.RowCount()
For ll_LoopCount = 1 to ll_RowCount
	ls_InputString += dw_late_load_notify.GetItemString( ll_LoopCount, "notify_type", Primary!, FALSE)+&
							"~t"+dw_late_load_notify.GetItemString( ll_LoopCount, "load_key", Primary!, FALSE)+&
							"~t"+dw_late_load_notify.GetItemString( ll_LoopCount, "sales_person_code", Primary!, FALSE)+"~r~n"
Next
dw_late_load_notify.RowsDiscard( 1, dw_late_load_notify.RowCount(), Primary!)
dw_late_load_notify.SetFilter("")
dw_late_load_notify.Filter()

lstr_error_info.se_event_name = "wf_Delete"
lstr_error_info.se_window_name = "w_late_notification"
lstr_error_info.se_procedure_name = "orpo53fr"
lstr_error_info.se_user_id = Message.nf_GetUserid()
lstr_error_info.se_message = Space(71)


//li_rtn = iu_orp001.nf_orpo53ar_delete_sales_notify( &
//			lstr_error_info, ls_InputString)
li_rtn = iu_ws_orp3.uf_orpo53fr( &
			lstr_error_info, ls_InputString)

This.TriggerEvent("ue_Query", 0 , is_open_parm)
iw_frame.SetMicrohelp('Delete Successfull')
dw_late_load_notify.SetRedraw( True)
end subroutine

public function boolean wf_update ();SetMicroHelp("No update available")
Return FALSE
end function

event ue_query;call super::ue_query;Int		li_PageNumber,&
			li_rtn
Double	ld_Task_Number

string	ls_PassedString,&
		 	ls_OutPut

s_error	s_error_info

s_error_info.se_event_name = "ue_query"
s_error_info.se_window_name = "w_late_notification"
s_error_info.se_procedure_name = "orpo55ar_late_note"
s_error_info.se_user_id = Message.nf_GetUserid()
s_error_info.se_message = Space(71)


ls_PassedString = String(Message.LongParm, "address")

dw_late_load_notify.SetRedraw( False)

li_PageNumber = 0
ld_Task_Number = 0

//Li_rtn =  iu_orp001.nf_orpo55ar_late_note( &
//			   s_error_info, &
//			   ls_PassedString, &
//				ld_Task_Number,&
//				li_PageNumber,&
//			   ls_output)
				
Li_rtn =  iu_ws_orp3.uf_orpo55fr( &
			   s_error_info, &
			   ls_PassedString, &
			   ls_output)				

IF li_rtn  >= 0 Then 
	dw_late_load_notify.Reset()
	dw_late_load_notify.ImportString( ls_Output)
END IF
/*
DO While ld_Task_Number > 0 And li_PageNumber > 0 
	Li_rtn =  iu_orp001.nf_orpo55ar_late_note( &
			   s_error_info, &
			   ls_PassedString, &
				ld_Task_Number,&
				li_PageNumber,&
			   ls_output)

	IF li_rtn  >= 0 Then 
			dw_late_load_notify.ImportString( ls_Output)
	END IF
Loop
*/
dw_late_load_notify.SetRedraw( True)
end event

on deactivate;call w_base_sheet_ext::deactivate;IW_FRAME.IM_MENU.MF_disABLE("m_delete")
end on

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_input,&
						ldwc_ToShare

u_string_functions	lu_string_functions

iw_Frame.SetMicroHelp( "Ready")

dw_late_load_notify.GetChild( "customer_id", ldwc_input)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_input)

dw_late_load_notify.GetChild( "customer_id_name", ldwc_ToShare)
//iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_ToShare)
ldwc_ToShare.SetTransObject(SQLCA)
ldwc_ToShare.Retrieve("000","999")

   
dw_late_load_notify.GetChild( "sales_person_code", ldwc_input)
iw_frame.iu_project_functions.nf_gettsrs ( ldwc_INput, Message.is_smanLocation)
dw_late_load_notify.GetChild( "sales_person_code_name", ldwc_ToShare)
ldwc_Input.ShareData( ldwc_ToShare)

IF lu_string_functions.nf_IsEmpty( is_Open_Parm) Then
	This.Post wf_retrieve()
ELSE
	This.PostEvent( "Ue_Query",0, is_Open_parm)
END IF

end event

event close;call super::close;Destroy ( iu_orp001)
Destroy ( iu_ws_orp3)

end event

on w_late_notification.create
int iCurrent
call super::create
this.dw_late_load_notify=create dw_late_load_notify
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_late_load_notify
end on

on w_late_notification.destroy
call super::destroy
destroy(this.dw_late_load_notify)
end on

event open;call super::open;is_Open_Parm = Message.StringParm
IF Not IsValid(iu_orp001) Then
	iu_orp001 = Create u_orp001
END IF
IF Not IsValid(iu_ws_orp3) Then
	iu_ws_orp3 = Create u_ws_orp3
END IF
ib_first_time = true

end event

event activate;call super::activate;IW_FRAME.IM_MENU.MF_ENABLE("m_delete")
IW_FRAME.IM_MENU.MF_DISABLE("m_Save")

end event

type dw_late_load_notify from u_base_dw_ext within w_late_notification
integer x = 9
integer width = 2875
integer height = 1404
string dataobject = "d_late_load_notify"
boolean vscrollbar = true
end type

event clicked;call super::clicked;String	ls_SmanCode,&
			ls_SmanLoc,&
			ls_UserID

u_string_functions	lu_string_functions

IF row > 0 Then
	ls_SmanCode = This.GetItemString( row,"sales_person_code")
	ls_SmanLoc  = Message.is_smanlocation
	Select salesman.UserID
	INTO :ls_UserID
	FROM salesman
	Where salesman.SmanCode = :ls_SmanCode AND salesman.SmanLoc = :ls_SmanLoc
	USING SQLCA;
	
//	If Not lu_string_functions.nf_isempty( ls_UserID ) Then
		IF This.GetItemString( row, "delete_button") = 'Y' Then
			This.SetItem( row, "delete_button", "N") 
			This.SelectRow( row, False)
		ELSE	
			This.SetItem( row, "delete_button", 'Y')
			This.SelectRow( row, True)
		END IF
//	END IF
End if
end event

event doubleclicked;call super::doubleclicked;Long	ll_clickedRow,&
		ll_ColumnNumber

String	ls_ColumnName

Window	lw_window

if row > 0 then
	ll_ClickedRow	= row
		ls_ColumnName  = dwo.name
		Choose Case ls_ColumnName
		Case "order_id"
			OpenSheetWithParm( lw_window, This.GetItemString( ll_ClickedRow, ls_ColumnName),&
							"w_sales_order_detail", iw_frame,8, iw_Frame.im_Menu.iao_ArrangeOpen)
		Case "load_key"
		IF ll_ClickedRow > 0  Then
			OpenSheetWithParm( lw_Window, This.GetItemString( ll_ClickedRow, "order_id"),&
								 "w_load_detail", iw_frame,8, iw_Frame.im_Menu.iao_ArrangeOpen)
		END IF
			
	END Choose
end if



end event

event rbuttondown;call super::rbuttondown;IF Upper(Left(SQLCA.USERID,4)) = 'IBDK' Then This.InsertRow(0)
end event

