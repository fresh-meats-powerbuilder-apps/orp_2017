﻿$PBExportHeader$w_report_list.srw
forward
global type w_report_list from w_netwise_sheet
end type
type ole_report from olecustomcontrol within w_report_list
end type
end forward

global type w_report_list from w_netwise_sheet
integer x = 27
integer y = 64
integer width = 2386
integer height = 1400
string title = "Report List"
long backcolor = 12632256
event ue_view ( )
event ue_morbius ( )
event ue_pps ( )
event ue_print ( )
event ue_delete ( )
event ue_reinquire ( )
ole_report ole_report
end type
global w_report_list w_report_list

type variables
u_utl001				iu_utl001
u_dwselect			iu_dwselect

boolean				ib_OLE_Error 
boolean				ib_list, &
						ib_portrait,&
						ib_first_time
						
string 				is_report_id, &
						is_userid, &
						is_password,&	
						is_option
						
m_report_options	im_popmenu
u_olecom				iu_oleReportList
oleobject			iu_oleReport
m_netwise_menu		im_menu
//w_netwise_frame	iw_frame
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_retrieve ()
public subroutine wf_print ()
end prototypes

event ue_view;//s_Error					ls_Error
//
//oleobject				lu_oleReport
//u_string_functions	lu_string_functions
//integer					li_rtn, li_count
//long						ll_row
//string 					ls_output, ls_reportid
//
//if not ib_list then return
//
//
//ll_row = dw_1.getselectedrow(0)
//if ll_row = 0 then
//	iw_frame.setmicrohelp("A report has to be selected to view.")
//	return
//end if
//iw_frame.setmicrohelp("Inquiring to Mainframe for report information.")
//SetPointer(HourGlass!)
//this.SetRedraw(False)
//ls_reportid = dw_1.getitemstring(ll_row, "report_id")
//
//lu_oleReport = Create u_olecom
//// This is the correct way of call fo rthe report handle
////lu_oleReport = iu_oleReportList.item(ll_row)
//
////This is the way it works(but not the correct way to do things)
////iu_oleReportList.SetReport(ll_row)
////lu_oleReport = iu_oleReportList.NeededReport
//// This is a better way to do it incorrectlly
//lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//
//If IsNull(lu_oleReport) Then
//	// Didn't get an object, but no error happened.  This is weird.  Better exit just in case.
//	iw_frame.setmicrohelp("No connection made to the Com Object. - ue_view of w_report_list - ") 
//	SetPointer(Arrow!)
//	this.SetRedraw(True)
//	Return
//End If
//
//ls_output = lu_oleReport.rawdata
////ls_output = "12345678901234567890123456789012345678901234567890123456789012345678901234567890" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"         1         2         3      Report       5         6         7         8" + "~r~n" + & 
////				"         1         2         3       View        5         6         7         8" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"~f" + "~r~n" + & 
////				"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*(){}[]::;;<>,.?/-=_+|\            X" + "~r~n"
//
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"p","~f",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"a","|",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"ÿ"," ",1)
//
//
//
//if len(ls_output) = 0 then
//	iw_frame.setmicrohelp("The Report is not available.")
//	SetPointer(Arrow!)
//	this.SetRedraw(True)
//	return
//end if
//
//if dw_1.getitemstring(ll_row,"report_width") = "W" then
//	ib_portrait = false
//else
//	ib_portrait = true
//end if
//
//iu_oleReport = lu_oleReport
//dw_1.DataObject = 'd_report_detail'
//dw_1.reset()
//
//if ib_portrait then
//	dw_1.object.detail.width = 2750
//else
//	dw_1.object.detail.width = 4306
//end if
//
//dw_1.importstring(ls_output)
//im_popmenu.m_popup.m_view.disable()
//
//ib_list = false
//
//SetPointer(Arrow!)
//this.SetRedraw(True)
//iw_frame.setmicrohelp("Ready")
end event

event ue_morbius;//s_Error		ls_Error
//oleobject		lu_oleReport
//integer		li_rtn, li_count
//long			ll_row
//string 		ls_output, ls_input
//
//ls_Error.se_User_ID = SQLCA.userid
//dw_1.SetRedraw(False)
//ll_row = dw_1.getselectedrow(0)
//if ll_row = 0 then
//	iw_frame.setmicrohelp("A report has to be selected to delete.")
//	return
//end if
//
//lu_oleReport = Create u_olecom
//
//if ib_list then
//	ll_row = dw_1.getselectedrow(0)
////	This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////	
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly 
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//
//lu_oleReport.Route("M")
//
//if ib_list then 
//	wf_retrieve()
//end if
//
//Messagebox("Process Flow","This is mobius!  " + ls_output)
//
//dw_1.SetRedraw(true)
end event

event ue_pps;//s_Error		ls_Error
//oleobject		lu_oleReport
//integer		li_rtn, li_count
//long			ll_row
//string 		ls_output, ls_input
//
//ls_Error.se_User_ID = SQLCA.userid
//
//lu_oleReport = Create u_olecom
//
//if ib_list then
//	ll_row = dw_1.getselectedrow(0)
//	if ll_row = 0 then
//		iw_frame.setmicrohelp("A report has to be selected to route to the PPS System.")
//		return
//	end if
//// This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//iw_frame.setmicrohelp("Inputting information to Mainframe for report.")
//SetPointer(HourGlass!)
//dw_1.SetRedraw(False)
//
//lu_oleReport.Route("P")
//
//ls_output = lu_oleReport.routeoutput
//choose case ls_output
//	case "1"
//		iw_frame.setmicrohelp("Route successful.")
//	case "2"
//		iw_frame.setmicrohelp("Route unsuccessful. The report has been reset..")
//		SetPointer(Arrow!)
//		dw_1.SetRedraw(true)
//		return
//	case "3"
//		iw_frame.setmicrohelp("Route unsuccessful.")
//		SetPointer(Arrow!)
//		dw_1.SetRedraw(true)
//		return
//	case else
//		iw_frame.setmicrohelp("Unknown return code.")
//		SetPointer(Arrow!)
//		dw_1.SetRedraw(true)
//		return
//end choose
//
//if ib_list then 
//	wf_retrieve()
//end if
//
//SetPointer(Arrow!)
//dw_1.SetRedraw(true)
//iw_frame.setmicrohelp("Ready")
end event

event ue_print;//oleobject				lu_oleReport
//datastore				lds_print_report, lds_report
//u_string_functions	lu_string_functions
//integer					li_rtn, li_count
//long						ll_row, ll_temp, ll_report_rowcount, ll_count, ll_remainder, ll_blank_count, ll_count2, ll_line_count
//string 					ls_output, ls_input, ls_string, ls_print_output
//boolean					lb_detail
//
//ll_row = dw_1.getselectedrow(0)
//if ll_row = 0  and ib_list then
//	iw_frame.setmicrohelp("A report has to be selected to print.")
//	return
//end if
//
//iw_frame.setmicrohelp("Inquiring to Mainframe for report information.")
//SetPointer(HourGlass!)
//dw_1.SetRedraw(False)
//
//if ib_list then
//	if dw_1.getitemstring(ll_row,"report_width") = "W" then
//		ib_portrait = false
//	else
//		ib_portrait = true
//	end if
//end if
//
//lu_oleReport = Create u_olecom
//
//if ib_list then 
////	This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////	
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly 
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//
//ls_output = lu_oleReport.RawData
//
////ls_output = "12345678901234567890123456789012345678901234567890123456789012345678901234567890" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"         1         2         3      Report       5         6         7         8" + "~r~n" + & 
////				"         1         2         3       View        5         6         7         8" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"~p" + "~r~n" + & 
////				"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*(){}[]::;;<>,.?/-=_+|\            x" + "~r~n"
////
////Integer i
////For i = 1 to lu_oleReportView.Count
////	lu_oleReport = lu_oleReportView.Item(i)
////	
////	// Do something with lu_oleReport, for example
////	//ls_description = lu_oleReport.Description	
////	
////	
////Next
//
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"p","~f",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"a","|",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"ÿ"," ",1)
//
//if len(ls_output) = 0 then
//	iw_frame.setmicrohelp("The Report is not available.")
//	SetPointer(Arrow!)
//	dw_1.SetRedraw(true)
//	return
//end if
//
//lds_report = create datastore
//lds_print_report = create datastore
//lds_report.dataobject = "d_report_detail"
//lds_print_report.dataobject = "d_report_print"
//lds_report.reset()
//lds_print_report.reset()
//
//if ib_portrait then
//	lds_print_report.Object.detail.Font.Height = 60
//else
//	lds_print_report.Object.detail.Font.Height = 40
//end if
//
//ll_report_rowcount = lds_report.importstring(ls_output)
//ll_count = 1
//ll_line_count = 1
//ls_print_output = ""
//lb_detail = false
//do until ll_count > ll_report_rowcount
//	choose case lds_report.getitemstring(ll_count, "detail_ind")
//		case "H"
//			if lb_detail then
//				ll_remainder = mod(ll_line_count, 68)
//				ll_blank_count = 69 - ll_remainder
//				if ll_blank_count > 0 then
//					for ll_count2 = 1 to ll_blank_count
//						ls_print_output += "                               " + "~r~n"
//						ll_line_count ++
//					next
//				end if
//				ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~r~n"
////				ll_line_count ++
////				ll_count ++
//			else
//				ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~r~n"
////				ll_line_count ++
////				ll_count ++
//			end if
//		case "D"
//			ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~r~n"
////			ll_line_count ++
////			ll_count ++
//			lb_detail = true
//	end choose
//	ll_line_count ++
//	ll_count ++
//loop
//
//lds_print_report.importstring(ls_print_output)
//
//if ib_portrait then
//	lds_report.Object.detail.Font.Height = 60
//else
//	lds_report.Object.detail.Font.Height = 40
//end if
// 
//lds_print_report.Print()
//SetPointer(Arrow!)
//dw_1.SetRedraw(true)
//iw_frame.setmicrohelp("Ready")
//
end event

event ue_delete;//s_Error		ls_Error
//oleobject		lu_oleReport
//integer		li_rtn, li_count
//long			ll_row
//string 		ls_output, ls_input
//
//ls_Error.se_User_ID = SQLCA.userid
//
//lu_oleReport = Create u_olecom
//
//if ib_list then
//	ll_row = dw_1.getselectedrow(0)	
//	if ll_row = 0 then
//		iw_frame.setmicrohelp("A report has to be selected to delete")
//		return
//	end if
////	This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////	
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly 
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//iw_frame.setmicrohelp("Deleting the report")
//SetPointer(HourGlass!)
//
//lu_oleReport.Delete
//
//ls_output = lu_oleReport.DeleteOutput
//choose case ls_output
//	case "1"
//		This.SetRedraw(True)
//		iw_frame.setmicrohelp("Delete successful")
//		wf_retrieve()
//		Return
//	case "2"
//		if ib_list then
//			dw_1.setitem(ll_row,"report_description", "** RETAINED **")
//		end if
//		iw_frame.setmicrohelp("Report is protected. Delete unsuccessful.")
//		SetPointer(Arrow!)
//		this.SetRedraw(True)
//		return
//	case "3"
//		iw_frame.setmicrohelp("Delete unsuccessful")
//		SetPointer(Arrow!)
//		this.SetRedraw(True)
//		return
//	case else
//		iw_frame.setmicrohelp("Unknown return code")
//		SetPointer(Arrow!)
//		this.SetRedraw(True)
//		return
//end choose
//
end event

event ue_reinquire;//
//dw_1.DataObject = 'd_report_view_print'
//dw_1.reset()
//
//im_popmenu.m_popup.m_view.enable()
//ib_list = true
end event

public subroutine wf_delete ();if ib_OLE_Error then return 

ole_report.object.DeleteReport()


end subroutine

public function boolean wf_retrieve ();s_Error		ls_Error
integer		li_rtn, li_count
string 		ls_output, ls_input
long			ll_rowcount, ll_x

if ib_OLE_Error then return TRUE

SetPointer(HourGlass!)
ole_Report.object.InquireList()

//
//
//
//
//
//
//if not ib_list then 
//	this.triggerevent ("ue_reinquire")
//end if
//
//dw_1.SetRedraw(False)
//
//dw_1.reset()
//
//if isvalid(iu_oleReport) then destroy iu_oleReport
//
//iw_frame.setmicrohelp("Wait...Inquiring Mainframe")
//
//ls_input = " "
//
//
//iu_oleReportList.GetList(is_userid, is_password)
//ls_output = iu_oleReportList.ReturnString
//
////Integer i
////For i = 1 to lu_oleReportList.Count
////	lu_oleReport = lu_oleReportList.Item(i)
////	
////	// Do something with lu_oleReport, for example
////	//ls_description = lu_oleReport.Description	
////	
////	
////Next
////messagebox("Data",ls_output)
//
//if iu_oleReportList.Count = 0 then
//	iw_frame.setmicrohelp("There are no reports available")
//	dw_1.setRedraw(True)
//	return true
//end if
//
////ls_output = "A" + "~t" + "This is a test" + "~t" + "08/03/1999" + "~t" + "15:25:25" + "~t" + "2" + "~t" + "N" + "~r~n" + &
////"B" + "~t" + "This " + "~t" + "08/03/1999" + "~t" + "15:25:25" + "~t" + "1" + "~t" + "W" + "~r~n" + &
////"12345678" + "~t" + "12345678901234567890" + "~t" + "08/03/1999" + "~t" + "15:25:25" + "~t" + "1" + "~t" + "W" + "~r~n" + &
////"abcdefgh" + "~t" + "Is       only " + "~t" + "08/03/1999" + "~t" + "15:25:25" + "~t" + "1" + "~t" + "W" + "~r~n" + &
////"ABCDEFGH" + "~t" + "A     test " + "~t" + "08/03/1999" + "~t" + "15:25:25" + "~t" + "1" + "~t" + "W" + "~r~n" + &
////"only" + "~t" + "This " + "~t" + "08/03/1999" + "~t" + "15:25:25" + "~t" + "999" + "~t" + "N" + "~r~n" 
//
//ll_rowcount = dw_1.importstring(ls_output)
//
//if ll_rowcount > 0 then
//	for ll_x = 1 to ll_rowcount
//		dw_1.setitem(ll_x,"index_key",ll_x)
//	next
//end if
//
//iw_frame.setmicrohelp(string("There are " + string(ll_rowcount) + " reports available"))
SetPointer(Arrow!)
//edw_1.SetRedraw(true)
return true

end function

public subroutine wf_print ();if ib_OLE_Error then return 

ole_report.object.DialogPrint()

end subroutine

on w_report_list.create
int iCurrent
call super::create
this.ole_report=create ole_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_report
end on

on w_report_list.destroy
call super::destroy
destroy(this.ole_report)
end on

event ue_postopen;call super::ue_postopen;String	ls_server_suffix

if ib_OLE_Error then return 

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

If IsNull(ls_server_suffix) Then  ls_server_suffix = " "
ole_report.object.SignonSystem = ls_server_suffix

IF IsNull(is_userid) Then is_userid = " "
ole_report.object.UserName = is_userid  

IF IsNull(is_password) Then is_password = " "
ole_report.object.Password = is_password 

wf_retrieve()

if this.width  > 10 then
	ole_report.Width = this.width - 50
end if

if this.height > 10 then
	ole_report.Height = this.height - 150
end if

//
//
//
//
//
//
//
//
//
//
//
//
//
//integer		li_rtn
//
//im_popmenu = Create m_report_options
//is_userid = SQLCA.UserID
//is_password = SQLCA.dbpass
//
//ib_list = true
//
//
//
//iu_oleReportList = Create u_olecom
//li_rtn = iu_oleReportList.ConnectToNewObject("ReportView.ReportList")
//
//If li_rtn < 0 Then
//	// Problem happened, handle this error and forget about retrieving the report list
//	messagebox("Error",string("Error has occurred. Return code from Com Object is " + &
//														string(li_rtn) + ". Please call help desk 3133")) 
//	Return 
//End If
//
//If IsNull(iu_oleReportList) Then
//	// Didn't get an object, but no error happened.  This is weird.  Better exit just in case.
//	iw_frame.setmicrohelp("No connection made to the Com Object. - wf_retrieve of w_report_list - ") 	
//	Return 
//End If
//
//ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 
//
//
//
//wf_retrieve()





end event

event activate;call super::activate;string	ls_option,ls_temp

ls_option = Message.StringParm

// ibdkdld 07/02 fix to work from last used windows option _
// and disable m_new
If ls_option <> "S" and ls_option <> "U" then ls_option = "U"

iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_generatesales')
iw_frame.im_menu.mf_Disable('m_complete')		
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_disable('m_new')

//ls_temp = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")

CHOOSE CASE ls_option
	//The User has requested the Scheduling System Reports	
	CASE "S"
		is_userid = "DCSCXXX"
		//password must have something in it
		is_password = "XXX"
		//User's are not allowed to delete system reports
		iw_frame.im_menu.mf_disable('m_delete')
	//The User has requested their own Reports	
	CASE "U"
		is_userid = SQLCA.userid
		//is_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
		is_password = SQLCA.dbpass
		iw_frame.im_menu.mf_enable('m_delete')
END CHOOSE

end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_generatesales')
iw_frame.im_menu.mf_enable('m_complete')		
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
// ibdkdld 07/02 fix to work from last used windows option _
// and disable m_new
iw_frame.im_menu.mf_enable('m_new')

//iw_frame.im_menu.mf_disable('m_print')

If is_userid = 'DCSCXXX' Then
	iw_frame.im_menu.mf_enable('m_delete')
End If
	
end event

event resize;call super::resize;if newwidth  > 100 then
	ole_report.Width = newwidth 
end if

if newheight > 200 then
	ole_report.Height = newheight 
end if

//integer li_x		= 14
//integer li_y		= 14

//.width	= width - (50 + li_x)
//dw_1.height	= height - (125 + li_y)

end event

event ue_fileprint;wf_print()
//this.triggerevent ("ue_print")
end event

event clicked;This.SetFocus()
This.BringtoTop = True

end event

event ue_printwithsetup;This.TriggerEvent("ue_FilePrint")
end event

type ole_report from olecustomcontrol within w_report_list
event messageposted ( string strinfomessage )
integer y = 4
integer width = 2327
integer height = 1272
integer taborder = 10
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
boolean focusrectangle = false
string binarykey = "w_report_list.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)
end event

event getfocus;if ib_OLE_Error then return

ole_report.object.SetFocusBack()
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

if ib_OLE_Error  then 
	action = ExceptionIgnore! 
	return
end if

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

close(parent)
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

if ib_OLE_Error  then 
	action = ExceptionIgnore! 
	return
end if

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
04w_report_list.bin 
2600000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000003e3827a001c22e8900000003000000c00000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000280000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a0000000200000001000000047bb8640d11d55115010004a0db902802000000003e3827a001c22e893e3827a001c22e89000000000000000000000000fffffffe00000002fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
27ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0066006700640069006500670067006f006700710068006400660066006500620065006f00630067002000290072002000740065007200750073006e006c00200000b29300000070000800034757f20b000000200065005f00740078006e0065007800740000349b000800034757f20a000000200065005f00740078006e006500790074000020de000c000b21b1db89ffffffc0006f0063006e00750076007400730069006200690065006c0000000000200072007000780073006f0020002c006e0069006500740065006700200072007000790073006f00290020002000200065007200750074006e007200200073006f006c0067006e005b002000620070005f006d0062007200740075006f00740064006e0077006f005d006e00720000006d00650074006f00650065006500780020006300200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064007800650063006500740075005d006500720000006d00650074006f006800650074006f0069006c006b006e00740073007200610020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f006500640064006100690076006500730000005d00650072006f006d00650074006f0068006c0074006e00690073006b004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000100000070000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
14w_report_list.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
