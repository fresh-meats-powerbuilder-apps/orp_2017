﻿$PBExportHeader$u_pas201.sru
$PBExportComments$IBDKDLD Contains pasp00br - pasp08br, p41b-p42b, p71br --
forward
global type u_pas201 from u_netwise_transaction
end type
end forward

global type u_pas201 from u_netwise_transaction
end type
global u_pas201 u_pas201

type prototypes
// PowerBuilder Script File: c:\ibp\pas201\pas201.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Wed Jul 17 08:28:31 2013
// Source Interface File: c:\ibp\pas201\pas201.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: orpo68br_schedres
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo68br_schedres( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    string detail_info_in, &
    ref string header_info_out, &
    ref string detail_info_out, &
    ref string inc_exc_out, &
    ref double task_number, &
    ref double page_number, &
    int CommHnd &
) library "pas201.dll" alias for "orpo68br_schedres;Ansi"


//
// Declaration for procedure: pasp00br_inq_fab
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp00br_inq_fab( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product_code[10], &
    char product_state, &
    ref char projections_exist, &
    ref char product_status, &
    ref string tpasfab_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp00br_inq_fab;Ansi"


//
// Declaration for procedure: pasp01br_upd_fab
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp01br_upd_fab( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string tpasfab_update_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp01br_upd_fab;Ansi"


//
// Declaration for procedure: pasp01dr_inq_sold_position
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp01dr_inq_sold_position( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp01dr_inq_sold_position;Ansi"


//
// Declaration for procedure: pasp02br_inq_tree
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp02br_inq_tree( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string product_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp02br_inq_tree;Ansi"


//
// Declaration for procedure: pasp03br_inq_fab_descr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp03br_inq_fab_descr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char fab_product[10], &
    char product_state, &
    ref string fab_description, &
    ref string valid_product_states, &
    int CommHnd &
) library "pas201.dll" alias for "pasp03br_inq_fab_descr;Ansi"


//
// Declaration for procedure: pasp04br_inq_step
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp04br_inq_step( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref char fab_product_descr[30], &
    ref char fab_group[2], &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp04br_inq_step;Ansi"


//
// Declaration for procedure: pasp05br_inq_step_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp05br_inq_step_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref int shift_duration, &
    ref char step_type, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp05br_inq_step_detail;Ansi"


//
// Declaration for procedure: pasp06br_upd_step
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp06br_upd_step( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string detail_string, &
    ref int new_step_sequence, &
    int CommHnd &
) library "pas201.dll" alias for "pasp06br_upd_step;Ansi"


//
// Declaration for procedure: pasp07br_inq_pltchr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp07br_inq_pltchr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref char fab_group[2], &
    ref string plant_string, &
    ref string char_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp07br_inq_pltchr;Ansi"


//
// Declaration for procedure: pasp08br_upd_pltchr
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp08br_upd_pltchr( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string plant_string, &
    string char_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp08br_upd_pltchr;Ansi"


//
// Declaration for procedure: pasp36cr_inq_rmt_sched_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp36cr_inq_rmt_sched_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp36cr_inq_rmt_sched_detail;Ansi"


//
// Declaration for procedure: pasp37cr_upd_rmt_sched_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp37cr_upd_rmt_sched_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp37cr_upd_rmt_sched_detail;Ansi"


//
// Declaration for procedure: pasp38cr_inq_rmt_sched
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp38cr_inq_rmt_sched( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp38cr_inq_rmt_sched;Ansi"


//
// Declaration for procedure: pasp39cr_upd_rmt_sched
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp39cr_upd_rmt_sched( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp39cr_upd_rmt_sched;Ansi"


//
// Declaration for procedure: pasp40cr_inq_rmt_consumption
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp40cr_inq_rmt_consumption( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp40cr_inq_rmt_consumption;Ansi"


//
// Declaration for procedure: pasp41br_inq_exceptions
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp41br_inq_exceptions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp41br_inq_exceptions;Ansi"


//
// Declaration for procedure: pasp41cr_upt_rmt_consumption
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp41cr_upt_rmt_consumption( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp41cr_upt_rmt_consumption;Ansi"


//
// Declaration for procedure: pasp42br_upd_exceptions
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp42br_upd_exceptions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string exception_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp42br_upd_exceptions;Ansi"


//
// Declaration for procedure: pasp42cr_inq_rmt_sched_output
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp42cr_inq_rmt_sched_output( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp42cr_inq_rmt_sched_output;Ansi"


//
// Declaration for procedure: pasp45cr_products_on_po_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp45cr_products_on_po_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp45cr_products_on_po_inq;Ansi"


//
// Declaration for procedure: pasp46cr_products_on_po_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp46cr_products_on_po_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp46cr_products_on_po_upd;Ansi"


//
// Declaration for procedure: pasp47cr_inq_sap_purch_ord
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp47cr_inq_sap_purch_ord( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp47cr_inq_sap_purch_ord;Ansi"


//
// Declaration for procedure: pasp48cr_acct_sap_purch_ord
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp48cr_acct_sap_purch_ord( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp48cr_acct_sap_purch_ord;Ansi"


//
// Declaration for procedure: pasp49cr_inq_dest_plants
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp49cr_inq_dest_plants( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp49cr_inq_dest_plants;Ansi"


//
// Declaration for procedure: pasp50cr_remove_demand_pts
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp50cr_remove_demand_pts( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp50cr_remove_demand_pts;Ansi"


//
// Declaration for procedure: pasp51cr_inq_area_sect_names
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp51cr_inq_area_sect_names( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp51cr_inq_area_sect_names;Ansi"


//
// Declaration for procedure: pasp52cr_upd_area_sect_names
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp52cr_upd_area_sect_names( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp52cr_upd_area_sect_names;Ansi"


//
// Declaration for procedure: pasp53cr_inq_sched_sections
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp53cr_inq_sched_sections( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp53cr_inq_sched_sections;Ansi"


//
// Declaration for procedure: pasp54cr_upd_sched_sections
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp54cr_upd_sched_sections( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp54cr_upd_sched_sections;Ansi"


//
// Declaration for procedure: pasp55cr_inq_all_area_sects
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp55cr_inq_all_area_sects( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp55cr_inq_all_area_sects;Ansi"


//
// Declaration for procedure: pasp56cr_inq_sched_sub_sects
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp56cr_inq_sched_sub_sects( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp56cr_inq_sched_sub_sects;Ansi"


//
// Declaration for procedure: pasp57cr_upd_sched_sub_sects
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp57cr_upd_sched_sub_sects( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp57cr_upd_sched_sub_sects;Ansi"


//
// Declaration for procedure: pasp58cr_inq_sched_carcass
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp58cr_inq_sched_carcass( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp58cr_inq_sched_carcass;Ansi"


//
// Declaration for procedure: pasp59cr_upd_sched_carcass
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp59cr_upd_sched_carcass( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp59cr_upd_sched_carcass;Ansi"


//
// Declaration for procedure: pasp60cr_inq_create_rel_sched
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp60cr_inq_create_rel_sched( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp60cr_inq_create_rel_sched;Ansi"


//
// Declaration for procedure: pasp61cr_upd_create_rel_sched
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp61cr_upd_create_rel_sched( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp61cr_upd_create_rel_sched;Ansi"


//
// Declaration for procedure: pasp62cr_inq_sched_comments
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp62cr_inq_sched_comments( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp62cr_inq_sched_comments;Ansi"


//
// Declaration for procedure: pasp63cr_upd_sched_comments
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp63cr_upd_sched_comments( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp63cr_upd_sched_comments;Ansi"


//
// Declaration for procedure: pasp64cr_inq_schedule
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp64cr_inq_schedule( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp64cr_inq_schedule;Ansi"


//
// Declaration for procedure: pasp65cr_upd_schedule
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp65cr_upd_schedule( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp65cr_upd_schedule;Ansi"


//
// Declaration for procedure: pasp66cr_inq_prod_not_sched
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp66cr_inq_prod_not_sched( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp66cr_inq_prod_not_sched;Ansi"


//
// Declaration for procedure: pasp67cr_one_box_transfer_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp67cr_one_box_transfer_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp67cr_one_box_transfer_inq;Ansi"


//
// Declaration for procedure: pasp68cr_one_box_transfer_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp68cr_one_box_transfer_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp68cr_one_box_transfer_upd;Ansi"


//
// Declaration for procedure: pasp69cr_mass_prod_struct_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp69cr_mass_prod_struct_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp69cr_mass_prod_struct_upd;Ansi"


//
// Declaration for procedure: pasp70cr_rpt_ctrl_file_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp70cr_rpt_ctrl_file_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp70cr_rpt_ctrl_file_inq;Ansi"


//
// Declaration for procedure: pasp71br_inq_weekly_sources
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp71br_inq_weekly_sources( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp71br_inq_weekly_sources;Ansi"


//
// Declaration for procedure: pasp71cr_rpt_ctrl_file_maint
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp71cr_rpt_ctrl_file_maint( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string update_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp71cr_rpt_ctrl_file_maint;Ansi"


//
// Declaration for procedure: pasp72br_inq_shift_duration
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp72br_inq_shift_duration( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp72br_inq_shift_duration;Ansi"


//
// Declaration for procedure: pasp72cr_init_mainframe_rpt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp72cr_init_mainframe_rpt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp72cr_init_mainframe_rpt;Ansi"


//
// Declaration for procedure: pasp73br_inq_weekly_avail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp73br_inq_weekly_avail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp73br_inq_weekly_avail;Ansi"


//
// Declaration for procedure: pasp73cr_inq_fax_email_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp73cr_inq_fax_email_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp73cr_inq_fax_email_info;Ansi"


//
// Declaration for procedure: pasp74br_inq_locdiv_param
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp74br_inq_locdiv_param( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp74br_inq_locdiv_param;Ansi"


//
// Declaration for procedure: pasp74cr_transfer_asap_order_gen
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp74cr_transfer_asap_order_gen( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_event_name, &
    ref string se_function_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp74cr_transfer_asap_order_gen;Ansi"


//
// Declaration for procedure: pasp75br_up_locdiv_param
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp75br_up_locdiv_param( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_string, &
    string se_input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp75br_up_locdiv_param;Ansi"


//
// Declaration for procedure: pasp75cr_gen_tfr_orders
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp75cr_gen_tfr_orders( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string header_string, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp75cr_gen_tfr_orders;Ansi"


//
// Declaration for procedure: pasp76br_inq_sold_position_rept
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp76br_inq_sold_position_rept( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp76br_inq_sold_position_rept;Ansi"


//
// Declaration for procedure: pasp76cr_chg_production_dates_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp76cr_chg_production_dates_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string header_string_in, &
    ref string header_string_out, &
    int CommHnd &
) library "pas201.dll" alias for "pasp76cr_chg_production_dates_inq;Ansi"


//
// Declaration for procedure: pasp77br_inq_age_avail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp77br_inq_age_avail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp77br_inq_age_avail;Ansi"


//
// Declaration for procedure: pasp77cr_chg_production_dates_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp77cr_chg_production_dates_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp77cr_chg_production_dates_upd;Ansi"


//
// Declaration for procedure: pasp78br_inq_loadlist
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp78br_inq_loadlist( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref string total_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp78br_inq_loadlist;Ansi"


//
// Declaration for procedure: pasp78cr_inq_tran_ord_ship_dtl
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp78cr_inq_tran_ord_ship_dtl( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string header_string_in, &
    ref string header_string_out, &
    int CommHnd &
) library "pas201.dll" alias for "pasp78cr_inq_tran_ord_ship_dtl;Ansi"


//
// Declaration for procedure: pasp79br_inq_loadcombo
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp79br_inq_loadcombo( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    ref string header_string_out, &
    ref string output_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp79br_inq_loadcombo;Ansi"


//
// Declaration for procedure: pasp79cr_upd_tran_ord_ship_dtl
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp79cr_upd_tran_ord_ship_dtl( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string update_string_in, &
    ref string update_string_out, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp79cr_upd_tran_ord_ship_dtl;Ansi"


//
// Declaration for procedure: pasp80br_inq_load_list_excpt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp80br_inq_load_list_excpt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp80br_inq_load_list_excpt;Ansi"


//
// Declaration for procedure: pasp80cr_inq_act_prod_not_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp80cr_inq_act_prod_not_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp80cr_inq_act_prod_not_upd;Ansi"


//
// Declaration for procedure: pasp81br_upd_load_list
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp81br_upd_load_list( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    string detail_string_in, &
    ref string detail_string_out, &
    string exception_string_in, &
    ref string exception_string_out, &
    int CommHnd &
) library "pas201.dll" alias for "pasp81br_upd_load_list;Ansi"


//
// Declaration for procedure: pasp81cr_inq_beef_carcass_inv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp81cr_inq_beef_carcass_inv( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp81cr_inq_beef_carcass_inv;Ansi"


//
// Declaration for procedure: pasp82br_gen_load_list
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp82br_gen_load_list( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp82br_gen_load_list;Ansi"


//
// Declaration for procedure: pasp82cr_upd_beef_carcass_inv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp82cr_upd_beef_carcass_inv( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string hot_box_string_in, &
    string temp_cooler_string_in, &
    string header_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp82cr_upd_beef_carcass_inv;Ansi"


//
// Declaration for procedure: pasp83cr_inq_def_times_rmt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp83cr_inq_def_times_rmt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp83cr_inq_def_times_rmt;Ansi"


//
// Declaration for procedure: pasp86br_inq_source_sequence
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp86br_inq_source_sequence( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp86br_inq_source_sequence;Ansi"


//
// Declaration for procedure: pasp87br_upd_source_sequence
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp87br_upd_source_sequence( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_string, &
    string input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp87br_upd_source_sequence;Ansi"


//
// Declaration for procedure: pasp88br_inq_work_hours
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp88br_inq_work_hours( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp88br_inq_work_hours;Ansi"


//
// Declaration for procedure: pasp89br_upd_work_hours
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp89br_upd_work_hours( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp89br_upd_work_hours;Ansi"


//
// Declaration for procedure: PASP90BR_INQ_VALID_PLANT_TRANSFER
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int PASP90BR_INQ_VALID_PLANT_TRANSFER( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "PASP90BR_INQ_VALID_PLANT_TRANSFER;Ansi"


//
// Declaration for procedure: pasp91br_upd_valid_plant_transfer
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp91br_upd_valid_plant_transfer( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp91br_upd_valid_plant_transfer;Ansi"


//
// Declaration for procedure: pasp92br_inq_product_plant_parm
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp92br_inq_product_plant_parm( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp92br_inq_product_plant_parm;Ansi"


//
// Declaration for procedure: pasp93br_upd_product_plant_parm
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp93br_upd_product_plant_parm( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp93br_upd_product_plant_parm;Ansi"


//
// Declaration for procedure: pasp96br_inq_upd_div_parameters
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp96br_inq_upd_div_parameters( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char option, &
    string detail_string_in, &
    ref string detail_string_out, &
    int CommHnd &
) library "pas201.dll" alias for "pasp96br_inq_upd_div_parameters;Ansi"


//
// Declaration for procedure: pasp97br_loads_not_trans
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp97br_loads_not_trans( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas201.dll" alias for "pasp97br_loads_not_trans;Ansi"


//
// Declaration for procedure: pasp98br_upd_auto_cpy_yld
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp98br_upd_auto_cpy_yld( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_header_string, &
    string se_input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp98br_upd_auto_cpy_yld;Ansi"


//
// Declaration for procedure: pasp99br_upd_commit_cpy_yld
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp99br_upd_commit_cpy_yld( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string se_input_string, &
    int CommHnd &
) library "pas201.dll" alias for "pasp99br_upd_commit_cpy_yld;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "pas201.dll"
function int WBpas201CkCompleted( int CommHnd ) &
    library "pas201.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
Int		ii_pas201_commhandle


end variables

forward prototypes
public function boolean nf_pasp01br (ref s_error astr_error_info, ref string as_tpasfab)
public function boolean nf_upd_exception (ref s_error astr_error_info, string as_header, string as_exception)
public function integer nf_pasp71br_inq_weekly_sources (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp72br_inq_shift_duration (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp74br_inq_locdiv_parameters (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp75br_up_locdiv_param (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer nf_pasp73br_inq_weekly_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp76br_inq_sold_position (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp77br_inq_age_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp80br_inq_load_list_except (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp79br_inq_load_combination (ref s_error astr_error_info, string as_header_stirng_in, ref string as_header_string_out, ref string as_output_string)
public function integer nf_pasp82br_send_load_list (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp87br_upd_source_sequence (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function boolean nf_pasp86br_inq_source_sequence (ref s_error astr_error_info, string as_input_string, ref string as_header_a, ref string as_shift_a, ref string as_header_b, ref string as_shift_b, ref string as_header_c, ref string as_shift_c)
public function integer nf_pasp81br_update_load_list_maint (ref s_error astr_error_info, string as_input_string, string as_detail_in, ref string as_detail_out, string as_exception_in, ref string as_exception_out)
public function boolean nf_pasp89br_upd_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function boolean nf_pasp93br_update_product_plant_para (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp90br_inq_valid_plant_transfer (s_error astr_error_info, ref string as_output_string)
public function boolean nf_pasp91br_upd_valid_plant_transfer (s_error astr_error_info, ref string as_input_string)
public function integer nf_pasp96br_inq_upd_div_parameters (ref s_error astr_error_info, character ac_option, string as_input_string, ref string as_output_string)
public function integer nf_pasp97br_loads_not_on_list (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring)
public function integer nf_pasp98br_upd_automate_cpy_yld (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer nf_pasp99br_upd_commit_copy_ylds (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp78br_inq_load_list_maint (ref s_error astr_error_info, string as_input_string, ref string as_output_maint, ref string as_output_except, ref string as_output_load_totals)
public function integer nf_orpo68ar_schedres (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref string as_inc_exc, ref double ad_tasknumber, ref double ad_pagenumber)
public function boolean wbckcompleted (ref s_error astr_error_info, character ac_fab_product_code[10], ref string as_fab_product_descr, ref string as_valid_product_states)
public function boolean nf_pasp03br (ref s_error astr_error_info, character ac_fab_product_code[10], character ac_product_state, ref string as_fab_product_descr, ref string as_valid_product_states)
public function boolean nf_pasp03br (ref s_error astr_error_info, character ac_fab_product_code[10], ref string as_fab_product_descr)
public function boolean nf_pasp02br (ref s_error astr_error_info, string as_header_string, ref string as_product_string, ref string as_tree)
public function boolean nf_pasp06br (ref s_error astr_error_info, string as_header_string, string as_detail_string, ref integer ai_new_step_sequence)
public function integer nf_pasp00br (ref s_error astr_error_info, character ac_fab_product_code[10], character ac_product_state, ref character ac_projections_exist, ref character ac_product_status, ref string as_tpasfab_string)
public function integer nf_pasp00br (ref s_error astr_error_info, character fab_product_code[10], ref character ac_projections_exist, ref character ac_product_status, ref string as_tpasfab_string)
public function integer nf_pasp01dr_inq_sold_postion_new (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_inq_exception (ref s_error astr_error_info, ref string as_input, ref string as_plant, ref string as_char, ref string as_exception)
public function boolean nf_pasp88br_inq_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_prod, ref string as_ship, ref string as_tran, ref string as_rec, ref string as_plan_tfr)
public function integer nf_pasp92br_product_plant_parameters (ref s_error astr_error_info, string as_input_string, ref string as_mto, ref string as_parameters)
public function boolean nf_pasp41cr_upt_rmt_consumption (s_error astr_error_info, string as_update_string, string as_header_string)
public function integer nf_pasp42cr_inq_rmt_sched_output (s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp37cr_upd_rmt_sched_detail (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function boolean nf_pasp39cr_upd_rmt_sched (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp36cr_inq_rmt_sched_detail (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp40cr_inq_rmt_consumption (s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp46cr_products_on_po_upd (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp47cr_inq_sap_purch_ord (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp45cr_products_on_po_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp48cr_acct_sap_purch_ord (s_error astr_error_info, string as_update_string, ref string as_output_string)
public function integer nf_pasp49cr_inq_dest_plants (ref s_error astr_error_info, ref string as_output_string)
public function boolean nf_pasp50cr_remove_demand_pts (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp51cr_inq_area_sect_names (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp52cr_upd_area_sect_names (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp53cr_inq_sched_sections (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp54cr_upd_sched_sections (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp56cr_inq_sched_sub_sects (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp59cr_upd_sched_carcass (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function boolean nf_pasp61cr_upd_create_rel_sched (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function boolean nf_pasp63cr_upd_sched_comments (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function boolean nf_pasp65cr_upd_schedule (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp60cr_inq_create_rel_sched (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp62cr_inq_sched_comments (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp64cr_inq_schedule (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp38cr_inq_rmt_sched (s_error astr_error_info, string as_input_string, ref string as_headerstring, ref string as_detailstring, ref string as_rmshstring)
public function integer nf_pasp55cr_inq_all_area_sects (ref s_error astr_error_info, string as_input_string, ref string as_area_string, ref string as_sect_string)
public function integer nf_pasp58cr_inq_sched_carcass (ref s_error astr_error_info, string as_input_string, ref string as_detail_string, ref string as_chain_string)
public function boolean nf_pasp57cr_upd_sched_sub_sects (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp66cr_inq_prod_not_sched (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp67cr_inq_one_box_trans (ref s_error astr_error_info, ref string as_output_string)
public function boolean nf_pasp68cr_upd_one_box_trans (ref s_error astr_error_info, ref string as_update_string)
public function boolean nf_pasp69cr_upd_mass_product_structure (ref s_error astr_error_info, ref string as_update_string)
public function integer nf_pasp70cr_rpt_ctrl_file_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp71cr_rpt_ctrl_file_maint (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function boolean nf_pasp72cr_init_mainframe_report (ref s_error astr_error_info, ref string as_input_string)
public function integer nf_pasp73cr_inq_fax_email_info (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp74cr_transfer_asap_order_gen (ref s_error astr_error_info, ref string as_input_string)
public function integer nf_pasp75cr_gen_tfr_orders (ref s_error astr_error_info, ref string as_output_string, string as_header_string, string as_input_string)
public function integer nf_pasp76cr_chg_production_dates_inq (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string, ref string as_include_exclude_string)
public function boolean nf_pasp05br (ref s_error astr_error_info, string as_header_string, ref integer ai_shift_duration, ref character ac_step_type, ref string as_output_string)
public function boolean nf_pasp07br (ref s_error astr_error_info, string as_header_string, ref character ac_fab_group[2], ref string as_plant_string, ref string as_char_string)
public function boolean pasp04br_inq_step (ref s_error astr_error_info, string as_fab_product_code, integer ai_mfg_step_sequence, ref integer ai_shift_duration, ref character ac_step_type, ref string as_output_string)
public function boolean nf_pasp04br (ref s_error astr_error_info, string as_header_string, ref character ac_fab_product_descr[30], ref character ac_fab_group[2], ref string as_mfg_step_string)
public function boolean nf_pasp08br (ref s_error astr_error_info, string as_header_string, string as_plant_string, string as_char_string)
public function integer nf_pasp78cr_inq_to_ship_detail (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string)
public function integer nf_pasp79cr_upd_to_ship_detail (ref s_error astr_error_info, string as_detail_string_in, ref string as_detail_string_out, string as_header_string)
public function integer nf_pasp80cr_inq_act_prod_not_upd (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp82cr_cooler_inventory_upd (ref s_error astr_error_info, string as_hot_box_info, string as_temp_cooler_info, string as_header_string)
public function integer nf_pasp83cr_inq_def_times_rmt (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
public function integer nf_pasp81cr_cooler_inventory_inq (ref s_error astr_error_info, string as_header_string_in, ref string as_output_hot_box, ref string as_output_temp_cooler, ref string as_output_plant_info)
public function integer nf_pasp77cr_chg_prod_dates_updt (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
end prototypes

public function boolean nf_pasp01br (ref s_error astr_error_info, ref string as_tpasfab);Int li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_rtn = pasp01br_upd_fab(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_tpasfab, &
//									ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_upd_exception (ref s_error astr_error_info, string as_header, string as_exception);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp42br_upd_exceptions"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp42br_upd_exceptions(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header, &
//									as_exception, &
//									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp71br_inq_weekly_sources (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp71br_inq_weekly_sources"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp71br_inq_weekly_sources(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//Long		ll_temp
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
//  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp72br_inq_shift_duration (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp72br_inq_shift_duration"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp72br_inq_shift_duration(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp74br_inq_locdiv_parameters (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp74br_inq_locdiv_param"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(5000)
ls_output = Fill(char(0),5000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp74br_inq_locdiv_param(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp75br_up_locdiv_param (ref s_error astr_error_info, string as_header_string, string as_input_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp75br_up_locdiv_param"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp75br_up_locdiv_param(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//									as_input_string, &
//									ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp73br_inq_weekly_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp73br_inq_weekly_avail"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp73br_inq_weekly_avail(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp76br_inq_sold_position (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp76br_inq_sold_position_rept"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(5000)
ls_output = Fill(char(0),5000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp76br_inq_sold_position_rept(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp77br_inq_age_avail (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp77br_inq_age_avail"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp77br_inq_age_avail(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp80br_inq_load_list_except (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp80br_load_list_except_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp80br_inq_load_list_excpt(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp79br_inq_load_combination (ref s_error astr_error_info, string as_header_stirng_in, ref string as_header_string_out, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_header_out
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''
as_header_string_out = ''

astr_error_info.se_procedure_name = "pasp78br_auto_load_list_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)
ls_header_out = Space(50)
ls_header_out = Fill(char(0),50)

lt_starttime = Now()

//li_rtn = pasp79br_inq_loadcombo(ls_app_name, &
//				ls_window_name,&
//				ls_function_name,&
//				ls_event_name, &
//				ls_procedure_name, &
//				ls_user_id, &
//				ls_return_code, &
//				ls_message, &
//				as_header_stirng_in, &
//				ls_header_out, &
//				ls_output, &
//				ii_pas201_commhandle)
//as_output_string += ls_output
//as_header_string_out += ls_header_out 
//lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp82br_send_load_list (ref s_error astr_error_info, string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message 
				
Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "pasp82br_send_load_list"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

lt_starttime = Now()

//li_rtn = pasp82br_gen_load_list(ls_app_name, &
//				ls_window_name,&
//				ls_function_name,&
//				ls_event_name, &
//				ls_procedure_name, &
//				ls_user_id, &
//				ls_return_code, &
//				ls_message, &
//				as_input_string, &
//				ii_pas201_commhandle)

lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp87br_upd_source_sequence (ref s_error astr_error_info, string as_header_string, string as_input_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp87br_upd_source_sequence"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp87br_upd_source_sequence(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//									as_input_string, &
//									ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp86br_inq_source_sequence (ref s_error astr_error_info, string as_input_string, ref string as_header_a, ref string as_shift_a, ref string as_header_b, ref string as_shift_b, ref string as_header_c, ref string as_shift_c);Int	li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "pasp86br_inq_source_sequence"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


	as_header_a		= ''
	as_shift_a     = ''
	as_header_b		= ''
	as_shift_b     = ''
	as_header_c		= ''
	as_shift_c     = ''
	ls_output_string = space(20000)
 	ls_output_string = fill(char(0),20000)

//Do
//		lt_starttime = Now()
//		li_ret = pasp86br_inq_source_sequence(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output_string, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//		lt_endtime = Now()
//	
//	
//		//	messagebox('output',string(as_output_string))
//
//		ls_string_ind = Mid(ls_output_string, 2, 1)
//		ls_output_string = Mid(ls_output_string, 3)
//		Do While Len(Trim(ls_output_string)) > 0
//			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//			Choose Case ls_string_ind
//				Case '1'
//					as_header_a += ls_output
//				Case '2'
//					as_header_b += ls_output
//				Case '3'
//					as_header_c += ls_output
//				Case 'A'
//					as_shift_a += ls_output
//				Case 'B'
//					as_shift_b += ls_output
//				Case 'C'
//					as_shift_c += ls_output
//			End Choose
//			ls_string_ind = Left(ls_output_string, 1)
//			ls_output_string = Mid(ls_output_string, 2)
//		Loop 
//
//		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp81br_update_load_list_maint (ref s_error astr_error_info, string as_input_string, string as_detail_in, ref string as_detail_out, string as_exception_in, ref string as_exception_out);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp81_update_load_list_maint"
astr_error_info.se_message = Space(70)

as_detail_out = Space(20000)
as_detail_out = Fill(char(0),20000)
as_exception_out = Space(20000)
as_exception_out = Fill(char(0),20000)


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp81br_upd_load_list(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								as_detail_in, &
//								as_detail_out, &
//								as_exception_in, &
//								as_exception_out, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

return li_ret


end function

public function boolean nf_pasp89br_upd_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number
				
Int		li_ret

Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "pasp89br_upd_work_hours"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0								

lt_starttime = Now()

//li_ret = pasp89br_upd_work_hours(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//								as_input_string, &
//								ld_task_number, &
//								ld_last_record_number, &
//								ld_max_record_number, &								
//									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp93br_update_product_plant_para (ref s_error astr_error_info, string as_input_string);Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number


Int						li_ret

Time						lt_starttime, &
							lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp93br_update_product_plant_para"
astr_error_info.se_message = Space(70)
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp93br_upd_product_plant_parm(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								"",&
								as_input_string, &
								ld_task_number,&
								ld_last_record_number,& 
								ld_max_record_number,&
								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp90br_inq_valid_plant_transfer (s_error astr_error_info, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output_string
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "pasp90br_inq_valid_plant_transfer"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message)
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output_string = Space(20000)
ls_output_string = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp90br_inq_valid_plant_transfer(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output_string, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output_string
//	lt_endtime = Now()
//	
//
//
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp91br_upd_valid_plant_transfer (s_error astr_error_info, ref string as_input_string);Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number


Int						li_ret

Time						lt_starttime, &
							lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp91br_upd_valid_plant_transfer"
astr_error_info.se_message = Space(71)
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp91br_upd_valid_plant_transfer(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								ld_task_number,&
//								ld_last_record_number,& 
//								ld_max_record_number,&
//								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp96br_inq_upd_div_parameters (ref s_error astr_error_info, character ac_option, string as_input_string, ref string as_output_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_header_out
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp96br_inq_upd_div_parameters"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

ls_output = Space(5000)
ls_output = Fill(char(0),5000)

lt_starttime = Now()

li_rtn = pasp96br_inq_upd_div_parameters(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				ac_option, &
				as_input_string, &
				ls_output, &
				ii_pas201_commhandle)
as_output_string += ls_output
lt_endtime = Now()
	
	
//	messagebox('output',string(as_output_string))
	
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp97br_loads_not_on_list (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_outputstring = ''

astr_error_info.se_procedure_name = "pasp97br_loads_not_on_list"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do
	lt_starttime = Now()

	li_rtn = pasp97br_loads_not_trans( &
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_inputstring, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_pas201_commhandle)

	as_outputstring += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp98br_upd_automate_cpy_yld (ref s_error astr_error_info, string as_header_string, string as_input_string);Int			li_rtn

Long			ll_pos

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_inputstring
				
Time							lt_starttime, &
								lt_endtime
u_String_functions		lu_string
								

astr_error_info.se_procedure_name = "nf_pasp98br_upd_automate_cpy_yld"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

Do 
	ll_pos = lu_string.nf_npos(as_input_string, '~n', 1, 500)
	If ll_pos = 0 Then ll_pos = Len(as_input_string)
	ls_inputstring = Left(as_input_string, ll_pos)
	as_input_string = Mid(as_input_string, ll_pos + 1)
	
	lt_starttime = Now()
	
	li_rtn =  pasp98br_upd_auto_cpy_yld(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_string, &
					ls_inputstring, &
					ii_pas201_commhandle)
						  
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while Len(as_input_string) > 0 and li_rtn >= 0

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp99br_upd_commit_copy_ylds (ref s_error astr_error_info, string as_input_string);Int			li_rtn

Long			ll_pos

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_inputstring
				
Time							lt_starttime, &
								lt_endtime
u_String_functions		lu_string
								

astr_error_info.se_procedure_name = "nf_pasp99br_upd_commit_copy_ylds"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

//Do 
//	ll_pos = lu_string.nf_npos(as_input_string, '~n', 1, 500)
//	If ll_pos = 0 Then ll_pos = Len(as_input_string)
//	ls_inputstring = Left(as_input_string, ll_pos)
//	as_input_string = Mid(as_input_string, ll_pos + 1)
//	
//	lt_starttime = Now()
//	
//	li_rtn =  pasp99br_upd_commit_cpy_yld(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_inputstring, &
//					ii_pas201_commhandle)
//						  
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//Loop while Len(as_input_string) > 0 and li_rtn >= 0

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp78br_inq_load_list_maint (ref s_error astr_error_info, string as_input_string, ref string as_output_maint, ref string as_output_except, ref string as_output_load_totals);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output_string, &
				ls_output, &
				ls_string_ind
				
Time			lt_starttime, &
				lt_endtime
				
u_string_functions	lu_string				
								
as_output_maint = ''
as_output_except = ''

astr_error_info.se_procedure_name = "pasp78br_auto_load_list_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	ls_output_string = Space(20000)
//	ls_output_string = Fill(char(0),20000)
//
//	as_output_load_totals = Space(100)
//	as_output_load_totals = fill(char(0), 100)
//	lt_starttime = Now()
//	
//	li_rtn = pasp78br_inq_loadlist(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output_string, &
//					as_output_load_totals, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//
//	ls_string_ind = Mid(ls_output_string, 2, 1)
//	ls_output_string = Mid(ls_output_string, 3)
//	Do While Len(Trim(ls_output_string)) > 0
//		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//		Choose Case ls_string_ind
//			Case 'M'
//				as_output_maint += ls_output
//			Case 'E'
//				as_output_except += ls_output
//		End Choose
//		ls_string_ind = Left(ls_output_string, 1)
//		ls_output_string = Mid(ls_output_string, 2)
//	Loop 
//
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_orpo68ar_schedres (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref string as_inc_exc, ref double ad_tasknumber, ref double ad_pagenumber);Boolean  lb_OK
Int		li_ret
String	ls_detail_out, &
			ls_header_out, &
 			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,ls_inc_exc_string

ls_inc_exc_string 	= space(20000)
ls_inc_exc_string 	= fill(char(0),20000)

ls_detail_out = Space(14000)
ls_header_out = Space(100)

astr_error_info.se_procedure_name = "orpo68ar_schedres"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

li_ret = orpo68br_schedres(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_in, &
								as_detail_in, &
								ls_Header_out, &
								ls_Detail_out, &
								ls_inc_exc_string, &
								ad_TaskNumber, &
								ad_PageNumber, &
								ii_pas201_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
lb_OK = nf_display_message(li_ret, astr_error_info, &
			ii_pas201_commhandle) 
as_header_in = ls_header_out
as_detail_in = ls_detail_out
as_inc_exc =  ls_inc_exc_string
return li_ret

end function

public function boolean wbckcompleted (ref s_error astr_error_info, character ac_fab_product_code[10], ref string as_fab_product_descr, ref string as_valid_product_states);//** IBDKEEM ** 08/12/2002 ** Added Product State

Int li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_fab_product_descr = Space(42)
as_valid_product_states = Space(50)

astr_error_info.se_user_id = SQLCA.Userid

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_rtn = pasp03br_inq_fab_descr(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								ac_fab_product_code, &
//								as_fab_product_descr, &
//								as_valid_product_states, &
//								ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp03br (ref s_error astr_error_info, character ac_fab_product_code[10], character ac_product_state, ref string as_fab_product_descr, ref string as_valid_product_states);//** IBDKEEM ** 08/12/2002 ** Added Product State

Int li_rtn

Time							lt_starttime, &
								lt_endtime
								
String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

astr_error_info.se_user_id = SQLCA.Userid

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_fab_product_descr = Space(42)
as_valid_product_states = Space(50)

lt_starttime = Now()

//li_rtn = pasp03br_inq_fab_descr(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								ac_fab_product_code, &
//								ac_product_state, &
//								as_fab_product_descr, &
//								as_valid_product_states, &
//								ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp03br (ref s_error astr_error_info, character ac_fab_product_code[10], ref string as_fab_product_descr);character lc_product_state
string ls_valid_product_states

lc_product_state = " "

//return nf_pasp03br(astr_error_info,ac_fab_product_code,lc_product_state ,as_fab_product_descr,ls_valid_product_states)
return true
end function

public function boolean nf_pasp02br (ref s_error astr_error_info, string as_header_string, ref string as_product_string, ref string as_tree);//** IBDKEEM ** 08/14/2002 ** changed refetch ** Added Header_string
Int				li_ret
DOUBLE			ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number
	
Time				lt_starttime, &
					lt_endtime
					
boolean			lb_first_time 
			
String			ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id,&
					ls_return_code,&
					ls_message,&
					ls_product_string,&
					ls_output_string
		
nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

as_product_string = ''
as_tree = ''

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

ls_product_string = Space(200)
ls_output_string = Space(20000)
lb_first_time = true

do
	lt_starttime = Now()
	
	li_ret = pasp02br_inq_tree(ls_app_name, &
									ls_window_name, &
									ls_function_name, &
									ls_event_name, &
									ls_procedure_name, &
									ls_user_id,&
									ls_return_code,&
									ls_message,&
									as_header_string, &
									ls_product_string, &
									ls_output_string, &
									ld_task_number, &
									ld_last_record_number, &
									ld_max_record_number, &
									ii_pas201_commhandle)
	
	lt_endtime = Now()
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	
	if li_ret >= 0 then
		if lb_first_time then
			as_product_string = ls_product_string
			lb_first_time = false
		end if
		
		as_tree += ls_output_string
	end if
	
Loop While ld_last_record_number < ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp06br (ref s_error astr_error_info, string as_header_string, string as_detail_string, ref integer ai_new_step_sequence);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_rtn = pasp06br_upd_step(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//									as_detail_string, &
//									ai_new_step_sequence, &
//									ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

Return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)
end function

public function integer nf_pasp00br (ref s_error astr_error_info, character ac_fab_product_code[10], character ac_product_state, ref character ac_projections_exist, ref character ac_product_status, ref string as_tpasfab_string);Time							lt_starttime, &
								lt_endtime
								
String			ls_app_name, &
					ls_window_name, &
					ls_function_name, &
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id,&
					ls_return_code,&
					ls_message

Int	li_rtn

as_tpasfab_string = Space(86)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_rtn = pasp00br_inq_fab(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								ac_fab_product_code, &
								ac_product_state,&
								ac_projections_exist, &
								ac_product_status, &
								as_tpasfab_string, &
								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

Return li_rtn
end function

public function integer nf_pasp00br (ref s_error astr_error_info, character fab_product_code[10], ref character ac_projections_exist, ref character ac_product_status, ref string as_tpasfab_string);return nf_pasp00br(astr_error_info, fab_product_code, ' ', ac_projections_exist, ac_product_status, as_tpasfab_string)
end function

public function integer nf_pasp01dr_inq_sold_postion_new (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp01dr_inq_sold_position_new"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(5000)
ls_output = Fill(char(0),5000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp01dr_inq_sold_position(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_inq_exception (ref s_error astr_error_info, ref string as_input, ref string as_plant, ref string as_char, ref string as_exception);Double					ld_last_record_number, &
							ld_max_record_number, &
							ld_task_number

Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output_string, &
							ls_output, &
							ls_string_ind

u_string_functions	lu_string
							

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

astr_error_info.se_procedure_name = "pasp41br_inq_exceptions"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

//Do
//	ls_output_string = Space(30000)
//	lt_starttime = Now()
//
//	li_ret = pasp41br_inq_exceptions(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input, &
//								ls_output_string, &
//								ld_task_number, &
//								ld_last_record_number, &
//								ld_max_record_number, &
//								ii_pas201_commhandle)
//
//	lt_endtime = Now()
//
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//
//	ls_string_ind = Mid(ls_output_string, 2, 1)
//	ls_output_string = Mid(ls_output_string, 3)
//	Do While Len(Trim(ls_output_string)) > 0
//		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//		Choose Case ls_string_ind
//			Case 'P'
//				as_plant += ls_output
//			Case 'C'
//				as_char += ls_output
//			Case 'E'
//				as_exception += ls_output
//		End Choose
//		ls_string_ind = Left(ls_output_string, 1)
//		ls_output_string = Mid(ls_output_string, 2)
//	Loop 
//Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp88br_inq_work_hours (ref s_error astr_error_info, ref string as_header_string, ref string as_prod, ref string as_ship, ref string as_tran, ref string as_rec, ref string as_plan_tfr);Int						li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "pasp88br_inq_work_hours"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


	as_prod				= ''
	as_ship				= ''
	as_tran				= ''
	ls_output_string 	= space(20000)
 	ls_output_string 	= fill(char(0),20000)

//Do
//		lt_starttime = Now()
//		li_ret = pasp88br_inq_work_hours(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_header_string, &
//					ls_output_string, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//		lt_endtime = Now()
//	
//	
////		messagebox('output',string(ls_output_string))
//
//		ls_string_ind = Mid(ls_output_string, 2, 1)
//		ls_output_string = Mid(ls_output_string, 3)
//		Do While Len(Trim(ls_output_string)) > 0
//			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//			Choose Case ls_string_ind
//				Case 'P'
//					as_prod += ls_output
//				Case 'S'
//					as_ship += ls_output
//				Case 'T'
//					as_tran += ls_output
//				Case 'R'
//					as_rec += ls_output
//				Case 'X'
//					as_plan_tfr += ls_output
//			End Choose
//			ls_string_ind = Left(ls_output_string, 1)
//			ls_output_string = Mid(ls_output_string, 2)
//		Loop 
//
//		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)



end function

public function integer nf_pasp92br_product_plant_parameters (ref s_error astr_error_info, string as_input_string, ref string as_mto, ref string as_parameters);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_len

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, & 
				ls_output_string, &
				ls_string_ind
u_string_functions	lu_string
				
Time							lt_starttime, &
								lt_endtime
								

//as_output_string = ''
as_mto = ''
as_parameters = ''

astr_error_info.se_procedure_name = "pasp92br_product_plant_parameters"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	ls_output_string = Space(20000)
//	ls_output_string = Fill(char(0),20000)
//
//	lt_starttime = Now()
//
//	li_rtn = pasp92br_inq_product_plant_parm( &
//					ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output_string, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//					
////function int pasp92br_inq_product_plant_parm( &
////    ref string se_app_name, &
////    ref string se_window_name, &
////    ref string se_function_name, &
////    ref string se_event_name, &
////    ref string se_procedure_name, &
////    ref string se_user_id, &
////    ref string se_return_code, &
////    ref string se_message, &
////    string input_string, &
////    ref string output_string, &
////    ref double task_number, &
////    ref double last_record_number, &
////    ref double max_record_number, &
////    int CommHnd &
////) library "pas201.dll"
//
//	//as_output_string += ls_output
//	ll_len = Len(ls_output_string)
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//	
//	ls_string_ind = Mid(ls_output_string, 2, 1)
//	ls_output_string = Mid(ls_output_string, 3)
//	Do While Len(Trim(ls_output_string)) > 0
//		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//		Choose Case ls_string_ind
//			Case 'D'
//				as_parameters += ls_output
//			Case 'M'
//				as_mto += ls_output
//			Case 'P'
//				as_mto += ls_output
//				as_parameters += ls_output
//		End Choose
//		ls_string_ind = Left(ls_output_string, 1)
//		ls_output_string = Mid(ls_output_string, 2)
//	Loop 
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function boolean nf_pasp41cr_upt_rmt_consumption (s_error astr_error_info, string as_update_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp41cr_upt_rmt_consumption"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp41cr_upt_rmt_consumption(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp42cr_inq_rmt_sched_output (s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp42cr_inq_rmt_sched_output"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp42cr_inq_rmt_sched_output(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function boolean nf_pasp37cr_upd_rmt_sched_detail (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp37cr_upd_rmt_sched_detail"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp37cr_upd_rmt_sched_detail(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp39cr_upd_rmt_sched (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp39cr_upd_rmt_sched"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp39cr_upd_rmt_sched(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp36cr_inq_rmt_sched_detail (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp36cr_inq_rmt_sched_detail"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

Do
	lt_starttime = Now()
	
	ls_output = Space(20000)
	ls_output = Fill(char(0),20000)

	li_rtn = pasp36cr_inq_rmt_sched_detail(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					as_input_string, &
					ii_pas201_commhandle)


	ll_temp = Len(ls_output)
	as_output_string += ls_output
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp40cr_inq_rmt_consumption (s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp40cr_inq_rmt_consumption"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp40cr_inq_rmt_consumption(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function boolean nf_pasp46cr_products_on_po_upd (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp46cr_products_on_po_upd"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp46cr_products_on_po_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp47cr_inq_sap_purch_ord (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp47cr_inq_sap_purch_ord"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp47cr_inq_sap_purch_ord(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp45cr_products_on_po_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp45cr_products_on_po_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp45cr_products_on_po_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function boolean nf_pasp48cr_acct_sap_purch_ord (s_error astr_error_info, string as_update_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_ret

Time			lt_starttime, &
				lt_endtime
								

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp48cr_acct_sap_purch_ord"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp48cr_acct_sap_purch_ord(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_output_string, &
//								ld_task_number, &
//								ld_last_record_number, &
//								ld_max_record_number, &
//								as_update_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function integer nf_pasp49cr_inq_dest_plants (ref s_error astr_error_info, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_input_string
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''
ls_input_string = ''

astr_error_info.se_procedure_name = "pasp49cr_inq_dest_plants"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp49cr_inq_dest_plants(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ls_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function boolean nf_pasp50cr_remove_demand_pts (s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp50cr_remove_demand_pts"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp50cr_remove_demand_pts(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function integer nf_pasp51cr_inq_area_sect_names (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp51cr_inq_area_sect_names"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp51cr_inq_area_sect_names(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function boolean nf_pasp52cr_upd_area_sect_names (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp52cr_upd_area_sect_names"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp52cr_upd_area_sect_names(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function integer nf_pasp53cr_inq_sched_sections (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp53cr_inq_sched_sections"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp53cr_inq_sched_sections(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function boolean nf_pasp54cr_upd_sched_sections (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp54cr_upd_sched_sections"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp54cr_upd_sched_sections(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function integer nf_pasp56cr_inq_sched_sub_sects (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp56cr_inq_sched_sub_sects"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp56cr_inq_sched_sub_sects(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function boolean nf_pasp59cr_upd_sched_carcass (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp59cr_upd_sched_carcass"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp59cr_upd_sched_carcass(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function boolean nf_pasp61cr_upd_create_rel_sched (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp61cr_upd_create_rel_sched"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp61cr_upd_create_rel_sched(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function boolean nf_pasp63cr_upd_sched_comments (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp63cr_upd_sched_comments"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp63cr_upd_sched_comments(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function boolean nf_pasp65cr_upd_schedule (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp65cr_upd_schedule"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()
//
//li_ret = pasp65cr_upd_schedule(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function integer nf_pasp60cr_inq_create_rel_sched (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp60cr_inq_create_rel_sched"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp60cr_inq_create_rel_sched(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function integer nf_pasp62cr_inq_sched_comments (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp62cr_inq_sched_comments"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp62cr_inq_sched_comments(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function integer nf_pasp64cr_inq_schedule (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp64cr_inq_schedule"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp64cr_inq_schedule(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function integer nf_pasp38cr_inq_rmt_sched (s_error astr_error_info, string as_input_string, ref string as_headerstring, ref string as_detailstring, ref string as_rmshstring);Boolean		lb_FirstTime

Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_StringInd, &
				ls_OutputString
				
Time			lt_starttime, &
				lt_endtime

u_string_functions	lu_string	

as_headerstring = ''
as_detailstring = ''
as_rmshstring = ''
								
astr_error_info.se_procedure_name = "pasp38cr_inq_rmt_sched"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lb_FirstTime = True
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_outputString = Space(20000)
ls_outputString = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//	
//	ls_outputString = Space(20000)
//	ls_outputString = Fill(char(0),20000)
//
//	li_rtn = pasp38cr_inq_rmt_sched(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_OutputString, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//										
//	THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)
//	
//	If lb_FirstTime Then
//		as_HeaderString = ''
//		as_DetailString = ''
//		as_RmshString = ''
//		lb_FirstTime = False
//	End If
//	
//	ls_StringInd = Mid(ls_OutputString, 2, 1)
//	ls_OutputString = Mid(ls_OutputString, 3)
//
//	//ll_temp = Len(ls_output)
//	Do While Len(Trim(ls_OutputString)) > 0
//		ls_output = lu_string.nf_GetToken(ls_OutputString, '~f')
//		
//		Choose Case ls_StringInd
//			Case 'H'
//				as_HeaderString += ls_output
//			Case 'D'
//				as_DetailString += ls_output
//			Case 'R'
//				as_RmshString += ls_output
//		End Choose
//
//		ls_StringInd = Left(ls_OutputString, 1)
//		ls_OutputString = Mid(ls_OutputString, 2)
//	Loop
//
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
//  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp55cr_inq_all_area_sects (ref s_error astr_error_info, string as_input_string, ref string as_area_string, ref string as_sect_string);Boolean		lb_FirstTime 

Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_OutputString, &
				ls_StringInd
				
Time			lt_starttime, &
				lt_endtime
								
u_string_functions	lu_string	


as_area_string = ''
as_sect_string = ''

astr_error_info.se_procedure_name = "pasp55cr_inq_all_area_sects"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lb_FirstTime = True
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_outputString = Space(20000)
//	ls_outputString = Fill(char(0),20000)
//
//	li_rtn = pasp55cr_inq_all_area_sects(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_outputString, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	If lb_FirstTime Then
//		as_area_String = ''
//		as_sect_String = ''
//		lb_FirstTime = False
//	End If
//	
//	ls_StringInd = Mid(ls_OutputString, 2, 1)
//	ls_OutputString = Mid(ls_OutputString, 3)
//
//	Do While Len(Trim(ls_OutputString)) > 0
//		ls_output = lu_string.nf_GetToken(ls_OutputString, '~f')
//		
//		Choose Case ls_StringInd
//			Case 'A'
//				as_area_String += ls_output
//			Case 'S'
//				as_Sect_String += ls_output
//		End Choose
//
//		ls_StringInd = Left(ls_OutputString, 1)
//		ls_OutputString = Mid(ls_OutputString, 2)
//	Loop
//	
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function integer nf_pasp58cr_inq_sched_carcass (ref s_error astr_error_info, string as_input_string, ref string as_detail_string, ref string as_chain_string);Boolean		lb_FirstTime 

Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_OutputString, &
				ls_StringInd
				
Time			lt_starttime, &
				lt_endtime
								
u_string_functions	lu_string	


as_detail_string = ''
as_chain_string = ''

astr_error_info.se_procedure_name = "pasp58cr_inq_sched_carcass"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
lb_FirstTime = True
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_outputstring = Space(20000)
//	ls_outputstring = Fill(char(0),20000)
//
//	li_rtn = pasp58cr_inq_sched_carcass(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_outputstring, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	If lb_FirstTime Then
//		as_detail_String = ''
//		as_chain_String = ''
//		lb_FirstTime = False
//	End If
//	
//	ls_StringInd = Mid(ls_OutputString, 2, 1)
//	ls_OutputString = Mid(ls_OutputString, 3)
//
//	Do While Len(Trim(ls_OutputString)) > 0
//		ls_output = lu_string.nf_GetToken(ls_OutputString, '~f')
//		
//		Choose Case ls_StringInd
//			Case 'D'
//				as_detail_String += ls_output
//			Case 'C'
//				as_chain_String += ls_output
//		End Choose
//
//		ls_StringInd = Left(ls_OutputString, 1)
//		ls_Outputstring = Mid(ls_OutputString, 2)
//	Loop
//	
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function boolean nf_pasp57cr_upd_sched_sub_sects (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp57cr_upd_sched_sub_sects"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp57cr_upd_sched_sub_sects(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()


nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)


end function

public function integer nf_pasp66cr_inq_prod_not_sched (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp66cr_inq_prod_not_sched"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp66cr_inq_prod_not_sched(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function boolean nf_pasp67cr_inq_one_box_trans (ref s_error astr_error_info, ref string as_output_string);Double		ld_task_number, ld_last_record_number, ld_max_record_number
Int			li_ret

String		ls_output_string, ls_output

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message 


Time			lt_starttime, lt_endtime

u_string_functions	lu_string
								
as_output_string = ''
astr_error_info.se_procedure_name = "nf_pasp67cr_inq_one_box_trans"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

SetPointer(HourGlass!)

//Call a Netwise external function to get the required information.
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	
//	ls_output_string = Space(20000)
//	ls_output_string = Fill(char(0),20000)
//	
//	lt_starttime = Now()
//	
//	li_ret = pasp67cr_one_box_transfer_inq(ls_app_name, &
//											ls_window_name,&
//											ls_function_name,&
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id, &
//											ls_return_code, &
//											ls_message, &
//											ls_output_string, &
//											ld_task_number, &
//											ld_last_record_number, &
//											ld_max_record_number, &
//											ii_pas201_commhandle)
//											
//	as_output_string += ls_output_string
//	lt_endtime = Now()
//
////	messagebox('output',string(as_output_string))
//
//	nf_write_benchmark(lt_starttime, lt_endtime, 'nf_pasp67cr_inq_one_box_trans', '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas203 li rtn',string(li_rtn))

return true

end function

public function boolean nf_pasp68cr_upd_one_box_trans (ref s_error astr_error_info, ref string as_update_string);Int	li_ret

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

Double		ld_task_number, ld_last_record_number, ld_max_record_number

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//as_update_string = Space(20000)
//as_update_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp68cr_upd_one_box_trans"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

//li_ret = pasp68cr_one_box_transfer_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								ld_task_number, &
//								ld_last_record_number, &
//								ld_max_record_number, &
//								ii_pas201_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp69cr_upd_mass_product_structure (ref s_error astr_error_info, ref string as_update_string);Int	li_ret

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

Double		ld_task_number, ld_last_record_number, ld_max_record_number

//as_update_string = Space(20000)
//as_update_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp69cr_mass_prod_struct_upd"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

//li_ret = pasp69cr_mass_prod_struct_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_update_string, &
//								ii_pas201_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp70cr_rpt_ctrl_file_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp70cr_rpt_ctrl_file_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//	lt_starttime = Now()
//
//	li_rtn = pasp70cr_rpt_ctrl_file_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_pas201_commhandle)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, 'nf_pasp70cr_rpt_ctrl_file_inq', '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean nf_pasp71cr_rpt_ctrl_file_maint (ref s_error astr_error_info, string as_header_string, string as_input_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "pasp71cr_rpt_ctrl_file_maint"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp71cr_rpt_ctrl_file_maint(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//								as_input_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

//messagebox ('function',string(ls_function_name))

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean nf_pasp72cr_init_mainframe_report (ref s_error astr_error_info, ref string as_input_string);Int	li_ret

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

Double		ld_task_number, ld_last_record_number, ld_max_record_number

//as_update_string = Space(20000)
//as_update_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp72cr_init_mainframe_rpt"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

//li_ret = pasp72cr_init_mainframe_rpt(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								ii_pas201_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp73cr_inq_fax_email_info (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int	li_ret

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

Double		ld_task_number, ld_last_record_number, ld_max_record_number

as_output_string = Space(1000)
as_output_string = Fill(char(0),1000)

astr_error_info.se_procedure_name = "pasp73cr_inq_fax_email_info"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

//li_ret = pasp73cr_inq_fax_email_info(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								as_output_string, &
//								ii_pas201_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
					
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

return li_ret
end function

public function boolean nf_pasp74cr_transfer_asap_order_gen (ref s_error astr_error_info, ref string as_input_string);Int	li_ret

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output

Double		ld_task_number, ld_last_record_number, ld_max_record_number

//as_update_string = Space(20000)
//as_update_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "pasp74cr_transfer_asap_order_gen"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

li_ret = pasp74cr_transfer_asap_order_gen(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input_string, &
								ii_pas201_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp75cr_gen_tfr_orders (ref s_error astr_error_info, ref string as_output_string, string as_header_string, string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn


Long 		ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_build_string
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp75cr_gen_tfr_orders"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(5000)
ls_output = Fill(char(0),5000)

//Do
//	lt_starttime = Now()
////
//	li_rtn = pasp75cr_gen_tfr_orders(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_header_string, &
//					as_input_string, &
//					ii_pas201_commhandle)
//					
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
////	as_output_string += ls_output
////	lt_endtime = Now()
//	
//	
////	messagebox('output',string(as_output_string))
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

//messagebox('Pas201 li rtn',string(li_rtn))

Return li_rtn

end function

public function integer nf_pasp76cr_chg_production_dates_inq (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string, ref string as_include_exclude_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output_string, &
				ls_string_ind, &
				ls_output, &
				ls_header_string_in, &
				ls_header_string_out
				
Time			lt_starttime, &
				lt_endtime
				
u_string_functions	lu_string				
								

as_detail_string = ''
as_include_exclude_string = ''

astr_error_info.se_procedure_name = "pasp76cr_chg_production_dates_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_header_string_in = as_header_string


//Do
//	lt_starttime = Now()
//	
//	ls_output_string = Space(20000)
//	ls_output_string = Fill(char(0),20000)
//	ls_header_string_out = Space(10000)
//	ls_header_string_out = Fill(char(0),1000)
//	
//
//	li_rtn = pasp76cr_chg_production_dates_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output_string, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ls_header_string_in, &
//					ls_header_string_out, &
//					ii_pas201_commhandle)
//					
//	ls_string_ind = Mid(ls_output_string, 2, 1)
//	ls_output_string = Mid(ls_output_string, 3)
//	as_header_string = ls_header_string_out
//	
//	Do While Len(Trim(ls_output_string)) > 0
//		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//		Choose Case ls_string_ind
//			Case 'D'
//				as_detail_string += ls_output
//			Case 'I'
//				as_include_exclude_string += ls_output
//		End Choose
//		ls_string_ind = Left(ls_output_string, 1)
//		ls_output_string = Mid(ls_output_string, 2)
//	Loop 
//
//	lt_endtime = Now()
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function boolean nf_pasp05br (ref s_error astr_error_info, string as_header_string, ref integer ai_shift_duration, ref character ac_step_type, ref string as_output_string);Char		lc_product_code[10],&
			lc_product_state,&
			lc_step_type
		
Int		li_ret,&
			li_shift_duration
			
boolean	lb_first_time

Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number
				
Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_output_string

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
ls_output_string = Fill(char(0),30000)

lb_first_time = True

ld_task_number				= 0
ld_last_record_number 	= 0
ld_max_record_number 	= 0


//Do
//	lt_starttime = Now()
//	li_ret = pasp05br_inq_step_detail(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message,&
//									as_header_string, &
//									li_shift_duration, &
//									lc_step_type, &
//									ls_output_string, &
//									ld_task_number, &
//									ld_last_record_number, &
//									ld_max_record_number, &
//									ii_pas201_commhandle)
//	
//	lt_endtime = Now()
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//		
//	if li_ret >= 0 then
//		as_output_string += ls_output_string
//		
//		if lb_first_time then
//			ai_shift_duration = li_shift_duration
//			ac_step_type		= lc_step_type
//			
//			lb_first_time = false
//		end if
//	end if
//Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp07br (ref s_error astr_error_info, string as_header_string, ref character ac_fab_group[2], ref string as_plant_string, ref string as_char_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_plant_string = Space(1001)
as_char_string = Space(4001)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp07br_inq_pltchr(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//								ac_fab_group, &
//								as_plant_string, &
//								as_char_string, &
//								ii_pas201_commhandle) 

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

Return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)

end function

public function boolean pasp04br_inq_step (ref s_error astr_error_info, string as_fab_product_code, integer ai_mfg_step_sequence, ref integer ai_shift_duration, ref character ac_step_type, ref string as_output_string);Char	lc_product_code[10]
Int	li_ret

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


as_output_string = Space(6301)
lc_product_code = Space(10)
lc_product_code = as_fab_product_code

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_ret = pasp05br_inq_step_detail(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								lc_product_code, &
//											ai_mfg_step_sequence, &
//											ai_shift_duration, &
//											ac_step_type, &
//											as_output_string, &
//											ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp04br (ref s_error astr_error_info, string as_header_string, ref character ac_fab_product_descr[30], ref character ac_fab_group[2], ref string as_mfg_step_string);//** IBDKEEM ** 08/12/2002 ** Added Product State ** Changed to new refetch

Int			li_ret

boolean		lb_first_time

Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
				
Time			lt_starttime, &
				lt_endtime
								
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message,&
				ls_output_string

character 	lc_fab_product_descr[30]
character 	lc_fab_group[2]

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lc_fab_product_descr = Fill(char(0),30)
lc_fab_group = Fill(char(0),2)
ls_output_string = Fill(char(0),30000)

lb_first_time = True

ld_task_number				= 0
ld_last_record_number 	= 0
ld_max_record_number 	= 0
				
//do
//	lt_starttime = Now()
//	
//	li_ret = pasp04br_inq_step(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message,&
//									as_header_string, &
//									lc_fab_product_descr, &
//									lc_fab_group, &
//									ls_output_string, &
//									ld_task_number, &
//									ld_last_record_number, &
//									ld_max_record_number, &
//									ii_pas201_commhandle)
//	 
//	lt_endtime = Now()
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//	
//	if li_ret >= 0 then
//		as_mfg_step_string += ls_output_string
//		
//		if lb_first_time then
//			ac_fab_product_descr = lc_fab_product_descr
//			ac_fab_group = lc_fab_group
//			
//			lb_first_time = false
//		end if
//	end if
//Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0
//

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas201_commhandle)
end function

public function boolean nf_pasp08br (ref s_error astr_error_info, string as_header_string, string as_plant_string, string as_char_string);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_rtn = pasp08br_upd_pltchr(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//								as_plant_string, &
//								as_char_string, &
//								ii_pas201_commhandle)
										
lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

Return nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)

end function

public function integer nf_pasp78cr_inq_to_ship_detail (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp78cr_inq_to_ship_detail"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

as_header_string_out = Space(1000)
as_header_string_out = Fill(char(0),1000)

//Do
//	lt_starttime = Now()
//	
//	ls_output = Space(20000)
//	ls_output = Fill(char(0),20000)
//
//	li_rtn = pasp78cr_inq_tran_ord_ship_dtl(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_header_string_in, &
//					as_header_string_out, &
//					ii_pas201_commhandle)
//
//
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	//nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function integer nf_pasp79cr_upd_to_ship_detail (ref s_error astr_error_info, string as_detail_string_in, ref string as_detail_string_out, string as_header_string);Int		li_ret

Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "pasp79cr_upd_to_ship_detail"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
					

lt_starttime = Now()

//li_ret = pasp79cr_upd_tran_ord_ship_dtl(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_detail_string_in, &
//								as_detail_string_out, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return li_ret
end function

public function integer nf_pasp80cr_inq_act_prod_not_upd (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int	li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time							lt_starttime, &
								lt_endtime
								

as_output_string = ''

astr_error_info.se_procedure_name = "pasp80cr_inq_act_prod_not_upd"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

//Do
//	lt_starttime = Now()
//
//	li_rtn = pasp80cr_inq_act_prod_not_upd(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_input_string, &
//					ii_pas201_commhandle)
//Long		ll_temp
//	ll_temp = Len(ls_output)
//	as_output_string += ls_output
//	lt_endtime = Now()
//	
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn

end function

public function integer nf_pasp82cr_cooler_inventory_upd (ref s_error astr_error_info, string as_hot_box_info, string as_temp_cooler_info, string as_header_string);Int		li_ret

Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "pasp79cr_upd_to_ship_detail"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
					

lt_starttime = Now()

//li_ret = pasp82cr_upd_beef_carcass_inv(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_hot_box_info, &
//								as_temp_cooler_info, &
//								as_header_string, &
//								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return li_ret
end function

public function integer nf_pasp83cr_inq_def_times_rmt (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);Int		li_ret

Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "pasp83cr_inq_def_times_rmt"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
					

lt_starttime = Now()

as_detail_string = space(1000)

//li_ret = pasp83cr_inq_def_times_rmt(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//								as_detail_string, &								
//								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return li_ret
end function

public function integer nf_pasp81cr_cooler_inventory_inq (ref s_error astr_error_info, string as_header_string_in, ref string as_output_hot_box, ref string as_output_temp_cooler, ref string as_output_plant_info);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

Long			ll_temp

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output_string, &
				ls_output, &
				ls_string_ind
				
Time			lt_starttime, &
				lt_endtime
				
u_string_functions	lu_string				
								

as_output_hot_box = ''
as_output_temp_cooler = ''
as_output_plant_info = ''

astr_error_info.se_procedure_name = "pasp81cr_inq_beef_carcass_inv"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

//Do
//	lt_starttime = Now()
//	
//	ls_output_string = Space(20000)
//	ls_output_string = Fill(char(0),20000)
//
//	li_rtn = pasp81cr_inq_beef_carcass_inv(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output_string, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					as_header_string_in, &
//					ii_pas201_commhandle)
//
//
//	ls_string_ind = Mid(ls_output_string, 2, 1)
//	ls_output_string = Mid(ls_output_string, 3)
//	Do While Len(Trim(ls_output_string)) > 0
//		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
//		
//		Choose Case ls_string_ind
//			Case 'H'
//				as_output_hot_box += ls_output
//			Case 'T'
//				as_output_temp_cooler += ls_output
//			Case 'P'
//				as_output_plant_info += ls_output				
//		End Choose
//		ls_string_ind = Left(ls_output_string, 1)
//		ls_output_string = Mid(ls_output_string, 2)
//	Loop 
//
//	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//	
//	//nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
//
//Loop while ld_last_record_number <> ld_max_record_number
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_pas201_commhandle)


Return li_rtn


end function

public function integer nf_pasp77cr_chg_prod_dates_updt (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);Int		li_ret

Time		lt_starttime, &
			lt_endtime
								

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


astr_error_info.se_procedure_name = "pasp77cr_chg_prod_date_updt"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
					

lt_starttime = Now()

//li_ret = pasp77cr_chg_production_dates_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_header_string, &
//								as_detail_string, &								
//								ii_pas201_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return li_ret
end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows
ii_pas201_commhandle = sqlca.nf_getcommhandle("pas201")
If ii_pas201_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end event

on u_pas201.create
call super::create
end on

on u_pas201.destroy
call super::destroy
end on

