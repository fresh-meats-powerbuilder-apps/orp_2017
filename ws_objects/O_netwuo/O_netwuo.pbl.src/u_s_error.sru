﻿$PBExportHeader$u_s_error.sru
forward
global type u_s_error from nonvisualobject
end type
end forward

global type u_s_error from nonvisualobject
end type
global u_s_error u_s_error

type variables
s_error	istr_s_error
end variables

event constructor;istr_s_error.se_app_name = Space(9)
istr_s_error.se_window_name = Space(9)
istr_s_error.se_function_name = space(9)
istr_s_error.se_event_name = Space(32)
istr_s_error.se_procedure_name = Space(33)
istr_s_error.se_user_id = Space(9)
istr_s_error.se_return_code = space(7)
istr_s_error.se_message = space(72)

end event

on u_s_error.create
TriggerEvent( this, "constructor" )
end on

on u_s_error.destroy
TriggerEvent( this, "destructor" )
end on

