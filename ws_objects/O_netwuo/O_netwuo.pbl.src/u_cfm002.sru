﻿$PBExportHeader$u_cfm002.sru
forward
global type u_cfm002 from u_netwise_transaction
end type
end forward

global type u_cfm002 from u_netwise_transaction
end type
global u_cfm002 u_cfm002

type prototypes
// PowerBuilder Script File: c:\ibp\src\cfm002.pbf
// Target Environment:  Netwise Application/Integrator
// Script File Creation Time: Wed May 29 09:43:41 1996
// Source Interface File: j:\pb\test\src\cfm002.ntf
//
// Script File Created By:
//
//     Netwise Application/Integrator WORKBENCH v.2.1
//
//     Netwise, Inc.
//     2477 55th Street
//     Boulder, Colorado 80301
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: cfmc14ar_get_customer
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int cfmc14ar_get_customer( &
    ref s_error s_error_info, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "cfm002.dll" alias for "cfmc14ar_get_customer;Ansi"


//
// Declaration for procedure: cfmc23ar_get_default_strings
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int cfmc23ar_get_default_strings( &
    ref s_error error_info, &
    char customer_id[7], &
    char customer_type, &
    char division_code[2], &
    ref string default_info, &
    int CommHnd &
) library "cfm002.dll" alias for "cfmc23ar_get_default_strings;Ansi"


end prototypes

type variables
Int ii_commhandle
end variables

forward prototypes
public function integer nf_cfmc23ar (ref s_error astr_error_info, ref character ac_customer_id[7], ref character ac_customer_type, ref character ac_division_code[2], ref string as_defaults)
public function integer nf_cfmc14ar (ref s_error astr_error_info, ref string as_input, ref string as_defaults)
end prototypes

public function integer nf_cfmc23ar (ref s_error astr_error_info, ref character ac_customer_id[7], ref character ac_customer_type, ref character ac_division_code[2], ref string as_defaults);INTEGER 	li_rtn

gw_netwise_Frame.SetMicroHelp("Wait.. Retrieving Customer Data.")

astr_Error_Info.se_procedure_name = 'cfmc23ar_get_default_strings'
astr_Error_Info.se_Message = Space(70)

as_defaults = Space(80)

//li_Rtn = cfmc23ar_get_default_strings(astr_Error_info, ac_customer_id, &
//				ac_Customer_Type, ac_division_code, as_defaults, ii_commhandle)

This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

return li_rtn
end function

public function integer nf_cfmc14ar (ref s_error astr_error_info, ref string as_input, ref string as_defaults);INTEGER 	li_rtn

gw_netwise_Frame.SetMicroHelp("Wait.. Retrieving Customer Data.")

astr_error_info.se_procedure_name = "cfmc14ar_get_customer"
as_defaults = Space(156)
as_defaults = Fill(Char(0),156)

//li_Rtn = cfmc14ar_get_customer(astr_Error_info, &
//										as_input, &
//										as_defaults, &
//										ii_commhandle)

This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

return li_rtn
end function

event constructor;call super::constructor;STRING	ls_MicroHelp

ls_MicroHelp = gw_netwise_Frame.wf_GetMicroHelp()

gw_netwise_frame.SetMicroHelp( "Opening MainFrame Connections")

ii_commhandle = SQLCA.nf_GetCommHandle("cfm002")

If ii_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_netwise_Frame.SetMicroHelp(ls_MicroHelp)
end event

on u_cfm002.create
call super::create
end on

on u_cfm002.destroy
call super::destroy
end on

