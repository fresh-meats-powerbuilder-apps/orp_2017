﻿$PBExportHeader$w_updt_order_prod_short_resp.srw
forward
global type w_updt_order_prod_short_resp from w_base_response_ext
end type
type st_1 from statictext within w_updt_order_prod_short_resp
end type
type st_2 from statictext within w_updt_order_prod_short_resp
end type
type dw_product_shortages from u_base_dw_ext within w_updt_order_prod_short_resp
end type
end forward

global type w_updt_order_prod_short_resp from w_base_response_ext
int X=1134
int Y=916
int Width=1637
int Height=1156
boolean TitleBar=false
long BackColor=12632256
st_1 st_1
st_2 st_2
dw_product_shortages dw_product_shortages
end type
global w_updt_order_prod_short_resp w_updt_order_prod_short_resp

type variables
String	is_shortage_string
end variables

on w_updt_order_prod_short_resp.create
int iCurrent
call super::create
this.st_1=create st_1
this.st_2=create st_2
this.dw_product_shortages=create dw_product_shortages
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.dw_product_shortages
end on

on w_updt_order_prod_short_resp.destroy
call super::destroy
destroy(this.st_1)
destroy(this.st_2)
destroy(this.dw_product_shortages)
end on

event ue_base_ok;call super::ue_base_ok;CloseWithReturn(This,"OK")
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"Cancel")
end event

event open;call super::open;dw_product_shortages.ImportString(Message.StringParm)
dw_product_shortages.SetRedraw(True)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_updt_order_prod_short_resp
int X=1326
int Y=344
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_updt_order_prod_short_resp
int X=1326
int Y=188
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_updt_order_prod_short_resp
int X=1326
int Y=32
end type

type st_1 from statictext within w_updt_order_prod_short_resp
int X=165
int Y=20
int Width=814
int Height=76
boolean Enabled=false
boolean BringToTop=true
string Text="Order update will result in shortages.  "
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_updt_order_prod_short_resp
int X=247
int Y=136
int Width=576
int Height=76
boolean Enabled=false
boolean BringToTop=true
string Text="Do you wish to continue?"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_product_shortages from u_base_dw_ext within w_updt_order_prod_short_resp
int X=18
int Y=260
int Width=1138
int TabOrder=11
boolean BringToTop=true
string DataObject="d_updt_order_product_shortages"
boolean VScrollBar=true
end type

