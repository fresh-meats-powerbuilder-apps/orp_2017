﻿$PBExportHeader$w_sales_order_detail.srw
$PBExportComments$This is for Sales Order Detail.
forward
global type w_sales_order_detail from w_base_sheet_ext
end type
type st_1 from statictext within w_sales_order_detail
end type
type dw_valid_product_info from u_base_dw_ext within w_sales_order_detail
end type
type dw_customer_info from uo_d_customer_info within w_sales_order_detail
end type
type tab_1 from tab within w_sales_order_detail
end type
type tp_detail from userobject within tab_1
end type
type dw_detail from u_base_dw_ext within tp_detail
end type
type tp_detail from userobject within tab_1
dw_detail dw_detail
end type
type tp_detail2 from userobject within tab_1
end type
type dw_detail2 from u_base_dw_ext within tp_detail2
end type
type tp_detail2 from userobject within tab_1
dw_detail2 dw_detail2
end type
type tp_detail3 from userobject within tab_1
end type
type dw_detail3 from u_base_dw_ext within tp_detail3
end type
type tp_detail3 from userobject within tab_1
dw_detail3 dw_detail3
end type
type tp_header from userobject within tab_1
end type
type dw_header from u_base_dw_ext within tp_header
end type
type dw_load_instruction from u_base_dw_ext within tp_header
end type
type dw_age_code_type from u_base_dw_ext within tp_header
end type
type dw_dept_code from u_base_dw_ext within tp_header
end type
type dw_gtl_split_code from u_base_dw_ext within tp_header
end type
type dw_export_country from u_base_dw_ext within tp_header
end type
type tp_header from userobject within tab_1
dw_header dw_header
dw_load_instruction dw_load_instruction
dw_age_code_type dw_age_code_type
dw_dept_code dw_dept_code
dw_gtl_split_code dw_gtl_split_code
dw_export_country dw_export_country
end type
type tp_instruction from userobject within tab_1
end type
type dw_permanent_instructions from u_base_dw_ext within tp_instruction
end type
type mle_1 from u_base_multilineedit_ext within tp_instruction
end type
type tp_instruction from userobject within tab_1
dw_permanent_instructions dw_permanent_instructions
mle_1 mle_1
end type
type tp_currency from userobject within tab_1
end type
type dw_currency from u_base_dw_ext within tp_currency
end type
type tp_currency from userobject within tab_1
dw_currency dw_currency
end type
type tp_confirm from userobject within tab_1
end type
type dw_confirmation from u_base_dw_ext within tp_confirm
end type
type tp_confirm from userobject within tab_1
dw_confirmation dw_confirmation
end type
type tab_1 from tab within w_sales_order_detail
tp_detail tp_detail
tp_detail2 tp_detail2
tp_detail3 tp_detail3
tp_header tp_header
tp_instruction tp_instruction
tp_currency tp_currency
tp_confirm tp_confirm
end type
type dw_cost_weight from u_base_dw_ext within w_sales_order_detail
end type
type dw_programs from u_base_dw_ext within w_sales_order_detail
end type
end forward

shared variables

Long	Gray,&
	White
//NetWise Vars
u_orp001	iu_orp001

uo_ext_sdk_calls iuo_ext_sdk_calls


end variables

global type w_sales_order_detail from w_base_sheet_ext
boolean visible = false
integer x = 5
integer y = 112
integer width = 3963
integer height = 1836
string title = "Sales Order"
event type long ue_move_rows ( unsignedlong wparam,  long lparam )
event type long ue_cancel ( unsignedlong wparam,  long lparam )
event type integer ue_retrieve ( )
event ue_complete_order ( )
event ue_getinstances ( )
event ue_resbrowse ( )
event type integer ue_set_order_id ( )
event type long ue_gen_sales ( )
event ue_print_fax ( )
event ue_fax ( )
event ue_print ( )
event ue_tla ( )
event ue_shortage_que ( )
event ue_auto_price ( )
event ue_ncclicked pbm_nclbuttondown
event ue_email ( )
event ue_tla_email ( )
event ue_tla_local_print ( )
event ue_tla_view ( )
event ue_edi ( )
event type long ue_product_sub ( unsignedlong wparam,  long lparam )
event ue_pa_summary_by_delivery ( )
event ue_order_acceptance_queue ( )
event ue_set_program_data ( )
st_1 st_1
dw_valid_product_info dw_valid_product_info
dw_customer_info dw_customer_info
tab_1 tab_1
dw_cost_weight dw_cost_weight
dw_programs dw_programs
end type
global w_sales_order_detail w_sales_order_detail

type prototypes

end prototypes

type variables
public:
string	is_OrderID, &
	is_To_Order_Id, &
	is_detail_data, &
	is_shortage_ind, &
	is_message, &
	is_description, &
	is_plant_table, &
	is_selected_plant_array, &
	is_header_ind, &
	is_modified_ind, &
	is_columnname, &
	is_detail_orig, &
	is_dept_codes, &
	is_original_type_of_order, &
	is_pricing_default_hdr, &
	is_pricing_default_dtl, &
	is_product_division


Private:
Boolean   ib_rows_Selected,&
	ib_Order_id_Changed_header,&
	ib_Order_id_Changed_Detail,&
	ib_Order_id_Changed_Instruction,&
	ib_Order_id_Changed_Currency, &
	ib_moveCopy,&
	Ib_inquireonopen = TRUE, &
	ib_settabtofocusonopen=TRUE, &
	ib_resolve_running,&
	ib_instruction_Modified, &
	ib_open_on_instruction_tab, &
	ib_all, &
	ib_updated_header = false, &
	ib_header, &
	ib_modified = false, &
	ib_open, &
	ib_detail_updateable, &
	ib_type_of_order_mesg_ovrd = FALSE, &
	ib_update_detail = FALSE
	
long	il_rowBelowMouse,&
	il_Row, &
	il_xpos, &
	il_ypos, &
	il_selected_row, &
	il_selected_plant_max = 10, &
	il_horz_scroll_pos

Char	ic_overide, &
	ic_process_option_in, &
	ic_send_to_sched_ind, &
	ic_process_tla_option_in

Int        	ii_rpc_retvalue, &
	ii_startrow, &
	ii_endrow, &
	ii_dddw_x, ii_dddw_y, ii_rpc_return

String	is_additional_Rows,&
	is_WhoHadLastFocus,&
	is_Header,&
	is_cost_Weight,&
	is_detail,&
	is_ResReductions = "", &
	is_Currency, &
	is_header_business_rule, &
	is_cust_business_rule, &
	is_mle_instruction, &
	is_instruction_type, &
	is_weight_override_string
	
s_error	istr_error_info
application 	ia_apptool
u_ws_orp1		iu_ws_orp1
u_orp002		iu_orp002
u_orp204		iu_orp204
datastore		ids_production_dates
Window		iw_This
w_tooltip_orp	iw_tooltip 
u_ws_orp2       iu_ws_orp2
u_ws_orp3       iu_ws_orp3
u_ws_orp4       iu_ws_orp4
end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_notify_complete (character ac_orderid[5])
public subroutine wf_complete_order ()
public function string wf_getshipdate ()
public function string wf_get_customer ()
public subroutine wf_set_to_order_id (string as_order_id)
public subroutine wf_gettypedesc (ref datawindowchild ldw_childdw)
public function string wf_getdivision ()
public function string wf_get_selected_rows ()
public subroutine wf_resbrowse ()
public function integer wf_checkfor_changes (ref u_base_dw_ext adw_target, string as_string)
public subroutine wf_overridewt_oncomplete (boolean arg_call_complete, ref datawindow adw_detail)
public function boolean wf_add_rows (string as_additional_rows, integer ai_startrow, integer ai_endrow, integer ai_startcolumn, integer ai_endcolumn, integer ai_dwstartcolumn)
public function string wf_reformat (string as_reformat)
public subroutine wf_protect_order (ref datawindow adw_calling)
public subroutine wf_accept_shortage (string args_order_id, character ac_autoresolve, string as_shortage_lines)
public subroutine wf_get_selected_plants (datawindow adw_datawindow)
public subroutine wf_set_selected_plants ()
public subroutine wf_setdetail_businessrules ()
public subroutine wf_overrideweight (string arg_whocalled, boolean arg_call_complete, ref datawindow adw_detail)
public subroutine wf_protect_for_105 ()
public function string wf_get_prod_for_sub ()
public function boolean wf_retrieve ()
public function string wf_remove_nonupdated_fields (string as_inputstring)
public subroutine wf_rightclick ()
public subroutine wf_default_newrow ()
public subroutine wf_load_oper_customers (string as_customer_list)
public subroutine wf_check_microhelp_message (s_error astr_error_info)
public function string wf_get_qty_wt_ind (string as_line_number)
public function string wf_get_productdivision ()
end prototypes

event type long ue_move_rows(unsignedlong wparam, long lparam);String ls_ReturnVal, ls_MicroHelp

long	ll_rowcount, ll_count, ll_countrow, ll_cnt

ib_all = false
//ib_all = True
if ib_rows_selected = False then
	ll_countrow = tab_1.tp_detail.dw_detail.rowcount()
	for ll_cnt = 1 to ll_countrow - 1
		if (tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "scheduled_units") > 0 and &
			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "ordered_units") <=  &
			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "scheduled_units")) then //or  &
//			(tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt +1 , "ordered_units") = 0 and &
//			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "ordered_units") > 0 ) then
//			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt +1 , "ordered_units") = 0 then
				tab_1.tp_detail.dw_detail.selectrow(ll_cnt, true)
				tab_1.tp_detail2.dw_detail2.selectrow(ll_cnt, true)
				tab_1.tp_detail3.dw_detail3.selectrow(ll_cnt, true)
		end if 
	next
	ib_all = True
end if

//if ib_rows_selected then 
//	if tab_1.tp_detail.dw_detail.Getselectedrow(0) > 0 then
//		ll_rowcount = tab_1.tp_detail.dw_detail.rowcount()
//		ll_count = 1
//		do while ll_count <= ll_rowcount - 1 and ib_all
//			if tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "scheduled_units") = 0  or &
//			tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "ordered_units") >  &
//			tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "scheduled_units")  then
//				tab_1.tp_detail.dw_detail.selectrow(ll_count, false)
//				tab_1.tp_detail2.dw_detail2.selectrow(ll_count, false)
//				tab_1.tp_detail3.dw_detail3.selectrow(ll_count, false)
//			else
//				if not tab_1.tp_detail.dw_detail.isselected(ll_count) then ib_all = False
//			end if
//			ll_count ++
//		loop	
//	else
//		if tab_1.tp_detail2.dw_detail2.Getselectedrow(0) > 0 then
//			ll_rowcount = tab_1.tp_detail2.dw_detail2.rowcount()
//			ll_count = 1
//			do while ll_count <= ll_rowcount - 1 and ib_all
//				if tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "ordered_units") = 0 then
//					tab_1.tp_detail.dw_detail.selectrow(ll_count, false)
//					tab_1.tp_detail2.dw_detail2.selectrow(ll_count, false)
//					tab_1.tp_detail3.dw_detail3.selectrow(ll_count, false)
//				else
//					if not tab_1.tp_detail2.dw_detail2.isselected(ll_count) then ib_all = False
//				end if
//				ll_count ++
//			loop		
//		else
//			if tab_1.tp_detail3.dw_detail3.Getselectedrow(0) > 0 then
//				ll_rowcount = tab_1.tp_detail3.dw_detail3.rowcount()
//				ll_count = 1
//				do while ll_count <= ll_rowcount - 1 and ib_all
//					if tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "ordered_units") = 0 then
//						tab_1.tp_detail.dw_detail.selectrow(ll_count, false)
//						tab_1.tp_detail2.dw_detail2.selectrow(ll_count, false)
//						tab_1.tp_detail3.dw_detail3.selectrow(ll_count, false)
//					else
//						if not tab_1.tp_detail3.dw_detail3.isselected(ll_count) then ib_all = False
//					end if
//					ll_count ++
//				loop	
//			end if
//		end if
//	end if
//end if

			

OpenWithParm( w_move_product_response, This)

ls_Returnval = Message.StringParm

IF ls_Returnval = 'OK' Then
	ls_MicroHelp = iw_frame.wf_getmicrohelp()
	This.TriggerEvent("ue_Retrieve")
	ib_rows_selected = False
	iw_frame.SetMicroHelp(ls_MicroHelp)
END IF

RETURN 1
end event

event type long ue_cancel(unsignedlong wparam, long lparam);//IF MessageBox("Cancel Order", "Are you sure you want to cancel this order", Question!, YesNo!, 2) = 2 Then Return 1
//
//istr_error_info.se_event_name = "ue_cancel"
//istr_error_info.se_procedure_name = "orpo63ar"
//istr_error_info.se_message = Space(71)
//
//IF Not IsValid( iu_orp001) Then iu_orp001 = Create u_orp001
//	
////  Display error info if rval < 0 	
//IF iu_orp001.nf_orpo63ar_cancel_sales_order( &
//			istr_error_info, is_orderid) < 0 THEN RETURN 1
//			
////  Return message in microhelp if rval = 2, prevent re-inquire so message stays
//IF iu_orp001.nf_orpo63ar_cancel_sales_order( &
//			istr_error_info, is_orderid) = 2 THEN RETURN 1
//
//This.TriggerEvent("ue_Retrieve")
//RETURN 1

long		 ll_rval 
string	ls_microhelp

IF MessageBox("Cancel Order", "Are you sure you want to cancel this order", Question!, YesNo!, 2) = 2 Then Return 1

istr_error_info.se_event_name = "ue_cancel"
istr_error_info.se_procedure_name = "orpo63ar"
istr_error_info.se_message = Space(71)

//IF Not IsValid( iu_orp001) Then iu_orp001 = Create u_orp001
	
//  Display error info if rval < 0 	
//ll_rval = iu_orp001.nf_orpo63ar_cancel_sales_order( &
//			istr_error_info, is_orderid) 

IF Not IsValid( iu_ws_orp3) Then iu_ws_orp3 = Create u_ws_orp3
	
//  Display error info if rval < 0 	
ll_rval = iu_ws_orp3.uf_orpo63fr_cancel_sales_order( &
			istr_error_info, is_orderid) 

if (ll_rval < 0) or (ll_rval = 2) then
	return 1
end if
	
////  Return message in microhelp if rval = 2, prevent re-inquire so message stays
//IF iu_orp001.nf_orpo63ar_cancel_sales_order( &
//			istr_error_info, is_orderid) = 2 THEN RETURN 1
//

ls_microhelp	=	iw_frame.wf_getmicrohelp()
iw_frame.SetMicrohelp(ls_microhelp)

This.TriggerEvent("ue_Retrieve")

iw_frame.SetMicrohelp(ls_microhelp)


RETURN 1
end event

event type integer ue_retrieve();dw_customer_info.TriggerEvent('ue_retrieve')
//CHOOSE CASE	is_whohadlastfocus
//	CASE 'DETAIL'
//		ib_order_id_changed_detail	= True
//		tab_1.tp_detail.dw_detail.TriggerEvent("ue_retrieve")
//		wf_setdetail_businessrules()
//	CASE 'HEADER'
//		ib_order_id_changed_header	=	True
//		tab_1.tp_header.dw_header.TriggerEvent("ue_retrieve")
//	CASE 'INSTRUCTION'
//		ib_order_id_changed_instruction	=	True
//		tab_1.tp_instruction.dw_permanent_instructions.TriggerEvent("ue_retrieve")
//		wf_setdetail_businessrules()
//	CASE 'CURRENCY'
//		ib_order_id_changed_currency	= True
//		tab_1.tp_currency.dw_currency.TriggerEvent("ue_retrieve")
//		wf_setdetail_businessrules()
//	Case 'CONFIRMATION'
//		tab_1.tp_confirm.dw_confirmation.TriggerEvent("ue_retrieve")
//END CHOOSE
CHOOSE CASE	is_whohadlastfocus
	CASE 'DETAIL'
		ib_order_id_changed_header	=	True
		tab_1.tp_header.dw_header.TriggerEvent("ue_retrieve")
		ib_order_id_changed_detail	= True
		tab_1.tp_detail.dw_detail.TriggerEvent("ue_retrieve")
		wf_setdetail_businessrules()
	CASE 'HEADER'
		ib_order_id_changed_detail	= True
		tab_1.tp_detail.dw_detail.TriggerEvent("ue_retrieve")
		wf_setdetail_businessrules()
		ib_order_id_changed_header	=	True
		tab_1.tp_header.dw_header.TriggerEvent("ue_retrieve")
	CASE 'INSTRUCTION'
		ib_order_id_changed_detail	= True
		tab_1.tp_detail.dw_detail.TriggerEvent("ue_retrieve")
		wf_setdetail_businessrules()
//		ib_order_id_changed_header	=	True
//		tab_1.tp_header.dw_header.TriggerEvent("ue_retrieve")
		ib_order_id_changed_instruction	=	True
		tab_1.tp_instruction.dw_permanent_instructions.TriggerEvent("ue_retrieve")
		wf_setdetail_businessrules()
	CASE 'CURRENCY'
		ib_order_id_changed_detail	= True
		tab_1.tp_detail.dw_detail.TriggerEvent("ue_retrieve")
		wf_setdetail_businessrules()
//		ib_order_id_changed_header	=	True
//		tab_1.tp_header.dw_header.TriggerEvent("ue_retrieve")
		ib_order_id_changed_currency	= True
		tab_1.tp_currency.dw_currency.TriggerEvent("ue_retrieve")
		wf_setdetail_businessrules()
	Case 'CONFIRMATION'
		tab_1.tp_confirm.dw_confirmation.TriggerEvent("ue_retrieve")
END CHOOSE
//dw_cost_weight.Modify("pricing_mass_override.Visible='0'")
//dw_cost_weight.Modify("t_8.Visible='0'")
RETURN 0 
end event

event ue_complete_order;call super::ue_complete_order;wf_complete_order()
end event

event ue_getinstances;call super::ue_getinstances;IF	dw_customer_info.is_incomplete() THEN
	Message.StringParm	=	''
ELSE
	Message.StringParm	=	is_orderid
END IF
end event

event ue_resbrowse;call super::ue_resbrowse;tab_1.tp_detail.dw_detail.TriggerEvent('ue_resbrowse')
end event

event ue_set_order_id;call super::ue_set_order_id;is_orderid = Message.StringParm
This.TriggerEvent('ue_retrieve')
RETURN 1
end event

event type long ue_gen_sales();String ls_ReturnVal, &
		ls_MicroHelp, ls_temp
		
long		ll_rowcount,ll_count, ll_countrow, ll_cnt
//
ib_all = false
//ib_all = True
if ib_rows_selected = False then
	ll_countrow = tab_1.tp_detail.dw_detail.rowcount()
	for ll_cnt = 1 to ll_countrow - 1       
		if (tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "scheduled_units") > 0   and & 
 			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "ordered_units") <=  &
			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "scheduled_units")) then // or  &
//			(tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt +1 , "ordered_units") = 0 and &
//			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt, "ordered_units") > 0 ) then
//			tab_1.tp_detail.dw_detail.getitemnumber(ll_cnt +1 , "ordered_units") = 0 then
				tab_1.tp_detail.dw_detail.selectrow(ll_cnt, true)
				tab_1.tp_detail2.dw_detail2.selectrow(ll_cnt, true)
				tab_1.tp_detail3.dw_detail3.selectrow(ll_cnt, true)
		end if 
	next
	ib_all = True
end if

//if ib_rows_selected then 
//	if tab_1.tp_detail.dw_detail.Getselectedrow(0) > 0 then
//		ll_rowcount = tab_1.tp_detail.dw_detail.rowcount()
//		ll_count = 1
//		do while ll_count <= ll_rowcount - 1 and ib_all
//			if (tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "scheduled_units") = 0  or &
//				tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "ordered_units") >  &
//				tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "scheduled_units")) then
//				tab_1.tp_detail.dw_detail.selectrow(ll_count, false)
//				tab_1.tp_detail2.dw_detail2.selectrow(ll_count, false)
//				tab_1.tp_detail3.dw_detail3.selectrow(ll_count, false)
//			else
//				if not tab_1.tp_detail.dw_detail.isselected(ll_count) then ib_all = False
//			end if
//			ll_count ++
//		loop	
//	end if
//end if
//		if tab_1.tp_detail2.dw_detail2.Getselectedrow(0) > 0 then
//			ll_rowcount = tab_1.tp_detail2.dw_detail2.rowcount()
//			ll_count = 1
//			do while ll_count <= ll_rowcount - 1 and ib_all
//				if tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "ordered_units") = 0 then
//					tab_1.tp_detail.dw_detail.selectrow(ll_count, false)
//					tab_1.tp_detail2.dw_detail2.selectrow(ll_count, false)
//					tab_1.tp_detail3.dw_detail3.selectrow(ll_count, false)
//				else
//					if not tab_1.tp_detail2.dw_detail2.isselected(ll_count) then ib_all = False
//				end if
//				ll_count ++
//			loop		
//		else
//			if tab_1.tp_detail3.dw_detail3.Getselectedrow(0) > 0 then
//				ll_rowcount = tab_1.tp_detail3.dw_detail3.rowcount()
//				ll_count = 1
//				do while ll_count <= ll_rowcount - 1 and ib_all
//					if tab_1.tp_detail.dw_detail.getitemnumber(ll_count, "ordered_units") = 0 then
//						tab_1.tp_detail.dw_detail.selectrow(ll_count, false)
//						tab_1.tp_detail2.dw_detail2.selectrow(ll_count, false)
//						tab_1.tp_detail3.dw_detail3.selectrow(ll_count, false)
//					else
//						if not tab_1.tp_detail3.dw_detail3.isselected(ll_count) then ib_all = False
//					end if
//					ll_count ++
//				loop	
//			end if
//		end if
//	end if
//end if

ls_temp = wf_get_selected_rows()

is_pricing_default_hdr = left(dw_cost_weight.GetItemString(1, "pricing_default_hdr"),1)
is_pricing_default_dtl = left(dw_cost_weight.GetItemString(1, "pricing_default_dtl"),1)

if len(ls_temp) > 0 then
	OpenWithParm( w_gen_order_response, This)
else
	iw_frame.SetMicroHelp("No valid rows selected")
end if

ls_Returnval = Message.StringParm

IF ls_Returnval = 'OK' Then
	ls_MicroHelp = iw_frame.wf_getmicrohelp()
	This.TriggerEvent("ue_Retrieve")
	iw_frame.SetMicroHelp( ls_MicroHelp)
	ib_rows_selected = False
END IF

RETURN 1

end event

event ue_print_fax();INTEGER	li_rtn

String	ls_order_id, ls_order_out, ls_customer_email

ls_order_id = dw_customer_info.Getitemstring(1, 'order_id') 
istr_error_info.se_event_name = 'ue_print_fax'

IF NOT isValid(iu_orp002) THEN
	iu_orp002 = Create u_orp002
End IF

li_rtn = iu_ws_orp4.nf_ORPO80FR(istr_error_info, &
										ic_process_option_in, &
										ls_order_id, &
										ls_order_out, &
										ls_customer_email, &
										ic_process_tla_option_in)

IF li_rtn >= 0 THEN
	This.SetRedraw(TRUE)
	Return
END IF
end event

event ue_fax;call super::ue_fax;ic_process_option_in = 'F'

This.triggerevent('ue_print_fax')
end event

event ue_print;call super::ue_print;ic_process_option_in = 'P'

This.triggerevent('ue_print_fax')
end event

event ue_tla();ic_process_option_in = 'A'
ic_process_tla_option_in = 'F'

This.triggerevent('ue_print_fax')

end event

event ue_shortage_que;call super::ue_shortage_que;//String	ls_Order_ID, ls_indicator

//Window	lw_shortagequeue
//
//ls_indicator = "S"
//
//This.Event ue_Get_Data("Order_ID")
//ls_Order_Id = Message.StringParm + '~t'
//ls_Order_Id += ls_indicator

is_shortage_ind = 'S'

OpenWithParm(w_product_shortage_response, THIS)
end event

event ue_auto_price();Integer	li_rtn
String	ls_order_id, ls_order_out, &
			ls_MicroHelp

ls_order_id = dw_customer_info.Getitemstring(1, 'order_id') 
istr_error_info.se_event_name = 'ue_auto_price'

IF NOT isValid(iu_orp204) THEN
	iu_orp204 = Create u_orp204
End IF

li_rtn = iu_ws_orp4.nf_ORPO86FR(istr_error_info,	ls_order_id)
IF li_rtn = 0 THEN
	ls_MicroHelp = iw_frame.wf_getmicrohelp()
	This.TriggerEvent("ue_Retrieve")
	iw_frame.SetMicroHelp( ls_MicroHelp)
END IF
end event

event ue_ncclicked;if ib_open then close(w_locations_popup)
end event

event ue_email();string	ls_sales_order_id, ls_order_status

ls_sales_order_id = dw_customer_info.GetItemstring(1, "order_id")

ls_order_status = dw_customer_info.Getitemstring(1, 'order_status')

If ls_order_status = 'A' then
	OpenWithParm(w_email_order_confirmations, ls_sales_order_id)
else
	iw_frame.SetMicroHelp( "Sales Order must be Accepted")
End if
end event

event ue_tla_email();ic_process_option_in = 'A'
ic_process_tla_option_in = 'E'

This.triggerevent('ue_print_fax')

end event

event ue_tla_local_print();ic_process_option_in = 'A'
ic_process_tla_option_in = 'P'

This.triggerevent('ue_print_fax')

end event

event ue_tla_view();ic_process_option_in = 'A'
ic_process_tla_option_in = 'V'

This.triggerevent('ue_print_fax')

end event

event ue_edi();ic_process_option_in = 'E'

This.triggerevent('ue_print_fax')
end event

event type long ue_product_sub(unsignedlong wparam, long lparam);Long		ll_row

String	ls_OpenString, &
			ls_Returnval

Window	lw_Temp

if ib_rows_selected = True then
	OpenWithParm( w_product_sub, This)
	ls_Returnval = Message.StringParm

	IF ls_Returnval = 'OK' Then
		This.TriggerEvent("ue_Retrieve")
		ib_rows_selected = False
	END IF
End If

Return 0
end event

event ue_pa_summary_by_delivery();String	ls_OpenString 

Window	lw_Temp

ls_OpenString = String(dw_cost_weight.GetItemDate(1,"delv_date"),"MM/dd/yyyy") &
									+"~t"+ dw_cost_weight.GetItemString(1,"ship_plant") &
									+"~t"+ dw_customer_info.GetItemString(1,"customer_id") + "~tD"
									
									
OpenSheetWithParm(lw_Temp, ls_OpenString,"w_pa-summary", &
						iw_frame,0,iw_frame.im_menu.iao_arrangeOpen)
end event

event ue_order_acceptance_queue();String		ls_order
Window lw_temp


ls_order = dw_customer_info.GetItemString(1, "order_id")
OpenSheetWithParm(lw_temp, ls_order, "w_order_acceptance_queue",iw_frame,0,iw_frame.im_menu.iao_arrangeopen) 
end event

event ue_set_program_data();String	ls_string

dw_programs.Reset()
ls_string = dw_customer_info.is_program_string
dw_programs.ImportString(dw_customer_info.is_program_string)
dw_programs.ResetUpdate()
dw_programs.Height = 195
end event

public function boolean wf_update ();CHOOSE CASE is_whohadlastfocus 
	CASE 'DETAIL' 
		tab_1.tp_detail.dw_detail.TriggerEvent("ue_update")
	CASE 'HEADER'
		tab_1.tp_header.dw_header.TriggerEvent("ue_update")
	CASE 'INSTRUCTION'
		tab_1.tp_instruction.mle_1.TriggerEvent("ue_update")
END CHOOSE
Return True



end function

public function boolean wf_notify_complete (character ac_orderid[5]);Boolean lb_Notify
// Let PA Display know that an order changed.  If it is open,
// then it will need to refresh the orders on activate
Window	lw_ToNotify

lw_ToNotify = iw_Frame.GetFirstSheet()
Do While IsValid(lw_ToNotify)
	lb_Notify = TRUE
	lw_ToNotify.TriggerEvent('ue_salesorderchanged', 0, ac_orderid)
	lw_ToNotify = iw_Frame.GetNextSheet(lw_ToNotify)
Loop

Return lb_Notify
end function

public subroutine wf_complete_order ();String	ls_Load_Status, &
		ls_weight_error_Flag, &
		ls_weight_Override, &
		ls_net_order_flag, &
		ls_order_status, &
		ls_send_to_sched_ind

Char	lc_Load_Status, &
		lc_order_id[5], &
		lc_weight_error_Flag, &
		lc_weight_Override, &
		lc_net_order_flag, &
		lc_order_status, &
		lc_send_to_sched_ind 
		
Int	li_retn, &
		li_shtg_line_count, li_row_cnt, li_row
		
Long	ll_temp

String   ls_order_id, &
			ls_Rtn, &
			ls_weight, &
			ls_detail_data, &
			ls_shortage_lines, ls_business_rule
Boolean lb_complete
			
lb_complete = false
			
u_string_functions	lu_string_function			

li_row_cnt = tab_1.tp_detail.dw_detail.RowCount()
			
lc_order_status	=	dw_customer_info.GetItemString( 1, "order_status")
IF LEN(TRIM(lc_order_status)) > 0 Then
	MessageBox("User Error", "Order already Complete")
	Return
END IF
		
If ib_resolve_running Then
	ib_resolve_running = False
Else
	CHOOSE CASE This.wf_modified( )
		CASE 1
			// Changes were made to one of the datawindows
			CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", &
											Question!, YesNoCancel!)
			CASE 1	// Save Changes
				This.wf_update()
				if ii_rpc_return = 1 then 
					Message.ReturnValue = 1
					return
				end if
//					If This.wf_update() = FALSE Then
//						Message.ReturnValue = 1	 
//					End If
			CASE 2  // Do not save changes, but complete the order
				for li_row =1 to li_row_cnt
					if Mid(tab_1.tp_detail.dw_detail.GetItemString(li_row, "business_rules"), 9, 1) = 'E' then 
						lb_complete = true
					end if 
				next 
				
				if lb_complete = true then 
					iw_frame.SetMicroHelp( "Please override prices before completing the order")
					return
				end if 
						
				
	
			CASE ELSE	// Do not save changes, do not complete the order
					Return
			END CHOOSE
		CASE 0
				// No modifications were made to any datawindows on this sheet...closing of sheet allowed
		
		CASE -1
			// Problem with the AcceptText() function against a datawindow.
			// The item error will handle setting focus to the correct column. 
			Message.ReturnValue = 1
			Return
	END CHOOSE
End If

lc_Load_Status= dw_cost_weight.GetItemString(1, 'load_stat')
lc_order_id = is_orderid
lc_weight_Override = ic_overide
lc_net_order_flag = 'N'

iw_frame.SetMicroHelp( "Wait... Completing Order")
IF Not IsValid( iu_orp001) Then 	iu_orp001 = Create u_orp001

istr_error_info.se_Event_name = 'wf_complete_order'

//li_retn =  iu_orp001.nf_orpo05ar(istr_error_info,&
//    										lc_order_id, &
//										   lc_load_status, &
//										   lc_order_status, &
//										   lc_weight_error_flag, &
//											lc_weight_override, &
//										   lc_net_order_flag, &
//											ls_detail_data, &
//											li_shtg_line_count, &
//											lc_send_to_sched_ind) 
											
											
ls_Load_Status = String(lc_load_status) 
ls_weight_error_Flag =  String(lc_weight_error_flag)
ls_weight_Override  = String(lc_weight_override)
ls_net_order_flag = String(lc_net_order_flag)
ls_order_status = String(lc_order_status)
ls_send_to_sched_ind		= String(lc_send_to_sched_ind)	
ls_order_id = String(lc_order_id)
											
li_retn =  iu_ws_orp1.nf_orpo05fr(istr_error_info,&
    										ls_order_id, &
										 ls_Load_Status, &
										  ls_Load_Status, &
										   ls_weight_error_Flag, &
											ls_weight_Override, &
										  ls_net_order_flag, &
											ls_detail_data, &
											li_shtg_line_count, &
											ls_send_to_sched_ind) 											
											
											
ic_send_to_sched_Ind = ls_send_to_sched_ind

ic_overide = ''
Do While li_shtg_line_count > 0 
	is_detail_data = ls_detail_data
	//Open the popup window
	CHOOSE CASE ls_weight_error_Flag
		CASE 'S', 'B'
			is_shortage_ind = 'C'
			OpenwithParm(w_product_shortage_response, THIS) 
			ls_rtn =	Message.StringParm
			lu_string_function.nf_parseleftright(ls_rtn, "~t", &
												ls_rtn, ls_shortage_lines)
		CHOOSE CASE ls_Rtn
			CASE	"A"
				ls_order_id = lc_order_id
				wf_accept_shortage(ls_order_ID,"", ls_shortage_lines)
				iw_frame.SetMicroHelp(is_message)
			Case "Q"
				ls_order_id = lc_order_id
				wf_accept_shortage(ls_order_ID, "Q", ls_shortage_lines)
				iw_frame.SetMicroHelp(is_message)
			CASE 	"ABORT"
				This.PostEvent( "ue_retrieve") 
				Return
			CASE	"R"
				Openwithparm(w_resolve_shortage, ls_shortage_lines)
				This.TriggerEvent( "ue_retrieve")
				wf_complete_order()
				Return
		END CHOOSE
	END CHOOSE

	CHOOSE CASE ls_weight_error_Flag
		CASE 'W', 'B'
			IF IsNull(dw_cost_weight.GetItemNumber( 1,"gross_weight")) THEN
				ls_weight	=	'0.00'
			ELSE
				ls_weight	=	String(dw_cost_weight.GetItemDecimal( 1,"gross_weight"))
			END IF

			ll_temp = lu_string_function.nf_countoccurrences(ls_shortage_lines,'~t')
		 	If ll_temp = li_shtg_line_count or ls_weight_error_Flag = 'W' Then
				OpenWithParm(w_weight_Criteria, ls_weight)
				IF Message.StringParm	<> 'ABORT' THEN 
					ic_overide	=	Message.StringParm
				END IF
					wf_Overrideweight("complete_Order", ls_weight_error_Flag = 'W', &
											tab_1.tp_detail.dw_detail)
				IF ls_weight_error_Flag = 'W' THEN 
					Return
				END IF
			End if
			If ls_weight_error_Flag = 'B' then
				If ls_Rtn = 'A' or ls_Rtn = 'Q' Then
					ls_detail_data = ''
					li_shtg_line_count = 0
				Else
					ls_detail_data = ls_shortage_lines
					li_shtg_line_count = 99
				End If
			End If
			
		CASE	'S'
			If ls_Rtn = 'A' or ls_Rtn = 'Q' Then
				ls_detail_data = ''
				li_shtg_line_count = 0
			Else
				ls_detail_data = ls_shortage_lines
				li_shtg_line_count = 99
			End If
		CASE ELSE
			This.PostEvent( "ue_retrieve")
			IF Not wf_notify_complete(	ls_order_id) Then 
				SetMicroHelp("Refresh Failed Other windows not notified of complete")
			END IF 
			Return
	END CHOOSE

	IF ls_weight_error_Flag = 'B' THEN
		lc_weight_Override = ic_overide
	ELSE
		lc_weight_Override = 'F'
	END IF
	ls_net_order_flag = 'N'
		

//li_retn =  iu_orp001.nf_orpo05ar(istr_error_info,&
//    										lc_order_id, &
//										   lc_load_status, &
//										   lc_order_status, &
//										   lc_weight_error_flag, &
//											lc_weight_override, &
//										   lc_net_order_flag, &
//											ls_detail_data, &
//											li_shtg_line_count, &
//											lc_send_to_sched_ind) 
										
		ls_Load_Status = String(lc_load_status) 
		ls_weight_error_Flag =  String(lc_weight_error_flag)
		ls_weight_Override  = String(lc_weight_override)
		ls_net_order_flag = String(lc_net_order_flag)
		ls_order_status = String(lc_order_status)
		ls_send_to_sched_ind	 = String(lc_send_to_sched_ind)	
		ls_order_id = String(lc_order_id)
		
		li_retn = iu_ws_orp1.nf_orpo05fr(istr_error_info, &
											ls_order_id, &
											ls_Load_Status, &
											ls_order_status, &
											ls_weight_error_Flag, &
											ls_weight_Override, &
											ls_net_order_flag, &
											ls_detail_data, &
											li_shtg_line_count, &
											ls_send_to_sched_ind)										
											
											
	ic_send_to_sched_Ind = ls_send_to_sched_ind										
	ic_overide = ''

	IF ls_rtn = 'A' OR ls_rtn = 'Q' THEN
			iw_frame.SetMicroHelp(is_message)
	END IF
LOOP

if li_retn = 5 then
	return
end if 

This.PostEvent( "ue_retrieve")
dw_cost_weight.ResetUpdate()
dw_customer_info.ResetUpdate()
IF Not wf_notify_complete(	ls_order_id) Then 
	SetMicroHelp("Refresh Failed Other windows not notified of complete")
END IF
end subroutine

public function string wf_getshipdate ();String ls_String
ls_String = String(dw_cost_weight.GetItemDate(1,"ship_date"),"MM/DD/YY")
Return ls_String
end function

public function string wf_get_customer ();Return dw_customer_info.uof_get_customer_id ( )
end function

public subroutine wf_set_to_order_id (string as_order_id);is_to_order_id = as_order_ID
end subroutine

public subroutine wf_gettypedesc (ref datawindowchild ldw_childdw);IF ldw_childdw.RowCount()	> 0 THEN RETURN
ldw_childdw.SetTransObject(SQLCA)
ldw_childdw.Retrieve()

end subroutine

public function string wf_getdivision ();Return dw_customer_info.GetItemString(1,"division_code")
end function

public function string wf_get_selected_rows ();String		ls_selected

DataStore	lds_selected

lds_selected	=	Create DataStore
lds_selected.DataObject		=	'd_sales_detail_part1'
if tab_1.tp_detail.dw_detail.GetSelectedRow(0)  > 0 then
	lds_selected.Object.Data	=	tab_1.tp_detail.dw_detail.Object.Data.Selected
end if
ls_selected	=	lds_selected.object.datawindow.data
Destroy	lds_selected
RETURN ls_selected

end function

public subroutine wf_resbrowse ();tab_1.tp_detail.dw_detail.TriggerEvent('ue_resbrowse')
end subroutine

public function integer wf_checkfor_changes (ref u_base_dw_ext adw_target, string as_string);integer li_rtn

IF adw_target.DataObject = 'd_cost_weight' or adw_target.DataObject = 'd_sales_header' THEN
	adw_target.AcceptText()
	dw_customer_info.AcceptText()
	Tab_1.tp_header.dw_header.AcceptText()
	IF adw_target.uf_modified() = 1 or dw_customer_info.uf_modified() = 1 or Tab_1.tp_header.dw_header.uf_modified() = 1 THEN
		li_rtn = MessageBox('Save Changes', 'Do you want to save changes made?', Question!, YESNOCANCEL!, 1)
	ELSE
		RETURN 0
	END IF
ELSE
	adw_target.AcceptText()
	IF adw_target.uf_modified() =  1 THEN
		li_rtn = MessageBox('Save Changes', 'Do you want to save changes made?', Question!, YESNOCANCEL!, 1)
	ELSE
		RETURN 0
	END IF
End IF

CHOOSE CASE li_rtn
	CASE 1
		IF adw_target.DataObject = 'd_cost_weight' or adw_target.DataObject = 'd_sales_header' THEN
			Tab_1.tp_header.dw_header.TriggerEvent('ue_update')
			IF ii_rpc_retvalue < 0 or ii_rpc_retvalue > 500 or &
					ii_rpc_retvalue = 1 or ii_rpc_retvalue = 5 THEN
				RETURN 1
			ELSE
				RETURN 0
			END IF
		ELSE
			adw_target.TriggerEvent('ue_update')
			IF ii_rpc_retvalue < 0 or ii_rpc_retvalue > 500 or &
					ii_rpc_retvalue = 1 or ii_rpc_retvalue = 5 THEN
				RETURN 1
			ELSE
				RETURN 0
			END IF
		END IF
		
	CASE 2
		IF adw_target.DataObject = 'd_cost_weight' or adw_target.DataObject = 'd_sales_header' THEN
			adw_target.Reset()
         	adw_target.ImportString(as_string)
            	adw_target.ResetUpdate()
			adw_target.ib_updateable = False
		ELSE
			adw_target.Reset()
			adw_target.ImportString(as_string)
			adw_target.ResetUpdate()
			adw_target.ib_updateable = False
		END IF
		Tab_1.tp_header.dw_header.Reset()
		Tab_1.tp_header.dw_header.ImportString(is_Header)
		Tab_1.tp_header.dw_header.ResetUpdate()
		Tab_1.tp_header.dw_header.ib_updateable =  False
		dw_customer_info.Reset()
		dw_customer_info.uof_retrieve()
		dw_customer_info.ResetUpdate()
		dw_customer_info.ib_updateable = False
		dw_cost_weight.Reset()
		dw_cost_weight.ImportString(is_cost_Weight)
		dw_cost_weight.ResetUpdate()
		dw_cost_weight.ib_updateable = False
		RETURN 0
		
	CASE  3
		RETURN 1
		
END CHOOSE

end function

public subroutine wf_overridewt_oncomplete (boolean arg_call_complete, ref datawindow adw_detail);Char		lc_Load_Status, &
			lc_order_id[5], &
			lc_order_Status, & 
			lc_weight_error_Flag, &	
			lc_net_order_flag, &
			lc_send_to_sched_ind
			
			
String   ls_order_id, &
			ls_Rtn, &
			ls_weight, &
			ls_shortage_lines			
			
String	ls_Load_Status, &
		ls_weight_error_Flag, &
		ls_weight_Override, &
		ls_net_order_flag, &
		ls_order_status, &
		ls_send_to_sched_ind			
		
Int		li_rtn, li_shtg_line_count

String	ls_detail_data

lc_load_status= dw_cost_weight.GetItemString(1, 'load_stat')
lc_Order_ID = is_orderid
lc_net_order_flag = 'N'

IF arg_Call_complete THEN
	
//	li_rtn =  iu_orp001.nf_orpo05ar(istr_error_info, lc_Order_ID, &
//  		 lc_load_status, lc_order_status, lc_weight_error_flag, &
//	    ic_overide, lc_net_order_flag, ls_detail_data, &
//		 li_shtg_line_count, lc_send_to_sched_ind)
	
		ls_Load_Status = String(lc_load_status) 
		ls_weight_error_Flag =  String(lc_weight_error_flag)
		ls_weight_Override  = String( ic_overide)
		ls_net_order_flag = String(lc_net_order_flag)
		ls_order_status = String(lc_order_status)
		ls_send_to_sched_ind		= String(lc_send_to_sched_ind)	
		ls_order_id = String(lc_order_id)
											
		li_rtn =  iu_ws_orp1.nf_orpo05fr(istr_error_info,&
    										ls_order_id, &
										 ls_Load_Status, &
										  ls_Load_Status, &
										   ls_weight_error_Flag, &
											ls_weight_Override, &
										  ls_net_order_flag, &
											ls_detail_data, &
											li_shtg_line_count, &
											ls_send_to_sched_ind) 	
		 
		 
	ic_send_to_sched_Ind = lc_send_to_sched_ind
	ic_overide = ' '
	iw_this.PostEvent( "ue_retrieve")
END IF
end subroutine

public function boolean wf_add_rows (string as_additional_rows, integer ai_startrow, integer ai_endrow, integer ai_startcolumn, integer ai_endcolumn, integer ai_dwstartcolumn);String	ls_Whole_String

long		ll_crln_pos

ls_Whole_String = as_additional_rows
ll_crln_pos = POS( ls_Whole_String, "~r~n")
Do While ll_crln_pos > 0 
	is_additional_rows += "A~t~t"+Left(ls_Whole_String, ll_crln_pos+1)
	ls_Whole_String = Mid(ls_Whole_String, ll_crln_pos+2)
	ll_crln_pos = POS( ls_Whole_String, "~r~n")
Loop
IF Len( Trim( ls_Whole_String)) > 0 Then is_additional_rows += "A~t~t"+Trim( ls_Whole_String)

ii_startrow = ai_startRow 
ii_endrow	=	ai_endrow
//ii_startcolumn = ai_startColumn
//ii_endcolumn = ai_end_column
//ii_dwstartcolumn = ai_dwstartcolumn
This.BringToTop = True
This.SetFocus()
RETURN TRUE

end function

public function string wf_reformat (string as_reformat);Long		ll_PlaceHolder,&
			ll_PLaceHolder2
String	ls_ReturnString

ls_ReturnString = '' // This can not be NULL to start

DO
	ll_PLaceHolder = Pos(as_Reformat, "~r~n")
	if ll_PlaceHolder < 1 then  // No carraige return line feed left in string
		IF Len(as_Reformat) > 40 Then
			IF Mid( as_Reformat,41,1) = ' ' Then
				ls_ReturnString += Mid( as_Reformat,1,40)
				as_Reformat = Mid( as_Reformat,41)
			ELSE
				ll_PLaceHolder2 = 40
				DO 
					IF Mid( as_Reformat, ll_PlaceHolder2,1) = ' ' Then
						ls_ReturnString += Mid( as_Reformat, 1, ll_PlaceHolder2)
						as_Reformat = Mid( as_Reformat, ll_PlaceHolder2 + 1)
						exit
					END IF
					ll_PlaceHolder2 --
				Loop While ll_PlaceHolder2 > 0
				IF ll_PLaceHolder2 = 0 Then 
					ls_ReturnString += Mid( as_Reformat, 1, 40)
					as_Reformat = Mid(as_Reformat, 41)
				END IF
			END IF
		ELSE
			ls_ReturnString += as_Reformat
			as_reformat = ''
		END IF
	ELSE
		IF ll_PLaceHolder > 40 Then
			IF Mid(as_Reformat, 41,1) = ' ' Then
				ls_ReturnString += Mid(as_Reformat,1,40)
				as_Reformat = Mid(as_Reformat,41)
			ELSE
				ll_PLaceHolder2 = 40
				DO 
					IF Mid( as_Reformat, ll_PlaceHolder2,1) = ' ' Then
						ls_ReturnString += Mid( as_Reformat, 1, ll_PlaceHolder2)
						as_Reformat = Mid( as_Reformat, ll_PlaceHolder2 + 1)
						exit
					END IF
					ll_PlaceHolder2 --
				Loop While ll_PlaceHolder2 > 0
				IF ll_PLaceHolder2 = 0 Then 
					ls_ReturnString += Mid( as_Reformat, 1, 40)
					as_Reformat = Mid(as_Reformat, 41)
				END IF
			END IF
		ELSE
			ls_ReturnString += Mid( as_Reformat, 1, ll_PLaceHolder - 1)
			as_Reformat = Mid( as_Reformat, ll_PLaceHolder + 2) // beyond the carriage return
		END IF
	END IF
	IF Len(as_reformat)	> 0 Then ls_ReturnString += "~r~n"
Loop While Len(as_Reformat) > 0
Return ls_ReturnString
end function

public subroutine wf_protect_order (ref datawindow adw_calling);u_project_functions lu_project_functions
integer	li_rtn
string	ls_input_string, ls_customer_loc, ls_type_code, ls_code
Boolean lb_view_loc
//
ls_customer_loc = dw_customer_info.GetItemString(1, "smanlocaton")
ls_code = ls_customer_loc + Message.is_smanlocation
lb_view_loc = false

SQLCA.nf_connect()

SELECT tutltypes.type_code
    INTO :ls_type_code
    FROM tutltypes
    WHERE tutltypes.record_type = 'VIEWLOC'
    AND type_code = :ls_code
	USING SQLCA ; 
	
if ls_type_code <> "" then
	lb_view_loc = true
end if 
//

	//IF (dw_customer_info.GetItemString( 1, "smanlocaton") <> Message.is_smanlocation) &
		//AND (Message.is_smanlocation <> '630' AND Message.is_smanlocation <> '606' AND Message.is_smanlocation <> '006') & 
IF (dw_customer_info.GetItemString( 1, "smanlocaton") <> Message.is_smanlocation) AND (lb_view_loc = false) &
   AND ( NOT lu_project_functions.nf_isscheduler() )	&
	AND ( NOT lu_project_functions.nf_isschedulermgr() )	&
	AND ( NOT lu_project_functions.nf_issalesmgr() ) THEN 

	If adw_calling.dataobject = 'd_sales_detail_part1' Then &
			wf_setdetail_businessrules()

	tab_1.tp_detail.dw_detail.Reset()
	tab_1.tp_detail2.dw_detail2.Reset()
	tab_1.tp_detail3.dw_detail3.Reset()
	tab_1.tp_confirm.dw_confirmation.Reset()
	adw_calling.SetRedraw(True)
	MessageBox("Warning", "Your location does not match the orders location.~r~nYou may not look at its detail.")
	tab_1.tp_confirm.enabled = False
	is_detail	=	''
	Return
ELSE
	
	if not isnull(is_customer_info) then
		
		if adw_calling.dataobject <> 'd_sales_header' Then
			
			lu_project_functions.nf_set_s_error( istr_error_info)
			
			SetPointer(HourGlass!)
			istr_error_info.se_event_name = 'wf_protect_order'
			
			ls_input_string = Message.is_salesperson_code + '~t' + &
									Message.is_smanlocation + '~t' + &
									mid(is_customer_info,1,7) + '~t' + &
									mid(is_customer_Info,8,1) + '~t' + &
									SQLCA.UserID + '~r~n'
									
			li_rtn = iu_ws_orp2.nf_orpo31fr(ls_input_string,istr_Error_Info)

			if li_rtn = 0 then
				tab_1.tp_confirm.enabled = TRUE
			else
				MessageBox("Warning", "You do not have access to view the order.~r~nYou may not view detail information.")
				tab_1.tp_confirm.enabled = False
				is_detail	=	''
			end if	
		end if	
	else
		tab_1.tp_confirm.enabled = FALSE
	end if	
		
END If
end subroutine

public subroutine wf_accept_shortage (string args_order_id, character ac_autoresolve, string as_shortage_lines);// Call orpo35ar to accept weight shortages

istr_error_info.se_event_name = 'wf_accept_shortage'

//iu_orp003.nf_orpo35ar( istr_error_info, &
//						    args_order_id,&
//							 ac_AutoResolve, &
//							 as_shortage_lines) 
							 
iu_ws_orp2.nf_orpo35fr(args_order_id,ac_AutoResolve,as_shortage_lines,istr_error_info)

is_message = iw_frame.wf_Getmicrohelp()
ic_overide = 'F'


end subroutine

public subroutine wf_get_selected_plants (datawindow adw_datawindow);long				ll_count
string			ls_plant
integer			li_windowx, li_windowy, li_height, li_height_H
Application					la_Application

is_selected_plant_array = ""
for ll_count = 1 to il_selected_plant_max
	ls_plant = adw_datawindow.getitemstring(il_selected_row, "plant" + string(ll_count)) 
	if isnull(ls_plant)  then
		is_selected_plant_array += " " + "~r~n"
	else
		is_selected_plant_array += ls_plant + "~r~n"
	end if
next
// find opening x/y positions for "Multi select drop down"
la_Application = GetApplication()
Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose

li_windowx = iw_frame.width
li_windowy = iw_frame.height
if ib_Header then
	if (iw_frame.width - (this.x + integer(dw_customer_info.X) + integer(dw_customer_info.Object.plant1.X))) > 1179 then
		ii_dddw_x = integer(dw_customer_info.Object.plant1.X) + 24 + this.x		
	else
		ii_dddw_x = iw_frame.width - 1179 - 30
		if ii_dddw_x < 0 then ii_dddw_x = integer(dw_customer_info.Object.plant1.X) + 24 + this.x
	end if
	if (iw_frame.height - (this.y + integer(dw_customer_info.Object.plant1.y) +  &
			integer(dw_customer_info.Object.plant1.height) + integer(dw_customer_info.y)) > (676 + 450)) then
		ii_dddw_y = integer(dw_customer_info.Object.plant1.y) + integer(dw_customer_info.Object.plant1.height) + 208 + this.y + li_height_H
	else
		ii_dddw_y = integer(dw_customer_info.Object.plant1.y) + 208 + this.y - 676 + li_height_H
		if ii_dddw_y < 0 then ii_dddw_y = integer(dw_customer_info.Object.plant1.y) + integer(dw_customer_info.Object.plant1.height) + &
							208 + this.y + li_height_H
	end if
else
	if (iw_frame.width - (this.x + integer(tab_1.tp_detail3.dw_detail3.X) + integer(tab_1.tp_detail3.dw_detail3.Object.plant1.X) - il_horz_scroll_pos)) > 1179 then
		ii_dddw_x = integer(tab_1.tp_detail3.dw_detail3.Object.plant1.X) + 38 + this.x + tab_1.x
	else
		ii_dddw_x = iw_frame.width - 1179 - 38
		if ii_dddw_x < 0 then ii_dddw_x = integer(tab_1.tp_detail3.dw_detail3.Object.plant1.X) + 38 + this.x + tab_1.x
	end if
	if (iw_frame.height - (this.y + tab_1.y +integer(tab_1.tp_detail3.dw_detail3.y) + ((integer(tab_1.tp_detail3.dw_detail3.Object.plant1.y) + &
					integer(tab_1.tp_detail3.dw_detail3.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(tab_1.tp_detail3.dw_detail3.Object.DataWindow.FirstRowOnPage) - 1)) + 440 + li_height)) > (676 + 200)) then
		ii_dddw_y = this.y + tab_1.y + integer(tab_1.tp_detail3.dw_detail3.y) + ((integer(tab_1.tp_detail3.dw_detail3.Object.plant1.y) + &
					integer(tab_1.tp_detail3.dw_detail3.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(tab_1.tp_detail3.dw_detail3.Object.DataWindow.FirstRowOnPage) - 1))) + 440 + li_height
	else
		ii_dddw_y = (integer(dw_customer_info.Object.plant1.y) + 288 + this.y + tab_1.y + (((integer(tab_1.tp_detail3.dw_detail3.Object.plant1.y) + &
					integer(tab_1.tp_detail3.dw_detail3.Object.plant1.height) + 8)) * &
					(il_selected_row - (integer(tab_1.tp_detail3.dw_detail3.Object.DataWindow.FirstRowOnPage) - 1))))  - 676 + li_height
		if ii_dddw_y < 0 then ii_dddw_y = this.y + tab_1.y + integer(tab_1.tp_detail3.dw_detail3.y) + ((integer(tab_1.tp_detail3.dw_detail3.Object.plant1.y) + &
					integer(tab_1.tp_detail3.dw_detail3.Object.plant1.height) + 8) * &
					(il_selected_row - (integer(tab_1.tp_detail3.dw_detail3.Object.DataWindow.FirstRowOnPage) - 1))) + 440  + li_height
	end if
end if

////test
//messagebox("pointery", string(li_windowy))
end subroutine

public subroutine wf_set_selected_plants ();DataStore			lds_selected_plants
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt

lds_selected_plants = Create DataStore
lds_selected_plants.DataObject = 'd_plant_code'
ll_rowcount = lds_selected_plants.importstring(is_selected_plant_array)
if ib_modified then
	if ib_header then
	//	for ll_count = 1 to il_selected_plant_max
	//		if ll_count <= ll_rowcount then
	//			dw_customer_info.setitem(il_selected_row, "plant" + string(ll_count) , &
	//												lds_selected_plants.getitemstring(ll_count, "plant"))
	//		else
	//			dw_customer_info.setitem(il_selected_row, "plant" + string(ll_count) , " ")
	//		end if
	//	next
	else
		for ll_count = 1 to il_selected_plant_max
			if ll_count <= ll_rowcount then
				tab_1.tp_detail3.dw_detail3.setitem(il_selected_row, "plant" + string(ll_count), &
												lds_selected_plants.getitemstring(ll_count, "plant"))
			else
				tab_1.tp_detail3.dw_detail3.setitem(il_selected_row, "plant" + string(ll_count), "   ")
			end if
		next
		if (long(tab_1.tp_detail3.dw_detail3.getitemnumber(il_selected_row,"ordered_units")) > 0) or &
				 (long(tab_1.tp_detail3.dw_detail3.getitemnumber(il_selected_row,"scheduled_units")) > 0) then 
			tab_1.tp_detail3.dw_detail3.setitem(il_selected_row,"update_flag","U")
		end if
	end if
end if
ib_open = false
end subroutine

public subroutine wf_setdetail_businessrules ();string	ls_orderstatus


ls_orderstatus	=	dw_customer_info.GetItemString(1, 'order_status')
dw_customer_info.SetItem(1, 'business_rule', 'VVVVVVVVVVV')
IF ls_orderstatus = 'M' or ls_orderstatus = 'N' or ls_orderstatus = 'R' &
	or ls_orderstatus = 'C' THEN
	dw_cost_weight.SetItem(1, 'business_rules', 'VVVVVVVVVVVVVVVVVVVVVV')
ELSE
	dw_cost_weight.SetItem(1, 'business_rules', 'VVVVVVVVVVUUVVVVVVVVUV')
END IF


end subroutine

public subroutine wf_overrideweight (string arg_whocalled, boolean arg_call_complete, ref datawindow adw_detail);istr_error_info.se_event_name = 'wf_overrideweight'

IF is_whohadlastfocus = 'DETAIL' Then
	IF arg_Whocalled = "SAVE" THEN
		is_weight_override_String = ic_overide+"~t"+Space(8)+"~r~n"
		adw_detail.TriggerEvent('ue_update')
		is_weight_override_String	=	''
	ELSE
	wf_overridewt_oncomplete(arg_call_complete, adw_detail)
	END IF
ELSE
	// Header 
	IF arg_whocalled = "SAVE" Then
		is_weight_override_String = ic_overide+"~t"+Space(8)+"~r~n"
		adw_detail.TriggerEvent('ue_update')
		is_weight_override_String	=	''
	ELSE
		wf_overridewt_oncomplete(arg_call_complete, adw_detail)	
	END IF
END IF  
if ii_rpc_retvalue <> 1 then
	tab_1.tp_detail.dw_detail.triggerevent ("ue_retrieve")	
end if

end subroutine

public subroutine wf_protect_for_105 ();
tab_1.tp_detail.dw_detail.OBJECT.order_detail_number.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.order_detail_number.protect = 1
tab_1.tp_detail3.dw_detail3.OBJECT.order_detail_number.protect = 1

tab_1.tp_detail.dw_detail.OBJECT.ordered_units.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.ordered_units.protect = 1
tab_1.tp_detail3.dw_detail3.OBJECT.ordered_units.protect = 1

tab_1.tp_detail.dw_detail.OBJECT.product_code.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.product_code.protect = 1
tab_1.tp_detail3.dw_detail3.OBJECT.product_code.protect = 1

tab_1.tp_detail.dw_detail.OBJECT.ordered_age.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.ordered_age.protect = 1
tab_1.tp_detail3.dw_detail3.OBJECT.ordered_age.protect = 1

tab_1.tp_detail.dw_detail.OBJECT.scheduled_units.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.scheduled_units.protect = 1
tab_1.tp_detail3.dw_detail3.OBJECT.scheduled_units.protect = 1

tab_1.tp_detail.dw_detail.OBJECT.sales_price.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.sales_price_ovrd_ind.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.pricing_uom.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.price_date.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.price_ind.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.gpo_ind.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.quantity_weight_ind.protect = 1
tab_1.tp_detail.dw_detail.OBJECT.weight_ovrd_ind.protect = 1

tab_1.tp_detail2.dw_detail2.OBJECT.special_flag.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.cattlepak_product_code.protect = 1
tab_1.tp_detail2.dw_detail2.OBJECT.primal_pricing_ind.protect = 1

tab_1.tp_detail3.dw_detail3.OBJECT.cab_ind.protect = 1
end subroutine

public function string wf_get_prod_for_sub ();Long		ll_row

String	ls_openstring

If ib_rows_selected = True then
	ll_row = tab_1.tp_detail.dw_detail.GetSelectedRow(0)
	ls_OpenString = dw_customer_info.GetItemstring(1, "order_id")
	
//extract all the info from detail, when the line is clicked in detail
//then detail2, detail3 are also selected, same applies for 
//detail2 and detail3.
	ls_OpenString = ls_OpenString + "~t" + tab_1.tp_detail.dw_detail.GetItemString(ll_row,"order_detail_number") &
 									+"~t"+ String(tab_1.tp_detail.dw_detail.GetItemDecimal(ll_row,"ordered_units")) &
									+"~t"+ String(tab_1.tp_detail.dw_detail.GetItemDecimal(ll_row,"scheduled_units")) &
									+ "~t"+ tab_1.tp_detail.dw_detail.GetItemString(ll_row,"product_code") &
									+ "~t"+ tab_1.tp_detail.dw_detail.GetItemString(ll_row,"ordered_age") &
									+ "~t"+ tab_1.tp_detail.dw_detail.GetItemString(ll_row,"scheduled_age") & 
									+ "~t"+ tab_1.tp_detail.dw_detail.GetItemString(ll_row,"quantity_weight_ind") &
									+ "~t"+ String(tab_1.tp_detail.dw_detail.GetItemDecimal(ll_row,"net_weight"))
									
End If

Return ls_openstring
	
end function

public function boolean wf_retrieve ();Boolean ib_Order_Incomplete

String	ls_Temp, &
			ls_return_string

u_string_functions	lu_string_functions
u_conversion_functions	lu_conversion
ib_Order_Incomplete = dw_customer_info.is_incomplete()

ib_rows_selected = False

CHOOSE CASE is_whohadlastfocus 
	CASE	'DETAIL'
		tab_1.tp_detail.dw_detail.AcceptText()
		tab_1.tp_detail2.dw_detail2.AcceptText()
		tab_1.tp_detail3.dw_detail3.AcceptText()
	CASE	'HEADER'
		dw_cost_weight.AcceptText()
		dw_customer_info.AcceptText()
		tab_1.tp_header.dw_header.AcceptText()
	CASE 'INSTRUCTION'
END CHOOSE
Call Super::CloseQuery
ls_temp = lu_conversion.nf_string( Ib_Order_Incomplete) + '~t' + is_orderid
if (Mid(ls_temp, 1,1) = 'F') or (Mid(ls_temp, 1,1) = 'T') then
	ib_update_detail = FALSE
end if
//if ib_update_detail = FALSE then 
if ii_rpc_return = 0 then //or ib_update_detail = FALSE then
CHOOSE CASE is_whohadlastfocus 
	CASE 'DETAIL','HEADER','CURRENCY','CONFIRMATION' 
			OpenWithParm( w_sales_Detail_Inq, ls_temp)
	CASE 'INSTRUCTION'
		ls_temp	+=	'~t' + Trim(is_instruction_type)
		OpenWithParm( w_sales_Instruction_inq, ls_temp)
END CHOOSE
ls_return_string	=	Message.StringParm
IF ls_return_string = 'Abort' Then 
	IF dw_customer_info.RowCount() > 0 THEN
		RETURN FALSE
	ELSE
		RETURN TRUE
	END IF
Else
	IF is_whohadlastfocus = 'INSTRUCTION' THEN
		is_orderid	=	lu_string_functions.nf_gettoken(ls_return_string, '~t')
		is_instruction_type	=	ls_return_string
	ELSE
		is_orderid	=	Message.StringParm
	END IF
	dw_customer_info.uof_set_sales_id(Message.StringParm)
END IF
ib_Order_id_Changed_header = FALSE
ib_Order_id_Changed_Detail = FALSE
ib_Order_id_Changed_Instruction = FALSE
ib_Order_id_Changed_Currency = False
This.TriggerEvent("ue_Retrieve")
RETURN TRUE
end if
ib_update_detail = FALSE
ii_rpc_return = 0
end function

public function string wf_remove_nonupdated_fields (string as_inputstring);string 			ls_output, ls_LineNumber, ls_business_rules, ls_input_string, ls_original_string, &
					ls_trail_business_rules
datastore		lds_temp, lds_temp2
long				ll_rowcount, ll_count, ll_rowcount2, ll_count2,ll_count3
boolean			lb_found = false, lb_modified = false

lds_temp	=	Create DataStore
lds_temp.DataObject		=	'd_sales_detail_part1'
lds_temp.importstring(as_inputstring)
ls_input_string = as_inputstring

lds_temp2	=	Create DataStore
lds_temp2.DataObject		=	'd_sales_detail_part1'
lds_temp2.importstring(is_detail_orig)
ls_original_string = is_detail_orig

ll_rowcount = lds_temp.rowcount()
ll_rowcount2 = lds_temp2.rowcount()
if ll_rowcount > 0 then
	for ll_count = 1 to ll_rowcount
		ls_LineNumber = lds_temp.getitemstring(ll_count,"order_detail_number")
		ll_count2 = 1
		do while not lb_found or ll_count2 <= ll_rowcount2
			if lds_temp2.getitemstring(ll_count2,"order_detail_number") = ls_LineNumber then
				if not(lds_temp2.getitemstring(ll_count2,"inc_exl_ind") = lds_temp.getitemstring(ll_count,"inc_exl_ind")) then
					lb_modified = true
				else
					for ll_count3 = 1 to il_selected_plant_max
						if lds_temp2.getitemstring(ll_count2,string("plant" + string(ll_count3))) = &
							lds_temp.getitemstring(ll_count,string("plant" + string(ll_count3))) then
							lb_modified = lb_modified
						else
							lb_modified = true
						end if
					next
				end if 
				ls_business_rules = lds_temp.getitemstring(ll_count,"business_rules")
//				if len(ls_business_rules) < 34 then
//					ls_business_rules += space(34 - len(ls_business_rules))
//				end if
				ls_trail_business_rules = mid(ls_business_rules, 34)
				
				if not lb_modified then
					ls_business_rules = left(ls_business_rules, 32) + ' ' + ls_trail_business_rules
				else
					ls_business_rules = left(ls_business_rules, 32) + 'M' + ls_trail_business_rules
				end if
				lds_temp.setitem(ll_count,"business_rules", ls_business_rules)
				lb_modified = False
				lb_found = true
////				test
//				messagebox(ls_LineNumber,String(" Business rule" + mid(ls_business_rules,33,1)))
			end if
			ll_count2 ++			
		loop
	next
	ls_output = lds_temp.Object.DataWindow.Data
else 
	ls_output = ''
end if
////test
//messagebox("ALL", ls_output)

return ls_output
end function

public subroutine wf_rightclick ();Constant Date ld_date = Today() 

//If dwo.name = "sales_price" then
tab_1.tp_detail.dw_detail.AcceptText()
tab_1.tp_detail.dw_detail.ib_updateable = True
//end if
m_sales_detail_popup NewMenu

u_project_functions lu_project_functions

if lu_project_functions.nf_isscheduler() then return

NewMenu = CREATE m_sales_detail_popup
//IF ib_Rows_Selected THEN 
	NewMenu.m_file.m_MoveRows.Enabled = TRUE 
//Else
//	NewMenu.m_file.m_MoveRows.Enabled = False
//End If
NewMenu.M_file.m_orderconfirmation.enabled = TRUE
NewMenu.M_file.m_completeorder.Enabled = True
NewMenu.M_File.m_pasummary.enabled = TRUE
NewMenu.M_File.m_pasummarybydelivery.enabled = TRUE
NewMenu.M_file.m_automaticprice.enabled = TRUE
//dmk SR7533
If dw_cost_weight.GetItemDate(1,"ship_date") < ld_date Then
	NewMenu.M_file.m_productsub.enabled = FALSE
Else
	NewMenu.M_file.m_productsub.enabled = TRUE
End If
NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu
end subroutine

public subroutine wf_default_newrow ();long	ll_count, ll_rowcount
string  ls_business_rules, &
		 ls_business_rules_new, &
		 ls_type_of_sale, &
		 ls_previous_bus_rules, &
		 ls_temp_bus_rules

ll_rowcount = tab_1.tp_detail3.dw_detail3.rowcount()
if ll_rowcount > 0 then
	if tab_1.tp_detail.dw_detail.getitemnumber(ll_rowcount,"ordered_units") = 0 then
		tab_1.tp_detail3.dw_detail3.setitem(ll_rowcount, "inc_exl_ind", dw_customer_info.getitemstring(1, "inc_exl_ind"))
		for ll_count = 1 to il_selected_plant_max
			tab_1.tp_detail3.dw_detail3.setitem(ll_rowcount, "plant" + string(ll_count), dw_customer_info.getitemstring(1, "plant" + string(ll_count)))
		next
	end if
	ls_business_rules_new = 'UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUVUUUUU' 
	tab_1.tp_detail3.dw_detail3.SetItem( ll_rowcount, "business_rules", ls_business_rules_new)
	tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "price_ind", dw_cost_weight.GetItemString(1, "pricing_default_dtl"))
	
	ls_type_of_sale = dw_cost_weight.GetItemString(1, "type_of_sale")
	if (ls_type_of_sale = 'E') or (ls_type_of_sale = 'I') then
		tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "quantity_weight_ind", 'W')
	else
		if (ls_type_of_sale = 'T') then
			tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "quantity_weight_ind", 'Q')
		else
			if dw_cost_weight.GetItemString(1, "order_by_weight_ind") = 'Y' then
				tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "quantity_weight_ind", dw_cost_weight.GetItemString(1, "order_by_weight_def"))	
			else
				tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "quantity_weight_ind", 'Q')
				ls_business_rules_new = 'UUUUUUUUUUUVUUUUUUUUUUUUUUUUUUUUUUVU' 
				tab_1.tp_detail.dw_detail.SetItem( ll_rowcount, "business_rules", ls_business_rules_new)
			end if
		end if	
	end if
	
	if ll_rowcount > 1 then
		tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "line_splitting_ind", "N")
		ls_previous_bus_rules = tab_1.tp_detail.dw_detail.GetItemString(ll_rowcount - 1, "business_rules")
		if mid(ls_previous_bus_rules,40,1) = 'V' then
			ls_temp_bus_rules = mid(tab_1.tp_detail.dw_detail.GetItemString(ll_rowcount, "business_rules"),1,39) + 'V'
			tab_1.tp_detail.dw_detail.SetItem(ll_rowcount, "business_rules", ls_temp_bus_rules)
		end if
	end if
	
end if
end subroutine

public subroutine wf_load_oper_customers (string as_customer_list);DataWindowChild	ldwc_temp
Integer	li_ret

tab_1.tp_detail2.dw_detail2.GetChild('oper_customer_id', ldwc_temp)
ldwc_temp.Reset()
as_customer_list = '       ' + '~r~n' + as_customer_list
li_ret = ldwc_temp.ImportString(as_customer_list)
Return
end subroutine

public subroutine wf_check_microhelp_message (s_error astr_error_info);String		ls_message, ls_type_code, ls_type_desc

Integer	li_type_code

ls_message = mid(astr_error_info.se_message,1,40)

SQLCA.nf_connect()

SELECT tutltypes.type_code
	 INTO :ls_type_code
	 FROM tutltypes
	 WHERE tutltypes.record_type = 'MESSGBOX'
	 AND type_desc = :ls_message
	USING SQLCA ; 

If SQLCA.sqlcode = 0 and (ls_type_code > '        ') Then
	li_type_code = Integer(ls_type_code)
	li_type_code ++
	ls_type_code = string(li_type_code, "00000000")
	
	SELECT tutltypes.type_desc
	 INTO :ls_type_desc
	 FROM tutltypes
	 WHERE tutltypes.record_type = 'MESSGBOX'
	 AND type_code = :ls_type_code
	USING SQLCA ; 
	
	If SQLCA.sqlcode = 0 and (ls_type_desc > '        ') Then
		MessageBox(ls_type_desc, astr_error_info.se_message)
	Else	
		MessageBox("", astr_error_info.se_message)
	End if
End If
end subroutine

public function string wf_get_qty_wt_ind (string as_line_number);Integer	li_find
String		ls_find_string

ls_find_string = "order_detail_number = '" + as_line_number + "'"

li_find = tab_1.tp_detail.dw_detail.Find(ls_find_string, 1, tab_1.tp_detail.dw_detail.RowCount())

If li_find > 0 Then
	Return tab_1.tp_detail.dw_detail.GetItemString(li_find, "quantity_weight_ind")
Else
	Return 'Q'
End if	
	
end function

public function string wf_get_productdivision ();String		ls_product, &
			ls_product_division


ls_product = tab_1.tp_detail3.dw_detail3.GetItemString(il_selected_row, "product_code")

SELECT PRODUCT_DIVISION
INTO :ls_product_division
FROM SKU_PRODUCTS
WHERE SKU_PRODUCT_CODE = :ls_product
USING SQLCA;

if SQLCA.sqlcode = 0 Then
	Return ls_product_division
Else
	Return ""
End If
end function

event ue_postopen;call super::ue_postopen;u_string_functions	lu_string_functions
u_project_functions 	lu_project_functions


This.SetRedraw( False)
ib_Order_id_Changed_Detail = FALSE
ib_Order_id_Changed_Header = FALSE
ib_Order_id_Changed_Instruction = FALSE
ib_Order_id_Changed_Currency = False
iw_Frame.im_Menu.mf_Enable("m_CompleteOrder")
iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = False

iu_orp001 = CREATE u_orp001
iu_orp003 = CREATE u_orp003
iu_ws_orp1 = CREATE u_ws_orp1
iu_ws_orp2 = CREATE u_ws_orp2
iu_ws_orp4 = CREATE u_ws_orp4

iw_this	=	This
istr_error_info.se_window_name = 'SODetail'
istr_error_info.se_User_ID = SQLCA.UserID
istr_error_info.se_app_name = Message.nf_Get_App_ID()
Tab_1.SelectedTab	=	1
IF lu_string_functions.nf_IsEmpty( is_to_order_id) Then
	IF Len(Trim(is_orderid)) < 1  Then
		Message.StringParm = "No Check"	
		IF NOT wf_Retrieve() Then
			This.SetRedraw( TRUE)
			CLOSE(THIS)
			RETURN
		END IF
	ELSE
		if ib_open_on_instruction_tab then
			tab_1.SelectedTab = 5
			dw_customer_info.TriggerEvent('ue_retrieve')
			tab_1.tp_instruction.dw_permanent_instructions.TriggerEvent('ue_retrieve')
		else
			dw_customer_info.TriggerEvent('ue_retrieve')		
			tab_1.tp_detail.dw_detail.TriggerEvent('ue_retrieve')
			wf_setdetail_businessrules()
		end if
	END IF
ELSE
	PostEvent( "ue_move_Rows")
END IF

if lu_project_functions.nf_isscheduler() then
	iw_frame.im_netwise_menu.M_file.M_Save.Enabled = false
//	wf_protect_for_105()
end if

This.SetRedraw( TRUE)
ib_faxing_available = TRUE
end event

event open;call super::open;Long		ll_count

String	ls_option


ll_count	= iw_frame.iu_project_functions.ids_customers.RowCount()


// if opening on instruction tab, order is followed by tab and 'I'

if len(Message.StringParm) > 5 then
	ls_option = mid(Message.StringParm,7,1)
	if ls_option = 'I' then
		ib_open_on_instruction_tab = true
	else
		ib_open_on_instruction_tab = false
	end if
	is_OrderID = mid(Message.StringParm,1,5)
else
	ib_open_on_instruction_tab = false
	is_OrderID = Message.StringParm
end if

is_whohadlastfocus =  'DETAIL'  

ib_faxing_available = TRUE
is_typecode = Message.is_smanlocation
end event

event closequery;Char		lc_status

String 	ls_sales_orderid

u_string_functions	lu_string_functions

lc_status = " "
dw_cost_weight.AcceptText()
tab_1.tp_detail.dw_detail.AcceptText()
tab_1.tp_detail2.dw_detail2.AcceptText()
tab_1.tp_detail3.dw_detail3.AcceptText()
tab_1.tp_header.dw_header.AcceptText()
CALL SUPER::CLOSEQUERY
lc_status = dw_customer_info.GetItemString( 1, "order_status")
ls_sales_orderid = dw_customer_info.GetItemString( 1, "order_id")
IF NOT ( KEYDOWN( KeyControl!) AND KEYDOWN( KeyAlt!) &
	AND KEYDOWN( KeyShift!)) Then
//	AND (dw_customer_info.GetItemString(1 , "smanlocaton") = message.is_smanlocation) Then
		IF lu_string_functions.nf_IsEmpty( lc_Status) &
		And Not lu_string_functions.nf_IsEmpty(ls_sales_orderid) Then
//			AND Not lu_string_functions.nf_IsEmpty( dw_customer_info.GetItemString(1,"smancode")) Then
//			ls_sales_orderid = dw_customer_info.GetItemString( 1, "order_id")
			MessageBox("Warning", "Order " + ls_sales_orderid + " Status is incomplete.~r~nPlease complete the order before continuing" ) 
			message.returnValue = 1
			Return
		END IF
END IF

// removed this code because of complaints running slow
//ls_sales_orderid = dw_customer_info.GetItemString( 1, "order_id")
//wf_notify_complete( 	ls_sales_orderid)
end event

event deactivate;iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE
iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = FALSE
iw_frame.im_menu.mf_DisAble("m_completeorder")
iw_Frame.Im_Menu.M_options.m_CancelOrder.Enabled = FALSE

iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_generatesales.Enabled = True
// added for defect 1096
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_orderconfirmation2.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_trimloadalert.Enabled = False
// end addition

IF IsValid(iw_tooltip) Then
	close(iw_tooltip)
END IF

end event

event close;call super::close;If IsValid( iu_orp001) Then Destroy( iu_orp001)
If IsValid( iu_Orp003) Then Destroy( iu_orp003)
If IsValid( iu_Orp002) Then Destroy( iu_orp002)
If IsValid( iu_orp204) Then Destroy( iu_orp204)
if IsValid(iw_ToolTip) Then Close(iw_ToolTip)
if IsValid(iu_ws_orp1) Then Destroy(iu_ws_orp1)
if IsValid(iu_ws_orp2) Then Destroy(iu_ws_orp2)
if IsValid(iu_ws_orp3) Then Destroy(iu_ws_orp3)
if IsValid(iu_ws_orp4) Then Destroy(iu_ws_orp4)




end event

event activate;call super::activate;u_project_functions lu_project_functions

iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = TRUE 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = True
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = True
// added for defect 1096
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_orderconfirmation2.Enabled = True
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_trimloadalert.Enabled = True
// end addition

if lu_project_functions.nf_isscheduler() then
	iw_Frame.Im_Menu.M_options.m_CancelOrder.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_applicationoptions.Enabled = TRUE
	iw_Frame.Im_Menu.M_options.m_viewmessages.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_messagealias.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_browsereservations.Enabled = FALSE
	iw_frame.im_menu.mf_Disable('m_generatesales')
	if is_WhoHadLastFocus = "HEADER" then
		iw_frame.im_netwise_menu.M_file.M_Save.Enabled = true
	end if
else
	iw_Frame.Im_Menu.M_options.m_CancelOrder.Enabled = TRUE
	iw_frame.im_menu.mf_Enable("m_completeorder")
	iw_frame.im_menu.mf_enable("m_print")
end if






end event

on w_sales_order_detail.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_valid_product_info=create dw_valid_product_info
this.dw_customer_info=create dw_customer_info
this.tab_1=create tab_1
this.dw_cost_weight=create dw_cost_weight
this.dw_programs=create dw_programs
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_valid_product_info
this.Control[iCurrent+3]=this.dw_customer_info
this.Control[iCurrent+4]=this.tab_1
this.Control[iCurrent+5]=this.dw_cost_weight
this.Control[iCurrent+6]=this.dw_programs
end on

on w_sales_order_detail.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_valid_product_info)
destroy(this.dw_customer_info)
destroy(this.tab_1)
destroy(this.dw_cost_weight)
destroy(this.dw_programs)
end on

event ue_fileprint;call super::ue_fileprint;tab_1.tp_confirm.dw_confirmation.Print()
IF Not ib_print_ok Then Return

end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event ue_get_data;STRING	ls_indicator, ls_rpc_message

//long		ll_window_x,ll_window_y, &
			
Choose Case as_value
	Case "Order_ID"
		Message.StringParm = is_orderid
	CAse "detail"
		Message.StringParm = is_detail
	Case "customer_id"
		Message.StringParm = dw_customer_info.GetITemString(1, "customer_id") 
	Case "delivery_date"
		Message.StringParm =  String(dw_cost_weight.GetItemDate(1,"delv_date"),"MM/DD/YYYY")
	Case "ship_date"
		Message.StringParm =  String(dw_cost_weight.GetItemDate(1,"ship_date"),"MM/DD/YYYY")
	Case "division_code"
		Message.StringParm =  dw_customer_info.GetITemString(1, "division_code") 
	Case "salesperson_code"
		Message.StringParm =  dw_customer_info.GetITemString(1, "smancode") 
	Case "ship_plant"
		Message.StringParm =  dw_cost_weight.GetITemString(1, "ship_plant")
	Case "detail_data"
		Message.StringParm = is_detail_data
	CASE "indicator"
		Message.StringParm = is_shortage_ind
	Case "shortage_ind"
		Message.StringParm = dw_customer_info.GetITemString(1, "short_que_ind")
	Case "retrieve"	
		ls_rpc_message = iw_frame.wf_getmicrohelp()											
		This.TriggerEvent('ue_retrieve')
		iw_Frame.SetMicroHelp(ls_rpc_message)
	case	"ToolTip"
		Message.StringParm = is_description
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case  "send_to_sched_ind"
		Message.StringParm = ic_send_to_sched_ind	
	case  "all"
		if ib_all then
			Message.StringParm = "Y"
		else
			Message.StringParm = "N"
		end if
	case "plant"
		Message.StringParm = ''
	case "selected"
		Message.StringParm = is_selected_plant_array
	case "header"
		if ib_header then
			Message.StringParm = "true"
		else
			Message.StringParm = 'false'
		end if
	case "dddwxpos"
		message.StringParm = string(ii_dddw_x)
	case "dddwypos"
		message.StringParm = string(ii_dddw_y)
	case "updateheader"
		message.StringParm = "false"
	case "updatedetail"
		if ib_detail_updateable then
			message.StringParm = "true"
		else
			message.StringParm = "false"
		end if
	case "pricing_default_hdr"
		Message.StringParm = is_pricing_default_hdr	
	case "type_of_sale_hdr"
		Message.StringParm = tab_1.tp_header.dw_header.GetItemString(1,"type_of_sale")		
	case "divisional_po"
		Message.StringParm = dw_cost_weight.GetItemString(1,"po_div")	
	case "corporate_po"
		Message.StringParm = dw_cost_weight.GetItemString(1,"corp_id")		
	case "product_division"
		Message.StringParm = is_product_division				
	Case else
		Message.StringParm = ''		
End Choose
end event

event ue_pa_summary();call super::ue_pa_summary;String	ls_OpenString 

Window	lw_Temp

ls_OpenString = String(dw_cost_weight.GetItemDate(1,"Ship_date"),"MM/dd/yyyy") &
									+"~t"+ dw_cost_weight.GetItemString(1,"ship_plant") + "~t~tA"

OpenSheetWithParm(lw_Temp, ls_OpenString,"w_pa-summary", &
						iw_frame,0,iw_frame.im_menu.iao_arrangeOpen)
end event

event ue_set_data;choose case as_data_item
	case "selected"
		is_selected_plant_array = as_value
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
end choose
end event

event resize;call super::resize;constant integer li_tab_x		= 30
constant integer li_tab_y		= 500
  
constant integer li_dw_x		= 80
constant integer li_dw_y		= 770

  
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tp_detail.dw_detail.width = newwidth - li_dw_x
tab_1.tp_detail.dw_detail.height = newheight - li_dw_y

tab_1.tp_detail2.dw_detail2.width = newwidth - li_dw_x
tab_1.tp_detail2.dw_detail2.height = newheight - li_dw_y

tab_1.tp_detail3.dw_detail3.width = newwidth - li_dw_x
tab_1.tp_detail3.dw_detail3.height = newheight - li_dw_y

dw_programs.width = 549
dw_programs.height = 136




end event

type st_1 from statictext within w_sales_order_detail
integer x = 2935
integer y = 32
integer width = 274
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Program(s):"
boolean focusrectangle = false
end type

type dw_valid_product_info from u_base_dw_ext within w_sales_order_detail
boolean visible = false
integer x = 1394
integer y = 748
integer width = 41
integer height = 48
integer taborder = 40
boolean dragauto = true
string dataobject = "d_valid_product_info"
boolean resizable = true
boolean livescroll = true
end type

event constructor;call super::constructor;This.ib_updateable = False

end event

type dw_customer_info from uo_d_customer_info within w_sales_order_detail
integer height = 248
integer taborder = 10
boolean ib_scrollable = false
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp
u_project_functions	lu_project_functions

ib_updateable	=	False
This.InsertRow(0)
this.Object.short_que_name.Visible = True
this.Object.short_que_ind.Visible = True

This.GetChild('credit_status', ldwc_temp)
lu_project_functions.nf_gettutltype(ldwc_temp, 'CREDSTAT')
end event

event itemchanged;call super::itemchanged;ib_Updateable =TRUE
end event

event ue_retrieve;////Populate this datawindow
uof_Set_sales_id(is_orderid)

IF uof_retrieve() THEN
	is_cost_weight	=	uof_get_weight_string()
	dw_cost_weight.TriggerEvent('ue_retrieve')
END IF
is_cust_business_rule	=	THIS.GetItemString(1, 'business_rule')
is_header_business_rule	=	dw_cost_weight.GetItemString(1, 'business_rules')
is_customer_info = THIS.GetItemString( 1,"customer_id")+'S'
RETURN 0
end event

event destructor;call super::destructor;iw_frame.iu_project_functions.ids_customers.ShareDataOff()
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event getfocus;call super::getfocus;dw_customer_info.SetColumn("order_id")
end event

event clicked;call super::clicked;is_columnname = dwo.name

choose case dwo.name
	case 'plant1'
		ib_header = true
		il_selected_row = row
		ib_open = true
		wf_get_selected_plants(this)
		is_product_division = this.GetItemString(1,"division_code")
		OpenWithParm( w_locations_popup, parent)
end choose
end event

event itemfocuschanged;call super::itemfocuschanged;is_columnname = dwo.name
end event

event losefocus;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = true
			il_selected_row = this.getrow()
			is_product_division = dw_customer_info.GetItemString(1,"division_code")
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, parent)
	end choose
end if
end event

type tab_1 from tab within w_sales_order_detail
event create ( )
event destroy ( )
integer y = 640
integer width = 3904
integer height = 1076
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
boolean boldselectedtext = true
integer selectedtab = 1
tp_detail tp_detail
tp_detail2 tp_detail2
tp_detail3 tp_detail3
tp_header tp_header
tp_instruction tp_instruction
tp_currency tp_currency
tp_confirm tp_confirm
end type

on tab_1.create
this.tp_detail=create tp_detail
this.tp_detail2=create tp_detail2
this.tp_detail3=create tp_detail3
this.tp_header=create tp_header
this.tp_instruction=create tp_instruction
this.tp_currency=create tp_currency
this.tp_confirm=create tp_confirm
this.Control[]={this.tp_detail,&
this.tp_detail2,&
this.tp_detail3,&
this.tp_header,&
this.tp_instruction,&
this.tp_currency,&
this.tp_confirm}
end on

on tab_1.destroy
destroy(this.tp_detail)
destroy(this.tp_detail2)
destroy(this.tp_detail3)
destroy(this.tp_header)
destroy(this.tp_instruction)
destroy(this.tp_currency)
destroy(this.tp_confirm)
end on

event selectionchanging;u_project_functions	lu_project_functions
//This is to prevent code in this event from executing when the tab is created
IF ib_inquireonopen THEN
	ib_inquireonopen	=	False
	RETURN 1
END IF

CHOOSE CASE oldindex
	CASE	1, 2, 3
		if lu_project_functions.nf_isscheduler() then return
		if (newindex = 1) or (newindex = 2) or (newindex = 3) then return
		RETURN wf_checkfor_changes(tp_detail.dw_detail, is_detail)
	CASE	4
		RETURN wf_checkfor_changes(dw_cost_weight, is_cost_Weight)
	CASE	5
	//RETURN wf_checkfor_changes(tp_instruction.mle_1, is_mle_instruction) 
END CHOOSE

end event

event selectionchanged;u_string_functions	lu_string_functions
u_project_functions 	lu_project_functions

Integer					ll_vert_scroll_amt

String 					ls_type_code, ls_code

Boolean  					lb_view_loc 
							


//This is to prevent code in this event from executing when the tab is created
IF ib_inquireonopen THEN
	ib_inquireonopen	=	False
	RETURN
END IF

CHOOSE CASE oldindex
	CASE 1
		ll_vert_scroll_amt = Long(this.tp_detail.dw_detail.Object.DataWindow.VerticalScrollPosition)
	CASE 2
		ll_vert_scroll_amt = Long(this.tp_detail2.dw_detail2.Object.DataWindow.VerticalScrollPosition)
	CASE 3
		ll_vert_scroll_amt = Long(this.tp_detail3.dw_detail3.Object.DataWindow.VerticalScrollPosition)
	CASE ELSE
		ll_vert_scroll_amt = 0
END CHOOSE

//iw_this.SetRedraw(false)
if IsValid(iw_ToolTip) Then Close(iw_ToolTip)
iw_frame.setMicrohelp("Ready")
CHOOSE CASE	newindex
	CASE	1, 2, 3
		if lu_project_functions.nf_isscheduler() then
			iw_frame.im_netwise_menu.M_file.M_Save.Enabled = false
		end if
		if ib_updated_header then
			tab_1.tp_detail.dw_detail.triggerevent ("ue_retrieve")
			ib_updated_header = false
		end if
		choose case newindex
			case 1
				this.tp_detail.dw_detail.setfocus()
				this.tp_detail.dw_detail.Object.DataWindow.VerticalScrollPosition = ll_vert_scroll_amt
			case 2
				this.tp_detail2.dw_detail2.setfocus()
				this.tp_detail2.dw_detail2.Object.DataWindow.VerticalScrollPosition = ll_vert_scroll_amt
			case 3
				this.tp_detail3.dw_detail3.setfocus()
				this.tp_detail3.dw_detail3.Object.DataWindow.VerticalScrollPosition = ll_vert_scroll_amt
			end choose
		IF ib_settabtofocusonopen THEN
			ib_settabtofocusonopen	=	False
			if lu_project_functions.nf_isscheduler() then
				iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = False
			else
				iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = True
			end if
			RETURN
		END IF
		if lu_project_functions.nf_isscheduler() then
				iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = False
				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = true
//				wf_protect_for_105()
			else
				iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = True
		end if
		if lu_project_functions.nf_isschedulermgr() then
			iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = true
		end if
		is_WhoHadLastFocus	=	'DETAIL'
		lb_view_loc = false 
		ls_code = dw_customer_info.GetItemString( 1, "smanlocaton") + Message.is_smanlocation

		SQLCA.nf_connect()

		SELECT tutltypes.type_code
			 INTO :ls_type_code
			 FROM tutltypes
			 WHERE tutltypes.record_type = 'VIEWLOC'
			 AND type_code = :ls_code
			USING SQLCA ; 
//		if SQLCA.sqlcode < 0 then
//			SQLCA.triggerEvent('ue_reconnect')
//		end if
		
		if ls_type_code <> "" then
			lb_view_loc = true
		end if 
		//IF (dw_customer_info.GetItemString( 1, "smanlocaton") <> Message.is_smanlocation) &
		//AND (Message.is_smanlocation <> '630' AND Message.is_smanlocation <> '606' AND Message.is_smanlocation <> '006') & 
		IF (dw_customer_info.GetItemString( 1, "smanlocaton") <> Message.is_smanlocation) AND (lb_view_loc = false) & 
			AND ( NOT lu_project_functions.nf_isscheduler() )	&
			AND ( NOT lu_project_functions.nf_isschedulermgr() )	&
			AND ( NOT lu_project_functions.nf_issalesmgr() ) THEN 		  
			wf_setdetail_businessrules()
			MessageBox("Warning", "Your location does not match the orders location.~r~nYou may not look at its detail")
		Else
			IF lu_string_functions.nf_isempty(is_detail) OR ib_Order_id_Changed_Detail = FALSE Then
				ib_Order_id_Changed_Detail = TRUE
//				tp_detail.dw_detail.PostEvent( "ue_retrieve")
			ELSE
				wf_setdetail_businessrules()
			END IF	
		END IF
		PArent.Title = 'Sales Order detail - '+dw_customer_info.uof_GetSalesID()
	CASE	4
		this.tp_header.dw_header.setfocus()
		
		if lu_project_functions.nf_isscheduler() then
			iw_frame.im_netwise_menu.M_file.M_Save.Enabled = true
			tp_header.dw_header.enabled = False
			dw_cost_weight.enabled = false
			dw_customer_info.enabled = false
		end if
		
		
		iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE
		iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = FALSE
		is_whohadlastfocus  = 'HEADER'
		IF lu_string_functions.nf_isempty(is_header) OR ib_Order_id_Changed_Header = FALSE Then
			ib_Order_id_Changed_Header = TRUE
			tp_header.dw_header.PostEvent( "ue_retrieve")
		ELSE
			dw_customer_info.SetItem(1, 'business_rule', is_cust_business_rule)
			dw_cost_weight.SetItem(1, 'business_rules', is_header_business_rule)
		END IF
		PArent.Title = 'Sales Order Header - '+dw_customer_info.uof_GetSalesID()
	CASE	5
		if lu_project_functions.nf_isscheduler() then
			iw_frame.im_netwise_menu.M_file.M_Save.Enabled = false
			tp_instruction.mle_1.ENABLED = FALSE
			
		end if
		this.tp_instruction.dw_permanent_instructions.setfocus()
		is_whohadlastfocus = 'INSTRUCTION'
		iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = FALSE
		iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE
		IF tp_instruction.dw_permanent_instructions.RowCount() < 1 OR &
			ib_Order_id_Changed_Instruction = FALSE Then
 			ib_Order_id_Changed_Instruction = TRUE
			tp_instruction.dw_permanent_instructions.TriggerEvent( "ue_retrieve")
		END IF
			wf_setdetail_businessrules()		
			PArent.Title = 'Sales Order Instruction - '+dw_customer_info.uof_GetSalesID()
	CASE	6
		if lu_project_functions.nf_isscheduler() then
			iw_frame.im_netwise_menu.M_file.M_Save.Enabled = false
			tp_currency.dw_currency.enabled = false
		end if
		this.tp_instruction.dw_permanent_instructions.setfocus()
		is_whohadlastfocus = 'CURRENCY'
		iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = FALSE
		iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE

		IF tp_currency.dw_currency.RowCount() < 1 OR ib_Order_id_Changed_Currency = FALSE Then
		 	ib_Order_id_Changed_Currency = TRUE
			tp_currency.dw_currency.PostEvent( "ue_retrieve")
		END IF
			wf_setdetail_businessrules()
			tp_currency.dw_currency.SetFocus()
		PArent.Title = 'Sales Order Currency - '+dw_customer_info.uof_GetSalesID()
END CHOOSE

//iw_this.SetRedraw(true)
end event

event rightclicked;wf_rightclick()
end event

type tp_detail from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
long backcolor = 12632256
string text = "Detail"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_detail dw_detail
end type

on tp_detail.create
this.dw_detail=create dw_detail
this.Control[]={this.dw_detail}
end on

on tp_detail.destroy
destroy(this.dw_detail)
end on

event rbuttondown;wf_rightclick()
end event

type dw_detail from u_base_dw_ext within tp_detail
event ue_resbrowse ( )
event ue_get_data ( string as_data )
integer y = 4
integer width = 3867
integer height = 936
integer taborder = 11
string dataobject = "d_sales_detail_part1"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
boolean ib_scrollable = false
end type

event ue_resbrowse;call super::ue_resbrowse;STRING 	ls_ParmString, &
			ls_temp, &
			ls_ordered_units

LONG		ll_Rows, &
			ll_Row

Integer	li_counter

This.AcceptText()
tab_1.tp_detail2.dw_detail2.AcceptText()
tab_1.tp_detail3.dw_detail3.AcceptText()

ls_ParmString	= dw_cost_weight.GetItemString(1, "c_AddProductInfo") + &
 					dw_customer_info.GetItemString(1, "c_AddProductInfo")
ll_Rows = This.Rowcount()

li_counter	=	0	
FOR ll_Row = 1 TO ll_Rows
	CHOOSE CASE This.GetItemStatus( ll_Row, 0 ,Primary!) 
		CASE DataModified!, NewModified!
			IF IsNull(This.GetItemNumber(ll_Row, "ordered_units")) THEN CONTINUE
			IF IsNull(This.GetItemNumber(ll_Row, "scheduled_units")) THEN 
				ls_ordered_units	=	String(This.GetItemDecimal(ll_Row, "ordered_units"))
			ELSE
				IF This.GetItemDecimal(ll_Row, "ordered_units") > This.GetItemDecimal(ll_Row, "scheduled_units") THEN
					ls_ordered_units	=	String(This.GetItemDecimal(ll_Row, "ordered_units") - This.GetItemDecimal(ll_Row, "scheduled_units"))
				ELSE
					CONTINUE
				END IF
			END IF
			li_counter++
			ls_ParmString += ls_ordered_units + &
							'~t' + This.GetItemString(ll_Row, "product_code") + &
							'~t' + This.GetItemString(ll_Row, "ordered_age") + &
							'~t' 
			IF IsNull(This.GetItemString(ll_Row, "ordered_uom")) THEN
				ls_ParmString += '' + '~r~n'
			ELSE
				ls_ParmString += This.GetItemString(ll_Row, "ordered_uom") + '~r~n'
			END IF
	END CHOOSE
NEXT

IF li_counter = 0 THEN
	iw_frame.SetMicrohelp('No Changes made, Cannot Browse Reservations')
	RETURN 
END IF

OpenWithParm(w_res_browse-product, ls_ParmString)
This.SetRedraw(True)
is_ResReductions = Message.StringParm
ls_temp = is_resreductions
IF TRIM(is_ResReductions) = "Cancel" THEN 
	RETURN
END IF
This.TriggerEvent("ue_update")
is_ResReductions = ""

end event

event clicked;call super::clicked;IF Row > 0 Then
	Choose Case dwo.name
		Case 'order_detail_number'
//			IF (Integer(This.GetItemString( row, "order_Detail_Number")) < 1) or (This.GetItemNumber(row, "scheduled_units") = 0)  then Return
			IF (Integer(This.GetItemString( row, "order_Detail_Number")) < 1)  or &
				This.GetItemString( row, "item_status") = 'R' or &
				This.GetItemString( row, "item_status") = 'T' or &
				This.GetItemString( row, "item_status") = 'X' or &
				dw_customer_info.GetItemString( 1, "order_status") = 'M' or &
				dw_customer_info.GetItemString( 1, "order_status") = 'N' Then Return
			
			This.SelectRow( row, Not(This.IsSelected( row)))
			tab_1.tp_detail2.dw_detail2.SelectRow( row, (This.IsSelected( row)))
			tab_1.tp_detail3.dw_detail3.SelectRow( row, (This.IsSelected( row)))
			IF This.GetSelectedRow( 0 ) > 0 Then 
//				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = TRUE
				ib_rows_selected = True
			ELSE
//				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE
				ib_rows_selected = False
			END IF
		Case Else
			Return
	END Choose
END IF

If Integer(This.Describe("ordered_units.ID")) > 0 Then This.SetColumn( "ordered_units")
end event

event doubleclicked;call super::doubleclicked;String	ls_Currency, &
			ls_value

Choose Case dwo.Name
	Case "sales_price"
		ls_Currency = Trim(dw_customer_info.GetItemString(1, 'Currency_code'))
		If Len(ls_Currency) < 3 Or IsNull(ls_Currency) Then
			SetMicroHelp("Please select a currency before opening Monthly Exchange Rate window.")
			Return
		Elseif ls_Currency = 'USD' Then
			SetMicroHelp("Monthly Exchange Rate window is only valid for foreign currencies")
			Return
		Else
			IF IsNull(This.GetItemNumber(row, 'sales_price')) THEN
				ls_value	=	'0.00'
			ELSE
				ls_value		=	String(This.GetItemDecimal(row, 'sales_price'))
			END IF
			OpenwithParm(w_exchange_rate_response, ls_Currency + "~t" + ls_value)
			If IsNumber(Message.StringParm) Then &
				This.SetItem(row, 'sales_price', Dec(Message.StringParm))
		End if
End Choose 
end event

event itemchanged;call super::itemchanged;u_project_functions lu_project_functions

string 	ls_ship_customer, &
			ls_plant_code, &
			ls_division, &
			ls_test_indicator, ls_business_rules, ls_temp
			
ls_ship_customer	= dw_customer_info.GetItemString(1, "customer_id")
ls_plant_code		= dw_cost_weight.GetItemString(1, "ship_plant")

ib_updateable	=	True
If dwo.name = "product_code" Then
	//This.SetItem( row, "pricing_uom", "01")
	This.SetItem( row, "quantity_weight_ind", "Q")
	This.SetItem( row, "ordered_age", lu_project_functions.nf_get_age_code(Data, ls_ship_customer, ls_plant_code))	
	This.SetItem( row, "product_code", data)
	This.SetItem( row, "update_flag", "A")
	IF KeyDown( KeyTab!) Then
		SetFocus()
		SetColumn( "ordered_age")
		SetRow( Row)
		SelectText(1,1)
	ELSE
		IF KeyDown( KeyEnter!) Then
			SetColumn( "ordered_units")
			SetRow(row)
			SetFocus()
		END IF
	END IF
END IF

If dwo.name = "gpo_ind" Then
	IF mid(data,1,1) = 'D' Then
		ls_division = wf_getdivision()
		If (ls_division = '05') or (ls_division = '11') or (ls_division = '31') or & 
			(ls_division = '32') or (ls_division = '57') Then
			This.SetItem( row, "price_ind", "N")
		End If
	End If
End If

If dwo.name = "sales_price" then
	tab_1.tp_detail.dw_detail.AcceptText()
	tab_1.tp_detail.dw_detail.ib_updateable = True
	ls_business_rules = tab_1.tp_detail.dw_detail.GetItemString(row, "business_rules")
	ls_business_rules = Replace(ls_business_rules, 9, 1, "M")
	tab_1.tp_detail.dw_detail.SetItem(row, "business_rules", ls_business_rules)
	
	
end if

if This.ib_NewRowOnChange then
	wf_default_newrow()
end if
ib_updateable = True
dw_cost_weight.ib_updateable = True
dw_customer_info.ib_updateable = True

end event

event itemerror;call super::itemerror;u_string_functions	lu_string_functions
String					ls_business_rules

CHOOSE CASE dwo.name
	CASE	"ordered_units"
		if lu_string_functions.nf_isempty(data) then 
			This.SetItem( row, "ordered_units", 0)
			This.SetItem( row, "update_flag", "U")
			ls_business_rules = This.GetItemString( row, "business_rules")
			This.SetItem( row, "business_rules", mid(ls_business_rules,1,2) + 'M' + mid(ls_business_rules,4))
		end if
	CASE "sales_price"
		iw_frame.SetMicroHelp("Sales Price must be numeric.")
END CHOOSE
RETURN (1)
end event

event ue_dwndropdown;call super::ue_dwndropdown;String				ls_ColumnName

DataWindowChild	ldw_DDDWChild

ls_ColumnName	=	Upper(This.GetColumnName())

CHOOSE CASE ls_ColumnName
	CASE	'PRICING_UOM'
		// Populate the pricing uom DDDW
		This.GetChild('pricing_uom', ldw_DDDWChild)		
	CASE	'QUANTITY_WEIGHT_IND'
		// Populate the qty weight DDDW
		This.GetChild("quantity_weight_ind", ldw_DDDWChild)
	CASE	'GPO_IND'
		// Populate the gpo ind  DDDW
		This.GetChild("gpo_ind", ldw_DDDWChild)
	Case 'PRICE_IND'
		// Populate the price ind  DDDW
		This.GetChild("price_ind", ldw_DDDWChild)
	CASE ELSE
		RETURN
END CHOOSE
wf_gettypedesc(ldw_DDDWChild)
end event

event ue_retrieve;
integer	li_rtn, &
			li_LoopCount, &
			li_RowCount, &
			li_Counter
     
string	ls_microHelp,&
			ls_pa_resolve_out, &
			ls_production_dates, &
			ls_dup_products_ind, &
			ls_string, ls_business_rule, ls_status, ls_oper_customers, ls_find_string
			
Long		ll_find_count			
			
u_string_functions	lu_string_functions
         
SetPointer(HourGlass!)
This.SetRedraw( False)
is_cost_weight = Space(300)
istr_error_info.se_event_name = 'ue_retrieve detail' 
is_detail = Space(30000)

//li_rtn = iu_orp003.nf_orpo33ar(istr_Error_Info, is_orderID, &
//		is_detail,is_cost_weight, is_ResReductions, ls_pa_resolve_out, "I" , ls_production_dates, is_dept_codes, ls_dup_products_ind)

li_rtn = iu_ws_orp2.nf_orpo33fr(is_orderID, &
		is_detail,is_cost_weight, is_ResReductions, ls_pa_resolve_out, "I" , ls_production_dates, is_dept_codes, ls_dup_products_ind, ls_oper_customers, istr_Error_Info)

is_dept_codes = '          ' + '~r~n' + lu_string_functions.nf_righttrim(is_dept_codes, true, true)

wf_load_oper_customers(ls_oper_customers)

ls_string = trim(is_detail)

IF li_rtn < 0 OR li_rtn > 500 Then
	This.SetRedraw( True)
	Return
END IF

ids_production_dates.reset()
ids_production_dates.importstring(ls_production_dates)

is_MicroHelp = iw_frame.wf_getmicrohelp()
wf_protect_order(dw_detail)

// Must Reset all req datawindows at this point
This.Reset()
IF This.ImportString(is_detail) < 1 THEN
	This.SetItem(InsertRow(0),"update_flag","A")
	wf_default_newrow()
Else
	wf_setdetail_businessrules()
	This.ResetUpdate()
   IF dw_customer_info.GetItemString( 1,"order_status") = 'A' OR Len(Trim(dw_customer_info.GetItemString( 1,"order_status"))) = 0 THEN
		This.SetItem( InsertRow(0), "update_flag", "A")
		wf_default_newrow()
  	END IF
END IF
is_detail_orig = this.Object.DataWindow.Data

//wf_default_newrow()

// default accrual ind to Y if blank comes in from database
String ls_ind
Integer li_row_cnt, li_row
Long ll_default_tab_text_color
Boolean	lb_change_text_color

ll_default_tab_text_color = tab_1.tp_detail.TabTextColor
lb_change_text_color = False

li_row_cnt = tab_1.tp_detail.dw_detail.RowCount()
If li_row_cnt > 0 Then
	For li_row = 1 to li_row_cnt
		ls_ind = tab_1.tp_detail.dw_detail.GetItemString(li_row,"accrual_ind")
		If tab_1.tp_detail.dw_detail.GetItemString(li_row,"accrual_ind") = ' ' Then
			tab_1.tp_detail.dw_detail.SetItem(li_row,"accrual_ind", "Y")
		End If
		If mid(tab_1.tp_detail.dw_detail.GetItemString(li_row, "business_rules"),32,1) = 'E' Then
			lb_change_text_color = True
		End If		
	Next
End If

IF lb_change_text_color Then
	tab_1.tp_detail3.TabTextColor = 255
Else
	tab_1.tp_detail3.TabTextColor = ll_default_tab_text_color
End If

ls_find_string = "ship_inv_quantity > 0"
ll_find_count = tab_1.tp_detail.dw_detail.Find(ls_find_string, 0, tab_1.tp_detail.dw_detail.RowCount())
If ll_find_count > 0 Then
	tab_1.tp_detail.dw_detail.object.ship_inv_quantity.Visible = True
	tab_1.tp_detail.dw_detail.object.t_shipped_units.Visible = True
Else
	tab_1.tp_detail.dw_detail.object.ship_inv_quantity.Visible = False
	tab_1.tp_detail.dw_detail.object.t_shipped_units.Visible = False
End If


This.ResetUpdate()
This.ShareData(tab_1.tp_detail2.dw_detail2)
This.ShareData(tab_1.tp_detail3.dw_detail3)
Choose Case dw_customer_info.GetItemString( 1, "order_status")
Case 'N', 'M', 'C'
	IF LEN( is_additional_rows ) > 0 THEN
		MESSAGEBOX("Unable to Save Add Ons", "Notepad Add ons could not be saved because order is not Accepted or incomplete")
		is_additional_rows = ''
	END if
Case Else
	IF Len(Trim(is_additional_rows)) > 0 Then
		String	ls_data
		ls_data	=	is_additional_rows
		ii_StartRow = This.RowCount()
		This.DeleteRow( ii_StartRow)
		ii_EndRow = This.ImportString( is_additional_rows)
		For li_LoopCount = ii_StartRow to (ii_EndRow + ii_StartRow - 1)
			IF lu_string_functions.nf_IsEmpty(This.GetItemString( li_LoopCount, "ordered_age")) Then
				This.SetItem( li_LoopCount, "ordered_age", "B")
			END IF
			This.SetItem( li_LoopCount, "update_Flag", "A")
			uf_changerowstatus ( li_LoopCount, NEWMODIFIED! )
		Next	
		is_additional_rows = ''
		This.SetRow(This.InsertRow(0))
		wf_default_newrow()
		Wf_Update()
		iw_This.Title = "Sales order detail - "+ is_orderid
		is_microHelp = ls_MicroHelp
		iw_frame.SetMicroHelp( ls_MicroHelp)
		This.SetRedraw(True)
		Return
	END IF
End Choose
iw_This.Title = "Sales order detail - "+ is_orderid
iw_frame.SetMicroHelp( is_MicroHelp)
This.SetFocus()
This.SetRedraw(True)
ib_resolve_running = False
If Not lu_string_functions.nf_IsEmpty(ls_pa_resolve_out) Then
	OpenWithParm(w_pa_resolve_response, ls_pa_resolve_out) 
	CHOOSE CASE Message.StringParm
	CASE	"R"
		This.PostEvent('ue_update')	
	CASE	"C"
		ib_resolve_running = True
		Post wf_complete_order()
	END CHOOSE
End If
wf_setdetail_businessrules()
dw_cost_weight.ResetUpdate()
dw_customer_info.ResetUpdate()
tab_1.tp_detail2.dw_detail2.ib_updateable	=	False
tab_1.tp_detail3.dw_detail3.ib_updateable	=	False
ib_updateable	=	False
dw_customer_info.ib_updateable	=	False
dw_cost_weight.ib_updateable	=	False
is_customer_info = dw_customer_info.GetItemString(1,"customer_id")+"S"
is_typecode = dw_customer_info.GetItemString(1,"smanlocaton")
SetPointer(Arrow!)
IF dw_customer_info.GetItemString( 1,"order_status") = 'A' OR Len(Trim(dw_customer_info.GetItemString( 1,"order_status"))) = 0 THEN
	wf_default_newrow()
end if

//

li_RowCount = this.RowCount() 

IF dw_customer_info.GetItemString( 1,"order_status") = ' ' then
	For li_Counter = 1 to li_RowCount
		ls_status = tab_1.tp_detail.dw_detail.GetItemString(li_Counter, "update_flag")
		ls_business_rule = tab_1.tp_detail.dw_detail.GetItemString(li_Counter, "business_rules")
		If mid(ls_business_rule, 9,1) = 'E' then 
			//lb_change_text_color = True
			dw_cost_weight.Modify("pricing_mass_override.Visible = '1'")
			dw_cost_weight.Modify("t_8.Visible = '1'")
			tab_1.tp_detail.dw_detail.SetItem(li_Counter, "update_flag", "A")
			uf_changerowstatus(li_Counter, NEWMODIFIED! )
			tab_1.tp_detail.dw_detail.ib_updateable = True
			dw_customer_info.ib_updateable = True

		End If
	next
end if


//li_RowCount = This.RowCount()
//
//For li_Counter =  1 to li_RowCount
//	If Mid(This.Getitemstring(li_Counter, "business_rules") , 9, 1) = 'V' then
//		This.SelectRow(li_Counter, True)
//	else
//		This.SelectRow(li_Counter, False)
//	end if
//Next
//

This.ResetUpdate()
//is_detail_orig = this.Object.DataWindow.Data
end event

event ue_update;String	ls_detail_info, &
			ls_weight_String,&
			ls_pa_resolve_out, &
			ls_production_dates, &
			ls_dept_codes, &
			ls_dup_products_ind, &
			ls_dup_message, ls_type_code, ls_code, ls_business_rule, ls_oper_customers

Long		ll_current_Row,&
			ll_loopCount,& 
			ll_RowCount, &
			ll_default_tab_text_color
			
Integer	li_ret, &
			li_RowCount, &
			li_Counter

Boolean	lb_change_text_color, lb_view_loc

u_string_functions	lu_string_functions
u_project_functions	lu_project_functions

if lu_project_functions.nf_isscheduler() then return 
 
SetPointer(HourGlass!)
//
ls_code = dw_customer_info.GetItemString(1, "smanlocaton") + Message.is_smanlocation
lb_view_loc = false
SELECT tutltypes.type_code
    INTO :ls_type_code
    FROM tutltypes
    WHERE tutltypes.record_type = 'VIEWLOC'
    AND type_code = :ls_code
	USING SQLCA ; 
	
if ls_type_code <> "" then
	lb_view_loc = true
end if 
//


//IF (dw_customer_info.GetItemString( 1, "smanlocaton") <> Message.is_smanlocation) &
//  	AND (Message.is_smanlocation <> '630' AND Message.is_smanlocation <> '606' AND Message.is_smanlocation <> '006') & 
IF (dw_customer_info.GetItemString( 1, "smanlocaton") <> Message.is_smanlocation) AND (lb_view_loc = false) &
	AND ( NOT lu_project_functions.nf_isscheduler() )	&
	AND ( NOT lu_project_functions.nf_isschedulermgr() )	&
	AND ( NOT lu_project_functions.nf_issalesmgr() ) THEN 	  
	RETURN
END IF

if ib_open then close(w_locations_popup) 

This.SetRedraw(FALSE)
ll_current_row = This.GetRow()
dw_cost_weight.AcceptText()
IF This.AcceptText() = - 1 or tab_1.tp_detail2.dw_detail2.AcceptText() = -1 &
									or tab_1.tp_detail3.dw_detail3.AcceptText() = -1 Then 
	This.SetFocus()
	This.SetRedraw(True)
END IF
ll_RowCount = This.RowCount()
If lu_string_functions.nf_IsEmpty(This.GetItemString(ll_rowcount, 'product_code')) Then
	This.DeleteRow(ll_RowCount)
End If
ls_detail_info		= lu_string_functions.nf_BuildUpdateString( This)
ls_detail_info 	= wf_remove_nonupdated_fields(ls_detail_info)
ls_detail_info 	= ls_detail_info +Space(20593 -  Len( ls_detail_info))
ls_weight_String 	= dw_cost_weight.Describe("DataWindow.Data")
ls_weight_String 	= ls_weight_String + Space( 198 - Len(ls_weight_string)) 
istr_error_info.se_event_name = 'ue_update on dw_detail'
ls_dup_products_ind = 'C'

//ii_rpc_retvalue  = 	iu_orp003.nf_orpo33ar(&
//		    				istr_error_info,&
//			 				is_orderid, &		
//		   				ls_detail_info,&
//			 				ls_weight_String,&
// 			 				is_ResReductions, &
//			 				ls_pa_resolve_out, &
//		    				"U", &
//							ls_production_dates, &
//							ls_dept_codes, &
//							ls_dup_products_ind)
							
ii_rpc_retvalue  = 	iu_ws_orp2.nf_orpo33fr(is_orderid, &		
						   ls_detail_info,&
							 ls_weight_String,&
							  is_ResReductions, &
							 ls_pa_resolve_out, &
						    "U", &
							ls_production_dates, &
							ls_dept_codes, &
							ls_dup_products_ind, &
							ls_oper_customers, &
							    istr_error_info)
								 
wf_check_microhelp_message(istr_error_info)								 
								 
wf_load_oper_customers(ls_oper_customers)								 
								 
ii_rpc_return = ii_rpc_retvalue
if ii_rpc_return = 1 then 
	ib_update_detail = TRUE		
else
	ib_update_detail = FALSE
end if
IF ii_rpc_retvalue <  0 OR ii_rpc_retvalue > 500 Then 
	This.SetRedraw( True)
	Return
END IF

If ii_rpc_retvalue = 7 Then
	ls_dup_message = 'Another line exists with the same product and age code.' 
	ls_dup_message += '~r~n' + 'Do you want to add a new line?'
	li_ret = MessageBox('', ls_dup_message, None! , YesNoCancel!) 
	If li_ret = 1 Then 
		ls_dup_products_ind = 'Y'
	Else
		If li_ret = 2 Then
			ls_dup_products_ind = 'N'
		Else 
			Return
		End If
	End If
	
//	ii_rpc_retvalue  = 	iu_orp003.nf_orpo33ar(&
//		    				istr_error_info,&
//			 				is_orderid, &		
//		   				ls_detail_info,&
//			 				ls_weight_String,&
// 			 				is_ResReductions, &
//			 				ls_pa_resolve_out, &
//		    				"U", &
//							ls_production_dates, &
//							ls_dept_codes, &
//							ls_dup_products_ind)
	
	ii_rpc_retvalue  = 	iu_ws_orp2.nf_orpo33fr(is_orderid, &		
						ls_detail_info,&
						ls_weight_String,&
						is_ResReductions, &
						ls_pa_resolve_out, &
						"U", &
						ls_production_dates, &
						ls_dept_codes, &
						ls_dup_products_ind, &
						ls_oper_customers, &
						istr_error_info)
						
	wf_check_microhelp_message(istr_error_info)						
End If

ids_production_dates.reset()
ids_production_dates.importstring(ls_production_dates)

wf_load_oper_customers(ls_oper_customers)

is_MicroHelp = iw_frame.wf_getmicrohelp()
This.Reset()
ll_RowCount	=	This.ImportString(Trim(ls_detail_info))
dw_customer_info.uof_retrieve()
dw_cost_weight.Reset()
dw_cost_weight.ImportString(ls_weight_string)
wf_setdetail_businessrules()

// default accrual ind to Y if blank comes in from database
// will happen if error comes back from an update
String ls_ind
Integer li_row_cnt, li_row

li_row_cnt = tab_1.tp_detail.dw_detail.RowCount()
If li_row_cnt > 0 Then
	For li_row = 1 to li_row_cnt
		ls_ind = tab_1.tp_detail.dw_detail.GetItemString(li_row,"accrual_ind")
		If tab_1.tp_detail.dw_detail.GetItemString(li_row,"accrual_ind") = ' ' Then
			tab_1.tp_detail.dw_detail.SetItem(li_row,"accrual_ind", "Y")
		End If
		ls_business_rule = tab_1.tp_detail.dw_detail.GetItemString(li_row, "business_rules")
		If mid(ls_business_rule, 32,1) = 'E' then 
			lb_change_text_color = True
		End If		
		If mid(ls_business_rule, 9, 1) = 'E' then 
			dw_cost_weight.Modify("pricing_mass_override.Visible = '1'")
			dw_cost_weight.Modify("t_8.Visible = '1'")
		end if
			
	Next
End If

//If li_row_cnt > 0 Then
//	for li_row = 1 to li_row_cnt
//		ls_business_rule = tab_1.tp_detail.dw_detail.GetItemString(li_row, "business_rules")
//		If mid(ls_business_rule, 32,1) = 'U' then 
//			//lb_change_text_color = True
//			dw_cost_weight.Modify("pricing_mass_override.Visible = '1'")
//			dw_cost_weight.Modify("t_8.Visible = '1'")
//		End If		
//	Next
//End If


IF lb_change_text_color Then
	tab_1.tp_detail3.TabTextColor = 255
Else
	tab_1.tp_detail3.TabTextColor = ll_default_tab_text_color
End If

//
//li_RowCount = This.RowCount()
//
//For li_Counter =  1 to li_RowCount
//	If Mid(This.Getitemstring(li_Counter, "business_rules") , 9, 1) = 'E' then
//		This.SelectRow(li_Counter, True)
//	else
//		This.SelectRow(li_Counter, False)
//	end if
//Next
//

This.SetRedraw(True)
IF ii_rpc_retValue = 6 Then
	IF MessageBox("Plant Locked", "Current Order cannot be updated, highlighted products locked by PA. Please Retry",&
						Exclamation!, RetryCancel!,1)  = 1 Then
		This.PostEvent('ue_update')
	END IF
END IF
IF ii_rpc_retvalue = 5 Then
	OpenWithParm(w_pa_resolve_response, ls_pa_resolve_out) 
	CHOOSE CASE Message.StringParm
	CASE	"R"
		This.PostEvent('ue_update')
	CASE	"C"
		ib_resolve_running = True
		wf_complete_order()
	END CHOOSE
End If
This.SetRedraw(false)
For ll_loopCount = 1 to ll_RowCount
	IF This.GetItemString( ll_LoopCount, "update_flag") =  " " Then
		uf_ChangeRowStatus( ll_LoopCount, NotModified!)
	END IF
NEXT
InsertRow(0)
IF dw_customer_info.GetItemString( 1,"order_status") = 'A' OR Len(Trim(dw_customer_info.GetItemString( 1,"order_status"))) = 0 THEN
	wf_default_newrow()
END IF
is_detail = This.Describe("DataWindow.Data")
IF Left(ls_weight_String,1) = 'T' Then 
	// display weight overide
   Message.id_load_weight = Double(Mid(ls_weight_String,3))
  	Open(w_weight_Criteria)
	ic_overide	=	Message.StringParm
   wf_Overrideweight("SAVE",FALSE, dw_detail)
	This.SetRedraw(TRUE)
	RETURN
End if
IF ii_rpc_retvalue <> 1 THEN
	This.ResetUpdate()
	dw_cost_weight.ResetUpdate()
	dw_customer_info.ResetUpdate()
END IF

ll_RowCount	=	This.RowCount()
IF ll_current_Row <= ll_RowCount Then This.ScrollToRow( ll_current_Row)
ib_Order_id_Changed_Detail = FALSE
ib_Order_id_Changed_Header = FALSE
ib_Order_id_Changed_Instruction = FALSE
ib_Order_id_Changed_Currency = False
tab_1.tp_detail2.dw_detail2.ib_updateable	=	False
tab_1.tp_detail3.dw_detail3.ib_updateable	=	False
ib_updateable	=	False
This.SetRedraw(True)
SetMicroHelp( is_MicroHelp)
SetPointer(Arrow!)
is_detail_orig = this.Object.DataWindow.Data
//This.Postevent("ue_retrieve")
end event

event constructor;call super::constructor;DataWindowChild	ldw_DDDWChild

InsertRow(0)
ib_updateable	=	False
ib_firstcolumnonnextrow	= True
ib_newrowonchange	=	True

This.GetChild("PRICE_IND", ldw_DDDWChild)
wf_gettypedesc(ldw_DDDWChild)

This.GetChild("line_splitting_ind", ldw_DDDWChild)
wf_gettypedesc(ldw_DDDWChild)

ids_production_dates = create datastore
ids_production_dates.dataobject = 'd_production_dates'
//
end event

event destructor;call super::destructor;This.ShareDataOff()
end event

event getfocus;call super::getfocus;u_project_functions	lu_project_functions

if lu_project_functions.nf_isscheduler() then
	iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = False
else
	iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = True
end if


end event

event ue_mousemove;call super::ue_mousemove;String	ls_description, &
			ls_product, &
			ls_line_number, &
			ls_find_string
			
long		ll_rowcount

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50
IF ia_apptool.ToolBarTips = TRUE THEN
	If row > 0  AND il_Row <> Row Then
		choose case dwo.name
			case 'product_code'
//		IF dwo.name = 'product_code' Then
				ls_product = dw_detail.GetItemString(row, "product_code")
				ls_product = TRIM(ls_product) + space(10 - len(TRIM(ls_product)))
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
				SELECT short_description   
					INTO :ls_description
					FROM sku_products  
					WHERE sku_product_code = :ls_product;   
				if len(trim(ls_description)) > 0 then 
					is_description = ls_description
					openwithparm(iw_tooltip, iw_this, iw_frame)
				ELSE
					iw_frame.SetMicroHelp("Ready")
				END IF
				il_row = Row
			case 'ordered_age','scheduled_age'
				ls_line_number = this.getitemstring(row,"order_detail_number")
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
				ls_find_string = "order_detail_number = '" + ls_line_number + "'"
				ll_rowcount = ids_production_dates.find(ls_find_string, 0 , ids_production_dates.rowcount())
				if ll_rowcount = 0 then
					iw_frame.SetMicroHelp("Ready")
				else
					is_description = ids_production_dates.getitemstring(ll_rowcount,"from_date") + " to " + &
											ids_production_dates.getitemstring(ll_rowcount,"to_date")
					openwithparm(iw_tooltip, iw_this, iw_frame)
				end if
				il_row = Row
//		ELSE	
			case else
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
//		END IF
		end choose
	ELSE	
		IF IsValid(iw_tooltip) AND il_Row <> Row  Then
			close(iw_tooltip)
		END IF
	END IF    
END IF
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

type tp_detail2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
long backcolor = 12632256
string text = "Detail 2"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_detail2 dw_detail2
end type

on tp_detail2.create
this.dw_detail2=create dw_detail2
this.Control[]={this.dw_detail2}
end on

on tp_detail2.destroy
destroy(this.dw_detail2)
end on

event rbuttondown;wf_rightclick()
end event

type dw_detail2 from u_base_dw_ext within tp_detail2
integer x = 14
integer y = 12
integer width = 3854
integer height = 924
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_sales_detail_part2"
boolean vscrollbar = true
boolean border = false
end type

event ue_dwndropdown;call super::ue_dwndropdown;String				ls_ColumnName

DataWindowChild	ldw_DDDWChild

ls_ColumnName	=	Upper(This.GetColumnName())

CHOOSE CASE ls_ColumnName
	CASE	'SPECIAL_FLAG'
		// Populate the spl flag  DDDW
		This.GetChild('special_flag', ldw_DDDWChild)
		wf_gettypedesc(ldw_DDDWChild)
END CHOOSE




end event

event constructor;call super::constructor;ib_newrowonchange	=	True
ib_firstcolumnonnextrow	=	True
ib_updateable	=	False
end event

event clicked;call super::clicked;IF Row > 0 Then
	Choose Case dwo.name
		Case 'order_detail_number'
//			IF Integer( This.GetItemString( row, "order_Detail_Number")) < 1 then Return
			IF (Integer(This.GetItemString( row, "order_Detail_Number")) < 1)  or &
				This.GetItemString( row, "item_status") = 'R' or &
				This.GetItemString( row, "item_status") = 'T' or &
				This.GetItemString( row, "item_status") = 'X' or &
				dw_customer_info.GetItemString( 1, "order_status") = 'M' or &
				dw_customer_info.GetItemString( 1, "order_status") = 'N' Then Return
			
			This.SelectRow( row, Not(This.IsSelected( row)))
			tab_1.tp_detail.dw_detail.SelectRow( row, (This.IsSelected( row)))
			tab_1.tp_detail3.dw_detail3.SelectRow( row, (This.IsSelected( row)))
			IF This.GetSelectedRow( 0 ) > 0 Then 
//				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = TRUE
				ib_rows_selected = True
			ELSE
//				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE
				ib_rows_selected = False
			END IF
		Case Else
			Return
	END Choose
END IF
If Integer(This.Describe("ordered_units.ID")) > 0 Then This.SetColumn( "ordered_units")
if This.ib_NewRowOnChange then
	wf_default_newrow()
end if
end event

event itemchanged;call super::itemchanged;u_project_functions lu_project_functions

DataWindowChild	ldwc_temp

string 	ls_ship_customer, &
			ls_plant_code, &
			ls_business_rules, &
			ls_business_rules_new, &
			ls_find_string

ls_ship_customer	= dw_customer_info.GetItemString(1, "customer_id")
ls_plant_code		= dw_cost_weight.GetItemString(1, "ship_plant")					

ib_updateable	=	True
If dwo.name = "product_code" Then
	//This.SetItem( row, "pricing_uom", "01")
	This.SetItem( row, "quantity_weight_ind", "Q")
	This.SetItem( row, "ordered_age", lu_project_functions.nf_get_age_code(Data,ls_ship_customer, ls_plant_code))
	This.SetItem( row, "product_code", data)
	This.SetItem( row, "update_flag", "A")
	IF KeyDown( KeyTab!) Then
		SetFocus()
		SetColumn( "ordered_age")
		SetRow( Row)
		SelectText(1,1)
	ELSE
		IF KeyDown( KeyEnter!) Then
			SetColumn( "ordered_units")
			SetRow(row)
			SetFocus()
		END IF
	END IF
END IF

If dwo.name = "date_override" Then
	If data = "Y" Then
//		set ordered_age protected
		ls_business_rules = This.GetItemString(row,"business_rules")
		ls_business_rules_new = mid(ls_business_rules,1,5) + 'V' + mid(ls_business_rules,7)
		This.SetItem(row,"business_rules",ls_business_rules_new)
//		now set from and to production dates unprotected	
		ls_business_rules = This.GetItemString(row,"business_rules")
		ls_business_rules_new = mid(ls_business_rules,1,36) + 'UU' + mid(ls_business_rules,39)
		This.SetItem(row,"business_rules",ls_business_rules_new)
	Else
//		set ordered_age un protected		
		ls_business_rules = This.GetItemString(row,"business_rules")
		ls_business_rules_new = mid(ls_business_rules,1,5) + 'U' + mid(ls_business_rules,7)
		This.SetItem(row,"business_rules",ls_business_rules_new)
//		now set from and to production dates protected	
		ls_business_rules = This.GetItemString(row,"business_rules")
		ls_business_rules_new = mid(ls_business_rules,1,36) + 'VV' + mid(ls_business_rules,39)
		This.SetItem(row,"business_rules",ls_business_rules_new)		
	End If
End If

if dwo.name = 'oper_customer_id' Then
	If data > '       ' Then
		This.GetChild("oper_customer_id", ldwc_temp)
		ls_find_string = "oper_customer_id = '" + data + "'"
		If ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount() ) <= 0 Then
				MessageBox ("Operator Customer Error", "Operator Customer is not valid")
				Return 1
		End If
	End If
End If

ib_updateable = True
dw_cost_weight.ib_updateable = True
dw_customer_info.ib_updateable = True

end event

event getfocus;call super::getfocus;u_project_functions	lu_project_functions

if lu_project_functions.nf_isscheduler() then
	iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = False
else
	iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = True
end if

end event

event rbuttondown;call super::rbuttondown;
wf_rightclick()
end event

event itemerror;call super::itemerror;u_string_functions	lu_string_functions
String					ls_business_rules

CHOOSE CASE dwo.name
	CASE	"ordered_units"
		if lu_string_functions.nf_isempty(data) then 
			This.SetItem( row, "ordered_units", 0)
			This.SetItem( row, "update_flag", "U")
			ls_business_rules = This.GetItemString( row, "business_rules")
			This.SetItem( row, "business_rules", mid(ls_business_rules,1,2) + 'M' + mid(ls_business_rules,4))
		end if
END CHOOSE
RETURN (1)
end event

event ue_mousemove;call super::ue_mousemove;String	ls_description, &
			ls_product, &
			ls_line_number, &
			ls_find_string
			
long		ll_rowcount

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50
IF ia_apptool.ToolBarTips = TRUE THEN
	If row > 0  AND il_Row <> Row Then
		choose case dwo.name
			case 'product_code'
//		IF dwo.name = 'product_code' Then
				ls_product = dw_detail2.GetItemString(row, "product_code")
				ls_product = TRIM(ls_product) + space(10 - len(TRIM(ls_product)))
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
				SELECT short_description   
					INTO :ls_description
					FROM sku_products  
					WHERE sku_product_code = :ls_product;   
				if len(trim(ls_description)) > 0 then 
					is_description = ls_description
					openwithparm(iw_tooltip, iw_this, iw_frame)
				ELSE
					iw_frame.SetMicroHelp("Ready")
				END IF
				il_row = Row
			case 'ordered_age','scheduled_age'
				ls_line_number = this.getitemstring(row,"order_detail_number")
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
				ls_find_string = "order_detail_number = '" + ls_line_number + "'"
				ll_rowcount = ids_production_dates.find(ls_find_string, 0 , ids_production_dates.rowcount())
				if ll_rowcount = 0 then
					iw_frame.SetMicroHelp("Ready")
				else
					is_description = ids_production_dates.getitemstring(ll_rowcount,"from_date") + " to " + &
											ids_production_dates.getitemstring(ll_rowcount,"to_date")
					openwithparm(iw_tooltip, iw_this, iw_frame)
				end if
				il_row = Row
//		ELSE	
			case else
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
//		END IF
		end choose
	ELSE	
		IF IsValid(iw_tooltip) AND il_Row <> Row  Then
			close(iw_tooltip)
		END IF
	END IF    
END IF
end event

type tp_detail3 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
long backcolor = 12632256
string text = "Detail 3"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_detail3 dw_detail3
end type

on tp_detail3.create
this.dw_detail3=create dw_detail3
this.Control[]={this.dw_detail3}
end on

on tp_detail3.destroy
destroy(this.dw_detail3)
end on

type dw_detail3 from u_base_dw_ext within tp_detail3
integer y = 16
integer width = 3886
integer height = 916
integer taborder = 2
string dataobject = "d_sales_detail_part3"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;string		ls_string, ls_test


is_columnname = dwo.name




IF Row > 0 Then
	Choose Case dwo.name
		Case 'order_detail_number'
		//	IF Integer( This.GetItemString( row, "order_Detail_Number")) < 1 then Return
			IF (Integer(This.GetItemString( row, "order_Detail_Number")) < 1)  or &
				This.GetItemString( row, "item_status") = 'R' or &
				This.GetItemString( row, "item_status") = 'T' or &
				This.GetItemString( row, "item_status") = 'X' or &
				dw_customer_info.GetItemString( 1, "order_status") = 'M' or &
				dw_customer_info.GetItemString( 1, "order_status") = 'N' Then Return
			
			This.SelectRow( row, Not(This.IsSelected( row)))
			tab_1.tp_detail.dw_detail.SelectRow( row, (This.IsSelected( row)))
			tab_1.tp_detail2.dw_detail2.SelectRow( row, (This.IsSelected( row)))
			IF This.GetSelectedRow( 0 ) > 0 Then 
//				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = TRUE
				ib_rows_selected = True
			ELSE
//				iw_Frame.im_Menu.m_Options.m_MoveRows.Enabled = FALSE
				ib_rows_selected = False
			END IF
		case 'plant1'
			ib_header = false
			il_selected_row = row
			is_product_division = wf_get_productdivision()
			ib_open = true
			wf_get_selected_plants(this)
			choose case (MID( this.getitemstring(row, "business_rules" ), 3, 1))
				case 'V'
					//protect
					ib_detail_updateable = False
				case 'S', 'E'
					//unprotect
					ib_detail_updateable = True 
				case else
					ib_detail_updateable = True
			end choose
			OpenWithParm( w_locations_popup, iw_this)
		Case Else
			Return
	END Choose
END IF
If Integer(This.Describe("ordered_units.ID")) > 0 Then This.SetColumn( "ordered_units")


end event

event constructor;call super::constructor;String				ls_ColumnName
DataWindowChild	ldw_DDDWChild

ib_newrowonchange	=	True
ib_firstcolumnonnextrow	=	True
ib_updateable	=	False

//// Populate the spl flag  DDDW
//This.GetChild('special_flag', ldw_DDDWChild)
//wf_gettypedesc(ldw_DDDWChild)
//
	
// Populate the gpo ind  DDDW
This.GetChild("gpo_ind", ldw_DDDWChild)
wf_gettypedesc(ldw_DDDWChild)

// Populate the price ind  DDDW
This.GetChild("price_ind", ldw_DDDWChild)
wf_gettypedesc(ldw_DDDWChild)
//
end event

event getfocus;call super::getfocus;u_project_functions	lu_project_functions

if lu_project_functions.nf_isscheduler() then
	iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = False
else
	iw_Frame.im_Menu.m_Options.m_BrowseReservations.Enabled = True
end if
end event

event itemchanged;call super::itemchanged;u_project_functions lu_project_functions
string 	ls_ship_customer, &
			ls_plant_code

ls_ship_customer	= dw_customer_info.GetItemString(1, "customer_id")
ls_plant_code		= dw_cost_weight.GetItemString(1, "ship_plant")			

ib_updateable	=	True
If dwo.name = "product_code" Then
	//This.SetItem( row, "pricing_uom", "01")
	This.SetItem( row, "quantity_weight_ind", "Q")
	This.SetItem( row, "ordered_age", lu_project_functions.nf_get_age_code(Data,ls_ship_customer, ls_plant_code))
	This.SetItem( row, "product_code", data)
	This.SetItem( row, "update_flag", "A")
	IF KeyDown( KeyTab!) Then
		SetFocus()
		SetColumn( "ordered_age")
		SetRow( Row)
		SelectText(1,1)
	ELSE
		IF KeyDown( KeyEnter!) Then
			SetColumn( "ordered_units")
			SetRow(row)
			SetFocus()
		END IF
	END IF
END IF

if This.ib_NewRowOnChange then
	wf_default_newrow()
end if

ib_updateable = True
dw_cost_weight.ib_updateable = True
dw_customer_info.ib_updateable = True


end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event ue_dwndropdown;call super::ue_dwndropdown;String				ls_ColumnName, &
						ls_filter_string

DataWindowChild	ldw_DDDWChild

ls_ColumnName	=	Upper(This.GetColumnName())

CHOOSE CASE ls_ColumnName
	CASE	'SPECIAL_FLAG'
		// Populate the spl flag  DDDW
		This.GetChild('special_flag', ldw_DDDWChild)
		wf_gettypedesc(ldw_DDDWChild)
	CASE	'GPO_IND'
		// Populate the gpo ind  DDDW
		This.GetChild("gpo_ind", ldw_DDDWChild)
		wf_gettypedesc(ldw_DDDWChild)
	CASE	'PRICE_IND'
		// Populate the price ind  DDDW
		This.GetChild("price_ind", ldw_DDDWChild)
		wf_gettypedesc(ldw_DDDWChild)
	CASE	'CUST_DEPT_CODE_1'
		// Populate the cust dept code ind  DDDW
		This.GetChild("cust_dept_code_1", ldw_DDDWChild)
		ldw_DDDWChild.Reset()
		ldw_DDDWChild.ImportString(is_dept_codes)
		
		
	CASE ELSE
		RETURN
END CHOOSE

end event

event itemerror;call super::itemerror;u_string_functions	lu_string_functions
String					ls_business_rules

CHOOSE CASE dwo.name
	CASE	"ordered_units"
		if lu_string_functions.nf_isempty(data) then 
			This.SetItem( row, "ordered_units", 0)
			This.SetItem( row, "update_flag", "U")
			ls_business_rules = This.GetItemString( row, "business_rules")
			This.SetItem( row, "business_rules", mid(ls_business_rules,1,2) + 'M' + mid(ls_business_rules,4))
		end if
END CHOOSE
RETURN (1)
end event

event ue_mousemove;call super::ue_mousemove;String	ls_description, &
			ls_product, &
			ls_line_number, &
			ls_find_string
			
long		ll_rowcount

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50
IF ia_apptool.ToolBarTips = TRUE THEN
	If row > 0  AND il_Row <> Row Then
		choose case dwo.name
			case 'product_code'
//		IF dwo.name = 'product_code' Then
				ls_product = this.GetItemString(row, "product_code")
				ls_product = TRIM(ls_product) + space(10 - len(TRIM(ls_product)))
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
				SELECT short_description   
					INTO :ls_description
					FROM sku_products  
					WHERE sku_product_code = :ls_product;   
				if len(trim(ls_description)) > 0 then 
					is_description = ls_description
					openwithparm(iw_tooltip, iw_this, iw_frame)
				ELSE
					iw_frame.SetMicroHelp("Ready")
				END IF
				il_row = Row
			case 'ordered_age','scheduled_age'
				ls_line_number = this.getitemstring(row,"order_detail_number")
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
				ls_find_string = "order_detail_number = '" + ls_line_number + "'"
				ll_rowcount = ids_production_dates.find(ls_find_string, 0 , ids_production_dates.rowcount())
				if ll_rowcount = 0 then
					iw_frame.SetMicroHelp("Ready")
				else
					is_description = ids_production_dates.getitemstring(ll_rowcount,"from_date") + " to " + &
											ids_production_dates.getitemstring(ll_rowcount,"to_date")
					openwithparm(iw_tooltip, iw_this, iw_frame)
				end if
				il_row = Row
//		ELSE	
			case else
				IF IsValid(iw_tooltip) Then
					close(iw_tooltip)
				END IF
//		END IF
		end choose
	ELSE	
		IF IsValid(iw_tooltip) AND il_Row <> Row  Then
			close(iw_tooltip)
		END IF
	END IF    
END IF
end event

event itemfocuschanged;call super::itemfocuschanged;is_columnname = dwo.name

if is_columnname = 'cust_prod_code' then
	This.SetText(Trim(this.GetText()))
	This.SelectText(1,len(trim(this.GetText())))
end if


end event

event losefocus;if KeyDown (keytab!) then
	choose case is_columnname
		case 'inc_exl_ind'
			ib_header = false
			il_selected_row = this.getrow()
			is_product_division = wf_get_productdivision()
			wf_get_selected_plants(this)
			ib_open = true
			OpenWithParm( w_locations_popup, iw_this)
	end choose
end if
end event

type tp_header from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
long backcolor = 12632256
string text = "Header"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_header dw_header
dw_load_instruction dw_load_instruction
dw_age_code_type dw_age_code_type
dw_dept_code dw_dept_code
dw_gtl_split_code dw_gtl_split_code
dw_export_country dw_export_country
end type

on tp_header.create
this.dw_header=create dw_header
this.dw_load_instruction=create dw_load_instruction
this.dw_age_code_type=create dw_age_code_type
this.dw_dept_code=create dw_dept_code
this.dw_gtl_split_code=create dw_gtl_split_code
this.dw_export_country=create dw_export_country
this.Control[]={this.dw_header,&
this.dw_load_instruction,&
this.dw_age_code_type,&
this.dw_dept_code,&
this.dw_gtl_split_code,&
this.dw_export_country}
end on

on tp_header.destroy
destroy(this.dw_header)
destroy(this.dw_load_instruction)
destroy(this.dw_age_code_type)
destroy(this.dw_dept_code)
destroy(this.dw_gtl_split_code)
destroy(this.dw_export_country)
end on

event rbuttondown;wf_rightclick()
end event

type dw_header from u_base_dw_ext within tp_header
integer x = 27
integer y = 16
integer width = 3474
integer height = 908
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_sales_header"
boolean border = false
string icon = "AppIcon!"
boolean ib_scrollable = false
end type

event ue_retrieve;/***************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/11/2006  SR3802        Added new loading instruction 6 and 7
Keairnsd		06/2008		SR5571		  Added domestic exporter logic
**********************************************************************************/


string		ls_weight_overide_String, &
				ls_temp, ls_age_type_code, &
				ls_pss_ind, ls_loading_instr, ls_protect, &
				ls_shortage_out,ls_gtl_code, &
				ls_dept_code, &
				ls_trans_mode, &
				ls_dom_exp_ind, &
				ls_exp_country, & 
				ls_order_status, &
				ls_102_group, &
				ls_business_rules, &
				ls_type_of_sale, &
				ls_protect_ind
				
char        lc_addition

SetPointer(Hourglass!)
This.SetRedraw(False)
is_header = is_orderid
istr_error_info.se_event_name = 'ue_retrieve on header'
//dld gtl
//dmk dom exp ind, export country sr5571
// Call a Netwise external function to get the customer information

//IF iu_orp003.nf_orpo32ar(istr_Error_Info, is_header, 'I', &
//	ls_weight_overide_String, is_cost_weight, lc_addition, ls_age_type_code, &
//	ls_pss_ind, ls_loading_instr, ls_protect, ' ', ls_shortage_out,ls_gtl_code, & 
//	ls_dept_code, ls_dom_exp_ind, ls_exp_country) < 0 Then 
//	This.SetRedraw(True)
//	Return
//END IF
//
IF iu_ws_orp2.nf_orpo32fr(is_header, 'I', &
	ls_weight_overide_String, is_cost_weight, lc_addition, ls_age_type_code, &
	ls_pss_ind, ls_loading_instr, ls_protect, ' ', ls_shortage_out,ls_gtl_code, & 
	ls_dept_code, ls_dom_exp_ind, ls_exp_country,istr_Error_Info, ' ') < 0 Then 
	This.SetRedraw(True)
	Return
END IF


This.Reset()
ls_temp = is_header
IF This.ImportString(Trim(is_header)) < 1 THEN 
  	InsertRow(0)
ELSE
	dw_customer_info.SetItem(1, 'business_rule', is_cust_business_rule)
	dw_cost_weight.SetItem(1, 'business_rules', is_header_business_rule)

   ls_trans_mode = this.getitemstring( 1, "trans_mode") 
   IF (ls_trans_mode = 'B') or (ls_trans_mode = 'L') THEN	
		this.object.buyers_delivery_date.protect = 0
		this.object.buyers_delivery_date.background.color = '16777215'
	else
		this.object.buyers_delivery_date.protect = 1
		this.object.buyers_delivery_date.background.color = '12632256'
	end if
	
	This.ResetUpdate()
END IF
////
//dw_header.SetItem(1, 'micro_test_ind', lc_addition)
////

//messagebox("PSS",ls_pss_ind)

//choose case ls_protect
//	case "1"
//		dw_load_instruction.enabled = false
//	case else
//		dw_load_instruction.enabled = true
//end choose

//SLH
If mid(dw_header.GetItemString(1, "business_rules"),37, 1) = 'U' then
	dw_header.object.partner_order_id.visible = false
	dw_header.object.t_partner_order_id.visible = false
Else
	dw_header.object.partner_order_id.visible = true
	dw_header.object.t_partner_order_id.visible = true	
End If 	
    

is_original_type_of_order = This.GetItemString(1, "type_of_order")

//choose case ls_loading_instr
//	case "1","2","3","4","5","6","7"
		dw_load_instruction.setitem(1,"load_instructions",ls_loading_instr)
//	case else
//		dw_load_instruction.setitem(1,"load_instructions","4")
//end choose
if isnull(ls_age_type_code) then
	dw_age_code_type.setitem(1,"age_type_code", "S")
else
	dw_age_code_type.setitem(1,"age_type_code",ls_age_type_code)
end if
if isnull(ls_pss_ind) then
	dw_age_code_type.setitem(1,"pss_ind", "N")
else
	dw_age_code_type.setitem(1,"pss_ind",ls_pss_ind)
end if
//dld gtl
if isnull(ls_gtl_code) then
	dw_gtl_split_code.setitem(1,"gtl_split_ind", "N")
else
	dw_gtl_split_code.setitem(1,"gtl_split_ind",ls_gtl_code)
end if
//dw_gtl_split_code.visable = true	

if isnull(ls_dept_code) then
	dw_dept_code.SetItem(1,"dept_code", '          ')
else
	dw_dept_code.SetItem(1,"dept_code", ls_dept_code)
end if

if ls_dom_exp_ind = 'Y' then
	dw_export_country.Visible = True
	dw_export_country.setitem(1,"export_country",ls_exp_country)
	ls_order_status = dw_customer_info.GetItemString( 1, "order_status") 
	if ls_order_status = ' ' or ls_order_status = 'A' Then
  	   dw_export_country.Object.export_country.Protect = 0 
		dw_export_country.Modify("export_country.Background.Color='16777215'") 	
	Else	
		dw_export_country.Object.export_country.Protect = 1 	
		dw_export_country.Modify("export_country.Background.Color='12632256'") 
	end if
else
	dw_export_country.Visible = False
	dw_export_country.TabOrder = 0
end if

//if mid(dw_header.GetItemString(1, "business_rules"),33, 1) = 'U' then
//	dw_header.object.sourcing_allowed_ind.protect = 0
//	dw_header.object.sourcing_allowed_ind.background.color = '16777215'	
//	
//	If dw_header.GetItemString(1, "sourcing_allowed_ind") = 'Y' Then
//		dw_header.object.splitting_allowed_ind.protect = 0
//		dw_header.object.splitting_allowed_ind.background.color = '16777215'
//	Else
//		dw_header.object.splitting_allowed_ind.protect = 1
//		dw_header.object.splitting_allowed_ind.background.color = '12632256'
//	End If 	
//else
//		dw_header.object.sourcing_allowed_ind.protect = 1
//		dw_header.object.sourcing_allowed_ind.background.color = '12632256'	
//	
//End If
//
//if mid(dw_header.GetItemString(1, "business_rules"),34, 1) = 'U' then
//	dw_header.object.splitting_allowed_ind.protect = 0
//	dw_header.object.splitting_allowed_ind.background.color = '16777215'
//	
//	If dw_header.GetItemString(1, "splitting_allowed_ind") = 'Y' Then
//		dw_header.object.splitting_uom.protect = 0
//		dw_header.object.splitting_uom.background.color = '16777215'
//	Else
//		dw_header.object.splitting_uom.protect = 1
//		dw_header.object.splitting_uom.background.color = '12632256'
//	End If
//else
//	dw_header.object.splitting_allowed_ind.protect = 1
//	dw_header.object.splitting_allowed_ind.background.color = '12632256'	
//End If
//
//if mid(dw_header.GetItemString(1, "business_rules"),35, 1) = 'U' then
//	dw_header.object.splitting_uom.protect = 0
//	dw_header.object.splitting_uom.background.color = '16777215'
//else
//	dw_header.object.splitting_uom.protect = 1
//	dw_header.object.splitting_uom.background.color = '12632256'	
//end if
	
//slh SR25459	
If dw_header.GetItemString(1, "micro_test_ind") = "Y" then
	dw_header.object.micro_test_ship_type.Visible = True
Else
	dw_header.object.micro_test_ship_type.Visible = False
End If

ls_102_group = 'N'
ls_order_status = dw_customer_info.GetItemString( 1, "order_status") 
ls_business_rules = this.GetItemString(1, "business_rules")
IF isValid(iw_frame) then
	IF IsValid(iw_frame.iu_netwise_data) Then
		If (iw_frame.iu_netwise_data.is_groupid = '102') then
			ls_102_group = 'Y'
		End If
	End If
End If

If (ls_order_status = ' ' or ls_order_status = 'A')  Then
	If ls_102_group = 'Y' Then
		this.object.micro_test_ship_type.protect = 0
		this.object.micro_test_ship_type.background.color = 16777215
		this.SetItem(1, "business_rules", mid(ls_business_rules, 1, 35) + 'U' + mid(ls_business_rules, 37) )
	Else	
		this.object.micro_test_ship_type.protect = 1
		this.object.micro_test_ship_type.background.color = 12632256
		this.SetItem(1, "business_rules", mid(ls_business_rules, 1, 35) + 'V' + mid(ls_business_rules, 37) )
	End If
Else
   	this.object.micro_test_ship_type.protect = 1
	this.object.micro_test_ship_type.background.color = 12632256
	this.SetItem(1, "business_rules", mid(ls_business_rules, 1, 35) + 'V' + mid(ls_business_rules, 37) )
End If

ls_type_of_sale = this.GetItemString( 1, "type_of_sale")
If (ls_type_of_sale) = 'E' or (ls_type_of_sale = 'I') Then
	ls_protect_ind = mid(This.GetItemString(1, "business_rules"), 38, 1)
	this.object.t_5.Visible = True
	this.object.container_ind.Visible = True
	if ls_protect_ind = 'U' Then
		this.object.container_ind.Protect = 0 
		this.object.container_ind.background.color = 16777215
	Else
		this.object.container_ind.Protect = 1 
		this.object.container_ind.background.color = 12632256		
	End If
Else
	this.object.container_ind.Visible = False
	this.object.t_5.Visible = False
End If

dw_customer_info.SetColumn("customer_id")
dw_customer_info.SetFocus()
iw_This.Title = "Sales order Header - "+ is_orderid
ib_updateable	=	False
dw_customer_info.ib_updateable	=	False
dw_cost_weight.ib_updateable	=	False
is_customer_info = dw_customer_info.GetItemString(1,"customer_id")+"S"
is_typecode = dw_customer_info.GetItemString(1,"smanlocaton")

wf_protect_order(dw_header)
iw_frame.im_netwise_menu.M_file.M_Save.Enabled = true
This.SetRedraw(True)
This.ResetUpdate()
SetPointer(Arrow!)

end event

event ue_update;String	ls_header_info,&
      	ls_cost_weight_info,&
			ls_business_rules, &
			ls_microhelp, &
			ls_age_type_code, &
			ls_pss_ind, &
			ls_loading_instr, ls_protect, &
			ls_shortage_out, &
			ls_ReturnVal,ls_gtl_code, &
			ls_dept_code, &
			ls_dom_exp_ind, &
			ls_exp_country, &
			ls_customer_id, &
			ls_new_type_of_order, &
			ls_find_string, &
			ls_temp
			
long		ll_found			
						
	
char     lc_addition        
boolean	lb_custid, &
			lb_smancode
			
Date	ldt_new_delivery_date			
			
Window	lw_detail
			
SetPointer(HourGlass!)			
This.SetRedraw(False)
// Take things out of cust dw put in header dw
IF dw_customer_info.AcceptText() = -1 THEN RETURN 
IF	dw_cost_weight.AcceptText()	= -1 THEN RETURN

IF Trim(This.Object.customer_id[1]) <> Trim(dw_customer_info.object.customer_id[1]) THEN
	This.Object.customer_id[1]	= dw_customer_info.GetItemString( 1, "customer_id")
	lb_custid	=	True
END IF	
IF Trim(This.Object.sman_code[1]) <> Trim(dw_customer_info.object.smancode[1]) THEN
	This.Object.sman_code[1]	= dw_customer_info.object.smancode[1]
	lb_smancode	=	True
END IF
	ls_business_rules	=	This.Object.business_rules[1]
IF lb_custid THEN
	ls_business_rules	=	Left(ls_business_rules, 2 ) + 'M' + Mid(ls_business_rules, 4)
END IF	
IF lb_smancode THEN
	ls_business_rules	=	Left(ls_business_rules, 1 ) + 'M' + Mid(ls_business_rules, 3)
END IF	
This.Object.business_rules[1]	=	ls_business_rules
This.AcceptText()
ls_loading_instr = dw_load_instruction.getitemstring(1,"load_instructions")

ls_header_info 		= Upper(dw_header.Describe( "DataWindow.Data")) 
ls_cost_weight_info	= Upper(dw_cost_weight.Describe( "DataWindow.Data"))	
istr_error_info.se_event_name = 'ue_header_save'

ls_age_type_code = dw_age_code_type.getitemstring(1,"age_type_code")
ls_pss_ind = dw_age_code_type.getitemstring(1,"pss_ind")
// dld gtl
ls_gtl_code = dw_gtl_split_code.getitemstring(1,"gtl_split_ind")
////test
ls_dept_code = dw_dept_code.getitemstring(1,"dept_code")
//dmk
ls_customer_id = dw_customer_info.getitemstring(1,"customer_id") 
ls_exp_country = dw_export_country.getitemstring(1,"export_country")
ls_dom_exp_ind = dw_export_country.getitemstring(1,"dom_export_ind")

If isnull(ls_exp_country) Then
	ls_exp_country = ' '
End if
If isnull(ls_dom_exp_ind) Then
	ls_dom_exp_ind = ' '
End if

ls_new_type_of_order = tab_1.tp_header.dw_header.GetItemString(1, "type_of_order")
If is_original_type_of_order <> ls_new_type_of_order Then
	If mid(is_original_type_of_order,1,1) = 'N' and ls_new_type_of_order = 'E' Then
		ls_find_string = "mid(gpo_ind,1,1) <> 'D' and mid(price_ind,1,1) <> 'S'"
		ll_found = tab_1.tp_detail.dw_detail.Find(ls_find_string, 1, tab_1.tp_detail.dw_detail.RowCount())
		If ll_found > 0 Then	
			If MessageBox ("Type of Order Warning", "Price indicator on all non-GPO order lines will be set to match order header", StopSign!, OKCancel!) = 2 Then
				Return		
			End If
		End If
	Else
		If mid(is_original_type_of_order,1,1) = 'E' and ls_new_type_of_order = 'N' Then 
			ls_find_string = "mid(gpo_ind,1,1) <> 'D' and mid(price_ind,1,1) <> 'N'"
			ll_found = tab_1.tp_detail.dw_detail.Find(ls_find_string, 1, tab_1.tp_detail.dw_detail.RowCount())
			If ll_found > 0 Then	
				If MessageBox ("Type of Order Warning", "Price indicator on all non-GPO order lines will be set to match order header", StopSign!, OKCancel!) = 2 Then
					Return		
				End If
			End If
		End If
	End If
End If


//messagebox("ls_gtl_code",ls_gtl_code)
//dmk added ls_exp_country
//ii_rpc_retvalue  = 	iu_orp003.nf_orpo32ar( istr_error_info, &
//  						ls_Header_Info, 'U', &
//    						is_weight_override_String, ls_cost_weight_info, lc_addition, &
//							ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
//							'P', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country)
							
							
ii_rpc_retvalue  = 	iu_ws_orp2.nf_orpo32fr(	ls_Header_Info, 'U', &
    						is_weight_override_String, ls_cost_weight_info, lc_addition, &
							ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
							'P', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country , istr_error_info, 'C')
							
wf_check_microhelp_message(istr_error_info)	

IF ii_rpc_retvalue <  0 Then 
	
	This.SetRedraw( True)
	Return
ELSE
	if ii_rpc_retvalue = 3 then
//		MessageBox("shortage string", ls_shortage_out)	
		OpenWithParm(w_updt_order_prod_short_resp, ls_shortage_out)
		ls_ReturnVal = Message.StringParm
   	IF ls_ReturnVal = 'OK' Then
			ls_loading_instr = dw_load_instruction.getitemstring(1,"load_instructions")
			ls_header_info 		= Upper(dw_header.Describe( "DataWindow.Data")) 
			ls_cost_weight_info	= Upper(dw_cost_weight.Describe( "DataWindow.Data"))	
			istr_error_info.se_event_name = 'ue_header_save'
			ls_age_type_code = dw_age_code_type.getitemstring(1,"age_type_code")
			ls_pss_ind = dw_age_code_type.getitemstring(1,"pss_ind")
		//dmk added ls_dom_exp_ind, ls_exp_country
			//ii_rpc_retvalue  = 	iu_orp003.nf_orpo32ar( istr_error_info, &
    			//			ls_Header_Info, 'U', &
    			//			is_weight_override_String, ls_cost_weight_info, lc_addition, &
		 	//				ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
			//				'U', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country)
				
			ii_rpc_retvalue  = 	iu_ws_orp2.nf_orpo32fr(ls_Header_Info, 'U', &
    						is_weight_override_String, ls_cost_weight_info, lc_addition, &
							ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
							'U', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country , istr_error_info, 'O')	
			wf_check_microhelp_message(istr_error_info)					
		else
			// User cancelled update //
			This.SetRedraw(True)
			Return
		end if
					
	end if
	
	if ii_rpc_retvalue = 4 then
		This.SetRedraw(True)
		If MessageBox ("Linked Orders Warning", "Warning - Delivery Date on linked orders is different from current order, Continue?", Question!, YesNo!) = 2 Then
			Return		
		End If

		ls_loading_instr = dw_load_instruction.getitemstring(1,"load_instructions")
		ls_header_info 		= Upper(dw_header.Describe( "DataWindow.Data")) 
		ls_cost_weight_info	= Upper(dw_cost_weight.Describe( "DataWindow.Data"))	
		istr_error_info.se_event_name = 'ue_header_save'
		ls_age_type_code = dw_age_code_type.getitemstring(1,"age_type_code")
		ls_pss_ind = dw_age_code_type.getitemstring(1,"pss_ind")
			
		ii_rpc_retvalue  = 	iu_ws_orp2.nf_orpo32fr(ls_Header_Info, 'U', &
						is_weight_override_String, ls_cost_weight_info, lc_addition, &
						ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
						'P', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country , istr_error_info, 'O')	
		wf_check_microhelp_message(istr_error_info)
			
		if ii_rpc_retvalue = 3 then
	//		MessageBox("shortage string", ls_shortage_out)	
			OpenWithParm(w_updt_order_prod_short_resp, ls_shortage_out)
			ls_ReturnVal = Message.StringParm
			IF ls_ReturnVal = 'OK' Then
				ls_loading_instr = dw_load_instruction.getitemstring(1,"load_instructions")
				ls_header_info 		= Upper(dw_header.Describe( "DataWindow.Data")) 
				ls_cost_weight_info	= Upper(dw_cost_weight.Describe( "DataWindow.Data"))	
				istr_error_info.se_event_name = 'ue_header_save'
				ls_age_type_code = dw_age_code_type.getitemstring(1,"age_type_code")
				ls_pss_ind = dw_age_code_type.getitemstring(1,"pss_ind")
			//dmk added ls_dom_exp_ind, ls_exp_country
				//ii_rpc_retvalue  = 	iu_orp003.nf_orpo32ar( istr_error_info, &
					//			ls_Header_Info, 'U', &
					//			is_weight_override_String, ls_cost_weight_info, lc_addition, &
				//				ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
				//				'U', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country)
					
				ii_rpc_retvalue  = 	iu_ws_orp2.nf_orpo32fr(ls_Header_Info, 'U', &
								is_weight_override_String, ls_cost_weight_info, lc_addition, &
								ls_age_type_code, ls_pss_ind, ls_loading_instr, ls_protect, &
								'U', ls_shortage_out,ls_gtl_code, ls_dept_code, ls_dom_exp_ind, ls_exp_country , istr_error_info, 'O')	
				wf_check_microhelp_message(istr_error_info)					
			else
				// User cancelled update //
				This.SetRedraw(True)
				Return
			end if
		end if	
					
	end if
	
	IF Left(is_weight_override_String,1) = 'T' THEN
		// display weight overide
		ls_temp = mid(is_weight_override_String,3)
   	   Message.StringParm = mid(is_weight_override_String,3)
	   OpenWithParm(w_weight_Criteria, ls_temp)
		IF Message.StringParm <> 'ABORT' Then
			ic_overide	=	Message.StringParm
			wf_overrideweight("SAVE",TRUE, dw_header)
		END IF
		RETURN
	END IF
	IF Len(Trim(( ls_Header_info))) > 0 THEN
		ls_microhelp	=	iw_frame.wf_getmicrohelp()
		dw_Header.Reset()
		dw_header.ImportString( ls_Header_Info)
		dw_customer_info.uof_retrieve()
		dw_cost_weight.Reset()
		dw_cost_weight.ImportString(ls_cost_weight_info)
		dw_customer_info.SetItem(1, 'business_rule', is_cust_business_rule)
//		dw_cost_weight.SetItem(1, 'business_rules', is_header_business_rule)
		is_header= ls_Header_Info
		iw_frame.SetMicrohelp(ls_microhelp)
	END IF
END IF
////
//	dw_header.SetItem(1, 'micro_test_ind', lc_addition)
////

//slh SR25459	
If dw_header.GetItemString(1, "micro_test_ind") = "Y" then
	dw_header.object.micro_test_ship_type.Visible = True
Else
	dw_header.object.micro_test_ship_type.Visible = False
End If

dw_age_code_type.SetItem(1, "age_type_code", ls_age_type_code)

dw_load_instruction.setitem(1,"load_instructions",ls_loading_instr)

IF ii_rpc_retvalue <> 1 THEN
	This.ResetUpdate()
	dw_cost_weight.ResetUpdate()
END IF
//dmk
If ls_dom_exp_ind = 'Y' Then
	dw_export_country.Visible = True
	dw_export_country.setitem(1,"export_country",ls_exp_country)
	dw_customer_info.setitem(1,"customer_id",ls_customer_id)
  
	If (MID( ls_business_rules , 7, 1) = 'V')  Then
		dw_export_country.Object.export_country.Protect = 1
		dw_export_country.Modify("export_country.Background.Color='12632256'") 	
	Else 
		dw_export_country.Object.export_country.Protect = 0
		dw_export_country.Modify("export_country.Background.Color='16777215'") 		
	End if
Else
	dw_export_country.Visible = False
	dw_export_country.TabOrder = 0
	dw_export_country.setitem(1,"export_country",' ')
End if

iw_frame.im_netwise_menu.M_file.M_Save.Enabled = true
ib_Order_id_Changed_Detail = FALSE
ib_Order_id_Changed_Header = FALSE
ib_Order_id_Changed_Instruction = FALSE
ib_Order_id_Changed_Currency = FALSE
ib_updateable	=	False
dw_customer_info.ib_updateable	=	False
dw_cost_weight.ib_updateable	=	False


This.SetRedraw(TRUE)
ib_updated_header = true
//tab_1.tp_detail.dw_detail.triggerevent ("ue_retrieve")
RETURN 
SetPointer(Arrow!)
end event

event constructor;call super::constructor;ib_updateable	=	False

DataWindowChild	ldwc_temp

This.getchild("purch_prod_status",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve("PPSTATUS")

This.getchild("store_door",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

This.getchild("deck_code",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

This.getchild("sourcing_allowed_ind",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

This.getchild("splitting_allowed_ind",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

This.getchild("splitting_uom",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

//SLH SR25459
This.getchild("micro_test_ship_type",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve("MICROTYP")

end event

event ue_postconstructor;call super::ue_postconstructor;This.SetTransObject( SQLCA)
This.Retrieve("     ")
end event

event itemchanged;call super::itemchanged;ib_updateable	=	True

String	ls_business_rules

data = TRIM(data)

IF dwo.name	=	'trans_mode' THEN
	IF data	=	'T' THEN	SetItem(row, 'freight_pymt_plan', 'C')
END IF		

//revgll
IF dwo.name	=	'trans_mode' THEN
	IF (data	=	'B') or (data = 'L') THEN	
		dw_header.object.buyers_delivery_date.protect = 0
		dw_header.object.buyers_delivery_date.background.color = '16777215'
	else
		dw_header.object.buyers_delivery_date.protect = 1
		dw_header.object.buyers_delivery_date.background.color = '12632256'
	end if
END IF		


IF dwo.name	=	'sourcing_allowed_ind' THEN
	IF data  = 'Y' Then
		This.object.splitting_allowed_ind.protect = 0
		This.object.splitting_allowed_ind.background.color = '16777215'
	Else
		This.object.splitting_allowed_ind.protect = 1
		This.object.splitting_allowed_ind.background.color = '12632256'
		This.SetItem(1, "splitting_allowed_ind", 'N')
		ls_business_rules = This.GetItemString(1,"business_rules")
		ls_business_rules = mid(ls_business_rules,1,33) + 'M' + mid(ls_business_rules,35)
		This.SetItem(1,"business_rules",ls_business_rules)		
		This.object.splitting_uom.protect = 1
		This.object.splitting_uom.background.color = '12632256'		
	End If 
End IF
		
IF dwo.name	=	'splitting_allowed_ind' THEN
	IF data  = 'Y' Then
		This.object.splitting_uom.protect = 0
		This.object.splitting_uom.background.color = '16777215'
	Else
		This.object.splitting_uom.protect = 1
		This.object.splitting_uom.background.color = '12632256'
	End If 
End IF

//slh SR25459
IF dwo.name = 'micro_test_ind' THEN
	IF data = 'Y' Then
		This.object.micro_test_ship_type.Visible = True
		This.SetItem(1,"micro_test_ship_type", "H")
	Else
		This.object.micro_test_ship_type.Visible = False
	End If
End If
		
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

type dw_load_instruction from u_base_dw_ext within tp_header
integer x = 2414
integer y = 336
integer width = 640
integer height = 60
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_load_instruction"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild			ldwc_temp

this.insertrow(0)
dw_load_instruction.GetChild('load_instructions', ldwc_temp)

IF ldwc_temp.RowCount()	= 0 THEN 
	ldwc_temp.SetTransObject(SQLCA)
	ldwc_temp.Retrieve()
end if
end event

event itemchanged;call super::itemchanged;/***************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/11/2006  SR3802        Modified if loading instructions type = 6, shrink wrap and palletize = 'N',
												  if loading instructions type= '7', shrink wrap and palletize = 'Y'.
**********************************************************************************/
String ls_type

//this is the change ibdkcjr
//choose case dwo.name
//	case "load_instructions"
//		choose case data
//			case '1','2','5','7' //,'7' Added for SR3802
//				dw_header.setitem(1,'palletize','Y')
//				dw_header.setitem(1,'shrink_wrap','Y')
//			case else
//				dw_header.setitem(1,'palletize','N')
//				dw_header.setitem(1,'shrink_wrap','N')
//		end choose
//end choose

//SR#26379
choose case dwo.name
	case "load_instructions"
		
		SELECT TYPE_CODE
		INTO :ls_type
		FROM TUTLTYPES
		WHERE RECORD_TYPE = 'LDINPALL'
			AND TYPE_CODE = :data
			USING SQLCA;
	
		if sqlca.sqlcode = 0 then
			dw_header.setitem(1,'palletize','Y')
		else					
		 	//not found
			dw_header.setitem(1,'palletize','N')
		end if
	
		SELECT TYPE_CODE
		INTO :ls_type
		FROM TUTLTYPES
		WHERE RECORD_TYPE = 'LDINSHRK'
			AND TYPE_CODE = :data
			USING SQLCA;
		
		if sqlca.sqlcode = 0 then
			dw_header.setitem(1,'shrink_wrap','Y')
		else					
		 	//not found
			dw_header.setitem(1,'shrink_wrap','N')
		end if
end choose

end event

type dw_age_code_type from u_base_dw_ext within tp_header
integer x = 2359
integer y = 404
integer width = 603
integer height = 64
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_age_code_type"
boolean border = false
end type

event constructor;call super::constructor;u_project_functions		lu_project
datawindowchild 			ldw_DDDWChild
this.insertrow(0)
This.GetChild("age_type_code", ldw_DDDWChild)
IF ldw_DDDWChild.RowCount() > 1 THEN RETURN
ldw_DDDWChild.SetTransObject(SQLCA)
ldw_DDDWChild.Retrieve()

this.setitem(1,"age_type_code","S")
if lu_project.nf_isscheduler() or lu_project.nf_isschedulermgr() then
	this.enabled = True
else
	this.enabled = false
end if


end event

type dw_dept_code from u_base_dw_ext within tp_header
integer x = 1856
integer y = 328
integer width = 581
integer height = 60
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_dept_code"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0) 
end event

event ue_dwndropdown;call super::ue_dwndropdown;DataWindowChild	ldw_DDDWChild

This.GetChild("dept_code", ldw_DDDWChild)
ldw_DDDWChild.Reset()
ldw_DDDWChild.ImportString(is_dept_codes)
		
end event

type dw_gtl_split_code from u_base_dw_ext within tp_header
integer x = 1906
integer y = 400
integer width = 325
integer height = 60
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_gtl_ind"
boolean controlmenu = true
boolean border = false
end type

event constructor;call super::constructor;//DataWindowChild			ldwc_temp
//
this.insertrow(0)
//dw_gtl_split_code.GetChild('gtl_split_code', ldwc_temp)
//
//IF ldwc_temp.RowCount()	= 0 THEN 
//	ldwc_temp.SetTransObject(SQLCA)
//	ldwc_temp.Retrieve()
//end if
end event

event itemchanged;call super::itemchanged;// added code for  gtl project ibdkdld
if data = "Y" Then 
	if not dw_cost_weight.getitemstring(1,"load_stat") = "U" Then
		This.setfocus()
		iw_frame.setmicrohelp("The Load Status must be uncombined for GTL Orders")
		return 1
	End If
End If

end event

event itemerror;call super::itemerror;// added code for  gtl project ibdkdld
return 2
end event

type dw_export_country from u_base_dw_ext within tp_header
integer x = 82
integer y = 636
integer width = 1189
integer height = 68
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_exp_country"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild			ldwc_temp

this.insertrow(0)
dw_export_country.GetChild('export_country', ldwc_temp)

IF ldwc_temp.RowCount()	= 0 THEN 
	ldwc_temp.SetTransObject(SQLCA)
	ldwc_temp.Retrieve()
end if
end event

type tp_instruction from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
long backcolor = 12632256
string text = "Instruction"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_permanent_instructions dw_permanent_instructions
mle_1 mle_1
end type

on tp_instruction.create
this.dw_permanent_instructions=create dw_permanent_instructions
this.mle_1=create mle_1
this.Control[]={this.dw_permanent_instructions,&
this.mle_1}
end on

on tp_instruction.destroy
destroy(this.dw_permanent_instructions)
destroy(this.mle_1)
end on

event rbuttondown;wf_rightclick()
end event

type dw_permanent_instructions from u_base_dw_ext within tp_instruction
integer y = 32
integer width = 1353
integer height = 588
integer taborder = 11
string dataobject = "d_instruction"
boolean border = false
end type

event ue_retrieve;call super::ue_retrieve;string	ls_Instruction_String, &
			ls_order_Instruction_In,&
			ls_Order_Instruction_Out
			
						
DataWindowChild	lddw_DDDWChild

SetPointer(Hourglass!)
This.SetRedraw(False)			
This.InsertRow(0)
This.GetChild('instr_type', lddw_DDDWChild)
wf_gettypedesc(lddw_DDDWChild)
IF Len(Trim(is_instruction_type)) < 1 THEN is_instruction_type = 'P' 
ls_Instruction_String = is_orderid + "~t" + Trim(is_instruction_type) + "~t" + "I"
istr_error_info.se_event_name = 'ue_instruction_inquire'
// Call a Netwise external function to get the customer instruction
//IF iu_orp003.nf_orpo37ar( istr_Error_Info, ls_Instruction_String, & 
//	ls_Order_Instruction_In, ls_order_Instruction_Out) < 0 Then 
//	This.SetRedraw(True)
//	Return
//End if

IF iu_ws_orp2.nf_orpo37fr(ls_Instruction_String, & 
	ls_Order_Instruction_In, ls_order_Instruction_Out,istr_Error_Info) < 0 Then 
	This.SetRedraw(True)
	Return
End if

is_MicroHelp = iw_frame.wf_getmicrohelp()
// Before importing the tab-delimited strings into the datawindows,
// reset them to have no rows in them
This.Reset()
IF This.ImportString(Trim(ls_instruction_String)) < 1 THEN  
  	InsertRow(0)
END IF
is_mle_instruction	=	Trim(ls_Order_Instruction_Out)
mle_1.TriggerEvent('ue_retrieve')
is_customer_info = dw_customer_info.GetItemString(1,"customer_id")+"S"
is_typecode = dw_customer_info.GetItemString(1,"smanlocaton")

wf_protect_order(dw_permanent_instructions)
This.SetRedraw(True)
iw_frame.SetMicroHelp( is_MicroHelp)
Return 
SetPointer(Arrow!)
end event

event itemchanged;call super::itemchanged;IF dwo.Name	=	'instr_type' THEN
	is_instruction_type	=	data
END IF
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

type mle_1 from u_base_multilineedit_ext within tp_instruction
event ue_retrieve ( )
event ue_update ( )
event ue_copy pbm_custom74
integer x = 1362
integer y = 268
integer width = 1518
integer height = 328
integer taborder = 2
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "Fixedsys"
long backcolor = 1090519039
end type

event ue_retrieve;call super::ue_retrieve;String	ls_instruction, &
			ls_business_rule
			
			
This.SetRedraw(False)
ls_business_rule = Right(is_mle_instruction, 1)
//SetItem(1, 'business_rules', ls_business_rule)

If ls_business_rule = 'V' Then 
	mle_1.enabled = false 
Else
	mle_1.enabled = true
End if

ls_instruction	=	left(is_mle_instruction, len(is_mle_instruction) - 1)
mle_1.Text = ls_instruction

SetMicroHelp(is_MicroHelp)

IF ib_moveCopy Then
	ib_moveCopy = False
	This.SetFocus()
	IF Len(Trim(mle_1.Text)) > 0 Then
	 IF MessageBox("Instructions", "You currently have instructions. Do you want to over write them?",Question!, YesNo!) = 1 Then
		This.Text = ClipBoard()
		wf_Update()
	 END IF
	ELSE 
		This.Text = ClipBoard()
		wf_Update()
	END IF
END IF
iw_This.Title = "Sales order Instruction - "+ is_orderid
SetFocus()
ib_instruction_Modified = False
This.SetRedraw(True)
end event

event ue_update();String	ls_instruct_key,&
			ls_order_Instruction_out,&
			ls_Order_Instruction_In,&
			ls_Inst_Type, &
			ls_MicroHelp
			
SetPointer(HourGlass!)
SetRedraw(False)
//ls_instruct_key = SPACE(9)
ls_Inst_Type	=	tab_1.tp_instruction.dw_permanent_instructions.GetItemString(1, 'instr_type')
IF Len( Trim( ls_Inst_Type)) < 1 Then
	ls_Inst_Type =  "P"
END IF

ls_instruct_key += is_orderid+"~t"
ls_instruct_key += ls_inst_Type+"~t"
ls_instruct_key += "U"

ls_Order_Instruction_In = wf_Reformat( mle_1.Text)


istr_error_info.se_event_name = 'ue_update on dw_instruction'

//ii_rpc_retvalue = iu_orp003.nf_orpo37ar( istr_error_info, ls_instruct_key, ls_Order_Instruction_In,  ls_order_Instruction_out)
ii_rpc_retvalue = iu_ws_orp2.nf_orpo37fr(ls_instruct_key, ls_Order_Instruction_In,  ls_order_Instruction_out, istr_error_info)
ls_MicroHelp = iw_frame.wf_getmicrohelp()

SetPointer(Arrow!)

IF ii_rpc_retvalue <  0 Then 
	This.SetRedraw( True)
	Return
ELSE
	choose case ii_rpc_retvalue
		case 2
			This.TriggerEvent('ue_retrieve')
//			SetMicroHelp( istr_error_info.se_message)
			return
		case else
			IF dw_permanent_instructions.ImportString(Trim(ls_instruct_key)) < 1 THEN  
				dw_permanent_instructions.InsertRow(0)
			END IF
			is_mle_instruction	=	Trim(ls_Order_Instruction_Out)
			This.TriggerEvent('ue_retrieve')
//			SetMicroHelp( is_MicroHelp)
	end choose
END IF

dw_customer_info.TriggerEvent('ue_retrieve')
dw_customer_info.ResetUpdate()

iw_frame.SetMicroHelp( ls_MicroHelp)

ib_Order_id_Changed_Detail = FALSE
ib_Order_id_Changed_Header = FALSE
ib_Order_id_Changed_Instruction = FALSE
ib_Order_id_Changed_Currency = FALSE
dw_customer_info.ib_updateable	=	False
dw_cost_weight.ib_updateable	=	False
//SetMicroHelp( is_MicroHelp)
SetRedraw(True)
RETURN 


end event

event ue_copy;call super::ue_copy;IF This.SelectedLength() > 0  Then
	This.Copy()
ELSE
	Clipboard(This.Text)
END IF
ib_moveCopy = TRUE
end event

event modified;call super::modified;ib_instruction_Modified = TRUE
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

type tp_currency from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
long backcolor = 12632256
string text = "Currency"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_currency dw_currency
end type

on tp_currency.create
this.dw_currency=create dw_currency
this.Control[]={this.dw_currency}
end on

on tp_currency.destroy
destroy(this.dw_currency)
end on

event rbuttondown;wf_rightclick()
end event

type dw_currency from u_base_dw_ext within tp_currency
integer width = 2903
integer height = 668
integer taborder = 11
string dataobject = "d_currency_detail"
end type

event ue_retrieve;call super::ue_retrieve;SetPointer(Hourglass!)
This.SetRedraw(False)
iw_This.Title = 'Sales Order Currency - '+ is_orderid
//If Not IsValid(iu_orp002) Then iu_orp002 = Create u_orp002
If Not IsValid(iu_ws_orp3) Then iu_ws_orp3 = Create u_ws_orp3
This.Reset()
//If Not iu_orp002.nf_orpo72ar_inq_exch_rate(istr_error_info, &
//												is_orderid, is_currency) Then 
If Not iu_ws_orp3.uf_orpo72fr_inq_exch_rate(istr_error_info, &
												is_orderid, is_currency) Then 	
	This.SetRedraw(True)
	RETURN
END IF

wf_protect_order(dw_currency)

This.ImportString(is_Currency)
is_customer_info = dw_customer_info.GetItemString(1,"customer_id")+"S"
is_typecode = dw_customer_info.GetItemString(1,"smanlocaton")
This.SetRedraw(True)
SetPointer(Arrow!)
end event

event constructor;call super::constructor;ib_updateable	=	False
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

type tp_confirm from userobject within tab_1
boolean visible = false
integer x = 18
integer y = 112
integer width = 3867
integer height = 948
boolean enabled = false
long backcolor = 12632256
string text = "Order Confirmation"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_confirmation dw_confirmation
end type

on tp_confirm.create
this.dw_confirmation=create dw_confirmation
this.Control[]={this.dw_confirmation}
end on

on tp_confirm.destroy
destroy(this.dw_confirmation)
end on

type dw_confirmation from u_base_dw_ext within tp_confirm
integer x = 50
integer y = 140
integer width = 2889
integer height = 692
integer taborder = 11
string dataobject = "d_order_confirmed_composite"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_retrieve;call super::ue_retrieve;DataWindowChild	ldwc_detail,&
						ldwc_Header,&
						ldwc_stop,&
						ldwc_Instruction

String	ls_Location,&
			ls_loc_code




Open( w_popup_confirmation)

ls_loc_code = dw_customer_info.GetItemString( 1, "smanlocaton")
SELECT service_centers.service_center_name
	INTO :ls_location
	FROM Service_centers
	WHERE Service_centers.service_center_code = :ls_Loc_code
	USING SQLCA ;

This.Object.sales_location_text.Text = ls_Location
This.GetChild("dw_header",ldwc_Header)
This.GetChild("dw_detail", ldwc_detail)
This.GetChild('dw_product_instruction', ldwc_Instruction)
This.Getchild('dw_stop',ldwc_stop)
//if NOT IsValid( iu_orp002) Then iu_orp002 = Create u_orp002
//iu_orp002.nf_orpo78br_get_order_confimation( dw_customer_info.GetItemString(1,"order_id"), ldwc_Stop, ldwc_header, &
//											ldwc_detail, ldwc_instruction,istr_error_info )
											
if NOT IsValid( iu_ws_orp4) Then iu_ws_orp4 = Create u_ws_orp4
iu_ws_orp4.nf_orpo78fr( dw_customer_info.GetItemString(1,"order_id"), ldwc_Stop, ldwc_header, &
								ldwc_detail, ldwc_instruction,istr_error_info )
											

wf_protect_order(dw_confirmation)

iw_frame.SetMicroHelp("Ready")
Close( w_popup_confirmation)
end event

type dw_cost_weight from u_base_dw_ext within w_sales_order_detail
integer y = 224
integer width = 3630
integer height = 416
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_cost_weight"
boolean border = false
boolean ib_scrollable = false
end type

event ue_retrieve;call super::ue_retrieve;This.Reset()
IF This.ImportString(is_cost_weight) < 1 THEN
	InsertRow(0)
	This.Reset()
ELSE
	dw_cost_weight.ResetUpdate()
END IF


end event

event constructor;call super::constructor;ib_updateable	=	False
InsertRow(0)
end event

event ue_postconstructor;call super::ue_postconstructor;datawindowchild	ldw_DDDWChild

This.GetChild("ship_plant", ldw_DDDWChild)
ldw_DDDWChild.SetTransObject(SQLCA)
ldw_DDDWChild.Retrieve()
This.GetChild("load_stat", ldw_DDDWChild)
ldw_DDDWChild.SetTransObject(SQLCA)
ldw_DDDWChild.Retrieve()


end event

event itemchanged;call super::itemchanged;// added code for  gtl project ibdkdld
IF dwo.name = "load_stat" Then
	if not data = "U" Then 
		if tab_1.tp_header.dw_gtl_split_code.getitemstring(1,"gtl_split_ind") = "Y" Then
			This.setfocus()
			iw_frame.setmicrohelp("The Load Status must be uncombined for GTL Orders")
			ib_updateable	=	False
			return 1
		End If
	End If
end If
//
ib_updateable	=	True
end event

event getfocus;call super::getfocus;//dw_cost_weight.SetColumn("delv_date")

end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event itemerror;call super::itemerror;// added code for  gtl project ibdkdld
return 2

end event

event clicked;call super::clicked;Long count
String ls_protected
IF Row > 0 Then
	Choose Case dwo.name
		Case 'linked_orders'
			OpenWithParm(w_linked_orders_popup, dw_customer_info.GetItemString(1, "order_id"))
		Case 'pricing_mass_override'
			dw_cost_weight.AcceptText()
			tab_1.tp_detail.dw_detail.AcceptText()
			tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'N')
			If dw_cost_weight.GetItemString(1, 'pricing_mass_override') = 'N' then
				For count = 1 to tab_1.tp_detail.dw_detail.RowCount()
					ls_protected = tab_1.tp_detail.dw_detail.GetItemString(count, "business_rules")
					If (Mid(ls_protected, 9, 1) =  'E' ) then
						tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'Y')
					end if
					If (Mid(ls_protected, 9, 1) =  'M' ) then
						tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'Y')
					end if
					If (Mid(ls_protected, 9, 1) =  'S' ) then
						tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'Y')
					end if
					
				Next
			else
				For count = 1 to tab_1.tp_detail.dw_detail.RowCount()
					ls_protected = tab_1.tp_detail.dw_detail.GetItemString(count, "business_rules")
					If (Mid(ls_protected, 9, 1) =  'E' ) then
						tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'N')
					end if
					If (Mid(ls_protected, 9, 1) =  'M' ) then
						tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'N')
					end if
					If (Mid(ls_protected, 9, 1) =  'S' ) then
						tab_1.tp_detail.dw_detail.SetItem(count, 'sales_price_ovrd_ind', 'N')
					end if
				Next
				
			End if
					
					
			
	End Choose
End If


end event

type dw_programs from u_base_dw_ext within w_sales_order_detail
integer x = 3214
integer y = 28
integer width = 549
integer height = 136
integer taborder = 21
string dataobject = "d_cust_programs"
boolean vscrollbar = true
end type

