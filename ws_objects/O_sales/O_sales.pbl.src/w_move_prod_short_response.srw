﻿$PBExportHeader$w_move_prod_short_response.srw
$PBExportComments$Response window for product move between orders and product short.
forward
global type w_move_prod_short_response from w_base_response_ext
end type
type st_1 from statictext within w_move_prod_short_response
end type
type st_2 from statictext within w_move_prod_short_response
end type
type dw_product_shortages from u_base_dw_ext within w_move_prod_short_response
end type
type cb_retry from commandbutton within w_move_prod_short_response
end type
end forward

global type w_move_prod_short_response from w_base_response_ext
integer x = 1134
integer y = 916
integer width = 1637
integer height = 1156
boolean titlebar = false
long backcolor = 12632256
st_1 st_1
st_2 st_2
dw_product_shortages dw_product_shortages
cb_retry cb_retry
end type
global w_move_prod_short_response w_move_prod_short_response

type variables
String	is_shortage_string
end variables

on w_move_prod_short_response.create
int iCurrent
call super::create
this.st_1=create st_1
this.st_2=create st_2
this.dw_product_shortages=create dw_product_shortages
this.cb_retry=create cb_retry
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.dw_product_shortages
this.Control[iCurrent+4]=this.cb_retry
end on

on w_move_prod_short_response.destroy
call super::destroy
destroy(this.st_1)
destroy(this.st_2)
destroy(this.dw_product_shortages)
destroy(this.cb_retry)
end on

event ue_base_ok;call super::ue_base_ok;CloseWithReturn(This,"OK")
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"Cancel")
destroy(w_move_prod_short_response)
end event

event open;call super::open;dw_product_shortages.ImportString(Message.StringParm)
dw_product_shortages.SetRedraw(True)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_move_prod_short_response
integer x = 1326
integer y = 488
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_move_prod_short_response
integer x = 1326
integer y = 348
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_move_prod_short_response
integer x = 1326
integer y = 56
end type

type cb_browse from w_base_response_ext`cb_browse within w_move_prod_short_response
end type

type st_1 from statictext within w_move_prod_short_response
integer x = 165
integer y = 20
integer width = 800
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Product move will result in shortages.  "
boolean focusrectangle = false
end type

type st_2 from statictext within w_move_prod_short_response
integer x = 247
integer y = 136
integer width = 576
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Do you wish to continue?"
boolean focusrectangle = false
end type

type dw_product_shortages from u_base_dw_ext within w_move_prod_short_response
integer x = 18
integer y = 260
integer width = 1138
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_move_rows_product_shortages"
boolean vscrollbar = true
end type

type cb_retry from commandbutton within w_move_prod_short_response
integer x = 1326
integer y = 200
integer width = 270
integer height = 108
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Retry"
end type

event clicked;CloseWithReturn(Parent, 'R')
end event

