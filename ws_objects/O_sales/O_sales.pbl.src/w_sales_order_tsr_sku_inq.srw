﻿$PBExportHeader$w_sales_order_tsr_sku_inq.srw
forward
global type w_sales_order_tsr_sku_inq from w_base_response_ext
end type
type dw_detail from u_base_dw_ext within w_sales_order_tsr_sku_inq
end type
type dw_sales_open from u_base_dw_ext within w_sales_order_tsr_sku_inq
end type
end forward

global type w_sales_order_tsr_sku_inq from w_base_response_ext
integer x = 55
integer y = 552
integer width = 2162
integer height = 980
string title = "Sales Orders by TSR SKU Inquire"
long backcolor = 12632256
dw_detail dw_detail
dw_sales_open dw_sales_open
end type
global w_sales_order_tsr_sku_inq w_sales_order_tsr_sku_inq

type variables
private:

w_sales_order_tsr_sku  iw_parent

String		is_openstring, &
                                is_type_ind

boolean                   ib_first_time

INTEGER	ii_Max_Rows = 99


end variables

forward prototypes
public function integer wf_check_box_prod (readonly integer ac_on_off)
public function integer wf_clean_products ()
public function integer wf_reset_prod_color (readonly integer ac_show)
public function boolean wf_first_time ()
end prototypes

public function integer wf_check_box_prod (readonly integer ac_on_off);//If ac_on_off = 1 Then
//	dw_sales_open.object.black_rec.visible = 1
//	dw_sales_open.object.white_rec.visible = 1
//	dw_sales_open.object.gray_rec.visible = 1
//	dw_sales_open.object.mGray_rec.visible = 1
//	dw_sales_open.object.all_prod_ind.visible = 0
//Else
//	dw_sales_open.object.black_rec.visible = 0
//	dw_sales_open.object.white_rec.visible = 0
//	dw_sales_open.object.gray_rec.visible = 0
//	dw_sales_open.object.mGray_rec.visible = 0
//	dw_sales_open.object.all_prod_ind.visible = 1
//End If
Return 0
end function

public function integer wf_clean_products ();Long		ll_RowCount, ll_index
String 	ls_value
Boolean	lb_finished
u_string_functions	lu_string_functions

lb_finished = False
ll_RowCount = dw_detail.RowCount()
ll_index = 1

Do
	ls_value = dw_detail.object.product_code[ll_index]
	If lu_string_functions.nf_isempty(ls_value) Then
		dw_detail.DeleteRow(ll_index)
		ll_RowCount = dw_detail.RowCount()
		ll_index = 1
	Else
		ll_index ++
	End If
	If ll_index > ll_RowCount Or ll_RowCount = 0 Then lb_finished = True
Loop Until lb_finished

RETURN ll_RowCount

end function

public function integer wf_reset_prod_color (readonly integer ac_show);Long		ll_RowCount, ll_index
String 	ls_value
Boolean	lb_finished
//u_string_functions	lu_string_functions

lb_finished = False
ll_RowCount = dw_detail.RowCount()
ll_index = 1

Do
	ls_value = dw_detail.object.detail_errors[ll_index]
	If IsNull(ls_value) Then ls_value = '  '
	Choose case ac_show
	case 0	
		ls_value = Right(ls_value,1)
		ls_value = ls_value + 'V'
	case 1
		ls_value = Left(ls_value,1)
		ls_value = ls_value + ls_value
	case 3
		ls_value = Left(ls_value,1)
		ls_value = ls_value + 'V'
	End Choose
	dw_detail.SetItem(ll_index,"detail_errors", ls_value)	
		
	ll_index ++
	If ll_index > ll_RowCount Or ll_RowCount = 0 Then lb_finished = True
Loop Until lb_finished

RETURN ll_RowCount
end function

public function boolean wf_first_time ();return ib_first_time
end function

event ue_postopen;Integer				li_rtn

If dw_sales_open.object.all_prod_ind[1] = 'N' Then
	dw_detail.Enabled = True
	//dw_detail.Object.product_code.BackGround.Color = 16777215
	li_rtn = wf_reset_prod_color(1)
Else
	dw_detail.Enabled = False
	//dw_detail.Object.product_code.BackGround.Color = 12632256
	li_rtn = wf_reset_prod_color(3)
End If
//
iw_parent.event ue_get_data("is_type_ind")
dw_sales_open.setitem(1, "short_queue", message.stringparm)
//
If ib_first_time Then
   dw_sales_open.SetFocus()
   dw_sales_open.SetColumn("cust_id")
   dw_Sales_open.SelectText( 1,10)
Else
	dw_detail.SetFocus()
	dw_detail.SetColumn("product_code")
	dw_detail.SelectText( 1,10)
End If

If dw_sales_open.object.short_queue[1] = 'N' Then
//	li_rtn = wf_check_box_prod(1)
	dw_detail.Enabled = True
Else
//	li_rtn = wf_check_box_prod(0)
End If

end event

event open;call super::open;String	ls_header_info, &
			ls_detail_info, &
			ls_first_time
DataWindowChild	ldwc_customer    
u_string_functions	lu_string_functions

iw_parent = message.PowerObjectparm
iw_parent.trigger event ue_get_data("is_openstring")
is_openstring = message.stringParm
iw_parent.trigger event ue_get_data("is_first_time")
ls_first_time = message.stringparm  
if ls_first_time = 'true' then
	ib_first_time = true
else
	ib_first_time = false
end if

If Not lu_string_functions.nf_IsEmpty(is_openstring) Then
	ls_header_info = iw_frame.iu_string.nf_gettoken( is_openstring, '~?')
	ls_detail_info = is_openstring
   dw_sales_open.ImportString(ls_header_info)
	dw_detail.ImportString(ls_detail_info)
End If
If Not lu_string_functions.nf_IsEmpty(ls_detail_info) Then
	dw_detail.DeleteRow(1)
End If
dw_detail.InsertRow(0)

dw_Sales_open.GetChild( "cust_id", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)
ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'customer_id', 'ALL')
ldwc_customer.SetItem(1, 'short_name', 'All Customers')

dw_sales_open.getchild( "tsr_id", ldwc_customer)
iw_frame.iu_project_functions.nf_gettsrs(ldwc_customer, message.is_smanlocation)

dw_sales_open.getchild( "div_code", ldwc_customer)
iw_frame.iu_project_functions.nf_gettutltype(ldwc_customer, "divcode")

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', 'PK')
ldwc_customer.SetItem(1, 'type_desc', 'ALL PORK DIVISIONS')

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', 'BF')
ldwc_customer.SetItem(1, 'type_desc', 'ALL BEEF DIVISIONS')

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', '  ')
ldwc_customer.SetItem(1, 'type_desc', 'ALL DIVISIONS')


end event

on w_sales_order_tsr_sku_inq.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_sales_open=create dw_sales_open
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_sales_open
end on

on w_sales_order_tsr_sku_inq.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_sales_open)
end on

event ue_base_ok;DataWindowChild	ldwc_Temp
String			ls_value, ls_temp, ls_returnstring
Integer			li_rtn
Long				ll_rtn
u_string_functions	lu_string_functions

dw_sales_open.AcceptText()
dw_detail.AcceptText()

ls_value = dw_sales_open.GetItemString(1, 'tsr_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('TSR is a required field.')
	Return
End If	
If Not isNumber(ls_value) Then
	iw_frame.SetMicroHelp('TSR Id Is Invalid.')
	Return
End If
//dw_sales_open.GetChild("tsr_id",ldwc_Temp)
//IF ldwc_Temp.Find("smancode = '"+ls_value+"'",1, ldwc_Temp.RowCount()) < 1 Then
//	iw_frame.SetMicrohelp('TSR is invalid')
//	dw_sales_open.SetColumn('tsr_id')
//	Return 
//END IF
/////////Customer all not allowed for 'N' for shortageQueue
//ls_value = dw_sales_open.GetItemString(1, 'cust_id')
//ls_temp = dw_sales_open.Object.short_queue[1]
//If (ls_value = 'ALL     ' Or ls_value = 'ALL') And ls_temp = 'N' Then
//	iw_frame.SetMicroHelp('ALL Customer Is Not A Valid Option When Shortage Queue Unchecked')
//	Return
//End If

//ls_value = dw_detail.Describe("DataWindow.Data")
ls_temp = dw_sales_open.Object.all_prod_ind[1]
If ls_temp = 'N' Then
	li_rtn = wf_clean_products()
	If li_rtn = 0 Then
		iw_frame.SetMicroHelp('No Products Are Specified')
		dw_detail.InsertRow(0)
		Return
	End If
End If
////checking date ranges
ll_rtn = DaysAfter(dw_sales_open.object.ship_date[1],dw_sales_open.object.ship_date_to[1])
If ll_rtn > 30 Then
	iw_frame.SetMicrohelp('Date Ranges are Greater Than 30 days')
	dw_sales_open.SetColumn("ship_date_to")
	dw_sales_open.SelectText(4,2)
	Return
End If
If ll_rtn < 0 Then
	iw_frame.SetMicrohelp('From Date Cannot Be Greater Than To Date')
	dw_sales_open.SetColumn("ship_date_to")
	dw_sales_open.SelectText(4,2)
	Return 
End If
////////
wf_clean_products()
iw_parent.event ue_set_data("is_type_ind", dw_sales_open.getitemstring(1, "short_queue"))



ls_returnstring = dw_sales_open.Describe("DataWIndow.Data") + "~?" + &
						Trim(dw_detail.Describe("DataWindow.Data"))
						
CloseWithReturn( This, ls_returnstring)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Abort")
end event

event close;call super::close;DataWindowChild	ldwc_customer    


dw_Sales_open.GetChild( "cust_id", ldwc_Customer)

ldwc_customer.DeleteRow(1)

ii_max_rows = 0
end event

event key;call super::key;IF keyflags = 0 THEN
	IF key = Keyenter! THEN
		GraphicObject which_control
		CommandButton cb_which
		string text_value
		which_control = GetFocus( ) 
		CHOOSE CASE TypeOf(which_control)
			CASE CommandButton!
				cb_which = which_control
				text_value = cb_which.Text
			CASE ELSE
				text_value = ""
		END CHOOSE
		IF text_value = '&OK' Then
			TriggerEvent("ue_base_ok")
		ElseIf text_value = '&Cancel' Then
			TriggerEvent("ue_base_cancel")
		End If
	END IF
END IF
end event

event ue_get_data;Choose Case as_value
	Case "type_ind"
		message.StringParm = is_type_ind
End Choose
return is_type_ind
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_sales_order_tsr_sku_inq
boolean visible = false
integer x = 1815
integer y = 396
integer taborder = 0
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_sales_order_tsr_sku_inq
integer x = 1847
integer y = 156
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_sales_order_tsr_sku_inq
integer x = 1847
integer y = 28
integer taborder = 30
boolean default = false
end type

type cb_browse from w_base_response_ext`cb_browse within w_sales_order_tsr_sku_inq
integer taborder = 0
end type

type dw_detail from u_base_dw_ext within w_sales_order_tsr_sku_inq
integer y = 260
integer width = 891
integer height = 624
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_prod_order_inq"
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;Long	ll_row, &
		ll_RowCount


ll_RowCount = This.RowCount()

IF row > ii_max_rows THEN 
	MessageBox("Max Limit","You are only allowed to enter "+String(ii_max_rows)+" Products")
	RETURN 2
End If

If row + 1 = ll_RowCount and ll_RowCount < ii_max_rows Then 
	This.InsertRow(0)
End If
end event

event constructor;call super::constructor;ib_NewRowOnChange = True
This.InsertRow(0)
end event

event ue_setbusinessrule;call super::ue_setbusinessrule;String ls_BusinessRule
Long		ll_Column

If row <= this.RowCount() Then
	Choose Case name
		Case 'product_code'
		 	ll_column = 2
	END CHOOSE
	ls_businessRule =This.GetItemString(row,"detail_errors")
	if IsNull(ls_BusinessRule) Then ls_BusinessRule = '  '
	ls_BusinessRule = Left(ls_BusinessRule,ll_Column -1) &
							+ Value + Mid(ls_BusinessRule,ll_Column +1)
							
	This.SetItem(row,"detail_errors", ls_BusinessRule)	
	Return 1
End If
Return 1
end event

event losefocus;call super::losefocus;Long	ll_RowCount

ll_RowCount = This.RowCount()
IF ll_RowCount > ii_max_rows THEN
	this.deleteRow(ll_RowCount)
End IF
end event

type dw_sales_open from u_base_dw_ext within w_sales_order_tsr_sku_inq
event ue_postitemchanged ( )
integer width = 1760
integer height = 508
integer taborder = 10
string dataobject = "d_sales_order_tsr_sku_inq"
boolean border = false
end type

event ue_postitemchanged;call super::ue_postitemchanged;This.SetItem(1, 'div_code', '  ')

end event

event itemchanged;call super::itemchanged;Integer				li_rtn
Long					ll_rtn
DataWindowChild	ldwc_Temp

u_string_functions	lu_string_functions

CHOOSE CASE This.GetColumnName()
CASE "tsr_id"
	If Not isNumber(data) Then
		iw_frame.SetMicroHelp('TSR Id Is Invalid.')
		Return 1
	End If
CASE "cust_id"
	If lu_string_functions.nf_isempty(data) Then
	iw_frame.SetMicroHelp('Customer Id is a required field.')
		Return 1
	End If	
Case 'div_code'
	If data = 'ALL DIVISIONS' Then
		This.PostEvent('ue_postitemchanged')
	End IF
	dw_sales_open.GetChild("div_code",ldwc_Temp)
	IF ldwc_Temp.Find("type_code = '"+data+"'",1, ldwc_Temp.RowCount()) < 1 then
		iw_frame.SetMicrohelp('Division code is invalid')
		This.SetColumn('div_code')
		Return 1
	END IF
Case 'all_prod_ind'
	If data = 'Y' Then
		dw_detail.RowsDiscard(1,dw_detail.RowCount(),Primary!)
		dw_detail.insertRow(0)
		dw_detail.insertRow(0)
		dw_detail.Enabled = False
		li_rtn = wf_reset_prod_color(0)
	Else
		dw_detail.Enabled = True
		li_rtn = wf_reset_prod_color(1)
	End IF
Case 'type_ind'
	IF data = 'A' or data = 'B' or data = 'Q' then
		
	else
		iw_frame.SetMicrohelp('Window type must be selected.')
		this.SetColumn('type_ind')
		Return 1
	End IF
Case 'ship_date_to'
	ll_rtn = DaysAfter(this.object.ship_date[1],date(data))
	If ll_rtn > 30 Then
		iw_frame.SetMicrohelp('Date Ranges are Greater Than 30 days')
		this.SetColumn("ship_date_to")
		this.SelectText(4,2)
		Return 1
	End If
	If ll_rtn < 0 Then
		iw_frame.SetMicrohelp('From Date Cannot Be Greater Than To Date')
		this.SetColumn("ship_date_to")
		this.SelectText(4,2)
		Return 1
	End If
//Case 'tsr_id'
END CHOOSE
iw_frame.setmicrohelp('Ready')









end event

event itemerror;call super::itemerror;Choose Case This.GetColumnName()
	Case 'div_code'
		iw_frame.SetMicrohelp('Division code is invalid - Select a valid Division')
		Return 1
	Case Else
		Return 1
End Choose

Return 1
end event

event constructor;call super::constructor;this.Object.all_prod_name.Visible = True
this.Object.all_prod_ind.Visible = True
this.Object.to_text_field.Visible = True
this.Object.ship_date_to.Visible = True
this.Object.date_label.Text = 'From:'
this.Object.date_type.Visible = True


end event

