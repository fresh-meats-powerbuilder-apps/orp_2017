﻿$PBExportHeader$w_edi_apply_changes.srw
forward
global type w_edi_apply_changes from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_edi_apply_changes
end type
type dw_edi_change_hdr from u_base_dw_ext within w_edi_apply_changes
end type
end forward

global type w_edi_apply_changes from w_base_sheet_ext
integer width = 4046
integer height = 1684
string title = "Sales EDI Change PO Review/Update"
dw_detail dw_detail
dw_edi_change_hdr dw_edi_change_hdr
end type
global w_edi_apply_changes w_edi_apply_changes

type variables
String	is_openingstring, &
			is_selected_customer, &
			is_selected_div_po, &
			is_selected_ts, &
			is_initial_reference 
			
Date		idt_selected_po_date		

//u_orp002		iu_orp002
u_ws_orp1		iu_ws_orp1

s_error	istr_error_info

application 	ia_apptool

w_tooltip_instr	iw_tooltip_instr 

Window		iw_This
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();String	ls_header_string_in, &
			ls_header_string_out, &
			ls_detail_string_in, &
			ls_detail_string_out
		
Integer	li_rtn		

u_string_functions	lu_string_functions 
			
This.TriggerEvent('closequery')

is_openingstring = is_initial_reference

istr_error_info.se_event_name = "wf_retrieve"		

//li_rtn = iu_orp002.nf_orpo24br_edi_apply_changes_inq_upd(istr_error_info, &
//																is_openingstring + 'I' + '~t', &
//																ls_header_string_in, &
//																ls_header_string_out, &
//																ls_detail_string_in, &
//																ls_detail_string_out)
																
li_rtn = iu_ws_orp1.nf_orpo24fr(istr_error_info, &
											is_openingstring + 'I' + '~t', &
											ls_header_string_in, &
											ls_header_string_out, &
											ls_detail_string_in, &
											ls_detail_string_out)																
																								
dw_edi_change_hdr.Reset()
dw_edi_change_hdr.ImportString(ls_header_string_out)

dw_detail.Reset()
dw_detail.ImportString(ls_detail_string_out)

Return True
end function

public function boolean wf_update ();Integer li_rtn

String	ls_header_string_in, &
			ls_header_string_out, &
			ls_detail_string_in, &
			ls_detail_string_out, &
			ls_so_id
		
Window		lw_detail		

istr_error_info.se_event_name = "wf_retrieve"	

ls_header_string_in = dw_edi_change_hdr.Describe("DataWindow.Data")
ls_detail_string_in = dw_detail.Describe("DataWindow.Data")
//
//li_rtn = iu_orp002.nf_orpo24br_edi_apply_changes_inq_upd(istr_error_info, &
//																is_openingstring + 'U' + '~t', &
//																ls_header_string_in, &
//																ls_header_string_out, &
//																ls_detail_string_in, &
//																ls_detail_string_out)
li_rtn = iu_ws_orp1.nf_orpo24fr(istr_error_info, &
											is_openingstring + 'U' + '~t', &
											ls_header_string_in, &
											ls_header_string_out, &
											ls_detail_string_in, &
											ls_detail_string_out)
																
If li_rtn = 0 Then
	dw_edi_change_hdr.Reset()
	dw_edi_change_hdr.ImportString(ls_header_string_out)
	dw_detail.Reset()
	dw_detail.ImportString(ls_detail_string_out)
End If

If Not isnull(dw_edi_change_hdr.GetItemString(1, "sales_order")) Then
	ls_so_id = dw_edi_change_hdr.GetItemString(1, "sales_order")
Else
	ls_so_id = '     '
End If

if (ls_so_id > '     ') and (li_rtn = 0) then
	ls_so_id = ls_so_id + '~t' + 'I'
	OpenSheetWithParm(lw_detail, ls_so_id, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)
end if
					
Return True
end function

on w_edi_apply_changes.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_edi_change_hdr=create dw_edi_change_hdr
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_edi_change_hdr
end on

on w_edi_apply_changes.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_edi_change_hdr)
end on

event open;call super::open;is_initial_reference = Message.StringParm

iw_this = This
end event

event ue_postopen;call super::ue_postopen;//iu_orp002 = CREATE u_orp002
iu_ws_orp1 = Create	u_ws_orp1

wf_retrieve()
end event

event close;call super::close;//If IsValid( iu_Orp002) Then Destroy( iu_orp002)
Destroy iu_ws_orp1
end event

event ue_getdata;call super::ue_getdata;STRING	ls_indicator, ls_rpc_message

//long		ll_window_x,ll_window_y, &
			
Choose Case as_stringvalue
	case	"Instructions"
		Message.StringParm = dw_edi_change_hdr.GetItemString(1,"instructions")
End Choose

end event

type dw_detail from u_base_dw_ext within w_edi_apply_changes
integer x = 32
integer y = 836
integer width = 3950
integer height = 704
integer taborder = 20
string dataobject = "d_edi_apply_change_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_edi_change_hdr from u_base_dw_ext within w_edi_apply_changes
integer x = 32
integer y = 24
integer width = 3945
integer height = 804
integer taborder = 10
string dataobject = "d_edi_apply_change_hdr"
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_order_status

This.GetChild('order_status_old', ldwc_order_status)
ldwc_order_status.SetTransObject(SQLCA)
ldwc_order_status.Retrieve()

This.GetChild('order_status_new', ldwc_order_status)
ldwc_order_status.SetTransObject(SQLCA)
ldwc_order_status.Retrieve()

//Delete 1st row from new status, don't want a space to show as Incomplete

ldwc_order_status.DeleteRow(1)
end event

event doubleclicked;call super::doubleclicked;STRING							ls_SalesID, ls_ColumnName 

Long								ll_row

Window							lw_detail

w_sales_order_detail			lw_Temp

// This has to be clickedrow because if you are are double clicking on 
// a protected row the row focus does change
ll_row = row

// You must get clicked column because order ID is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name

CHOOSE CASE ls_ColumnName
CASE	"sales_order"
	ls_SalesID = This.GetItemString(ll_row, "sales_order")
	ls_SalesID = ls_SalesID + '~t' + 'I'
	OpenSheetWithParm(lw_detail, ls_SalesID, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)
END CHOOSE
end event

event ue_mousemove;call super::ue_mousemove;String	ls_description, &
			ls_product, &
			ls_line_number, &
			ls_find_string
			
long		ll_rowcount

ia_apptool = GetApplication()
//il_xpos = gw_netwise_frame.PointerX() + 50
//il_ypos = gw_netwise_frame.PointerY() + 50
IF ia_apptool.ToolBarTips = TRUE THEN
	If row > 0  Then
		choose case dwo.name
			case 'instruction_ind'
					openwithparm(iw_tooltip_instr, This.GetItemString(1,"instructions"), iw_frame)
		end choose
	END IF    
END IF
end event

