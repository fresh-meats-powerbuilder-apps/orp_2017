﻿$PBExportHeader$w_product_shortage_queue.srw
forward
global type w_product_shortage_queue from w_base_response_ext
end type
type dw_main from datawindow within w_product_shortage_queue
end type
type dw_detail from u_base_dw_ext within w_product_shortage_queue
end type
type cb_1 from commandbutton within w_product_shortage_queue
end type
type dw_excluded_plants from u_base_dw_ext within w_product_shortage_queue
end type
type st_1 from statictext within w_product_shortage_queue
end type
type st_2 from statictext within w_product_shortage_queue
end type
end forward

global type w_product_shortage_queue from w_base_response_ext
integer x = 23
integer y = 424
integer width = 2423
integer height = 1224
string title = "Product Shortage Queue"
long backcolor = 12632256
dw_main dw_main
dw_detail dw_detail
cb_1 cb_1
dw_excluded_plants dw_excluded_plants
st_1 st_1
st_2 st_2
end type
global w_product_shortage_queue w_product_shortage_queue

type variables
Private:
s_error		istr_error_info

// This is a handle to the window that opened it if any
w_base_sheet	iw_ParentWindow

String	is_detail_data, &
	is_orderid, &
	is_shortage_ind, &
	is_rpc_message, &
	is_update_mode

u_ws_orp4		iu_ws_orp4
u_orp204		iu_orp204
end variables

forward prototypes
public subroutine wf_retrieve_plants ()
public subroutine wf_update_plants ()
end prototypes

public subroutine wf_retrieve_plants ();string		ls_process_option, &
				ls_order_id, &
				ls_customer_id, &
				ls_customer_type, &
				ls_input_string, &
				ls_output_string
				
integer		li_rtn, &
				li_start_pos

long			ll_rowcount, &
				ll_rowfound

ls_process_option = 'R'

if iw_ParentWindow.Title = 'Sales Orders by Customer' then
	iw_ParentWindow.Event ue_get_data('Order_ID')
	ls_order_id = Message.StringParm
	iw_ParentWindow.Event ue_get_data('customer_info')
	ls_customer_id = Message.StringParm
else
	iw_ParentWindow.Event ue_get_data('Order_ID')
	ls_order_id = Message.StringParm
	iw_ParentWindow.Event ue_get_data('customer_id')
	ls_customer_id = Message.StringParm
end if

ls_customer_type = 'S'

dw_excluded_plants.Reset()
//

li_rtn = iu_ws_orp4.nf_orpo87fr( istr_error_info, &
											  ls_process_option, &	
											  ls_order_id, &
											  ls_customer_id, &
											  ls_customer_type, &
											  ls_input_string, &
											  ls_output_string)															  
															  
															  
															  
															  
li_Rtn = dw_excluded_plants.ImportString( ls_output_string)	

ll_rowcount = dw_excluded_plants.RowCount()
li_start_pos = 1

ll_rowfound = dw_excluded_plants.Find("exclude_ind = 'Y'",li_start_pos,ll_rowcount)
DO WHILE (ll_rowfound > 0) 
	dw_excluded_plants.SelectRow(ll_rowfound,TRUE)
	dw_excluded_plants.SetItem(ll_rowfound,"update_flag",'U') 
	li_start_pos ++ 
	if li_start_pos > ll_rowcount THEN EXIT
	ll_rowfound = dw_excluded_plants.Find("exclude_ind = 'Y'",li_start_pos,ll_rowcount)
LOOP

Return
end subroutine

public subroutine wf_update_plants ();string		ls_process_option, &
				ls_order_id, &
				ls_customer_id, &
				ls_customer_type, &
				ls_input_string, &
				ls_output_string
				
integer		li_rtn, &
				li_start_pos

long			ll_rowcount, &
				ll_rowfound

u_string_functions lu_string_functions 

ls_process_option = 'U'

if iw_ParentWindow.Title = 'Sales Orders by Customer' then
	iw_ParentWindow.Event ue_get_data('Order_ID')
	ls_order_id = Message.StringParm
	iw_ParentWindow.Event ue_get_data('customer_info')
	ls_customer_id = Message.StringParm
else
	iw_ParentWindow.Event ue_get_data('Order_ID')
	ls_order_id = Message.StringParm
	iw_ParentWindow.Event ue_get_data('customer_id')
	ls_customer_id = Message.StringParm
end if

ls_customer_type = 'S'

ls_input_string = lu_string_functions.nf_BuildUpdateString(dw_excluded_plants)
															  
li_rtn = iu_ws_orp4.nf_orpo87fr( istr_error_info, &
											  ls_process_option, &	
											  ls_order_id, &
											  ls_customer_id, &
											  ls_customer_type, &
											  ls_input_string, &
											  ls_output_string)															  
															  
															  
Return
end subroutine

event open;call super::open;IF IsValid(Message.PowerObjectParm) THEN
	IF Message.PowerObjectParm.TypeOf() = Window! THEN
		iw_parentWindow = Message.PowerObjectParm
	End IF
END IF

//dw_main.InsertRow(0)
dw_main.TriggerEvent("ue_optionchanged")
end event

on w_product_shortage_queue.create
int iCurrent
call super::create
this.dw_main=create dw_main
this.dw_detail=create dw_detail
this.cb_1=create cb_1
this.dw_excluded_plants=create dw_excluded_plants
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_excluded_plants
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_2
end on

on w_product_shortage_queue.destroy
call super::destroy
destroy(this.dw_main)
destroy(this.dw_detail)
destroy(this.cb_1)
destroy(this.dw_excluded_plants)
destroy(this.st_1)
destroy(this.st_2)
end on

event ue_base_ok;////stringing the line number to send to shortage queue
Int	li_retn

String   ls_order_id, &
			ls_process_option, &
			ls_detail_data_in, &
			ls_detail_data_out, &
			ls_detail_temp, &
			ls_temp, ls_shortage

Long 		ll_row

Char		lc_send_to_sched_Ind

s_Error	lstr_Error

ls_temp = dw_main.GetItemString(1, "option")
			
ls_process_option = ls_temp
ls_order_id = is_orderid 

ll_row = dw_detail.Getselectedrow(0)
If ll_row <= 0 Then
	iw_frame.SetMicroHelp("No Lines Selected")
	Return
End If
// string line numbers
Do While ll_row > 0
	ls_detail_temp = dw_detail.Getitemstring(ll_row, 'order_detail_number')
	is_detail_data += ls_detail_temp + '~t'
	ll_row = dw_detail.Getselectedrow(ll_row)
LOOP

IF ls_process_option = 'R' THEN
	ls_detail_data_in = is_detail_data
	OpenWithParm(w_resolve_shortages, THIS)
ELSE
	ls_detail_data_in = is_detail_data
End IF

	lstr_Error.se_app_name  = "orp"
	lstr_Error.se_window_name = "w_product_shortage_queue"
	lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

	iw_frame.SetMicroHelp( "Wait...Connecting to the Mainframe")
	IF Not IsValid( iu_ws_orp4) Then 	iu_ws_orp4 = Create u_ws_orp4

	dw_detail.SetRedraw(FALSE)
	dw_detail.reset()

	li_retn = iu_ws_orp4.nf_orpo82fr(istr_error_info, &
											ls_process_option, &
											ls_order_id, &
											ls_detail_data_in, &
											ls_detail_data_out, &
											lc_send_to_sched_ind)
	
	dw_detail.ImportString(ls_detail_data_out)
	dw_detail.SetRedraw(TRUE)
	
	This.wf_update_plants()
	
	If li_retn = 3 Then
		is_rpc_message = iw_frame.wf_getmicrohelp()
		Close(THIS)
	End If
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "ABORT")

end event

event ue_postopen;String	ls_temp, &
		   ls_order_id, &
			ls_process_option, &
			ls_detail_data_in, &
			ls_detail_data_out
			
Int	li_retn

Long 	ll_row, &
		ll_FindRow

Char	lc_send_to_sched_ind

u_string_functions	lu_string_function

////sales order by customer window
iw_ParentWindow.Event ue_get_data('Order_ID')
is_orderid = Message.StringParm
dw_main.SetItem(1, 'order_id', is_orderid)

/////sales order detail window
iw_ParentWindow.Event ue_get_data('indicator')
is_shortage_ind = Message.StringParm
			
ls_process_option = 'I'
ls_order_id = is_orderid
ls_detail_data_in = ''

THIS.SetRedraw(FALSE)

IF Not IsValid( iu_ws_orp4) Then 	iu_ws_orp4 = Create u_ws_orp4

li_retn = iu_ws_orp4.nf_orpo82fr(istr_error_info, &
											ls_process_option, &
											ls_order_id, &
											ls_detail_data_in, &
											ls_detail_data_out, &
											lc_send_to_sched_ind)
											
if lc_send_to_sched_ind = 'Y' then
	dw_main.SetValue("option",1, 'Send to Scheduling Queue' + '~t' + 'S')
	dw_main.SetValue("option",2, 'Accept As Is' + '~t' + 'A')
	dw_main.SetValue("option",3, 'Resolve Via Reservations' + '~t' + 'R')
	dw_main.SetItem(1, 'option','S')
else
	dw_main.SetValue("option",1, 'Accept As Is' + '~t' + 'A')
	dw_main.SetValue("option",2, 'Resolve Via Reservations' + '~t' + 'R')
	dw_main.SetItem(1, 'option','A')
end if										

dw_detail.ImportString(ls_detail_data_out)

ll_row = dw_detail.RowCount()

ll_FindRow = 0

THIS.SetRedraw(TRUE)
If li_retn = 2 Then
	cb_base_ok.enabled = false
	is_update_mode = 'DISABLED'
Else
	is_update_mode = 'ENABLED'
End if
IF dw_main.GetITemString(1, "select_all") = 'Y' THEN
	dw_detail.SelectRow(0, TRUE)
	DO 
		ll_FindRow++
		ll_FindRow = dw_detail.Find("queue_ind = 'P'", ll_FindRow, ll_row + 1)
		IF ll_findRow > 0 THEN 
			dw_detail.SelectRow(ll_FindRow, FALSE)
		END IF
	LOOP WHILE ll_FindRow > 0
ELSE
	dw_detail.SelectRow(0, FALSE)
END IF

wf_retrieve_plants()
end event

event close;call super::close;Destroy iu_orp204
Destroy iu_ws_orp4
end event

event closequery;Int	li_retn

String   ls_order_id, &
			ls_process_option, &
			ls_detail_data_in, &
			ls_detail_data_out

Char		lc_send_to_sched_Ind

If is_update_mode = 'ENABLED' Then
	ls_process_option = 'C'
	ls_order_id = is_orderid
	ls_detail_data_in = ''
	
	IF Not IsValid( iu_ws_orp4) Then 	iu_ws_orp4 = Create u_ws_orp4
	
	li_retn = iu_ws_orp4.nf_orpo82fr(istr_error_info, &
												ls_process_option, &
												ls_order_id, &
												ls_detail_data_in, &
												ls_detail_data_out, &
												lc_send_to_sched_ind)
	iw_Frame.SetMicroHelp(is_rpc_message)
Else
	iw_Frame.SetMicroHelp('Ready')
End If
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case "Order_ID"
		Message.StringParm = is_orderid
//	CAse "detail"
//		Message.StringParm = is_detail
//	Case "customer_id"
//		Message.StringParm = dw_customer_info.GetITemString(1, "customer_id") 
//	Case "delivery_date"
//		Message.StringParm =  String(dw_cost_weight.GetItemDate(1,"delv_date"),"MM/DD/YYYY")
//	Case "ship_date"
//		Message.StringParm =  String(dw_cost_weight.GetItemDate(1,"ship_date"),"MM/DD/YYYY")
//	Case "division_code"
//		Message.StringParm =  dw_customer_info.GetITemString(1, "division_code") 
//	Case "salesperson_code"
//		Message.StringParm =  dw_customer_info.GetITemString(1, "smancode") 
//	Case "ship_plant"
//		Message.StringParm =  dw_cost_weight.GetITemString(1, "ship_plant")
	Case "detail_data"
		Message.StringParm = is_detail_data
//	CASE "indicator"
//		Message.StringParm = is_shortage_ind
End Choose
return message.stringparm
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_product_shortage_queue
integer x = 2094
integer y = 280
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_product_shortage_queue
integer x = 2094
integer y = 156
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_product_shortage_queue
integer x = 2094
integer y = 28
integer taborder = 40
end type

type cb_browse from w_base_response_ext`cb_browse within w_product_shortage_queue
integer taborder = 70
end type

type dw_main from datawindow within w_product_shortage_queue
event ue_lbuttondown pbm_lbuttondown
event ue_lbuttonup pbm_lbuttonup
event ue_optionchanged pbm_custom23
event ue_postconstructor ( )
integer x = 9
integer y = 12
integer width = 1019
integer height = 316
integer taborder = 10
string dataobject = "d_prod_shtg_que_header"
boolean border = false
boolean livescroll = true
end type

event ue_lbuttondown;//STRING	ls_ObjectString, &
//			ls_ObjectName
//
//ls_ObjectString = This.GetObjectAtPointer()
//ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)
//
//CHOOSE CASE UPPER(ls_ObjectName)
//CASE	"OK", "CANCEL"
//	This.Modify(ls_ObjectName + ".Border='5'")
//	This.PostEvent("ue_" + ls_ObjectName)
//END CHOOSE
end event

event ue_lbuttonup;//STRING	ls_ObjectString, &
//			ls_ObjectName
//
//ls_ObjectString = This.GetObjectAtPointer()
//ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)
//
//CHOOSE CASE ls_ObjectName
//CASE	"Ok", "Cancel"
//	This.Modify(ls_ObjectName + ".Border='6'")
//END CHOOSE
end event

event ue_optionchanged;CHOOSE CASE	This.GetItemString(1, "option")
CASE "S"
	iw_Frame.SetMicroHelp("Allow scheduling to Handle shortages.")
	parent.dw_excluded_plants.visible = False
	parent.st_1.Visible = False
	parent.st_2.Visible = False
CASE "A"
	iw_Frame.SetMicroHelp("Apply scheduled quantity to ordered quantity.")
	parent.dw_excluded_plants.visible = False
	parent.st_1.Visible = False
	parent.st_2.Visible = False
CASE "R"
	iw_Frame.SetMicroHelp("Release product from reservations.")
	parent.dw_excluded_plants.visible = False
	parent.st_1.Visible = False
	parent.st_2.Visible = False
CASE "Q"
	iw_Frame.SetMicroHelp("Remove product from Shortage Queue.")
	parent.dw_excluded_plants.visible = True
	parent.st_1.Visible = True
	parent.st_2.Visible = True
END CHOOSE
end event

event itemchanged;Long 	ll_row, &
		ll_FindRow
		

String	ls_temp

ll_row = dw_detail.RowCount()

ll_FindRow = 0

CHOOSE CASE THIS.GetColumnName()
CASE	"option"
	THIS.PostEvent("ue_optionchanged")
CASE "select_all"
	IF Data = 'Y' THEN 
		dw_detail.SelectRow(0, TRUE)
		DO 
			ll_FindRow++
			ll_FindRow = dw_detail.Find("queue_ind = 'P'", ll_FindRow, ll_row + 1)
			IF ll_findRow > 0 THEN 
				dw_detail.SelectRow(ll_FindRow, FALSE)
			END IF
		LOOP WHILE ll_FindRow > 0
	ELSE
		dw_detail.SelectRow(0, FALSE)
	END IF
END CHOOSE
end event

event constructor;THIS.InsertRow(0)
This.ClearValues('option')
THIS.TriggerEvent('open')


end event

type dw_detail from u_base_dw_ext within w_product_shortage_queue
integer x = 201
integer y = 552
integer width = 2034
integer height = 520
integer taborder = 30
string dataobject = "d_sales_detail_shortage"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String ls_temp
IF Row < 1 THEN RETURN

dw_main.SetItem(1, "select_all", 'N')

IF NOT This.IsSelected(Row) THEN
	Call Super::Clicked
	THIS.SelectRow(row, TRUE)
ELSE
  	THIS.SelectRow(row, FALSE)
END IF
ls_temp = dw_detail.object.queue_ind [row]
IF  ls_temp = 'P' THEN
	THIS.SelectRow(row, FALSE)
END IF 
end event

event constructor;call super::constructor;//THIS.SetRedraw(TRUE)


end event

type cb_1 from commandbutton within w_product_shortage_queue
integer x = 2094
integer y = 28
integer width = 270
integer height = 108
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Remove"
end type

type dw_excluded_plants from u_base_dw_ext within w_product_shortage_queue
boolean visible = false
integer x = 1093
integer y = 152
integer width = 965
integer height = 392
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_excluded_plants"
boolean vscrollbar = true
end type

event clicked;call super::clicked;IF Row > 0 THEN
	This.SelectRow( row, Not(This.IsSelected( row)))
	This.SetItem(row, "update_flag", 'U')
	IF This.IsSelected(row) = TRUE THEN
		This.SetItem(row, "exclude_ind", 'Y')
	ELSE
		This.SetItem(row, "exclude_ind", 'N')
	END IF
END IF
end event

type st_1 from statictext within w_product_shortage_queue
boolean visible = false
integer x = 1321
integer y = 12
integer width = 498
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Excluded Plants for "
boolean focusrectangle = false
end type

type st_2 from statictext within w_product_shortage_queue
boolean visible = false
integer x = 1353
integer y = 76
integer width = 434
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Shortage Queue:"
boolean focusrectangle = false
end type

