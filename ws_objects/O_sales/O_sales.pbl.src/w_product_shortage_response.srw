﻿$PBExportHeader$w_product_shortage_response.srw
forward
global type w_product_shortage_response from w_base_response_ext
end type
type dw_main from datawindow within w_product_shortage_response
end type
type dw_detail from u_base_dw_ext within w_product_shortage_response
end type
type dw_excluded_plants from u_base_dw_ext within w_product_shortage_response
end type
type st_1 from statictext within w_product_shortage_response
end type
type st_2 from statictext within w_product_shortage_response
end type
end forward

global type w_product_shortage_response from w_base_response_ext
integer x = 23
integer y = 424
integer width = 2363
integer height = 1168
string title = "Product Shortage Response"
long backcolor = 12632256
dw_main dw_main
dw_detail dw_detail
dw_excluded_plants dw_excluded_plants
st_1 st_1
st_2 st_2
end type
global w_product_shortage_response w_product_shortage_response

type variables
Private:
s_error		istr_error_info

// This is a handle to the window that opened it if any
w_base_sheet	iw_ParentWindow

String	is_detail_data

u_orp204		iu_orp204
u_ws_orp4		iu_ws_orp4

end variables

forward prototypes
public subroutine wf_retrieve_plants ()
public subroutine wf_update_plants ()
end prototypes

public subroutine wf_retrieve_plants ();string		ls_process_option, &
				ls_order_id, &
				ls_customer_id, &
				ls_customer_type, &
				ls_input_string, &
				ls_output_string
				
integer		li_rtn, &
				li_start_pos

long			ll_rowcount, &
				ll_rowfound

ls_process_option = 'C'

iw_ParentWindow.Event ue_get_data('Order_ID')
ls_order_id = Message.StringParm

iw_ParentWindow.Event ue_get_data('customer_id')
ls_customer_id = Message.StringParm

ls_customer_type = 'S'

dw_excluded_plants.Reset()

li_rtn = iu_ws_orp4.nf_orpo87fr( istr_error_info, &
										  ls_process_option, &	
										  ls_order_id, &
										  ls_customer_id, &
										  ls_customer_type, &
										  ls_input_string, &
										  ls_output_string)
															  
li_Rtn = dw_excluded_plants.ImportString( ls_output_string)	

ll_rowcount = dw_excluded_plants.RowCount()
li_start_pos = 1

ll_rowfound = dw_excluded_plants.Find("exclude_ind = 'Y'",li_start_pos,ll_rowcount)
DO WHILE (ll_rowfound > 0) 
	dw_excluded_plants.SelectRow(ll_rowfound,TRUE)
	dw_excluded_plants.SetItem(ll_rowfound, "update_flag", 'U') 
	li_start_pos ++ 
	if li_start_pos > ll_rowcount THEN EXIT
	ll_rowfound = dw_excluded_plants.Find("exclude_ind = 'Y'",li_start_pos,ll_rowcount)
LOOP

Return															  
															  
															  
															  
														  
																
																
end subroutine

public subroutine wf_update_plants ();string		ls_process_option, &
				ls_order_id, &
				ls_customer_id, &
				ls_customer_type, &
				ls_input_string, &
				ls_output_string
				
integer		li_rtn, &
				li_start_pos

long			ll_rowcount, &
				ll_rowfound

u_string_functions lu_string_functions 

ls_process_option = 'U'

iw_ParentWindow.Event ue_get_data('Order_ID')
ls_order_id = Message.StringParm

iw_ParentWindow.Event ue_get_data('customer_id')
ls_customer_id = Message.StringParm

ls_customer_type = 'S'

ls_input_string = lu_string_functions.nf_BuildUpdateString(dw_excluded_plants)

li_rtn = iu_ws_orp4.nf_orpo87fr( istr_error_info, &
										  ls_process_option, &	
										  ls_order_id, &
										  ls_customer_id, &
										  ls_customer_type, &
										  ls_input_string, &
										  ls_output_string)
															  
Return															  
end subroutine

event open;call super::open;IF IsValid(Message.PowerObjectParm) THEN
	IF Message.PowerObjectParm.TypeOf() = Window! THEN
		iw_parentWindow = Message.PowerObjectParm
	End IF
END IF

//iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4
end event

on w_product_shortage_response.create
int iCurrent
call super::create
this.dw_main=create dw_main
this.dw_detail=create dw_detail
this.dw_excluded_plants=create dw_excluded_plants
this.st_1=create st_1
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_excluded_plants
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.st_2
end on

on w_product_shortage_response.destroy
call super::destroy
destroy(this.dw_main)
destroy(this.dw_detail)
destroy(this.dw_excluded_plants)
destroy(this.st_1)
destroy(this.st_2)
end on

event ue_base_ok;//stringing the line number to send to shortage queue
String	ls_temp

Long 		ll_row

//ll_row = dw_detail.Getselectedrow(ll_row)
ll_row = dw_detail.Getselectedrow(0)
If ll_row <= 0 Then
	iw_frame.SetMicroHelp("No Lines Selected")
	Return
Else
	ls_temp = dw_main.GetItemString(1, "option") + '~t'
	Do While ll_row > 0
		ls_temp += dw_detail.Getitemstring(ll_row, 'order_detail_number') + '~t'
		ll_row = dw_detail.Getselectedrow(ll_row)
	LOOP
	IF dw_main.GetItemString(1, "option") = 'Q' then
		This.wf_update_plants()
	END IF
	CloseWithReturn(This, ls_temp)
End If
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "ABORT")

end event

event ue_postopen;char	lc_send_to_sched_ind

iw_ParentWindow.Event ue_get_data('detail_data')
is_detail_data = Message.StringParm
dw_detail.ImportString(is_detail_data)


iw_ParentWindow.Event ue_get_data('send_to_sched_ind')
lc_send_to_sched_ind = Message.StringParm

if lc_send_to_sched_ind = 'Y' then
	dw_main.SetValue("option",1, 'Send to Scheduling Queue' + '~t' + 'S')
	dw_main.SetValue("option",2, 'Accept As Is' + '~t' + 'A')
	dw_main.SetValue("option",3, 'Resolve Via Reservations' + '~t' + 'R')
	dw_main.SetValue("option",4, 'Send to Shortage Queue' + '~t' + 'Q')
	dw_main.SetItem(1, 'option','S')
else
	dw_main.SetValue("option",1, 'Accept As Is' + '~t' + 'A')
	dw_main.SetValue("option",2, 'Resolve Via Reservations' + '~t' + 'R')
	dw_main.SetValue("option",3, 'Send to Shortage Queue' + '~t' + 'Q')
	dw_main.SetItem(1, 'option','A')
end if

IF dw_main.GetITemString(1, "select_all") = 'Y' THEN
	dw_detail.SelectRow(0, TRUE)
ELSE
	dw_detail.SelectRow(0, FALSE)
END IF

This.wf_retrieve_plants()


end event

event close;//Destroy( iu_orp204)
Destroy( iu_ws_orp4)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_product_shortage_response
integer x = 2057
integer y = 280
integer width = 256
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_product_shortage_response
integer x = 2057
integer y = 156
integer width = 256
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_product_shortage_response
integer x = 2057
integer y = 28
integer width = 256
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_product_shortage_response
integer taborder = 60
end type

type dw_main from datawindow within w_product_shortage_response
event ue_lbuttondown pbm_lbuttondown
event ue_lbuttonup pbm_lbuttonup
event ue_optionchanged pbm_custom23
event ue_postconstructor ( )
integer x = 9
integer y = 20
integer width = 965
integer height = 240
integer taborder = 10
string dataobject = "d_product_shortage_response"
boolean border = false
boolean livescroll = true
end type

on ue_lbuttondown;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE UPPER(ls_ObjectName)
CASE	"OK", "CANCEL"
	This.Modify(ls_ObjectName + ".Border='5'")
	This.PostEvent("ue_" + ls_ObjectName)
END CHOOSE
end on

on ue_lbuttonup;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE ls_ObjectName
CASE	"Ok", "Cancel"
	This.Modify(ls_ObjectName + ".Border='6'")
END CHOOSE
end on

event ue_optionchanged;CHOOSE CASE	This.GetItemString(1, "option")
CASE "S"
	iw_Frame.SetMicroHelp("Allow scheduling to Handle shortages.")
	parent.dw_excluded_plants.visible = False
	parent.st_1.Visible = False
	parent.st_2.Visible = False
CASE "A"
	iw_Frame.SetMicroHelp("Apply scheduled quantity to ordered quantity.")
	parent.dw_excluded_plants.visible = False
	parent.st_1.Visible = False
	parent.st_2.Visible = False
CASE "R"
	iw_Frame.SetMicroHelp("Release product from reservations.")
	parent.dw_excluded_plants.visible = False
	parent.st_1.Visible = False
	parent.st_2.Visible = False
CASE "Q"
	iw_Frame.SetMicroHelp("Remove product from Shortage Queue.")
	parent.dw_excluded_plants.visible = True
	parent.st_1.Visible = True
	parent.st_2.Visible = True
END CHOOSE
end event

event itemchanged;CHOOSE CASE THIS.GetColumnName()
CASE	"option"
	THIS.PostEvent("ue_optionchanged")
CASE "select_all"
	IF Data = 'Y' THEN 
		dw_detail.SelectRow(0, TRUE)
	ELSE
		dw_detail.SelectRow(0, FALSE)
	END IF
END CHOOSE
end event

event constructor;This.InsertRow(0)
This.ClearValues('option')
end event

type dw_detail from u_base_dw_ext within w_product_shortage_response
integer x = 146
integer y = 612
integer width = 2030
integer height = 424
integer taborder = 20
string dataobject = "d_sales_detail_shortage"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;IF Row < 1 THEN RETURN

dw_main.SetItem(1, "select_all", 'N')

IF NOT This.IsSelected(Row) THEN
	Call Super::Clicked
	THIS.SelectRow(row, TRUE)
ELSE
  	THIS.SelectRow(row, FALSE)
END IF
end event

event constructor;call super::constructor;THIS.SetRedraw(TRUE)
end event

type dw_excluded_plants from u_base_dw_ext within w_product_shortage_response
boolean visible = false
integer x = 1015
integer y = 180
integer width = 997
integer height = 400
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_excluded_plants"
boolean vscrollbar = true
end type

event clicked;call super::clicked;IF Row > 0 THEN
	This.SelectRow( row, Not(This.IsSelected( row)))
	This.SetItem(row, "update_flag", 'U')
	IF This.IsSelected(row) = TRUE THEN
		This.SetItem(row, "exclude_ind", 'Y')
	ELSE
		This.SetItem(row, "exclude_ind", 'N')
	END IF
END IF
end event

type st_1 from statictext within w_product_shortage_response
boolean visible = false
integer x = 1239
integer y = 40
integer width = 425
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Excluded Plants for"
boolean focusrectangle = false
end type

type st_2 from statictext within w_product_shortage_response
boolean visible = false
integer x = 1266
integer y = 100
integer width = 466
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Shortage Queue:"
boolean focusrectangle = false
end type

