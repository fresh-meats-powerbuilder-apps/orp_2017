﻿$PBExportHeader$w_edi_selection.srw
forward
global type w_edi_selection from w_base_sheet_ext
end type
type dw_parameters from u_base_dw_ext within w_edi_selection
end type
type dw_detail from u_base_dw_ext within w_edi_selection
end type
end forward

global type w_edi_selection from w_base_sheet_ext
integer width = 3525
integer height = 1396
string title = "Sales EDI Change PO Review/Update Selection"
dw_parameters dw_parameters
dw_detail dw_detail
end type
global w_edi_selection w_edi_selection

type variables
String	is_openingstring, &
			is_selected_customer, &
			is_selected_div_po, &
			is_selected_ts, &
			is_initial_reference 
			
Date		idt_selected_po_date			


u_orp002		iu_orp002
u_ws_orp1		iu_ws_orp1

s_error	istr_error_info


end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();String	ls_header_string, &
			ls_detail_string
		
Integer	li_rtn		

u_string_functions	lu_string_functions 
			
//This.TriggerEvent('closequery')

If Not lu_string_functions.nf_IsEmpty(is_initial_reference) Then
	is_openingstring = is_initial_reference
	dw_parameters.Reset()
	dw_parameters.ImportString( is_openingstring)
Else	
	OpenWithParm( w_edi_selection_inq, is_openingstring)

	If message.StringParm = 'Cancel' Then 
		Return False
	Else
		is_openingstring = message.StringParm
		dw_parameters.Reset()
		dw_parameters.ImportString( is_openingstring)
	End If
	
End If

istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_orp002.nf_orpo23br_edi_selection_browse(istr_error_info, &
//																		is_openingstring, &
//																		ls_header_string, &
//																		ls_detail_string)		
																		
li_rtn = iu_ws_orp1.nf_orpo23fr(istr_error_info, &
											is_openingstring, &
											ls_header_string, &
											ls_detail_string)																			

dw_detail.Reset()
dw_detail.Importstring(ls_detail_string)

//dw_header.Reset()
//If dw_header.Importstring(ls_header_string) > 0 Then
//	dw_header.SelectRow(1, TRUE) 
//	wf_set_filter(1)
//End If


Return True
end function

on w_edi_selection.create
int iCurrent
call super::create
this.dw_parameters=create dw_parameters
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parameters
this.Control[iCurrent+2]=this.dw_detail
end on

on w_edi_selection.destroy
call super::destroy
destroy(this.dw_parameters)
destroy(this.dw_detail)
end on

event ue_postopen;call super::ue_postopen;iu_orp002 = CREATE u_orp002
iu_ws_orp1 = Create u_ws_orp1

wf_retrieve()
end event

event close;call super::close;If IsValid( iu_Orp002) Then Destroy( iu_orp002)
Destroy iu_ws_orp1
end event

event open;call super::open;is_initial_reference = Message.StringParm


end event

type dw_parameters from u_base_dw_ext within w_edi_selection
integer x = 18
integer y = 24
integer width = 3429
integer height = 140
integer taborder = 10
string dataobject = "d_edi_parameters"
end type

type dw_detail from u_base_dw_ext within w_edi_selection
integer x = 9
integer y = 172
integer width = 3438
integer height = 1044
integer taborder = 10
string dataobject = "d_edi_selection"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;call super::clicked;String ls_open_string

String	ls_selected_customer, &
			ls_selected_div_po, &
			ls_selected_ts, &
			ls_selected_salesperson, &
			ls_selected_salesperson_name, &
			ls_find_string, &
			ls_selected_po_date
			
Date		ldt_selected_po_date	

Long		ll_RowFound

If row > 0 Then
	ls_selected_customer = dw_detail.GetItemString(row,"customer_code")
	ls_selected_div_po = dw_detail.GetItemString(row, "div_po")
	ldt_selected_po_date = dw_detail.GetItemDate(row, "div_po_date")
	ls_selected_po_date = String(ldt_selected_po_date)
	ls_selected_ts = dw_detail.GetItemString(row, "transaction_ts")
	
	ls_find_string =  'customer_code = ' + "'" + ls_selected_customer + "' AND " + &
							'div_po = ' + "'" + ls_selected_div_po + "' AND " + &
							'div_po_date = Date(' + "'" + ls_selected_po_date + "'" + ') AND ' + &
							'transaction_ts < ' + "'" + ls_selected_ts + "'"
							
//	MessageBox ("Find String", ls_find_string)
   
	ll_RowFound = This.Find(ls_find_string, 1, This.RowCount())
	
	If ll_RowFound > 0 Then
		MessageBox ("Selection Error", "Process order changes in Transaction Time Stamp Order")
		Return
	End If

	ls_open_string = 	ls_selected_customer + '~t' + &
							ls_selected_div_po + '~t' + &
							String(ldt_selected_po_date, "yyyy-mm-dd") + '~t' + &
							ls_selected_ts + '~t' 
	
	iw_Frame.wf_OpenSheet( "w_edi_apply_changes", ls_open_string ) 

End If


end event

