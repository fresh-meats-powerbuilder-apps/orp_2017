﻿$PBExportHeader$w_popup_confirmation.srw
forward
global type w_popup_confirmation from w_base_popup
end type
type r_1 from rectangle within w_popup_confirmation
end type
type st_1 from statictext within w_popup_confirmation
end type
end forward

global type w_popup_confirmation from w_base_popup
int X=663
int Y=1445
int Width=1230
int Height=217
boolean TitleBar=false
long BackColor=1090519039
boolean ControlMenu=false
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
boolean Border=false
r_1 r_1
st_1 st_1
end type
global w_popup_confirmation w_popup_confirmation

on w_popup_confirmation.create
int iCurrent
call w_base_popup::create
this.r_1=create r_1
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=r_1
this.Control[iCurrent+2]=st_1
end on

on w_popup_confirmation.destroy
call w_base_popup::destroy
destroy(this.r_1)
destroy(this.st_1)
end on

type r_1 from rectangle within w_popup_confirmation
int Width=1226
int Height=217
boolean Enabled=false
int LineThickness=21
long LineColor=16711680
long FillColor=12632256
end type

type st_1 from statictext within w_popup_confirmation
int X=55
int Y=61
int Width=1070
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Please wait preparing order confirmation"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

