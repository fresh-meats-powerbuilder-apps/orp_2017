﻿$PBExportHeader$w_sales_order_confirmation.srw
forward
global type w_sales_order_confirmation from w_base_sheet_ext
end type
type dw_order_confirmation from u_base_dw_ext within w_sales_order_confirmation
end type
end forward

global type w_sales_order_confirmation from w_base_sheet_ext
integer height = 1428
string title = "Sales Order Confirmation"
dw_order_confirmation dw_order_confirmation
end type
global w_sales_order_confirmation w_sales_order_confirmation

type variables
string is_orderid

u_orp002 iu_orp002

u_ws_orp4	iu_ws_orp4

s_error istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();string ls_orderid
OpenWithParm( w_sales_Detail_Inq, is_orderid)
ls_orderid	=	Message.StringParm
IF ls_orderid = 'Abort' Then 
	RETURN FALSE
Else
	is_orderid	=	ls_orderid
	this.triggerevent('ue_query')
END IF
return true
end function

on w_sales_order_confirmation.create
int iCurrent
call super::create
this.dw_order_confirmation=create dw_order_confirmation
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_order_confirmation
end on

on w_sales_order_confirmation.destroy
call super::destroy
destroy(this.dw_order_confirmation)
end on

event ue_query;call super::ue_query;DataWindowChild	ldwc_detail,&
						ldwc_Header,&
						ldwc_stop,&
						ldwc_Instruction

String	ls_Location,&
			ls_loc_code, ls_type_code, ls_code
			
Boolean lb_view_loc


uo_d_customer_info	luo_d_customer_info
luo_d_customer_info = create uo_d_customer_info

luo_d_customer_info.dataobject = 'd_customer_info'

luo_d_customer_info.iw_parent = this

Open( w_popup_confirmation)

luo_d_customer_info.uof_Set_sales_id(is_orderid)

IF not luo_d_customer_info.uof_retrieve() Then
	
	Close( w_popup_confirmation)
	destroy (luo_d_customer_info)
	return 
end if

ls_loc_code = luo_d_customer_info.uof_get_location()
// Delete Later
//SELECT service_centers.service_center_name
//	INTO :ls_location
//	FROM Service_centers
//	WHERE Service_centers.service_center_code = :ls_Loc_code
//	USING SQLCA ;
SELECT locations.location_name
	INTO :ls_location
	FROM Locations
	WHERE locations.location_code = :ls_Loc_code
	USING SQLCA ; 
	
lb_view_loc = false
ls_code = ls_Loc_code +  message.is_smanlocation
SELECT tutltypes.type_code
    INTO :ls_type_code
    FROM tutltypes
    WHERE tutltypes.record_type = 'VIEWLOC'
    AND type_code = :ls_code
	USING SQLCA ; 

if ls_type_code <> "" then
	lb_view_loc = true
end if


if (ls_loc_code = message.is_smanlocation) or lb_view_loc then
	dw_order_confirmation.Object.sales_location_text.Text = ls_Location	
	is_typecode = ls_location
	dw_order_confirmation.GetChild("dw_header",ldwc_Header)
	dw_order_confirmation.GetChild("dw_detail", ldwc_detail)
	dw_order_confirmation.GetChild('dw_product_instruction', ldwc_Instruction)
	dw_order_confirmation.Getchild('dw_stop',ldwc_stop)
//	if NOT IsValid( iu_orp002) Then iu_orp002 = Create u_orp002
//	iu_orp002.nf_orpo78br_get_order_confimation(is_orderid, ldwc_Stop, ldwc_header, &
//											ldwc_detail, ldwc_instruction,istr_error_info )
											
	if NOT IsValid( iu_ws_orp4) Then iu_ws_orp4 = Create u_ws_orp4
	iu_ws_orp4.nf_orpo78fr(is_orderid, ldwc_Stop, ldwc_header, &
									ldwc_detail, ldwc_instruction,istr_error_info )
											
	is_customer_info = ldwc_Header.GetItemString(1,"ship_to")+"S"											

else
	messagebox('Sales Order Confirmation',"your location does not match order's location.  May not view it")
end if
	
iw_frame.SetMicroHelp("Ready")
Close( w_popup_confirmation)
destroy (luo_d_customer_info)
end event

event ue_postopen;call super::ue_postopen;is_typecode = Message.is_smanlocation

u_string_functions lu_string_functions
IF lu_string_functions.nf_isempty ( is_orderid ) Then
	if not wf_retrieve() then
		close (this)
	end if
ELSE
	This.PostEvent("ue_query")
END IF

	
end event

event open;call super::open;iu_orp003 = create u_orp003
ib_faxing_available = TRUE
is_orderid = Message.StringParm
end event

event close;call super::close;destroy iu_orp003
end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return
dw_order_confirmation.Print()


end event

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_print")
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_disable("m_print")
end event

type dw_order_confirmation from u_base_dw_ext within w_sales_order_confirmation
integer x = 27
integer y = 8
integer width = 2775
integer height = 1308
string dataobject = "d_order_confirmed_composite"
boolean hscrollbar = true
boolean vscrollbar = true
end type

