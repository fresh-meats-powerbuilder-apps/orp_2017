﻿$PBExportHeader$w_product_sub.srw
forward
global type w_product_sub from w_base_response_ext
end type
type dw_product_for_sub from datawindow within w_product_sub
end type
type dw_product_to_sub from datawindow within w_product_sub
end type
end forward

global type w_product_sub from w_base_response_ext
integer width = 2967
integer height = 616
string title = "Product Substitution"
long backcolor = 12632256
dw_product_for_sub dw_product_for_sub
dw_product_to_sub dw_product_to_sub
end type
global w_product_sub w_product_sub

type variables
u_orp001	iu_orp001

u_ws_orp4    iu_ws_orp4

s_error	s_error_info

w_sales_order_detail iw_sales_order_detail

end variables

forward prototypes
public function integer wf_validate_product (string as_product_code)
public function boolean wf_retrieve ()
end prototypes

public function integer wf_validate_product (string as_product_code);Long		ll_count


Select Count(*) 
	into :ll_count
	from sku_products
	WHERE sku_products.sku_product_code = :as_Product_code
	USING SQLCA;

IF ll_Count < 1 AND SQLCA.SQLCode = 0 then 		
	Return -1
Else
	Return 0
End If
end function

public function boolean wf_retrieve ();INT		li_rtn

Long		ll_rtn, &
			ll_ordered_units, &
			ll_ordered_wgt

String	ls_input, &
			ls_output_string, &
			ls_sub_age
			

BOOLEAN	lb_Modified = FALSE

u_string_functions	lu_string_functions

DataWindowChild	ldwc_temp

s_Error	lstr_Error

dw_product_for_sub.AcceptText()

dw_product_for_sub.SetRedraw(FALSE)
dw_product_to_sub.SetRedraw(FALSE)

lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "w_product_sub"
lstr_Error.se_function_name = "wf_Retriev"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

ls_input = dw_product_for_sub.GetItemString( 1, "order_id") + '~t' + &
				dw_product_for_sub.GetItemString( 1, "order_detail_number") + '~t' + '~r~n'
			  

//li_rtn = iu_orp001.nf_orpo06cr_sales_prod_sub_inq(s_error_info, &
//												ls_input, &
//												ls_output_string)
												
li_rtn = iu_ws_orp4.nf_orpo06gr(s_error_info, &
												ls_input, &
												ls_output_string)
												

If li_rtn = 0 Then
	//continue
Else
	This.TriggerEvent("ue_base_cancel")
	Return True
End If

dw_product_to_sub.Reset()
dw_product_to_sub.InsertRow(0)

dw_product_to_sub.setitem(1, "order_detail_number", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
dw_product_to_sub.setitem(1, "sub_instr_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
ll_ordered_wgt = Long(lu_string_functions.nf_gettoken(ls_output_string, '~t'))

if dw_product_for_sub.GetItemString(1, "ordered_uom") = 'W' Then
	ll_ordered_units = dw_product_for_sub.GetItemNumber(1, "net_weight") - &
							ll_ordered_wgt
	dw_product_for_sub.setitem(1, "ordered_units", ll_ordered_wgt)
	dw_product_for_sub.setitem(1, "scheduled_units", dw_product_for_sub.GetItemNumber(1, "net_weight"))		
Else 
	ll_ordered_units = dw_product_for_sub.GetItemNumber(1, "ordered_units") - &
						dw_product_for_sub.GetItemNumber(1, "scheduled_units")
End If

ls_sub_age = dw_product_for_sub.GetItemString(1,"ordered_age")

If ll_ordered_units < 0 Then
	ll_ordered_units = 0
End If

dw_product_to_sub.setitem(1, "ordered_units", ll_ordered_units)

dw_product_to_sub.setitem(1, "ordered_age", ls_sub_age)

li_rtn = dw_product_to_sub.GetChild('product_code', ldwc_temp)
ldwc_temp.Reset()
ls_output_string = lu_string_functions.nf_righttrim(ls_output_string, TRUE, TRUE)
ll_rtn = ldwc_temp.ImportString('          ' + '~r~n' + ls_output_string)


dw_product_to_sub.SetRedraw(True)
dw_product_to_sub.AcceptText()

dw_product_for_sub.SetRedraw(True)
				
return true


end function

on w_product_sub.create
int iCurrent
call super::create
this.dw_product_for_sub=create dw_product_for_sub
this.dw_product_to_sub=create dw_product_to_sub
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_for_sub
this.Control[iCurrent+2]=this.dw_product_to_sub
end on

on w_product_sub.destroy
call super::destroy
destroy(this.dw_product_for_sub)
destroy(this.dw_product_to_sub)
end on

event ue_postopen;call super::ue_postopen;String	ls_importstring
		
ls_Importstring = iw_sales_order_detail.wf_get_prod_for_sub()
dw_product_for_sub.ImportString(ls_importstring)

dw_product_for_sub.AcceptText()
dw_product_for_sub.SetFocus()
dw_product_for_sub.SetRedraw(True)

wf_retrieve()

			

end event

event open;call super::open;iw_sales_order_detail = Message.PowerObjectParm

iu_ws_orp4 = Create u_ws_orp4

iu_orp001 = Create u_orp001


end event

event activate;call super::activate;dw_product_for_sub.Enabled = TRUE
dw_product_to_sub.Enabled = TRUE
end event

event deactivate;call super::deactivate;dw_product_for_sub.Enabled = FALSE
dw_product_to_sub.Enabled = FALSE
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
//CloseWithReturn( This,"Cancel")
end event

event ue_base_ok;call super::ue_base_ok;INT		li_rtn

Long		ll_rtn, &
			ll_ordered_units

String	ls_input, &
			ls_output_string, &
			ls_order_id, &
			ls_org_line_num, &
			ls_sub_line_num, &
			ls_org_product, &
			ls_sub_product, &
			ls_sub_age, &
			ls_uom
			

BOOLEAN	lb_Modified = FALSE

u_string_functions	lu_string_functions

DataWindowChild	ldwc_temp

s_Error	lstr_Error

dw_product_to_sub.AcceptText()

IF dw_product_to_sub.AcceptText() = -1 Then
	Return
Else
	If IsNull(dw_product_to_sub.GetItemString( 1, "product_code")) Then
		iw_frame.SetMicroHelp('Product Code is a required field.')
		Return 
	End If	
	
		If dw_product_to_sub.GetItemNumber(1,"ordered_units") > 0 Then
		//continue
	Else
		iw_frame.SetMicroHelp('Units must be greater than zero.')
		dw_product_to_sub.SetColumn('ordered_units')
		dw_product_to_sub.SetRow(1)
		dw_product_to_sub.SetFocus()
		dw_product_to_sub.SetRedraw(True)
		Return 
	End If
	
	lstr_Error.se_app_name  = "orp"
	lstr_Error.se_window_name = "w_product_sub"
	lstr_Error.se_function_name = "sub product"
	lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")


	ls_order_id = dw_product_for_sub.GetItemString( 1, "order_id")
	ls_org_line_num = dw_product_for_sub.GetItemString( 1, "order_detail_number")
	ls_sub_line_num = dw_product_to_sub.GetItemString( 1, "order_detail_number")
	ls_org_product = dw_product_for_sub.GetItemString( 1, "product_code")
	ls_sub_product = dw_product_to_sub.GetItemString( 1, "product_code")
	ls_sub_age =  dw_product_to_sub.GetItemString( 1, "ordered_age") 
	ls_uom =  dw_product_for_sub.GetItemString( 1, "ordered_uom") 
	ll_ordered_units = dw_product_to_sub.GetItemNumber( 1, "ordered_units") 

	ls_input = ls_order_id + '~t' + ls_org_line_num + '~t' + &
					ls_sub_line_num + '~t' + ls_org_product + '~t' + ls_sub_product + '~t' + &
					ls_sub_age + '~t' + ls_uom + '~t' + String(ll_ordered_units) + '~r~n'
			  

//	li_rtn = iu_orp001.nf_orpo07cr_sales_prod_sub_upd(s_error_info, &
//												ls_input)
												
	li_rtn = iu_ws_orp4.nf_orpo07gr(s_error_info, &
												ls_input)

Choose Case li_rtn				
	Case 0
		CloseWithReturn(This,"OK")
	Case 1
		//Error in move.  Response window stays open
end choose						

End If


end event

event close;call super::close;If IsValid( iu_orp001) Then Destroy( iu_orp001)
If IsValid( iu_ws_orp4) Then Destroy( iu_ws_orp4)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_product_sub
boolean visible = false
integer x = 1394
integer y = 456
integer taborder = 0
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_product_sub
integer x = 2569
integer y = 212
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_product_sub
integer x = 2574
integer y = 64
end type

type cb_browse from w_base_response_ext`cb_browse within w_product_sub
integer x = 2085
integer y = 456
integer height = 100
integer taborder = 0
boolean enabled = false
end type

type dw_product_for_sub from datawindow within w_product_sub
integer x = 73
integer y = 56
integer width = 2423
integer height = 220
boolean bringtotop = true
string title = "none"
string dataobject = "d_product_for_sub"
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_product_to_sub from datawindow within w_product_sub
integer x = 73
integer y = 280
integer width = 2231
integer height = 88
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_product_to_sub"
boolean border = false
end type

event itemchanged;long						ll_count
String					ls_column_name, &
							ls_product_code
							
DataWindowChild		ldwc_Temp
u_string_functions	lu_string_functions


CHOOSE CASE This.GetColumnName()
CASE "product_code"
	If lu_string_functions.nf_isempty(data) Then
		iw_frame.SetMicroHelp('Product Code is a required field.')
		Return 1
	End If
	If wf_validate_product(data) = 0 Then
		//continue
	Else
		iw_Frame.SetMicroHelp("Invalid/Inactive product code")
		dw_product_to_sub.SetColumn('product_code')
		dw_product_to_sub.SetRow(1)
		dw_product_to_sub.SetFocus()
		dw_product_to_sub.SetRedraw(True)
		Return 0
	END IF
Case 'ordered_units'
	If lu_string_functions.nf_isempty(data) Then
		iw_frame.SetMicroHelp('Units is a required field.')
		Return 1
	End If
		
END CHOOSE
iw_frame.setmicrohelp('Ready')
end event

