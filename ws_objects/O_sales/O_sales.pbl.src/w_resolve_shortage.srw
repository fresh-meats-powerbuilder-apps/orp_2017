﻿$PBExportHeader$w_resolve_shortage.srw
$PBExportComments$window for res shortage
forward
global type w_resolve_shortage from window
end type
type cb_2 from commandbutton within w_resolve_shortage
end type
type cb_1 from commandbutton within w_resolve_shortage
end type
type dw_resolve_shortage from datawindow within w_resolve_shortage
end type
type dw_header from datawindow within w_resolve_shortage
end type
end forward

global type w_resolve_shortage from window
integer x = 384
integer y = 200
integer width = 2139
integer height = 1536
boolean titlebar = true
string title = "Resolve Shortages"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
cb_2 cb_2
cb_1 cb_1
dw_resolve_shortage dw_resolve_shortage
dw_header dw_header
end type
global w_resolve_shortage w_resolve_shortage

type variables
u_Orp003		ino_orp003
u_ws_orp2       iu_ws_orp2
LONG		il_ScrollPage = 1064
end variables

forward prototypes
public function boolean wf_update ()
end prototypes

public function boolean wf_update ();s_Error	ls_Error_Info

INTEGER	li_Rtn

STRING	ls_Detail_Info, &
			ls_Header_Info

LONG		ll_CurrentRow

Character	lc_ind

ll_CurrentRow = dw_resolve_shortage.GetRow()

lc_ind = 'C'

li_Rtn = dw_header.AcceptText()
IF li_Rtn = -1 THEN RETURN FALSE
li_Rtn = dw_resolve_shortage.AcceptText()
IF li_Rtn = -1 THEN RETURN FALSE

ls_Header_Info = dw_header.Describe("DataWindow.Data")
ls_Detail_Info = dw_resolve_shortage.Describe("DataWindow.Data")

ls_Error_Info.se_User_ID = Message.nf_GetUserID()
ls_Error_Info.se_Window_Name = "w_res_short"
ls_Error_Info.se_App_Name = "OPR"

//li_Rtn = ino_Orp003.nf_orpo48ar(ls_Error_Info, ls_Header_Info, ls_Detail_Info, lc_ind)

li_Rtn = iu_ws_orp2.nf_orpo48fr(ls_Header_Info, ls_Detail_Info, lc_ind, ls_Error_Info)

This.SetRedraw(FALSE)
dw_resolve_shortage.Reset()
dw_resolve_shortage.ImportString(ls_Detail_Info)
dw_header.Reset()
dw_header.ImportString(ls_Header_Info)
dw_resolve_shortage.Sort()
dw_resolve_shortage.GroupCalc()

dw_resolve_shortage.ScrollToRow(ll_CurrentRow)

This.SetRedraw(TRUE)

RETURN TRUE
end function

event open;INTEGER	li_Rtn

STRING	ls_Detail_Info, &
			ls_Header_Info, &
			ls_shortage_lines
			
Character	lc_ind			

ino_orp003 = CREATE u_orp003
iu_ws_orp2  = CREATE  u_ws_orp2

s_Error	ls_Error_Info
w_Sales_Order_Detail		lw_Parent
ls_shortage_lines = Message.stringParm

lc_ind = 'C'

lw_Parent  = iw_Frame.GetFirstSheet()
dw_header.SetItem(1, "sales_id", lw_Parent.is_orderid)
ls_Header_Info = dw_header.Describe("DataWindow.Data")
ls_Error_Info.se_User_ID = Message.nf_GetUserID()
ls_Error_Info.se_Window_Name = "w_res_short"
ls_Error_Info.se_App_Name = "OPR"

//li_Rtn = ino_Orp003.nf_orpo47ar(ls_Error_Info, ls_Header_Info, &
//											ls_Detail_Info, ls_shortage_lines, lc_ind)
											
li_Rtn = iu_ws_orp2.nf_orpo47fr(ls_Header_Info, &
											ls_Detail_Info, ls_shortage_lines, lc_ind, ls_Error_Info)


dw_resolve_shortage.Reset()
dw_resolve_shortage.ImportString(ls_Detail_Info)
dw_header.Reset()
dw_header.ImportString(ls_Header_Info)
dw_resolve_shortage.Sort()
dw_resolve_shortage.GroupCalc()

if li_rtn = 3 Then
	cb_1.Enabled = False
else
	dw_header.Enabled = TRUE
	dw_resolve_shortage.Enabled = TRUE
end if
end event

on w_resolve_shortage.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_resolve_shortage=create dw_resolve_shortage
this.dw_header=create dw_header
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_resolve_shortage,&
this.dw_header}
end on

on w_resolve_shortage.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_resolve_shortage)
destroy(this.dw_header)
end on

event close;DESTROY iu_ws_orp2
end event

type cb_2 from commandbutton within w_resolve_shortage
integer x = 1783
integer y = 144
integer width = 247
integer height = 108
integer taborder = 20
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Close"
boolean cancel = true
end type

on clicked;dw_header.Enabled = FALSE
dw_resolve_shortage.Enabled = FALSE
Close(Parent)
end on

type cb_1 from commandbutton within w_resolve_shortage
integer x = 1787
integer y = 20
integer width = 247
integer height = 108
integer taborder = 40
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Save"
boolean default = true
end type

on clicked;dw_resolve_shortage.Enabled = FALSE
wf_Update()
dw_resolve_shortage.Enabled = TRUE
end on

type dw_resolve_shortage from datawindow within w_resolve_shortage
event ue_lbuttondown pbm_lbuttondown
event ue_vcrfirst pbm_custom33
event ue_vcrprevious pbm_custom34
event ue_vcrnext pbm_custom35
event ue_vcrlast pbm_custom36
event ue_microhelp pbm_custom37
event ue_mousemove pbm_mousemove
integer y = 268
integer width = 2098
integer height = 1148
integer taborder = 10
boolean enabled = false
string dataobject = "d_resolve_shortage"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on ue_lbuttondown;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE ls_ObjectName
CASE	"vcrfirst", "vcrprevious", "vcrnext", "vcrlast"
	This.Modify(ls_ObjectName + ".Border='5'")
	This.TriggerEvent("ue_" + ls_ObjectName)
	This.Modify(ls_ObjectName + ".Border='6'")
END CHOOSE
end on

on ue_vcrfirst;STRING	ls_line
LONG		ll_Row
LONG 	li_Rtn

IF This.GetRow() > 0 THEN
ELSE
	li_Rtn = This.ScrollPriorPage()
	Return
END IF

ls_line = This.GetItemString(This.GetRow(), "order_line")

ls_line = STRING(INTEGER(ls_line) - 100, "00000")

ll_Row = This.Find("order_line = '" + ls_line  + "'", 1, This.RowCount())

IF ll_Row > 0 THEN 
ELSE 
	ll_Row = 1
END IF

IF This.RowCount() > 0 THEN li_Rtn = This.ScrollToRow(ll_Row)

RETURN
end on

on ue_vcrprevious;IF This.RowCount() > 0 THEN This.ScrollPriorPage()
end on

on ue_vcrnext;IF This.RowCount() > 0 THEN This.ScrollNextPage()
end on

on ue_vcrlast;STRING	ls_line
LONG		ll_Row

IF This.GetRow() > 0 THEN
ELSE
	This.ScrollNextPage()
	Return
END IF

ls_line = This.GetItemString(This.GetRow(), "order_line")

ls_line = STRING(INTEGER(ls_line) + 100, "00000")

ll_Row = This.Find("order_line = '" + ls_line  + "'", 1, This.RowCount())

IF ll_Row > 0 THEN 
ELSE 
	ll_Row = 999999
END IF

IF This.RowCount() > 0 THEN This.ScrollToRow(ll_Row)
end on

on ue_microhelp;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE ls_ObjectName
CASE	"vcrfirst"
	iw_Frame.SetMicroHelp("Click to view previous product.")
CASE	"vcrprevious"
	iw_Frame.SetMicroHelp("Click to view previous page.")
CASE	"vcrnext"
	iw_Frame.SetMicroHelp("Click to view next page.")
CASE	"vcrlast"
	iw_Frame.SetMicroHelp("Click to view next product.")
CASE ELSE
	iw_Frame.SetMicroHelp("Ready")
END CHOOSE
end on

on ue_mousemove;This.PostEvent("ue_microhelp")
end on

event destructor;If IsValid(ino_orp003) Then Destroy( ino_orp003)
end event

type dw_header from datawindow within w_resolve_shortage
event ue_lbuttondown pbm_lbuttondown
event ue_microhelp pbm_mousemove
integer x = 9
integer y = 16
integer width = 1678
integer height = 256
integer taborder = 30
boolean enabled = false
string dataobject = "d_resolve_shortage_header"
boolean border = false
end type

on ue_lbuttondown;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE UPPER(ls_ObjectName)
CASE	"UPDATE"
	dw_header.Enabled = FALSE
	dw_resolve_shortage.Enabled = FALSE
	This.Modify(ls_ObjectName + ".Border='5'")
	wf_Update()
	This.Modify(ls_ObjectName + ".Border='6'")
	dw_header.Enabled = TRUE
	dw_resolve_shortage.Enabled = TRUE
CASE	"RETURN"
	dw_header.Enabled = FALSE
	dw_resolve_shortage.Enabled = FALSE
	This.Modify(ls_ObjectName + ".Border='5'")
	Close(Parent)
END CHOOSE
end on

on ue_microhelp;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE UPPER(ls_ObjectName)
CASE	"UPDATE"
	iw_Frame.SetMicroHelp("Click to recalculate scheduled units.")
CASE	"RETURN"
	iw_Frame.SetMicroHelp("Click to return to sales order.")
CASE ELSE
	iw_Frame.SetMicroHelp("Ready")
END CHOOSE
end on

