﻿$PBExportHeader$w_move_product_response.srw
$PBExportComments$Window used to move product from one resv to another
forward
global type w_move_product_response from w_base_response_ext
end type
type dw_sales_detail_base from u_base_dw_ext within w_move_product_response
end type
type em_order_id from editmask within w_move_product_response
end type
type st_1 from statictext within w_move_product_response
end type
type st_2 from statictext within w_move_product_response
end type
type sle_from_sales_order from singlelineedit within w_move_product_response
end type
end forward

global type w_move_product_response from w_base_response_ext
integer x = 690
integer y = 368
integer width = 1691
integer height = 1204
string title = "Product Move to Existing Order"
long backcolor = 12632256
dw_sales_detail_base dw_sales_detail_base
em_order_id em_order_id
st_1 st_1
st_2 st_2
sle_from_sales_order sle_from_sales_order
end type
global w_move_product_response w_move_product_response

type variables
u_orp001	iu_orp001
u_ws_orp2 	iu_ws_orp2

s_error	s_error_info

w_sales_order_detail iw_sales_order_detail

string	is_shortage_string_out, &
		is_save_to_order

end variables

forward prototypes
public function integer wf_move ()
public subroutine wf_check_microhelp_message (s_error astr_error_info)
end prototypes

public function integer wf_move ();string ls_additional_data, &
		ls_new_order, &
		ls_plant, &
		ls_ship_date, &
		ls_all_ind, &
		ls_shortage_string_in, &
		ls_dup_string
		
Integer	li_rtn		
		
s_error_info.se_app_name = "ORP"
s_error_info.se_event_name="CLICKED"
s_error_info.se_function_name="move product"
s_error_info.se_procedure_name = "nf_orpo50ar"
s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
s_error_info.se_window_name = "u_orp001"
ls_new_order = em_order_id.Text

iw_sales_order_detail.Event ue_get_data("all")
ls_all_ind = Message.StringParm
//dmk move spaces to shortage string for first call
ls_shortage_string_in = ' '

//Return iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//														sle_from_sales_order.Text, & 
//														em_order_id.Text, & 
//														ls_plant, & 
//														ls_ship_date, & 
//														'P' + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data") , & 
//														ls_additional_data, &
//														ls_shortage_string_in, &
//														is_shortage_string_out, &
//														ls_dup_string)

li_rtn = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
											sle_from_sales_order.Text, & 
											em_order_id.Text, & 
											ls_plant, & 
											ls_ship_date, & 
											'P' + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data") , & 
											ls_additional_data, &
											ls_shortage_string_in, &
											is_shortage_string_out, &
											ls_dup_string)
											
wf_check_microhelp_message(s_error_info)											
											
Return li_rtn											

end function

public subroutine wf_check_microhelp_message (s_error astr_error_info);String		ls_message, ls_type_code, ls_type_desc

Integer	li_type_code

ls_message = mid(astr_error_info.se_message,1,40)

SQLCA.nf_connect()

SELECT tutltypes.type_code
	 INTO :ls_type_code
	 FROM tutltypes
	 WHERE tutltypes.record_type = 'MESSGBOX'
	 AND type_desc = :ls_message
	USING SQLCA ; 

If SQLCA.sqlcode = 0 and (ls_type_code > '        ') Then
	li_type_code = Integer(ls_type_code)
	li_type_code ++
	ls_type_code = string(li_type_code, "00000000")
	
	SELECT tutltypes.type_desc
	 INTO :ls_type_desc
	 FROM tutltypes
	 WHERE tutltypes.record_type = 'MESSGBOX'
	 AND type_code = :ls_type_code
	USING SQLCA ; 
	
	If SQLCA.sqlcode = 0 and (ls_type_desc > '        ') Then
		MessageBox(ls_type_desc, astr_error_info.se_message)
	Else	
		MessageBox("", astr_error_info.se_message)
	End if
End If
end subroutine

event close;call super::close;IF IsValid( iu_orp001) Then Destroy( iu_orp001)
Destroy iu_ws_orp2
end event

event ue_postopen;call super::ue_postopen;String 	ls_Import

sle_from_sales_order.Text = iw_sales_order_detail.is_orderid
em_order_id.Text	= iw_sales_order_detail.is_to_order_id
//iw_sales_Order_Detail.is_orderid	=	''
ls_Import = iw_sales_order_detail.wf_get_selected_rows()
dw_sales_detail_base.ImportString( ls_Import)


end event

event open;call super::open;iw_sales_order_detail = Message.PowerObjectParm

iu_orp001 = Create u_orp001
iu_ws_orp2 = Create u_ws_orp2
end event

on w_move_product_response.create
int iCurrent
call super::create
this.dw_sales_detail_base=create dw_sales_detail_base
this.em_order_id=create em_order_id
this.st_1=create st_1
this.st_2=create st_2
this.sle_from_sales_order=create sle_from_sales_order
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_detail_base
this.Control[iCurrent+2]=this.em_order_id
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.sle_from_sales_order
end on

on w_move_product_response.destroy
call super::destroy
destroy(this.dw_sales_detail_base)
destroy(this.em_order_id)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.sle_from_sales_order)
end on

event ue_base_ok;window lw_detail
integer li_rval
string ls_returnval, &
		 ls_additional_data, &
		 ls_new_order, &
		 ls_plant, &
		 ls_ship_date, &
		 ls_shortage_string_in, &
		 ls_shortage_string_out, &
		 ls_all_ind, &
		 ls_dup_string, &
		 ls_option
		 
u_string_functions	lu_string_functions

SetPointer(HourGlass!)
IF lu_string_functions.nF_IsEmpty(em_order_id.Text) Then
	MessageBox( "To Sales Order ID", "Please enter a sales order ID to move to")
	Return
END IF
is_save_to_order = em_order_id.Text
IF dw_sales_detail_base.AcceptText() = -1 Then
	Return
Else
	li_rval = wf_move()
	em_order_id.Text = is_save_to_order
	ls_shortage_string_in = is_shortage_string_out
	Choose Case li_rval
		Case 0
			CloseWithReturn(This,"OK")
		Case 1
			//Error in move.  Response window stays open
		Case 2,5
			OpenSheetWithParm(lw_detail, em_order_id.text, 'w_sales_order_detail', iw_frame, 0, &
				iw_Frame.im_menu.iao_ArrangeOpen)
			CloseWithReturn(This,"OK")	
		Case 6
			OpenWithParm(w_product_add_to_existing_response, sle_from_sales_order.Text + '~t' + em_order_id.Text + '~t' + 'M' + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"))
			ls_dup_string = Message.StringParm	
			if ls_dup_string <>  "Cancel" Then
				iw_sales_order_detail.Event ue_get_data("all")
				ls_all_ind = Message.StringParm
				s_error_info.se_app_name = "ORP"
				s_error_info.se_event_name="CLICKED"
				s_error_info.se_function_name="move product"
				s_error_info.se_procedure_name = "nf_orpo50ar"
				s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
				s_error_info.se_window_name = "u_orp001"
//				li_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//																			sle_from_sales_order.Text, & 
//																			em_order_id.Text, ls_plant, & 
//																			ls_ship_date, & 
//																			'M' + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//																			ls_additional_data, &
//																			ls_shortage_string_in, &
//																			ls_shortage_string_out, &
//																			ls_dup_string) 

				li_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
											sle_from_sales_order.Text, & 
											em_order_id.Text, ls_plant, & 
											ls_ship_date, & 
											'M' + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
											ls_additional_data, &
											ls_shortage_string_in, &
											ls_shortage_string_out, &
						  					ls_dup_string) 
											  
				wf_check_microhelp_message(s_error_info)						  
				
				ls_shortage_string_in = ls_shortage_string_out
				em_order_id.Text = is_save_to_order
				//dmk continue to call orpo50br if the rval is 3 (short pa)
				
				Choose Case li_rval				
					Case 0
						CloseWithReturn(This,"OK")
					Case 1
						//Error in move.  Response window stays open
					Case 2,5
						OpenSheetWithParm(lw_detail, em_order_id.text, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)
						CloseWithReturn(This,"OK")
					Case 3
						OpenWithParm( w_move_prod_short_response, is_shortage_string_out)
						ls_returnval = Message.StringParm
						IF ls_returnval = 'OK' or ls_returnval = 'R' Then
						Do
							iw_sales_order_detail.Event ue_get_data("all")
							ls_all_ind = Message.StringParm
							s_error_info.se_app_name = "ORP"
							s_error_info.se_event_name="CLICKED"
							s_error_info.se_function_name="move product"
							s_error_info.se_procedure_name = "nf_orpo50ar"
							s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
							s_error_info.se_window_name = "u_orp001"
					
							If ls_returnval = 'OK' Then
								ls_option = 'M'
							Else
								ls_option = 'R'
							End IF
//							li_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//															sle_from_sales_order.Text, & 
//															em_order_id.Text, ls_plant, & 
//															ls_ship_date, & 
//															ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//															ls_additional_data, &
//															ls_shortage_string_in, &
//															ls_shortage_string_out, &
//															ls_dup_string) 
							li_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
														sle_from_sales_order.Text, & 
														em_order_id.Text, ls_plant, & 
														ls_ship_date, & 
														ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
														ls_additional_data, &
														ls_shortage_string_in, &
														ls_shortage_string_out, &
														ls_dup_string) 															
															
							wf_check_microhelp_message(s_error_info)									
															
			 				ls_shortage_string_in = ls_shortage_string_out
							em_order_id.Text = is_save_to_order
							If li_rval = 3 Then 
								OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
								ls_returnval = Message.StringParm
							Else
								CloseWithReturn(This,"OK")
							End If
						Loop until li_rval <> 3 or ls_returnval = 'Cancel'
					end if
				end choose		
			end if
		Case 3
			OpenWithParm( w_move_prod_short_response, is_shortage_string_out)
			ls_returnval = Message.StringParm
			//dmk add Retry logic continue to call ORPO50BR to check pa
			// until no shorts are found
			IF ls_returnval = 'OK' or ls_returnval = 'R' Then
				Do
				IF ls_returnval = 'OK' or ls_returnval = 'R' Then
					iw_sales_order_detail.Event ue_get_data("all")
					ls_all_ind = Message.StringParm
					s_error_info.se_app_name = "ORP"
					s_error_info.se_event_name="CLICKED"
					s_error_info.se_function_name="move product"
					s_error_info.se_procedure_name = "nf_orpo50ar"
					s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
					s_error_info.se_window_name = "u_orp001"
					//dmk
					If ls_returnval = 'OK' Then
						ls_option = 'M'
					Else
						ls_option = 'R'
					End IF
//					li_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//																			sle_from_sales_order.Text, & 
//																			em_order_id.Text, ls_plant, & 
//																			ls_ship_date, & 
//																			ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//																			ls_additional_data, &
//																			ls_shortage_string_in, &
//																			ls_shortage_string_out, &
//																			ls_dup_string) 
					li_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
																sle_from_sales_order.Text, & 
																em_order_id.Text, ls_plant, & 
																ls_ship_date, & 
																ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
																ls_additional_data, &
																ls_shortage_string_in, &
																ls_shortage_string_out, &
																ls_dup_string) 															
																			
					wf_check_microhelp_message(s_error_info)															
																			
					ls_shortage_string_in = ls_shortage_string_out
					em_order_id.Text = is_save_to_order
					If li_rval = 3 Then 
						OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
						ls_returnval = Message.StringParm
					End If
				End If
			Loop until li_rval <> 3 or ls_returnval = 'Cancel' 
				
				Choose Case li_rval				
					Case 0
						CloseWithReturn(This,"OK")
					Case 1
						//Error in move.  Response window stays open
					Case 2,5
						OpenSheetWithParm(lw_detail, em_order_id.text, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)
						CloseWithReturn(This,"OK")
					Case 6
						OpenWithParm(w_product_add_to_existing_response, sle_from_sales_order.Text + '~t' + em_order_id.text + '~t' + 'M' + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"))
						ls_dup_string = Message.StringParm	
						if ls_dup_string <>  "Cancel" Then
							iw_sales_order_detail.Event ue_get_data("all")
							ls_all_ind = Message.StringParm
							s_error_info.se_app_name = "ORP"
							s_error_info.se_event_name="CLICKED"
							s_error_info.se_function_name="move product"
							s_error_info.se_procedure_name = "nf_orpo50ar"
							s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
							s_error_info.se_window_name = "u_orp001"
//							li_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//																						sle_from_sales_order.Text, & 
//																						em_order_id.Text, ls_plant, & 
//																						ls_ship_date, & 
//																						'M' + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//																						ls_additional_data, &
//																						ls_shortage_string_in, &
//																						ls_shortage_string_out, &
//																						ls_dup_string) 
							li_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
																		sle_from_sales_order.Text, & 
																		em_order_id.Text, ls_plant, & 
																		ls_ship_date, & 
																		'M' + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
																		ls_additional_data, &
																		ls_shortage_string_in, &
																		ls_shortage_string_out, &
																		ls_dup_string) 
 
							wf_check_microhelp_message(s_error_info)



							ls_shortage_string_in = ls_shortage_string_out
							em_order_id.Text = is_save_to_order
							If li_rval = 3 Then
								OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
								ls_returnval = Message.StringParm
								Do
									iw_sales_order_detail.Event ue_get_data("all")
									ls_all_ind = Message.StringParm
									s_error_info.se_app_name = "ORP"
									s_error_info.se_event_name="CLICKED"
									s_error_info.se_function_name="move product"
									s_error_info.se_procedure_name = "nf_orpo50ar"
									s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
									s_error_info.se_window_name = "u_orp001"
					
									If ls_returnval = 'OK' Then
										ls_option = 'M'
									Else
										ls_option = 'R'
									End IF
//									li_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//																			sle_from_sales_order.Text, & 
//																			em_order_id.Text, ls_plant, & 
//																			ls_ship_date, & 
//																			ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//																			ls_additional_data, &
//																			ls_shortage_string_in, &
//																			ls_shortage_string_out, &
//																			ls_dup_string) 

									li_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
																			sle_from_sales_order.Text, & 
																			em_order_id.Text, ls_plant, & 
																			ls_ship_date, & 
																			ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
																			ls_additional_data, &
																			ls_shortage_string_in, &
																			ls_shortage_string_out, &
																			ls_dup_string) 


									wf_check_microhelp_message(s_error_info)


			 						ls_shortage_string_in = ls_shortage_string_out
									em_order_id.Text = is_save_to_order
									If li_rval = 3 Then 
										OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
										ls_returnval = Message.StringParm
//									Else
//										CloseWithReturn(This,"OK")
									End If
								Loop until li_rval <> 3 or ls_returnval = 'Cancel'
							End if
							Choose Case li_rval				
								Case 0
									CloseWithReturn(This,"OK")
								Case 1
									//Error in move.  Response window stays open
								Case 2,5
									OpenSheetWithParm(lw_detail, em_order_id.text, 'w_sales_order_detail', iw_frame, 0, &
									iw_Frame.im_menu.iao_ArrangeOpen)
									CloseWithReturn(This,"OK")
							end choose		
						end if
				end choose
			End If
	    end choose
END IF
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This,"Cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_move_product_response
boolean visible = false
integer x = 1143
integer y = 48
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_move_product_response
integer x = 1362
integer y = 372
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_move_product_response
integer x = 1362
integer y = 244
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_move_product_response
boolean visible = true
integer x = 1362
integer y = 500
integer width = 270
integer taborder = 60
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
end type

event cb_browse::clicked;call super::clicked;window lw_temp

String	ls_to_orderid, ls_35_tabs, ls_ship_date, ls_future_date

ls_35_tabs = "~t" + "~t" + "~t" + "~t" + "~t" + &
					"~t" + "~t" + "~t" + "~t" + "~t" + &
					"~t" + "~t" + "~t" + "~t" + "~t" + &
					"~t" + "~t" + "~t" + "~t" + "~t" + &
					"~t" + "~t" + "~t" + "~t" + "~t" + &
					"~t" + "~t" + "~t" + "~t" + "~t" + &
					"~t" + "~t" + "~t" + "~t" + "~t"


IF iw_frame.wf_windowisvalid (lw_temp , "w_sales-orders-by-customer") &
		Then Message.PowerObjectParm = iw_sales_order_detail
		
ls_ship_date = iw_sales_order_detail.wf_getshipdate ( )
ls_future_date = string(RelativeDate(date(ls_ship_date),365),"MM/DD/YYYY")

iw_Frame.wf_OpenSheet( "w_sales-orders-by-customer", ls_ship_date + "~t" + iw_sales_order_detail.wf_get_customer ( ) + ls_35_tabs + "Y" + "~t" + "~t" + "~t" + "N" + "~t" + "~t" + "~t" + "~t" + "~t" + ls_future_date + "~t")
Do While Not IsValid (lw_temp)
	IF iw_frame.wf_windowisvalid (lw_temp , "w_sales-orders-by-customer") Then
		lw_temp.Dynamic wf_set_iw_sales_detail( iw_sales_order_detail)
	END IF
Loop
ls_to_orderid	=	Message.StringParm

Close(Parent)
end event

type dw_sales_detail_base from u_base_dw_ext within w_move_product_response
integer x = 32
integer y = 236
integer width = 1289
integer height = 824
integer taborder = 20
string dataobject = "d_sales_detail_base"
boolean vscrollbar = true
end type

event itemerror;call super::itemerror;IF GetColumnName()  = "ordered_units" Then
	Messagebox( "Ordered Units", "Ordered units must be > 0, but can not be greater than the original scheduled units")
	RETURN 1	
END IF
end event

event itemchanged;call super::itemchanged;long	ll_ordered,&
	ll_sch_units
String ls_columnName, ls_qty_wt_ind

ls_columnName = GetColumnName()
IF    ls_columnName = "ordered_units" Then
  	ls_qty_wt_ind = iw_sales_order_detail.wf_get_qty_wt_ind(This.GetItemString(row, "order_detail_number"))

	IF ls_qty_wt_ind = 'W' THEN
		// do nothing
	ELSE		
		ll_ordered = Long(This.GetText())
		ll_sch_units = This.GetItemDecimal( This.GetRow(), "scheduled_units") 
		IF ll_ordered > ll_sch_units Then 
			RETURN 1
		END IF
	END IF
END IF
end event

type em_order_id from editmask within w_move_product_response
integer x = 517
integer y = 108
integer width = 325
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
integer accelerator = 116
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!####"
string displaydata = ""
end type

on modified;IF Upper(Trim(This.Text)) <> Upper(Trim(sle_from_sales_order.Text)) Then
	This.Text = Upper( This.Text)
ELSE
	MessageBox( This.Text, "You can not copy rows to the same sales order")
	This.Text = ""
END IF

end on

on getfocus;This.SelectText(1, Len(This.Text))
end on

type st_1 from statictext within w_move_product_response
integer x = 498
integer y = 16
integer width = 407
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "&To Sales Order #"
boolean focusrectangle = false
end type

type st_2 from statictext within w_move_product_response
integer y = 16
integer width = 485
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "From Sales Order #"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_from_sales_order from singlelineedit within w_move_product_response
integer x = 59
integer y = 108
integer width = 325
integer height = 88
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean autohscroll = false
textcase textcase = upper!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

