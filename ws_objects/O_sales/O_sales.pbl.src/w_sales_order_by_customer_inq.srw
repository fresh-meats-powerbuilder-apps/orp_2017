﻿$PBExportHeader$w_sales_order_by_customer_inq.srw
$PBExportComments$The Inquire box for sales order by customer
forward
global type w_sales_order_by_customer_inq from w_base_response_ext
end type
type dw_sales_open from u_base_dw_ext within w_sales_order_by_customer_inq
end type
type dw_plant from u_sales_ship_location within w_sales_order_by_customer_inq
end type
end forward

global type w_sales_order_by_customer_inq from w_base_response_ext
integer x = 55
integer y = 552
integer width = 1719
integer height = 1392
string title = "Sales Orders by Customer Inquire"
long backcolor = 12632256
dw_sales_open dw_sales_open
dw_plant dw_plant
end type
global w_sales_order_by_customer_inq w_sales_order_by_customer_inq

type variables
Boolean		ib_Filtered
String		is_openstring, is_division, is_plant

w_sales-orders-by-customer iw_Parent
end variables

event ue_postopen;call super::ue_postopen;Date					ldt_ship_date, &
						ldt_to_ship_date, &
						ldt_delivery_date, &
						ldt_to_delivery_date

Integer				li_rtn

String				ls_ship_date, &
						ls_to_ship_date, &
						ls_delivery_date, &
						ls_to_delivery_date, &
						ls_customer, &
						ls_div_code, &
						ls_psqi, &
						ls_division_po, &
						ls_corporate_po, &
						ls_ship_date_ind, &
						ls_delivery_date_ind, &
						ls_ship_location_ind, &
						ls_ship_location, &
						ls_load_status_ind, &
						ls_load_status, &
						ls_micromessage, &
						ls_linked_orders_ind	

DataWindowChild	ldwc_ship_loc

u_string_functions	lu_string_functions
u_project_functions	lu_project_functions

dw_Sales_open.InsertRow(0)
dw_plant.InsertRow(0)

If Not lu_string_functions.nf_IsEmpty(is_openstring) Then
	ls_customer = lu_string_functions.nf_gettoken(is_openstring, '~t')
	
	ls_division_po = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_corporate_po = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_div_code = lu_string_functions.nf_gettoken(is_openstring, '~t')

	ls_ship_date_ind = lu_string_functions.nf_gettoken(is_openstring, '~t')

	ls_ship_date = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ldt_ship_date = Date(ls_ship_date)

	ls_to_ship_date = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ldt_to_ship_date = Date(ls_to_ship_date)
	
	ls_psqi = lu_string_functions.nf_gettoken(is_openstring, '~t')
	
	ls_delivery_date_ind = lu_string_functions.nf_gettoken(is_openstring, '~t')
	
	ls_delivery_date = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ldt_delivery_date = Date(ls_delivery_date)

	ls_to_delivery_date = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ldt_to_delivery_date = Date(ls_to_delivery_date)
	
	ls_load_status_ind = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_load_status = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_ship_location_ind = lu_string_functions.nf_gettoken(is_openstring, '~t')
	
	ls_ship_location = lu_string_functions.nf_gettoken(is_openstring, '~t')
	
	ls_linked_orders_ind = lu_string_functions.nf_gettoken(is_openstring, '~t')

	dw_Sales_open.SetItem(1, "customer_id", ls_customer)
	dw_Sales_open.SetItem(1, "divisional_po", ls_division_po)
	dw_Sales_open.SetItem(1, "corporate_po", ls_corporate_po)
	dw_Sales_open.SetItem(1, "div_code", ls_div_code)
	dw_Sales_open.SetItem(1, "ship_date_ind", ls_ship_date_ind)
	dw_Sales_open.SetItem(1, "ship_date", ldt_ship_date)
	dw_Sales_open.SetItem(1, "to_ship_date", ldt_to_ship_date)
	dw_Sales_open.SetItem(1, "prod_short_que_ind", ls_psqi)
	dw_Sales_open.SetItem(1, "delivery_date_ind", ls_delivery_date_ind)
	dw_Sales_open.SetItem(1, "delivery_date", ldt_delivery_date)
	dw_Sales_open.SetItem(1, "to_delivery_date", ldt_to_delivery_date)
	dw_Sales_open.SetItem(1, "load_status_ind", ls_load_status_ind)
	dw_Sales_open.SetItem(1, "load_status", ls_load_status)
	dw_Sales_open.SetItem(1, "ship_plant_ind", ls_ship_location_ind)
	dw_Sales_open.SetITem(1, "show_linked_orders_ind", ls_linked_orders_ind)
	dw_plant.SetItem(1, "location_code", ls_ship_location)
	
	
	
	If ls_ship_date_ind = 'Y' Then
		dw_sales_open.Object.ship_date.Visible = 1
		dw_sales_open.Object.ship_date.protect = FALSE
		dw_sales_open.Modify("ship_date.background.color = '16777215'")
		dw_sales_open.Object.to_ship_date.Visible = 1
		dw_sales_open.Object.to_ship_date.protect = FALSE
		dw_sales_open.Modify("to_ship_date.background.color = '16777215'")
	Else
		dw_sales_open.Object.ship_date.Visible = 0
		dw_sales_open.Object.ship_date.protect = TRUE
		dw_sales_open.Modify("ship_date.background.color = '12632256'")
		dw_sales_open.Object.to_ship_date.Visible = 0
		dw_sales_open.Object.to_ship_date.protect = TRUE
		dw_sales_open.Modify("to_ship_date.background.color = '12632256'")
	End If

	If ls_delivery_date_ind = 'Y' Then
		dw_sales_open.Object.delivery_date.Visible = 1
		dw_sales_open.Object.delivery_date.protect = FALSE
		dw_sales_open.Modify("delivery_date.background.color = '16777215'")
		dw_sales_open.Object.to_delivery_date.Visible = 1
		dw_sales_open.Object.to_delivery_date.protect = FALSE
		dw_sales_open.Modify("to_delivery_date.background.color = '16777215'")
	Else
		dw_sales_open.Object.delivery_date.Visible = 0
		dw_sales_open.Object.delivery_date.protect = TRUE
		dw_sales_open.Modify("delivery_date.background.color = '12632256'")
		dw_sales_open.Object.to_delivery_date.Visible = 0
		dw_sales_open.Object.to_delivery_date.protect = TRUE
		dw_sales_open.Modify("to_delivery_date.background.color = '12632256'")	
	End If

	If ls_load_status_ind = 'Y' Then
		dw_sales_open.Object.load_status.Visible = 1
		dw_sales_open.Object.load_status.protect = FALSE
		dw_sales_open.Modify("load_status.background.color = '16777215'")
	Else
		dw_sales_open.Object.load_status.Visible = 0
		dw_sales_open.Object.load_status.protect = TRUE
		dw_sales_open.Modify("load_status.background.color = '12632256'")
	End If

	If ls_ship_location_ind = 'Y' Then
		dw_plant.uf_fill_plant_and_descr(ls_ship_location)
		dw_plant.enable()
		dw_plant.uf_show_location_code()
	Else
		dw_plant.disable()
		dw_plant.uf_hide_ship_location()
	End If
ELSE
	dw_Sales_open.SetItem(1, "customer_id", " ")
	dw_Sales_open.SetItem(1, "divisional_po", " ")
	dw_Sales_open.SetItem(1, "corporate_po", " ")
	dw_Sales_open.SetItem(1, "div_code", " ")
	dw_Sales_open.SetItem(1, "ship_date_ind", "Y")
	dw_Sales_open.SetItem(1, "ship_date", Today())
	dw_Sales_open.SetItem(1, "to_ship_date", RelativeDate(Today(), 365))
	dw_Sales_open.SetItem(1, "prod_short_que_ind", " ")
	dw_Sales_open.SetItem(1, "delivery_date_ind", "N")
	dw_Sales_open.SetItem(1, "delivery_date", Today())
	dw_Sales_open.SetItem(1, "to_delivery_date", RelativeDate(Today(), 365))
	dw_Sales_open.SetItem(1, "load_status_ind", "N")
	dw_Sales_open.SetItem(1, "load_status", "U")
	dw_Sales_open.SetItem(1, "ship_plant_ind", "N")
	dw_Sales_open.SetItem(1, "show_linked_orders_ind", "N")

	dw_sales_open.Object.delivery_date.Visible = 0
	dw_sales_open.Object.delivery_date.protect = TRUE
	dw_sales_open.Modify("delivery_date.background.color = '12632256'")
	dw_sales_open.Object.to_delivery_date.Visible = 0
	dw_sales_open.Object.to_delivery_date.protect = TRUE
	dw_sales_open.Modify("to_delivery_date.background.color = '12632256'")	

	dw_sales_open.Object.load_status.Visible = 0
	dw_sales_open.Object.load_status.protect = TRUE
	dw_sales_open.Modify("load_status.background.color = '12632256'")

	dw_plant.disable()
	dw_plant.uf_hide_ship_location()
END IF

dw_sales_open.SetColumn("customer_id")
dw_Sales_open.SelectText(1,10)

dw_plant.SetColumn("location_code")
dw_plant.SelectText(1, 3)

dw_plant.SetColumn("location_name")
dw_plant.SelectText(1, 29)

dw_sales_open.SetFocus()

end event

event open;call super::open;DataWindowChild	ldwc_customer, ldwc_ship_loc

is_openstring = message.stringparm
//iw_parent = Message.PowerObjectParm

dw_Sales_open.GetChild( "customer_id", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_Sales_open.GetChild( "customer_id_Name", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_Sales_open.GetChild( "customer_id_City", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)

dw_sales_open.getchild( "div_code", ldwc_customer)
iw_frame.iu_project_functions.nf_gettutltype(ldwc_customer, "divcode")

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', 'PK')
ldwc_customer.SetItem(1, 'type_desc', 'ALL PORK DIVISIONS')

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', 'BF')
ldwc_customer.SetItem(1, 'type_desc', 'ALL BEEF DIVISIONS')

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', '  ')
ldwc_customer.SetItem(1, 'type_desc', 'ALL DIVISIONS')

end event

on w_sales_order_by_customer_inq.create
int iCurrent
call super::create
this.dw_sales_open=create dw_sales_open
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_open
this.Control[iCurrent+2]=this.dw_plant
end on

on w_sales_order_by_customer_inq.destroy
call super::destroy
destroy(this.dw_sales_open)
destroy(this.dw_plant)
end on

event ue_base_ok;call super::ue_base_ok;Integer			li_length
String			ls_value, &
					ls_input_string, &
					ls_customer_id, &
					ls_customer_id_name, &
					ls_divisonal_po, &
					ls_corporate_po, &
					ls_div_code, &
					ls_ship_date_ind, &
					ls_ship_date, &
					ls_to_ship_date, &
					ls_prod_short_que, &
					ls_load_status_ind, &
					ls_load_status, &
					ls_delivery_date_ind, &
					ls_delivery_date, &
					ls_to_delivery_date, &
					ls_ship_plant_ind, &
					ls_ship_plant, &
					ls_ship_plant_desc, &
					ls_delv_date_comp, &
					ls_to_delv_date_comp, &
					ls_ship_date_comp, &
					ls_to_ship_date_comp, &
					ls_show_linked_orders

u_string_functions	lu_string_functions

If dw_sales_open.AcceptText() = -1 then return
If dw_plant.AcceptText() = -1 then return

//ls_value = Upper(dw_sales_open.Describe("DataWindow.Data"))					//RevGLL
//CloseWithReturn( This, Upper(dw_sales_open.Describe("DataWindow.Data")))	//RevGLL

ls_customer_id = dw_sales_open.GetItemString(1, "customer_id")
ls_customer_id_name = dw_sales_open.GetItemString(1, "customer_id_name")
ls_divisonal_po = dw_sales_open.GetItemString(1, "divisional_po")
ls_corporate_po = dw_sales_open.GetItemString(1, "corporate_po")
ls_div_code = dw_sales_open.GetItemString( 1, "div_code")
ls_ship_date_ind = dw_sales_open.GetItemString(1, "ship_date_ind")
ls_ship_date = String(dw_sales_open.GetItemDate(1, "ship_date"), 'mm/dd/yyyy')
ls_ship_date_comp = String(dw_sales_open.GetItemDate(1, "ship_date"), 'yyyymmdd')
ls_to_ship_date = String(dw_sales_open.GetItemDate(1, "to_ship_date"), 'mm/dd/yyyy')
ls_to_ship_date_comp = String(dw_sales_open.GetItemDate(1, "to_ship_date"), 'yyyymmdd')
ls_prod_short_que = dw_sales_open.GetItemString(1, "prod_short_que_ind")
ls_load_status_ind = dw_sales_open.GetItemString(1, "load_status_ind")
ls_load_status = dw_sales_open.GetItemString(1, "load_status")
ls_delivery_date_ind = dw_sales_open.GetItemString(1, "delivery_date_ind")
ls_delivery_date = String(dw_sales_open.GetItemDate(1, "delivery_date"), 'mm/dd/yyyy')
ls_delv_date_comp = String(dw_sales_open.GetItemDate(1, "delivery_date"), 'yyyymmdd')
ls_to_delivery_date = String(dw_sales_open.GetItemDate(1, "to_delivery_date"), 'mm/dd/yyyy')
ls_to_delv_date_comp = String(dw_sales_open.GetItemDate(1, "to_delivery_date"), 'yyyymmdd')
ls_ship_plant_ind = dw_sales_open.GetItemString(1, "ship_plant_ind")
ls_ship_plant = dw_plant.uf_get_plant_code()
ls_ship_plant_desc = dw_plant.uf_get_plant_descr()
ls_show_linked_orders = dw_sales_open.GetItemString(1, "show_linked_orders_ind")

If IsNull(ls_customer_id) Then ls_customer_id = ' '
If IsNull(ls_customer_id_name) Then	ls_customer_id_name = ' '
If IsNull(ls_divisonal_po) Then ls_divisonal_po = ' '
If IsNull(ls_corporate_po) Then	ls_corporate_po = ' '
If IsNull(ls_div_code) Then	ls_div_code = ' '
If IsNull(ls_ship_date_ind) Then	ls_ship_date_ind = ' '
If IsNull(ls_ship_date) Then	ls_ship_date = ' '
If IsNull(ls_to_ship_date) Then	ls_to_ship_date = ' '
If IsNull(ls_prod_short_que) Then	ls_prod_short_que = ' '
If IsNull(ls_load_status_ind) Then	ls_load_status_ind = ' '
If IsNull(ls_load_status) Then	ls_load_status = ' '
If IsNull(ls_delivery_date_ind) Then	ls_delivery_date_ind = ' '
If IsNull(ls_delivery_date) Then	ls_delivery_date = ' '
If IsNull(ls_to_delivery_date) Then	ls_to_delivery_date = ' '
If IsNull(ls_ship_plant_ind) Then	ls_ship_plant_ind = ' '
If IsNull(ls_ship_plant) Then	ls_ship_plant = ' '
If IsNull(ls_ship_plant_desc) Then	ls_ship_plant_desc = ' '
If IsNull(ls_show_linked_orders) Then	ls_show_linked_orders = ' '

// Run a quick validation test.
If ls_ship_date_ind = 'Y' Then
	If ls_ship_date_comp > ls_to_ship_date_comp Then
		dw_sales_open.SetFocus()
		MessageBox("Ship Date Range Error", "Ship Date can not be greater than Ship To Date.")
		Return
	End If
End If

If ls_delivery_date_ind = 'Y' Then
	If ls_delv_date_comp > ls_to_delv_date_comp then
		dw_sales_open.SetFocus()
		MessageBox("Delivery Date Range Error", "Delivery Date can not be greater than Delivery To Date.")
		Return
	End If
End If

If ls_ship_plant_ind = 'Y' And &
	ls_ship_plant = "" Then 
	dw_plant.SetFocus()
	MessageBox("Ship Location Error", "Invalid Ship Location Selected. Please select a valid Location.")
	Return
Else
	dw_plant.uf_set_plant_profile(ls_ship_plant)
End If

If ls_ship_plant_ind = 'Y' And &
	ls_ship_plant_desc = "" Then
	dw_plant.uf_set_plant_code(ls_ship_plant)
End If

//	Messagebox(Message.StringParm,'')		//RevGLL

ls_input_string = ls_customer_id + '~t' + &
						ls_customer_id_name + '~t' + &
						ls_divisonal_po + '~t' + &
						ls_corporate_po + '~t' + &
						ls_div_code + '~t' + &
						ls_ship_date_ind + '~t' + &
						ls_ship_date + '~t' + &
						ls_to_ship_date + '~t' + &
						ls_prod_short_que + '~t' + &
						ls_load_status_ind + '~t' + &
						ls_load_status + '~t' + &
						ls_delivery_date_ind + '~t' + &
						ls_delivery_date + '~t' + &
						ls_to_delivery_date + '~t' + &
						ls_ship_plant_ind + '~t' + &
						ls_ship_plant + '~t' + &
						ls_ship_plant_desc + '~t' + &
						ls_show_linked_orders + '~t'

CloseWithReturn(This, ls_input_string)
end event

event ue_base_cancel;call super::ue_base_cancel;iw_frame.setmicrohelp('Ready')
CloseWithReturn(This, "Cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_sales_order_by_customer_inq
boolean visible = false
integer x = 750
integer y = 336
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_sales_order_by_customer_inq
integer x = 1408
integer y = 148
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_sales_order_by_customer_inq
integer x = 1408
integer y = 32
integer taborder = 40
end type

type cb_browse from w_base_response_ext`cb_browse within w_sales_order_by_customer_inq
integer taborder = 20
end type

type dw_sales_open from u_base_dw_ext within w_sales_order_by_customer_inq
event ue_postitemchanged ( )
integer width = 1381
integer height = 1248
integer taborder = 10
string dataobject = "d_sales_order_by_cust_inq"
boolean border = false
end type

event ue_postitemchanged;call super::ue_postitemchanged;This.SetItem(1, 'div_code', '  ')

end event

event itemchanged;call super::itemchanged;String					ls_column_name, ls_ship_plant
DataWindowChild		ldwc_Temp
u_string_functions	lu_string_functions

CHOOSE CASE This.GetColumnName()
// dld GTL project allow for Customer to be blank		
//CASE "customer_id"
//	If lu_string_functions.nf_isempty(data) Then
//	iw_frame.SetMicroHelp('Customer Id is a required field.')
//		Return 1
//	End If	
Case 'div_code'
	If data = 'ALL DIVISIONS' Then
		This.PostEvent('ue_postitemchanged')
	End IF
	dw_sales_open.GetChild("div_code",ldwc_Temp)
	IF ldwc_Temp.Find("type_code = '"+data+"'",1, ldwc_Temp.RowCount()) < 1 then
		iw_frame.SetMicrohelp('Division code is invalid')
		This.SetItem(1, 'div_code', '  ')
		This.SetColumn('div_code')
		Return 1
	END IF
Case 'ship_date_ind'
	If data = 'Y' Then
		dw_sales_open.Object.ship_date.Visible = 1
		dw_sales_open.Object.ship_date.protect = FALSE
		dw_sales_open.Modify("ship_date.background.color = '16777215'")
		dw_sales_open.Object.to_ship_date.Visible = 1
		dw_sales_open.Object.to_ship_date.protect = FALSE
		dw_sales_open.Modify("to_ship_date.background.color = '16777215'")
	Else
		dw_sales_open.Object.ship_date.Visible = 0
		dw_sales_open.Object.ship_date.protect = TRUE
		dw_sales_open.Modify("ship_date.background.color = '12632256'")
		dw_sales_open.Object.to_ship_date.Visible = 0
		dw_sales_open.Object.to_ship_date.protect = TRUE
		dw_sales_open.Modify("to_ship_date.background.color = '12632256'")
	End If
Case 'delivery_date_ind'
	If data = 'Y' Then
		dw_sales_open.Object.delivery_date.Visible = 1
		dw_sales_open.Object.delivery_date.protect = FALSE
		dw_sales_open.Modify("delivery_date.background.color = '16777215'")
		dw_sales_open.Object.to_delivery_date.Visible = 1
		dw_sales_open.Object.to_delivery_date.protect = FALSE
		dw_sales_open.Modify("to_delivery_date.background.color = '16777215'")
	Else
		dw_sales_open.Object.delivery_date.Visible = 0
		dw_sales_open.Object.delivery_date.protect = TRUE
		dw_sales_open.Modify("delivery_date.background.color = '12632256'")
		dw_sales_open.Object.to_delivery_date.Visible = 0
		dw_sales_open.Object.to_delivery_date.protect = TRUE
		dw_sales_open.Modify("to_delivery_date.background.color = '12632256'")	
	End If
Case 'load_status_ind'
	If data = 'Y' Then
		dw_sales_open.Object.load_status.Visible = 1
		dw_sales_open.Object.load_status.protect = FALSE
		dw_sales_open.Modify("load_status.background.color = '16777215'")
	Else
		dw_sales_open.Object.load_status.Visible = 0
		dw_sales_open.Object.load_status.protect = TRUE
		dw_sales_open.Modify("load_status.background.color = '12632256'")
	End If
Case 'ship_plant_ind'
	If data = 'Y' Then
		dw_plant.uf_fill_plant_and_descr(ls_ship_plant)
		dw_plant.enable()
		dw_plant.uf_show_location_code()
	Else
		dw_plant.disable()
		dw_plant.uf_hide_ship_location()
	End If
END CHOOSE
iw_frame.setmicrohelp('Ready')
end event

event itemerror;call super::itemerror;Choose Case This.GetColumnName()
	Case 'div_code'
		If data = 'ALL DIVISIONS' Then
			This.PostEvent('ue_postitemchanged')
		Else
			iw_frame.SetMicrohelp('Division code is invalid - Select a valid Division')
			Return 1
		End IF
	Case Else
		Return 1
End Choose

Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;iw_frame.setmicrohelp('Ready')
end event

type dw_plant from u_sales_ship_location within w_sales_order_by_customer_inq
integer x = 114
integer y = 1052
integer taborder = 30
boolean bringtotop = true
end type

event getfocus;call super::getfocus;This.SetRow(1)
This.SetColumn(1)
This.SelectText(1,3)
end event

