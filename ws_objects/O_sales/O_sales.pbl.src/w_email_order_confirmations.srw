﻿$PBExportHeader$w_email_order_confirmations.srw
forward
global type w_email_order_confirmations from w_base_response_ext
end type
type dw_customer_email_address from u_base_dw_ext within w_email_order_confirmations
end type
end forward

global type w_email_order_confirmations from w_base_response_ext
integer width = 3342
integer height = 844
string title = "Email Order Confirmation"
long backcolor = 12632256
dw_customer_email_address dw_customer_email_address
end type
global w_email_order_confirmations w_email_order_confirmations

type variables
string		is_sales_order_id
Char			ic_process_option_in

s_error		istr_error_info
u_orp001		iu_orp001
u_orp002		iu_orp002
u_ws_orp1		iu_ws_orp1
u_ws_orp4       iu_ws_orp4


Boolean  		ib_modified = false, &
		ib_updating , &
		ib_reinquire, &
		ib_retrieve, &
		ib_updateable
end variables

forward prototypes
public subroutine wf_get_customer_email_address ()
public subroutine wf_default_new_row (integer ai_row, integer ai_row_new)
end prototypes

public subroutine wf_get_customer_email_address ();String		ls_sales_id, &
				ls_customer_id, &
				ls_division_id, &
				ls_customer_email_address, &
				ls_input, &
				ls_output
			
Integer		li_rtn

Long			ll_rowcount_detail, & 
				ll_rowcount_ds, &
				ll_rowcount, &
				ll_count, &
				ll_pos

datastore	lds_email_orders
u_string_functions	lu_string_functions			

ls_input = is_sales_order_id + '~r~n'
istr_error_info.se_event_name = "wf_get_customer_email_address"			  

//li_rtn = iu_orp001.nf_orpo15br(istr_error_info, &
//								    ls_input, &
//								 ls_output)
										
li_rtn = iu_ws_orp1.nf_orpo15fr(istr_error_info, &
									    ls_input, &
										 ls_output)										

dw_customer_email_address.Reset()
dw_customer_email_address.ImportString(ls_output)

ll_rowcount = dw_customer_email_address.RowCount() 

//if (dw_customer_email_address.getitemstring(ll_rowcount, 'customer_email_address') <> '                                                                                ' ) then
//	if ll_rowcount < 50 then 
//		dw_customer_email_address.InsertRow(0)
//		wf_default_new_row()
//	end if
//end if




//	if ll_rowcount < 50 then
//	
//		dw_customer_email_address.InsertRow(0)
//		wf_default_new_row()
//		
//	end if


end subroutine

public subroutine wf_default_new_row (integer ai_row, integer ai_row_new);//dw_customer_email_address.SetItem(dw_customer_email_address.RowCount(), 'sales_order_id', &
//                     dw_customer_email_address.GetItemString(1, 'sales_order_id'))
//                                                                                                                
//dw_customer_email_address.SetItem(dw_customer_email_address.RowCount(), 'division_id', &
//                     dw_customer_email_address.GetItemString(1, 'division_id'))
//                                                                                                                
//dw_customer_email_address.SetItem(dw_customer_email_address.RowCount(), 'customer_id', &
//                     dw_customer_email_address.GetItemString(1, 'customer_id'))
							
							
dw_customer_email_address.SetItem(ai_row_new, 'sales_order_id', &
                     dw_customer_email_address.GetItemString(ai_row, 'sales_order_id'))
                                                                                                                
dw_customer_email_address.SetItem(ai_row_new, 'division_id', &
                     dw_customer_email_address.GetItemString(ai_row, 'division_id'))
                                                                                                                
dw_customer_email_address.SetItem(ai_row_new, 'customer_id', &
                     dw_customer_email_address.GetItemString(ai_row, 'customer_id'))
end subroutine

on w_email_order_confirmations.create
int iCurrent
call super::create
this.dw_customer_email_address=create dw_customer_email_address
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_customer_email_address
end on

on w_email_order_confirmations.destroy
call super::destroy
destroy(this.dw_customer_email_address)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_order_out, &
         ls_email_test, &
			ls_customer_email, &
			ls_order_in, &
			ls_order_test

Integer	li_rtn

Long 	tot_row, ll_row

u_string_functions	lu_string_functions	

istr_error_info.se_event_name = 'ue_base_ok'
ic_process_option_in = '1'

if dw_customer_email_address.accepttext() = -1 then return

tot_row = dw_customer_email_address.rowcount()
Do Until ll_row = tot_row
	ll_row = ll_row + 1
	//jac 1-14
	ls_email_test = dw_customer_email_address.Getitemstring(ll_row, 'customer_email_address')
	ls_order_test = dw_customer_email_address.Getitemstring(ll_row, 'sales_order_id')
	If IsNull(ls_email_test) Then
		ls_email_test = " "
	End If
	//ls_customer_email += dw_customer_email_address.Getitemstring(ll_row, 'customer_email_address') + '~t'
	If Not lu_string_functions.nf_IsEmpty(ls_email_test) Then
		ls_customer_email += ls_email_test + '~t'
		ls_order_in += ls_order_test + '~t'
	end if
LOOP

IF NOT isValid(iu_orp002) THEN
	iu_orp002 = Create u_orp002
End IF

//TESTING
li_rtn = iu_ws_orp4.nf_ORPO80FR(istr_error_info, &
										ic_process_option_in, &
										ls_order_in, &
										ls_order_out, &
										ls_customer_email)

IF li_rtn >= 0 THEN
	Close(w_email_order_confirmations)
	Return
END IF
end event

event ue_postopen;call super::ue_postopen;This.wf_get_customer_email_address()
end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp1 = Create u_ws_orp1
iu_ws_orp4 = Create u_ws_orp4
is_sales_order_id = Message.StringParm
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_email_order_confirmations
boolean visible = false
integer x = 1440
integer y = 276
string text = ""
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_email_order_confirmations
integer x = 1577
integer y = 612
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_email_order_confirmations
integer x = 1266
integer y = 612
end type

type cb_browse from w_base_response_ext`cb_browse within w_email_order_confirmations
end type

type dw_customer_email_address from u_base_dw_ext within w_email_order_confirmations
event tabout pbm_dwntabout
integer x = 37
integer y = 32
integer width = 3255
integer height = 448
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_customer_email_address"
boolean vscrollbar = true
boolean ib_firstcolumnonnextrow = true
end type

event tabout;long      ll_row
long      ll_rowcount

ll_row = this.GetRow()
ll_rowcount = this.RowCount()

IF ll_row > 0 AND ll_row < ll_rowcount THEN
      this.SetRow(ll_Row + 1)
            
      // have to post a setfocus to make sure we stay in the control
      this.function POST SetFocus()
END IF


end event

event itemchanged;call super::itemchanged;string				ls_temp, &
						ls_customer_email, &
						ls_order_1, &
						ls_order_2

long					ll_rowfound, &
						ll_rowcount



ll_rowcount = this.RowCount() 

//if ll_rowcount < 50 then
//  IF This.ib_NewRowOnChange AND (This.il_CurrentRow + 1 = This.RowCount()) THEN
//                wf_default_new_row()
//  END IF
//end if 

ls_order_1 = dw_customer_email_address.Getitemstring(row, 'sales_order_id')
if row = ll_rowcount then
	ls_order_2 = " "
else
	ls_order_2 = dw_customer_email_address.Getitemstring(row + 1, 'sales_order_id')
end if

if ls_order_1 <> ls_order_2 then
	this.insertrow(row + 1)
	wf_default_new_row(row, row + 1)
end if

end event

event constructor;call super::constructor;//ib_NewRowOnChange = True
ib_firstcolumnonnextrow = True

end event

