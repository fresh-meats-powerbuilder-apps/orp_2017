﻿$PBExportHeader$w_confirmed_order.srw
forward
global type w_confirmed_order from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_confirmed_order
end type
type dw_stop from u_base_dw_ext within w_confirmed_order
end type
type dw_header from u_base_dw_ext within w_confirmed_order
end type
end forward

global type w_confirmed_order from w_base_sheet_ext
int Width=2963
int Height=1457
boolean TitleBar=true
string Title="w_order_confirmation"
dw_detail dw_detail
dw_stop dw_stop
dw_header dw_header
end type
global w_confirmed_order w_confirmed_order

on w_confirmed_order.create
int iCurrent
call w_base_sheet_ext::create
this.dw_detail=create dw_detail
this.dw_stop=create dw_stop
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_detail
this.Control[iCurrent+2]=dw_stop
this.Control[iCurrent+3]=dw_header
end on

on w_confirmed_order.destroy
call w_base_sheet_ext::destroy
destroy(this.dw_detail)
destroy(this.dw_stop)
destroy(this.dw_header)
end on

type dw_detail from u_base_dw_ext within w_confirmed_order
int X=10
int Y=929
int Width=2789
int Height=269
int TabOrder=20
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
end type

type dw_stop from u_base_dw_ext within w_confirmed_order
int X=10
int Y=765
int Width=1674
int Height=169
int TabOrder=30
string DataObject="d_order_confirmed_stop"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type dw_header from u_base_dw_ext within w_confirmed_order
int X=10
int Y=29
int Width=2789
int Height=729
string DataObject="d_order_confirmed_header"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
end type

