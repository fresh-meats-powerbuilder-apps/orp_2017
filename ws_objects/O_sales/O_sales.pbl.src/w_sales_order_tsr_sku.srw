﻿$PBExportHeader$w_sales_order_tsr_sku.srw
forward
global type w_sales_order_tsr_sku from w_base_sheet_ext
end type
type dw_customer_data from u_base_dw_ext within w_sales_order_tsr_sku
end type
type cbx_price_over from checkbox within w_sales_order_tsr_sku
end type
type dw_detail from u_base_dw_ext within w_sales_order_tsr_sku
end type
type dw_input from u_base_dw_ext within w_sales_order_tsr_sku
end type
end forward

shared variables

end variables

global type w_sales_order_tsr_sku from w_base_sheet_ext
integer x = 82
integer y = 452
integer width = 4992
integer height = 1656
string title = "Sales Orders by TSR/SKU"
event ue_retrieve pbm_custom02
event ue_print_fax ( )
event ue_print ( )
event ue_fax ( )
event ue_edi ( )
event ue_tla ( )
event type integer ue_prod_shrtg_que ( )
event ue_complete_order ( )
event type integer ue_post_active ( )
event type integer ue_reinquire ( )
event ue_email ( )
event ue_tla_email ( )
event ue_tla_local_print ( )
event ue_tla_view ( )
event ue_order_acceptance_queue ( )
dw_customer_data dw_customer_data
cbx_price_over cbx_price_over
dw_detail dw_detail
dw_input dw_input
end type
global w_sales_order_tsr_sku w_sales_order_tsr_sku

type prototypes

end prototypes

type variables
// Netwise Instance variables
Boolean		ib_nv_created, ib_reretrieve, &
		ib_incomplete, ib_find_incomplete, &
                                ib_first_time, ib_update_detail = FALSE
		
String 	is_from_window,&
	is_OpeningString ,&
	is_Sales_ID, &
	is_header_info, &
	is_products, &
	is_shtg_qu_ind, &
	is_type_ind, &
	is_oper_customer_info
	  
					 

Window	iw_sales_detail

Long	il_current_column

u_orp002		iu_orp002
u_orp204		iu_orp204
u_ws_orp4       iu_ws_orp4

s_error		istr_error_info

CHAR	ic_process_option_in, ic_process_tla_option_in

Int ii_rpc_retvalue

w_sales_order_tsr_sku_inq  iw_child
end variables

forward prototypes
public function string wf_get_current_setting ()
public function boolean wf_notepad ()
public subroutine wf_set_iw_sales_detail (w_sales_order_detail iw_window)
public function boolean wf_inquire (double ad_tasknumber, integer ai_pagenumber)
public function boolean wf_getselectedorders (string as_order_selected)
public function integer wf_check_order_status ()
public function integer wf_findincomplete ()
public subroutine wf_reset_order_status (string as_modified)
public function integer wf_mod_sub_lines ()
public subroutine wf_set_net_shortage ()
public function integer wf_close_query ()
public subroutine wf_gettypedesc (ref datawindowchild ldw_childdw)
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

event ue_print_fax();INTEGER	li_rtn

String	ls_order_id, ls_order_out, ls_temp, ls_customer_email

Long 	ll_row

ll_row = dw_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_detail.Getitemstring(ll_row, 'order_status') 
	IF ls_temp = 'A' THEN
			ls_order_id += dw_detail.Getitemstring(ll_row, 'order_id') + '~t'
	END IF
	ll_row = dw_detail.Getselectedrow(ll_row)
LOOP

istr_error_info.se_event_name = 'ue_print_fax'

IF NOT isValid(iu_orp002) THEN
	iu_orp002 = Create u_orp002
End IF

//li_rtn = iu_orp002.nf_orpo80br_print_fax(istr_error_info, &
//										ic_process_option_in, &
//										ls_order_id, &
//										ls_order_out, &
//										ls_customer_email, &
//										ic_process_tla_option_in)
										
										
li_rtn = iu_ws_orp4.nf_orpo80fr(istr_error_info, &
										ic_process_option_in, &
										ls_order_id, &
										ls_order_out, &
										ls_customer_email, &
										ic_process_tla_option_in)										
										
										

dw_detail.SelectRow(0, False)
il_current_column = 0
dw_detail.SetColumn(6)

IF li_rtn >= 0 THEN
	This.SetRedraw(TRUE)
	Return
END IF
end event

event ue_print;call super::ue_print;ic_process_option_in = 'P'

This.triggerevent('ue_print_fax')
end event

event ue_fax;call super::ue_fax;ic_process_option_in = 'F'

This.triggerevent('ue_print_fax')
end event

event ue_edi;ic_process_option_in = 'E'

This.triggerevent('ue_print_fax')
end event

event ue_tla();ic_process_option_in = 'A'
ic_process_tla_option_in = 'F'

This.triggerevent('ue_print_fax')

end event

event ue_prod_shrtg_que;call super::ue_prod_shrtg_que;return 1

end event

event ue_complete_order;call super::ue_complete_order;String	ls_OrderID, &
			ls_ColumnName
Integer	li_row
Window	lw_OrderDetail

u_string_functions	lu_string_function

If ClassName(GetFocus()) = 'dw_detail' Then
	li_row = dw_detail.GetRow()
   ls_OrderID = dw_detail.GetItemString(li_row, 'order_id')
Else
	setMicroHelp("Set focus on Order ID to complete an order")
	Return
End If
If lu_string_function.nf_IsEmpty(ls_OrderID) Then Return

ib_reretrieve = True
OpenSheetWithParm(lw_OrderDetail, ls_OrderID, 'w_sales_order_detail', iw_frame, 0, &
			iw_frame.im_menu.iao_ArrangeOpen)
Do 
Loop While Yield()
lw_OrderDetail.PostEvent('ue_complete_order')
end event

event ue_post_active;call super::ue_post_active;//If ib_reretrieve Then
//	This.SetRedraw(False)
//	dw_Detail.Reset()
//	This.wf_Inquire(0, 0)
//	wf_set_net_shortage()
//	wf_mod_sub_lines()
//	dw_detail.uf_changerowstatus ( 0, NotModified! )
//	This.SetRedraw(True)
//	ib_reretrieve = False
//End If
Return 1
end event

event type integer ue_reinquire();String ls_businessrule
Integer li_rowcount, li_count
This.SetRedraw(False)
dw_Detail.Reset()
This.wf_Inquire(0, 0)
wf_set_net_shortage()
wf_mod_sub_lines()
dw_detail.uf_changerowstatus ( 0, NotModified! )
This.SetRedraw(True)
//
li_rowcount = dw_detail.RowCount()
if li_rowcount > 0 then
	for li_count = 1 to li_rowcount
		ls_businessrule = Mid(dw_detail.GetItemString(li_count, "business_rule"),3,1)
		if ls_businessrule = 'V' Then
			if cbx_price_over.Enabled = true then
				cbx_price_over.Enabled = false
			end if
		end if
	next
end if
//	
ib_reretrieve = False

Return 1
end event

event ue_email();string	ls_temp, ls_order_id

long		ll_row

ll_row = dw_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_detail.Getitemstring(ll_row, 'order_status')
	if ls_temp = 'A' then
		ls_order_id += dw_detail.getitemstring(ll_row, 'order_id') + '~t'
	end if
	ll_row = dw_detail.getselectedrow(ll_row)
loop

if ls_order_id > ' ' then
	Openwithparm(w_email_order_confirmations, ls_order_id)
else
	iw_frame.SetMicroHelp( "Sales Order must be Accepted")
end if
end event

event ue_tla_email();ic_process_option_in = 'A'
ic_process_tla_option_in = 'E'

This.triggerevent('ue_print_fax')

end event

event ue_tla_local_print();ic_process_option_in = 'A'
ic_process_tla_option_in = 'P'

This.triggerevent('ue_print_fax')

end event

event ue_tla_view();ic_process_option_in = 'A'
ic_process_tla_option_in = 'V'

This.triggerevent('ue_print_fax')

end event

event ue_order_acceptance_queue();Window lw_temp


OpenSheetWithParm(lw_temp, is_Sales_ID, "w_order_acceptance_queue",iw_frame,0,iw_frame.im_menu.iao_arrangeopen) 
end event

public function string wf_get_current_setting ();String	ls_Return_String

ls_Return_String  = String(dw_input.GetItemDate( 1,"ship_date*")) + "~t"&
				 +dw_input.GetItemString( 1,"shipto_customer_id")
Return ls_Return_String
end function

public function boolean wf_notepad ();//String  ls_order_numbers
//
//ls_order_numbers  = This.wf_GetSelectedOrders()
//
//IF Len(Trim( ls_order_numbers)) > 0 Then
//	Open(w_salesordersbycustomernotepad)
//	IF Message.StringParm = 'Abort' Then
//		Return False
//	Else
//		iw_frame.wf_OpenSheet("w_orderbrowsebyproduct", Message.StringParm +","+ ls_order_numbers )
//		RETURN TRUE
//	END IF
//ELSE
//	MessageBox( "No lines selected", "You must select at least one line to go into the notepad")
//	Return FALSE
//END IF
RETURN FALSE
end function

public subroutine wf_set_iw_sales_detail (w_sales_order_detail iw_window);iw_sales_detail = iw_window
end subroutine

public function boolean wf_inquire (double ad_tasknumber, integer ai_pagenumber);INTEGER 	li_Rtn, li_cont, li_rowcount, li_count

CHARACTER	lc_indicator	

STRING	ls_detail, ls_businessrule

s_Error	ls_Error

lc_indicator = 'I'
ls_detail = ''
ls_Error.se_User_ID = Message.nf_getuserid()
dw_detail.SetRedraw(False)

li_Rtn = iu_ws_orp4.nf_orpo84fr(ls_Error, is_header_info, is_products, ls_detail, lc_indicator, 	is_oper_customer_info)

CHOOSE CASE li_Rtn		
	CASE 0
		dw_detail.ImportString(TRIM(ls_detail))
		li_rowcount = dw_detail.RowCount()
		if li_rowcount > 0 then
		for li_count = 1 to li_rowcount
			ls_businessrule = Mid(dw_detail.GetItemString(li_count, "business_rule"),3,1)
			if ls_businessrule = 'V' Then
				if cbx_price_over.Enabled = true then
					cbx_price_over.Enabled = false
				end if
			end if
		next
		end if
		IF ai_PageNumber <> 0 THEN	wf_Inquire(ad_TaskNumber, ai_PageNumber)
		dw_detail.uf_changerowstatus ( 0, NotModified! )
		dw_detail.SetRedraw(TRUE)
		il_current_column = 0
		cbx_price_over.checked = False

		RETURN TRUE
	CASE 1
		dw_detail.ImportString(TRIM(ls_detail))
		IF ai_PageNumber <> 0 THEN	wf_Inquire(ad_TaskNumber, ai_PageNumber)
		li_rowcount = dw_detail.RowCount()
		if li_rowcount > 0 then
			for li_count = 1 to li_rowcount
				ls_businessrule = Mid(dw_detail.GetItemString(li_count, "business_rule"),3,1)
				if ls_businessrule = 'V' Then
					if cbx_price_over.Enabled = true then
						cbx_price_over.Enabled = false
					end if
				end if
			next
		end if
		dw_detail.SetRedraw(TRUE)
		RETURN TRUE
	CASE ELSE
		dw_detail.SetRedraw(TRUE)
		RETURN FALSE
END CHOOSE
end function

public function boolean wf_getselectedorders (string as_order_selected);Long 	ll_row
String ls_temp

ll_row = dw_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_detail.Getitemstring(ll_row, 'order_id') 
	IF ls_temp = as_order_selected THEN
			Return True
	END IF
	ll_row = dw_detail.Getselectedrow(ll_row)
LOOP
Return False
//String 			ls_order_numbers_to_Return
//
//Long 				ll_rowCount,&
//					ll_nextselected
//
//ll_nextselected = dw_detail.GetSelectedRow(0)
//
//Do While ll_nextselected > 0
//	ls_order_numbers_to_Return+= dw_detail.GetItemString( ll_nextselected,"order_id")+"~t"
//	ls_order_numbers_to_Return+= dw_detail.GetItemString( ll_nextselected,"order_status")+"~t"
//	ls_order_numbers_to_Return+= "UnKnown~t"
//	ls_order_numbers_to_Return+= dw_detail.GetItemString( ll_nextselected,"ship_plant_code")+"~t"
//	ls_order_numbers_to_Return+= String(dw_detail.GetItemDate( ll_nextselected,"ship_date"),"MM/DD/YYYY")+"~t"
//	ls_order_numbers_to_Return+= String(dw_detail.GetItemDate( ll_nextselected,"delivery_date"), "MM/DD/YYYY")+"~t~r~n"
//	ll_nextselected = dw_detail.GetSelectedRow(ll_nextselected)
//Loop

//Return ls_order_numbers_to_Return
end function

public function integer wf_check_order_status ();Integer	li_rowcount, li_loop
String	ls_so_id
window	lw_detail[]

li_rowcount = dw_detail.rowcount()
ls_so_id = ' '
For li_loop = 1 to li_rowcount
	If dw_detail.object.order_status[li_loop] = ' ' Then
		If ls_so_id <> dw_detail.object.order_id[li_loop] Then
			ls_so_id = dw_detail.object.order_id[li_loop]
		
			OpenSheetWithParm(lw_detail[li_loop], ls_so_id, 'w_sales_order_detail', &
									iw_frame, 0, iw_Frame.im_menu.iao_ArrangeOpen)
		End If
	End IF
Next
Return 1
end function

public function integer wf_findincomplete ();Int						li_Counter
Long						ll_row, ll_row_count
String					ls_Status, ls_order
DataWindowChild		ldwc_status
u_string_functions	lu_string_function

ll_row_count = dw_detail.RowCount()
For li_Counter = 1 to ll_row_count
	ls_Order = dw_detail.GetItemString(li_Counter, 'order_status')
//	If lu_string_function.nf_IsEmpty(ls_Order) Then Continue
//	If dw_detail.object.order_status[li_Counter] = ' ' Then Return li_counter
	If ls_Order = ' ' Then Return li_counter
Next
Return 0
end function

public subroutine wf_reset_order_status (string as_modified);Long			ll_Row, ll_RowCount, ll_temp_count, ll_dw_row, ll_dw_count
STRING		ls_temp, ls_order, ls_expression
DataStore	lds_temp
//////////////////////////////////////////////////////////////////////////////////////////
ll_RowCount = dw_Detail.RowCount()

lds_temp = Create DataStore
lds_temp.DataObject = 'd_sales_tsr_sku_all'
lds_temp.ImportString(as_modified)
ll_temp_count = lds_temp.RowCount()
ls_order = ''
For ll_row = 1 To ll_temp_count
	If ls_order =  lds_temp.object.order_id[ll_row] Then
		
	Else
		ls_order = lds_temp.object.order_id[ll_row]
		//loop through dw_detail and change all order status for ls_order
		ll_dw_count = dw_detail.RowCount()
		ls_expression = "order_id = '" + ls_order + "'"
		ll_dw_row = dw_detail.Find(ls_expression,1,ll_dw_count)
		DO While ll_dw_row > 0
			dw_detail.object.order_status[ll_dw_row] = lds_temp.object.order_status[ll_row]
			ll_dw_row = dw_detail.Find(ls_expression,ll_dw_row + 1,ll_dw_count)
			If ll_dw_row = ll_dw_count Then 
				dw_detail.object.order_status[ll_dw_row] = lds_temp.object.order_status[ll_row]
				ll_dw_row = 0
			End If
		Loop 
	End If
Next
end subroutine

public function integer wf_mod_sub_lines ();Long			ll_count, ll_row, ll_row_total
String		ls_order, ls_line, ls_prev_order, ls_prev_line
Decimal		ld_decimal, ld_total, ld_temp
Boolean		lb_sub = False

//wf_set_net_shortage()

ll_count = dw_detail.RowCount()
If ll_count < 2 Then
	Return -1
End If
ls_prev_order = Trim(dw_detail.Object.order_id[1])
ls_prev_line = Trim(dw_detail.Object.Line_number[1])
ll_row_total = 1
ld_total = dw_detail.Object.ordered_units[1] - dw_detail.Object.scheduled_units[1]

For ll_row = 2 To ll_count
	ls_order = Trim(dw_detail.Object.order_id[ll_row])
	ls_line = Trim(dw_detail.Object.Line_number[ll_row])
//	If ls_order = ls_prev_order Then
//		dw_detail.Object.cutomer_id[ll_row].Visible = False
//		dw_detail.Object.ship_date[ll_row].Visible = False
//	End If
	If ls_order = ls_prev_order And Left(ls_line,2) = Left(ls_prev_line,2) Then
		//dw_detail.Object.net_shortages[ll_row].Visible = False
		dw_detail.Object.net_shortages[ll_row] = 0
		ld_temp = dw_detail.Object.scheduled_units[ll_row]
		ld_total = ld_total - dw_detail.Object.scheduled_units[ll_row]
		lb_sub = True
	Else
		//dw_detail.Object.net_shortages[ll_row_total].Visible = True
		If lb_sub Then
			dw_detail.Object.net_shortages[ll_row_total] = ld_total
			lb_sub = False
		End If
		ll_row_total = ll_row
		ld_total = dw_detail.Object.ordered_units[ll_row] - dw_detail.Object.scheduled_units[ll_row]
	End If
	ls_prev_order = ls_order
	ls_prev_line = ls_line
Next
Return 0
end function

public subroutine wf_set_net_shortage ();Long			ll_count, ll_row
Decimal		ld_decimal

ll_count = dw_detail.RowCount()
For ll_row = 1 To ll_count
	ld_decimal = dw_detail.Object.ordered_units[ll_row] - dw_detail.Object.scheduled_units[ll_row]
	If ld_decimal < 0 Then ld_decimal = 0
	dw_detail.Object.net_shortages[ll_row] = ld_decimal
Next
end subroutine

public function integer wf_close_query ();Integer	li_ret

li_ret = This.wf_findincomplete()
If li_ret > 0 Then
	If KeyDown(KeyControl!) And KeyDown(KeyAlt!) And KeyDown(KeyShift!) Then 
//		Message.returnValue = 0
	Else
		dw_detail.SetColumn('ordered_units')
		dw_detail.ScrollTORow(li_ret)
		MessageBox( "Incomplete Order", "There are outstanding incomplete~r~n" + &
						"orders pending.  You must complete~r~n" + &
						"all outstanding incomplete orders.", Exclamation!)
		dw_detail.SetFocus()
		ib_incomplete = True
//		Message.ReturnValue = 1
		Return 1
	End if
	ib_incomplete = False
	Return 0
End If
ib_incomplete = False
Return 0

end function

public subroutine wf_gettypedesc (ref datawindowchild ldw_childdw);IF ldw_childdw.RowCount()	> 0 THEN RETURN
ldw_childdw.SetTransObject(SQLCA)
ldw_childdw.Retrieve()
end subroutine

public function boolean wf_update ();Double					ld_TaskNumber
Long						ll_Row, &
							ll_RowCount,&
							ll_CurrentRow, ll_lines, &
							ll_end_row, ll_start_row			
INTEGER 					li_PageNumber, &
							li_Rtn, li_count
STRING					ls_temp, ls_mass_price_override, &
							ls_Detail, ls_hold_detail, ls_businessrule, &
							ls_oper_customer_info
CHARACTER				lc_indicator
Boolean 					lb_still_loop
u_project_functions	lu_prj_fun
DataStore				lds_temp
s_Error					ls_Error

ld_TaskNumber = 0
li_PageNumber = 0
lc_indicator = 'U'
lb_still_loop = True
IF dw_detail.AcceptText() = -1 OR dw_input.AcceptText() = -1 Then Return FALSE
If dw_detail.ModifiedCount( )  = 0 Then
	iw_frame.SetMicroHelp("No lines are modified")
	Return False
End If
If cbx_price_over.checked Then
	ls_mass_price_override = "Y"
Else
	ls_mass_price_override = "N"
End If
ll_RowCount = dw_Detail.RowCount()
ll_CurrentRow = dw_detail.GetRow() 
ls_Error.se_User_ID = Message.nf_getuserid()
This.SetRedraw(False)
dw_detail.SelectRow(0, False)
ll_row = 0
Do 
	ll_row = dw_detail.GetNextModified(ll_row, Primary!)
	If ll_row <> 0 Then dw_detail.SelectRow(ll_row, True)
Loop While ll_row > 0
lds_temp = Create DataStore
lds_temp.DataObject = 'd_sales_tsr_sku_all'
lds_temp.Object.Data = dw_detail.object.Data.Selected
ll_row = lds_temp.RowCount()
ll_end_row = 84
ll_start_row = 1
If ll_row > ll_end_row Then
	lds_temp.RowsMove(1, lds_temp.RowCount(), Primary!, lds_temp,10000, Filter!)
Else
	lb_still_loop = False
End IF
DO
	If lb_still_loop Then
		lds_temp.RowsMove(ll_start_row,ll_end_row , Filter!, lds_temp, 10000, Primary!)
		ls_Detail = lds_temp.Describe("DataWindow.Data")
		lds_temp.RowsDiscard(1, 84, Primary!)
	Else
		ls_Detail = lds_temp.Object.DataWindow.Data
	End If
	ls_hold_detail = ls_Detail
//	//call RPC\\
	li_Rtn = iu_ws_orp4.nf_orpo84fr(ls_Error, is_customer_info, ls_mass_price_override, ls_detail, lc_indicator, 	ls_oper_customer_info)
	IF li_rtn = 0 or li_rtn = +2 OR li_rtn = 5 Then
		wf_reset_order_status(ls_detail)
		lds_temp.ImportString(ls_detail)
	Else
		lds_temp.ImportString(ls_hold_detail)
	End If
	If lb_still_loop Then
		lds_temp.RowsMove(1, 84, Primary!, lds_temp, 1, Filter!)
		ll_start_row += 84
		ll_end_row += 84
		If ll_row < ll_start_row Then lb_still_loop = False
	Else
		lds_temp.RowsMove(1, ll_row, Primary!, lds_temp, 1, Filter!)
	End If
Loop While lb_still_loop

ii_rpc_retvalue = li_Rtn
if ii_rpc_retvalue = 5 then 
	ib_update_detail = TRUE
else
	ib_update_detail = FALSE
end if
lds_temp.RowsMove(1,lds_temp.FilteredCount(), Filter!, lds_temp, 10000, Primary!)
ls_temp = lds_temp.Describe("DataWindow.Data")
lu_prj_fun.nf_replace_rows(dw_detail, ls_temp)
//dw_detail.object.Data.Selected = lds_temp.object.Data
dw_detail.SelectRow(0, False)

dw_detail.ScrolltoRow(ll_CurrentRow)
//cbx_price_over.visible = true

CHOOSE CASE li_Rtn
CASE 0
	wf_set_net_shortage()
	wf_mod_sub_lines()
	This.SetRedraw(TRUE)
	dw_detail.uf_changerowstatus (0, NotModified!)
	cbx_price_over.checked = False
	ll_RowCount = dw_detail.RowCount()
	if ll_RowCount > 0 then
		for li_count = 1 to ll_RowCount
			ls_businessrule = Mid(dw_detail.GetItemString(li_count, "business_rule"),3,1)
			if ls_businessrule = 'E' Then
				cbx_price_over.visible = true
				cbx_price_over.Enabled = true
			end if
			If ls_businessrule = 'M' then
				cbx_price_over.visible = true
				cbx_price_over.Enabled = true
			end if		
		next
	end if
	RETURN TRUE
case 5
		//
	ll_RowCount = dw_detail.RowCount()
	if ll_RowCount > 0 then
		for li_count = 1 to ll_RowCount
			ls_businessrule = Mid(dw_detail.GetItemString(li_count, "business_rule"),3,1)
			if ls_businessrule = 'E' Then
				cbx_price_over.visible = true
				cbx_price_over.Enabled = true
			end if
			If ls_businessrule = 'M' then
				cbx_price_over.visible = true
				cbx_price_over.Enabled = true
			end if		
		next
	end if
	
case +2
//	wf_set_net_shortage()
//	wf_mod_sub_lines()
	This.SetRedraw(TRUE)
	cbx_price_over.checked = False
	RETURN TRUE
CASE ELSE
	This.SetRedraw(TRUE)
	RETURN FALSE
END CHOOSE
This.SetRedraw(TRUE)
RETURN FALSE
end function

public function boolean wf_retrieve ();String 	ls_temp, ls_hold_string, ls_businessrule
Boolean	lb_return
Integer	li_ret, &
			li_x, &
			li_y, li_rowcount, li_count

ib_find_incomplete = False
This.TriggerEvent('closequery')
ib_find_incomplete = True

if ii_rpc_retvalue = 0 and  ib_update_detail = FALSE then
	OpenWithParm( w_sales_order_tsr_sku_inq, This)

	IF message.StringParm = 'Abort' Then 
		Return False
	ELSE
		ls_hold_string = message.StringParm
	//
		If ib_first_time Then
			ib_first_time = False
		Else
			If is_openingstring <> ls_hold_string Then
				li_ret = This.wf_findincomplete()
				If li_ret > 0 Then
					This.TriggerEvent('ue_reinquire')
	//				check to see if incomplete
					li_ret = wf_close_query()
					If ib_incomplete Then Return False
				End If
			End If
		End If
		
		Choose Case is_type_ind
			Case 'B'
				dw_detail.DataObject = 'd_sales_tsr_sku_bol'
				this.width = 3756
			Case 'Q'
				dw_detail.DataObject = 'd_sales_tsr_sku_que'
				this.width = 4652
			Case 'A'
				dw_detail.DataObject = 'd_sales_tsr_sku_all'
				this.width = 5005
		End Choose
	
		ib_reretrieve = False
	//
		is_openingstring = ls_hold_string
		is_header_info = iw_frame.iu_string.nf_gettoken( is_openingString, '~?')
		dw_input.Reset()
		dw_input.ImportString(is_header_info)
		is_products = is_openingString
		is_openingstring = is_header_info + '~?' + is_products
	
		ls_temp = dw_input.GetItemString(1, 'date_type')
		IF ls_temp = 'D' Then
			dw_detail.Object.date_label.text = "Delivery~r~nDate"
		Else
			dw_detail.Object.date_label.text = "Ship~r~nDate"
		End If
		dw_Detail.Reset()
		lb_return = This.wf_Inquire(0, 0)
		wf_set_net_shortage()
		wf_mod_sub_lines()
		dw_detail.uf_changerowstatus ( 0, NotModified! )
		dw_detail.SetRedraw(True)
		
		cbx_price_over.visible = false
		
		Return lb_return
	END IF
end if
ib_update_detail = FALSE
ii_rpc_retvalue = 0
end function

event ue_postopen;call super::ue_postopen;STRING 	ls_JobID,&
			ls_UserID,&
			ls_salesloc

DataWindowChild	ldwc_currency

IF UPPER(LEFT(SQLCA.UserID, 4)) <> 'IBDK' THEN
	iw_Frame.im_Menu.m_Options.m_NotePad.Enabled = FALSE  
END IF
 
IF NOT ib_nv_created Then
	iu_orp002 = Create u_orp002
	iu_orp204 = Create u_orp204
	iu_ws_orp4 = Create u_ws_orp4
   ib_nv_created =  TRUE
END IF
ls_salesloc = Message.is_smanlocation
dw_detail.Reset()

is_type_ind = 'A'

IF LEFT(is_OpeningString, 4) = "MENU" Then
	is_OpeningString = ''
	message.triggerEvent("ue_getsmancode")
	is_OpeningString = message.stringparm + '~t'
	is_OpeningString += 'ALL     ~t  ~t'
	is_OpeningString += string(today())
	is_OpeningString += '~tN~tN~t'	
	is_OpeningString += string(RelativeDate(today(),30)) 
	is_OpeningString += '~tD'	
	IF NOT wf_Retrieve() Then CLOSE( This)
ELSE
	dw_input.ImportString( is_OpeningString)
	message.triggerEvent("ue_getsmancode")
	is_OpeningString = message.stringparm + '~t'
	is_OpeningString += 'ALL     ~t  ~t'
	is_OpeningString += string(today())
	is_OpeningString += '~tN~tN~t'
	is_OpeningString += string(RelativeDate(today(),30))
	is_OpeningString += '~tD'
	is_from_window = "POPUP"
	wf_Retrieve()
END IF


end event

event open;call super::open;//this.width = 2651
iw_sales_detail = Message.PowerObjectParm
is_OpeningString = Message.StringParm
ib_find_incomplete = True
ib_first_time = True
end event

event close;call super::close;If IsValid(iu_orp204) Then Destroy iu_orp204
If IsValid(iu_orp002) Then Destroy iu_orp002
end event

on w_sales_order_tsr_sku.create
int iCurrent
call super::create
this.dw_customer_data=create dw_customer_data
this.cbx_price_over=create cbx_price_over
this.dw_detail=create dw_detail
this.dw_input=create dw_input
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_customer_data
this.Control[iCurrent+2]=this.cbx_price_over
this.Control[iCurrent+3]=this.dw_detail
this.Control[iCurrent+4]=this.dw_input
end on

on w_sales_order_tsr_sku.destroy
call super::destroy
destroy(this.dw_customer_data)
destroy(this.cbx_price_over)
destroy(this.dw_detail)
destroy(this.dw_input)
end on

event ue_get_data;Choose Case as_value
	Case "Order_ID"
		Message.StringParm = is_sales_id
	Case "customer_info"
		Message.StringParm = is_customer_info
	Case "shortage_ind"
		Message.StringParm = is_shtg_qu_ind
	Case "is_openstring"
		Message.StringParm = is_openingstring
	Case "is_first_time"
		if ib_first_time then
			message.StringParm = 'true'
		else
			message.StringParm = 'false'
		end if 
	Case "is_type_ind"
		message.StringPaRM = is_type_ind
End Choose
end event

event activate;call super::activate;iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = true
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_generatesales.Enabled = false
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_orderconfirmation2.Enabled = true
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_trimloadalert.Enabled = true
//iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = True

//iw_Frame.im_Menu.m_holding5.m_reinquire.Enabled = True

//PostEvent("ue_post_active")
end event

event closequery;call super::closequery;// This would not allow the user to close the window when there are orders incomplete
Int	li_ret

If ib_find_incomplete Then
	li_ret = wf_close_query()
	Return li_ret
Else
	Return 0
End If

end event

event deactivate;call super::deactivate;iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = False
//iw_Frame.im_Menu.m_holding5.m_reinquire.Enabled = False

end event

event ue_set_data;Choose Case as_data_item
	case "is_type_ind"
		is_type_ind = as_value
end choose
end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 50, this.height - dw_detail.y - 105)

long       ll_x = 50,  ll_y = 105

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)


end event

type dw_customer_data from u_base_dw_ext within w_sales_order_tsr_sku
boolean visible = false
integer x = 18
integer y = 372
integer width = 46
integer height = 56
integer taborder = 10
string dataobject = "d_customer_data"
end type

type cbx_price_over from checkbox within w_sales_order_tsr_sku
integer x = 1787
integer y = 96
integer width = 411
integer height = 76
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Price Override"
end type

event clicked;String ls_protected
Long count

If cbx_price_over.Checked = true then
	For count = 1 to dw_detail.RowCount()
		ls_protected = dw_detail.GetItemString(count, "business_rule")
		If (Mid(ls_protected, 3,1) = 'E') then
			dw_detail.SetItem(count, 'price_override', 'Y')
		end if
		If (Mid(ls_protected, 3,1) = 'M') then
			dw_detail.SetItem(count, 'price_override', 'Y')
		end if
	next
else
	For count = 1 to dw_detail.Rowcount()
		ls_protected = dw_detail.GetItemString(count, "business_rule")
		If (Mid(ls_protected, 3, 1) = 'E') then
			dw_detail.SetItem(count, 'price_override', 'N')
		end if
		If (Mid(ls_protected, 3, 1) = 'M') then
			dw_detail.SetItem(count, 'price_override', 'N')
		end if
	next
		
end if
		

		
	
end event

type dw_detail from u_base_dw_ext within w_sales_order_tsr_sku
event ue_order_acceptance_queue ( )
integer y = 192
integer width = 4905
integer height = 1300
integer taborder = 20
string dataobject = "d_sales_tsr_sku_all"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_order_acceptance_queue();Window lw_temp

OpenSheetWithParm(lw_temp, is_Sales_ID, "w_order_acceptance_queue",iw_frame,0,iw_frame.im_menu.iao_arrangeopen) 
end event

event clicked;IF Row < 1 THEN RETURN

IF dwo.name = "order_id" THEN
	If wf_getselectedorders(This.GetItemString(row,'order_id')) Then
		iw_frame.SetMicroHelp("Order Number Already Selected")
		Return
	Else
		il_current_column ++
		IF il_current_column >= 50 THEN
			MessageBox("ERROR", "You can only select 50 Order_id")	
			THIS.SelectRow(row, FALSE)
			il_current_column --
			Return
		END IF
		is_selection = '4'
		Call Super::Clicked
		THIS.SelectRow(row, TRUE)
	End If
ELSE
	il_current_column --
  	THIS.SelectRow(row, FALSE)
END IF
end event

event constructor;call super::constructor;DataWindowChild	ldw_DDDWChild

ib_updateable = true

InsertRow(0)

This.GetChild("pricing_ind", ldw_DDDWChild)
wf_gettypedesc(ldw_DDDWChild)

end event

event doubleclicked;call super::doubleclicked;STRING	ls_SalesID, &
			ls_ColumnName

Long		ll_row

w_sales_order_detail			lw_Temp

//// This has to be clickedrow because if you are are double clicking on 
//// a protected row the row focus does change
ll_row = row
//// You must get clicked column because order ID is protected it will 
//// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name

CHOOSE CASE ls_ColumnName
CASE	"order_id"
	ib_reretrieve = True
	ls_SalesID = This.GetItemString(ll_row, "order_id")
	IF IsValid( iw_sales_detail) Then
		iw_sales_detail.dynamic wf_set_to_order_id( ls_SalesID)
		iw_sales_detail.SetFocus() 
		iw_sales_detail.PostEvent("Ue_Move_Rows")			
		iw_sales_detail = lw_temp
	ELSE
		iw_frame.iu_project_functions.nf_list_sales_orders(ls_salesid)
	END IF
END CHOOSE
end event

event itemchanged;call super::itemchanged;Int		li_column, li_ret
Long		ll_Row, &
			ll_next_row, &
			ll_RowCount
String	ls_fields,&
			ls_ColumnName, &
			ls_order, &
			ls_next_order, &
			ls_find_string, &
			ls_temp
			
DataStore	lds_oper_customers		

u_project_functions lu_project_functions

ll_row = row
ll_RowCount= This.RowCount() + 1
ls_ColumnName = dwo.Name
ls_fields = This.GetItemString( ll_row, "business_rule")
Choose Case ls_ColumnName
	Case 'ordered_units'
	 	li_column = 1
	Case 'price'
	 	li_column = 2
	Case 'price_override'
	 	li_column = 3
	Case 'oper_customer_id'
		If data > '       ' Then
			lds_oper_customers = create datastore
			lds_oper_customers.dataobject = 'd_oper_mult_customers_dddw'
			lds_oper_customers.Reset()
			lds_oper_customers.ImportString(is_oper_customer_info)

			ls_temp = lds_oper_customers.Describe("DataWindow.Data")
			ls_find_string = "customer_id = " + "'" + This.GetItemString(row, "cutomer_id") + "' AND oper_customer_id = '" + data + "'"
			li_ret =  lds_oper_customers.Find(ls_find_string, 1, lds_oper_customers.RowCount()) 
			If li_ret <= 0 Then
					MessageBox ("Operator Customer Error", "Operator Customer is not valid")
//					This.SetRow(row)
//					This.SetColumn("oper_customer_id")
//					This.SelectText(1,10)
					Return 1
			End If
		End If
		li_column = 5
END CHOOSE
If li_column > 0 Then
	ls_fields = Left( ls_fields, li_column - 1) + "M" + Mid(ls_fields, li_column + 1)
	This.SetItem( ll_row, "business_rule", ls_fields)
End if

Choose Case ls_ColumnName
	Case 'div_po'
		ll_next_row = ll_Row + 1
		if ll_next_row < ll_RowCount Then
			ls_order = Trim(dw_detail.Object.order_id[ll_row])
			ls_next_order = Trim(dw_detail.Object.order_id[ll_next_row])
				
			Do while ls_order = ls_next_order
				This.SetItem( ll_next_row, "div_po", data)	
				ll_next_row = ll_next_row + 1
				ls_next_order = Trim(dw_detail.Object.order_id[ll_next_row])
			loop
		end if	
	Case 'corporate_po'
		ll_next_row = ll_Row + 1
		if ll_next_row < ll_RowCount Then
			ls_order = Trim(dw_detail.Object.order_id[ll_row])
			ls_next_order = Trim(dw_detail.Object.order_id[ll_next_row])
				
			Do while ls_order = ls_next_order
				This.SetItem( ll_next_row, "corporate_po", data)	
				ll_next_row = ll_next_row + 1
				ls_next_order = Trim(dw_detail.Object.order_id[ll_next_row])
			loop
		end if			
END CHOOSE
end event

event itemfocuschanged;call super::itemfocuschanged;IF Row > 0 Then
	is_Sales_ID = This.GetItemString(row,"order_id")
	is_Shtg_qu_ind = This.GetItemString(row,"short_queue")
Else
	is_Sales_ID = ''
	is_Shtg_qu_ind = ''
End IF
end event

event rbuttondown;call super::rbuttondown;m_sales_orders_popup 		NewMenu

If This.RowCount() > 0 Then
	IF Row > 0 Then
		is_Sales_ID = This.GetItemString(row,"order_id")
		is_Shtg_qu_ind = This.GetItemString(row,"short_queue")
	Else
		is_Sales_ID = ''
		is_Shtg_qu_ind = ''
	End IF
	
	NewMenu = CREATE m_sales_orders_popup
	NewMenu.M_file.m_completeorder.Enabled = TRUE
	//NewMenu.M_file.m_reinquire.Enabled = False
	NewMenu.M_file.m_orderconfirmation.Enabled = TRUE
	//NewMenu.M_file.m_productshortagequeue.Enabled = TRUE
	NewMenu.M_file.m_automaticprice.Enabled = FALSE
	NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
End If
end event

event ue_dwndropdown;call super::ue_dwndropdown;String				ls_ColumnName, ls_filter_string, ls_import

Integer			li_current_row

DataWindowChild	ldw_DDDWChild, ldwc_oper_customers

ls_ColumnName	=	Upper(This.GetColumnName())

CHOOSE CASE ls_ColumnName
	Case 'PRICING_IND'
		// Populate the price ind  DDDW
		This.GetChild("pricing_ind", ldw_DDDWChild)
	CASE "OPER_CUSTOMER_ID"
		li_current_row = This.GetRow()
		This.GetChild("oper_customer_id", ldwc_oper_customers)	
		ldwc_oper_customers.Reset()
		//insert a blank row for the shipto customer of the row clicked
		ls_import = this.GetItemString(li_current_row, "cutomer_id") + "~t" + "S" + "~t" + "       " + "~r~n" + is_oper_customer_info
		ldwc_oper_customers.ImportString(ls_import)
		ls_filter_string = "customer_id = "
		// customer_id is mispelled on all 3 data windows
		ls_filter_string += "'" + this.GetItemString(li_current_row, "cutomer_id") + "'"
		ls_filter_string += " and customer_type = 'S'"
		ldwc_oper_customers.SetFilter(ls_filter_string)
		ldwc_oper_customers.Filter()
		
		RETURN
END CHOOSE
wf_gettypedesc(ldw_DDDWChild)
end event

event itemerror;call super::itemerror;Return 1
end event

type dw_input from u_base_dw_ext within w_sales_order_tsr_sku
integer y = 4
integer width = 1760
integer height = 164
integer taborder = 0
string dataobject = "d_sales_order_tsr_inq"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
ib_updateable = False
This.Modify('tsr_id.Protect = 1 tsr_id.BackGround.Color = 12632256 ' + &
				'cust_id.Protect = 1 cust_id.Background.Color = 12632256 ' + &
				'ship_date.Protect = 1 ship_date.Background.Color = 12632256 ')

this.Object.all_prod_name.Visible = True
this.Object.all_prod_ind.Visible = True
this.Object.to_text_field.Visible = True
this.Object.ship_date_to.Visible = True

this.Object.ship_date_to.BackGround.Color = 12632256
this.Object.ship_date_to.Protect = 1

this.Object.date_label.Text = 'From:'
this.Object.date_type.Visible = True
this.Object.date_type.Protect = 1

end event

