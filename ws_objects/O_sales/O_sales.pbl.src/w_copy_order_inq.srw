﻿$PBExportHeader$w_copy_order_inq.srw
forward
global type w_copy_order_inq from w_base_response_ext
end type
type st_1 from statictext within w_copy_order_inq
end type
type em_sales_id from editmask within w_copy_order_inq
end type
end forward

type wstr_instruction_type from structure
    boolean show_Instruction
    string Instruction_List
end type

global type w_copy_order_inq from w_base_response_ext
int X=55
int Y=552
int Width=1280
int Height=372
boolean TitleBar=true
string Title="Copy Order Inquire"
long BackColor=12632256
boolean ControlMenu=true
st_1 st_1
em_sales_id em_sales_id
end type
global w_copy_order_inq w_copy_order_inq

type variables
boolean	ib_ok_to_close
end variables

event open;call super::open;//Boolean lb_Incomplete_Order
//
//String	ls_boolean, &
//			ls_orderid_string
//
//u_conversion_functions	lu_conversion
//
//ls_orderid_string	=	Message.Stringparm
//ls_boolean	=	iw_frame.iu_string.nf_gettoken(ls_orderid_string, '~t')
//lb_Incomplete_Order = lu_conversion.nf_boolean( ls_boolean)
//em_sales_id.text	=	ls_orderid_string
//IF lb_Incomplete_Order Then
//	em_sales_id.Enabled = FALSE
//	em_sales_ID.backcolor = 16761024
//ELSE
//	em_sales_id.SetFocus()
//	em_sales_id.SelectText( 1, Len(em_sales_id.Text))
//END IF
end event

on w_copy_order_inq.create
int iCurrent
call super::create
this.st_1=create st_1
this.em_sales_id=create em_sales_id
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.em_sales_id
end on

on w_copy_order_inq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.em_sales_id)
end on

event ue_base_ok;call super::ue_base_ok;String ls_Return_String 
IF LEN(TRIM(em_sales_id.Text))  = 0 Then
	Messagebox("Copy Order Inquire", "Sales Order ID is required")
	em_sales_id.Setfocus()
	Return
END IF
IF LEN(TRIM(em_sales_id.Text))  = 00000 Then
	Messagebox("Copy Order Inquire", "Please enter a valid Sales Order ID")
	em_sales_id.Setfocus()
	Return
END IF

ls_Return_String = Upper(em_sales_id.Text)
ib_ok_to_close = true

CloseWithReturn(This, ls_Return_String)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,'Abort')
end event

event close;call super::close;If not ib_ok_to_close Then
	CloseWithReturn(This,'Abort')
End If



end event

type cb_base_help from w_base_response_ext`cb_base_help within w_copy_order_inq
int X=539
int Y=172
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_copy_order_inq
int X=969
int Y=148
int TabOrder=50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_copy_order_inq
int X=969
int Y=20
int TabOrder=40
end type

type cb_browse from w_base_response_ext`cb_browse within w_copy_order_inq
int TabOrder=20
end type

type st_1 from statictext within w_copy_order_inq
int X=27
int Y=100
int Width=407
int Height=72
boolean Enabled=false
string Text="Sales Order ID"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_sales_id from editmask within w_copy_order_inq
int X=434
int Y=80
int Width=494
int Height=96
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
string Mask="!####"
MaskDataType MaskDataType=StringMask!
int Accelerator=83
long TextColor=33554432
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;//IF (NOT(IsNumber(Left(ls_value,1))) OR Not(f_IsAlpha(left(ls_value,1),1,1))) AND Not(IsNumber(Mid(ls_value,2,4)))  THEN
//	MessageBox("Invalid Order Number - "+ls_value,"All order Ids must start with one letter or number and end with 4 numbers")
//	SetActionCode(1)
//END IF
//
end event

