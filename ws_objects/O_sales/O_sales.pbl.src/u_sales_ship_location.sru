﻿$PBExportHeader$u_sales_ship_location.sru
$PBExportComments$This is for a new Ship Location setup for SR3353 *(RevGLL).
forward
global type u_sales_ship_location from u_netwise_dw
end type
end forward

global type u_sales_ship_location from u_netwise_dw
integer width = 1280
integer height = 172
string dataobject = "d_sales_ship_location"
boolean border = false
boolean livescroll = true
end type
global u_sales_ship_location u_sales_ship_location

type variables
datawindowchild idddw_child

Boolean	ib_protected = FALSE
end variables

forward prototypes
public subroutine disable ()
public subroutine enable ()
public function string nf_get_plant_code ()
public function string uf_get_plant_code ()
public function string uf_get_plant_descr ()
public function integer uf_set_plant_code (string as_plant_code)
public subroutine uf_hide_ship_location ()
public subroutine uf_show_location_code ()
public subroutine uf_fill_plant_and_descr (string as_plant_code)
public subroutine uf_set_plant_profile (string as_plant_code)
end prototypes

public subroutine disable ();This.Modify("location_code.Background.Color = 12632256 location_code.Protect = 1 " + &
				"location_code.Pointer = 'Arrow!'")

end subroutine

public subroutine enable ();This.Modify("location_code.Background.Color = 16777215 location_code.Protect = 0 " + &
				"location_code.Pointer = 'Beam!'")

end subroutine

public function string nf_get_plant_code ();return This.GetItemString(1, "location_code")
end function

public function string uf_get_plant_code ();return This.GetItemString(1, "location_code")
end function

public function string uf_get_plant_descr ();return Trim(This.GetItemString(1, "location_name"))
end function

public function integer uf_set_plant_code (string as_plant_code);Long	ll_row, &
		ll_row_count

String	ls_text

This.GetChild("location_code",idddw_child)

ls_text = 'Trim(location_code) = "' + as_plant_code + '"'
ll_row_count = idddw_child.RowCount()
ll_row = idddw_child.Find(ls_text, 1, ll_row_count + 1)
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(Trim(as_plant_code) + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(as_plant_code))
	return 1
Else
	This.SetItem(1,"location_code",as_plant_code)
	This.SetItem(1,"location_name",idddw_child.GetItemString(ll_row,"location_name"))
	This.SetFocus()
	This.SelectText(1, Len(as_plant_code))
	iw_frame.SetMicroHelp("Ready")
End if

return 0
end function

public subroutine uf_hide_ship_location ();This.Object.location_code.Visible = 0
This.Object.location_name.Visible = 0

end subroutine

public subroutine uf_show_location_code ();This.Object.location_code.Visible = 1
This.Object.location_name.Visible = 1

end subroutine

public subroutine uf_fill_plant_and_descr (string as_plant_code);nvuo_pa_business_rules 	nvuo_pa
String 						ls_group_id

ls_group_id = 'GROUP' + iw_frame.iu_netwise_data.is_groupid 
nvuo_pa.uf_check_user_group(ls_group_id)

This.GetChild('location_code', idddw_child)
idddw_child.SetTransObject(SQLCA)
idddw_child.Retrieve(ls_group_id)

If as_plant_code = "" Then
	as_plant_code = ProfileString( iw_frame.is_UserINI, "Pas", "Lastplant","")
End If

This.SetItem( 1, "location_code", as_plant_code)

Long ll_row
ll_row = idddw_child.Find('location_code = "' + Trim(as_plant_code) + '"', 1, idddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(as_plant_code + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(as_plant_code))
Else
	This.SetItem(1, 'location_name', idddw_child.GetItemString(ll_row, 'location_name'))
	This.SetFocus()
	This.SelectText(1, Len(as_plant_code))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

end subroutine

public subroutine uf_set_plant_profile (string as_plant_code);SetProfileString(iw_frame.is_UserINI, "Pas", "Lastplant",Trim(as_plant_code))
end subroutine

event getfocus;call super::getfocus;If This.GetColumnName() = "location_code" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

event itemchanged;call super::itemchanged;Long	ll_row
String ls_location_name

nvuo_pa_business_rules 	nvuo_pa
String 						ls_group_id

ls_group_id = 'GROUP' + iw_frame.iu_netwise_data.is_groupid 
//Rev#01 check if there are any user groups
nvuo_pa.uf_check_user_group(ls_group_id)

This.GetChild('location_code', idddw_child)
idddw_child.SetTransObject(SQLCA)
idddw_child.Retrieve(ls_group_id)

If Len(Trim(data)) = 0 Then 
	// User entered an empty plant code -- set fields to blank
	// The last row is an empty row
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	This.ScrollToRow(This.RowCount())
	This.SetItem(1,"location_code","")
	This.SetItem(1,"location_name","")
	return 
End if

// Find the entered plant code in the table -- don't count the last row, it's blank
ll_row = idddw_child.Find('location_code = "' + Trim(data) + '"', 1, idddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(data + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
Else
	ls_location_name = idddw_child.GetItemString(ll_row, 'location_name')
	This.SetItem(1, 'location_name', idddw_child.GetItemString(ll_row, 'location_name'))
	This.SetFocus()
	This.SelectText(1, Len(data))
	SetProfileString( iw_frame.is_UserINI, "Pas", "Lastplant",Trim(data))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

return 

end event

event itemerror;call super::itemerror;Return 1
end event

event ue_postconstructor;call super::ue_postconstructor;If ib_protected Then
	This.Modify("location_code.Protect='1' location_code.Background.Color='12632256'")
End If
end event

on u_sales_ship_location.create
end on

on u_sales_ship_location.destroy
end on

event constructor;call super::constructor;//REV#01 added ls_group_id to pass to the retrieve
nvuo_pa_business_rules 	nvuo_pa
Long							ll_row
String 						ls_group_id, ls_text

ls_group_id = 'GROUP' + iw_frame.iu_netwise_data.is_groupid 
//Rev#01 check if there are any user groups
nvuo_pa.uf_check_user_group(ls_group_id)

This.GetChild('location_code', idddw_child)
idddw_child.SetTransObject(SQLCA)
idddw_child.Retrieve(ls_group_id)
// Add a blank row at the end of the table
idddw_child.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastplant","")
This.SetItem( 1, "location_code", ls_text)

ll_row = idddw_child.Find('location_code = "' + Trim(ls_text) + '"', 1, idddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(ls_text + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
Else
	This.SetItem(1, 'location_name', idddw_child.GetItemString(ll_row, 'location_name'))
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

end event

