﻿$PBExportHeader$w_tla_inquiry.srw
forward
global type w_tla_inquiry from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_tla_inquiry
end type
type dw_header from u_base_dw_ext within w_tla_inquiry
end type
end forward

global type w_tla_inquiry from w_base_sheet_ext
integer width = 3291
integer height = 1512
string title = "Trim Load Alert Inquiry"
event ue_tla ( )
event ue_print_fax ( )
event ue_tla_email ( )
event ue_tla_local_print ( )
event ue_tla_view ( )
dw_detail dw_detail
dw_header dw_header
end type
global w_tla_inquiry w_tla_inquiry

type variables
s_error		istr_error_info

u_orp204	  	iu_orp204
u_orp002		iu_orp002
u_ws_orp4		iu_ws_orp4

long		il_selected_rows

Char		 ic_process_option_in, ic_process_tla_option_in

Window		iw_sales_detail
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_rightclick ()
end prototypes

event ue_tla();ic_process_option_in = 'A'
ic_process_tla_option_in = 'F'

This.triggerevent('ue_print_fax')
end event

event ue_print_fax();INTEGER	li_rtn
String	ls_order_id, ls_order_out, ls_temp, ls_customer_email
Long 	ll_row

ll_row = dw_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_order_id += dw_detail.Getitemstring(ll_row, 'order_number') + '~t'
	ll_row = dw_detail.Getselectedrow(ll_row)
LOOP

istr_error_info.se_event_name = 'ue_print_fax'

IF NOT isValid(iu_orp002) THEN
	iu_orp002 = Create u_orp002
End IF

//li_rtn = iu_orp002.nf_orpo80br_print_fax(istr_error_info, &
//										ic_process_option_in, &
//										ls_order_id, &
//										ls_order_out, &
//										ls_customer_email, &
//										ic_process_tla_option_in)

li_rtn = iu_ws_orp4.nf_orpo80fr(istr_error_info, &
										ic_process_option_in, &
										ls_order_id, &
										ls_order_out, &
										ls_customer_email, &
										ic_process_tla_option_in)



dw_detail.SelectRow(0, False)
il_selected_rows = 0

IF li_rtn >= 0 THEN
	This.SetRedraw(TRUE)
	Return
END IF
end event

event ue_tla_email();ic_process_option_in = 'A'
ic_process_tla_option_in = 'E'

This.triggerevent('ue_print_fax')
end event

event ue_tla_local_print();ic_process_option_in = 'A'
ic_process_tla_option_in = 'P'

This.triggerevent('ue_print_fax')
end event

event ue_tla_view();ic_process_option_in = 'A'
ic_process_tla_option_in = 'V'

This.triggerevent('ue_print_fax')
end event

public function boolean wf_retrieve ();String		ls_input, &
				ls_output_string
				
Integer		li_rtn

OpenWithParm(w_tla_inquiry_inq, This)
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_input = dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
			  String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~r~n'
			  
istr_error_info.se_event_name = "wf_retrieve"

dw_header.SetRedraw(True)
//
//li_rtn = iu_orp204.nf_orpo89br_tla_inquiry(istr_error_info, &
//												ls_input, &
//												ls_output_string)

li_rtn = iu_ws_orp4.nf_orpo89fr(istr_error_info, ls_input,ls_output_string)
												
dw_detail.Reset()
dw_detail.ImportString(ls_output_string)
dw_detail.SetRedraw(True)

il_selected_rows = 0

This.SetRedraw(True)
				
Return TRUE
end function

public subroutine wf_rightclick ();m_tla_popup NewMenu

NewMenu = CREATE m_tla_popup
IF il_selected_rows > 0 THEN 
	NewMenu.m_file.m_trimloadalert.Enabled = TRUE 
Else
	NewMenu.m_file.m_trimloadalert.Enabled = False
End If

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu
end subroutine

on w_tla_inquiry.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_tla_inquiry.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event ue_set_data;Choose Case as_data_item
	Case 'location_code'
		dw_header.SetItem( 1, "location_code", as_value)
	Case 'location_text'
		dw_header.SetItem( 1, "location_text", as_value)
	Case 'division_code'
		dw_header.SetItem( 1, "division_code", as_value)
	Case 'division_text'
		dw_header.SetItem( 1, "division_text", as_value)
	Case 'ship_date'
	 	dw_header.SetItem( 1, "ship_date", Date(as_value))
		
end Choose
end event

event open;call super::open;iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4
end event

event close;call super::close;Destroy( iu_orp204)
If IsValid(iu_orp002) Then Destroy iu_orp002
end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 70, this.height - dw_detail.y - 150)

long       ll_x = 70,  ll_y = 150

if il_BorderPaddingWidth > ll_x  Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)


end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")

end event

event deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")

end event

type dw_detail from u_base_dw_ext within w_tla_inquiry
integer x = 9
integer y = 272
integer width = 3237
integer height = 1124
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_tla_inquiry_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;is_selection = '2'
end event

event clicked;call super::clicked;IF Row < 1 THEN RETURN

if This.IsSelected(row) then
	il_selected_rows ++
else
	il_selected_rows --
end if

IF il_selected_rows > 50 THEN
	MessageBox("ERROR", "You can only select 50 Orders")	
	THIS.SelectRow(row, FALSE)
	il_selected_rows --
	Return
END IF


end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event doubleclicked;call super::doubleclicked;STRING							ls_SalesID, ls_ColumnName

Long								ll_row

w_sales_order_detail			lw_Temp

// This has to be clickedrow because if you are are double clicking on 
// a protected row the row focus does change
ll_row = row

// You must get clicked column because order ID is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name

IF ls_ColumnName = "order_number" then
	ls_SalesID = This.GetItemString(ll_row, "order_number")
	IF IsValid( iw_sales_detail) Then
		iw_sales_detail.dynamic wf_set_to_order_id( ls_SalesID)
		opensheet(lw_Temp,"w_sales_order_detail", &
	  					iw_frame,0,iw_frame.im_menu.iao_arrangeOpen)
//		iw_sales_detail.SetFocus() 
//		iw_sales_detail.PostEvent("Ue_Move_Rows")			
//		iw_sales_detail = lw_temp
	ELSE
		iw_frame.iu_project_functions.nf_list_sales_orders(ls_salesid)
	END IF
END IF
end event

type dw_header from u_base_dw_ext within w_tla_inquiry
integer x = 101
integer y = 24
integer width = 2354
integer height = 232
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_tla_inquiry_header"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

