﻿$PBExportHeader$w_edi_history_display.srw
forward
global type w_edi_history_display from w_base_sheet_ext
end type
type dw_instructions from u_base_dw_ext within w_edi_history_display
end type
type dw_parameters from u_base_dw_ext within w_edi_history_display
end type
type dw_detail from u_base_dw_ext within w_edi_history_display
end type
type dw_header from u_base_dw_ext within w_edi_history_display
end type
end forward

global type w_edi_history_display from w_base_sheet_ext
integer width = 3808
integer height = 1584
string title = "Sales EDI Change PO History Display"
dw_instructions dw_instructions
dw_parameters dw_parameters
dw_detail dw_detail
dw_header dw_header
end type
global w_edi_history_display w_edi_history_display

type variables
String	is_openingstring

u_orp002		iu_orp002
u_ws_orp1		iu_ws_orp1

s_error	istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_set_filter (long al_selected_row)
end prototypes

public function boolean wf_retrieve ();String	ls_header_string, &
			ls_detail_string
		
Integer	li_rtn		
			
This.TriggerEvent('closequery')

OpenWithParm( w_edi_history_display_inq, is_openingstring)

IF message.StringParm = 'Cancel' Then 
	Return False
ELSE
	is_openingstring = message.StringParm
	dw_parameters.Reset()
	dw_parameters.ImportString( is_openingstring)
END IF

istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_orp002.nf_orpo17br_edi_history_browse(istr_error_info, &
	//																	is_openingstring, &
		//																ls_header_string, &
			//															ls_detail_string)		
									
li_rtn = iu_ws_orp1.nf_orpo17fr(istr_error_info, &
											is_openingstring, &
											ls_header_string, &
											ls_detail_string)										
																		
																		
																		

dw_detail.Reset()
dw_detail.Importstring(ls_detail_string)

dw_header.Reset()
If dw_header.Importstring(ls_header_string) > 0 Then
	dw_header.SelectRow(1, TRUE) 
	wf_set_filter(1)
End If


Return True
end function

public subroutine wf_set_filter (long al_selected_row);String	ls_instructions, &
			ls_filter, &
			ls_selected_customer, &
			ls_selected_div_po, &
			ls_selected_ts
			
Date		ldt_selected_po_date			

ls_selected_customer = dw_parameters.GetItemString(1,"customer")
ls_selected_div_po = dw_header.GetItemString(al_selected_row, "divisional_po")
ldt_selected_po_date = dw_header.GetItemDate(al_selected_row, "po_date")
ls_selected_ts = dw_header.GetItemString(al_selected_row, "transaction_ts")
			
ls_filter = "customer = '" + ls_selected_customer + "' and " 
ls_filter += "divisional_po = '" + ls_selected_div_po + "' and " 
ls_filter += "po_date = Date('" + String(ldt_selected_po_date, "yyyy-mm-dd") + "') and "
ls_filter += "transaction_ts = '" + ls_selected_ts + "'" 

//MessageBox('Filter is', ls_filter)

dw_detail.SetRedraw(false)

dw_detail.SetFilter(ls_filter)
dw_detail.Filter()
dw_detail.SetSort("#1 A, #2 A, #3 A, #4 A, #6 A")
dw_detail.Sort()
dw_detail.SetRedraw(true)

ls_instructions = dw_header.GetItemString(al_selected_row, "instructions")

dw_instructions.SetItem(1,"instructions1",mid(ls_instructions,1,100))
dw_instructions.SetItem(1,"instructions2",mid(ls_instructions,101,100))
end subroutine

on w_edi_history_display.create
int iCurrent
call super::create
this.dw_instructions=create dw_instructions
this.dw_parameters=create dw_parameters
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_instructions
this.Control[iCurrent+2]=this.dw_parameters
this.Control[iCurrent+3]=this.dw_detail
this.Control[iCurrent+4]=this.dw_header
end on

on w_edi_history_display.destroy
call super::destroy
destroy(this.dw_instructions)
destroy(this.dw_parameters)
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;iu_orp002 = CREATE u_orp002
iu_ws_orp1 = Create u_ws_orp1
wf_retrieve()
end event

event close;call super::close;If IsValid( iu_Orp002) Then Destroy( iu_orp002)
Destroy iu_ws_orp1
end event

type dw_instructions from u_base_dw_ext within w_edi_history_display
integer x = 14
integer y = 1220
integer width = 3328
integer height = 212
integer taborder = 30
string dataobject = "d_edi_history_instructions"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_parameters from u_base_dw_ext within w_edi_history_display
integer x = 27
integer y = 16
integer width = 2560
integer height = 120
integer taborder = 10
string dataobject = "d_edi_parameters"
boolean border = false
end type

type dw_detail from u_base_dw_ext within w_edi_history_display
integer x = 9
integer y = 704
integer width = 2194
integer height = 500
integer taborder = 20
string dataobject = "d_edi_history_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_header from u_base_dw_ext within w_edi_history_display
integer x = 9
integer y = 160
integer width = 3726
integer height = 520
integer taborder = 10
string dataobject = "d_edi_history_header"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
is_selection= "1"
end event

event clicked;call super::clicked;If Row < 1 Then Return

wf_set_filter(row)
end event

event doubleclicked;call super::doubleclicked;//String ls_open_string
//
//String	ls_selected_customer, &
//			ls_selected_customer_name, &
//			ls_selected_div_po, &
//			ls_selected_ts, &
//			ls_selected_salesperson, &
//			ls_selected_salesperson_name
//			
//Date		ldt_selected_po_date	
//
//If row > 1 Then
//	ls_selected_customer = dw_parameters.GetItemString(1,"customer")
//	ls_selected_customer_name = dw_parameters.GetItemString(1,"customer_name")
//	ls_selected_salesperson = dw_parameters.GetItemString(1,"salesperson")
//	ls_selected_salesperson_name = dw_parameters.GetItemString(1,"salesperson_name")
//
//	ls_selected_div_po = dw_header.GetItemString(row, "divisional_po")
//	ldt_selected_po_date = dw_header.GetItemDate(row, "po_date")
//	ls_selected_ts = dw_header.GetItemString(row, "transaction_ts")
//End If
//
//ls_open_string = 	ls_selected_customer + '~t' + &
//						ls_selected_customer_name + '~t' + &
//						ls_selected_salesperson + '~t' + &
//						ls_selected_salesperson_name + '~t' + &
//						ls_selected_div_po + '~t' 
//
//iw_Frame.wf_OpenSheet( "w_edi_selection", ls_open_string ) 


end event

