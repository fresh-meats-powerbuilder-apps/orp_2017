﻿$PBExportHeader$u_base_transaction_ext.sru
$PBExportComments$Extension layer Transaction Object - inherited from u_netwise_transaction.
forward
global type u_base_transaction_ext from u_netwise_transaction
end type
end forward

global type u_base_transaction_ext from u_netwise_transaction
end type
global u_base_transaction_ext u_base_transaction_ext

forward prototypes
public function boolean nf_display_message (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle)
end prototypes

public function boolean nf_display_message (integer ai_rtn, s_error astr_errorinfo, integer ai_commhandle);Boolean lb_retval

w_base_sheet_ext	lw_sheet

lb_retval  = Super::nf_Display_Message( ai_rtn, astr_errorinfo, ai_commhandle)
lw_sheet = iw_Frame.iw_Active_Sheet

IF IsValid(lw_sheet) Then 
	lw_sheet.is_MicroHelp = astr_errorinfo.se_Message
END IF

Return lb_retval
end function

on u_base_transaction_ext.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_base_transaction_ext.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

