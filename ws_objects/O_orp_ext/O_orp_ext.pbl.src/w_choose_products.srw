﻿$PBExportHeader$w_choose_products.srw
forward
global type w_choose_products from w_base_response_ext
end type
type dw_products from u_base_dw_ext within w_choose_products
end type
type dw_search from datawindow within w_choose_products
end type
end forward

global type w_choose_products from w_base_response_ext
int Width=2597
int Height=1508
boolean TitleBar=true
string Title="Product Codes"
long BackColor=12632256
event ue_check_product ( )
dw_products dw_products
dw_search dw_search
end type
global w_choose_products w_choose_products

type variables
Boolean	save , &
	ib_1st_time

Long	il_CurrentRowSelected

Integer	ii_keysTyped = 2

String	is_WhatWasTyped,&
	is_HowToSort, &	
	is_passed_product


end variables

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This,"CANCEL")
end event

event ue_base_ok;IF il_CurrentRowSelected > 0 Then
	CloseWithReturn (This,dw_products.GetItemString( il_currentrowselected, "product_code"))
END IF
end event

on w_choose_products.create
int iCurrent
call super::create
this.dw_products=create dw_products
this.dw_search=create dw_search
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_products
this.Control[iCurrent+2]=this.dw_search
end on

on w_choose_products.destroy
call super::destroy
destroy(this.dw_products)
destroy(this.dw_search)
end on

event ue_postopen;
iw_frame.iu_project_functions.ids_Product_pallet_info.ShareData(dw_products)

Choose Case is_howtosort
	Case 'product_code_text'
		dw_products.Object.product_code_text.border =2
		dw_products.Object.product_code_text.height = 104
		dw_products.Object.description_text.border = 6
		dw_products.Object.description_text.Height = 100
		dw_products.SetSort("#1 A")
		dw_products.Sort()
		dw_search.Object.product_decription.Protect = 1
		dw_search.Object.product_decription.BackGround.Color = 12632256
		dw_search.Object.product_code.Protect = 0
		dw_search.Object.product_code.BackGround.Color = 16777215
		dw_search.SetColumn("product_code")
	Case 'description_text'
		dw_products.Object.product_code_text.border = 6
		dw_products.Object.product_code_text.height = 100
		dw_products.Object.description_text.border = 2
		dw_products.Object.description_text.Height = 104
		dw_products.SetSort("#2 A")
		dw_products.Sort()
		dw_search.Object.product_decription.protect = 0
		dw_search.Object.product_decription.BackGround.Color = 16777215
		dw_search.Object.product_code.protect = 1
		dw_search.Object.product_code.BackGround.Color = 12632256
		dw_search.SetColumn("sku_long_description")
	Case ELSE
		MessageBox("No sort specified", "Please click on a column Heading before beggining")
End Choose

ii_keystyped = len(trim(is_passed_product)) 
is_whatwastyped = is_passed_product

if len(trim(is_passed_product)) > 0 then
	ii_keystyped = len(trim(is_passed_product)) + 1
	is_whatwastyped = is_passed_product
	dw_search.TriggerEvent("editchanged")
else
	ii_keystyped = 1
	dw_search.SetFocus()
	dw_search.SetRow(1)
	dw_search.SetColumn(1)
	ii_keystyped = 2
	save = false
end if



end event

event open;call super::open;u_string_functions	lu_string_functions

lu_string_functions.nf_parseleftright(Message.StringParm,"~t", is_howtosort, is_passed_product)





end event

type cb_base_help from w_base_response_ext`cb_base_help within w_choose_products
int X=2263
int Y=380
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_choose_products
int X=2263
int Y=256
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_choose_products
int X=2263
int Y=136
end type

type dw_products from u_base_dw_ext within w_choose_products
int X=5
int Y=112
int Width=2217
int Height=1292
int TabOrder=0
string DataObject="d_sku_prod_pallet_info"
boolean VScrollBar=true
end type

event clicked;call super::clicked;Choose Case String(dwo.name)
	Case 'product_code_text'
		This.Object.product_code_text.border =2
		This.Object.product_code_text.height = 104
		This.Object.description_text.border = 6
		This.Object.description_text.Height = 100
		This.SetSort("#1 A")
		This.Sort()
		dw_search.Object.product_decription.Protect = 1
		dw_search.Object.product_decription.BackGround.Color = 12632256
		dw_search.Object.product_code.Protect = 0
		dw_search.Object.product_code.BackGround.Color = 16777215
		dw_search.SetColumn("product_code")
		This.ScrollToRow(This.Find("IsSelected()",1,this.RowCOunt()))
	Case 'description_text'
		This.Object.product_code_text.border = 6
		This.Object.product_code_text.height = 100
		This.Object.description_text.border = 2
		This.Object.description_text.Height = 104
		This.SetSort("#2 A")
		This.Sort()
		dw_search.Object.product_decription.protect = 0
		dw_search.Object.product_decription.BackGround.Color = 16777215
		dw_search.Object.product_code.protect = 1
		dw_search.Object.product_code.BackGround.Color = 12632256
		dw_search.SetColumn("product_decription")
		This.ScrollToRow(This.Find("IsSelected()",1,this.RowCOunt()))
		ii_keystyped = 2
	Case 'division_text'
	Case ELSE
		IF Row > 0 Then
			This.SelectRow( 0, False)
			This.SelectRow( row, True)
			il_CurrentRowSelected = Row
			dw_search.SetItem( 1, "product_code", This.GetItemString( row,"product_code"))
			dw_search.SetItem( 1, "product_decription", This.GetItemString(row, "sku_long_description"))
		END IF
END CHOOSE

end event

event doubleclicked;This.SelectRow( 0, False)
This.SelectRow( row, True)
il_CurrentRowSelected = Row

Parent.TriggerEvent("ue_base_ok")
end event

event ue_postconstructor;call super::ue_postconstructor;IF iw_frame.iu_project_functions.ids_products.RowCount() < 1 Then
	SetMicroHelp("Loading product codes please wait")
	IF Not IsValid(iw_frame.iu_project_functions.ids_products) Then
		Destroy (iw_frame.iu_project_functions.ids_products)
		iw_frame.iu_project_functions.ids_products = Create DataStore
		iw_frame.iu_project_functions.ids_products.DataObject = This.DataObject
	END IF
	iw_frame.iu_project_functions.ids_products.SetTransObject(SQLCA)
	iw_frame.iu_project_functions.ids_products.Retrieve()
END IF
iw_frame.iu_project_functions.ids_products.ShareData( This)
end event

type dw_search from datawindow within w_choose_products
event ue_anotherkeydown pbm_dwnkey
event ue_show_product ( )
int X=5
int Y=24
int Width=2126
int Height=88
int TabOrder=10
boolean BringToTop=true
string DataObject="d_product_code_search"
boolean LiveScroll=true
end type

event ue_anotherkeydown;Choose Case key
	Case	KeyBack!
		save = False
		ii_keystyped = Len(This.GetText())+ 1
	Case KeyLeftArrow!   
		ii_keystyped --
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		save = False		
		ii_keystyped = Len(This.GetText())
END choose

end event

event editchanged;Long		ll_row, &
			ll_first_row, &
			ll_last_row


String	ls_FindString

IF KEYDOWN(KeyBack!) OR KEYDOWN(KeyLeftArrow!) OR KEYDOWN(KeyEnd!) OR  KEYDOWN(KeyHome!) OR  KEYDOWN( KeyRightArrow!)&
							OR KEYDOWN(Keydelete!) Then RETURN
							
if isnull(dwo) then
	ls_FindString = "product_code >= '" + Trim(is_passed_product) + "'"
	This.SetFocus()
	This.SetRow(1)
	This.SetColumn(1)
	Save = True
else
	Choose Case String(dwo.Name)
		Case 'product_code'
				save = Not Save			
				IF Not save Then Return
				is_whatwastyped += Right(data,1)
				ls_FindString = "product_code >= '"+Trim(Left(data,ii_keystyped))+"'"
		Case "product_decription"
			//ls_findString = "long_description >= '"+Trim(Data)+"'"
			ls_FindString = "Pos(sku_long_description,'" + Data +"') > 0"
		Case Else
			Return
	End Choose
end if

ll_Row = dw_products.Find( ls_FindString, 1, dw_products.RowCount()+1)

If ll_row > 0 Then 
	if isnull(dwo) then
		This.SetItem(1,"product_code", is_passed_product)
		This.SetText(trim(dw_products.GetItemString(ll_row,"product_code")))
		This.SelectText(ii_keystyped ,10)
		IF ll_row + 1 <= dw_products.RowCount() Then
			if pos(dw_products.GetItemString( ll_row +1,"product_code"),&
				Trim(Left(Data,ii_keystyped))) = 0 Then 
				Beep(1)
				dw_products.SelectRow(0,FALSE)
				dw_products.SelectRow( ll_row, TRUE)
				il_CurrentRowSelected = ll_row
				This.SetItem( 1, "product_decription", dw_products.GetItemString( ll_row, "sku_long_description"))
			ELSE
				dw_products.SelectRow(0,FALSE)
				il_CurrentRowSelected = 0
			END IF
		END IF
	ELSE
		if String(dwo.Name) =  'product_code' Then
			This.SetText(dw_products.GetItemString(ll_row,"product_code"))
			This.SelectText(ii_keystyped,10)
			IF ll_row + 1 <= dw_products.RowCount() Then
				if pos(dw_products.GetItemString( ll_row +1,"product_code"),&
						Trim(Left(Data,ii_keystyped))) = 0 Then 
					Beep(1)
					dw_products.SelectRow(0,FALSE)
					dw_products.SelectRow( ll_row, TRUE)
					il_CurrentRowSelected = ll_row
					This.SetItem( 1, "product_decription", dw_products.GetItemString( ll_row, "sku_long_description"))
				ELSE
					dw_products.SelectRow(0,FALSE)
					il_CurrentRowSelected = 0
				END IF
			END IF
		END IF
	end if	
	
	dw_products.ScrollToRow(ll_row)
	
	dw_products.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_products.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_products.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_products.SetRedraw(False)
	dw_products.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_products.ScrollToRow(ll_row)
	dw_products.SetRow(ll_row + 1)
	dw_products.SetRedraw(True)
End If
ii_keystyped++
end event

event constructor;This.InsertRow(0)
end event

