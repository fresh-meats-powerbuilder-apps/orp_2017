﻿$PBExportHeader$w_message_icons_ext.srw
$PBExportComments$Window That displays the diffrent type of messages in icon format
forward
global type w_message_icons_ext from w_message_icons
end type
type uo_edi from u_icons within w_message_icons_ext
end type
type uo_prc from u_icons within w_message_icons_ext
end type
type uo_lat from u_icons within w_message_icons_ext
end type
type dw_sales_people from u_base_dw_ext within w_message_icons_ext
end type
end forward

global type w_message_icons_ext from w_message_icons
int Width=2725
int Height=848
uo_edi uo_edi
uo_prc uo_prc
uo_lat uo_lat
dw_sales_people dw_sales_people
end type
global w_message_icons_ext w_message_icons_ext

on w_message_icons_ext.create
int iCurrent
call super::create
this.uo_edi=create uo_edi
this.uo_prc=create uo_prc
this.uo_lat=create uo_lat
this.dw_sales_people=create dw_sales_people
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_edi
this.Control[iCurrent+2]=this.uo_prc
this.Control[iCurrent+3]=this.uo_lat
this.Control[iCurrent+4]=this.dw_sales_people
end on

on w_message_icons_ext.destroy
call super::destroy
destroy(this.uo_edi)
destroy(this.uo_prc)
destroy(this.uo_lat)
destroy(this.dw_sales_people)
end on

type uo_other from w_message_icons`uo_other within w_message_icons_ext
int X=2126
int Y=116
int TabOrder=70
end type

type uo_all from w_message_icons`uo_all within w_message_icons_ext
int X=1577
int Y=116
int TabOrder=40
end type

type dw_classes from w_message_icons`dw_classes within w_message_icons_ext
int TabOrder=30
end type

type uo_edi from u_icons within w_message_icons_ext
int X=576
int Y=116
int TabOrder=60
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event ue_clicked;call super::ue_clicked;iw_Frame.wf_OpenSheet( "w_co_so", Message.is_salesperson_code+"~t"+&
							STRING(TODAY(),"MM/DD/YYYY")+"~t"+&
							String(RelativeDate(Today(),30),"mm/dd/yyyy")+"~t~t~tS~t~tE")


end event

event constructor;uf_set_icon("EDI2.bmp", "EDI")
end event

on uo_edi.destroy
call u_icons::destroy
end on

type uo_prc from u_icons within w_message_icons_ext
int X=123
int Y=116
int TabOrder=50
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on ue_clicked;call u_icons::ue_clicked;Window	lw_Window
OpenSheet( lw_Window, "w_pricing_review", iw_Frame, 0 , iw_Frame.im_Menu.iao_ArrangeOpen)
end on

on constructor;uf_set_icon("Pri.bmp", "Pricing")
end on

on uo_prc.destroy
call u_icons::destroy
end on

type uo_lat from u_icons within w_message_icons_ext
int X=1083
int Y=116
int TabOrder=20
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event ue_clicked;call super::ue_clicked;String		ls_fileName,&
				ls_OpenString

Long			ll_rowcount,&
				ll_loop_count

ls_fileName = iw_frame.is_workingDir+"select.dbf"
dw_sales_people.ImportFile( ls_fileName)
ls_OpenString = "I~t"+Message.is_smanlocation+"~t"
ll_RowCount = dw_sales_people.RowCount()
IF ll_RowCount = 0 Then
	ls_openString = ""
ELSE
	FOR ll_Loop_Count = 1 to ll_RowCount
		ls_OpenString += dw_sales_people.GetItemString( ll_Loop_Count, "smancode") +"~t"
	Next
END IF
iw_frame.wf_OpenSheet("w_late_notification", ls_OpenString)

end event

on constructor;uf_set_icon("LAT.bmp", "Late Not.")
end on

on uo_lat.destroy
call u_icons::destroy
end on

type dw_sales_people from u_base_dw_ext within w_message_icons_ext
int X=2578
int Y=632
int Width=96
int Height=96
boolean Visible=false
string DataObject="d_sales_people"
end type

