﻿$PBExportHeader$w_tooltip_expanded.srw
forward
global type w_tooltip_expanded from window
end type
type dw_description from datawindow within w_tooltip_expanded
end type
end forward

global type w_tooltip_expanded from window
integer x = 672
integer y = 268
integer width = 1170
integer height = 84
boolean border = false
windowtype windowtype = child!
long backcolor = 15793151
dw_description dw_description
end type
global w_tooltip_expanded w_tooltip_expanded

type variables
w_base_sheet_ext	iw_Parent

Window	iw_ActiveSheet
end variables

on w_tooltip_expanded.create
this.dw_description=create dw_description
this.Control[]={this.dw_description}
end on

on w_tooltip_expanded.destroy
destroy(this.dw_description)
end on

event open;Long						ll_NewX, &
							ll_NewY,&
							ll_Width,&
							ll_Height,&
							ll_Temp, &
							ll_frame, &
							ll_desc, &
							ll_desc_width, &
							ll_rowcount, &
							ll_tooltip_height, &
							ll_row, &
							ll_curr_col, &
							ll_curr_row, &
							ll_desc_rowcount, &
							ll_desc_row, &	
							ll_tooltip_desc_col, &
							ll_tooltip_desc_row, &
							ll_dw_row, &
							ll_new_row
							
String					ls_data,&
							ls_String, &
							ls_filter, &
							ls_curr_col, &
							ls_curr_row, & 
							ls_tooltip, &
							ls_new_tooltip, &
							ls_colname 


u_string_functions	lu_strings
DataStore	lds_description

iw_ActiveSheet = iw_frame.GetActiveSheet()

lds_description = Create DataStore
lds_description.DataObject = "d_tooltip_expanded"
ll_Temp = 15

iw_parent = Message.PowerObjectParm
IF IsValid(iw_parent) Then
	iw_parent.Event ue_get_data("ToolTip")
	ls_String = Message.StringParm 
	iw_parent.Event ue_get_data("CurrCol")
	ll_curr_col = long(Message.StringParm) 
	iw_parent.Event ue_get_data("CurrRow")
	ll_curr_row = long(Message.StringParm)
	iw_parent.Event ue_get_data("ColName")
	ls_colname = Message.StringParm
ELSE
	ls_String = Message.StringParm 
END IF
ls_curr_row = string(ll_curr_row)
//ll_height = dw_description.ImportString(ls_String)
ll_height = lds_description.ImportString(ls_string)
ll_row = lds_description.GetItemNumber(1, "tooltip_row")
//ll_row = dw_description.GetItemNumber(1, "tooltip_row")
if ll_row = 99 then
	ll_height = dw_description.ImportString(ls_string)
else
	ll_desc_rowcount = lds_description.RowCount()
	ll_desc_row = 1
	ll_new_row = 0
	do while ll_desc_row <= ll_desc_rowcount
		ll_tooltip_desc_row = lds_description.GetItemNumber(ll_desc_row, "tooltip_row")
		ll_tooltip_desc_col = lds_description.GetItemNumber(ll_desc_row, "tooltip_column")
		ls_tooltip = lds_description.GetItemString(ll_desc_row, "tooltip")
		
		if ll_new_row = 0 and IsValid(iw_parent) then
			Choose Case iw_parent.title
				Case "Product Availability Summary"
						ll_New_Row = dw_description.InsertRow(ll_new_Row + 1)
						dw_description.SetItem(ll_New_Row, "tooltip", "Production  Days")
						ll_New_Row = dw_description.InsertRow(ll_new_Row + 1)
						dw_description.SetItem(ll_New_Row, "tooltip", "Date        Old    Quantity")
				Case "PA Inquiry"
						If Left(ls_colname, 8) = "pa_units" Then
	 						ll_New_Row = dw_description.InsertRow(ll_new_Row + 1)
							dw_description.SetItem(ll_New_Row, "tooltip", "Production  Days")
							ll_New_Row = dw_description.InsertRow(ll_new_Row + 1)
							dw_description.SetItem(ll_New_Row, "tooltip", "Date        Old    Quantity")
						End If
			End Choose
		end if
		
		if ll_tooltip_desc_row = ll_curr_row then
			if ll_tooltip_desc_col = ll_curr_col then
				ll_New_Row = dw_description.InsertRow(ll_new_Row + 1)
				dw_description.SetItem(ll_New_Row, "tooltip", lds_description.GetItemString(ll_desc_row, "tooltip"))
				ls_new_tooltip = dw_description.GetItemString(ll_new_row, "tooltip")
			end if
		end if
		ll_desc_row = ll_desc_row + 1
	loop
end if

// PJM 08/08/2014 added error checking
If dw_description.RowCount() <= 0 Then Return

ll_NewX = iw_frame.PointerX() + 50
ll_NewY = iw_Frame.PointerY() + 50


if ll_row = 99 then
	If ll_NewX + This.Width > iw_Frame.Width Then
		ll_NewX = iw_Frame.Width - This.Width - 5
	End if
	If ll_NewY + This.Height > iw_Frame.Height Then
		ll_NewY = iw_Frame.Height - This.Height - 100
	End if
else	

	ll_frame = iw_Frame.Height
	ll_desc = Long(dw_description.Object.tooltip.Height) 
//	ll_height = dw_description.ImportString(ls_String)
    ll_height = ll_new_row
	If ll_NewY + (ll_desc * ll_height) + 1000 > iw_Frame.Height Then
		ll_NewY = ll_Frame - (ll_desc * ll_height)  - 1000
	End If
	// pjm 09/04/2014 added check for Width
	ll_width = (dw_description.GetItemNumber(1,"c_max")+1) * 35
	ll_desc_width = Long(dw_description.Object.tooltip.Width)
	If iw_parent.title = 'PA Inquiry' Then
		If ll_NewX + (ll_desc_width ) + 50 > iw_Frame.Width Then
			ll_NewX = ll_newx  - (ll_desc) - 500
		End if
	End If
end if

This.Move(ll_NewX, ll_NewY)

//ll_row = dw_description.GetItemNumber(1, "tooltip_row")
if ll_row = 99 then
	ll_height = ll_height * ll_desc + 60
//	ll_width = 300
else
	ll_height = ll_height * ll_desc + (ll_temp * ll_height)
//	ll_width = 650
end if
//	ll_height = ll_height * ll_desc + 50
//end if
If ll_NewY + This.Height > iw_Frame.Height Then
	ll_NewY = iw_Frame.Height - This.Height - 100
End if
	//ll_height = ll_height * ll_desc + ll_temp
//else
//	ll_height = 1000
//	ll_height = ll_height * ll_desc + 50
//end if
	
	
This.Move(ll_NewX, ll_NewY)//5 ) //ll_NewY)

//ll_height = dw_description.ImportString(ls_String)
//ll_height = ll_height * (Long(dw_description.Object.tooltip.Height)+ll_Temp)

//ll_width = 35
ll_width = (dw_description.GetItemNumber(1,"c_max")+1) * 35
//iw_frame.setMicroHelp(ls_String)

dw_description.Resize(ll_width,ll_height)
//Window should be a tiny bit larger then the DW

//Timer(5, This)

This.Resize(ll_width+50, ll_height)

IF IsValid(iw_parent) Then
	if iw_parent = iw_ActiveSheet Then
		iw_parent.setfocus()
	end if
end if
end event

event timer;close(this)
end event

event mousemove;
IF IsValid(iw_parent) Then
	if iw_parent = iw_ActiveSheet Then
		iw_parent.setfocus()
	end if
end if
Close(This)
end event

type dw_description from datawindow within w_tooltip_expanded
integer x = 5
integer y = 4
integer width = 1061
integer height = 52
integer taborder = 1
string dataobject = "d_tooltip_expanded"
boolean border = false
end type

event constructor;//This.insertRow(0)
end event

