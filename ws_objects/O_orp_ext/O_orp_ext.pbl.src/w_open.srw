﻿$PBExportHeader$w_open.srw
$PBExportComments$file open window -- Displays user objects
forward
global type w_open from window
end type
type cb_1 from commandbutton within w_open
end type
type cb_2 from commandbutton within w_open
end type
type tab_1 from tab within w_open
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_3 from userobject within tab_1
end type
type tabpage_3 from userobject within tab_1
end type
type tabpage_4 from userobject within tab_1
end type
type tabpage_4 from userobject within tab_1
end type
type tabpage_5 from userobject within tab_1
end type
type tabpage_5 from userobject within tab_1
end type
type tabpage_6 from userobject within tab_1
end type
type tabpage_6 from userobject within tab_1
end type
type tabpage_7 from userobject within tab_1
end type
type tabpage_7 from userobject within tab_1
end type
type tabpage_8 from userobject within tab_1
end type
type tabpage_8 from userobject within tab_1
end type
type tabpage_9 from userobject within tab_1
end type
type tabpage_9 from userobject within tab_1
end type
type tabpage_10 from userobject within tab_1
end type
type tabpage_10 from userobject within tab_1
end type
type tab_1 from tab within w_open
tabpage_2 tabpage_2
tabpage_1 tabpage_1
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
tabpage_7 tabpage_7
tabpage_8 tabpage_8
tabpage_9 tabpage_9
tabpage_10 tabpage_10
end type
type dw_main from datawindow within w_open
end type
end forward

global type w_open from window
integer x = 201
integer y = 304
integer width = 2208
integer height = 1044
boolean titlebar = true
string title = "File Open"
windowtype windowtype = response!
long backcolor = 12632256
event ue_postopen ( )
cb_1 cb_1
cb_2 cb_2
tab_1 tab_1
dw_main dw_main
end type
global w_open w_open

forward prototypes
public subroutine wf_deny_access ()
public subroutine wf_temp_tab ()
public subroutine wf_set_position ()
end prototypes

public subroutine wf_deny_access ();Boolean ib_IsAccessible

Long	ll_RowCount,&
		ll_LoopCount,&
		ll_pos

integer li_rtn

String	ls_Window_name, &
			ls_Installed, &
			ls_RegistryString

ll_RowCount  =dw_main.RowCount()
For ll_LoopCount = 1 to ll_RowCount
	ls_Window_Name = UPPER(TRIM(dw_main.GetItemString( ll_LoopCount, "window_name")))
	ll_pos = POS( ls_Window_Name, ",")
	IF ll_Pos > 0 Then ls_Window_Name = Left(ls_Window_Name, ll_Pos - 1)
	ib_IsAccessible = iw_frame.iu_netwise_data.nf_iswindowaccessable( ls_Window_Name)
	if Not ib_IsAccessible THEN
		dw_main.DeleteRow( ll_LoopCount)
		ll_LoopCount --
		ll_RowCount = dw_main.RowCount()
//**EEM**
//	else	
//		if ls_Window_Name = "W_CHANGE_ORDER_PRODUCTION_DATES" then
//			// checking to see if UpdOrderProdDates.ocx is registered 
//			ls_Installed = ""
//			ls_RegistryString = "HKEY_CLASSES_ROOT\CLSID\{30C2E3CF-FF49-11D3-9F38-400000002928}\InprocServer32"
//			li_rtn = RegistryGet(  ls_RegistryString,	"", ls_Installed)
//			IF li_rtn < 0 OR (len(ls_Installed) <= 0) Then
//				dw_main.DeleteRow( ll_LoopCount)
//				ll_LoopCount --
//				ll_RowCount = dw_main.RowCount()
//			END IF
//		end if
	end if
Next
end subroutine

public subroutine wf_temp_tab ();String ls_groups

ls_Groups = upper(ProfileString( iw_frame.is_UserINI, "Orp", "W_open.ADD New Group Tab Remove Product Tab", ""))
if ls_groups = "" Then 
	SetProfileString( iw_frame.is_UserINI, "Orp", "W_open.ADD New Group Tab Remove Product Tab", "Y")
	ls_groups = "Y"
End IF	
		
if ls_groups = "N" then
	this.tab_1.tabpage_8.visible = True
	this.tab_1.tabpage_9.visible = False
Else
	this.tab_1.tabpage_8.visible = False
	this.tab_1.tabpage_9.visible = True
End if	

end subroutine

public subroutine wf_set_position ();Long	frameX, &
		frameY, &
		responseX, &
		responseY, &
		parent_windowX, &
		parent_windowY
		
responseX = This.X
responseY = This.Y

frameX = gw_netwise_frame.X
frameY = gw_netwise_frame.Y

If responseX < frameX Then
	This.Move (frameX + 400, frameY + 600)
End If
end subroutine

on w_open.create
this.cb_1=create cb_1
this.cb_2=create cb_2
this.tab_1=create tab_1
this.dw_main=create dw_main
this.Control[]={this.cb_1,&
this.cb_2,&
this.tab_1,&
this.dw_main}
end on

on w_open.destroy
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.tab_1)
destroy(this.dw_main)
end on

event open;dw_Main.SetRedraw(False)

DataStore	lds_Temp

String	ls_DefaultButton, &
			ls_Find

Long		ll_row,&
			ll_Len

Window	lw_ActiveSheet

Long	ll_frameX, &
		ll_frameY

ll_frameX = gw_netwise_frame.X
ll_frameY = gw_netwise_frame.Y

This.Move(ll_frameX + 600, ll_frameY + 600)

iw_frame.SetMicroHelp("Wait..Checking security")
// add function for new group tab ibdkdld
wf_temp_tab()
wf_set_position()
	
Message.ib_netwise_error = FALSE
lds_Temp = Create DataStore
lds_Temp.DataObject = dw_main.DataObject
wf_deny_access()
lw_ActiveSheet = iw_frame.GetActiveSheet()
//If IsValid(lw_ActiveSheet) Then
//	/*****************************************************************
//	**
//	** If there is a current sheet, we want the open box to have the 
//	** last used button down by default.  'ls_find' is a find statement that
//	** searches lds_main to find the button that the window belongs to.  
//	** In lds_main, parameters are tacked to the window_name column, not a 
//	** separate column, so to find a match, we need to chop off parameters 
//	** if they exist.
//	**
//	******************************************************************/
//
//	//ls_find = "Trim( Lower( Left( window_name, Pos( window_name, ',') - " + &
//	//			 "If( Pos( window_name, ',') > 0, 1, 0 - Len( Trim( window_name) ) ) ) ) ) = '" +&
//	//				Lower(Trim(ClassName(lw_ActiveSheet))) + "'"
//	ll_Len = Len(ClassName(lw_ActiveSheet))
//	ls_Find = "Left(window_name,"+String(ll_Len)+") = '"+ClassName(lw_ActiveSheet)+"'"
//	ll_row = lds_Temp.Find( ls_Find, 1, lds_Temp.RowCount() )
//	If ll_row > 0 Then 
//		ls_DefaultButton = lds_Temp.GetItemString(ll_row, 'window_type')
//	Else
//		ls_DefaultButton = "sales"
//	End if
//Else
//	ls_DefaultButton = "sales"
//End if
ls_DefaultButton = "sales"
Destroy( lds_Temp)
Choose Case Lower(ls_DefaultButton)
	Case 'cust_res'
	   tab_1.SelectTab(2)
	Case 'sales'
		tab_1.SelectTab(1)
	Case 'pa'
		tab_1.SelectTab(3)
	Case 'management'
		tab_1.SelectTab(4)
	Case 'traffic'
		tab_1.SelectTab(5)
	Case 'customer'
		tab_1.SelectTab(6)
	Case 'pricing'
		tab_1.SelectTab(7)
	Case 'product'
		tab_1.SelectTab(8)
	Case 'groups'
		tab_1.SelectTab(9)
	Case 'edi'
		tab_1.SelectTab(10)
END CHOOSE
dw_Main.SetRedraw(True)
iw_Frame.SetMicroHelp("Ready")
dw_Main.SetFocus()
//cb_1.SetFocus()
end event

type cb_1 from commandbutton within w_open
integer x = 1897
integer y = 76
integer width = 247
integer height = 108
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&OK"
boolean default = true
end type

event clicked;String  ls_return_string
Long	ll_Current_Row
if dw_main.RowCount() = 0 then
	MessageBox('Invalid Option', 'No options available on this tab.')
else
	ll_current_Row = dw_main.GetRow()
	IF ll_current_Row = 0 then ll_current_Row =1
	ls_return_String  = 	Trim(dw_main.GetItemString( ll_Current_Row,  "window_name"))+", "
	CloseWithReturn(Parent,ls_Return_String)
end if
end event

type cb_2 from commandbutton within w_open
integer x = 1902
integer y = 220
integer width = 247
integer height = 108
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Cancel"
boolean cancel = true
end type

event clicked;CloseWithReturn( Parent, 'ABORT')
end event

type tab_1 from tab within w_open
event constructor pbm_constructor
event selectionchanged pbm_tcnselchanged
event ue_getselectedwindow ( )
event create ( )
event destroy ( )
integer x = 119
integer y = 744
integer width = 1376
integer height = 184
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean multiline = true
boolean powertips = true
boolean boldselectedtext = true
tabposition tabposition = tabsonbottom!
integer selectedtab = 1
tabpage_2 tabpage_2
tabpage_1 tabpage_1
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
tabpage_7 tabpage_7
tabpage_8 tabpage_8
tabpage_9 tabpage_9
tabpage_10 tabpage_10
end type

event constructor;IF iw_frame.iu_project_functions.nf_IsSalesMgr()  or iw_frame.iu_project_functions.nf_isschedulermgr() Then
	tab_1.tabpage_4.Enabled = True
END IF
end event

event selectionchanged;tab_1.Control[newindex].Triggerevent ("ue_highlight")
end event

on tab_1.create
this.tabpage_2=create tabpage_2
this.tabpage_1=create tabpage_1
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.tabpage_6=create tabpage_6
this.tabpage_7=create tabpage_7
this.tabpage_8=create tabpage_8
this.tabpage_9=create tabpage_9
this.tabpage_10=create tabpage_10
this.Control[]={this.tabpage_2,&
this.tabpage_1,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5,&
this.tabpage_6,&
this.tabpage_7,&
this.tabpage_8,&
this.tabpage_9,&
this.tabpage_10}
end on

on tab_1.destroy
destroy(this.tabpage_2)
destroy(this.tabpage_1)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
destroy(this.tabpage_6)
destroy(this.tabpage_7)
destroy(this.tabpage_8)
destroy(this.tabpage_9)
destroy(this.tabpage_10)
end on

type tabpage_2 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Order"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type='sales'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.sort()
dw_main.SetRedraw(True)
end event

type tabpage_1 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Reservation"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
string powertiptext = "Customer Orders - Reservations"
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type='cust_res'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.Sort()
dw_main.SetRedraw(True)

end event

type tabpage_3 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "PA"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'pa'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.Sort()
dw_main.SetRedraw(True)
end event

type tabpage_4 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
boolean enabled = false
long backcolor = 12632256
string text = "Manager"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'management'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw(True)
end event

type tabpage_5 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Load"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'traffic'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw( True)
end event

type tabpage_6 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Customer"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'customer'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw(TRUE)

end event

type tabpage_7 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Pricing"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'pricing'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw(True)
end event

type tabpage_8 from userobject within tab_1
event ue_highlight ( )
boolean visible = false
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Product"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
end type

event ue_highlight;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'product'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw(True)
end event

type tabpage_9 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "Groups"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

event ue_highlight();dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'groups'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw(True)
end event

type tabpage_10 from userobject within tab_1
event ue_highlight ( )
integer x = 18
integer y = 16
integer width = 1339
integer height = -16
long backcolor = 12632256
string text = "EDI"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

event ue_highlight();dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("window_type = 'edi'")
dw_main.Filter()
If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(1, True)
End If
dw_main.SetRedraw(True)
end event

type dw_main from datawindow within w_open
event ue_keydown pbm_dwnkey
integer x = 114
integer y = 44
integer width = 1376
integer height = 696
integer taborder = 20
string dataobject = "d_orp_open"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_keydown;//KeyPageUp!
//KeyPageDown!
//KeyEnd!
//KeyHome!
//KeyLeftArrow!
//KeyUpArrow!
//KeyRightArrow!
//KeyDownArrow!

Long	ll_CurrentRow,&
		ll_MaxRows
		
ll_MaxRows = This.RowCount()		
ll_CurrentRow = This.GetRow()
IF ll_CurrentRow = 0 Then 
	ll_CurrentRow = 1
	IF This.SetRow(ll_CurrentRow) < 1 Then Return
END IF

Choose Case Key
	Case KeyRightArrow!, KeyDownArrow!
		ll_CurrentRow++
	Case KeyLeftArrow!, KeyUpArrow!
		ll_CurrentRow --
	Case KeyHome!
		ll_CurrentRow = 1
	Case KeyEnd!
		ll_CurrentRow = ll_MaxRows
	Case KeyPageUp!
		ll_CurrentRow += - 10
	Case KeyPageDown!
		ll_CurrentRow += 10
End Choose	

IF ll_CurrentRow > ll_MaxRows Then ll_CurrentRow = ll_MaxRows
IF ll_CurrentRow < 1 then ll_CurrentRow = 1

This.SelectRow(0, False)
This.SelectRow( ll_CurrentRow, True)

 
end event

event clicked;This.SelectRow(0, False)
If row > 0 Then This.SelectRow(row, True)

end event

event doubleclicked;cb_1.TriggerEvent('Clicked')
end event

