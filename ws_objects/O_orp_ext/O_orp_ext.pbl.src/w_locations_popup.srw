﻿$PBExportHeader$w_locations_popup.srw
forward
global type w_locations_popup from w_netwise_popup
end type
type dw_loc from datawindow within w_locations_popup
end type
end forward

global type w_locations_popup from w_netwise_popup
integer width = 1179
integer height = 676
boolean titlebar = false
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = child!
long backcolor = 1090519039
event ue_keydown pbm_dwnkey
dw_loc dw_loc
end type
global w_locations_popup w_locations_popup

type variables
string		is_division, &
		is_selected_plant_array, &
		is_plant_table, &
		is_header, &
		is_modified
boolean		ib_header = false, &
		ib_Modified = false, &
		ib_updated, &
		ib_open = true
window		iw_parent
datastore		ids_selected_plants
long		il_selected_count, &
		il_selected_max = 10
		
 
end variables

event ue_keydown;if KeyDown (keytab!) then
	close(this)
end if 
end event

on w_locations_popup.create
int iCurrent
call super::create
this.dw_loc=create dw_loc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_loc
end on

on w_locations_popup.destroy
call super::destroy
destroy(this.dw_loc)
end on

event ue_postopen;String 					ls_Location,&
							ls_Filter_String, &
							ls_plant, &
							ls_find
u_string_functions	lu_string_function
long						ll_rowcount, ll_count, ll_row, ll_rowcnt

//
//if not (lu_string_function.nf_IsEmpty(is_plant_table)) then
//	dw_loc.reset()
//	dw_loc.importstring(is_plant_table)
//end if

// pjm 08/15/2014 added for excluded plants on customer profile
If iw_parent.title = "Order Processing" Then
	
	Choose Case is_Division
   	Case '11' 
			ls_Filter_String = "location_type = 'P' OR location_type = 'S'" 
		Case '31'
			ls_Filter_String = "location_type = 'M' OR location_type = 'N'" 
	End Choose
	ib_open = true
Else
	
	Choose Case is_Division
		Case '11'
			ls_Filter_String = "location_type = 'P' OR location_type = 'Y' OR location_type = 'X' OR location_type = 'W' or location_code = '415'"
		Case '31'
			ls_Filter_String = "location_type = 'M' OR location_type = 'Y' OR location_type = 'X' OR location_type = 'W' or location_code = '415'"
		Case Else
			ls_Filter_String = "location_type = 'P' OR location_type = 'M' OR location_type = 'Y' OR location_type = 'X' OR location_type = 'W' OR location_type = 'S' or location_code = '415'"
	END CHOOSE
End If	
	
dw_loc.SetFilter(ls_Filter_String)
dw_loc.Filter()


ids_selected_plants = Create DataStore
ids_selected_plants.DataObject = 'd_plant_code'
ll_rowcount = ids_selected_plants.importstring(is_selected_plant_array)
ll_rowcnt = dw_loc.rowcount()
il_selected_count = 0
if ll_rowcount > 0 then
	for ll_count = 1 to ll_rowcount
		ls_plant = ids_selected_plants.getitemstring(ll_count, 'plant')
		ls_find = "location_code = '" + ls_plant + "'"
		ll_row = dw_loc.find(ls_find,0,ll_rowcnt)
		if ll_row > 0 and il_selected_count < il_selected_max then 
			dw_loc.selectrow(ll_row, true)
			il_selected_count ++
		ELSE
			IF ll_row = 0 then
				ids_selected_plants.setitem(ll_count, 'plant', '   ')
				ib_modified = true
			end if
		end if
	next
end if
end event

event open;call super::open;integer 	li_xpos, li_ypos
string	ls_update
u_string_functions	lu_string_functions

iw_parent = Message.PowerObjectParm

If iw_parent.title = "Customer Profile" Then
	
	iw_parent.DYNAMIC event ue_get_data("product_division")
	is_division = Message.StringParm
	if lu_string_functions.nf_isempty(is_division) then
		iw_parent.DYNAMIC event ue_getdata("division_code")
		is_division = Message.StringParm
		if lu_string_functions.nf_isempty(is_division) then
			iw_parent.DYNAMIC event ue_getdata("division")
			is_division = Message.StringParm
		end if 
	end if
	
	iw_parent.DYNAMIC event ue_getdata("header")
	is_header = Message.StringParm

	if is_header = 'true' then 
		ib_header = true
		iw_parent.DYNAMIC event ue_getdata("updateheader")
		ls_update = Message.StringParm
	else
		iw_parent.DYNAMIC event ue_getdata("updatedetail")
		ls_update = Message.StringParm
	end if
	if ls_update = 'true' then
		ib_updated = true
	else
		ib_updated = false
	end if
	iw_parent.DYNAMIC event ue_getdata("selected")
	is_selected_plant_array = Message.StringParm
	iw_parent.DYNAMIC event ue_getdata("plant")
	is_plant_table = Message.StringParm
	iw_parent.DYNAMIC event ue_getdata("dddwxpos")
	li_xpos = integer(Message.StringParm)
	iw_parent.DYNAMIC event ue_getdata("dddwypos")
	li_ypos = integer(Message.StringParm)
Else
	
	iw_parent.DYNAMIC event ue_get_data("product_division")
	is_division = Message.StringParm
	if lu_string_functions.nf_isempty(is_division) then
		iw_parent.DYNAMIC event ue_getdata("division_code")
		is_division = Message.StringParm
		if lu_string_functions.nf_isempty(is_division) then
			iw_parent.DYNAMIC event ue_getdata("division")
			is_division = Message.StringParm
		end if 
	end if
	
	iw_parent.DYNAMIC event ue_get_data("header")
	is_header = Message.StringParm

	if is_header = 'true' then 
		ib_header = true
		iw_parent.DYNAMIC event ue_get_data("updateheader")
		ls_update = Message.StringParm
	else
		iw_parent.DYNAMIC event ue_get_data("updatedetail")
		ls_update = Message.StringParm
	end if
	if ls_update = 'true' then
		ib_updated = true
	else
		ib_updated = false
	end if
	iw_parent.DYNAMIC event ue_get_data("selected")
	is_selected_plant_array = Message.StringParm
	iw_parent.DYNAMIC event ue_get_data("plant")
	is_plant_table = Message.StringParm
	iw_parent.DYNAMIC event ue_get_data("dddwxpos")
	li_xpos = integer(Message.StringParm)
	iw_parent.DYNAMIC event ue_get_data("dddwypos")
	li_ypos = integer(Message.StringParm)
End If	
	
this.move(li_xpos, li_ypos)
dw_loc.SetTransObject(SQLCA)
dw_loc.Retrieve()

end event

event closequery;long	ll_row, ll_count1, ll_count2

if ib_modified then is_selected_plant_array = ""


// pjm 08/267/2014 changed for customer profile
string ls_update_flag
If iw_parent.title = "Customer Profile" Then
	For ll_row = 1 to dw_loc.RowCount()
		ls_update_flag = dw_loc.GetItemString(ll_row,"update_flag")
		If Len(ls_update_flag) > 0 And ls_update_flag <> ' ' Then
			is_selected_plant_array += dw_loc.getitemstring( ll_row, 'location_code') + "~t" + ls_update_flag + "~r~n"
		End If
	Next
Else
	ll_row = dw_loc.GetSelectedRow(0)
	do while ll_row > 0 and ib_modified
		is_selected_plant_array += dw_loc.getitemstring( ll_row, 'location_code') + "~r~n"
		ll_row = dw_loc.GetSelectedRow(ll_row)
	loop
End If
// pjm 08/18/2014 added for customer profile
If iw_parent.title = "Customer Profile" Then
	iw_parent.dynamic event ue_set_excl_plants("selected",is_selected_plant_array)
Else
	iw_parent.dynamic event ue_set_data("selected",is_selected_plant_array)
End If

if ib_modified then
	is_modified = "true"
else
	is_modified = "false"
end if
// pjm 08/18/2014 added for customer profile
If iw_parent.title = "Customer Profile" Then
	iw_parent.dynamic event ue_set_excl_plants("modified",is_modified)
Else
	iw_parent.dynamic event ue_set_data("modified",is_modified)
End If	

if ib_modified then iw_parent.dynamic wf_set_selected_plants()

close(this)
end event

event close;close(this)
end event

type dw_loc from datawindow within w_locations_popup
event ue_keydown pbm_dwnkey
integer width = 1170
integer height = 672
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_location_window_popup"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_keydown;if KeyDown (keytab!) then
	close(parent)
end if 
end event

event clicked;// pjm 08/26/2014
String ls_old_update_flag

IF Row > 0 and ib_updated THEN
	ls_old_update_flag = This.GetItemString(row,"update_flag")
	If IsNull(ls_old_update_flag) Then ls_old_update_flag = " "
	if this.isselected(row) then
		This.SelectRow(row, false)
		il_selected_count --
		ib_modified = true
		// pjm 08/26/2014 added for customer profile - excluded plants
		If ls_old_update_flag <> "A" Then
			This.SetItem(row,"update_flag","D")
		Else  // already selected
			This.SetItem(row,"update_flag"," ")
		End If
	else
		if il_selected_count < il_selected_max then
			This.SelectRow( row, true)
			il_selected_count ++
			ib_modified = true
			// pjm 08/26/2014 added for customer profile - excluded plants
			If ls_old_update_flag <> "D" Then
		   	This.SetItem(row,"update_flag","A")
			Else
				This.SetItem(row,"update_flag"," ")
			End If
		end if
	end if
END IF
end event

event losefocus;
close(parent)
end event

