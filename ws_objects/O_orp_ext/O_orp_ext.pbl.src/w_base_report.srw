﻿$PBExportHeader$w_base_report.srw
forward
global type w_base_report from w_base_sheet_ext
end type
type uo_report from uo_reports within w_base_report
end type
type hsb_scroll from hscrollbar within w_base_report
end type
type vsb_scroll from vscrollbar within w_base_report
end type
type dw_page from u_base_dw_ext within w_base_report
end type
end forward

global type w_base_report from w_base_sheet_ext
int Width=2391
int Height=1049
boolean TitleBar=true
string Title="Report"
long BackColor=12632256
uo_report uo_report
hsb_scroll hsb_scroll
vsb_scroll vsb_scroll
dw_page dw_page
end type
global w_base_report w_base_report

on w_base_report.create
int iCurrent
call w_base_sheet_ext::create
this.uo_report=create uo_report
this.hsb_scroll=create hsb_scroll
this.vsb_scroll=create vsb_scroll
this.dw_page=create dw_page
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=uo_report
this.Control[iCurrent+2]=hsb_scroll
this.Control[iCurrent+3]=vsb_scroll
this.Control[iCurrent+4]=dw_page
end on

on w_base_report.destroy
call w_base_sheet_ext::destroy
destroy(this.uo_report)
destroy(this.hsb_scroll)
destroy(this.vsb_scroll)
destroy(this.dw_page)
end on

type uo_report from uo_reports within w_base_report
event ue_resize pbm_custom07
int X=1
int Y=1
int Width=2373
int TabOrder=10
boolean Visible=false
boolean Border=true
BorderStyle BorderStyle=StyleBox!
end type

on ue_resize;call uo_reports::ue_resize;This.Resize(Parent.Width, This.Height)
end on

on constructor;call uo_reports::constructor;This.Resize(Parent.Width, This.Height)
end on

on uo_report.destroy
call uo_reports::destroy
end on

type hsb_scroll from hscrollbar within w_base_report
event ue_resize pbm_custom65
int X=970
int Y=865
int Width=321
int Height=53
int TabOrder=30
boolean Visible=false
boolean Enabled=false
int MinPosition=1
int MaxPosition=99
int Position=1
end type

on ue_resize;This.Move(1, Parent.WorkspaceHeight() - This.Height+5)
This.Resize(Parent.WorkspaceWidth() - vsb_scroll.Width+5, This.Height)
This.MaxPosition = INT((dw_Page.Width - (Parent.WorkSpaceWidth() - vsb_scroll.Width))/20)
IF This.MaxPosition < 2 THEN
 	This.Visible = FALSE
ELSE
	This.Visible = TRUE
END IF
end on

on moved;dw_page.Move(-(dw_page.Width - (Parent.WorkspaceWidth() - vsb_scroll.Width))/This.MaxPosition*This.Position, dw_page.y)

end on

on lineright;This.Position = This.Position +1
This.TriggerEvent(Moved!)
end on

on pageleft;CHOOSE CASE This.Position - This.MinPosition
CASE IS > 20
	This.Position = This.Position - 20
CASE IS > 0 
	This.Position = This.MinPosition
CASE	0
	This.Position = This.Position - 1
END CHOOSE

This.TriggerEvent(Moved!)
end on

on pageright;CHOOSE CASE This.MaxPosition - This.Position
CASE IS > 20
	This.Position = This.Position + 20
CASE IS > 0 
	This.Position = This.MaxPosition
CASE	0
	This.Position = This.Position + 1
END CHOOSE

This.TriggerEvent(Moved!)
end on

on lineleft;This.Position = This.Position -1
This.TriggerEvent(Moved!)
end on

type vsb_scroll from vscrollbar within w_base_report
event ue_resize pbm_custom75
int X=2213
int Y=257
int Width=60
int Height=273
boolean Visible=false
boolean Enabled=false
int MinPosition=1
int MaxPosition=99
int Position=1
end type

on ue_resize;This.Move(Parent.WorkspaceWidth() - This.Width, uo_report.Height)
This.Resize(This.Width, Parent.WorkspaceHeight() - uo_report.Height - hsb_scroll.Height+5) 
This.MaxPosition = INT((dw_Page.Height - (Parent.WorkSpaceHeight() - hsb_scroll.Height - uo_report.Height))/20)
IF This.MaxPosition < 2 THEN
 	This.Visible = FALSE
ELSE
	This.Visible = TRUE
END IF


end on

on moved;dw_page.Move(dw_page.x, uo_report.Height - (dw_page.Height - (Parent.WorkspaceHeight() - hsb_scroll.Height - uo_report.Height))/This.MaxPosition*This.Position)

end on

on pageup;CHOOSE CASE This.Position - This.MinPosition
CASE IS > 20
	This.Position = This.Position - 20
CASE IS > 0 
	This.Position = This.MinPosition
CASE	0
	This.Position = This.Position - 1
END CHOOSE

This.TriggerEvent(Moved!)
end on

on linedown;This.Position = This.Position + 1
This.TriggerEvent(Moved!)
end on

on lineup;This.Position = This.Position - 1
This.TriggerEvent(Moved!)
end on

on pagedown;CHOOSE CASE This.MaxPosition - This.Position
CASE IS > 20
	This.Position = This.Position + 20
CASE IS > 0 
	This.Position = This.MaxPosition
CASE	0
	This.Position = This.Position + 1
END CHOOSE

This.TriggerEvent(Moved!)
end on

type dw_page from u_base_dw_ext within w_base_report
event ue_resize pbm_custom30
int X=5
int Y=1
int Height=677
int TabOrder=20
end type

on constructor;call u_base_dw_ext::constructor;This.Move(1, uo_report.Height + 1)


end on

