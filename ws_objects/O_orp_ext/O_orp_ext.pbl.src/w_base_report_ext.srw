﻿$PBExportHeader$w_base_report_ext.srw
forward
global type w_base_report_ext from w_base_report
end type
end forward

global type w_base_report_ext from w_base_report
end type
global w_base_report_ext w_base_report_ext

on w_base_report_ext.create
call w_base_report::create
end on

on w_base_report_ext.destroy
call w_base_report::destroy
end on

type dw_page from w_base_report`dw_page within w_base_report_ext
int X=1
end type

