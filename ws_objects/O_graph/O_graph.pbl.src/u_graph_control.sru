﻿$PBExportHeader$u_graph_control.sru
forward
global type u_graph_control from UserObject
end type
type tab_1 from tab within u_graph_control
end type
type tabpage_type from userobject within tab_1
end type
type dw_graph_type from datawindow within tabpage_type
end type
type tabpage_category from userobject within tab_1
end type
type dw_category_axis from datawindow within tabpage_category
end type
type tabpage_values from userobject within tab_1
end type
type dw_value_axis from datawindow within tabpage_values
end type
type tabpage_series from userobject within tab_1
end type
type dw_series_axis from datawindow within tabpage_series
end type
type tabpage_data from userobject within tab_1
end type
type dw_graph_data from datawindow within tabpage_data
end type
type tabpage_title from userobject within tab_1
end type
type dw_graph_name from datawindow within tabpage_title
end type
type tabpage_1 from userobject within tab_1
end type
type dw_text from datawindow within tabpage_1
end type
type tabpage_type from userobject within tab_1
dw_graph_type dw_graph_type
end type
type tabpage_category from userobject within tab_1
dw_category_axis dw_category_axis
end type
type tabpage_values from userobject within tab_1
dw_value_axis dw_value_axis
end type
type tabpage_series from userobject within tab_1
dw_series_axis dw_series_axis
end type
type tabpage_data from userobject within tab_1
dw_graph_data dw_graph_data
end type
type tabpage_title from userobject within tab_1
dw_graph_name dw_graph_name
end type
type tabpage_1 from userobject within tab_1
dw_text dw_text
end type
type tab_1 from tab within u_graph_control
tabpage_type tabpage_type
tabpage_category tabpage_category
tabpage_values tabpage_values
tabpage_series tabpage_series
tabpage_data tabpage_data
tabpage_title tabpage_title
tabpage_1 tabpage_1
end type
end forward

global type u_graph_control from UserObject
int Width=2917
int Height=328
long BackColor=79416533
long PictureMaskColor=536870912
long TabTextColor=33554432
long TabBackColor=67108864
tab_1 tab_1
end type
global u_graph_control u_graph_control

type variables
Private:
u_base_dw		iu_Graph
String			is_GraphName, &
			is_CurrentObject, &
			is_GraphType

u_string_functions		iu_string
end variables

forward prototypes
public function boolean uf_setgraph (datawindow ad_graph, string as_name)
private subroutine uf_enable_tabpages (string as_graphtype)
private subroutine uf_initialize_axis (datawindow adw_target, string as_axis)
private subroutine uf_initialize_data (datawindow adw_data)
private subroutine uf_initialize_name (datawindow adw_data)
private subroutine uf_initialize_text (datawindow adw_text, string as_textobject)
end prototypes

public function boolean uf_setgraph (datawindow ad_graph, string as_name);is_graphname = as_Name
iu_graph = ad_graph

tab_1.tabpage_type.dw_graph_type.SetFocus()
tab_1.tabpage_type.dw_graph_type.TriggerEvent("getfocus")
Return True
end function

private subroutine uf_enable_tabpages (string as_graphtype);is_graphtype	=	as_graphtype
IF as_graphtype	=	'03' or as_graphtype	=	'08' or &
	as_graphtype	=	'15' or as_graphtype	=	'16' THEN 
	tab_1.tabpage_category.enabled	=	True
	tab_1.tabpage_values.enabled		=	True
	tab_1.tabpage_series.enabled		=	True
ELSEIF as_graphtype	=	'13' or as_graphtype	=	'17' THEN
	tab_1.tabpage_category.enabled	=	False
	tab_1.tabpage_values.enabled		=	False
	tab_1.tabpage_series.enabled		=	False
ELSE
	tab_1.tabpage_category.enabled	=	True
	tab_1.tabpage_values.enabled		=	True
	tab_1.tabpage_series.enabled		=	False
END IF
end subroutine

private subroutine uf_initialize_axis (datawindow adw_target, string as_axis);string		ls_majortic, &
				ls_minortic

adw_target.SetRedraw(False)
as_axis	=	is_graphname + '.' + as_axis
ls_majortic	=	iu_graph.Describe(as_axis + '.MajorTic')
ls_minortic	=	iu_graph.Describe(as_axis + '.MinorTic')
adw_target.object.label[1]						= iu_graph.Describe(as_axis + '.label')
adw_target.object.majordivisions[1]			= long(iu_graph.Describe(as_axis + '.MajorDivisions'))
adw_target.object.minordivisions[1]			= long(iu_graph.Describe(as_axis + '.MinorDivisions'))
adw_target.object.displayeverynlabels[1]	= long(iu_graph.Describe(as_axis + '.DisplayEveryNLabels'))
adw_target.Object.format[1]					= iu_graph.Describe(as_axis + '.Dispattr.Format')

CHOOSE CASE	ls_majortic
	CASE	'1'
		adw_target.object.cb_majortic.FileName = 'TIC_NO.BMP'
	CASE	'2'
		adw_target.object.cb_majortic.FileName = 'TIC_UP.BMP'
	CASE	'3'
		adw_target.object.cb_majortic.FileName = 'TIC_DN.BMP'
	CASE  '4'
		adw_target.object.cb_majortic.FileName = 'TIC_CNTR.BMP'
END CHOOSE

CHOOSE CASE	ls_minortic
	CASE	'1'
		adw_target.object.cb_minortic.FileName = 'TIC_NO.BMP'
	CASE	'2'
		adw_target.object.cb_minortic.FileName = 'TIC_UP.BMP'
	CASE	'3'
		adw_target.object.cb_minortic.FileName = 'TIC_DN.BMP'
	CASE  '4'
		adw_target.object.cb_minortic.FileName = 'TIC_CNTR.BMP'
END CHOOSE
adw_target.SetRedraw(true)
end subroutine

private subroutine uf_initialize_data (datawindow adw_data);int 		li_index,  &
			li_col_count
string 	ls_colnames[], &
			ls_rc, &
			ls_series, &
			ls_category, &
			ls_values, &
			ls_string, &
			ls_value

adw_data.SetRedraw(False)
f_dw_getcolnames ( iu_graph, ls_colnames[] )
li_col_count = UpperBound ( ls_colnames[] )

ls_category	=	"category.Values='" + ls_colnames[1] + "~~t" + ls_colnames[1]
ls_series	=	"Series_Value.Values='" + ls_colnames[1] + "~~t" + ls_colnames[1]
For li_index	 =	2 TO li_col_count
	ls_category	+=	"/" + ls_colnames[li_index] + "~~t" + ls_colnames[li_index] 
	ls_series	+=	"/" + ls_colnames[li_index] + "~~t" + ls_colnames[li_index] 
NEXT

ls_category	+=	"'"
ls_series	+= "'"

FOR li_index = 1 TO li_col_count
	ls_rc = iu_graph.Describe(ls_colnames[li_index]+'.Coltype')
	IF ls_rc = 	'number' or Left(ls_rc, 1)	=	'd' or ls_rc =	'real' or &
		ls_rc	=	'long' or ls_rc	=	'ulong' THEN 
		ls_string	=	'Sum(' + ls_colnames[li_index] + ' for graph)'
	ELSE
		ls_string	=	'Count(' + ls_colnames[li_index] + ' for graph)'
	END IF
	ls_value	+= ls_colnames[li_index] + "~~t" + ls_colnames[li_index] + &
					'/' + ls_string + "~~t" + ls_string	+ '/'
NEXT
ls_value	=	"Value.Values='" + Left(ls_value, Len(ls_value) - 1 ) + "'"	
adw_data.Modify(ls_category )
adw_data.Modify(ls_series)
adw_data.Modify(ls_Value)
ls_category = iu_graph.Describe(is_graphname + ".Category")
ls_category = iu_string.nf_globalreplace ( ls_category, '~~', '' )
ls_category = iu_string.nf_globalreplace ( ls_category, '""', '"' )
adw_data.SetItem(1, 'category', ls_category)
ls_values = iu_graph.Describe(is_graphname + ".Values")
ls_values = iu_string.nf_globalreplace ( ls_values, '~~', '' )
ls_values = iu_string.nf_globalreplace ( ls_values, '""', '"' )
adw_data.SetItem(1, 'Value', ls_values )
ls_series = iu_graph.Describe(is_graphname + ".Series")
IF ls_series = '?' THEN ls_series = ""
ls_series = iu_string.nf_globalreplace ( ls_series, '~~', '' )
ls_series = iu_string.nf_globalreplace ( ls_series, '""', '"' )
IF trim(ls_series) = "" THEN
	adw_data.SetItem(1, 'Series', 0)
ELSE
	adw_data.SetItem(1, 'Series', 1)
	adw_data.SetItem(1, 'Series_value', ls_series)
END IF
adw_data.SetRedraw(True)
Return	
end subroutine

private subroutine uf_initialize_name (datawindow adw_data);SetRedraw(False)
adw_data.SetItem(1, 'title', iu_graph.Describe(is_graphname + '.title'))
adw_data.SetItem(1, 'category_sort', long(iu_graph.Describe(is_graphname + '.category.sort')))
adw_data.SetItem(1, 'series_sort', Long(iu_graph.Describe(is_graphname + '.series.sort')))
adw_data.SetItem(1, 'legend_location', Long(iu_graph.Describe(is_graphname + '.legend')))
adw_data.SetItem(1, 'overlap', Long(iu_graph.Describe(is_graphname + '.overlappercent') ))
adw_data.SetItem(1, 'spacing', Long(iu_graph.Describe(is_graphname + '.spacing') ))
SetRedraw(True)
return 	
end subroutine

private subroutine uf_initialize_text (datawindow adw_text, string as_textobject);string	ls_weight, &
			ls_italic, &
			ls_style, &
			ls_height, &
			ls_face
int		li_autosize, &
			li_underline
			
SetRedraw(False)			
li_autosize	=	Integer(iu_graph.Describe(as_textobject + '.AutoSize'))
ls_height	=	iu_graph.Describe(as_textobject + '.Font.Height')
If ls_height = "!" Then	ls_height = ""
li_underline=	Integer(iu_graph.Describe(as_textobject  + '.Font.Underline'))
ls_face		=	iu_graph.Describe(as_textobject +  '.Font.Face')
If ls_face = "!" Then ls_face = ""
ls_italic	=	iu_graph.Describe(as_textobject +  '.Font.Italic')
if ls_italic = "!" Then ls_italic = ""
ls_weight	=	iu_graph.Describe(as_textobject +  '.Font.Weight')
if ls_Weight = "!" Then ls_weight = ""

IF ls_weight	=	'400' THEN
	IF ls_italic	=	'0' THEN
		ls_style	=	'Normal'
	ELSE
		ls_style	=	'Italic'
	END IF
ELSE
	IF ls_italic	=	'0' THEN
		ls_style	=	'Bold'
	ELSE
		ls_style	=	'BoldItalic'		
	END IF
END IF
adw_text.SetItem(1, 'font_style', ls_style)
adw_text.SetItem(1, 'text_object', is_currentobject)
adw_text.SetItem(1, 'font_size', ls_height)
adw_text.SetItem(1, 'autosize', li_autosize)
adw_text.SetItem(1, 'underline', li_Underline)
adw_text.SetItem(1, 'font', ls_face)
SetRedraw(True)
return 
end subroutine

on u_graph_control.create
this.tab_1=create tab_1
this.Control[]={this.tab_1}
end on

on u_graph_control.destroy
destroy(this.tab_1)
end on

type tab_1 from tab within u_graph_control
event create ( )
event destroy ( )
int X=9
int Width=2894
int Height=320
int TabOrder=10
boolean FixedWidth=true
boolean FocusOnButtonDown=true
boolean RaggedRight=true
boolean BoldSelectedText=true
Alignment Alignment=Center!
int SelectedTab=1
long BackColor=79416533
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_type tabpage_type
tabpage_category tabpage_category
tabpage_values tabpage_values
tabpage_series tabpage_series
tabpage_data tabpage_data
tabpage_title tabpage_title
tabpage_1 tabpage_1
end type

on tab_1.create
this.tabpage_type=create tabpage_type
this.tabpage_category=create tabpage_category
this.tabpage_values=create tabpage_values
this.tabpage_series=create tabpage_series
this.tabpage_data=create tabpage_data
this.tabpage_title=create tabpage_title
this.tabpage_1=create tabpage_1
this.Control[]={this.tabpage_type,&
this.tabpage_category,&
this.tabpage_values,&
this.tabpage_series,&
this.tabpage_data,&
this.tabpage_title,&
this.tabpage_1}
end on

on tab_1.destroy
destroy(this.tabpage_type)
destroy(this.tabpage_category)
destroy(this.tabpage_values)
destroy(this.tabpage_series)
destroy(this.tabpage_data)
destroy(this.tabpage_title)
destroy(this.tabpage_1)
end on

event selectionchanged;CHOOSE CASE	newindex
	CASE	1
		tabpage_type.dw_graph_type.SetFocus()
	CASE	2
		uf_initialize_axis(tabpage_category.dw_category_axis, 'category')
		tabpage_category.dw_category_axis.SetFocus()
	CASE	3
		uf_initialize_axis(tabpage_values.dw_value_axis, 'values')
		tabpage_values.dw_value_axis.SetFocus()
	CASE	4
		uf_initialize_axis(tabpage_series.dw_series_axis, 'series')
		tabpage_series.dw_series_axis.SetFocus()
	CASE	5
		uf_initialize_data(tabpage_data.dw_graph_data)
		tabpage_data.dw_graph_data.SetFocus()
	CASE	6
		uf_initialize_name(tabpage_title.dw_graph_name)
		tabpage_title.dw_graph_name.SetFocus()
	CASE	7
		uf_initialize_text(tabpage_1.dw_text, is_graphname + ".DispAttr")
		tabpage_title.dw_graph_name.SetFocus()
END CHOOSE
end event

type tabpage_type from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Type"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_graph_type dw_graph_type
end type

on tabpage_type.create
this.dw_graph_type=create dw_graph_type
this.Control[]={this.dw_graph_type}
end on

on tabpage_type.destroy
destroy(this.dw_graph_type)
end on

type dw_graph_type from datawindow within tabpage_type
int Y=20
int Width=2839
int Height=164
int TabOrder=20
string DataObject="d_graph_type"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;string	ls_graphtype


ls_graphtype	=	Trim(RIGHT(String(dwo.Name), 2))

IF ISNUMBER(ls_graphtype) THEN
	uf_enable_tabpages(ls_graphtype)
END IF
			
IF Row = 1 AND ISNUMBER(ls_graphtype) THEN
	SetItem(1, 'c_Position', INTEGER(ls_graphtype))
	iu_graph.Modify( is_graphname + '.GraphType = ' + ls_graphtype)
END IF

This.TriggerEvent("getfocus")

end event

event getfocus;string	ls_graphtype

This.SetRedraw(False)
ls_graphtype	=	string(iu_graph.Describe(is_graphname + '.GraphType'))
SetItem(1, 'c_Position', long(ls_graphtype) )
uf_enable_tabpages(ls_GraphType)
This.GroupCalc()
This.SetRedraw(True)


end event

type tabpage_category from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Category"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_category_axis dw_category_axis
end type

on tabpage_category.create
this.dw_category_axis=create dw_category_axis
this.Control[]={this.dw_category_axis}
end on

on tabpage_category.destroy
destroy(this.dw_category_axis)
end on

type dw_category_axis from datawindow within tabpage_category
int X=14
int Width=2770
int Height=184
int TabOrder=10
string DataObject="d_axis"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'CB_MAJORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				iu_graph.Object.gr_1.Category.MajorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				iu_graph.Object.gr_1.Category.MajorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				iu_graph.Object.gr_1.Category.MajorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				iu_graph.Object.gr_1.Category.MajorTic = 2
		END CHOOSE
		dwo.Border = 6
	CASE 'CB_MINORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				iu_graph.Object.gr_1.Category.MinorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				iu_graph.Object.gr_1.Category.MinorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				iu_graph.Object.gr_1.Category.MinorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				iu_graph.Object.gr_1.Category.MinorTic = 2
		END CHOOSE
		dwo.Border = 6
END CHOOSE
end event

event constructor;InsertRow(0)
end event

event getfocus;IF is_graphtype	=	'14' THEN
	This.Object.majordivisions.protect					=	0
	This.Object.minordivisions.protect					=	0
	This.Object.majordivisions.background.color		=	16777215
	This.Object.minordivisions.background.color		=	16777215
ELSE
	This.Object.majordivisions.protect					=	1
	This.Object.minordivisions.protect					=	1
	This.Object.majordivisions.background.color		=	12632256
	This.Object.minordivisions.background.color		=	12632256
END IF
end event

event itemchanged;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'LABEL'
		iu_graph.Object.gr_1.Category.Label = data
	CASE 'MAJORDIVISIONS'
		IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Major Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Category.MajorDivisions = INTEGER(data)
	CASE 'MINORDIVISIONS'
		IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Minor Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Category.MinorDivisions = INTEGER(data)
	CASE 'DISPLAYEVERYNLABELS'
		IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Display labels must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Category.DisplayEveryNLabels 	= INTEGER(data)
	CASE	'FORMAT'
		iu_graph.Object.gr_1.Category.Dispattr.Format		=	data
END CHOOSE

end event

event itemerror;RETURN 1
end event

type tabpage_values from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Values"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_value_axis dw_value_axis
end type

on tabpage_values.create
this.dw_value_axis=create dw_value_axis
this.Control[]={this.dw_value_axis}
end on

on tabpage_values.destroy
destroy(this.dw_value_axis)
end on

type dw_value_axis from datawindow within tabpage_values
int Y=4
int Width=2839
int Height=188
int TabOrder=20
string DataObject="d_axis"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'CB_MAJORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				iu_graph.Object.gr_1.Values.MajorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				iu_graph.Object.gr_1.Values.MajorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				iu_graph.Object.gr_1.Values.MajorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				iu_graph.Object.gr_1.Values.MajorTic = 2
		END CHOOSE
		dwo.Border = 6
	CASE 'CB_MINORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				iu_graph.Object.gr_1.Values.MinorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				iu_graph.Object.gr_1.Values.MinorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				iu_graph.Object.gr_1.Values.MinorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				iu_graph.Object.gr_1.Values.MinorTic = 2
		END CHOOSE
		dwo.Border = 6
END CHOOSE
end event

event constructor;InsertRow(0)
end event

event itemchanged;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'LABEL'
		iu_graph.Object.gr_1.Values.Label = GetText()
	CASE 'MAJORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Major Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Values.MajorDivisions = INTEGER(GetText())
	CASE 'MINORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Minor Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Values.MinorDivisions = INTEGER(GetText())
	CASE 'DISPLAYEVERYNLABELS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Display labels must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Values.DisplayEveryNLabels = INTEGER(GetText())
	CASE	'FORMAT'
		iu_graph.Object.gr_1.Values.Dispattr.Format		=	data
END CHOOSE
end event

event itemerror;RETURN 1
end event

type tabpage_series from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Series"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_series_axis dw_series_axis
end type

on tabpage_series.create
this.dw_series_axis=create dw_series_axis
this.Control[]={this.dw_series_axis}
end on

on tabpage_series.destroy
destroy(this.dw_series_axis)
end on

type dw_series_axis from datawindow within tabpage_series
int Width=2757
int Height=188
int TabOrder=10
string DataObject="d_axis"
boolean Border=false
boolean LiveScroll=true
end type

event clicked;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'CB_MAJORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				iu_graph.Object.gr_1.Series.MajorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				iu_graph.Object.gr_1.Series.MajorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				iu_graph.Object.gr_1.Series.MajorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				iu_graph.Object.gr_1.Series.MajorTic = 2
		END CHOOSE
		dwo.Border = 6
	CASE 'CB_MINORTIC'
		dwo.Border = 5
		CHOOSE CASE UPPER(String(dwo.FileName))
			CASE 'TIC_UP.BMP'
				dwo.FileName = 'TIC_CNTR.BMP'
				iu_graph.Object.gr_1.Series.MinorTic = 4
			CASE 'TIC_CNTR.BMP'
				dwo.FileName = 'TIC_DN.BMP'
				iu_graph.Object.gr_1.Series.MinorTic = 3
			CASE 'TIC_DN.BMP'
				dwo.FileName = 'TIC_NO.BMP'
				iu_graph.Object.gr_1.Series.MinorTic = 1
			CASE 'TIC_NO.BMP'
				dwo.FileName = 'TIC_UP.BMP'
				iu_graph.Object.gr_1.Series.MinorTic = 2
		END CHOOSE
		dwo.Border = 6
END CHOOSE
end event

event constructor;InsertRow(0)
end event

event itemchanged;CHOOSE CASE UPPER(String(dwo.Name))
	CASE 'LABEL'
		iu_graph.Object.gr_1.Series.Label = GetText()
	CASE 'MAJORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Major Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Series.MajorDivisions = INTEGER(GetText())
	CASE 'MINORDIVISIONS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Minor Divisions must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Series.MinorDivisions = INTEGER(GetText())
	CASE 'DISPLAYEVERYNLABELS'
			IF Integer(data)	< 0 THEN
			MessageBox('Invalid Data', 'Display labels must be greater than or equal to zero')
			RETURN	1
		END IF
		iu_graph.Object.gr_1.Series.DisplayEveryNLabels = INTEGER(GetText())
END CHOOSE
end event

event itemerror;RETURN 1
end event

type tabpage_data from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Data"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_graph_data dw_graph_data
end type

on tabpage_data.create
this.dw_graph_data=create dw_graph_data
this.Control[]={this.dw_graph_data}
end on

on tabpage_data.destroy
destroy(this.dw_graph_data)
end on

type dw_graph_data from datawindow within tabpage_data
int Y=4
int Width=2848
int Height=192
int TabOrder=30
string DataObject="d_graph_data"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event itemchanged;string 	ls_rc, &
			ls_mod_string, &
			ls_data

CHOOSE CASE String(dwo.Name)
	CASE	'category'
		ls_data			=	String(data)
		ls_data		 	= 	iu_string.nf_globalreplace ( ls_data, "'", "~~~'" )
		ls_mod_string 	= 	"gr_1.Category='"+ls_data+"'"
		ls_rc 			= iu_graph.Modify(ls_mod_string)
		IF ls_rc <> "" THEN 
			MessageBox( "Invalid Category", "Category entered is not a Valid Column", stopsign! )
			This.SelectText(1, Len(ls_data))
			Return 1
		END IF
	CASE	'value'
		ls_data 			=	String(data)
		ls_data 			= 	iu_string.nf_globalreplace ( ls_data, "'", "~~~'" )
		ls_mod_string 	= "gr_1.Values='"+ls_data+"'"
		ls_rc 			= iu_graph.Modify(ls_mod_string)
		IF ls_rc <> "" THEN 
			MessageBox( "Invalid Values", "Value entered is not a Valid Column", stopsign!  )
			This.SelectText(1, Len(ls_data))
			Return 1
		END IF
	CASE	'series'
		IF Integer(data)	=	0 THEN
			This.setitem(1, 'series_value', '')
			ls_data 			=	''
			ls_rc 			= iu_graph.Modify("gr_1.series = '~"noseries ~" ' ")
			This.SetRedraw(True)
		END IF
	CASE	'series_value'
		ls_data 			=	String(data)
		ls_data 			=	iu_string.nf_globalreplace ( ls_data, "'", "~~~'" )
		ls_mod_string 	= 	"gr_1.Series = '" + ls_data + " '"
		ls_rc 			= iu_graph.Modify(ls_mod_string)
		IF ls_rc <> "" THEN 
			MessageBox( "Graph Series", "The Series entered is not Valid Column or a Combination of Columns", stopsign! )
			This.SelectText(1, Len(ls_data))
			Return 1
		END IF
END CHOOSE
end event

event itemerror;RETURN 1
end event

type tabpage_title from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Title"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_graph_name dw_graph_name
end type

on tabpage_title.create
this.dw_graph_name=create dw_graph_name
this.Control[]={this.dw_graph_name}
end on

on tabpage_title.destroy
destroy(this.dw_graph_name)
end on

type dw_graph_name from datawindow within tabpage_title
int Width=2857
int Height=192
int TabOrder=10
string DataObject="d_graph_name"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event getfocus;This.SetRedraw(False)
IF is_graphtype	=	'01' or is_graphtype	=	'12' or &
	is_graphtype	=	'13' or is_graphtype	=	'14' THEN
	This.Object.overlap.protect				=	1
	This.Object.spacing.protect				=	1
	This.Object.overlap.background.color	=	12632256
	This.Object.spacing.background.color	=	12632256
ELSEIF is_graphtype	=	'02' or is_graphtype	=	'07' THEN
	This.Object.overlap.protect				=	0
	This.Object.spacing.protect				=	0
	This.Object.overlap.background.color	=	16777215
	This.Object.spacing.background.color	=	16777215
ELSE
	This.Object.overlap.protect				=	1
	This.Object.overlap.background.color	=	12632256
	This.Object.spacing.protect				=	0
	This.Object.spacing.background.color	=	16777215
END IF
This.SetRedraw(True)
end event

event itemchanged;CHOOSE CASE	String(dwo.name)
	CASE	'category_sort'
		iu_graph.Object.gr_1.category.sort 	= long(data)
	CASE	'series_sort'
		iu_graph.Object.gr_1.series.sort 	= long(data)
	CASE	'title'
		iu_graph.Object.gr_1.title 			= string(data)
	CASE	'legend_location'
		iu_graph.Object.gr_1.legend 			= long(data)
	CASE	'overlap'
		iu_graph.Object.gr_1.overlappercent = long(data)
	CASE	'spacing'
		iu_graph.Object.gr_1.spacing			= long(data)
END CHOOSE

end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
int X=18
int Y=112
int Width=2857
int Height=192
long BackColor=12632256
string Text="Text"
long TabBackColor=12632256
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_text dw_text
end type

on tabpage_1.create
this.dw_text=create dw_text
this.Control[]={this.dw_text}
end on

on tabpage_1.destroy
destroy(this.dw_text)
end on

type dw_text from datawindow within tabpage_1
int Y=8
int Width=2853
int Height=200
int TabOrder=30
boolean BringToTop=true
string DataObject="d_graph_text"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;InsertRow(0)
end event

event itemchanged;String		ls_ObjectText

This.SetRedraw(False)

ls_ObjectText = is_graphname + "." + This.GetItemString(1,"text_object") + ".Dispattr"

CHOOSE CASE UPPER(String(dwo.Name))
	CASE	'TEXT_OBJECT'
		is_currentobject	=	data
		CHOOSE CASE	data
			CASE	'CategoryAxisLabel'
				ls_objecttext	=	is_graphname + '.Category.LabelDispattr'
			CASE	'ValueAxisLabel'
				ls_objecttext	=	is_graphname + '.Values.LabelDispattr'
			CASE	'SeriesAxisLabel'
				ls_objecttext	=	is_graphname + '.Series.LabelDispattr'
			CASE ELSE
				ls_objecttext	=	is_graphname + '.' + data + '.Dispattr'
		END CHOOSE
		uf_initialize_text(dw_text, ls_objecttext)
	CASE	'FONT'
		iu_graph.Modify(ls_objecttext + '.Font.Face="' + data + '"')
	CASE	'FONT_SIZE'
		iu_graph.Modify(ls_objecttext + '.Font.Height=' + data)
	CASE	'FONT_STYLE'
		CHOOSE CASE Upper(data)
			CASE	'NORMAL'
				iu_graph.Modify(ls_objecttext + '.Font.Italic=0')
				iu_graph.Modify(ls_objecttext + '.Font.Weight=400')
			CASE	'BOLD'
				iu_graph.Modify(ls_objecttext + '.Font.Italic=0')
				iu_graph.Modify(ls_objecttext + '.Font.Weight=700')
			CASE	'ITALIC'
				iu_graph.Modify(ls_objecttext + '.Font.Weight=400')
				iu_graph.Modify(ls_objecttext + '.Font.Italic=1')
			CASE	'BOLDITALIC'
				iu_graph.Modify(ls_objecttext + '.Font.Weight=700')
				iu_graph.Modify(ls_objecttext + '.Font.Italic=1')
		END CHOOSE
	CASE	'UNDERLINE'
		iu_graph.Modify(ls_objecttext + '.Font.Underline=' + String(data))
	CASE	'AUTOSIZE'
		iu_graph.Modify(ls_objecttext + '.AutoSize=' + String(data))
END CHOOSE
This.SetRedraw(True)
end event

