﻿$PBExportHeader$utlu01sr_programinterfaceutlu01pgutl000sr_program_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterfaceUtlu01pgUtl000sr_program_container from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterfaceUtlu01pgUtl000sr_program_container from nonvisualobject
end type

type variables
    int utl000sr_rval
    string utl000sr_message
    ulong utl000sr_task_num
    ulong utl000sr_max_record_num
    ulong utl000sr_last_record_num
    uint utl000sr_version_number
end variables

on utlu01sr_ProgramInterfaceUtlu01pgUtl000sr_program_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterfaceUtlu01pgUtl000sr_program_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

