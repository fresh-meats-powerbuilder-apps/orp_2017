﻿$PBExportHeader$utlu01sr_programinterfaceutlu01ot.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterfaceUtlu01ot from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterfaceUtlu01ot from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on utlu01sr_ProgramInterfaceUtlu01ot.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterfaceUtlu01ot.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

