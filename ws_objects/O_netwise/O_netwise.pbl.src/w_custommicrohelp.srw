﻿$PBExportHeader$w_custommicrohelp.srw
forward
global type w_custommicrohelp from Window
end type
type dw_microhelp from datawindow within w_custommicrohelp
end type
end forward

global type w_custommicrohelp from Window
int X=526
int Y=1333
int Width=3242
int Height=77
boolean Enabled=false
long BackColor=12632256
boolean Border=false
WindowType WindowType=popup!
event ue_update_microhelp ( string as_type,  integer ai_value )
dw_microhelp dw_microhelp
end type
global w_custommicrohelp w_custommicrohelp

type variables
// Pointer to the parent window
window iw_parent

// Original widths of the columns
integer 	ii_CurrentDateTime, &
	ii_Connected, &
	ii_UserName, &
	ii_MicroMessage


end variables

forward prototypes
public subroutine wf_parentresized ()
public subroutine wf_setmicromessage (string as_message)
public subroutine wf_setconnectedmessage (string as_connect)
public subroutine wf_setusernamemessage (string as_username)
public function string wf_getmicromessage ()
public subroutine wf_changecolor ()
public subroutine wf_set_textcolors (string as_type, long al_value)
end prototypes

event ue_update_microhelp;CHOOSE CASE	as_type
	CASE	'userid'
		dw_microhelp.Object.username.visible			=	ai_value
	CASE	'microhelp_status'
		dw_microhelp.Object.connected.visible			=	ai_value
	CASE	'microhelp_datetime'
		dw_microhelp.Object.currentdatetime.visible	=	ai_value
END CHOOSE
gw_netwise_frame.iu_base_data.Trigger Event ue_updatemicrohelp(as_type, ai_value)
wf_parentresized()
dw_microhelp.SetRedraw(True)
end event

public subroutine wf_parentresized ();//integer	li_PreviousColX
//// To prevent the microhelp from flickering
//dw_microhelp.SetRedraw(FALSE)
//
//// Resize and reposition this window to fit inside its parent
//This.x 		= iw_parent.x + 14
//This.y 		= (iw_parent.height + iw_parent.y) - This.height - 15
//This.width 	= iw_parent.width - 35
//
//
//// Reposition the three columns to the right
//dw_microhelp.Modify("currentdatetime.x='" + &
//		String(dw_microhelp.width - (ii_CurrentDateTime - 30)) + "'")
//
//li_PreviousColX = Integer(dw_microhelp.Describe("currentdatetime.x"))
//
////If iw_frame.iu_base_data.ii_DateTime <> 0  then
////     dw_microhelp.Modify("currentdatetime.x='" + &
////		    String(dw_microhelp.width - (ii_currentdatetime - 30)) + "'")
//////     li_PreviousColX = Integer(dw_microhelp.Describe("CurrentDateTime.x"))
////End if
////   li_PreviousColX = Integer(dw_microhelp.Describe("CurrentDateTime.x"))
//
//If iw_frame.iu_base_data.ii_status <> 0  then
//     dw_microhelp.Modify("connected.x='" + &
//		    String(li_PreviousColX - (ii_Connected + 22)) + "'")
//     li_PreviousColX = Integer(dw_microhelp.Describe("connected.x"))
//End if
//
//IF iw_frame.iu_base_data.ii_userid <> 0 THEN 
//     dw_microhelp.Modify("username.x='" + &
//	   	String(li_PreviousColX - (ii_username + 22)) + "'")
//     li_PreviousColX = Integer(dw_microhelp.Describe("username.x"))
//End if
//
//// Resize the micromessage column
//dw_MicroHelp.Modify("micromessage.width='" + String(li_PreviousColX - 35) + "'")
//
//
//// To prevent the microhelp from flickering
//dw_microhelp.SetRedraw(TRUE)
//
end subroutine

public subroutine wf_setmicromessage (string as_message);dw_microhelp.SetItem(1, "micromessage", as_message)

// Force a redraw on the parent to make sure
// the message above SetItem appears right away
This.SetRedraw(TRUE)
end subroutine

public subroutine wf_setconnectedmessage (string as_connect);// Set the message in the connect column to
// let the user know if he is connected to the DB
dw_microhelp.SetItem(1, "connected", as_connect)

// Force a redraw on the parent to make sure
// the message above SetItem appears right away
This.SetRedraw(TRUE)
end subroutine

public subroutine wf_setusernamemessage (string as_username);// Set the message in the connect column to
// let the user know if he is connected to the DB
dw_microhelp.SetItem(1, "username", as_username)

// Force a redraw on the parent to make sure
// the message above SetItem appears right away
This.SetRedraw(TRUE)
end subroutine

public function string wf_getmicromessage ();Return dw_microhelp.GetItemString(1, "micromessage")
end function

public subroutine wf_changecolor ();//Long			ll_new_color
//u_sdkcalls	lu_sdkcalls
//
//lu_sdkcalls	 = Create u_sdkcalls	
//lu_sdkcalls	.nf_choose_color(ll_new_color)
//Destroy lu_sdkcalls	
//
//dw_microhelp.Modify("micromessage.Color='String(ll_new_color) '")
//
//SetProfileString(iw_frame.is_UserIni, "Settings", "NewTextColor", String(ll_new_color))
//
end subroutine

public subroutine wf_set_textcolors (string as_type, long al_value);//CHOOSE CASE	as_type
//	CASE	'm_'
//		iw_frame.iu_base_data.il_microhelp_color	=	al_value
//		dw_microhelp.Modify("micromessage.color = &
//								'" + string(al_value) + "'")
//	CASE	'u_'
//		iw_frame.iu_base_data.il_userid_color	=	al_value
//		dw_microhelp.Modify("username.color = &
//								'" + string(al_value) + "'")
//	CASE	'd_'
//		iw_frame.iu_base_data.il_datetime_color	=	al_value
//		dw_microhelp.Modify("currentdatetime.color = &
//								'" + string(al_value) + "'")
//	CASE	's_'
//		iw_frame.iu_base_data.il_status_color	=	al_value
//		dw_microhelp.Modify("connected.color = &
//								'" + string(al_value) + "'")
//	CASE	ELSE
//		dw_microhelp.Modify("micromessage.color = &
//				'" + string(iw_frame.iu_base_data.il_microhelp_color) + "'")
//		dw_microhelp.Modify("username.color = &
//				'" + string(iw_frame.iu_base_data.il_userid_color) + "'")
//		dw_microhelp.Modify("currentdatetime.color = &
//				'" + string(iw_frame.iu_base_data.il_datetime_color) + "'")
//		dw_microhelp.Modify("connected.color = &
//				'" + string(iw_frame.iu_base_data.il_status_color) + "'")
//END CHOOSE
//		
end subroutine

on resize;// Resize the datawindow control to fit in the window
dw_microhelp.width = This.WorkSpaceWidth()
end on

event open;//// Get a pointer to the parent window
//iw_parent = ParentWindow()
//
//IF iw_frame.iu_base_data.ii_datetime <> 0 THEN
//	dw_MicroHelp.Modify("currentdatetime.visible = 1")
//ELSE
//	dw_MicroHelp.Modify("currentdatetime.visible = 0")
//END IF
//
//IF iw_frame.iu_base_data.ii_userid <> 0 THEN 
//	dw_MicroHelp.Modify("username.visible = 1")
//ELSE
//	dw_MicroHelp.Modify("username.visible = 0")
//END IF
//
//// Get the original widths of the columns
//ii_MicroMessage = &
//	Integer(dw_MicroHelp.Describe("micromessage.width"))
//
//ii_UserName = &
//	Integer(dw_MicroHelp.Describe("username.width"))
//
//ii_Connected = &
//	Integer(dw_MicroHelp.Describe("connected.width"))
//
//ii_CurrentDateTime = &
//	Integer(dw_MicroHelp.Describe("currentdatetime.width"))
//
//
//// Call a functio that resizes and repositions this window on the parent window
//This.wf_parentResized()
//
//// Trigger the timer event to update the clock
//If Lower(ProfileString(iw_frame.is_UserIni, "System Settings", "Clock", "on")) <> "off" Then
//	Timer(60)
//	This.TriggerEvent(TIMER!)
//End if
//
//
end event

on timer;datetime	ld_datetime


ld_datetime = DateTime(Today(), Now())

dw_microhelp.SetItem(1, "currentdatetime", ld_datetime)
end on

on w_custommicrohelp.create
this.dw_microhelp=create dw_microhelp
this.Control[]={ this.dw_microhelp}
end on

on w_custommicrohelp.destroy
destroy(this.dw_microhelp)
end on

type dw_microhelp from datawindow within w_custommicrohelp
int X=5
int Width=3187
int Height=81
int TabOrder=1
string DataObject="d_custommicrohelp"
boolean Border=false
boolean LiveScroll=true
end type

