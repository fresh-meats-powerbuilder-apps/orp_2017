﻿$PBExportHeader$u_download_data.sru
$PBExportComments$Export Ibdkdld
forward
global type u_download_data from nonvisualobject
end type
end forward

global type u_download_data from nonvisualobject
end type
global u_download_data u_download_data

type prototypes

end prototypes

type variables




end variables

forward prototypes
public function boolean nf_checkformessages ()
public function boolean nf_update_registry ()
end prototypes

public function boolean nf_checkformessages ();u_utl001 	lu_utl001
Char			lc_messages
s_error		lstr_error_info

lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_app_name = Message.nf_Get_App_ID()
lstr_error_info.se_message = Space(70)

lu_utl001 = Create u_utl001
// Don't worry about return code, since it could set MicroHelp if successful
lu_utl001.nf_utlu12ar(lstr_error_info, lc_messages)

If lc_messages = 'Y' Then
	gw_netwise_frame.im_netwise_menu.m_options.m_ViewMessages.ToolbarItemVisible = True
	return True
Else
	gw_netwise_frame.im_netwise_menu.m_options.m_ViewMessages.ToolbarItemVisible = False
	return False
End if

IF Isvalid (lu_utl001) THEN Destroy	lu_utl001
end function

public function boolean nf_update_registry ();//*************************************************************
//Run the installer logic **EEM**
//*************************************************************
	nvuo_windows_installer u_installer
	u_installer = CREATE nvuo_windows_installer 
	IF Isvalid (u_installer) THEN
		u_installer.uf_install()
		 Destroy	u_installer
	END IF
	
	Return False
//*************************************************************
//END
//*************************************************************
end function

on u_download_data.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_download_data.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;String		ls_application
Long			ll_count

SQLCA.nf_getsqlcafromini(gw_netwise_frame.is_userini)

gw_netwise_frame.SetMicroHelp('Connecting to the local database')
Open(w_loadingtables, gw_netwise_frame)

gw_netwise_frame.SetMicroHelp('Checking installed components')
//nf_update_registry() 

w_loadingtables.Event ue_status('Loading Application Defaults ...')

IF SQLCA.DBHandle()	<>	0 THEN RETURN
IF SQLCA.nf_connect() THEN
	gw_netwise_frame.SetMicrohelp('Connected to the Database')
	//Check to see if the database has any data.
	  SELECT count(tutltypes.record_type)  
		 INTO :ll_count  
		 FROM tutltypes  ;
	If ll_count < 1 Then 
		ls_application = Message.nf_Get_App_ID()
		If ls_application = "ORP" Or ls_application = "PAS" Then
			MessageBox('Empty Database', 'The Database is empty, Call Applications.', Exclamation!, ok!)
		End IF
	End IF
END IF

gw_netwise_frame.SetMicroHelp("Checking for new messages ...")
nf_CheckforMessages()
gw_netwise_frame.SetMicroHelp("Ready")
end event

event destructor;Close(w_loadingtables)

end event

