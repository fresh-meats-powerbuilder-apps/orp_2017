﻿$PBExportHeader$w_rpc_error.srw
forward
global type w_rpc_error from window
end type
type cb_email from commandbutton within w_rpc_error
end type
type d_rpc_error from datawindow within w_rpc_error
end type
type cb_print from commandbutton within w_rpc_error
end type
type cb_exit from commandbutton within w_rpc_error
end type
type cb_ok from commandbutton within w_rpc_error
end type
end forward

global type w_rpc_error from window
integer x = 114
integer y = 208
integer width = 2624
integer height = 1800
boolean titlebar = true
string title = "Error Information"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
event type integer ue_closeuserobject ( u_netwise_dw au_userobject )
cb_email cb_email
d_rpc_error d_rpc_error
cb_print cb_print
cb_exit cb_exit
cb_ok cb_ok
end type
global w_rpc_error w_rpc_error

type prototypes

end prototypes

type variables
MailSession	ims_mail
end variables

forward prototypes
public function integer nf_find_severity (long al_commerror, long al_primaryerror, long al_secondaryerror, long al_neterror)
end prototypes

event ue_closeuserobject;Return CloseUserObject ( au_userobject )
end event

public function integer nf_find_severity (long al_commerror, long al_primaryerror, long al_secondaryerror, long al_neterror);//Return Values	1 - "Please close applications, exit windows and reboot your machine"
//  					2 - "Please close all applications and exit windows"
//						3 - "Please close the application"
//						4 - "Please close the current window"
//						5 - "Try your last action again"
//					  -1 - "Please call the help desk at 3133"

CHOOSE CASE al_neterror
	Case 10036
		Return 2
	Case	0
		CHOOSE CASE	al_secondaryerror
			Case	115
				CHOOSE CASE al_primaryerror
					Case	115
						CHOOSE CASE	al_commerror
							Case	108
								Return 7
							Case Else
									Return -1
						END CHOOSE
					Case	Else
							Return -1
				END CHOOSE	
			Case 0
				Choose Case al_secondaryerror
					Case 0 
						Choose Case	al_primaryerror
							Case	0
								Choose Case al_commerror
									Case 113
										Return 2
									Case ELSE 
										Return -1
								END CHOOSE
							Case 	ELSE
								Return -1
						END CHOOSE
					Case Else
						Return -1
				END CHOOSE	
			Case 609
				Choose Case al_Primaryerror
					Case 113
						Choose Case al_commerror
							Case 108
								Return 6
							Case Else
								Return -1
						END Choose
					Case Else
						Return -1
				End Choose
			Case Else
					Return -1
		END CHOOSE		
	Case Else
		Return -1
END CHOOSE
	
end function

event open;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_rpc_error.open
//
// Purpose:
// 			Displays rpc errors and allows the user to either continue
//				running the application, or print the 
//				error message.  Called from f_check_rpc_error
//
// Log:
// 
// DATE		NAME				REVISION  Rev Description
//------		-------------------------------------------------------------
// 10/27/94 krishna        ver 1.0
// 04/19/95 James A. Weier ver 1.1   Display Memory Usage
// 05/06/96 James A. Weier	ver 2.0   Give more meaningful message to the user
///////////////////////////////////////////////////////////////////////////

Long		ll_RowFound,&
			ll_Severity

String	ls_Error_Message,&
			ls_winsock_msg,&
			ls_Directions

DataStore	ldw_severity
DataStore	ldw_WinSockErrors
s_rpc_error s_rpc_error_info

s_rpc_error_info = Message.PowerObjectParm
 
ldw_severity = Create DataStore
ldw_severity.DataObject = "d_directions_on_error"

ldw_WinsockErrors = Create DataStore
ldw_WinsockErrors.DataObject = "d_winsock_errors"






d_rpc_error.insertrow (1)

d_rpc_error.setitem (1, "app_name", s_rpc_error_info.se_app_name)
d_rpc_error.setitem (1, "window_name", s_rpc_error_info.se_window_name)
d_rpc_error.setitem (1, "function_name", s_rpc_error_info.se_function_name)
d_rpc_error.setitem (1, "event_name", s_rpc_error_info.se_event_name)
d_rpc_error.setitem (1, "procedure_name", s_rpc_error_info.se_procedure_name)
d_rpc_error.setitem (1, "return_code", s_rpc_error_info.se_return_code)
//d_rpc_error.setitem (1, "user_id", s_rpc_error_info.se_user_id)
d_rpc_error.setitem (1, "message", s_rpc_error_info.se_message)
d_rpc_error.setitem (1, "rval", s_rpc_error_info.se_rval)
d_rpc_error.setitem (1, "commerror", s_rpc_error_info.se_commerror)
d_rpc_error.setitem (1, "commerrmsg", s_rpc_error_info.se_commerrmsg)
d_rpc_error.setitem (1, "neterror", s_rpc_error_info.se_neterror)
d_rpc_error.setitem (1, "primaryerror", s_rpc_error_info.se_primaryerror)
d_rpc_error.setitem (1, "secondaryerror", s_rpc_error_info.se_secondaryerror)

ll_RowFound = ldw_WinSockErrors.Find( "Value='"+String(s_rpc_error_info.se_neterror)+"'", 1, ldw_WinSockErrors.RowCount())

IF ll_RowFound > 0 Then
	ls_winsock_msg = ldw_WinSockErrors.GetItemString( ll_RowFound,"Meaning")
ELSE
	ls_winsock_msg = ""
END IF

ll_severity = nf_find_severity(s_rpc_error_info.se_commerror,&
										s_rpc_error_info.se_primaryerror,&
										s_rpc_error_info.se_secondaryerror,&
										s_rpc_error_info.se_neterror)

ll_rowFound = ldw_severity.Find("SEVERITY_LEVEL = '"+String(ll_Severity)+"'", 1, ldw_severity.RowCount())
IF ll_RowFound > 0 Then
	ls_Directions = ldw_severity.GetItemString( ll_RowFound, "INSTRUCTION")
	//Added below line for for HD0000000636867
	ls_Directions=Mid(ls_Directions,1,Pos(ls_Directions, "3133")-1) +gs_helpdesk_no
ELSE
	//Start changes for HD0000000636867
	//ls_Directions = "Unknown Error Please call the help desk at 3133. Please write down, or print all information currently on the screen."
	ls_Directions = "Unknown Error Please call the help desk at "+gs_helpdesk_no+". Please write down, or print all information currently on the screen."
	//End changes for HD0000000636867
END IF
ls_Error_Message =	Trim(s_rpc_error_info.se_neterrmsg) + " "+&
							Trim(ls_winsock_msg) + " " + &
							Trim(ls_Directions)


d_rpc_error.setitem (1, "neterrmsg", ls_Error_Message)							

Destroy ldw_severity 
Destroy ldw_WinSockErrors  

end event

on w_rpc_error.create
this.cb_email=create cb_email
this.d_rpc_error=create d_rpc_error
this.cb_print=create cb_print
this.cb_exit=create cb_exit
this.cb_ok=create cb_ok
this.Control[]={this.cb_email,&
this.d_rpc_error,&
this.cb_print,&
this.cb_exit,&
this.cb_ok}
end on

on w_rpc_error.destroy
destroy(this.cb_email)
destroy(this.d_rpc_error)
destroy(this.cb_print)
destroy(this.cb_exit)
destroy(this.cb_ok)
end on

type cb_email from commandbutton within w_rpc_error
boolean visible = false
integer x = 2176
integer y = 236
integer width = 379
integer height = 88
integer taborder = 41
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&E-mail"
end type

event destructor;If IsValid(ims_mail) Then 
	ims_mail.mailLogoff()
	Destroy(ims_Mail)
End if
end event

event clicked;mailMessage		lmm_Msg

mailRecipient	lmr_to

String			ls_Body, &
					ls_TO

ls_TO =  ProfileString('ibp002.ini', 'EMAIL', "TO", "HELP DESK")

If MessageBox("E-mail Help Desk", "This will e-mail this error to the Help Desk." + &
				"Do you want to continue?", Question!, YesNo!, 1) = 2 Then
	MessageBox("E-mail Help Desk", "E-mail was not sent to the Help Desk")
	return
End if

If Not IsValid(ims_mail) Then
	ims_mail = CREATE mailSession

	If ims_Mail.mailLogon() <> mailReturnSuccess! Then
		//Start changes for HD0000000636867
		//MessageBox("E-Mail Help Desk", "Unable to start mail.  Please call the help desk at extension 3133")
		MessageBox("E-Mail Help Desk", "Unable to start mail.  Please call the help desk at " + gs_helpdesk_no)
		//End changes for HD0000000636867

		Destroy(ims_mail)
		return
	End if
End if

ls_Body = "This message was generated by the " + &
			d_rpc_error.GetItemString (1, "app_name") + &
			" system.~r~n~r~n" + &
			"Please log this issue and forward it on to Applications Development.~r~n"
ls_Body += "This error was generated by user: " + SQLCA.userid + "~r~n~r~n"
			
ls_Body += "Application ID:~t" + d_rpc_error.GetItemString (1, "app_name") + "~r~n"
ls_Body += "Window Name:~t" + d_rpc_error.GetItemString (1, "window_name") + "~r~n"
ls_Body += "Function Name:~t" + d_rpc_error.GetItemString (1, "function_name") + "~r~n"
ls_Body += "Event Name:~t" + d_rpc_error.GetItemString (1, "event_name") + "~r~n"
ls_Body += "Procedure Name:~t" + d_rpc_error.GetItemString (1, "procedure_name") + "~r~n"
ls_Body += "Return Code:~t" + d_rpc_error.GetItemString (1, "return_code") + "~r~n"
//ls_Body += "User ID:~t" + d_rpc_error.GetItemString (1, "user_id") + "~r~n"
ls_Body += "Message:~t" + d_rpc_error.GetItemString (1, "message") + "~r~n"
ls_Body += "Rval:~t" + String(d_rpc_error.GetItemNumber (1, "rval")) + "~r~n"
ls_Body += "Comm Error:~t" + String(d_rpc_error.GetItemNumber (1, "commerror")) + "~r~n"
ls_Body += "Comm Error Message:~t" + d_rpc_error.GetItemString (1, "commerrmsg") + "~r~n"
ls_Body += "Net Error:~t" + String(d_rpc_error.GetItemNumber (1, "neterror")) + "~r~n"
ls_Body += "Primary Error:~t" + String(d_rpc_error.GetItemNumber (1, "primaryerror")) + "~r~n"
ls_Body += "Secondary Error:~t" + String(d_rpc_error.GetItemNumber (1, "secondaryerror")) + "~r~n"
ls_Body += "Net Error Message:~t" + d_rpc_error.GetItemString (1, "neterrmsg") + "~r~n"
 
lmm_Msg.Subject = 'Message from the ' + d_rpc_error.GetItemString (1, "app_name") + ' system'
lmm_Msg.Recipient[1].Name = ls_TO
lmm_Msg.NoteText = ls_Body


If ims_mail.MailSend(lmm_Msg) = mailReturnSuccess! Then
	MessageBox("E-mail Help Desk", "The message was successfully sent to the Help Desk")
Else
	//Start changes for HD0000000636867
	//MessageBox("E-mail Help Desk", "There was an error e-mailing the Help Desk.  Please call them at Extension 3133")
	MessageBox("E-mail Help Desk", "There was an error e-mailing the Help Desk.  Please call them at "+gs_helpdesk_no)
	//End changes for HD0000000636867
End if

end event

type d_rpc_error from datawindow within w_rpc_error
integer x = 23
integer y = 16
integer width = 2103
integer height = 1608
integer taborder = 10
boolean enabled = false
string dataobject = "d_rpc_error"
borderstyle borderstyle = stylelowered!
end type

type cb_print from commandbutton within w_rpc_error
integer x = 2176
integer y = 128
integer width = 379
integer height = 88
integer taborder = 40
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Print"
end type

event clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_print.clicked!
//
// Purpose:
// 			Event cb_print.clicked - Print the current error message
//				and write the error message to the supplied file name.
//
// Log:
// 
//  DATE		NAME				REVISION
// ------	-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

//li_prt   = printopen("Error Information")

d_rpc_error.print(TRUE)
// Print each string variable

//print    (li_prt, "Error Information - "+string(today())+" - "+string(now(), "HH:MM:SS")+" - "+vg_userid)
//print    (li_prt, " ")

//d_rpc_error.setitem (1, "function_name", s_rpc_error_info.se_function_name)
//d_rpc_error.setitem (1, "event_name", s_rpc_error_info.se_event_name)
//d_rpc_error.setitem (1, "procedure_name", s_rpc_error_info.se_procedure_name)
//d_rpc_error.setitem (1, "return_code", s_rpc_error_info.se_return_code)
////d_rpc_error.setitem (1, "user_id", s_rpc_error_info.se_user_id)
//d_rpc_error.setitem (1, "message", s_rpc_error_info.se_message)
//d_rpc_error.setitem (1, "commerror", s_rpc_error_info.se_commerror)
//d_rpc_error.setitem (1, "commerrmsg", s_rpc_error_info.se_commerrmsg)
//d_rpc_error.setitem (1, "neterror", s_rpc_error_info.se_neterror)
//d_rpc_error.setitem (1, "neterrmsg", s_rpc_error_info.se_neterrmsg)
//d_rpc_error.setitem (1, "primaryerror", s_rpc_error_info.se_primaryerror)
//d_rpc_error.setitem (1, "secondaryerror", s_rpc_error_info.se_secondaryerror)
//ls_line = "Error Number  : " + getitemstring(d_rpc_error,1,"app_name")
//print    (li_prt, ls_line)
//
//ls_line = "Error Message : " + getitemstring(d_rpc_error,1,"window_name")
//print    (li_prt, ls_line)
//
//ls_line = "Window/Menu   : " + getitemstring(d_rpc_error,1,3)
//print    (li_prt, ls_line)
//
//ls_line = "Object        : " + getitemstring(d_rpc_error,1,4)
//print    (li_prt, ls_line)
//
//ls_line = "Event         : " + getitemstring(d_rpc_error,1,5)
//print    (li_prt, ls_line)
//
//ls_line = "Line Number   : " + getitemstring(d_rpc_error,1,6)
//print    (li_prt, ls_line)

//printclose(li_prt)
return
end event

type cb_exit from commandbutton within w_rpc_error
boolean visible = false
integer x = 87
integer y = 984
integer width = 562
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Exit The Program"
boolean default = true
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_exit
//
// Purpose:
// 			Ends the application session
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

halt close
end on

type cb_ok from commandbutton within w_rpc_error
integer x = 2176
integer y = 20
integer width = 379
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
boolean default = true
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_continue
//
// Purpose:
// 			Closes w_system_error
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

close(parent)
end on

