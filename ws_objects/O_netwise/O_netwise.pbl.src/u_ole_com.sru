﻿$PBExportHeader$u_ole_com.sru
$PBExportComments$Used to create and use Active X DLL's.
forward
global type u_ole_com from oleobject
end type
end forward

global type u_ole_com from oleobject
end type
global u_ole_com u_ole_com

event error;String	ls_message


ls_message = String(errornumber) + " " + String(errortext)

MessageBox("u_ole_error",ls_message)

If errornumber = 803 Then 
	SetNull(returnvalue)
	action = ExceptionSubstituteReturnValue! 
else
	action = ExceptionIgnore!
End If

end event

event externalexception;String	ls_message

ls_message = String(resultcode) + " " + String(exceptioncode) + " " + String(description)

MessageBox("u_ole_externalexception",ls_message)

action = ExceptionIgnore!
end event

on u_ole_com.create
call oleobject::create
TriggerEvent( this, "constructor" )
end on

on u_ole_com.destroy
call oleobject::destroy
TriggerEvent( this, "destructor" )
end on

