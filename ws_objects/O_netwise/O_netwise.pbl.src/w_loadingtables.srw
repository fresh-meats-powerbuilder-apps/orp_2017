﻿$PBExportHeader$w_loadingtables.srw
forward
global type w_loadingtables from Window
end type
type st_1 from statictext within w_loadingtables
end type
type p_1 from picture within w_loadingtables
end type
end forward

global type w_loadingtables from Window
int X=604
int Y=345
int Width=1550
int Height=585
long BackColor=12632256
WindowType WindowType=child!
event type integer ue_status ( string as_text )
st_1 st_1
p_1 p_1
end type
global w_loadingtables w_loadingtables

type variables
integer ii_rectangle_width = 51
end variables

event ue_status;st_1.text	=	as_text
RETURN 1
end event

on timer;//int li_rec_width
//
//IF table_name.Text <> message.istr_loadtable_data.table_name THEN
//   table_name.Text      = message.istr_loadtable_data.table_name
//   ii_rectangle_width = 0
//END IF
//li_rec_width = Message.nf_get_rec_width()
//IF li_rec_width > ii_rectangle_width THEN
//   ii_rectangle_width = li_rec_width
//ELSE
//	ii_rectangle_width = ii_rectangle_width +10
//END IF
//r_1.Resize(ii_rectangle_width,45)
//If st_1.visible Then
//	st_1.visible = FALSE
//Else
//	st_1.visible = TRUE
//End If
//
end on

event open;//r_1.Resize(0,45)
//Timer(1)

end event

on w_loadingtables.create
this.st_1=create st_1
this.p_1=create p_1
this.Control[]={ this.st_1,&
this.p_1}
end on

on w_loadingtables.destroy
destroy(this.st_1)
destroy(this.p_1)
end on

type st_1 from statictext within w_loadingtables
int X=229
int Y=237
int Width=1125
int Height=89
boolean Enabled=false
string Text="Loading Tables......Please Wait"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-12
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type p_1 from picture within w_loadingtables
int X=5
int Width=1537
int Height=577
string PictureName="splash.bmp"
boolean FocusRectangle=false
boolean OriginalSize=true
end type

