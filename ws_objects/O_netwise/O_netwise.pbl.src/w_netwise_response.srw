﻿$PBExportHeader$w_netwise_response.srw
forward
global type w_netwise_response from w_base_response
end type
end forward

global type w_netwise_response from w_base_response
end type
global w_netwise_response w_netwise_response

forward prototypes
public function integer setmicrohelp (string as_microhelp)
end prototypes

public function integer setmicrohelp (string as_microhelp);return gw_netwise_frame.SetMicroHelp(as_microhelp)

end function

on w_netwise_response.create
call super::create
end on

on w_netwise_response.destroy
call super::destroy
end on

type cb_base_help from w_base_response`cb_base_help within w_netwise_response
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_netwise_response
end type

type cb_base_ok from w_base_response`cb_base_ok within w_netwise_response
end type

