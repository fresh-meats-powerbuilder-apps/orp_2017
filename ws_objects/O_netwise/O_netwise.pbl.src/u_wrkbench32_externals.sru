﻿$PBExportHeader$u_wrkbench32_externals.sru
forward
global type u_wrkbench32_externals from u_sdkcalls
end type
end forward

global type u_wrkbench32_externals from u_sdkcalls
end type
global u_wrkbench32_externals u_wrkbench32_externals

type prototypes
function int WBGetAPIVerRel( ref int version, ref int release ) &
    library "wrkbe32.dll"

function int WBGetVerRel( ref int version, ref int release ) &
    library "wrkbe32.dll"

function int WBGetCommHandle( ref int CommHnd ) &
    library "wrkbe32.dll"

function int WBReleaseCommHandle( int CommHnd ) &
    library "wrkbe32.dll"

function int WBSetServerAlias( string serveralias, int CommHnd ) &
    library "wrkbe32.dll" alias for "WBSetServerAlias;Ansi"

function int WBGetErrorInfo( ref int commerror, ref string commerrmsg, &
                             ref long neterror, ref string neterrmsg, &
                             ref long primaryerror, ref long secondaryerror, &
                             int CommHnd ) &
    library "wrkbe32.dll" alias for "WBGetErrorInfo;Ansi"

function int WBSetSecurity( string userid, string password, &
                            string key, int CommHnd ) &
    library "wrkbe32.dll" alias for "WBSetSecurity;Ansi"

function int WBSetUserInfo( long userinfo, int CommHnd ) &
    library "wrkbe32.dll"

function int WBDispNumToDP( string str, ref double dval ) &
    library "wrkbe32.dll" alias for "WBDispNumToDP;Ansi"

function int WBDPToDispNum( double dval, ref string str, int len ) &
    library "wrkbe32.dll" alias for "WBDPToDispNum;Ansi"

function int WBPackDecToDP( string str, ref double dval ) &
    library "wrkbe32.dll" alias for "WBPackDecToDP;Ansi"

function int WBDPToPackDec( double dval, ref string str, int len ) &
    library "wrkbe32.dll" alias for "WBDPToPackDec;Ansi"

Function int _nl_reset() library "nww32ti.dll"


function int CloseConn_orp003(int CommHnd) library "orp003.dll"
function int OpenConn_orp003(int CommHnd) library "orp003.dll"

function int CloseConn_cfm001(int CommHnd) library "cfm001.dll"
function int OpenConn_cfm001(int CommHnd) library "cfm001.dll"

function int OpenConn_orp020( int CommHnd) library "orp020.dll"
function int CloseConn_orp020( int CommHnd) library "orp020.dll"

function int OpenConn_orp002( int CommHnd) library "orp002.dll"
function int CloseConn_orp002( int CommHnd) library "orp002.dll"

function int OpenConn_cfm002( int CommHnd) library "cfm002.dll"
function int CloseConn_cfm002( int CommHnd) library "cfm002.dll"

function int OpenConn_orp001( int CommHnd) library "orp001.dll"
function int CloseConn_orp001( int CommHnd) library "orp001.dll"

function int OpenConn_otr002( int CommHnd) library "otr002.dll"
function int CloseConn_otr002( int CommHnd) library "otr002.dll"

function int OpenConn_otr003( int CommHnd) library "otr003.dll"
function int CloseConn_otr003( int CommHnd) library "otr003.dll"


end prototypes

on u_wrkbench32_externals.create
TriggerEvent( this, "constructor" )
end on

on u_wrkbench32_externals.destroy
TriggerEvent( this, "destructor" )
end on

