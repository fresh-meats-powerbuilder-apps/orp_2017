﻿$PBExportHeader$w_netwise_sheet.srw
forward
global type w_netwise_sheet from w_base_sheet
end type
end forward

global type w_netwise_sheet from w_base_sheet
event ue_addrow pbm_custom66
event ue_deleterow pbm_custom65
end type
global w_netwise_sheet w_netwise_sheet

forward prototypes
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function integer setmicrohelp (string as_microhelp)
public subroutine wf_print ()
end prototypes

on ue_addrow;call w_base_sheet::ue_addrow;wf_addrow()
end on

on ue_deleterow;call w_base_sheet::ue_deleterow;wf_deleterow()
end on

public function boolean wf_addrow ();Return True
end function

public function boolean wf_deleterow ();Return True
end function

public function integer setmicrohelp (string as_microhelp);return gw_netwise_frame.SetMicroHelp(as_microhelp)

end function

public subroutine wf_print ();
end subroutine

event activate;call super::activate;boolean	lb_rtn
String	ls_classname, &
			ls_Add_Access,&
			ls_Del_Access,&
			ls_Mod_Access,&
			ls_Inq_Access

			
ls_classname = UPPER(this.ClassName())
lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
IF NOT lb_rtn THEN
	ls_classname = 'W_MENU_DEFAULTS'
	lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
	IF NOT lb_rtn THEN
		// if the window is not listed in the security at all, then allow full permissions
   	ls_Add_Access =  'Y'
	   ls_Del_Access =  'Y'
		ls_Mod_Access =  'Y'
		ls_Inq_Access =  'Y'
	END IF
End if

IF UPPER(ls_Add_Access) = 'N' THEN
	gw_netwise_Frame.im_netwise_Menu.M_File.M_New.Enabled = FALSE
ELSE
	gw_netwise_Frame.im_netwise_Menu.M_file.m_New.Enabled = TRUE
END IF

IF UPPER(ls_Del_Access) = 'N' THEN
	gw_netwise_Frame.im_netwise_menu.M_file.m_Delete.Enabled = FALSE
ELSE
	gw_netwise_Frame.im_netwise_menu.M_file.M_Delete.Enabled = TRUE
END IF

IF UPPER(ls_Inq_Access) = 'N' THEN
	gw_netwise_Frame.im_netwise_menu.M_file.M_Inquire.Enabled = FALSE
ELSE
	gw_netwise_Frame.im_netwise_menu.M_file.M_Inquire.Enabled = TRUE
END IF

IF UPPER(ls_Mod_Access) = 'N' THEN
	gw_netwise_Frame.im_netwise_menu.M_file.M_Save.Enabled = FALSE
ELSE
	gw_netwise_Frame.im_netwise_menu.M_file.M_Save.Enabled = TRUE
END IF

   






end event

on w_netwise_sheet.create
call super::create
end on

on w_netwise_sheet.destroy
call super::destroy
end on

event ue_fileprint;call super::ue_fileprint;u_sdkcalls	lu_sdkcalls

u_utl001		lu_utl001

string		ls_def_printer, &
				ls_output_file, &
				ls_windows_dir, &
				ls_telephone_number, &
				ls_coverform, &
				ls_recipient_name, &
				ls_recipient_attn

Long			ll_FileNum

int			li_rtn

lu_sdkcalls	=	Create	u_sdkcalls

// Function that will print the datawindows on the sheet
//PrintSetup()
IF Not ib_print_ok Then Return 
lu_sdkcalls.nf_get_defaultprinter(ls_def_printer)

ls_windows_dir	=	lu_sdkcalls.nf_getwindowsdirectory()
Destroy	lu_sdkcalls

//Create a file output.fcl with the following contents. This will
//prevent the address book for the faxing to open up.
IF ib_faxing_available THEN
	IF Left(ls_def_printer, 8)	=	'HFLANFAX' THEN
		lu_utl001	=	Create u_utl001
//		li_rtn		=	lu_utl001.nf_utlu25ar_getfaxing_info(is_customer_info, is_typecode, &
//												 ls_telephone_number,ls_coverform, &
//												 ls_recipient_name, ls_recipient_attn)
		IF li_rtn <> 0 THEN
			This.TriggerEvent('ue_fileprint')
			RETURN
		END IF
				
		ls_output_file	='{{ begin }}~r~n' 	+ &
			+ '{{ quality standard }}~r~n' 	+ &
			+ '{{ priority high }}~r~n' 		+ &
			+ '{{ include lanfax.inc }}~r~n' + &
			+ '{{ owner lanfax }}~r~n' 		+ &
			+ '{{ fax ' + ls_telephone_number + ' }}~r~n' + &
			+ '{{ contact ' + ls_recipient_attn + ' }}~r~n'	+ &
			+ '{{ company ' + ls_recipient_name + ' }}~r~n' 	+ &
			+ '{{ notifyhost feedback.inc }}~r~n' + &
			+ '{{ comment PC  LAN ' + SQLCA.UserID + ' ' + This.Title+' }}~r~n' +&
			+ '{{ cover ' + ls_coverform + ' }}~r~n' +&
			+ '{{ end }}'
		
		ll_FileNum = FileOpen(ls_windows_dir + "\output.fcl", StreamMode!, Write!, LockWrite!, Replace!)							
		FileWrite(ll_FileNum, ls_output_file)
		FileClose(ll_FileNum)
	END IF
END IF


end event

