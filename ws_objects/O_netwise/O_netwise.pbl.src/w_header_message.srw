﻿$PBExportHeader$w_header_message.srw
forward
global type w_header_message from w_netwise_sheet
end type
type dw_message from u_netwise_dw within w_header_message
end type
end forward

global type w_header_message from w_netwise_sheet
integer x = 133
integer y = 328
integer width = 2629
integer height = 1168
string title = "Header Message"
long backcolor = 12632256
dw_message dw_message
end type
global w_header_message w_header_message

type variables
s_error istr_error_info

u_utl001 iu_utl001
u_ws_utl iu_ws_utl

string is_user_id_in, is_signon_id
end variables

forward prototypes
public function boolean wf_addrow ()
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_addrow ();RETURN TRUE
end function

public subroutine wf_delete ();long		ll_rowcount, i, ll_count

string	ls_message_string

integer 	li_commhandle, li_rtn

boolean	lb_return

ll_rowcount = dw_message.RowCount()

	For i = 1 to ll_rowcount
		IF dw_message.IsSelected(i) Then
			ll_count++
			ls_message_string = ls_message_string + &
				string(dw_message.GetItemNumber(i, "msg_number")) + "~t"
			IF ll_count > 250 Then 
				MessageBox("Maximum Limit Exceeded", "Cannot Delete more than 250 &
						rows at a time")
				Return 
			End IF
		End IF
	NEXT

li_rtn = MessageBox("Delete Messages", "Do you want to delete "&
		+ string(ll_count)+ " row(s) selected", INFORMATION!, YESNO!)

If li_rtn = 2 Then return 

SetRedraw(false)
istr_error_info.se_event_name = "wf_delete"
istr_error_info.se_procedure_name = "nf_utlu11ar"

//If Not Isvalid(iu_utl001) Then
//	iu_utl001 = Create u_utl001
//End If

  If Not Isvalid (iu_ws_utl) Then
	iu_ws_utl = Create u_ws_utl
End If
//lb_return = iu_utl001.nf_utlu11ar(istr_error_info, ls_message_string,&
//				 li_commhandle)
				 
lb_return = iu_ws_utl.nf_utlu11er(istr_error_info, ls_message_string)
				 


IF lb_return Then
	wf_deleterow()
	SetMicroHelp(String(ll_count) + " rows deleted")
	dw_message.ResetUpdate()
	SetRedraw(true)
Else
	SetRedraw(true)
End IF

end subroutine

public function boolean wf_deleterow ();long	ll_row

ll_row = 1
Do While ll_row <> 0
	ll_row = dw_message.GetSelectedRow(0)
	IF ll_row <> 0 THEN dw_message.DeleteRow(ll_row)
Loop

Return True
end function

public function boolean wf_retrieve ();string	ls_header_string, &
			ls_exception_string
			

long		ll_refetch_message,&
			ll_rowcount


boolean	lb_return

integer	li_commhandle, &
			li_pos
			
u_string_functions	lu_string


SetRedraw(False)
ls_exception_string = Trim(Message.StringParm)

IF IsNull(ls_exception_string) or ls_exception_string = " " THEN
	ls_exception_string = Space(31)
ELSE
	li_pos = Pos(ls_exception_string, "~t")
	is_user_id_in = Right(ls_exception_string, (Len(ls_exception_string) - li_pos))
	ls_exception_string = Left(ls_exception_string, li_pos - 1)
	This.Title = "Message Header for " + is_user_id_in
	IF Trim(is_signon_id) = Trim(is_user_id_in) THEN
		gw_netwise_frame.im_netwise_menu.m_file.m_delete.enable()
	ELSE
		gw_netwise_frame.im_netwise_menu.m_file.m_delete.disable()
	END IF
END IF

istr_error_info.se_window_name = "Header Message"
istr_error_info.se_event_name = "postopen"

//If Not Isvalid(iu_utl001) Then
//	iu_utl001 = Create u_utl001
//End If

ls_header_string = Space(17601)

//lb_return = iu_utl001.nf_utlu09ar(istr_error_info, ll_refetch_message,&
//				 ls_header_string, ls_exception_string, is_user_id_in, li_commhandle)
lb_return = iu_ws_utl.nf_utlu09er(istr_error_info, ls_header_string, ls_exception_string, is_user_id_in)


ls_header_string = Trim(ls_header_string)
ls_header_string = Left(ls_header_string, &
				Len(ls_header_string) - 2)

If lb_return Then
	If lu_string.nf_IsEmpty(ls_header_string) Then
		MessageBox("Information", "No Messages Found", Information!)
	Else
		dw_message.Reset()
		ll_rowcount = dw_message.ImportString(ls_header_string) 
		Do While ll_refetch_message > 0
			ls_header_string = Space(17601)
			ll_refetch_message = dw_message.GetItemNumber(ll_rowcount, 1)
//			lb_return = iu_utl001.nf_utlu09ar(istr_error_info, ll_refetch_message, &
//					ls_header_string, ls_exception_string, is_user_id_in, li_commhandle)
			lb_return = iu_ws_utl.nf_utlu09er(istr_error_info, ls_header_string, ls_exception_string, is_user_id_in)
			If NOT lb_return Then Return False
			ll_rowcount = dw_message.ImportString(ls_header_string)
		Loop
	End If
Else
	SetRedraw(true)
	return false
End If

dw_message.Sort()		
dw_message.ResetUpdate()
SetRedraw(True)
return true


end function

public function boolean wf_update ();long		ll_deletedcount, i, ll_count

string	ls_message_string

integer 	li_commhandle, li_rtn

boolean	lb_return

ll_deletedcount = dw_message.DeletedCount()

	For i = 1 to ll_deletedcount
		ll_count++
		ls_message_string = ls_message_string + string(&
			dw_message.GetItemNumber(i, "msg_number", delete!, true))+"~t"
			IF ll_count > 250 Then 
				MessageBox("Maximum Limit Exceeded", "Cannot Delete more than 250 &
						rows at a time")
				Return false
			End IF
	NEXT

li_rtn = MessageBox("Delete Messages", "Do you want to delete "&
		+ string(ll_count)+ " row(s) selected", INFORMATION!, YESNO!)

If li_rtn = 2 Then return false

SetRedraw(false)
istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_utlu11ar"

//If Not Isvalid(iu_utl001) Then
//	iu_utl001 = Create u_utl001
//End If
  If Not Isvalid (iu_ws_utl) Then
	iu_ws_utl = Create u_ws_utl
End If

//lb_return = iu_utl001.nf_utlu11ar(istr_error_info, ls_message_string,&
//				 li_commhandle)
  lb_return =iu_ws_utl.nf_utlu11er(istr_error_info, ls_message_string) 

SetRedraw(true)
IF lb_return Then
	MessageBox("Delete Successfull", string(ll_count) + " row(s) deleted")
	dw_message.ResetUpdate()
	return true
Else
	MessageBox("Delete Not Successfull", "There was an error deleting the rows")
	return false
End IF


end function

on ue_query;call w_netwise_sheet::ue_query;wf_retrieve()
dw_message.SetFocus()

end on

event ue_postopen;call super::ue_postopen;iu_ws_utl = CREATE u_ws_utl
is_signon_id  = SQLCA.UserID
istr_error_info.se_user_id = is_signon_id
TriggerEvent("ue_query")
end event

event close;call super::close;DESTROY iu_ws_utl
//If IsValid( iu_utl001) Then Destroy( iu_utl001)
gw_netwise_frame.im_netwise_menu.m_options.m_viewmessages.ToolbarItemvisible = False

end event

event activate;call super::activate;IF Trim(is_signon_id) = Trim(is_user_id_in) THEN
	gw_netwise_frame.im_netwise_menu.m_file.m_delete.enable()
ELSE
	gw_netwise_frame.im_netwise_menu.m_file.m_delete.disable()
END IF

gw_netwise_frame.im_netwise_menu.m_file.m_save.disable()
end event

event deactivate;call super::deactivate;gw_netwise_frame.im_netwise_menu.m_file.m_save.enable()
gw_netwise_frame.im_netwise_menu.m_file.m_delete.enable()
end event

on w_header_message.create
int iCurrent
call super::create
this.dw_message=create dw_message
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_message
end on

on w_header_message.destroy
call super::destroy
destroy(this.dw_message)
end on

type dw_message from u_netwise_dw within w_header_message
integer width = 2560
integer height = 1024
string dataobject = "d_header_message"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;long 						ll_message_number
string					ls_detail, &
							ls_detail_string
time						ldt_time
date						ld_date

IF NOT row > 0 THEN RETURN
ls_detail = dw_message.GetItemString(row, "detail")

If ls_detail = "Y" Then
	ll_message_number = dw_message.GetItemNumber(row, 1)
	If NOT ll_message_number > 0 Then return	
	ld_date = Date( DateTime(dw_message.GetItemDate(row, "date")))
	ldt_time = Time(DateTime(ld_date, dw_message.GetItemTime(row, "time")))
	ls_detail_string = String(ll_message_number)+"~t"+string(ld_date) &
			+"~t"+ string(ldt_time) +"~t"+ Trim(dw_message.GetItemString(row, "from")) &
			+"~t"+ Trim(dw_message.GetItemString(row, "subject"))
	ls_detail_string = ls_detail_string + "~r~n" + is_user_id_in 
	OpenWithParm(w_detail_message, ls_detail_string , gw_netwise_frame)
Else
	MessageBox("Select Error", "There are no details for this message", &
					Question!)
End If


end event

on constructor;call u_netwise_dw::constructor;is_selection = "3"
end on

