﻿$PBExportHeader$w_detail_message.srw
forward
global type w_detail_message from w_base_response
end type
type dw_detail from u_netwise_dw within w_detail_message
end type
type dw_1 from u_netwise_dw within w_detail_message
end type
end forward

global type w_detail_message from w_base_response
integer x = 357
integer y = 192
integer width = 2112
integer height = 888
string title = "Detail Message"
long backcolor = 12632256
dw_detail dw_detail
dw_1 dw_1
end type
global w_detail_message w_detail_message

type variables
u_utl001 iu_utl001
u_ws_utl iu_ws_utl

end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();string				ls_detail_string,&
						ls_final_string, &
						ls_header_string, &
						ls_user_id
			
long					ll_refetch_linenum,&
						ll_message_number
						
boolean				lb_return

integer				li_commhandle, &
						li_pos

s_error				lstr_error_info	

SetRedraw(false)
ls_header_string  = Trim(Message.StringParm)
IF IsNull(ls_header_string) or ls_header_string = "" THEN
	SetRedraw(true)
	Return False
ELSE
	li_pos = Pos(ls_header_string, "~r~n")
	ls_user_id = Right(ls_header_string, (Len(ls_header_string) - (li_pos + 1) ))
	ls_header_string = Left(ls_header_string, (li_pos - 1))
	This.Title = "Detail Message For " + ls_user_id
	dw_detail.ImportString(ls_header_string)
END IF

ll_message_number = dw_detail.GetItemNumber(1,"msg_number")

lstr_error_info.se_app_name = "Detail Message"
lstr_error_info.se_window_name = "Detail Message"
lstr_error_info.se_event_name = "postopen"
lstr_error_info.se_procedure_name = "nf_utlu10ar"
lstr_error_info.se_user_id = SQLCA.UserID
lstr_error_info.se_message = Space(71)

//If Not Isvalid(iu_utl001) Then
//	iu_utl001 = Create u_utl001
//End If

If Not Isvalid(iu_ws_utl) Then
	iu_ws_utl = Create u_ws_utl
End If


ls_final_string = Space(20001)

//lb_return = iu_utl001.nf_utlu10ar(lstr_error_info, ll_message_number,&
//				 ll_refetch_linenum, ls_final_string, ls_user_id, li_commhandle)
				 
lb_return = iu_ws_utl.nf_utlu10er(lstr_error_info, ls_final_string, ls_user_id, ll_message_number )				 
		
If lb_return Then
		Do While ll_refetch_linenum > 0
			ls_detail_string = Space(20001)
//			lb_return = iu_utl001.nf_utlu10ar(lstr_error_info, ll_message_number, &
//							ll_refetch_linenum, ls_detail_string, ls_user_id, li_commhandle)
			lb_return =iu_ws_utl.nf_utlu10er(lstr_error_info, ls_detail_string, ls_user_id, ll_message_number)
			If NOT lb_return Then Return False
			ls_final_string = ls_final_string + "~r~n" + ls_detail_string
		Loop
Else
	return false
End If 		

dw_1.ImportString(ls_final_string)
dw_1.ResetUpdate()
dw_detail.ResetUpdate()
setredraw(true)
return true


end function

on ue_postopen;call w_base_response::ue_postopen;SetRedraw(false)
wf_retrieve()
SetRedraw(true)
end on

on w_detail_message.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_1
end on

on w_detail_message.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_1)
end on

event ue_base_ok;call super::ue_base_ok;Close(This)
end event

type cb_base_help from w_base_response`cb_base_help within w_detail_message
boolean visible = false
integer x = 55
integer y = 640
integer taborder = 10
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_detail_message
boolean visible = false
integer x = 379
integer y = 640
integer taborder = 30
end type

type cb_base_ok from w_base_response`cb_base_ok within w_detail_message
integer x = 818
integer y = 636
integer taborder = 20
end type

type dw_detail from u_netwise_dw within w_detail_message
integer y = 4
integer width = 2075
integer height = 188
integer taborder = 40
string dataobject = "d_detail"
boolean border = false
end type

type dw_1 from u_netwise_dw within w_detail_message
integer y = 196
integer width = 2080
integer height = 412
integer taborder = 50
string dataobject = "d_detail_message"
boolean vscrollbar = true
boolean border = false
end type

