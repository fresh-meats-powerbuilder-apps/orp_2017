﻿$PBExportHeader$w_sales_order_variance.srw
$PBExportComments$Sales Order Variance Report
forward
global type w_sales_order_variance from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_sales_order_variance
end type
type dw_header from u_base_dw_ext within w_sales_order_variance
end type
type dw_print from u_base_dw_ext within w_sales_order_variance
end type
end forward

global type w_sales_order_variance from w_base_sheet_ext
integer width = 2816
integer height = 1480
string title = "Sales Order Variance Report"
dw_detail dw_detail
dw_header dw_header
dw_print dw_print
end type
global w_sales_order_variance w_sales_order_variance

type variables
// String to inquire upon
string	is_inquire

//Netwise object to communicate with the RPC
u_orp002	iu_orp002

u_ws_orp3 	iu_ws_orp3

s_error	istr_error_info
end variables

forward prototypes
public subroutine wf_print ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_print ();DataWindowChild	ldwc_temp_header, &
						ldwc_temp_detail

Long		ll_RowCount

Integer	li_Counter

String	ls_tsr, &
			ls_location, &
			ls_detail_data

dw_print.GetChild("dw_header",ldwc_temp_header)
dw_print.GetChild("dw_detail",ldwc_temp_detail)

dw_header.ShareData(ldwc_temp_header)

ls_detail_data=dw_detail.Describe("DataWindow.Data")
ldwc_temp_detail.ImportString(ls_detail_data)
ll_RowCount=ldwc_temp_detail.RowCount()
FOR li_Counter = 1 TO ll_RowCount
	ldwc_temp_detail.SetItem(li_Counter,"customer_name", &
				dw_detail.Describe("Evaluate('LookUpDisplay(customer_name) ', " + &
				String(li_Counter) + ")" ))
NEXT

ldwc_temp_detail.Modify("customer_name.DDDW.UseAsBorder=NO")
ldwc_temp_header.Modify("review_date.EditMask.Spin=NO")
IF dw_header.GetItemString(1,"alllocations") <> "Y" THEN
	ls_location = dw_header.GetItemString(1,"location")
	IF dw_header.GetItemString(1,"alltsrs") <> "Y" THEN
		ls_tsr = dw_header.GetItemString(1,"tsr")
	ELSE
		ls_tsr = "All"
	END IF
ELSE
	ls_location = "All"
	ls_tsr = "All"
END IF
ldwc_temp_header.Modify("tsr_t.Text = 'TSR: " + ls_tsr + "'" + &
								"location_t.Text = 'Location: " + &
										ls_location + "'")
ldwc_temp_header.Modify("review_date.Border='0'")

dw_print.Print()
return
end subroutine

public function boolean wf_retrieve ();String  	ls_header_in, &
			ls_detail_out
String	ls_temp_alltsrs


OpenWithParm(w_sales_order_variance_inq, is_inquire)
is_inquire = Message.StringParm
IF Len(is_inquire) = 0 THEN
	return false
END IF

dw_header.SetRedraw(false)

dw_header.Reset()
dw_header.ImportString(is_inquire)

ls_header_in = String(dw_header.GetItemDate(1,"review_date"), &
			"yyyy-mm-dd") + "~t"
IF dw_header.GetItemString(1,"alllocations") = "Y" THEN
	ls_header_in += "ALL" + "~t" + "ALL" + "~t"
ELSE
	ls_header_in += dw_header.GetItemString(1,"location") + "~t"
	IF dw_header.GetItemString(1,"alltsrs") = "Y" THEN
		ls_header_in += "ALL" + "~t"
	ELSE
		ls_header_in += dw_header.GetItemString(1,"tsr") + "~t"
	END IF
END IF

//IF iu_orp002.nf_orpo71ar_sales_order_variance(istr_error_info, &
//				ls_header_in,  &
//				ls_detail_out)  THEN
				
IF iu_ws_orp3.uf_orpo71fr_sales_order_variance(istr_error_info, &
				ls_header_in,  &
				ls_detail_out)  THEN				

	dw_detail.Reset()
 	dw_detail.ImportString(ls_detail_out)
	dw_header.SetRedraw(true)
	return true
ELSE
	dw_header.SetRedraw(true)
	return false
END IF

end function

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Disable('m_print')
end on

event ue_postopen;call super::ue_postopen;String			ls_location



Message.TriggerEvent("ue_getlocation")
ls_location = Message.StringParm
dw_header.SetItem(1,"location",ls_location)

dw_header.SetItem(1,"review_date",Today())

dw_header.Object.tsr.Protect=1
dw_header.Object.location.Protect=1
dw_header.Object.review_date.Protect=1
dw_header.Object.alllocations.Protect=1
dw_header.Object.alltsrs.Protect=1
dw_header.Object.tsr.Protect=1

dw_header.Object.tsr.Background.Color = '12632256'
dw_header.Object.location.Background.Color = '12632256'
dw_header.Object.review_date.Background.Color = '12632256'

iu_orp002 = Create u_orp002
iu_ws_orp3 = Create u_ws_orp3
is_inquire = ""

iw_frame.im_menu.m_file.m_inquire.PostEvent(Clicked!)
end event

event close;call super::close;IF IsValid(iu_orp002) THEN
	Destroy iu_orp002
END IF

IF IsValid(iu_ws_orp3) THEN
	Destroy iu_ws_orp3
END IF
end event

on w_sales_order_variance.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
this.dw_print=create dw_print
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.dw_print
end on

on w_sales_order_variance.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
destroy(this.dw_print)
end on

on activate;call w_base_sheet_ext::activate;iw_frame.im_menu.mf_Enable('m_print')
end on

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 75, this.height - dw_detail.y - 150)

long       ll_x = 75,  ll_y = 150

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)



end event

type dw_detail from u_base_dw_ext within w_sales_order_variance
integer x = 46
integer y = 416
integer width = 2688
integer height = 936
string dataobject = "d_sales_order_variance_detail2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;String	ls_order_num
Window	lw_sales_order_detail


IF row < 1 THEN Return

IF dwo.name = "order_num" THEN
	ls_order_num = dw_detail.GetItemString(row, &
		"order_num")	
	OpenSheetWithParm(lw_sales_order_detail,ls_order_num, &
		"w_sales_order_detail",iw_frame,8, &
		iw_frame.im_menu.iao_arrangeopen)
END IF
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild		ldwc_temp_storage


dw_detail.GetChild("customer_name",ldwc_temp_storage)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_temp_storage)
end event

type dw_header from u_base_dw_ext within w_sales_order_variance
integer x = 69
integer y = 36
integer width = 2514
integer height = 380
integer taborder = 30
string dataobject = "d_sales_order_variance_header"
boolean border = false
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild 	ldwc_temp_storage
u_project_functions	lu_project_functions

dw_header.GetChild("location",ldwc_temp_storage)
lu_project_functions.nf_getlocations(ldwc_temp_storage, '')

dw_header.GetChild("tsr",ldwc_temp_storage)
lu_project_functions.nf_gettsrs(ldwc_temp_storage, '')


return

end event

type dw_print from u_base_dw_ext within w_sales_order_variance
boolean visible = false
integer x = 2286
integer y = 136
integer width = 1947
integer height = 512
integer taborder = 20
string dataobject = "d_sales_order_variance_print"
end type

