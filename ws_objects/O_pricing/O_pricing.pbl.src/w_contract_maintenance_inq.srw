﻿$PBExportHeader$w_contract_maintenance_inq.srw
forward
global type w_contract_maintenance_inq from w_base_response_ext
end type
type dw_contract from u_base_dw_ext within w_contract_maintenance_inq
end type
type dw_inquire from u_base_dw_ext within w_contract_maintenance_inq
end type
end forward

global type w_contract_maintenance_inq from w_base_response_ext
integer width = 3035
integer height = 744
string title = "Contract Maintenance Inquire"
long backcolor = 12632256
dw_contract dw_contract
dw_inquire dw_inquire
end type
global w_contract_maintenance_inq w_contract_maintenance_inq

type variables
String is_openstring, &
          is_whatwastyped
			 
w_base_sheet_ext		iw_parentwindow

u_ws_orp3	iu_ws_orp3

s_error 		istr_error_info


end variables
on w_contract_maintenance_inq.create
int iCurrent
call super::create
this.dw_contract=create dw_contract
this.dw_inquire=create dw_inquire
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_contract
this.Control[iCurrent+2]=this.dw_inquire
end on

on w_contract_maintenance_inq.destroy
call super::destroy
destroy(this.dw_contract)
destroy(this.dw_inquire)
end on

event open;call super::open;is_openstring = Message.StringParm



end event

event ue_postopen;call super::ue_postopen;String 	ls_operator_customers_out, &
        		ls_contracts_out, &
			ls_input
				  
Integer   li_rtn, ll_row_cnt

DataWindowChild	ldwc_customerids, ldwc_contractStatus

iu_ws_orp3 = Create u_ws_orp3

ls_input = ''

dw_inquire.object.customer_id.Visible = false
dw_inquire.object.contract_status_t.Visible = false
dw_inquire.object.contract_status.Visible = false
dw_inquire.object.contract_id.Visible = true

dw_inquire.SetRedraw(false)
dw_contract.SetRedraw(false)

ls_input = ' ' + '~t' +  ' ' + '~t' + ' '+ '~t' + 'L' + '~r~n'

// ls_input   options - last value 
// C: Contracts for ID #  
// O: Operator/Status - return all contracts for that operator id/status combination
// L: List of Operator Customers for dropdown

If iu_ws_orp3.uf_orpo16gr_contract_maint_inq(istr_error_info, &
				ls_input, &
				ls_operator_customers_out, &
				ls_contracts_out) Then
		
	dw_inquire.GetChild('customer_id', ldwc_customerids)
	ldwc_customerids.Reset()
	ldwc_customerids.ImportString(ls_operator_customers_out)	
		
	dw_inquire.GetChild('contract_status', ldwc_contractStatus)	
	ldwc_contractStatus.SetTransObject(SQLCA)
	ldwc_contractStatus.Retrieve()
	dw_inquire.SetItem(1, 'contract_status', 'A')
	
Else
	dw_inquire.SetRedraw(true)
	dw_contract.SetRedraw(true)
End If

dw_inquire.SetFocus()
dw_inquire.SetColumn("contract_id")
dw_inquire.SetRedraw(true)
dw_contract.SetRedraw(true)


is_whatwastyped = ''	

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"CANCEL")
end event

event ue_base_ok;call super::ue_base_ok;String    ls_contractid, ls_create_date, ls_rtn_string
Long      ll_row, ll_rowpos

u_string_functions		lu_string_functions

IF dw_inquire.AcceptText() = -1 Then
		return	
End If

ll_row = dw_contract.GetSelectedRow(0)
If ll_row = 0 Then
	iw_frame.SetMicroHelp("Contract Number is a required field")
	return
End If

ls_contractid = Trim(dw_contract.GetItemString(ll_row, 'contract_id'))
ls_create_date = String(dw_contract.GetItemDate(ll_row, 'create_date'))

ls_rtn_string = ls_contractid + '~t' + ls_create_date + '~r~n'

CloseWithReturn(This, ls_rtn_string)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_contract_maintenance_inq
boolean visible = false
integer x = 978
integer y = 1004
integer width = 229
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_contract_maintenance_inq
integer x = 786
integer y = 504
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_contract_maintenance_inq
integer x = 379
integer y = 504
end type

type cb_browse from w_base_response_ext`cb_browse within w_contract_maintenance_inq
integer x = 544
integer y = 1036
end type

type dw_contract from u_base_dw_ext within w_contract_maintenance_inq
integer x = 1586
integer y = 32
integer width = 1381
integer height = 580
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_customer_contracts"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;This.SelectRow(0, false)
This.SelectRow(row, true)
end event

type dw_inquire from u_base_dw_ext within w_contract_maintenance_inq
integer x = 69
integer y = 28
integer width = 1454
integer height = 424
integer taborder = 10
string dataobject = "d_contract_maint_inq"
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;String   ls_ColumnName, &
		   ls_operCust, &
		   ls_contractStatus, &
		   ls_input, &
		   ls_operator_customers_out, &
		   ls_contracts_out
			
DataWindowChild	ldwc_temp			
	
ls_ColumnName = dwo.Name

iw_frame.SetMicroHelp('')

ls_input = ''
ls_operator_customers_out = ''
ls_contracts_out = ''

Choose Case ls_ColumnName
	Case 'inquire_option'
		If data = 'C' then
			dw_inquire.object.contract_id.Visible = true
			dw_inquire.object.customer_id.Visible = false
			dw_inquire.object.contract_status_t.Visible = false
			dw_inquire.object.contract_status.Visible = false
			dw_contract.Reset()
			dw_inquire.SetItem(1, "contract_id", '')
			dw_inquire.SetColumn("contract_id")
		End If
		If data = 'O' then
			dw_inquire.object.contract_id.Visible = false
			dw_inquire.object.customer_id.Visible = true
			dw_inquire.object.contract_status_t.Visible = true
			dw_inquire.object.contract_status.Visible = true
			dw_contract.Reset()
			dw_inquire.SetItem(1, "customer_id", '')
			dw_inquire.SetItem(1,"contract_status", "A")
			dw_inquire.SetColumn("customer_id")
		End If
	Case 'contract_id'
		dw_contract.Reset()
		ls_input = trim(data) + '~t' +  ' ' + '~t' + ' '+ '~t' + 'C' + '~r~n'
		If ls_input > '' Then
			If iu_ws_orp3.uf_orpo16gr_contract_maint_inq(istr_error_info, &
							ls_input, &
							ls_operator_customers_out, &
							ls_contracts_out) Then
					If ls_contracts_out > '' Then
						dw_contract.ImportString(ls_contracts_out)	
					Else
						iw_frame.SetMicroHelp("No contracts found for entered Contract Number")
					End If
			End If
		End If
	Case 'contract_status'
         dw_contract.Reset() 
		ls_contractStatus = trim(data)
		ls_operCust = dw_inquire.GetItemString(1, "customer_id")
		If ls_operCust > ''  Then
			ls_input = ' ' + '~t' +  ls_contractStatus + '~t' + ls_operCust + '~t' + 'O' + '~r~n'
			If iu_ws_orp3.uf_orpo16gr_contract_maint_inq(istr_error_info, &
							ls_input, &
							ls_operator_customers_out, &
							ls_contracts_out) Then
				     If ls_contracts_out > '' Then							
						dw_contract.ImportString(ls_contracts_out)	
					Else
						iw_frame.SetMicroHelp("No contracts found for selected operator customer and contract status.")
					End If
			End If			
		End If
	Case 'customer_id'
		dw_contract.Reset()
		ls_contractStatus = dw_inquire.GetItemString(1, "contract_status")
		ls_operCust = trim(data)
		ls_input = ' ' + '~t' +  ls_contractStatus + '~t' + ls_operCust + '~t' + 'O' + '~r~n'
		If iu_ws_orp3.uf_orpo16gr_contract_maint_inq(istr_error_info, &
						ls_input, &
						ls_operator_customers_out, &
						ls_contracts_out) Then
   		     If ls_contracts_out > '' Then							
				dw_contract.ImportString(ls_contracts_out)	
			Else
				iw_frame.SetMicroHelp("No contracts found for selected operator customer and contract status.")
			End If
		End If			
End Choose


end event

event itemerror;call super::itemerror;Return 1
end event

