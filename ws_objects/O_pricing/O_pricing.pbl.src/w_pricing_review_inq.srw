﻿$PBExportHeader$w_pricing_review_inq.srw
$PBExportComments$Inquire window for Pricing Review Queue
forward
global type w_pricing_review_inq from w_base_response_ext
end type
type dw_header from u_base_dw_ext within w_pricing_review_inq
end type
end forward

global type w_pricing_review_inq from w_base_response_ext
int Width=1458
int Height=560
boolean TitleBar=true
string Title="Pricing Review Queue Inquire"
long BackColor=12632256
dw_header dw_header
end type
global w_pricing_review_inq w_pricing_review_inq

type variables
String is_openstring
end variables

event ue_postopen;String 						ls_sman, &
								ls_location
								
u_string_functions		lu_string_functions								

IF NOT lu_string_functions.nf_isempty(is_openstring) THEN	
	dw_header.Reset()
	dw_header.ImportString(is_openstring)
	IF dw_header.GetItemString(1,"tsr") = "ALL" THEN
		dw_header.SetItem(1,"alltsrs","Y")
	END IF
ELSE
	message.triggerEvent("ue_getsmancode")
	ls_sman = message.stringparm

	message.triggerEvent("ue_getlocation")
	ls_location = message.stringparm

	dw_header.SetItem(1,"tsr", ls_sman)
	dw_header.SetItem(1,"location", ls_location)
END IF


end event

on w_pricing_review_inq.create
int iCurrent
call super::create
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
end on

on w_pricing_review_inq.destroy
call super::destroy
destroy(this.dw_header)
end on

on open;call w_base_response_ext::open;is_openstring = Message.StringParm
end on

event ue_base_ok;u_string_functions		lu_string_functions

IF dw_header.GetItemString(1,"alltsrs") = "N" THEN
	IF lu_string_functions.nf_IsEmpty(dw_header.GetItemString(1,"tsr") ) THEN
		MessageBox("No TSR","TSR is a required field.")
		Return
	END IF
END IF

IF lu_string_functions.nf_IsEmpty(dw_header.GetItemString(1,"location")) THEN
	MessageBox("No Location","Location is a required field.")
	Return
END IF

CloseWithReturn(This,dw_header.Describe("DataWindow.data"))
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pricing_review_inq
int X=1074
int Y=292
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pricing_review_inq
int X=1074
int Y=168
int TabOrder=50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pricing_review_inq
int X=1074
int Y=44
int TabOrder=10
end type

type cb_browse from w_base_response_ext`cb_browse within w_pricing_review_inq
int TabOrder=20
end type

type dw_header from u_base_dw_ext within w_pricing_review_inq
int X=78
int Y=60
int Width=768
int Height=340
int TabOrder=30
string DataObject="d_pricing_review_header"
end type

on constructor;call u_base_dw_ext::constructor;This.InsertRow(0)
end on

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild 		ldwc_temp_storage
u_project_functions	lu_project_functions


dw_header.GetChild("location",ldwc_temp_storage)
lu_project_functions.nf_getlocations(ldwc_temp_storage, '')


dw_header.GetChild("tsr",ldwc_temp_storage)
lu_project_functions.nf_gettsrs(ldwc_temp_storage, '')

end event

