﻿$PBExportHeader$w_contract_subproducts.srw
forward
global type w_contract_subproducts from w_base_response_ext
end type
type dw_products from u_base_dw_ext within w_contract_subproducts
end type
type dw_header from u_base_dw_ext within w_contract_subproducts
end type
type dw_product_search from u_base_dw_ext within w_contract_subproducts
end type
type cb_select_prod from commandbutton within w_contract_subproducts
end type
type cb_remove from commandbutton within w_contract_subproducts
end type
type cb_remove_all from commandbutton within w_contract_subproducts
end type
type dw_selected_products from u_base_dw_ext within w_contract_subproducts
end type
end forward

global type w_contract_subproducts from w_base_response_ext
integer width = 3191
integer height = 1920
string title = "Contract Product Substitution Maintenance"
boolean controlmenu = true
long backcolor = 67108864
dw_products dw_products
dw_header dw_header
dw_product_search dw_product_search
cb_select_prod cb_select_prod
cb_remove cb_remove
cb_remove_all cb_remove_all
dw_selected_products dw_selected_products
end type
global w_contract_subproducts w_contract_subproducts

type variables
Boolean	save 
Long	il_CurrentRowSelected

String	is_WhatWasTyped, &
		is_option, &
		is_contract_id, &
		is_line_id, &
		is_create_date
		
Integer	ii_keysTyped = 2

u_ws_orp3	iu_ws_orp3

s_error 		istr_error_info
end variables

forward prototypes
public function boolean wf_product_selected (string as_product)
public function boolean wf_update_products ()
end prototypes

public function boolean wf_product_selected (string as_product);Long		ll_row
String	ls_exp


ls_exp = "sku_product_code = '" + as_product + "'"

ll_row = dw_selected_products.Find(ls_exp, 1, dw_selected_products.RowCount())

If ll_row > 0 or as_product = dw_header.GetItemString(1,"product_code") Then
	Return True
Else
	Return False
End IF
end function

public function boolean wf_update_products ();Long			ll_idx, ll_DeletedCount, ll_ModifiedCount
String			ls_inputstring, ls_out


ll_DeletedCount = dw_selected_products.DeletedCount()
ll_ModifiedCount = dw_selected_products.ModifiedCount()

If ll_ModifiedCount = 0 and ll_DeletedCount = 0 Then
	Return true
End If

// Delete Sub Products
If ll_DeletedCount > 0 Then
	For ll_idx = 1 to ll_DeletedCount
		ls_inputstring += is_contract_id + '~t' + &
							   is_line_id + '~t' + &
							   dw_header.GetItemString(1,"product_code") + '~t' + &
						    	   dw_selected_products.GetItemString(ll_idx, "sku_product_code", Delete!, False) + '~t' + &
							   is_create_date + '~t' + &	
						        'D~r~n'
	Next
End If

// Add Sub Products (there is no update/modify functionality for this window)
If ll_ModifiedCount > 0 Then
	ll_idx = dw_selected_products.GetNextModified(0, Primary!)
	Do while ll_idx > 0
		ls_inputstring += is_contract_id + '~t' + &
		                         is_line_id + '~t' + &
							  dw_header.GetItemString(1,"product_code") + '~t' + &
  					           dw_selected_products.GetItemString(ll_idx, "sku_product_code")  + '~t' + &
							   is_create_date + '~t' + &
						       'I~r~n'
		ll_idx = dw_selected_products.GetNextModified(ll_idx, Primary!)
	Loop
		
End If

istr_error_info.se_event_name = "wf_update_products"

If iu_ws_orp3.uf_orpo20gr_sub_products_update(istr_error_info, &
				ls_inputstring, &
				ls_out)  Then

	dw_selected_products.ResetUpdate()
	return true
Else 
	return false
End If


end function

on w_contract_subproducts.create
int iCurrent
call super::create
this.dw_products=create dw_products
this.dw_header=create dw_header
this.dw_product_search=create dw_product_search
this.cb_select_prod=create cb_select_prod
this.cb_remove=create cb_remove
this.cb_remove_all=create cb_remove_all
this.dw_selected_products=create dw_selected_products
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_products
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.dw_product_search
this.Control[iCurrent+4]=this.cb_select_prod
this.Control[iCurrent+5]=this.cb_remove
this.Control[iCurrent+6]=this.cb_remove_all
this.Control[iCurrent+7]=this.dw_selected_products
end on

on w_contract_subproducts.destroy
call super::destroy
destroy(this.dw_products)
destroy(this.dw_header)
destroy(this.dw_product_search)
destroy(this.cb_select_prod)
destroy(this.cb_remove)
destroy(this.cb_remove_all)
destroy(this.dw_selected_products)
end on

event open;call super::open;

dw_products.SetTransObject (SQLCA)
dw_products.Retrieve()


end event

event ue_postopen;call super::ue_postopen;String			 ls_header_out, &
				 ls_selected_sub_products_out, &
				 ls_contract_id, &
				 ls_line_id, &
				 ls_create_date, &
				 ls_Inquire
				 
 iu_ws_orp3 = Create u_ws_orp3
 
 u_string_functions 	lu_string_functions	

ls_Inquire = Message.StringParm
iw_frame.iu_string.nf_ParseLeftRight(ls_Inquire, '~t', ls_contract_id, ls_line_id)
iw_frame.iu_string.nf_ParseLeftRight(ls_line_id, '~t', ls_line_id, ls_create_date)

is_contract_id = ls_contract_id
is_line_id = ls_line_id
is_create_date = ls_create_date

If iu_ws_orp3.uf_orpo19gr_contract_sub_products(istr_error_info, &
				ls_contract_id, &
				ls_line_id, &
				ls_create_date, &
				ls_header_out, &
				ls_selected_sub_products_out) Then

	dw_header.Reset()
	dw_selected_products.Reset()

	dw_header.ImportString(ls_header_out)	

    If NOT lu_string_functions.nf_isempty(ls_selected_sub_products_out) THEN
		dw_selected_products.ImportString(ls_selected_sub_products_out)
	End If
	
End If

dw_products.SetSort("sku_product_code, division_code")
dw_products.Sort()

dw_selected_products.ResetUpdate()

dw_product_search.InsertRow(0)
dw_product_search.SetFocus()

This.SetRedraw(True)
end event

event closequery;call super::closequery;Long ll_ModifiedCount, ll_DeletedCount
Integer li_rtn


If dw_selected_products.AcceptText() = -1 Then return 1

ll_ModifiedCount = dw_selected_products.ModifiedCount()
ll_DeletedCount = dw_selected_products.DeletedCount()
			
If ll_ModifiedCount + ll_DeletedCount > 0 Then
	li_rtn = MessageBox("Save Changes" , "Do you want to save changes?", Question!, YesNo!)
	If li_rtn = 1 Then 
		wf_update_products()
		Message.Stringparm = "SAVED"
	End If
	dw_selected_products.ResetUpdate()
End If

return 0
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_contract_subproducts
boolean visible = false
integer x = 2514
integer y = 1376
integer taborder = 0
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_contract_subproducts
integer x = 2619
integer y = 248
integer taborder = 80
end type

event cb_base_cancel::clicked;call super::clicked;Long ll_ModifiedCount, ll_DeletedCount
Integer li_rtn


If dw_selected_products.AcceptText() = -1 Then return 1

ll_ModifiedCount = dw_selected_products.ModifiedCount()
ll_DeletedCount = dw_selected_products.DeletedCount()
			
If ll_ModifiedCount + ll_DeletedCount > 0 Then
	li_rtn = MessageBox("Save Changes" , "Do you want to save changes?", Question!, YesNo!)
	If li_rtn = 1 Then 
		wf_update_products()
		CloseWithReturn(Parent, "SAVED")
	Else
		dw_selected_products.ResetUpdate()
		CloseWithReturn(Parent, "CANCEL")
	End If
Else
	CloseWithReturn(Parent, "CANCEL")		
End If


end event

type cb_base_ok from w_base_response_ext`cb_base_ok within w_contract_subproducts
integer x = 2619
integer y = 72
integer taborder = 70
end type

event cb_base_ok::clicked;call super::clicked;String	    ls_products


IF dw_selected_products.AcceptText() = -1 THEN
	SetMicroHelp("error in product list")
	Return
END IF

SetPointer(HourGlass!)

If wf_update_products() Then
	SetPointer(Arrow!)
	CloseWithReturn(Parent, "OK")
End If

SetPointer(Arrow!)
end event

type cb_browse from w_base_response_ext`cb_browse within w_contract_subproducts
integer x = 2030
integer y = 1476
integer taborder = 0
boolean enabled = false
end type

type dw_products from u_base_dw_ext within w_contract_subproducts
integer x = 32
integer y = 476
integer width = 1600
integer height = 1328
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_select_product"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;SetMicroHelp("Loading Product Codes")
SetPointer(HourGlass!)
This.SetTransObject(sqlca)
This.Retrieve()
SetPointer(Arrow!)
SetMicroHelp("Ready")

is_selection = '3'
end event

event ue_keydown;call super::ue_keydown;Long 		ll_row

ll_row = this.GetRow()

if Key = KeyDownArrow! and ll_row < this.RowCount() then
	ll_row ++
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
End If

if Key = KeyUpArrow! and ll_row > 1 then
	ll_row --
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
end if

Return 0

end event

type dw_header from u_base_dw_ext within w_contract_subproducts
integer x = 32
integer y = 28
integer width = 2450
integer height = 300
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_contract_subproduct_hdr"
boolean maxbox = true
boolean hscrollbar = true
boolean border = false
end type

type dw_product_search from u_base_dw_ext within w_contract_subproducts
event ue_another_key pbm_dwnkey
integer x = 32
integer y = 344
integer width = 1097
integer height = 108
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_contract_product"
boolean border = false
end type

event ue_another_key;Choose Case key
	Case	KeyBack!
		save = False
		ii_keystyped = Len(This.GetText())+ 1
	Case KeyLeftArrow!   
		ii_keystyped --
		if ii_keystyped < 2 then ii_keystyped = 2
	Case 	KeyEnd!
		save = FALSE
		ii_Keystyped = 10
	Case	KeyHome!
		save = False		
		ii_keystyped = 2
	Case	KeyRightArrow!
		save = False		
		ii_keystyped ++
		if ii_keystyped > 10 then ii_keystyped = 10
	Case Keydelete!
		save = False		
		ii_keystyped = Len(This.GetText())
END choose
end event

event itemfocuschanged;call super::itemfocuschanged;ii_keystyped = 2
end event

event itemerror;call super::itemerror;Return 1
end event

event editchanged;call super::editchanged;Long		ll_row, ll_first_row, ll_last_row
String	     ls_FindString


if String(dwo.Name) =  'product_code' then
	data = RightTrim(data)
	if is_whatwastyped = data then
		return
	else
		if len(is_whatwastyped) < len(data) then
			is_whatwastyped = data
		else
			is_whatwastyped = data
			return
		end if
	end if
	ls_FindString = "sku_product_code >= '"+ data +"'"
	ll_Row = dw_products.Find( ls_FindString, 1, dw_products.RowCount()+1)
	If ll_row > 0 Then 
		This.SelectText(len(data)+1,0)
		IF ll_row + 1 <= dw_products.RowCount() Then
			dw_products.SelectRow( il_CurrentRowSelected, fALSE)
			dw_products.SelectRow( ll_row, TRUE)
			il_CurrentRowSelected = ll_row
		END IF
		dw_products.ScrollToRow(ll_row)
		dw_products.SetRow(ll_row + 1)
	End If
	ll_first_row = Long(dw_products.Object.DataWindow.FirstRowOnPage)
	ll_last_row = Long(dw_products.Object.DataWindow.LastRowOnPage)
	If ll_row > ll_first_row and ll_row <= ll_last_row Then 
		dw_products.SetRedraw(False)
		dw_products.ScrollToRow(ll_row + ll_last_row - ll_first_row)
		dw_products.ScrollToRow(ll_row)
		dw_products.SetRow(ll_row + 1)
		dw_products.SetRedraw(True)
	End If
End If

Return
end event

type cb_select_prod from commandbutton within w_contract_subproducts
integer x = 1691
integer y = 732
integer width = 183
integer height = 116
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = ">"
end type

event clicked;Integer		li_row = 0, li_new
Long           ll_first_row, ll_last_row
String		    ls_product, ls_userid

ls_userid = SQLCA.Userid

ll_first_row = Long(dw_products.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_products.object.datawindow.LastRowOnPage)

li_row = dw_products.GetSelectedRow(li_row)
If li_row = 0 Then Return
Do
	ls_product = dw_products.Object.sku_product_code[li_row]
	If Not wf_product_selected(ls_product) Then
		li_new = dw_selected_products.InsertRow(0)
		dw_selected_products.object.sku_product_code[li_new] = ls_product
		dw_selected_products.object.last_update_date[li_new] = today()
		dw_selected_products.object.last_update_userid[li_new] = ls_userid
		dw_selected_products.object.create_date[li_new] = is_create_date
	End If
	li_row = dw_products.GetSelectedRow(li_row)
Loop Until li_row = 0

This.SetRedraw(False)

dw_selected_products.Sort()

dw_products.ScrollToRow(ll_last_row)
dw_products.ScrollToRow(ll_first_row)

This.SetRedraw(True)

end event

type cb_remove from commandbutton within w_contract_subproducts
integer x = 1691
integer y = 1008
integer width = 183
integer height = 116
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<"
end type

event clicked;Integer		li_row
Long  ll_first_row, ll_last_row


ll_first_row = Long(dw_products.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_products.object.datawindow.LastRowOnPage)

li_row = dw_selected_products.GetSelectedRow(0)
If li_row = 0 Then Return
Do
	dw_selected_products.DeleteRow(li_row)
	li_row = dw_selected_products.GetSelectedRow(0)
Loop Until li_row = 0

This.SetRedraw(False)

dw_selected_products.SelectRow(0,False)

dw_products.ScrollToRow(ll_last_row)
dw_products.ScrollToRow(ll_first_row)

This.SetRedraw(True)



end event

type cb_remove_all from commandbutton within w_contract_subproducts
integer x = 1691
integer y = 1284
integer width = 183
integer height = 116
integer taborder = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "<<"
end type

event clicked;Integer		li_row
Long		ll_first_row, ll_last_row

ll_first_row = Long(dw_products.object.datawindow.FirstRowOnPage)
ll_last_row = Long(dw_products.object.datawindow.LastRowOnPage)

This.SetRedraw(False)

Do
	li_row = dw_selected_products.RowCount()
	If li_row > 0 Then dw_selected_products.DeleteRow(1)
Loop Until li_row <= 0

dw_products.ScrollToRow(ll_last_row)
dw_products.ScrollToRow(ll_first_row)

This.SetRedraw(TRUE)
end event

type dw_selected_products from u_base_dw_ext within w_contract_subproducts
integer x = 1911
integer y = 476
integer width = 1221
integer height = 1324
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_productselected"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;is_selection = '3'
end event

