﻿$PBExportHeader$w_contract_maintenance.srw
forward
global type w_contract_maintenance from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_contract_maintenance
end type
type dw_header from u_base_dw_ext within w_contract_maintenance
end type
end forward

global type w_contract_maintenance from w_base_sheet_ext
integer width = 5358
integer height = 1692
string title = "Contract Maintenance"
dw_detail dw_detail
dw_header dw_header
end type
global w_contract_maintenance w_contract_maintenance

type variables
string	is_inquire, &
		is_whatwastyped
		
Boolean ib_refresh

u_ws_orp3       iu_ws_orp3

s_error	istr_error_info

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function string wf_buildupdatestring_dtl ()
public function string wf_buildupdatestring_hdr ()
public function boolean wf_check_dates (long al_row)
public subroutine wf_delete ()
public function boolean wf_validate_dtl (long al_row)
public function boolean wf_validate_hdr ()
end prototypes

public function boolean wf_retrieve ();String  	ls_header_out, &
			ls_operator_customers_out, &
			ls_detail_out
				
DataWindowChild	ldwc_customerids
						
u_string_functions 	lu_string_functions			

This.TriggerEvent(CloseQuery!)
IF Message.ReturnValue = 1 Then
	Return False
End If

If NOT ib_refresh Then
	OpenWithParm(w_contract_maintenance_inq, is_inquire)
	is_inquire = Trim(Message.StringParm)
	IF Len(is_inquire) = 0 THEN
		return false
	END IF
End If

If iu_ws_orp3.uf_orpo17gr_contract_maintenance(istr_error_info, &
				is_inquire, &
				ls_header_out, &
				ls_operator_customers_out, &
				ls_detail_out) Then			
		
	dw_header.SetRedraw(false)
	dw_detail.SetRedraw(false)
	dw_header.Reset()
	dw_detail.Reset()
	
	dw_header.GetChild('customer_id', ldwc_customerids)
	ldwc_customerids.Reset()
	ldwc_customerids.ImportString(ls_operator_customers_out)	

	If NOT lu_string_functions.nf_isempty(ls_header_out) THEN
		dw_header.ImportString(ls_header_out)
		dw_header.object.contract_id.Protect = true
		dw_header.object.contract_id.Background.Color = 12632256
	Else
		dw_header.InsertRow(0)
		dw_header.object.contract_id.Protect = false
		dw_header.object.contract_id.Background.Color = 16777215
		dw_header.SetItem(1, 'create_date', string(Today(), 'yyyy-mm-dd'))
	End If
	
	If NOT lu_string_functions.nf_isempty(ls_detail_out) THEN
    		dw_detail.ImportString(ls_detail_out)
	End If
	
	dw_detail.InsertRow(0)
	
	dw_header.SetRedraw(true)
	dw_detail.SetRedraw(true)
	
	If lu_string_functions.nf_isempty(ls_header_out) Then
		dw_header.SetFocus()
		dw_header.SetColumn("contract_id")
	Else
		dw_header.ResetUpdate()
	End If
	
	dw_detail.ResetUpdate()
	
	is_whatwastyped = ''
	return true	
Else
	return false
End If

end function

public function boolean wf_update ();String							ls_detail_in, &
								ls_out, &
								ls_header_in

Long							ll_ModifiedCount, &
								ll_DeletedCount, &
								ll_row

Integer                         li_rtn

u_string_functions	lu_string_functions

iu_ws_orp3 = Create u_ws_orp3
									
SetPointer(HourGlass!)

If dw_header.AcceptText() = -1 Then return False
If dw_detail.AcceptText() = -1 Then return False

ls_header_in = ''
ls_detail_in = ''

// Header
ll_ModifiedCount = dw_header.ModifiedCount()
If ll_ModifiedCount > 0 Then
	ls_header_in = wf_BuildUpdateString_Hdr()
	If lu_string_functions.nf_isempty(ls_header_in) Then
		This.SetRedraw(true)
		Return False
	End If
Else
	ls_header_in = 'NOCHG'
End If
	
// Detaill
ll_ModifiedCount = dw_detail.ModifiedCount()
If ll_ModifiedCount > 0 Then
	ls_detail_in = wf_BuildUpdateString_Dtl()
	If lu_string_functions.nf_isempty(ls_detail_in) Then
		This.SetRedraw(true)
		Return False
	End If
End If

//Delete buffer on detail
ll_DeletedCount = dw_detail.DeletedCount()
If ll_DeletedCount > 0 Then
	For ll_row = 1 to ll_DeletedCount
		ls_detail_in += trim(dw_detail.GetItemString(ll_row, "contract_id", Delete!, false)) + '~t' + &
							string(dw_detail.GetItemNumber(ll_row, "line_id", Delete!, false)) + '~t' + &
							dw_detail.GetItemString(ll_row, "product_code", Delete!, false) + '~t' + &
							string(dw_detail.GetItemDate(ll_row, "start_date", Delete!, false), "MM/DD/YYYY") + '~t' + &
							string(dw_detail.GetItemDate(ll_row, "end_date", Delete!, false),"MM/DD/YYYY") + '~t' + &									
							string(dw_detail.GetItemDecimal(ll_row, "contract_qty", Delete!, false)) + '~t' + &
							dw_detail.GetItemString(ll_row, "contract_qty_uom", Delete!, false) + '~t' + &
							string(dw_detail.GetItemDecimal(ll_row, "price_amt", Delete!, false)) + '~t' + &
							dw_detail.GetItemString(ll_row, "price_uom_type", Delete!, false) + '~t' + &									
							dw_detail.GetItemString(ll_row, "comment", Delete!, false) + '~t' + &		
							dw_detail.GetItemString(ll_row, "create_date", Delete!, false) + '~t' + &
							'D~r~n'
	Next
End If

If ll_ModifiedCount = 0 and ll_DeletedCount = 0 Then
	ls_detail_in = 'NOCHG'
End If

If ls_header_in = 'NOCHG' and ls_detail_in = 'NOCHG' Then
	iw_frame.SetMicroHelp('No Update Necessary.')
	This.SetRedraw(true)
	Return true
End If

istr_error_info.se_event_name = "wf_update"

li_rtn = iu_ws_orp3.uf_orpo18gr_contract_maint(istr_error_info, &
				ls_header_in, ls_detail_in, &
				ls_out) 
				
If li_rtn = 0 Then	
    ib_refresh = true
	is_inquire = dw_header.GetItemString(1, "contract_id") + '~t' + dw_header.GetItemString(1, "create_date") + '~r~n'
	dw_header.ResetUpdate()
	dw_detail.ResetUpdate()
	wf_retrieve()
	ib_refresh = false
	This.SetRedraw(true)
	return true
Else
	This.SetRedraw(true)
	return false
End If

end function

public function string wf_buildupdatestring_dtl ();String   ls_UpdateString, ls_comment
			
Long	  ll_DtlRow

u_string_functions	lu_string_functions

dwItemStatus		lis_UpdateStatus


ls_UpdateString = ''

SetRedraw(false)

ll_dtlRow = 0
ll_dtlRow =dw_detail.GetNextModified(ll_dtlRow, Primary!)

Do
	If Not wf_validate_dtl(ll_dtlRow) Then 
		dw_detail.SelectRow(0, False)
		dw_detail.SelectRow(ll_dtlRow, True)
		Return ''
	End If

	If Not wf_check_dates(ll_dtlRow) Then
		dw_detail.SelectRow(0, False)
		dw_detail.SelectRow(ll_dtlRow, True)
		SetMicroHelp("The Effective Date ranges with the same Product Code cannot overlap.")
		Return ''
	End If
	
	If IsNull(dw_detail.GetItemString(ll_dtlRow, 'comment')) Then
		ls_comment = ' '
	Else
		ls_comment = dw_detail.GetItemString(ll_dtlRow, 'comment')
	End If			
	
	dw_detail.SelectRow(0, False)
	dw_detail.SelectRow(ll_dtlRow, True)
	lis_UpdateStatus = dw_detail.GetItemStatus(ll_dtlRow, 0, Primary!)
	Choose Case lis_UpdateStatus		
		Case NewModified!	
			ls_UpdateString += trim(dw_header.GetItemString(1, 'contract_id')) + '~t' + &
									'0' + '~t' + &
									dw_detail.GetItemString(ll_dtlRow, 'product_code') + '~t' + &
									string(dw_detail.GetItemDate(ll_dtlRow, 'start_date'), "MM/DD/YYYY") + '~t' + &
									string(dw_detail.GetItemDate(ll_dtlRow, 'end_date'),"MM/DD/YYYY") + '~t' + &									
									string(dw_detail.GetItemDecimal(ll_dtlRow, 'contract_qty')) + '~t' + &
									dw_detail.GetItemString(ll_dtlRow, 'contract_qty_uom') + '~t' + &
									string(dw_detail.GetItemDecimal(ll_dtlRow, 'price_amt')) + '~t' + &
									trim(dw_detail.GetItemString(ll_dtlRow, 'price_uom_type')) + '~t' + &									
									ls_comment + '~t' + &	
									trim(dw_header.GetItemString(1, 'create_date')) + '~t' + &
									'I~r~n'
			
		Case DataModified!
			//line status is always Active - no need to pass to rpc
				ls_UpdateString += trim(dw_detail.GetItemString(ll_dtlRow, 'contract_id')) + '~t' + &
									string(dw_detail.GetItemNumber(ll_dtlRow, 'line_id')) + '~t' + &
									dw_detail.GetItemString(ll_dtlRow, 'product_code') + '~t' + &
									string(dw_detail.GetItemDate(ll_dtlRow, 'start_date'), "MM/DD/YYYY") + '~t' + &
									string(dw_detail.GetItemDate(ll_dtlRow, 'end_date'),"MM/DD/YYYY") + '~t' + &									
									string(dw_detail.GetItemDecimal(ll_dtlRow, 'contract_qty')) + '~t' + &
									dw_detail.GetItemString(ll_dtlRow, 'contract_qty_uom') + '~t' + &
									string(dw_detail.GetItemDecimal(ll_dtlRow, 'price_amt')) + '~t' + &
									trim(dw_detail.GetItemString(ll_dtlRow, 'price_uom_type')) + '~t' + &									
									ls_comment + '~t' + &			
									trim(dw_detail.GetItemString(ll_dtlRow, 'create_date')) + '~t' + &
									'U~r~n'
	End Choose
	ll_dtlRow = dw_detail.GetNextModified(ll_dtlRow, Primary!)
Loop While ll_dtlRow > 0


SetRedraw(true)
	
return ls_UpdateString
end function

public function string wf_buildupdatestring_hdr ();String   ls_UpdateString

u_string_functions  	lu_string_functions

dwItemStatus		lis_UpdateStatus

ls_UpdateString = ''

If Not wf_validate_hdr() Then 
	Return ''
End If

lis_UpdateStatus = dw_header.GetItemStatus(1, 0, Primary!)
Choose Case lis_UpdateStatus
	Case NewModified!				
			ls_UpdateString += trim(dw_header.GetItemString(1, 'contract_id')) + '~t' + &
								dw_header.GetItemString(1, 'contract_descr') + '~t' + &
								dw_header.GetItemString(1, 'customer_id') + '~t' + &
								trim(dw_header.GetItemString(1, 'contract_status')) + '~t' + &
								trim(dw_header.GetItemString(1, 'contract_type')) + '~t' + &
								dw_header.GetItemString(1, 'originating_tsr') + '~t' + &
								dw_header.GetItemString(1, 'create_date')  + '~t' + &
								'I~r~n'
			
	Case DataModified!
			ls_UpdateString += trim(dw_header.GetItemString(1, 'contract_id')) + '~t' + &
								dw_header.GetItemString(1, 'contract_descr') + '~t' + &
								dw_header.GetItemString(1, 'customer_id') + '~t' + &
								trim(dw_header.GetItemString(1, 'contract_status')) + '~t' + &
								trim(dw_header.GetItemString(1, 'contract_type')) + '~t' + &
								dw_header.GetItemString(1, 'originating_tsr') + '~t' + &
								dw_header.GetItemString(1, 'create_date')  + '~t' + &
								'U~r~n'
End Choose
		
return ls_UpdateString
				

end function

public function boolean wf_check_dates (long al_row);Long		ll_row_count, ll_row

String 	ls_current_product, ls_product

Date		ld_current_end_date, ld_current_start_date, ld_compare_start_date, ld_compare_end_date

//when the same product exists on multiple lines, the start and end dates between the lines cannot overlap

ls_current_product = Trim(dw_detail.GetItemString(al_row, "product_code"))
ld_current_start_date = dw_detail.GetItemDate(al_row, "start_date")
ld_current_end_date = dw_detail.GetItemDate(al_row, "end_date")

ll_row_count = dw_detail.RowCount() - 1

For ll_row = 1 to ll_row_count
	ls_product = Trim(dw_detail.GetItemString(ll_row, "product_code"))
	If ls_current_product = ls_product and ll_row <> al_row Then
		ld_compare_start_date = dw_detail.GetItemDate(ll_row, "start_date")
		ld_compare_end_date = dw_detail.GetItemDate(ll_row, "end_date")
		
		// current start date
		If ld_current_start_date <= ld_compare_end_date and &
		   ld_current_start_date >= ld_compare_start_date Then
				dw_detail.SelectRow(ll_row, true)
				return false
		End If
		
		// current end date
		If ld_current_end_date <= ld_compare_end_date and &
		   ld_current_end_date >= ld_compare_start_date Then
				dw_detail.SelectRow(ll_row, true)
				return false
		End If
		
		// 
		If ld_current_start_date <= ld_compare_start_date and &
		   ld_current_end_date  >= ld_compare_end_date Then
			    dw_detail.SelectRow(ll_row,true)
				 return false
		End If
		
	End If		
Next

return true
end function

public subroutine wf_delete ();Int     li_counter

Long	ll_RowCount, &
		ll_row, &
		ll_count
		
String  ls_DeleteString


ll_RowCount = dw_detail.RowCount()
ll_row = 0

Do
	ll_row = dw_detail.GetSelectedRow(ll_row)
	If ll_Row > 0 Then
		li_counter ++
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

If li_counter < 1 Then return 

If MessageBox("Delete Rows", "Are you sure you want to delete " + String(li_counter) + &
 				" rows?", Question!, YesNo!, 2) = 2 Then return 

SetRedraw(false)
ll_row = 0
Do
	ll_row = dw_detail.GetSelectedRow(0)
	If ll_Row > 0 Then
		dw_detail.DeleteRow(ll_row)
	End if
Loop while ll_row > 0 And ll_row < ll_RowCount

SetRedraw(true)

dw_detail.SetFocus()
end subroutine

public function boolean wf_validate_dtl (long al_row);String							ls_temp

Long							ll_count

Date							ldt_EndDate, ldt_StartDate

u_String_Functions		lu_string


ls_temp = dw_detail.GetItemString(al_row, 'product_code')
  
If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Product Code is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('product_code')
	dw_detail.SetRow(al_row)
	Return False
End If

Select Count(*) 
	into :ll_count
	from sku_products
	WHERE sku_products.sku_product_code = :ls_temp
	USING SQLCA;

IF ll_Count < 1 AND SQLCA.SQLCode = 0 then 		
	SetMicroHelp('Invalid/Inactive product code')
	dw_detail.SetFocus()
	dw_detail.SetColumn('product_code')
	dw_detail.SetRow(al_row)
	Return False
End If

ls_temp = string(dw_detail.GetItemDecimal(al_row, 'contract_qty'))

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Volume is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('contract_qty')
	dw_detail.SetRow(al_row)
	Return False
End If

If long(ls_temp) = 0 Then
	SetMicroHelp('Volume must be greater than 0')
	dw_detail.SetFocus()
	dw_detail.SetColumn('contract_qty')
	dw_detail.SetRow(al_row)
	Return False
End If	
 
ls_temp = dw_detail.GetItemString(al_row, 'contract_qty_uom')
  
If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Volume UOM is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('contract_qty_uom')
	dw_detail.SetRow(al_row)
	Return False
End If

ls_temp = string(dw_detail.GetItemDecimal(al_row, 'price_amt'))

If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Price is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('price_amt')
	dw_detail.SetRow(al_row)
	Return False
End If

If long(ls_temp) = 0 Then 
	SetMicroHelp('Price must be greater than 0')
	dw_detail.SetFocus()
	dw_detail.SetColumn('price_amt')
	dw_detail.SetRow(al_row)
	Return False
End If

ls_temp = dw_detail.GetItemString(al_row, 'price_uom_type')
  
If lu_string.nf_IsEmpty(ls_temp) Then 
	SetMicroHelp('Price UOM is a required field')
	dw_detail.SetFocus()
	dw_detail.SetColumn('price_uom_type')
	dw_detail.SetRow(al_row)
	Return False
End If
 
ldt_EndDate = dw_detail.GetItemDate(al_row,'end_date')
ldt_StartDate = dw_detail.GetItemDate(al_row,'start_date')

If DayName(ldt_StartDate) <> "Sunday" Then
	SetMicroHelp('Start Date must be a Sunday')
	dw_detail.SetFocus()
	dw_detail.SetColumn('start_date')
	dw_detail.SetRow(al_row)
	Return False
End If

If DayName(ldt_EndDate) <> "Saturday" Then
	SetMicroHelp('End Date must be a Saturday')
	dw_detail.SetFocus()
	dw_detail.SetColumn('end_date')
	dw_detail.SetRow(al_row)
	Return False
End If		

If ldt_EndDate <= ldt_StartDate Then
	SetMicroHelp('End Date must be greater than Start Date')
	dw_detail.SetFocus()
	dw_detail.SetColumn('start_date')
	dw_detail.SetRow(al_row)
	Return False
End If
 
 Return true
end function

public function boolean wf_validate_hdr ();String  ls_temp

u_string_functions  	lu_string_functions

ls_temp = dw_header.GetItemString(1, "contract_id")
If lu_string_functions.nf_isempty(ls_temp) Then
	SetMicroHelp("Contract ID is Required")
	dw_header.SetFocus()
	dw_header.SetColumn("contract_id")
	Return false
End If

ls_temp = dw_header.GetItemString(1, "contract_descr")
If lu_string_functions.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Contract Description is Required")
	dw_header.SetFocus()
	dw_header.SetColumn("contract_descr")	
	Return false
End If

//can be blank but not null
ls_temp = dw_header.GetItemString(1, "customer_id")
If isNull(ls_temp) Then
	SetMicroHelp("Operator Customer ID is Required")
	dw_header.SetFocus()
	dw_header.SetColumn("customer_id")
	Return false
End If

ls_temp = dw_header.GetItemString(1, "contract_status")
If lu_string_functions.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Contract Status is Required")
	dw_header.SetFocus()
	dw_header.SetColumn("contract_status")	
	Return false
End If

ls_temp = dw_header.GetItemString(1, "contract_type")
If lu_string_functions.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Contract Type is Required")
	dw_header.SetFocus()
	dw_header.SetColumn("contract_type")	
	Return false
End If

ls_temp = dw_header.GetItemString(1, "originating_tsr")
If lu_string_functions.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Originating TSR is Required")
	dw_header.SetFocus()
	dw_header.SetColumn("originating_tsr")
	Return false
End If

return true
end function

on w_contract_maintenance.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_contract_maintenance.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;
iu_ws_orp3 = Create u_ws_orp3
is_inquire = ""
ib_refresh = false

wf_retrieve()
end event

event close;call super::close;IF IsValid(iu_ws_orp3) THEN
	Destroy iu_ws_orp3
END IF
end event

event activate;call super::activate;iw_frame.im_menu.mf_Enable('m_delete')
end event

type dw_detail from u_base_dw_ext within w_contract_maintenance
integer x = 50
integer y = 484
integer width = 5175
integer height = 1060
integer taborder = 20
string dataobject = "d_contract_maint_dtl"
boolean vscrollbar = true
boolean border = false
boolean ib_updateable = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_priceUom, ldwc_qtyUom

This.InsertRow(0)

This.GetChild('price_uom_type', ldwc_priceUom)	
ldwc_priceUom.SetTransObject(SQLCA)
ldwc_priceUom.Retrieve()

This.GetChild('contract_qty_uom', ldwc_qtyUom)	
ldwc_qtyUom.SetTransObject(SQLCA)
ldwc_qtyUom.Retrieve()

end event

event clicked;call super::clicked;
iw_frame.SetMicroHelp('Ready')

If row > 0 Then
	Choose case dwo.name
		Case 'line_id'
			This.SelectRow( row, Not(This.IsSelected( row)))
	End Choose
End If


end event

event itemchanged;call super::itemchanged;Long		ll_row

Date		ld_EndDate, ld_StartDate


If row = This.RowCount() Then
	ll_row = This.InsertRow(0)
End if

Choose Case This.GetColumnName()
	Case 'end_date' 
		// must be a Saturday
		ld_EndDate = Date(data)
		ld_StartDate = dw_detail.GetItemDate(row, 'start_date')
		
		If DayName(ld_EndDate) <> "Saturday" Then
			SetMicroHelp('End Date must be a Saturday')
		End If		
		If ld_EndDate <= ld_StartDate Then
			SetMicroHelp('End Date must be greater than Start Date')
		End If
		
	Case 'start_date'
		// must be a Sunday
		ld_StartDate = Date(data)
		ld_EndDate = dw_detail.GetItemDate(row, 'end_date')
		
		If DayName(ld_StartDate) <> "Sunday" Then
			SetMicroHelp('Start Date must be a Sunday')
		End If
		If ld_StartDate >= ld_EndDate Then
			SetMicroHelp("Start Date must be less than End Date")
		End If
		
End Choose

end event

event doubleclicked;call super::doubleclicked;String    ls_String, ls_stringParm
Long      ll_ModifiedCount_HD, ll_ModifiedCount_DT, ll_DeletedCount
Integer   li_rtn, li_lineid
Boolean  lb_reset


If row > 0 Then
	Choose case dwo.name
		case 'sub_prod_code'
			
			li_lineid = dw_detail.GetItemNumber(row, 'line_id')
			If IsNull(li_lineid) Then
				Return 0 
			End If
			
			If dw_header.AcceptText() = -1 Then return 1
			If dw_detail.AcceptText() = -1 Then return 1
			
			ll_ModifiedCount_HD = dw_header.ModifiedCount()
			ll_ModifiedCount_DT = dw_detail.ModifiedCount()
			
			//Delete buffer on detail
			ll_DeletedCount = dw_detail.DeletedCount()
			
			// check for changes - don't open window until all pending changes are saved or discarded
			lb_reset = false
			If ll_ModifiedCount_HD + ll_ModifiedCount_DT + ll_DeletedCount > 0 Then
				li_rtn = MessageBox("Save Changes" , "Pending changes must be saved before selecting Sub Products. ~n Do you want to save changes?", Question!, YesNo!)
				If li_rtn = 1 Then
					If NOT wf_update() Then
						Return 0
					End If
				Else
					dw_header.ResetUpdate()
					dw_detail.ResetUpdate()
					lb_reset = true
				End If
			End If
					
			ls_String = trim(dw_detail.GetItemString(row, 'contract_id')) + '~t' + &
									string(dw_detail.GetItemNumber(row, 'line_id')) + '~t' + &
									trim(dw_detail.GetItemString(row, 'create_date'))
			
			OpenWithParm(w_contract_subproducts, ls_String)
			
			ls_stringparm = Message.StringParm
			If ls_stringparm = "OK" or ls_stringparm = "SAVED" or lb_reset Then
				ib_refresh = true
				is_inquire = trim(dw_header.GetItemString(1, "contract_id")) + '~t' + dw_header.GetItemString(1, "create_date") + '~r~n'
				wf_retrieve()
				ib_refresh = false
			End If
	End Choose
End If
end event

type dw_header from u_base_dw_ext within w_contract_maintenance
integer x = 471
integer y = 36
integer width = 3145
integer height = 432
integer taborder = 10
string dataobject = "d_contract_maint_hdr"
boolean border = false
boolean ib_updateable = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_contractStatus, &
						ldwc_contractType, &
						ldwc_tsr
						
u_project_functions	lu_project_functions						

This.InsertRow(0)

This.GetChild('contract_status', ldwc_contractStatus)	
ldwc_contractStatus.SetTransObject(SQLCA)
ldwc_contractStatus.Retrieve()
	
This.GetChild('contract_type', ldwc_contractType)	
ldwc_contractType.SetTransObject(SQLCA)
ldwc_contractType.Retrieve()

This.GetChild('originating_tsr', ldwc_tsr)	
lu_project_functions.nf_gettsrs(ldwc_tsr, '')
end event

event ue_keydown;call super::ue_keydown;Choose Case key
  	Case Key1!
		is_whatwastyped += '1'
	Case Key2!
		is_whatwastyped += '2'
	Case Key3!
		is_whatwastyped += '3'
	Case Key4!
		is_whatwastyped += '4'
	Case Key5!
		is_whatwastyped += '5'
	Case Key6!
		is_whatwastyped += '6'
	Case Key7!
		is_whatwastyped += '7'
	Case Key8!
		is_whatwastyped += '8'
	Case Key9!
		is_whatwastyped += '9'
	Case Key0!
		is_whatwastyped += '0'
	Case KeyA!
		is_whatwastyped += 'A'
	Case KeyB!
		is_whatwastyped += 'B'
	Case KeyC!
		is_whatwastyped += 'C'		
	Case KeyD!
		is_whatwastyped += 'D'
	Case KeyE!
		is_whatwastyped += 'E'
	Case KeyF!
		is_whatwastyped += 'F'		
	Case KeyG!
		is_whatwastyped += 'G'
	Case KeyH!
		is_whatwastyped += 'H'
	Case KeyI!
		is_whatwastyped += 'I'		
	Case KeyJ!
		is_whatwastyped += 'J'
	Case KeyK!
		is_whatwastyped += 'K'
	Case KeyL!
		is_whatwastyped += 'L'		
	Case KeyM!
		is_whatwastyped += 'M'
	Case KeyN!
		is_whatwastyped += 'N'
	Case KeyO!
		is_whatwastyped += 'O'		
	Case KeyP!
		is_whatwastyped += 'P'
	Case KeyQ!
		is_whatwastyped += 'Q'
	Case KeyR!
		is_whatwastyped += 'R'		
	Case KeyS!
		is_whatwastyped += 'S'
	Case KeyT!
		is_whatwastyped += 'T'
	Case KeyU!
		is_whatwastyped += 'U'		
	Case KeyV!
		is_whatwastyped += 'V'
	Case KeyW!
		is_whatwastyped += 'W'		
	Case KeyX!
		is_whatwastyped += 'X'
	Case KeyY!
		is_whatwastyped += 'Y'	
	Case KeyZ!
		is_whatwastyped += 'Z'			
//	Case KeySpaceBar!
//	is_whatwastyped += ' '			
END choose

end event

event editchanged;call super::editchanged;String		 ls_FindString, ls_temp

Long		ll_Row

DataWindowChild	ldwc_temp
	
IF KEYDOWN(KeyBack!) OR KEYDOWN(KeyLeftArrow!) OR KEYDOWN(KeyEnd!) OR  KEYDOWN(KeyHome!) OR  KEYDOWN( KeyRightArrow!)&
							OR KEYDOWN(Keydelete!) Then RETURN
							
if isnull(dwo) then
	Return
else
	Choose Case String(dwo.Name)
		Case 'customer_id'
				ls_FindString = "customer_id >= '"+Trim(Left(is_whatwastyped,len(is_whatwastyped)))+"'"
		Case 'originating_tsr'
				ls_FindString = "originating_tsr >= '"+Trim(Left(is_whatwastyped,len(is_whatwastyped)))+"'"				
		Case Else
			Return
	End Choose
end if	

if is_whatwastyped > '' Then
	
	If dwo.Name = 'originating_tsr' Then
		This.GetChild('originating_tsr', ldwc_temp)
	
		ll_Row = ldwc_temp.Find( ls_FindString, 1, ldwc_temp.RowCount()+1)
	
		ls_temp = ldwc_temp.Describe("Datawindow.Data")
	
		if ll_Row > 0 Then
			this.SetItem(row, "originating_tsr", ldwc_temp.GetItemString(ll_Row, "originating_tsr"))
			ldwc_temp.ScrollToRow(ll_row)
			ldwc_temp.SelectRow(0,FALSE)
			ldwc_temp.SelectRow( ll_Row, TRUE)
		End If
	End If
	
	If dwo.Name = 'customer_id' Then
		This.GetChild('customer_id', ldwc_temp)
	
		ll_Row = ldwc_temp.Find( ls_FindString, 1, ldwc_temp.RowCount()+1)
	
		ls_temp = ldwc_temp.Describe("Datawindow.Data")
	
		if ll_Row > 0 Then
			this.SetItem(row, "customer_id", ldwc_temp.GetItemString(ll_Row, "customer_id"))
			ldwc_temp.ScrollToRow(ll_row)
			ldwc_temp.SelectRow(0,FALSE)
			ldwc_temp.SelectRow( ll_Row, TRUE)
		End If
	End If
	
End If	

Return 0
end event

event itemchanged;call super::itemchanged;DatawindowChild  ldwc_temp

String 				ls_find_string


Choose Case This.GetColumnName()	
	Case 'originating_tsr'
		SetMicroHelp('')
		If data > '   ' Then 
			This.GetChild('originating_tsr', ldwc_temp)
			ls_find_string = "smancode = '" + data + "'"
			If ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount() ) <= 0 Then
				SetMicroHelp( 'Originating TSR code is Not Valid!')
				Return 1
			End If
		End If
	Case 'customer_id'
		SetMicroHelp('')
		If data > '   ' Then 
			This.GetChild('customer_id', ldwc_temp)
			ls_find_string = "customer_id = '" + data + "'"
			If ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount() ) <= 0 Then
				SetMicroHelp( 'Customer ID is Not Valid!')
				Return 1
			End If
		End If		
	Case 'contract_status'
		SetMicroHelp('')
		If trim(data) = 'E' Then 
			SetMicroHelp( 'Cannot Change Status to Expired')
			Return 1
		End If				
End Choose
end event

event itemerror;call super::itemerror;return 1
end event

event clicked;call super::clicked;iw_frame.SetMicroHelp('Ready')
end event

