﻿$PBExportHeader$w_customer_markup_inq.srw
$PBExportComments$Inquire window for Pricing Review Queue
forward
global type w_customer_markup_inq from w_base_response_ext
end type
type dw_main from u_base_dw_ext within w_customer_markup_inq
end type
end forward

global type w_customer_markup_inq from w_base_response_ext
integer width = 1129
integer height = 704
string title = "Customer Markup Inquire"
long backcolor = 12632256
dw_main dw_main
end type
global w_customer_markup_inq w_customer_markup_inq

type variables
//String is_openstring

w_base_sheet_ext		iw_parentwindow
end variables

event ue_postopen;DataWindowChild	ldwc_temp
string				ls_temp, ls_data


dw_main.GetChild("hide_plant", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

ldwc_temp.InsertRow(1)
ldwc_temp.SetItem(1, "location_code", "ALL")
ldwc_temp.SetItem(1, "location_name", "ALL")
ls_data = ldwc_temp.Describe("DataWindow.Data")

dw_main.GetChild("hide_customer", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

dw_main.GetChild("hide_product", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

iw_parentwindow.Event ue_get_data("hide_plant")
dw_main.SetItem(1,"hide_plant", Message.StringParm)

iw_parentwindow.Event ue_get_data("hide_customer")
dw_main.SetItem(1,"hide_customer", Message.StringParm)

iw_parentwindow.Event ue_get_data("hide_customer_type")
dw_main.SetItem(1,"hide_customer_type", Message.StringParm)
ls_temp = Message.StringParm


iw_parentwindow.Event ue_get_data("hide_product")
dw_main.SetItem(1,"hide_product", Message.StringParm)

dw_main.SetFocus()
dw_main.SetRow(1)
dw_main.SetColumn("hide_plant")
dw_main.SelectText(1, 3)


end event

on w_customer_markup_inq.create
int iCurrent
call super::create
this.dw_main=create dw_main
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
end on

on w_customer_markup_inq.destroy
call super::destroy
destroy(this.dw_main)
end on

event open;call super::open;iw_parentwindow = Message.PowerObjectParm
end event

event ue_base_ok;String 	ls_hide_plant, &
			ls_hide_customer, &
			ls_hide_customer_type, &
			ls_hide_product

If dw_main.AcceptText() = -1 then return

ls_hide_plant = dw_main.GetItemString(1, "hide_plant")
If isnull(ls_hide_plant) or ls_hide_plant = "" Then
	MessageBox ("Hide Plant Error", "A Hide Plant is required")
	Return
End If

ls_hide_customer = dw_main.GetItemString(1, "hide_customer")
If isnull(ls_hide_customer) or ls_hide_customer = "" Then
	MessageBox ("Hide Customer Error", "A Hide Customer is required")
	Return
End If

ls_hide_customer_type = dw_main.GetItemString(1, "hide_customer_type")

ls_hide_product = dw_main.GetItemString(1, "hide_product")
If isnull(ls_hide_product) or ls_hide_product = "" Then
	MessageBox ("Hide Product Error", "A Hide Product is required")
	Return	
End If

iw_parentwindow.Event ue_set_data("hide_plant",ls_hide_plant)
iw_parentwindow.Event ue_set_data("hide_customer",ls_hide_customer)
iw_parentwindow.Event ue_set_data("hide_customer_type",ls_hide_customer_type)
iw_parentwindow.Event ue_set_data("hide_product",ls_hide_product)

//ib_valid_return = True

CloseWithReturn(This, "OK")
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_customer_markup_inq
boolean visible = false
integer x = 1074
integer y = 292
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_customer_markup_inq
integer x = 731
integer y = 456
integer width = 338
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_customer_markup_inq
integer x = 297
integer y = 456
integer width = 329
end type

type cb_browse from w_base_response_ext`cb_browse within w_customer_markup_inq
integer taborder = 30
end type

type dw_main from u_base_dw_ext within w_customer_markup_inq
integer x = 37
integer y = 36
integer width = 1015
integer height = 380
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_customer_markup_inq"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)



end event

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
String				ls_find_string
Long					ll_rowcount

if row = 0 then return

Choose case dwo.name
	case "hide_plant"

		This.GetChild("hide_plant", ldwc_temp)
		ls_find_string = "location_code = '" + data + "'"
		ll_rowcount = ldwc_temp.Find(ls_find_string, 0 , ldwc_temp.rowcount())
		If ll_rowcount = 0 Then
			MessageBox ("Hide Plant Error", "Plant chosen is not a valid hide plant")
			This.SelectText(1, len(data))
			Return 1
		End If
		
	case "hide_customer"

		This.GetChild("hide_customer", ldwc_temp)
		ls_find_string = "customer_id = '" + data + "'"
		ll_rowcount = ldwc_temp.Find(ls_find_string, 0 , ldwc_temp.rowcount())
		If ll_rowcount = 0 Then
			MessageBox ("Hide Customer Error", "Customer chosen is not a valid hide customer")
			This.SelectText(1, len(data))
			Return 1
		End If
		
	case "hide_product"

		This.GetChild("hide_product", ldwc_temp)
		ls_find_string = "sku_product_code = '" + data + "'"
		ll_rowcount = ldwc_temp.Find(ls_find_string, 0 , ldwc_temp.rowcount())
		If ll_rowcount = 0 Then
			MessageBox ("Hide Product Error", "Product chosen is not a valid hide product")
			This.SelectText(1, len(data))
			Return 1
		End If		
		
End Choose


end event

event itemerror;call super::itemerror;Return 1
end event

