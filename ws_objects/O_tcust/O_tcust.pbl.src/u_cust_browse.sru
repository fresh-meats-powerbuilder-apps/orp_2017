﻿$PBExportHeader$u_cust_browse.sru
forward
global type u_cust_browse from nonvisualobject
end type
end forward

global type u_cust_browse from nonvisualobject autoinstantiate
end type

type variables
String 	req_customer_name
Character customer_id[7]
Boolean	cancel
end variables

on u_cust_browse.create
TriggerEvent( this, "constructor" )
end on

on u_cust_browse.destroy
TriggerEvent( this, "destructor" )
end on

