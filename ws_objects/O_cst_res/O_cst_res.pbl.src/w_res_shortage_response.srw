﻿$PBExportHeader$w_res_shortage_response.srw
forward
global type w_res_shortage_response from w_base_response_ext
end type
type dw_main from datawindow within w_res_shortage_response
end type
end forward

global type w_res_shortage_response from w_base_response_ext
int Width=1093
int Height=661
boolean TitleBar=true
string Title="Product Shortage Response"
long BackColor=12632256
dw_main dw_main
end type
global w_res_shortage_response w_res_shortage_response

event open;call super::open;dw_main.InsertRow(0)
dw_main.TriggerEvent("ue_optionchanged")
end event

on w_res_shortage_response.create
int iCurrent
call w_base_response_ext::create
this.dw_main=create dw_main
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_main
end on

on w_res_shortage_response.destroy
call w_base_response_ext::destroy
destroy(this.dw_main)
end on

event ue_base_ok;call super::ue_base_ok;
CloseWithReturn(This, dw_main.GetItemString(1, "option"))

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "ABORT")

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_res_shortage_response
int X=691
int Y=421
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_res_shortage_response
int X=394
int Y=421
int TabOrder=50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_res_shortage_response
int X=97
int Y=421
end type

type cb_browse from w_base_response_ext`cb_browse within w_res_shortage_response
int TabOrder=30
end type

type dw_main from datawindow within w_res_shortage_response
event ue_lbuttondown pbm_lbuttondown
event ue_lbuttonup pbm_lbuttonup
event ue_optionchanged pbm_custom23
int X=5
int Y=9
int Width=1066
int Height=353
int TabOrder=10
string DataObject="d_res_shortage_response"
boolean Border=false
boolean LiveScroll=true
end type

on ue_lbuttondown;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE UPPER(ls_ObjectName)
CASE	"OK", "CANCEL"
	This.Modify(ls_ObjectName + ".Border='5'")
	This.PostEvent("ue_" + ls_ObjectName)
END CHOOSE
end on

on ue_lbuttonup;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE ls_ObjectName
CASE	"Ok", "Cancel"
	This.Modify(ls_ObjectName + ".Border='6'")
END CHOOSE
end on

event ue_optionchanged;CHOOSE CASE	This.GetItemString(1, "option")
CASE "S"
	iw_Frame.SetMicroHelp("Allow scheduling to Handle shortages.")
CASE "A"
	iw_Frame.SetMicroHelp("Apply scheduled quantity to ordered quantity.")
END CHOOSE

end event

event itemchanged;IF This.GetColumnName() = 'option' THEN
	This.PostEvent("ue_optionchanged")
END IF
end event

