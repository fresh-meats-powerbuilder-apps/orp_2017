﻿$PBExportHeader$w_split_into.srw
$PBExportComments$Used to split a detail line
forward
global type w_split_into from Window
end type
type dw_split_into from datawindow within w_split_into
end type
type cb_2 from commandbutton within w_split_into
end type
type cb_1 from commandbutton within w_split_into
end type
end forward

global type w_split_into from Window
int X=682
int Y=273
int Width=837
int Height=565
boolean TitleBar=true
string Title="Split Line"
long BackColor=12632256
WindowType WindowType=response!
dw_split_into dw_split_into
cb_2 cb_2
cb_1 cb_1
end type
global w_split_into w_split_into

on w_split_into.create
this.dw_split_into=create dw_split_into
this.cb_2=create cb_2
this.cb_1=create cb_1
this.Control[]={ this.dw_split_into,&
this.cb_2,&
this.cb_1}
end on

on w_split_into.destroy
destroy(this.dw_split_into)
destroy(this.cb_2)
destroy(this.cb_1)
end on

type dw_split_into from datawindow within w_split_into
int X=23
int Y=17
int Width=737
int Height=253
int TabOrder=10
string DataObject="d_split_into"
BorderStyle BorderStyle=StyleLowered!
boolean LiveScroll=true
end type

on constructor;This.InsertRow(0)
end on

type cb_2 from commandbutton within w_split_into
int X=426
int Y=313
int Width=247
int Height=109
int TabOrder=30
string Text="&Cancel"
boolean Cancel=true
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;																											/*
-------------------------------------------------------------------------------	
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
						$$													$$
						$$			Declare Local Variables			$$
						$$													$$			
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$								*/

	STRING	ls_ReturnData
																											/*
-------------------------------------------------------------------------------	
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
						$$													$$
						$$					Abort & Return				$$
						$$													$$			
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$								*/

	ls_ReturnData = "ABORT"
	CLOSEWITHRETURN(Parent, ls_ReturnData)
end on

type cb_1 from commandbutton within w_split_into
int X=92
int Y=317
int Width=247
int Height=109
int TabOrder=20
string Text="&OK"
boolean Default=true
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;																											/*
-------------------------------------------------------------------------------	
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
						$$													$$
						$$			Declare Local Variables			$$
						$$													$$			
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$								*/

	STRING	ls_ReturnData
																											/*
-------------------------------------------------------------------------------	
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
						$$													$$
						$$				Get Data & Return				$$
						$$													$$			
						$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$								*/

	dw_split_into.AcceptText()
	ls_ReturnData = dw_split_into.Describe("DataWindow.Data")
	CLOSEWITHRETURN(Parent, ls_ReturnData)
end on

