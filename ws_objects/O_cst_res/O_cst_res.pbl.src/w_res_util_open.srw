﻿$PBExportHeader$w_res_util_open.srw
$PBExportComments$Inquire resonse window for res_util Report window-- Looks like a report
forward
global type w_res_util_open from Window
end type
type cb_cancel from commandbutton within w_res_util_open
end type
type cb_ok from commandbutton within w_res_util_open
end type
type dw_1 from u_base_dw_ext within w_res_util_open
end type
end forward

global type w_res_util_open from Window
int X=41
int Y=268
int Width=1385
int Height=784
boolean TitleBar=true
string Title="Inquiry"
long BackColor=12632256
WindowType WindowType=response!
cb_cancel cb_cancel
cb_ok cb_ok
dw_1 dw_1
end type
global w_res_util_open w_res_util_open

type variables
STRING 		is_window, &
		is_other_info

end variables

on open;Window	lw_ParentWindow



lw_ParentWindow = Message.PowerObjectParm
If Not IsValid(lw_ParentWindow) Then
	Close(This)
	return
End if

This.Title = lw_ParentWindow.Title + " Inquire"
end on

on w_res_util_open.create
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.dw_1=create dw_1
this.Control[]={this.cb_cancel,&
this.cb_ok,&
this.dw_1}
end on

on w_res_util_open.destroy
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.dw_1)
end on

type cb_cancel from commandbutton within w_res_util_open
int X=1102
int Y=136
int Width=247
int Height=108
int TabOrder=30
string Text="&Cancel"
boolean Cancel=true
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;CloseWithReturn(parent, "CANCEL")
end on

type cb_ok from commandbutton within w_res_util_open
int X=1102
int Y=32
int Width=247
int Height=108
int TabOrder=20
string Text="&OK"
boolean Default=true
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;BOOLEAN	lb_Flag
		 
dw_1.AcceptText()
is_other_info = dw_1.Describe("DataWindow.Data")

	lb_flag = TRUE

IF IsNull( is_other_info) Then
	is_other_info = " "
END IF 

CloseWithReturn(parent, is_other_info)
Message.StringParm = is_Window + "," + is_other_info




end event

type dw_1 from u_base_dw_ext within w_res_util_open
int X=14
int Y=32
int Width=1029
int Height=604
string DataObject="d_reservation_utilization_open"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event itemchanged;call super::itemchanged;//DataWindowChild	ldwc_dataWindowChild
//
//INT	li_loop_count,&
//		li_row_count
//
//String 	ls_columnName,&
//			ls_cust_type
//
//	ls_columnName = This.GetColumnName()
//	IF ls_columnName = 'customer_type' THEN
//		ls_cust_type = This.GetText()
//		This.GetChild( "customer_id", ldwc_DataWindowChild)
//		ldwc_DataWindowChild.Reset()
//		CHOOSE CASE ls_cust_Type
//			CASE 'C'
//			message.iw_ProjectData.wf_getcustdatabytype( ldwc_DataWindowChild, "corp_cust_id")
//			CASE 'B'
//			Message.iw_ProjectData.wf_getcustdatabytype( ldwc_DataWindowChild, "bill_to_cust_id")
//			CASE 'S'
//			Message.iw_ProjectData.wf_getcustdatabytype( ldwc_DataWindowChild, "customer_id")
//		END CHOOSE
//		li_row_count = ldwc_DataWindowChild.RowCount()
//		FOR li_loop_count = 1 to li_Row_Count
//			ldwc_dataWindowChild.SetItem( li_loop_count, "type_to_display", ls_cust_Type)
//		NEXT
//	END IF
//
end event

event ue_dwndropdown;DATAWINDOWCHILD 	ldw_dddwchild

u_project_functions	lu_project_functions

is_Window = "w_reservation_utilization"
this.DataObject = 'd_reservation_utilization_open'
this.InsertRow(0)

this.GetChild("salesperson_num", ldw_DDDWChild)
lu_project_functions.nf_gettsrs(ldw_DDDWChild, Message.is_smanlocation)

this.GetChild("sales_location", ldw_DDDWChild)
lu_project_functions.nf_getlocations(ldw_dddwchild, '')


this.SetItem(1, "sales_location", Message.is_smanlocation)
this.SetItem(1, "salesperson_num", Message.is_salesperson_code)
this.SetItem(1, "reservation_week", STRING(INT(Day(Today())/7)+1))
this.SetItem(1, "reservation_month", STRING(Today(), "mm"))
this.SetItem(1, "reservation_year", STRING(Today(), "yyyy"))
		
IF lu_project_functions.nf_issalesmgr() or lu_project_functions.nf_isschedulermgr() Then
	this.Modify( "sales_location.Protect='0'")
   this.Modify( "sales_location.BackGround.Color='16777215'")
ELSE
 	this.Modify( "sales_location.Protect='1'")
	this.Modify( "sales_location.BackGround.Color='12632256'")
END IF

end event

