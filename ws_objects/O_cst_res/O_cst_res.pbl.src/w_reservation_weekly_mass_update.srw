﻿$PBExportHeader$w_reservation_weekly_mass_update.srw
forward
global type w_reservation_weekly_mass_update from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_reservation_weekly_mass_update
end type
type dw_header from u_base_dw_ext within w_reservation_weekly_mass_update
end type
type sle_1 from u_base_singlelineedit_ext within w_reservation_weekly_mass_update
end type
end forward

global type w_reservation_weekly_mass_update from w_base_sheet_ext
integer x = 37
integer y = 172
integer width = 2958
integer height = 1360
string title = "Mass Weekly Reservation Update"
event ue_copy ( )
event ue_paste ( )
event ue_complete_order ( )
dw_detail dw_detail
dw_header dw_header
sle_1 sle_1
end type
global w_reservation_weekly_mass_update w_reservation_weekly_mass_update

type variables
Private:

s_Error		istr_s_Error

u_orp204		iu_orp204
u_ws_orp4       iu_ws_orp4 

String		is_OpenParm, &
		is_copy_column

Boolean		ib_JustInquired = False, &
		ib_selected = False
Integer		ii_copy_column
Long		il_RowsUpdated[]
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_setcustomer_name_city (string data)
public subroutine wf_set_head_dates (string as_dates)
public subroutine wf_select_column (integer ai_column)
public subroutine wf_deselect_all ()
public subroutine wf_paste_column (integer ai_column)
public subroutine wf_rightclick ()
public function boolean wf_update ()
end prototypes

event ue_copy;call super::ue_copy;ib_selected = True
end event

event ue_complete_order();String 	ls_header, &
			ls_detail, &
			ls_dates_info
			
Integer	li_rtn

Long		ll_current_row

If dw_header.AcceptText() = -1 Then return 
If dw_detail.AcceptText() = -1 Then return 

if not wf_update() then Return 

if dw_header.GetItemString(1,"order_status") = 'I' then
	
	ll_current_row = dw_detail.GetRow()
	OpenWithParm(w_res_shortage_response, 'S')
	if Message.StringParm =	'A' then
		dw_header.SetItem(1, "app_ind", 'A')
	else
		dw_header.SetItem(1, "app_ind", 'S')
	end if

	ls_detail = dw_detail.Describe("DataWindow.Data")
	ls_header = dw_Header.Describe("DataWindow.Data")

	li_rtn = iu_ws_orp4.nf_ORPO85FR( istr_s_error, &
												ls_header, &
												ls_detail, &
												ls_dates_info)
	dw_header.Reset()
	dw_header.ImportString(ls_header)
	dw_header.SetRedraw(True)
	
	dw_detail.Reset()
	dw_detail.ImportString(ls_detail)
	dw_detail.SetRedraw(True)
	dw_detail.ScrolltoRow(ll_current_row)
	dw_detail.ResetUpdate()	
End If

Return
end event

public function boolean wf_retrieve ();String	ls_customerorderid, &
			ls_outputstring
			
u_string_functions	lu_string_functions			
			
This.TriggerEvent(CloseQuery!)

if Message.ReturnValue = 1 Then
	Return False
end if 	
			
ls_customerorderid = dw_header.GetItemString(1, 'customer_order_id')
If lu_string_functions.nf_IsEmpty(ls_customerorderid) or &
		ls_customerorderid = '0000000' Then
	ls_outputstring = ''
Else
	ls_outputstring = ls_customerorderid  + '~t~t~t~t~t' + &
								String(dw_header.GetItemDate(1, 'month'), 'yyyy-mm-dd')
End If

OpenWithParm( w_cust_res_open, ls_outputstring)
is_openparm = Message.StringParm

IF is_openparm = "CANCEL" Then
	Close(This)
	Return FALSE
ELSE
	ib_selected = False
	is_copy_column = 'NO_COLUNM_COPY'
	This.PostEvent("ue_Query", 0, is_openparm)
	Return TRUE
END IF

end function

public function boolean wf_setcustomer_name_city (string data);String	ls_customerName, ls_CustomerCity, ls_customerstate

u_project_functions	lu_project_functions

IF lu_project_functions.nf_ValidateCustomerstate(&
										Data, dw_Header.GetItemString(1,"customer_type"),&
										ls_CustomerName,ls_CustomerCity, ls_customerstate) < 1 Then
	dw_header.Object.customer_name.Text = ''
	dw_header.Object.customer_city.Text = ''	
	dw_header.Object.customer_state.Text = ''
	Return False
ELSE
	if isnull(ls_CustomerName) then
		dw_header.Object.customer_name.Text = ''
	else
		dw_header.Object.customer_name.Text = ls_CustomerName
	end if
	if isnull(ls_CustomerCity) then
		dw_header.Object.customer_city.Text = ''
	else
		dw_header.Object.customer_city.Text = ls_CustomerCity
	end if
	if isnull(ls_customerstate) then
		dw_header.Object.customer_state.Text = ''
	else
		dw_header.Object.customer_state.Text = ls_customerstate
	end if
	iw_frame.SetMicroHelp("Ready")
END IF
Return True
end function

public subroutine wf_set_head_dates (string as_dates);String					ls_date
u_string_functions	lu_string_functions

If lu_string_functions.nf_IsEmpty(as_dates) Then as_dates = ' ~t ~t ~t ~t ~t ~t ~t ~t '

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.minus_3_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.minus_2_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.minus_1_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.auto_roll_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.current_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.plus_1_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.plus_2_date.text = ls_date

ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.plus_3_date.text = ls_date
	
//ls_date = as_dates
ls_date = lu_string_functions.nf_gettoken(as_dates, '~t')
dw_detail.object.plus_4_date.text = ls_date
end subroutine

public subroutine wf_select_column (integer ai_column);Long				ll_row, ll_rowCount		
String			ls_fields

wf_deselect_all()

ll_rowCount = dw_detail.RowCount()

For ll_row = 1 to ll_rowCount
	ls_fields = dw_detail.GetItemString( ll_row, "business_rule")
	If isNull(ls_fields) Then ls_fields = "UUUUVVVUVUUUUUVVVVVVVVVVVVVVVU"
	If Mid(ls_fields, ai_column, 1) = "V" Then
		//Do not highlight
	Else
		ls_fields = Left( ls_fields, ai_column - 1) + "S" + Mid(ls_fields, ai_column + 1)
		dw_detail.SetItem( ll_row, "business_rule", ls_fields)
	End If
Next

Choose Case ai_column
	Case 8
		dw_detail.Object.quantity_auto_roll_t.Color = 16711680
	Case 10
		dw_detail.Object.quantity_current_t.Color = 16711680
	Case 11
		dw_detail.Object.quantity_1_plus_t.Color = 16711680
	Case 12
		dw_detail.Object.quantity_2_plus_t.Color = 16711680
	Case 13
		dw_detail.Object.quantity_3_plus_t.Color = 16711680
	Case 14
		dw_detail.Object.quantity_4_plus_t.Color = 16711680
End Choose

Return
end subroutine

public subroutine wf_deselect_all ();Long						ll_row, ll_rowCount
String					ls_fields
u_string_functions	lu_string_functions


ll_rowCount = dw_detail.RowCount()

ib_selected = False
is_copy_column = 'NO_COLUNM_COPY'

For ll_row = 1 to ll_rowCount
	ls_fields = dw_detail.GetItemString( ll_row, "business_rule")
//	if IsNull(ls_fields) Then ls_fields = '     '
//	ls_fields = '     '
	ls_fields = lu_string_functions.nf_globalreplace(ls_fields,'S','U')  
	ls_fields = lu_string_functions.nf_globalreplace(ls_fields,'P','M') 
//	ls_fields = Left( ls_fields, li_column - 1) + "U" + Mid(ls_fields, li_column + 1)
	dw_detail.SetItem( ll_row, "business_rule", ls_fields)
Next

dw_detail.Object.quantity_auto_roll_t.Color = 0
dw_detail.Object.quantity_current_t.Color = 0
dw_detail.Object.quantity_1_plus_t.Color = 0
dw_detail.Object.quantity_2_plus_t.Color = 0
dw_detail.Object.quantity_3_plus_t.Color = 0
dw_detail.Object.quantity_4_plus_t.Color = 0

Return
end subroutine

public subroutine wf_paste_column (integer ai_column);Long						ll_row, ll_rowCount, ll_color
String					ls_fields
Boolean					lb_paste

ll_rowCount = dw_detail.RowCount()

///////////////////////////////////
// Need to delete commented code //
///////////////////////////////////

//ls_fields = dw_detail.GetItemString(1, "business_rule")
//If isNull(ls_fields) Then ls_fields = "UUUUVVVUVUUUUUVVVVVVVVVVVVVVVU"
//If Mid(ls_fields,ai_column,1) = 'P' Then
//	lb_paste = False
//	ll_color = 0
//Else
//	lb_paste = True
//	ll_color = 65280
//End If

For ll_row = 1 to ll_rowCount
	ls_fields = dw_detail.GetItemString( ll_row, "business_rule")
	If isNull(ls_fields) Then ls_fields = "UUUUVVVUVUUUUUVVVVVVVVVVVVVVVU"
	//If Not lb_paste Then
	//	ls_fields = Left( ls_fields, ai_column - 1) + " " + Mid(ls_fields, ai_column + 1)
	//Else
		If Mid(ls_fields, ai_column, 1) = "V" Then
			// do not highlight
		Else
			If Mid(ls_fields, ai_column, 1) = "P" Then
				// unselect
				ls_fields = Left( ls_fields, ai_column - 1) + " " + Mid(ls_fields, ai_column + 1)
				ll_color = 0
			Else
				// select
				ls_fields = Left( ls_fields, ai_column - 1) + "P" + Mid(ls_fields, ai_column + 1)
				ll_color = 65280
			End If
		End If
	//End If
	dw_detail.SetItem( ll_row, "business_rule", ls_fields)
Next

Choose Case ai_column
	Case 8
		dw_detail.Object.quantity_auto_roll_t.Color = ll_color
	Case 10
		dw_detail.Object.quantity_current_t.Color = ll_color
	Case 11
		dw_detail.Object.quantity_1_plus_t.Color = ll_color
	Case 12
		dw_detail.Object.quantity_2_plus_t.Color = ll_color
	Case 13
		dw_detail.Object.quantity_3_plus_t.Color = ll_color
	Case 14
		dw_detail.Object.quantity_4_plus_t.Color = ll_color
End Choose

Return
end subroutine

public subroutine wf_rightclick ();m_res_mass_update_popup NewMenu

NewMenu = CREATE m_res_mass_update_popup

NewMenu.m_file.m_complete.Enabled = True
NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
Destroy NewMenu
end subroutine

public function boolean wf_update ();String			ls_detail, &
					ls_header, & 
					ls_dates_info, &
					ls_rpc_detail, &
					ls_UpdateFlag, &
					ls_input_detail
					
Integer			li_rtn, &
					li_Counter
					
Long				ll_pos, &
					ll_rowcount, &
					ll_current_row, &
					ll_99th_row, &
					ll_selected_row
					
u_string_functions	lu_string_functions				
u_project_functions	lu_project_functions

If dw_header.AcceptText() = -1 Then return False
If dw_detail.AcceptText() = -1 Then return False

This.SetRedraw( False)

dw_Header.SetItem( 1, "app_ind", "U")

ls_detail = dw_detail.Describe("DataWindow.Data")
ls_header = dw_Header.Describe("DataWindow.Data")

ll_rowcount = dw_detail.RowCount()

For ll_pos = 1 to ll_rowcount
	ls_UpdateFlag = dw_detail.GetItemString( ll_pos, "update_flag")
	IF ls_UpDateFlag = 'U' OR ls_UpDateFlag = 'A'Then
		dw_detail.SelectRow(ll_pos, TRUE)
	END IF
Next

If dw_detail.GetSelectedRow(0) > 0 Then
	ll_current_row = dw_detail.GetRow()
	ls_Input_detail = lu_string_functions.nf_BuildUpdateString( dw_Detail)

	Do
		ll_99th_Row = lu_string_functions.nf_nPos( ls_Input_Detail, "~r~n",1,99)
		IF ll_99th_Row > 0 Then
			ls_RPC_Detail = Left( ls_Input_Detail, ll_99th_Row + 1)
			ls_Input_Detail = Mid( ls_Input_Detail, ll_99th_Row + 2)
		ELSE
			ls_Rpc_detail = ls_Input_Detail
			ls_Input_Detail = ''
		END IF
		
		ls_Rpc_detail = ls_Rpc_detail + "~r~n"
																		
		li_rtn = iu_ws_orp4.nf_ORPO85FR( istr_s_error, &
													ls_header, &
													ls_rpc_detail, &
													ls_dates_info)																		
	
		if len(ls_rpc_detail) > 0 then
			lu_project_functions.nf_Replace_Rows(dw_detail, ls_rpc_detail)
			if ll_99th_Row > 0 then
				For li_Counter = 1 to 99
					ll_selected_row = dw_detail.GetSelectedRow(0)
					dw_detail.SelectRow(ll_selected_row, False)
				Next
			end if
		end if
		
	Loop While Not lu_string_functions.nf_IsEmpty( ls_Input_Detail)	
	
	dw_detail.SelectRow(0,False)
	dw_detail.ResetUpdate()
	dw_header.Reset()
	dw_header.ImportString(ls_header)
	dw_detail.SetRedraw(True)
	dw_detail.ScrolltoRow(ll_current_row)
		
End If

This.SetRedraw(True)
dw_detail.Object.DataWindow.HorizontalScrollSplit = 1150
Return True 
end function

on w_reservation_weekly_mass_update.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.sle_1
end on

on w_reservation_weekly_mass_update.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
destroy(this.sle_1)
end on

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 60, this.height - dw_detail.y - 110)
//
long       ll_x = 60,  ll_y = 110

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)




// Must do because of the split horizonal bar
dw_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_detail.Object.Datawindow.HorizontalScrollPosition2 = '0'

dw_detail.Object.DataWindow.HorizontalScrollSplit = '1150'
dw_detail.Object.Datawindow.HorizontalScrollPosition2 = '1150'

end event

event close;call super::close;Destroy( iu_orp204)

DESTROY iu_ws_orp4
end event

event open;call super::open;is_openparm = Message.StringParm
is_typecode = Message.is_Smanlocation
dw_header.Enabled = False
is_copy_column = 'NO_COLUNM_COPY'

end event

event ue_postopen;call super::ue_postopen;u_project_functions	lu_project_functions

iu_orp204 = Create u_orp204
lu_project_functions.nf_set_s_error( istr_s_error)
SetMicroHelp("Ready")

wf_retrieve()

iu_ws_orp4 = CREATE u_ws_orp4

end event

event ue_query;call super::ue_query;u_string_functions lu_string_functions
u_project_functions lu_project_functions

Int		li_PageNumber, li_rtn

String	ls_OpenString,&
			ls_header,&
			ls_detail,&
			ls_dates_info,&
			ls_Saved_Header, &
			ls_Saved_dates, &
			ls_res_number, &
  			ls_salesman, &
			ls_salesloc, &
			ls_text, &
			ls_location_name

Double	ld_Task_Number

ls_OpenString = String(Message.LongParm, "address")
ls_res_number = lu_string_functions.nf_gettoken(ls_OpenSTring, '~t')
ls_header = ls_res_number + '~t~t~t~t~t~t~t~t~t~t' + 'I'
					
This.SetRedraw(False)
dw_detail.SetRedraw(False)

istr_s_error.se_event_name = "Ue_Query"
istr_s_error.se_procedure_name = "orpo85fr"

dw_Detail.Reset()
dw_header.Reset()												

li_rtn = iu_ws_orp4.nf_ORPO85FR( istr_s_error,&
															ls_header,& 
															ls_detail, &
															ls_dates_info)																	
															

wf_set_head_dates(ls_dates_info)

IF lu_string_functions.nf_IsEmpty(ls_Saved_Header) Then ls_Saved_Header = ls_header
IF lu_string_functions.nf_IsEmpty(ls_Saved_dates) Then ls_Saved_dates = ls_dates_info
li_Rtn = dw_Detail.ImportString( ls_detail)

IF Not lu_string_functions.nf_IsEmpty( ls_Saved_Header) Then
	dw_header.Reset()
	dw_Header.ImportString( ls_Saved_Header)
	This.Title = 'Mass Weekly Reservation Update - ' + &
						dw_header.GetItemString( 1, 'customer_order_id')
END IF
 
ls_text = dw_header.GetItemstring(1,"sales_person_code");

SELECT salesman.smanname, 
			salesman.smanloc
	INTO :ls_salesman, 
			:ls_salesloc
	FROM salesman  
WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';
dw_header.object.sales_person_name.Text = ls_salesman
dw_header.object.sales_location_code.Text = ls_salesloc
 
SELECT locations.location_name
  INTO :ls_location_name
  FROM locations
WHERE locations.location_code = :ls_salesloc;	
dw_header.Object.sales_location_name.Text = ls_location_name

This.wf_setcustomer_name_city ( dw_header.GetItemString(1,"customer_ID") )

dw_detail.InsertRow(0)

dw_header.AcceptText()
dw_detail.SetRedraw(True)

dw_detail.ResetUpdate()
dw_header.ResetUpdate()

This.SetRedraw(True)

end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_cancelorder')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_Menu.mf_disable( "m_copyrows")
iw_Frame.Im_Menu.mf_disable( "m_notepad")
iw_Frame.im_Menu.mf_enable( "m_completeorder")
iw_Frame.im_Menu.mf_disable("m_generatesalesorder")

end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event closequery;call super::closequery;if dw_header.GetItemString(1,"order_status") = 'I' then
	IF MessageBox( "Complete Reservation","Reservation must be sent to Scheduling.  Do you wish to complete the reservation?", StopSign!, YesNo!,1) = 1 Then
		This.TriggerEvent("ue_Complete_order")
		Return Message.ReturnValue
	ELSE
		Return 1
	END IF
END IF
end event

type dw_detail from u_base_dw_ext within w_reservation_weekly_mass_update
integer x = 5
integer y = 368
integer width = 2894
integer height = 876
integer taborder = 20
string dataobject = "d_weekly_reservation_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event constructor;call super::constructor;
This.InsertRow(0)
//This.Modify("DataWindow.Selected.Mouse = No")
ib_updateable = True
ib_NewRowOnChange = True
ib_firstcolumnonnextrow = True
//is_selection = '4'

ib_selected = False
is_copy_column = 'NO_COLUNM_COPY'
end event

event ue_copy;call super::ue_copy;If is_copy_column = 'NO_COLUNM_COPY' Then
	ib_selected = False
Else
	ib_selected = True
End If

end event

event ue_paste;// over riding ancestor script
///////////////////////////////
Long				ll_row, ll_rowCount, ll_col
Integer			li_value, li_selcol
String			ls_fields, ls_column_name, ls_copy

IF ib_selected Then
	Choose Case is_copy_column
		Case 'quantity_auto_roll_t'
		 	ls_copy = 'qty_auto_roll'
			li_selcol = 8
		Case 'quantity_current_t'
		 	ls_copy = 'quantity_current'	
			li_selcol = 10
		Case 'quantity_1_plus_t'
		 	ls_copy = 'qty_1_plus'
			li_selcol = 11
		Case 'quantity_2_plus_t'
		 	ls_copy = 'qty_2_plus'
			li_selcol = 12
		Case 'quantity_3_plus_t'
		 	ls_copy = 'qty_3_plus'
			li_selcol = 13
		Case 'quantity_4_plus_t'
		 	ls_copy = 'qty_4_plus'
			li_selcol = 14
		Case Else
			ls_copy = 'Error'
			li_selcol = 0
	END CHOOSE
	ll_rowCount = dw_detail.RowCount()
	For ll_row = 1 to ll_rowCount
		ls_fields = dw_detail.GetItemString( ll_row, "business_rule")
		If Mid(ls_fields,li_selcol,1) = 'S' Then
			For ll_col = 8 to 14
				If Mid(ls_fields,ll_col,1) = 'P' Then
					Choose Case ll_col
						Case 8
							ls_column_name = 'qty_auto_roll'
						Case 10
							ls_column_name = 'quantity_current'	 
						Case 11
							ls_column_name = 'qty_1_plus'
						Case 12
							ls_column_name = 'qty_2_plus'
						Case 13
							ls_column_name = 'qty_3_plus'
						Case 14
							ls_column_name = 'qty_4_plus'
						Case Else
							ls_column_name = 'Error'
					End Choose
					If ls_column_name = 'Error' Then
						// do nothing
					Else
						li_value = dw_detail.GetItemNumber(ll_row, ls_copy)
						dw_detail.SetItem(ll_row, ls_column_name, li_value)
						dw_detail.SetItem(ll_row, "update_flag", "U")
					End If
				End If
			Next
		End If
	Next
	wf_deselect_all()
Else
	Paste()
End IF
end event

event clicked;call super::clicked;Int				li_column
Long				ll_row, ll_rowCount		
String			ls_fields, ls_ColumnName, ls_FirstName

This.AcceptText()
iw_frame.SetMicroHelp("Ready")
ll_rowCount = This.RowCount()
ll_row = row
ls_ColumnName = dwo.Name
If Left(ls_ColumnName,9) = 'quantity_' Then
	dw_detail.AcceptText()
	If ls_ColumnName = is_copy_column Then 
		wf_deselect_all()
		Return
	End If
	Choose Case ls_ColumnName
		Case 'quantity_auto_roll_t'
		 	li_column = 8
		Case 'quantity_current_t'
		 	li_column = 10	 
		Case 'quantity_1_plus_t'
		 	li_column = 11
		Case 'quantity_2_plus_t'
		 	li_column = 12
		Case 'quantity_3_plus_t'
		 	li_column = 13
		Case 'quantity_4_plus_t'
		 	li_column = 14
		Case Else
			Return
	END CHOOSE
	IF ib_selected Then
		If ii_copy_column <> li_column Then wf_paste_column(li_column)
	Else
		If is_copy_column = ls_ColumnName Then
			wf_deselect_all()
		Else
			wf_select_column(li_column)
			is_copy_column = ls_ColumnName 
			ii_copy_column = li_column
		End If
	End If
End If
end event

event itemchanged;call super::itemchanged;Int						li_column

Long						ll_RowFound, &
							ll_error
							
String 					ls_FindExpression , &
							ls_columnname, &
							ls_business_rule	

string 	ls_ship_customer, ls_plant
			
u_string_functions 	lu_string_functions	

u_project_functions 	lu_project_functions

datawindowchild		ldw_childdw

li_column = This.GetColumn()
ls_columnname = This.GetColumnName( )

ls_business_rule = This.GetItemString( row, "business_rule")
if lu_string_functions.nf_isempty(ls_business_rule) Then This.SetItem( row, 'business_rule', 'UUUUVVVUVUUUUUVVVVVVVVVVVVVVVU')

This.SetItem( row, 'business_rule', Replace(This.GetItemString( row, 'business_rule'), &
				li_column, 1, 'M'))

If This.GetItemString(row, 'update_flag') = ' ' Then &
		This.SetItem( row, 'update_flag', 'U')
		
IF ls_ColumnName = 'product_code' Then
	ls_ship_customer	=	dw_header.GetItemString(1, "customer_id")
	ls_plant				= 	dw_detail.GetItemString(row, "plant_code")
	IF iw_frame.iu_string.nf_IsEmpty(ls_plant) THEN
		This.SetItem( row, 'age_code', lu_project_functions.nf_get_age_code(Data, ls_ship_customer, TRUE))	
	ELSE
		This.SetItem( row, 'age_code', lu_project_functions.nf_get_age_code(Data, ls_ship_customer, ls_plant))	
	END IF		
END IF		
		
if ls_columnname = "plant_code" then
	This.GetChild('plant_code', ldw_ChildDW)
	If ldw_ChildDW.RowCount() < 1 Then
		ll_error = ldw_ChildDW.SetTransObject( SQLCA)
		ll_error=ldw_ChildDW.Retrieve()
	End If
	ll_rowfound	=	ldw_ChildDW.RowCount()
	ls_FindExpression = "location_code = ~"" + Trim(data) + "~""
	ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
	If  ll_RowFound <= 0 Then 
		MessageBox('Invalid Data', data + ' is an invalid plant code')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF	
END IF		

end event

event ue_dwndropdown;call super::ue_dwndropdown;DataWindowChild	ldwc_temp

long					ll_error

String				ls_column

u_project_functions lu_project_functions

ls_column = This.GetColumnName()
Choose Case  ls_column
Case 'plant_code'
		This.GetChild('plant_code', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			ll_error = ldwc_temp.SetTransObject( SQLCA)
			ll_error=ldwc_temp.Retrieve()
		End If
End Choose
end event

event itemerror;call super::itemerror;RETURN 1
end event

event ue_setbusinessrule;call super::ue_setbusinessrule;String 		ls_BusinessRule
Long			ll_Column

If row <= This.RowCount() Then
	Choose Case name
		Case 'product_code'
		 	ll_column = 1
	END CHOOSE
	ls_businessRule =This.GetItemString(row, "business_rule")
	If IsNull(ls_BusinessRule) Then ls_BusinessRule = "UUUUVVVUVUUUUUVVVVVVVVVVVVVVVU"
	ls_BusinessRule = Left(ls_BusinessRule, ll_Column - 1) &
								+ Value + Mid(ls_BusinessRule, ll_Column + 1)
							
	This.SetItem(row,"business_rule", ls_BusinessRule)	
End If
Return 1
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

event scrollhorizontal;if pane = 2 then
	if scrollpos < 1068 then 
		this.Object.DataWindow.HorizontalScrollPosition2 = 1068
	end if
end if
end event

event resize;if this.width > 3370 then this.width = 3370
//	end if
//else
//	if this.width > 3300 then
//		this.width = 3300
//	end if
//end if

this.Object.DataWindow.HorizontalScrollPosition2 = 1068

end event

type dw_header from u_base_dw_ext within w_reservation_weekly_mass_update
integer width = 2725
integer height = 356
integer taborder = 0
string dataobject = "d_weekly_reservation_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.is_Selection = '0'
This.Object.sales_location_name.Visible = True
This.InsertRow(0)


end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_trans_mode, &
						ldwc_auto_rollover
						
u_project_functions	lu_project_functions						
						
This.GetChild('trans_mode', ldwc_trans_mode)
lu_project_functions.nf_gettutltype(ldwc_trans_mode, 'TRANMODE')
This.SetItem(1, 'trans_mode', 'T')

This.GetChild('auto_rollover_ind', ldwc_auto_rollover)
lu_project_functions.nf_gettutltype(ldwc_auto_rollover, 'RESROLL')
This.SetItem(1, 'auto_rollover_ind', 'Y')
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

type sle_1 from u_base_singlelineedit_ext within w_reservation_weekly_mass_update
integer x = 2725
integer y = 168
integer width = 78
integer height = 64
integer taborder = 10
long backcolor = 12632256
boolean border = false
borderstyle borderstyle = stylebox!
end type

event getfocus;call super::getfocus;dw_detail.SetFocus()
dw_detail.SetRow(1)
dw_detail.Setcolumn("qty_auto_roll")
dw_detail.ScrolltoRow(1)
end event

event rbuttondown;call super::rbuttondown;wf_rightclick()
end event

