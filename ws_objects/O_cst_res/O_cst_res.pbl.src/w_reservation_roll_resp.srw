﻿$PBExportHeader$w_reservation_roll_resp.srw
forward
global type w_reservation_roll_resp from w_base_response_ext
end type
type dw_roll_info from u_base_dw_ext within w_reservation_roll_resp
end type
end forward

global type w_reservation_roll_resp from w_base_response_ext
int X=1134
int Y=513
int Width=1317
int Height=509
boolean TitleBar=true
string Title="Reservation - Mass Update"
long BackColor=12632256
dw_roll_info dw_roll_info
end type
global w_reservation_roll_resp w_reservation_roll_resp

on w_reservation_roll_resp.create
int iCurrent
call w_base_response_ext::create
this.dw_roll_info=create dw_roll_info
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_roll_info
end on

on w_reservation_roll_resp.destroy
call w_base_response_ext::destroy
destroy(this.dw_roll_info)
end on

event ue_base_ok;call super::ue_base_ok;if dw_roll_info.AcceptText() = -1 then Return
CloseWithReturn (This, dw_roll_info.Describe("DataWindow.Data")+ "~x"+ cb_base_ok.ClassName())
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn (This, "~x"+ cb_base_cancel.ClassName())
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_reservation_roll_resp
int X=1002
int Y=289
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_reservation_roll_resp
int X=1002
int Y=165
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_reservation_roll_resp
int X=1002
int Y=41
end type

type dw_roll_info from u_base_dw_ext within w_reservation_roll_resp
int X=37
int Y=41
int Width=929
int Height=293
int TabOrder=2
boolean BringToTop=true
string DataObject="d_res_roll_resp"
end type

event constructor;call super::constructor;InsertRow(0)
dw_roll_info.SetItem(1, "sunset_date", Today())
dw_roll_info.SetItem(1, "roll_type", 'U')

end event

event itemchanged;call super::itemchanged;
if dwo.Name = 'number_of_weeks' then
	if data > "5" then
		MessageBox("Reservation Mass Update","Entered number of weeks is greater than 5.")
		This.SetFocus()
		This.SelectText(1, Len(data))
		Return 1
	end if
end if	
	

end event

event itemerror;call super::itemerror;Return 1
end event

