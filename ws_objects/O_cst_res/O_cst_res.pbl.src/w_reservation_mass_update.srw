﻿$PBExportHeader$w_reservation_mass_update.srw
forward
global type w_reservation_mass_update from w_base_sheet_ext
end type
type cb_2 from commandbutton within w_reservation_mass_update
end type
type cb_1 from commandbutton within w_reservation_mass_update
end type
type dw_detail from u_base_dw_ext within w_reservation_mass_update
end type
end forward

global type w_reservation_mass_update from w_base_sheet_ext
integer width = 2565
integer height = 1196
string title = "Reservation Mass Update"
cb_2 cb_2
cb_1 cb_1
dw_detail dw_detail
end type
global w_reservation_mass_update w_reservation_mass_update

type variables
Private:

s_Error		istr_s_Error

u_ws_orp4       iu_ws_orp4 


end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_edit_checks ()
end prototypes

public function boolean wf_update ();Int			li_rtn
String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output, &
				ls_inquire
				
dw_detail.AcceptText()

If NOT wf_edit_checks() Then Return False

				
u_string_functions	lu_string_functions				
	
SetPointer(HourGlass!)

istr_s_Error.se_procedure_name = "uf_orpo91gr"
istr_s_Error.se_message = Space(71)

//nf_get_s_error_values ( &
//		istr_s_Error, &
//		ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message )

dw_detail.AcceptText()
		
ls_inquire = dw_detail.GetItemString(1, "type_of_change") + "~t" 
ls_inquire += dw_detail.GetItemString(1, "reservation_number") + "~t"
ls_inquire += String(dw_detail.GetItemDate(1, "start_date"), "mm/dd/yyyy") + "~t"
ls_inquire += String(dw_detail.GetItemDate(1, "end_date"), "mm/dd/yyyy") + "~t"

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "product_code_1")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "product_code_1") + "~t" 
End If

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "product_code_2")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "product_code_2") + "~t" 
End If

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "product_code_3")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "product_code_3") + "~t" 
End If

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "product_code_4")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "product_code_4") + "~t" 
End If

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "product_code_5")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "product_code_5") + "~t" 
End If

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "all_products_ind")) Then
	ls_inquire += "N" + "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "all_products_ind") + "~t" 
End If

ls_inquire += String(dw_detail.GetItemNumber(1, "transit_days")) + "~t"

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "plant_code")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "plant_code") + "~t"
End If

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "age_code")) Then
	ls_inquire += "~t"
Else
	ls_inquire += dw_detail.GetItemString(1, "age_code") + "~t"
End If

//messagebox("Data",ls_inquire_output)

li_rtn = iu_ws_orp4.nf_ORPO21GR(istr_s_Error, &
												ls_inquire)



// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
//THIS.nf_display_message(li_rtn, istr_s_Error, ii_orp204_commhandle)

Return True


end function

public function boolean wf_edit_checks ();
u_string_functions	lu_string_functions

If lu_string_functions.nf_IsEmpty(dw_detail.GetItemString(1, "reservation_number")) or &
		dw_detail.GetItemString(1, "reservation_number") <= "'0000000" Then
	SetMicroHelp("Reservation number is required")
	Return False
End If

If dw_detail.GetItemDate(1, "start_date") > dw_detail.GetItemDate(1, "end_date") Then
	SetMicroHelp("Start Ship Date is greater than End Ship Date")
	Return False
End If


If (dw_detail.GetItemNumber(1, "transit_days") < 0) or (dw_detail.GetItemNumber(1, "transit_days") > 9) Then
	SetMicroHelp("Transit Days must be between 0 and 9")
	Return False
End If

Return True
end function

on w_reservation_mass_update.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_detail
end on

on w_reservation_mass_update.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_detail)
end on

event ue_postopen;call super::ue_postopen;u_project_functions	lu_project_functions

iu_ws_orp4 = CREATE u_ws_orp4

lu_project_functions.nf_set_s_error( istr_s_error)
SetMicroHelp("Ready")

dw_detail.SetItem(1, "type_of_change", "P")
dw_detail.SetItem(1, "start_date", Today())
dw_detail.SetItem(1, "end_date", Date("2999-12-31"))
dw_detail.SetItem(1, "transit_days", 0)
dw_detail.SetItem(1, "age_code", "A")
dw_detail.SetItem(1, "all_products_ind", "Y")

dw_detail.object.plant_code.Visible = True
dw_detail.Object.transit_days.Visible = False
dw_detail.Object.age_code.Visible = False

dw_detail.Object.product_code_1.Visible = False
dw_detail.Object.product_code_2.Visible = False
dw_detail.Object.product_code_3.Visible = False
dw_detail.Object.product_code_4.Visible = False
dw_detail.Object.product_code_5.Visible = False

dw_detail.Object.ship_plant_t.Visible = True
dw_detail.Object.product_code_1_t.Visible = False
dw_detail.Object.transit_days_t.Visible = False
dw_detail.Object.age_code_t.Visible = False






end event

type cb_2 from commandbutton within w_reservation_mass_update
integer x = 1239
integer y = 864
integer width = 343
integer height = 92
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Cancel"
end type

event clicked;Close(Parent)
end event

type cb_1 from commandbutton within w_reservation_mass_update
integer x = 608
integer y = 864
integer width = 343
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Update"
end type

event clicked;Parent.wf_update()
end event

type dw_detail from u_base_dw_ext within w_reservation_mass_update
integer x = 23
integer y = 28
integer width = 2368
integer height = 988
integer taborder = 10
string dataobject = "d_reservation_mass_update"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild("plant_code", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()



end event

event itemchanged;call super::itemchanged;String		ls_old_type_of_change

Choose Case dwo.name
	Case "type_of_change"
		Choose Case data
			Case "P" 
				This.object.plant_code.Visible = True
				This.Object.transit_days.Visible = False
				This.Object.age_code.Visible = False	
			
				This.Object.ship_plant_t.Visible = True
				This.Object.transit_days_t.Visible = False
				This.Object.age_code_t.Visible = False
				
			Case "T" 
				This.object.plant_code.Visible = False
				This.Object.transit_days.Visible = True
				This.Object.age_code.Visible = False	

				This.Object.ship_plant_t.Visible = False
				This.Object.transit_days_t.Visible = True
				This.Object.age_code_t.Visible = False	
				
			Case "A" 
				This.object.plant_code.Visible = False
				This.Object.transit_days.Visible = False
				This.Object.age_code.Visible = True	
				
				This.Object.ship_plant_t.Visible = False
				This.Object.transit_days_t.Visible = False
				This.Object.age_code_t.Visible = True				
				
			Case "R" 
				
				ls_old_type_of_change = This.GetItemString(1, "type_of_change")
				If This.GetItemString(1, "all_products_ind") = "Y" Then
					MessageBox("Input Error", "All products not allowed when Type of Change is Remove Products")
					This.SetItem(1, "type_of_change", ls_old_type_of_change)
					Return 2
				End If
				
				ls_old_type_of_change = This.GetItemString(1, "type_of_change")
				
				This.object.plant_code.Visible = False
				This.Object.transit_days.Visible = False
				This.Object.age_code.Visible = False	
				
				This.Object.ship_plant_t.Visible = False
				This.Object.transit_days_t.Visible = False
				This.Object.age_code_t.Visible = False		
				
		End Choose
		
	Case "all_products_ind"
			Choose case data 
				Case "Y"
					
					If This.GetItemString(1, "type_of_change") = "R" Then
						MessageBox("Input Error", "All products not allowed when Type of Change is Remove Products")
						
						This.Object.product_code_1_t.Visible = True
						This.SetItem(1, "product_code_1", "")
						This.SetItem(1, "product_code_2", "")
						This.SetItem(1, "product_code_3", "")
						This.SetItem(1, "product_code_4", "")
						This.SetItem(1, "product_code_5", "")
						
						This.Object.product_code_1.Visible = True
						This.Object.product_code_2.Visible = True
						This.Object.product_code_3.Visible = True
						This.Object.product_code_4.Visible = True
						This.Object.product_code_5.Visible = True		
						Return  2
						
					Else
						
						This.Object.product_code_1_t.Visible = False
						
						This.SetItem(1, "product_code_1", "")
						This.SetItem(1, "product_code_2", "")
						This.SetItem(1, "product_code_3", "")
						This.SetItem(1, "product_code_4", "")
						This.SetItem(1, "product_code_5", "")
						
						This.Object.product_code_1.Visible = False
						This.Object.product_code_2.Visible = False
						This.Object.product_code_3.Visible = False
						This.Object.product_code_4.Visible = False
						This.Object.product_code_5.Visible = False	
					End If
					
				Case "N"	
					
					This.Object.product_code_1_t.Visible = True
					
					This.SetItem(1, "product_code_1", "")
					This.SetItem(1, "product_code_2", "")
					This.SetItem(1, "product_code_3", "")
					This.SetItem(1, "product_code_4", "")
					This.SetItem(1, "product_code_5", "")
					
					This.Object.product_code_1.Visible = True
					This.Object.product_code_2.Visible = True
					This.Object.product_code_3.Visible = True
					This.Object.product_code_4.Visible = True
					This.Object.product_code_5.Visible = True
					
			End Choose
End Choose		
		
		
		

	
end event

