﻿$PBExportHeader$w_sales_order_max_pallet_defaults_inq.srw
forward
global type w_sales_order_max_pallet_defaults_inq from w_base_response
end type
type cb_browse from u_base_commandbutton_ext within w_sales_order_max_pallet_defaults_inq
end type
type dw_pallet_defaults_inq from u_base_dw_ext within w_sales_order_max_pallet_defaults_inq
end type
end forward

global type w_sales_order_max_pallet_defaults_inq from w_base_response
int Width=1317
int Height=656
boolean TitleBar=true
string Title="Build Loads - Order/Max Pallet Defaults Inquiry"
long BackColor=12632256
boolean ControlMenu=false
cb_browse cb_browse
dw_pallet_defaults_inq dw_pallet_defaults_inq
end type
global w_sales_order_max_pallet_defaults_inq w_sales_order_max_pallet_defaults_inq

type variables
w_base_sheet_ext		iw_parentwindow
String			is_openstring
Boolean	ib_valid_return
end variables

on w_sales_order_max_pallet_defaults_inq.create
int iCurrent
call super::create
this.cb_browse=create cb_browse
this.dw_pallet_defaults_inq=create dw_pallet_defaults_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_browse
this.Control[iCurrent+2]=this.dw_pallet_defaults_inq
end on

on w_sales_order_max_pallet_defaults_inq.destroy
call super::destroy
destroy(this.cb_browse)
destroy(this.dw_pallet_defaults_inq)
end on

event open;call super::open;datawindowchild			ldwc_temp

								
String						ls_data
								
iw_parentwindow = Message.PowerObjectParm
ls_data = Message.StringParm

dw_pallet_defaults_inq.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_temp)

dw_pallet_defaults_inq.GetChild('billto_id', ldwc_temp)
iw_frame.iu_project_functions.ids_billtos.ShareData(ldwc_temp)

dw_pallet_defaults_inq.GetChild('corp_id', ldwc_temp)
iw_frame.iu_project_functions.ids_corps.ShareData(ldwc_temp)

If dw_pallet_defaults_inq.ImportString(ls_data) < 1 Then 
	dw_pallet_defaults_inq.InsertRow(0)
End If
end event

event ue_base_ok;String 	ls_customer_id, &
			ls_customer_type, &
			ls_value

u_string_functions	lu_string_functions

dw_pallet_defaults_inq.AcceptText()

ls_value = dw_pallet_defaults_inq.GetItemString(1, 'customer_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('Customer Id is a required field.')
	Return
End If	

ls_value = dw_pallet_defaults_inq.GetItemString(1, 'customer_type')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('Customer Type is a required field.')
	Return
End If	

ls_customer_id = dw_pallet_defaults_inq.getitemstring(1, 'customer_id')
ls_customer_type = dw_pallet_defaults_inq.getitemstring(1, 'customer_type')

iw_parentwindow.Event ue_set_data('customer_id', ls_customer_id)
iw_parentwindow.Event ue_set_data('customer_type', ls_customer_type)

//ls_customer_id.SetFocus()

ib_valid_return = True

CloseWithReturn(This, "OK")

end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;String				ls_customer_id, &
						ls_customer_type

u_string_functions	lu_string_functions

iw_parentwindow.Event ue_get_data('customer_id')
ls_customer_id = message.stringparm
dw_pallet_defaults_inq.setitem(1, "customer_id", message.stringparm)

iw_parentwindow.Event ue_get_data('customer_type')
ls_customer_type = message.stringparm
dw_pallet_defaults_inq.setitem(1, "customer_type", ls_customer_type)

dw_pallet_defaults_inq.SetFocus()
dw_pallet_defaults_inq.setrow(1)
dw_pallet_defaults_inq.SetColumn(1)
end event

type cb_base_help from w_base_response`cb_base_help within w_sales_order_max_pallet_defaults_inq
int X=992
int Y=256
int TabOrder=30
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_sales_order_max_pallet_defaults_inq
int X=992
int Y=140
int TabOrder=40
end type

type cb_base_ok from w_base_response`cb_base_ok within w_sales_order_max_pallet_defaults_inq
int X=992
int Y=24
int TabOrder=20
end type

type cb_browse from u_base_commandbutton_ext within w_sales_order_max_pallet_defaults_inq
int X=2126
int Y=396
int TabOrder=10
boolean Visible=false
string Text="&Browse"
end type

type dw_pallet_defaults_inq from u_base_dw_ext within w_sales_order_max_pallet_defaults_inq
int X=27
int Y=24
int Width=919
int Height=516
int TabOrder=11
boolean BringToTop=true
string DataObject="d_sales_order_max_pallet_defaults_inq"
BorderStyle BorderStyle=StyleLowered!
end type

event itemchanged;call super::itemchanged;datawindowchild	ldwc_customer_id, &
						ldwc_customer_type
				
this.getchild("customer_id", ldwc_customer_id)
ldwc_customer_id.settransobject(sqlca)
ldwc_customer_id.retrieve(Message.is_smanlocation, Message.is_smanlocation)

CHOOSE Case data 
	Case  'B' 
		This.SetItem( 1, "customer_id", "")
	Case  'C'
		This.SetItem( 1, "customer_id", "")
	Case  'S'
		This.SetItem( 1, "customer_id", "")
END  CHOOSE

this.getchild("customer_type", ldwc_customer_type)
ldwc_customer_id.settransobject(sqlca)
ldwc_customer_id.retrieve(Message.is_smanlocation, Message.is_smanlocation)
end event

event constructor;call super::constructor;dw_pallet_defaults_inq.insertrow(0)

end event

