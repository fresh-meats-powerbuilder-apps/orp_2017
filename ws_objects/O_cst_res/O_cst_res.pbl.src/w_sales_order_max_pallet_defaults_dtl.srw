﻿$PBExportHeader$w_sales_order_max_pallet_defaults_dtl.srw
forward
global type w_sales_order_max_pallet_defaults_dtl from w_netwise_sheet
end type
type dw_pallet_defaults_header from u_base_dw_ext within w_sales_order_max_pallet_defaults_dtl
end type
type dw_pallet_defaults_detail from u_base_dw_ext within w_sales_order_max_pallet_defaults_dtl
end type
end forward

global type w_sales_order_max_pallet_defaults_dtl from w_netwise_sheet
integer width = 1847
integer height = 1360
string title = "Build Loads - Order/Max Pallet Default"
boolean resizable = false
long backcolor = 12632256
event ue_getcustomertype pbm_custom64
event ue_getcustomerid pbm_custom63
event ue_systemcommand pbm_syscommand
event ue_open_order_confirmation ( )
event ue_getdata ( string as_stringvalue )
event ue_pa_summary ( )
dw_pallet_defaults_header dw_pallet_defaults_header
dw_pallet_defaults_detail dw_pallet_defaults_detail
end type
global w_sales_order_max_pallet_defaults_dtl w_sales_order_max_pallet_defaults_dtl

type variables
s_error		istr_error_info

u_orp003		iu_orp003
u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1

long		il_selected_rows

String		is_MicroHelp, &
		is_customer_inq, &
		is_update_string

Boolean  		ib_modified = false, &
		ib_updating 
	
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_update_modify (long al_row, character ac_status_ind)
public subroutine wf_delete ()
end prototypes

event ue_getcustomertype;Message.StringParm = ""
end event

on ue_getcustomerid;call w_netwise_sheet::ue_getcustomerid;Message.StringParm = ''
end on

event ue_systemcommand;call super::ue_systemcommand;Window	lw_Window

IF Message.WordParm = 61504 OR Message.WordParm = 61520 Then 
	lw_Window = iw_Frame.GetFirstSheet()
	lw_Window = iw_Frame.GetNextSheet( lw_Window)
	IF IsValid( lw_Window) Then Open(w_ctrl_tab_window_switching)
	Message.processed = true
	Message.returnvalue = 0
END IF



end event

event ue_getdata;call super::ue_getdata;Choose Case as_stringValue
	Case "Title"
		Message.StringParm = This.Title
	Case "Customer_Info"
		Message.StringParm = is_customer_info
	Case "iu_orp003"
		Message.PowerObjectParm = iu_orp003
END CHoose
end event

public function boolean wf_retrieve ();String		ls_input, &
				ls_output_string
				
Integer		li_rtn


TriggerEvent("closequery")

OpenWithParm(w_sales_order_max_pallet_defaults_inq, This)
IF Message.StringParm = "Cancel" then return False

This.SetRedraw(False)

ls_input = dw_pallet_defaults_header.GetItemString( 1, "customer_id") + '~t' + &
			  dw_pallet_defaults_header.GetItemString( 1, "customer_type") + '~r~n'
			  
istr_error_info.se_function_name = "wf_retrieve"

dw_pallet_defaults_detail.Reset()

//li_rtn = iu_orp001.nf_orpo19br (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp1.nf_orpo19fr (istr_error_info, ls_input, ls_output_string)

dw_pallet_defaults_detail.ImportString(ls_output_string)
dw_pallet_defaults_detail.ResetUpdate()
il_selected_rows = 0

This.SetRedraw(True)
dw_pallet_defaults_detail.insertrow(0)
dw_pallet_defaults_detail.setrow(dw_pallet_defaults_detail.rowcount())
Return TRUE
end function

public function boolean wf_update ();String	ls_input, &
			ls_output_string_in, &
			ls_output_string, &
			ls_Updateflag, &
			ls_output_string_returned, &
			ls_MicroHelp, &
			ls_business_rules, &
			ls_division_code, &
			ls_location, &
			ls_load_type, &
			ls_trans_mode, &
			ls_type_code

Integer	li_rtn, &
			li_count, &
			li_require_temp, &
			li_max_pallet
			
Long		ll_rowcount, &
			ll_pos, &
			ll_col, &
			ll_row, &
			ll_modrows, &
			ll_delrows

double	ll_require_temp, &
			ll_max_pallet

char		lc_status_ind

Boolean	lb_error_found

u_string_functions	lu_string_functions

if dw_pallet_defaults_detail.AcceptText() = -1 then
	Return False
end if

SetPointer(HourGlass!)
This.SetRedraw(false)

is_update_string = ''

ll_modrows = dw_pallet_defaults_detail.modifiedcount()
ll_delrows = dw_pallet_defaults_detail.deletedcount()

ls_input = dw_pallet_defaults_header.GetItemString( 1, "customer_id") + '~t' + &
			   dw_pallet_defaults_header.GetItemString( 1, "customer_type") + '~r~n' 

// get all deleted records from buffer
For li_count = 1 to ll_delrows

	lc_status_ind = 'D'
	
	if not this.wf_update_modify(li_count, lc_status_ind) then
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if

Next

// get all new & modified rows from buffer
ll_row = 0

for li_count = 1 to ll_modrows
	ll_row = dw_pallet_defaults_detail.getnextmodified(ll_row, primary!)
	
	ls_division_code = dw_pallet_defaults_detail.getitemstring(ll_row, "division_code")
	if iw_frame.iu_string.nf_isempty(ls_division_code) then
		iw_frame.setmicrohelp("Division Code is a required field.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	ls_location = dw_pallet_defaults_detail.getitemstring(ll_row, "service_center_code")
	if iw_frame.iu_string.nf_isempty(ls_location) then
		iw_frame.setmicrohelp("Location Code is a required field.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	ls_load_type = dw_pallet_defaults_detail.getitemstring(ll_row, "load_type")
	if iw_frame.iu_string.nf_isempty(ls_load_type) then
		iw_frame.setmicrohelp("Load Type is a required field.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	ls_trans_mode = dw_pallet_defaults_detail.getitemstring(ll_row, "trans_mode")
	if iw_frame.iu_string.nf_isempty(ls_trans_mode) then
		iw_frame.setmicrohelp("Trans Mode is a required field.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	ls_type_code = dw_pallet_defaults_detail.getitemstring(ll_row, "type_code")
	if iw_frame.iu_string.nf_isempty(ls_type_code) then
		iw_frame.setmicrohelp("Ordered Age is a required field.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	li_require_temp = dw_pallet_defaults_detail.getitemnumber(ll_row, "required_temp")
	if isnull(li_require_temp) then
		iw_frame.setmicrohelp("Required Temp must be a number.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if
	
	li_max_pallet = dw_pallet_defaults_detail.getitemnumber(ll_row, "max_pallets")
	if isnull(li_max_pallet) then
		iw_frame.setmicrohelp("Max Pallets must be a number.")
		ib_updating = false
		This.SetRedraw(True)
		return false
	end if

	choose case dw_pallet_defaults_detail.getitemstatus(ll_row, 0, primary!)
		case newmodified!
			lc_status_ind = 'A'
		case datamodified!
			lc_status_ind = 'M'
	end choose
	
	if not this.wf_update_modify(ll_row, lc_status_ind) then
		ib_updating = false
		return false
	end if
	
next

ls_output_string_in = is_update_string
istr_error_info.se_function_name = "wf_update"


//  mainframe call
//li_rtn = iu_orp001.nf_orpo20br (istr_error_info, ls_input, ls_output_string_in)
li_rtn = iu_ws_orp1.nf_orpo20fr (istr_error_info, ls_input, ls_output_string_in)																			
if li_rtn < 0 then
	This.SetRedraw(True)
	SetPointer(Arrow!)																		
	return false
end if

dw_pallet_defaults_detail.ResetUpdate()
																		
This.SetRedraw(True)
SetPointer(Arrow!)																		
dw_pallet_defaults_detail.SelectRow(0,False)

Return True

end function

public function boolean wf_update_modify (long al_row, character ac_status_ind);dwbuffer	ldwb_buffer

if ac_status_ind = 'D' then
	ldwb_buffer = delete!
else
	ldwb_buffer = primary!
end if

is_update_string += &
	dw_pallet_defaults_detail.getitemstring (al_row, "division_code", ldwb_buffer, false) + "~t" + &
	dw_pallet_defaults_detail.getitemstring (al_row, "service_center_code", ldwb_buffer, false) + "~t" + &
	dw_pallet_defaults_detail.getitemstring (al_row, "load_type", ldwb_buffer, false) + "~t" + &
	dw_pallet_defaults_detail.getitemstring (al_row, "trans_mode", ldwb_buffer, false) + "~t" + &
	dw_pallet_defaults_detail.getitemstring (al_row, "type_code", ldwb_buffer, false) + "~t" + &
	string(dw_pallet_defaults_detail.getitemnumber (al_row, "required_temp", ldwb_buffer, false)) + "~t" + &
	string(dw_pallet_defaults_detail.getitemnumber (al_row, "max_pallets", ldwb_buffer, false)) + "~t" + &
	ac_status_ind + "~r~n"
	
return true
end function

public subroutine wf_delete ();long	ll_deleted_row

ll_deleted_row = dw_pallet_defaults_detail.getselectedrow(0)


DO WHILE ll_deleted_row > 0
	dw_pallet_defaults_detail.deleterow(ll_deleted_row)
	ll_deleted_row = dw_pallet_defaults_detail.getselectedrow(ll_deleted_row)
LOOP


end subroutine

on w_sales_order_max_pallet_defaults_dtl.create
int iCurrent
call super::create
this.dw_pallet_defaults_header=create dw_pallet_defaults_header
this.dw_pallet_defaults_detail=create dw_pallet_defaults_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pallet_defaults_header
this.Control[iCurrent+2]=this.dw_pallet_defaults_detail
end on

on w_sales_order_max_pallet_defaults_dtl.destroy
call super::destroy
destroy(this.dw_pallet_defaults_header)
destroy(this.dw_pallet_defaults_detail)
end on

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return
end event

event ue_query;call super::ue_query;IF wf_Retrieve() = false Then
		This.SetRedraw( TRUE)
		CLOSE(THIS)
		RETURN
END IF
end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")

end event

event activate;call super::activate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_delete")
iw_frame.im_menu.mf_disable("m_cancelorder")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.Im_Menu.mf_disable("m_notepad")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")

end event

event deactivate;iw_frame.im_menu.mf_enable("m_save")
iw_frame.im_menu.mf_enable("m_cancelorder")
iw_frame.im_Menu.mf_enable("m_copyrows")
iw_Frame.Im_Menu.mf_enable("m_notepad")
iw_Frame.im_Menu.mf_enable("m_complete")
iw_Frame.im_Menu.mf_enable("m_generatesales")

end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
destroy iu_ws_orp1

end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp1 = Create u_ws_orp1

end event

event ue_set_data;choose case as_data_item
	case "customer_id"
		dw_pallet_defaults_header.setitem(1, 'customer_id', as_value)
	case "customer_type"
		dw_pallet_defaults_header.setitem(1, 'customer_type', as_value)
end choose
end event

event ue_get_data;Choose Case as_value
	Case 'customer_id'
		message.StringParm = dw_pallet_defaults_header.getitemstring (1, "customer_id")
	Case 'customer_type'
		message.stringparm = dw_pallet_defaults_header.getitemstring (1, "customer_type")
End choose

end event

type dw_pallet_defaults_header from u_base_dw_ext within w_sales_order_max_pallet_defaults_dtl
integer y = 24
integer width = 745
integer height = 384
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sales_order_max_pallet_defaults_header"
boolean border = false
end type

event constructor;call super::constructor;dw_pallet_defaults_header.InsertRow(0)
end event

type dw_pallet_defaults_detail from u_base_dw_ext within w_sales_order_max_pallet_defaults_dtl
integer y = 380
integer width = 1797
integer height = 876
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_sales_order_max_pallet_defaults_detail"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild	ldwc_division_code, &
						ldwc_trans_mode, &
						ldwc_service_center_code, &
						ldwc_type_code


is_selection = '2'
ib_updateable = true

dw_pallet_defaults_detail.InsertRow(0)

This.GetChild("division_code", ldwc_division_code)
ldwc_division_code.SetTransObject(SQLCA)
ldwc_division_code.Retrieve("DIVCODE")
//ldwc_division_code.Sort(Ascending!, 1, 1)


this.getchild("trans_mode", ldwc_trans_mode)
ldwc_trans_mode.settransobject(sqlca)
ldwc_trans_mode.retrieve("trans_mode")

this.getchild("service_center_code", ldwc_service_center_code)
ldwc_service_center_code.settransobject(sqlca)
ldwc_service_center_code.retrieve("service_center_code")

this.getchild("type_code", ldwc_type_code)
ldwc_type_code.settransobject(sqlca)
ldwc_type_code.retrieve("type_code")

ib_NewRowOnChange = True
ib_firstcolumnonnextrow = True
end event

event itemchanged;call super::itemchanged;datawindowchild	ldwc_division_code, &
						ldwc_locations

string				ls_temp

long					ll_rowfound, &
						ll_rowcount

Choose Case dwo.name

	case "division_code"
		if iw_frame.iu_string.nf_isempty(data) then
			iw_frame.setmicrohelp("Division Code is a required field.")
			return 1
		end if
		
		This.GetChild('division_code', ldwc_division_code)
		ls_temp = "type_code = ~"" + Trim(data) + "~""
		ll_RowFound = ldwc_division_code.Find(ls_temp, 0, ldwc_division_code.RowCount())
		ll_rowcount = ldwc_division_code.rowcount()
		If  ll_RowFound <= 0 Then 
			MessageBox('Invalid Data', data + ' is an invalid Division Code')
			This.SelectText(1, Len(data))
			RETURN 1
		END IF	
		
	case "service_center_code"
		if iw_frame.iu_string.nf_isempty(data) then
			iw_frame.setmicrohelp("Location Code is a required field.")
			return 1
		end if
		
		This.GetChild('service_center_code', ldwc_locations)
		ls_temp = "location_code = ~"" + Trim(data) + "~""
		ll_RowFound = ldwc_locations.Find(ls_temp, 0, ldwc_locations.RowCount())
		If  ll_RowFound <= 0 Then 
			MessageBox('Invalid Data', data + ' is an invalid Location Code')
			This.SelectText(1, Len(data))
			RETURN 1
		END IF	

	case "load_type"
		if iw_frame.iu_string.nf_isempty(data) then
			iw_frame.setmicrohelp("Load Type is a required field.")
			return 1
		end if
		
	case "trans_mode"
		if iw_frame.iu_string.nf_isempty(data) then
			iw_frame.setmicrohelp("Trans Mode is a required field.")
			return 1
		end if
		
	case "type_code"
		if iw_frame.iu_string.nf_isempty(data) then
			iw_frame.setmicrohelp("Order Age is a required field.")
			return 1
		end if
	
	Case "require_temp" 
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Required Temp must be a number")
			Return 1
		End If
	
	Case "max_pallets"
		if not isnumber(data) then
			iw_frame.setmicrohelp("Quantity must be a number")
			return 1
		end if

end choose



end event

event itemerror;call super::itemerror;return 1
end event

