﻿$PBExportHeader$w_products_on_res_by_tsr_resp.srw
forward
global type w_products_on_res_by_tsr_resp from w_base_response_ext
end type
type dw_data from u_base_dw_ext within w_products_on_res_by_tsr_resp
end type
end forward

global type w_products_on_res_by_tsr_resp from w_base_response_ext
integer x = 699
integer y = 600
integer width = 2766
integer height = 756
string title = "Products on Reservation by TSR Inquire"
long backcolor = 12632256
dw_data dw_data
end type
global w_products_on_res_by_tsr_resp w_products_on_res_by_tsr_resp

type variables
String	is_open_string
end variables

on w_products_on_res_by_tsr_resp.create
int iCurrent
call super::create
this.dw_data=create dw_data
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_data
end on

on w_products_on_res_by_tsr_resp.destroy
call super::destroy
destroy(this.dw_data)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;call super::ue_postopen;dw_data.SetFocus()

u_string_functions	lu_string_functions

datawindowchild			ldwc_temp

String						ls_data

is_open_string = Message.StringParm
//MessageBox("ls_open_string",is_open_string)

If NOT lu_string_functions.nf_isempty(is_open_string) Then 
	dw_data.Reset()
	dw_data.ImportString(is_open_string)
	
	If dw_data.GetItemString(1,"all_cust") = 'Y' Then
		dw_data.object.customer_id.Protect = 1 
		dw_data.object.customer_id.BackGround.Color = 12632256
		dw_data.SetItem(1,"customer_id", "ALL")
		dw_data.SetITem(1,"customer_name","ALL")
	Else
		dw_data.SetItem(1,"all_cust","N")	
		dw_data.object.customer_id.Protect = 0 
		dw_data.object.customer_id.BackGround.Color = 16777215
	End If

End If
end event

event ue_base_ok;call super::ue_base_ok;String	ls_input_string

dw_data.AcceptText()

ls_input_string = dw_data.Describe("DataWindow.Data")

CloseWithReturn(This, ls_input_string)
end event

event open;call super::open;is_open_string = Message.StringParm
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_products_on_res_by_tsr_resp
integer x = 2281
integer y = 928
integer taborder = 0
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_products_on_res_by_tsr_resp
integer x = 2464
integer y = 156
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_products_on_res_by_tsr_resp
integer x = 2464
integer y = 36
end type

type cb_browse from w_base_response_ext`cb_browse within w_products_on_res_by_tsr_resp
integer taborder = 40
end type

type dw_data from u_base_dw_ext within w_products_on_res_by_tsr_resp
integer width = 2414
integer height = 632
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_products_on_res_by_tsr_inq"
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

String					ls_location

u_string_functions		lu_string_functions

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)

This.GetChild( "salesperson",ldwc_temp) 
ls_location = message.is_smanlocation

iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, '')

This.SetItem(1,"week","A")
This.SetITem(1,"gpo_date", Today())


end event

event itemchanged;call super::itemchanged;Int			li_row_count, &
				li_loop_count, &
				li_column

String		ls_temp, &
				ls_salesloc, &
				ls_salesman, &
				ls_text, &
				ls_location_name,&
				ls_CustomerName,&
				ls_CustomerCity, &
				ls_customer_state

long			ll_count, ll_rowcount, ll_count2, ll_linenumber
				
u_project_functions	lu_project_functions				
				
li_column = This.GetColumn()
ls_text = data

If IsNull(ls_text) or (ls_text = '') Then Return

Choose Case This.GetColumnName()
Case 'customer_id'
	IF lu_project_functions.nf_validatecustomerstate( Data, 'S',&
				ls_CustomerName,ls_CustomerCity, ls_customer_state) < 1 Then
			This.SetItem(1, "customer_name", '')
		Return 1
	Else
		If isnull(ls_CustomerName) then
			This.SetItem(1, "customer_name", '')
		else
			This.SetItem(1, "customer_name", ls_CustomerName)
		end if
		
	END IF
	
CASE 'salesperson'
	SELECT salesman.smanname, 
			salesman.smanloc
		INTO :ls_salesman, 
				:ls_salesloc
		FROM salesman  
	WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';

	This.SetItem(1, "salesperson_name", ls_salesman)
	
CASE 'all_cust'	
		If ls_text = 'Y' Then
			This.object.customer_id.Protect = 1 
			This.object.customer_id.BackGround.Color = 12632256
			This.SetItem(1,"customer_id", "ALL")
			This.SetITem(1,"customer_name","ALL")
		Else
			This.object.customer_id.Protect = 0 
			This.object.customer_id.BackGround.Color = 16777215
			This.SetItem(1,"customer_id", "")
			This.SetITem(1,"customer_name","")

		End If
	
End Choose

Return



end event

