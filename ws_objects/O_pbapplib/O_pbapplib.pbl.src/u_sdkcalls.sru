﻿$PBExportHeader$u_sdkcalls.sru
forward
global type u_sdkcalls from nonvisualobject
end type
type s_choosecolor from structure within u_sdkcalls
end type
type s_memorystatus from structure within u_sdkcalls
end type
type os_filedatetime from structure within u_sdkcalls
end type
type os_systemtime from structure within u_sdkcalls
end type
type os_finddata from structure within u_sdkcalls
end type
end forward

type s_choosecolor from structure
	unsignedlong		lstructsize
	unsignedinteger		hwndowner
	unsignedinteger		hinstance
	long		rgbresult
	string		lpcustcolors
	unsignedlong		flags
	long		lcustdata
	unsignedinteger		lpfnhook
	string		lptemplatename
end type

type s_memorystatus from structure
	long		dwlength
	long		dwmemoryload
	long		dwtotalphys
	long		dwavailphys
	long		dwtotalpagefile
	long		dwavailpagefile
	long		dwtotalvirtual
	long		dwavailvirtual
end type

type os_filedatetime from structure
	unsignedlong		ul_lowdatetime
	unsignedlong		ul_highdatetime
end type

type os_systemtime from structure
	unsignedinteger		ui_wyear
	unsignedinteger		ui_wmonth
	unsignedinteger		ui_wdayofweek
	unsignedinteger		ui_wday
	unsignedinteger		ui_whour
	unsignedinteger		ui_wminute
	unsignedinteger		ui_wsecond
	unsignedinteger		ui_wmilliseconds
end type

type os_finddata from structure
	unsignedlong		ul_fileattributes
	os_filedatetime		str_creationtime
	os_filedatetime		str_lastaccesstime
	os_filedatetime		str_lastwritetime
	unsignedlong		ul_filesizehigh
	unsignedlong		ul_filesizelow
	unsignedlong		ul_reserved0
	unsignedlong		ul_reserved1
	character		ch_filename[260]
	character		ch_alternatefilename[14]
end type

global type u_sdkcalls from nonvisualobject
end type
global u_sdkcalls u_sdkcalls

type prototypes
Public:
function int 	ChooseColor(ref s_choosecolor lstr_color) 			LIBRARY "commdlg.dll" alias for "ChooseColor;Ansi"
function int 	ChooseColorA(ref s_choosecolor lstr_color) 			LIBRARY "comdlg32" alias for "ChooseColorA;Ansi"
Function Uint 	SetFocus( Uint WindowHandle) 								LIBRARY "User.Exe"
Function Uint 	FindWindow( String Classname, String WindowTitle) 	LIBRARY "User.Exe" alias for "FindWindow;Ansi"
Function int 	EnableHardwareInput(Boolean lb_val) 					LIBRARY "user.exe"
Function Uint 	SetActiveWindow( Uint ActiveWindowHandle) 			LIBRARY "User.exe"
Function Uint 	SetFocusA( Uint WindowHandle) 							LIBRARY "User32"
Function Uint 	FindWindowA( String Classname, String WindowTitle) LIBRARY "User32" alias for "FindWindowA;Ansi"
Function int 	EnableHardwareInputA(Boolean lb_val) 					LIBRARY "user32"
Function Uint 	SetActiveWindowA( Uint ActiveWindowHandle) 			LIBRARY "User32"
function uint 	GetDriveTypeA(String ls_RootDir) 						LIBRARY "kernel32" alias for "GetDriveTypeA;Ansi"
function uint 	GetFreeSystemResources(uint SysResource) 				LIBRARY 'user.dll'
function uint 	GetDriveType(int DriveNumber) 							LIBRARY "kernel.exe"
function int 	GetModuleFileName(int hinst, ref string filename, int namesize) 	LIBRARY "kernel.exe" alias for "GetModuleFileName;Ansi"
function uint 	GetWindowsDirectory(ref string WindowsDir, int StringSize) 			LIBRARY "kernel.exe" alias for "GetWindowsDirectory;Ansi"
function int 	GetModuleFileNameA(int hinst, ref string filename, int namesize) 	LIBRARY "kernel32" alias for "GetModuleFileNameA;Ansi"
function uint 	GetWindowsDirectoryA(ref string WindowsDir, int StringSize) 		LIBRARY "kernel32" alias for "GetWindowsDirectoryA;Ansi"
subroutine 		GlobalMemoryStatus( ref s_MemoryStatus as_MemoryStatus) 				LIBRARY "kernel32" alias for "GlobalMemoryStatus;Ansi"
function long 	GetProfileStringA ( string as_section, string as_key, &
						string as_default, ref string as_value, long al_size) 			LIBRARY "KERNEL32" alias for "GetProfileStringA;Ansi"

// Win32 calls for file date and time
Function boolean 	GetFileTime(long hFile, ref os_filedatetime  lpCreationTime, &
							ref os_filedatetime  lpLastAccessTime, &
							ref os_filedatetime  lpLastWriteTime  )  							LIBRARY "KERNEL32.DLL" alias for "GetFileTime;Ansi"
Function boolean 	FileTimeToSystemTime(ref os_filedatetime lpFileTime, &
							ref os_systemtime lpSystemTime) 										LIBRARY "KERNEL32.DLL" alias for "FileTimeToSystemTime;Ansi"
Function boolean 	FileTimeToLocalFileTime(ref os_filedatetime lpFileTime, &
							ref os_filedatetime lpLocalFileTime) 								LIBRARY "KERNEL32.DLL" alias for "FileTimeToLocalFileTime;Ansi"
Function boolean 	SetFileTime(ulong hFile, os_filedatetime  lpCreationTime, &
							os_filedatetime  lpLastAccessTime, &
							os_filedatetime  lpLastWriteTime  )  								LIBRARY "KERNEL32.DLL" alias for "SetFileTime;Ansi"
Function boolean 	SystemTimeToFileTime(os_systemtime lpSystemTime, &
							ref os_filedatetime lpFileTime) 										LIBRARY "KERNEL32.DLL" alias for "SystemTimeToFileTime;Ansi"
Function boolean 	LocalFileTimeToFileTime(ref os_filedatetime lpLocalFileTime, &
							ref os_filedatetime lpFileTime) 										LIBRARY "KERNEL32.DLL" alias for "LocalFileTimeToFileTime;Ansi"
Function long 		FindFirstFileA (ref string filename, &
							ref os_finddata findfiledata) 										LIBRARY "KERNEL32.DLL" alias for "FindFirstFileA;Ansi"
Function boolean 	FindNextFileA (long handle, ref os_finddata findfiledata) 		LIBRARY "KERNEL32.DLL" alias for "FindNextFileA;Ansi"
Function boolean 	FindClose (long handle) 													LIBRARY "KERNEL32.DLL"
Function boolean 	ExitWindowsEx(Uint Uflags, Double dwreserved  )  					LIBRARY "User32"

Private:
// pass 13 for selected color, 14 for selected text color
// pass 23 fot tooltip text color, 24 for tooltip background color
Function Long GetSysColor(Int index) LIBRARY "user32.dll"
end prototypes

type variables
environment	ie_env
end variables

forward prototypes
public function integer nf_mouse_enabled (boolean ab_switch)
public function boolean nf_setactiveapp (string app_name, string as_ini)
public function integer nf_getfreeresourcepercent ()
public function String nf_getmodulefilename ()
public function integer nf_choose_color (ref long arg_color)
public function integer nf_convertfiledatetimetopb (os_filedatetime astr_filetime, ref string as_filedate, ref string as_filetime)
public function integer nf_getcreationdatetime (string as_filename, ref string as_date, ref string as_time)
public subroutine nf_exitwindows ()
public subroutine nf_get_defaultprinter (ref string as_printer)
public function string nf_getwindowsdirectory ()
public function long nf_getsyscolor (integer ai_option)
public function integer nf_getuserinipath (ref string as_inifile, ref string as_working_dir)
end prototypes

public function integer nf_mouse_enabled (boolean ab_switch);Int li_ret_val

// This function disables the mouse and keyboard use with caution
IF Ab_Switch Then
   SetPointer( Arrow!)
ELSE
	SetPointer( HourGlass!)
END IF
IF ie_Env.OSMajorRevision = 4 Or ie_Env.OsType = WindowsNT! THEN
	li_ret_val =  EnableHardwareInputA( ab_switch)
ELSE
	li_ret_val =  EnableHardwareInput( ab_switch)
END IF	
Return li_ret_val

end function

public function boolean nf_setactiveapp (string app_name, string as_ini);Uint	lu_WindowHandle
String ls_ClassName,&
	  ls_WindowTitle

Return TRUE


SetNull( ls_classname)
ls_WindowTitle = Trim(ProfileString ( gw_base_frame.is_userini, message.nf_Get_App_ID(), "FrameWindowTitle", ""))




IF ie_Env.OSMajorRevision = 4 Or ie_Env.OsType = WindowsNT! THEN
	lu_WindowHandle  = FindWindowA( ls_classname, ls_WindowTitle)
	IF IsNull( lu_WindowHandle) Then Return False
	IF SetActiveWindowA( lu_WindowHandle) = lu_WindowHandle Then Return FALSE
	IF IsNUll( SetFocusA( lu_WindowHandle)) Then Return False
ELSE
	lu_WindowHandle  = FindWindow( ls_classname, ls_WindowTitle)
	IF IsNull( lu_WindowHandle) Then Return False
	IF SetActiveWindow( lu_WindowHandle) = lu_WindowHandle Then Return FALSE
	IF IsNUll( SetFocus( lu_WindowHandle)) Then Return False
END IF	
Return TRUE
end function

public function integer nf_getfreeresourcepercent ();s_MemoryStatus		lstr_MemoryStatus

If ie_env.win16 Then
	return GetFreeSystemResources(0)
Else
	GlobalMemoryStatus(lstr_MemoryStatus)
	Return lstr_MemoryStatus.dwMemoryLoad
End if
end function

public function String nf_getmodulefilename ();String	ls_FilePath


ls_filepath = Space(64)
If ie_env.win16 Then
	GetModuleFileName(Handle(GetApplication()), ls_filepath, 64)
Else
	GetModuleFileNameA(Handle(GetApplication()), ls_filepath, 64)
End if

return ls_FilePath
end function

public function integer nf_choose_color (ref long arg_color);s_ChooseColor	lstr_color

Int				li_null
					
String			ls_cust_colors




If ie_Env.OSMajorRevision = 4 Or ie_Env.OsType = WindowsNT! Then
	SetNull(li_null)

	ls_cust_colors = Space(64)

	lstr_color.hwndOwner 	= li_null
	lstr_color.hInstance 	= 0
	lstr_color.rgbResult 	= 0
	lstr_color.lpCustColors = ls_cust_colors
	lstr_color.Flags 			= 0
	lstr_color.lCustData 	= 0
	lstr_color.lpfnHook		= 0
	lstr_color.lpTemplateName = ls_cust_colors
	lstr_color.lStructSize 	= 36

	ChooseColorA(lstr_color)
	
	arg_color = lstr_color.rgbResult

	return 0
Else
	SetNull(li_null)

	ls_cust_colors = Space(64)

	lstr_color.lStructSize 	= 32
	lstr_color.hwndOwner 	= li_null
	lstr_color.hInstance 	= 0
	lstr_color.rgbResult 	= 0
	lstr_color.lpCustColors = ls_cust_colors
	lstr_color.Flags 			= 0
	lstr_color.lCustData 	= 0
	lstr_color.lpfnHook		= 0
	lstr_color.lpTemplateName = ls_cust_colors
	
	ChooseColor(lstr_color)
 
	arg_color = lstr_color.rgbResult

	return 0
End if



end function

public function integer nf_convertfiledatetimetopb (os_filedatetime astr_filetime, ref string as_filedate, ref string as_filetime);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  of_ConvertFileDatetimeToPB
//
//	Access:  protected
//
//	Arguments:
//	astr_FileTime				The os_filedatetime structure containg the 
//									system date/time for the file.
//	ad_FileDate				The file date in PowerBuilder Date format,
//									passed by reference.
//	at_FileTime				The file time in PowerBuilder Time format,
//									passed by reference.
//
//	Returns:		Integer
//					1 if successful, -1 if an error occurrs.
//
//	Description:	Convert a sytem file type to PowerBuilder Date and Time.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright © 1996 Powersoft Corporation.  All Rights Reserved.
//	Any distribution of the PowerBuilder Foundation Classes (PFC)
//	source code by other than Powersoft is prohibited.
//
//////////////////////////////////////////////////////////////////////////////

os_filedatetime		lstr_LocalTime
os_systemtime		lstr_SystemTime

If Not FileTimeToLocalFileTime(astr_FileTime, lstr_LocalTime) Then Return -1
//
If Not FileTimeToSystemTime(lstr_LocalTime, lstr_SystemTime) Then Return -1

as_filedate = String(lstr_SystemTime.ui_WMonth) + "/" + &
				String(lstr_SystemTime.ui_WDay) + "/" + &
				String(lstr_SystemTime.ui_wyear)

as_filetime = String(lstr_SystemTime.ui_wHour) + ":" + &
				String(lstr_SystemTime.ui_wMinute) + ":" + &
				String(lstr_SystemTime.ui_wSecond) + ":" + &
				String(lstr_SystemTime.ui_wMilliseconds)
Return 1

end function

public function integer nf_getcreationdatetime (string as_filename, ref string as_date, ref string as_time);//////////////////////////////////////////////////////////////////////////////
//
//	Function:  nf_GetCreationDatetime
//
//	Access:  public
//
//	Arguments:
//	as_FileName				The name of the file for which you want its date
//									and time; an absolute path may be specified or it
//									will be relative to the current working directory
//	ad_Date						The date the file was created, passed by reference.
//	at_Time						The time the file was created, passed by reference.
//
//	Returns:		Integer
//					1 if successful, -1 if an error occurrs.
//
//	Description:	Get the date and time a file was created.
//
//////////////////////////////////////////////////////////////////////////////
//
//	Revision History
//
//	Version
//	5.0   Initial version
//
//////////////////////////////////////////////////////////////////////////////
//
//	Copyright © 1996 Powersoft Corporation.  All Rights Reserved.
//	Any distribution of the PowerBuilder Foundation Classes (PFC)
//	source code by other than Powersoft is prohibited.
//
//////////////////////////////////////////////////////////////////////////////

Long						ll_Handle
os_finddata	lstr_FindData

// Get the file information
ll_Handle = FindFirstFileA(as_FileName, lstr_FindData)
If ll_Handle <= 0 Then Return -1
FindClose(ll_Handle)

// Convert the date and time
Return nf_ConvertFileDatetimeToPB(lstr_FindData.str_CreationTime, as_Date, as_Time)

end function

public subroutine nf_exitwindows ();int	li_rtn

li_rtn	=	MessageBox('Exit Windows', 'Due to the registry changes,' +&
'~r~n' + 'You have to reboot the system, Continue', STOPSIGN!, YESNO!, 1)
IF li_rtn	=	1 THEN
	SetProfileString(gw_base_frame.is_userini, 'pbdnload', 'DataSource', 'Installed')
//This will reboot the machine
	ExitWindowsEx(2,0)
ELSE
	HALT CLOSE
END IF
end subroutine

public subroutine nf_get_defaultprinter (ref string as_printer);as_printer = Space(255)

GetProfileStringA("windows", "device", ",,,", as_printer, Len(as_printer))



end subroutine

public function string nf_getwindowsdirectory ();Boolean	lb_Win32

String	ls_windows_dir


Choose Case ie_env.OSType 
	Case WindowsNT! 
		lb_Win32 = True

	Case Windows!
		// In 32 bit exe's, version for win95 is 4.0
		If ie_env.OSMajorRevision = 4 Then
			lb_Win32 = True
		End if
End Choose

ls_windows_dir = Space(64)
If lb_Win32 Then
	GetWindowsDirectoryA(ls_windows_dir, 64)
Else
	GetWindowsDirectory(ls_windows_dir, 64)
End if	

//MessageBox ( 'u_sdkcalls -- nf_getwindowsdirectory' , "ls_working_dir = API GetWindowsDirectory" + "~r~n" + &
//				ls_windows_dir  )


RETURN ls_windows_dir


end function

public function long nf_getsyscolor (integer ai_option);// This is from winuser.h

//#define COLOR_SCROLLBAR         0
//#define COLOR_BACKGROUND        1
//#define COLOR_ACTIVECAPTION     2
//#define COLOR_INACTIVECAPTION   3
//#define COLOR_MENU              4
//#define COLOR_WINDOW            5
//#define COLOR_WINDOWFRAME       6
//#define COLOR_MENUTEXT          7
//#define COLOR_WINDOWTEXT        8
//#define COLOR_CAPTIONTEXT       9
//#define COLOR_ACTIVEBORDER      10
//#define COLOR_INACTIVEBORDER    11
//#define COLOR_APPWORKSPACE      12
//#define COLOR_HIGHLIGHT         13
//#define COLOR_HIGHLIGHTTEXT     14
//#define COLOR_BTNFACE           15
//#define COLOR_BTNSHADOW         16
//#define COLOR_GRAYTEXT          17
//#define COLOR_BTNTEXT           18
//#define COLOR_INACTIVECAPTIONTEXT 19
//#define COLOR_BTNHIGHLIGHT      20
//
//#if(WINVER >= 0x0400)
//#define COLOR_3DDKSHADOW        21
//#define COLOR_3DLIGHT           22
//#define COLOR_INFOTEXT          23
//#define COLOR_INFOBK            24
//
//#define COLOR_DESKTOP           COLOR_BACKGROUND
//#define COLOR_3DFACE            COLOR_BTNFACE
//#define COLOR_3DSHADOW          COLOR_BTNSHADOW
//#define COLOR_3DHIGHLIGHT       COLOR_BTNHIGHLIGHT
//#define COLOR_3DHILIGHT         COLOR_BTNHIGHLIGHT
//#define COLOR_BTNHILIGHT        COLOR_BTNHIGHLIGHT
//

Return GetSysColor(ai_option)
end function

public function integer nf_getuserinipath (ref string as_inifile, ref string as_working_dir);// as_inifile already holds the filename.  This will tack on the 
// absolute path to the front of the string

Boolean	lb_Win32

Int		li_drivetype, &
			li_counter, &
			li_length, &
			li_file

String	ls_windows_dir, &
			ls_filepath

Choose Case ie_env.OSType 
	Case WindowsNT! 
		lb_Win32 = True

	Case Windows!
		// In 32 bit exe's, version for win95 is 4.0
		If ie_env.OSMajorRevision = 4 Then
			lb_Win32 = True
		End if
End Choose

ls_filepath = Space(64)
If lb_Win32 Then
	GetModuleFileNameA(Handle(GetApplication()), ls_filepath, 64)
Else
	GetModuleFileName(Handle(GetApplication()), ls_filepath, 64)
End if
//MessageBox ( 'u_sdkcalls -- nf_getuserinipath' , "ls_filepath = API GetModuleFileName" + "~r~n" + &
//			ls_filepath  )

//changed for consistancy
li_length = Len(ls_filepath)
For li_counter = li_length to 1 step -1
	if Mid(ls_filepath, li_counter, 1) = "\" then
		exit
	End if
Next
as_working_dir = Left(ls_filepath, li_counter)

ls_windows_dir = Space(64)
If lb_Win32 Then
	GetWindowsDirectoryA(ls_windows_dir, 64)
Else
	GetWindowsDirectory(ls_windows_dir, 64)
End if	
as_inifile = ls_windows_dir + "\" + as_inifile 

//MessageBox ( 'u_sdkcalls -- nf_getuserinipath' , "as_working_dir = API GetWindowsDirectory" + "~r~n" + &
//				as_working_dir  )

// validate files existence
//If Not FileExists(as_inifile) Then
//	li_file = FileOpen(as_inifile, LineMode!, Write!)
//	FileWrite(li_file, " ")
//	FileClose(li_file)
//End if
Return 0




end function

on u_sdkcalls.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_sdkcalls.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;GetEnvironment(ie_Env)
end event

