﻿$PBExportHeader$w_db_error.srw
forward
global type w_db_error from Window
end type
type cb_3 from commandbutton within w_db_error
end type
type cb_2 from commandbutton within w_db_error
end type
type cb_1 from commandbutton within w_db_error
end type
type mle_err_msg from multilineedit within w_db_error
end type
end forward

global type w_db_error from Window
int X=676
int Y=269
int Width=1846
int Height=1149
boolean TitleBar=true
string Title="Database Error"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
mle_err_msg mle_err_msg
end type
global w_db_error w_db_error

on open;mle_err_msg.text = Message.StringParm
end on

on w_db_error.create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.mle_err_msg=create mle_err_msg
this.Control[]={ this.cb_3,&
this.cb_2,&
this.cb_1,&
this.mle_err_msg}
end on

on w_db_error.destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.mle_err_msg)
end on

type cb_3 from commandbutton within w_db_error
int X=1540
int Y=340
int Width=246
int Height=109
int TabOrder=40
string Text="&Close"
boolean Cancel=true
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;// Confirm with the user that they really want to exit
//If MessageBox("Close", "Do you really want to close the window?", &
//		Question!, YesNo!, 1) = 1 Then Close(Parent)

Close(Parent)
		
end on

type cb_2 from commandbutton within w_db_error
int X=1540
int Y=177
int Width=246
int Height=109
int TabOrder=30
string Text="&File"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;string		ls_filepath, ls_filename, ls_log_header, ls_log_trailer
integer		li_return, li_file_nbr
writemode	le_writemode = Replace!


ls_log_header = &
"**************************************************************************~r~n" + &
"*** Start of transaction *************************************************~r~n" + &
"**************************************************************************~r~n"

ls_log_trailer = &
"**************************************************************************~r~n" + &
"*** End of transaction ***************************************************~r~n" + &
"**************************************************************************~r~n~r~n"


// Prompt the user for the file name that they want to save the log file to.
li_return = GetFileSaveName("Save Log File As", ls_filepath, ls_filename, &
			"TXT", "TextFiles (*.TXT),*.TXT, All Files (*.*), *.*")

// Call a FUNCky or SDK function to check if the file already exits
/* Uncomment this section to have the ability to check if file exists
	If isFile(ls_filepath, 6) Then
		Choose Case MessageBox("File Already Exists", &
							"Overwrite - 'Yes'~r~n~r~nAppend - 'No'", Question!, YesNoCancel!)
			Case 1 
				le_writemode = Replace!
			Case 2
				le_writemode = Append!
			Case 3
				Return
		End Choose
	Else
		le_writemode = Replace!
	End If
*/

// Open the file for writing
li_file_nbr = FileOpen(ls_filepath, LineMode! ,Write! ,Shared!, le_writemode)

// Write the log information out to the log file
li_return = FileWrite(li_file_nbr, ls_log_header + "~r~n" + mle_err_msg.text + "~r~n" + ls_log_trailer)

// Check to see if there was any error writing to the file
If li_return = -1 Then
	MessageBox("Write Error", "There was a problem "+&
							"writing to " + ls_filename + ".", StopSign!)
Else
	FileClose(li_file_nbr)
	Close(Parent)
End If
end on

type cb_1 from commandbutton within w_db_error
int X=1540
int Y=49
int Width=246
int Height=109
int TabOrder=20
string Text="&Print"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;MessageBox("Print", "This will be printing....")
end on

type mle_err_msg from multilineedit within w_db_error
int X=54
int Y=36
int Width=1398
int Height=967
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean AutoVScroll=true
boolean DisplayOnly=true
long TextColor=33554432
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

