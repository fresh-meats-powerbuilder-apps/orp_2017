﻿$PBExportHeader$w_calendar.srw
forward
global type w_calendar from Window
end type
type cb_cancel from commandbutton within w_calendar
end type
type cb_next from commandbutton within w_calendar
end type
type cb_previous from commandbutton within w_calendar
end type
type dw_calendar from datawindow within w_calendar
end type
end forward

global type w_calendar from Window
int X=672
int Y=264
int Width=759
int Height=880
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
cb_cancel cb_cancel
cb_next cb_next
cb_previous cb_previous
dw_calendar dw_calendar
end type
global w_calendar w_calendar

type variables
Integer ii_old_column, ii_Day, ii_Month, ii_Year
String is_DateFormat
Date id_DateSelected

//this variable is set to force the execution of 
//wf_draw_month() before the window is closed
boolean	ib_readytoclose

end variables

forward prototypes
public function int wf_get_month_number (string month)
public subroutine wf_init_calendar (date ad_start_date)
public function integer wf_days_in_month (integer ai_month, integer ai_year)
public function string wf_get_month_string (integer ai_month)
public subroutine wf_enter_day_numbers (integer ai_start_day_num, integer ai_days_in_month)
public subroutine wf_prevmonth ()
public subroutine wf_nextmonth ()
public subroutine wf_draw_month (integer ai_year, integer ai_month)
end prototypes

public function int wf_get_month_number (string month);Integer li_month_number

CHOOSE CASE month
	CASE "Jan"
		li_month_number = 1
	CASE "Feb"
		li_month_number = 2
	CASE "Mar"
		li_month_number = 3
	CASE "Apr"
		li_month_number = 4
	CASE "May"
		li_month_number = 5
	CASE "Jun"
		li_month_number = 6
	CASE "Jul"
		li_month_number = 7
	CASE "Aug"
		li_month_number = 8
	CASE "Sep"
		li_month_number = 9
	CASE "Oct"
		li_month_number = 10
	CASE "Nov"
		li_month_number = 11
	CASE "Dec"
		li_month_number = 12
END CHOOSE

return li_month_number
end function

public subroutine wf_init_calendar (date ad_start_date);integer	li_FirstDayNum, li_Cell, li_DaysInMonth
string	ls_Month, ls_Modify, ls_Return
date		ld_FirstDay


// Insert a row into the script datawindow
InsertRow(dw_calendar, 0)

// Set the variables for Day, Month and Year passed start date
ii_Month =  Month(ad_start_date)
ii_Year  =  Year (ad_start_date)
ii_Day   =  Day  (ad_start_date)

// Find how many days in the relevant month
li_DaysInMonth = wf_days_in_month(ii_Month, ii_Year)

// Find the date of the first day of this month
ld_FirstDay = Date(ii_Year, ii_Month,1)

// What day of the week is the first day of the month
li_FirstDayNum = DayNumber(ld_FirstDay)

// Set the starting "cell" in the datawindow. i.e the column in which
// the first day of the month will be displayed
li_Cell = li_FirstDayNum + ii_Day - 1

// Set the Title of the calendar with the Month and Year
ls_Month = wf_get_month_string(ii_Month) + " " + string(ii_Year)
Modify(dw_calendar, "st_month.text=~"" + ls_Month + "~"")

// Enter the days into the datawindow
wf_enter_day_numbers(li_FirstDayNum, li_DaysInMonth)


SetItem(dw_calendar, 1, li_Cell, String(Day(ad_start_date)))

// Display the current day in bold (or 3D)
// ls_Modify = "#" + String(li_Cell) + ".font.weight=~"700~""
ls_Modify = "#" + String(li_Cell) + ".border=5"
ls_Return = Modify(dw_calendar, ls_Modify)
If ls_Return <> "" then MessageBox("Modify Error", ls_Return)

// Set the instance variable i_old_column to hold the current cell, so
// when we change it, we know waht the old column was
ii_old_column = li_Cell

end subroutine

public function integer wf_days_in_month (integer ai_month, integer ai_year);// Most cases are straight forward in that there are a fixed number of 
// days in 11 of the 12 months.  February is, of course, the problem.
// In a leap year February has 29 days, otherwise 28.

integer	li_DaysInMonth, li_month
boolean	lb_LeapYear

li_month	=	ai_month
CHOOSE CASE li_month
	CASE 1, 3, 5, 7, 8, 10, 12
		li_DaysInMonth = 31
	CASE 4, 6, 9, 11
		li_DaysInMonth = 30
	CASE 2
			IF Mod(ai_year,4) <> 0	Then 
				lb_LeapYear = False
			Elseif Mod(ai_Year,400) = 0 Then
				lb_LeapYear = True
			Elseif Mod(Ai_Year,100) = 0 Then
				lb_LeapYear = False	
			ELse
				lb_LeapYear = True
			END IF
				
			If lb_LeapYear Then
				li_DaysInMonth = 29
			Else
				li_DaysInMonth = 28
			End If

END CHOOSE

// Return the number of days in the passed month
Return li_DaysInMonth
end function

public function string wf_get_month_string (integer ai_month);String ls_Month

CHOOSE CASE ai_month
	CASE 1
		ls_Month = "January"
	CASE 2
		ls_Month = "February"
	CASE 3
		ls_Month = "March"
	CASE 4
		ls_Month = "April"
	CASE 5
		ls_Month = "May"
	CASE 6
		ls_Month = "June"
	CASE 7
		ls_Month = "July"
	CASE 8
		ls_Month = "August"
	CASE 9
		ls_Month = "September"
	CASE 10
		ls_Month = "October"
	CASE 11
		ls_Month = "November"
	CASE 12
		ls_Month = "December"
END CHOOSE

Return ls_Month
end function

public subroutine wf_enter_day_numbers (integer ai_start_day_num, integer ai_days_in_month);// This function populates the datawindow with the correct days
// for the current month


integer	li_Count, li_DayCount
string	ls_modify, ls_Return


// Blank the columns before the first day of the month
For li_Count = 1 to ai_start_day_num
	SetItem(dw_calendar, 1, li_Count, "")
Next

// Set the columns for the days to the String of their Day number
For li_Count = 1 to ai_days_in_month

	// Use nDayCount to find which column needs to be set
	li_DayCount = ai_start_day_num + li_Count - 1
	SetItem(dw_calendar, 1, li_DayCount, String(li_Count))

Next

// Move to next column
li_DayCount = li_DayCount + 1

// Blank remainder of columns
For li_Count = li_DayCount to 42
	SetItem(dw_calendar, 1, li_Count, "")
Next


// If there was an old column turn off the highlight
If ii_old_column <> 0 then
	ls_Modify = "#" + string(ii_old_Column) + ".font.weight=~"400~""
	ls_Modify = "#" + string(ii_old_Column) + ".border=0"
	ls_Return = Modify(dw_calendar, ls_Modify)
	If ls_Return <> "" Then MessageBox("Modify", ls_Return)
End If


ii_old_column = 0

end subroutine

public subroutine wf_prevmonth ();// Decrement the month, if 0, set to 12 (December)

ii_Month = ii_Month - 1

If ii_Month = 0 Then
	ii_Month = 12
	ii_Year = ii_Year - 1
End If
end subroutine

public subroutine wf_nextmonth ();//Increment the month number, but if its 13, set back to 1 (January)
ii_Month = ii_Month + 1
If ii_Month = 13 then
	ii_Month = 1
	ii_Year = ii_Year + 1
End If
end subroutine

public subroutine wf_draw_month (integer ai_year, integer ai_month);integer	li_FirstDayNum, li_Cell, li_DaysInMonth, li_day
date		ld_FirstDay
string	ls_Month,ls_Modify, ls_Return

// Set Pointer to an Hourglass and turn off redrawing of Calendar
SetPointer(Hourglass!)
SetRedraw(dw_calendar, FALSE)

// Set Instance variables to arguments
ii_Month = ai_month
ii_Year = ai_year

// Work out how many days in the month
li_DaysInMonth = wf_days_in_month(ii_Month, ii_Year)

// Find the date of the first day in the month
ld_FirstDay = Date(ii_Year, ii_Month, 1)

// Find what day of the week this is
li_FirstDayNum = DayNumber(ld_FirstDay)

// Set the first cell
li_Cell = li_FirstDayNum + ii_Day - 1

// If the day is greater than the number of days set the day to the last day of the month.
If ii_day > li_DaysInMonth Then 
	li_cell = li_DaysInMonth + li_firstDayNum - 1
	li_day = li_DaysInMonth
Else
	li_day = ii_day
End If

// If there was an old column turn off the highlight
If ii_old_column <> 0 Then
	ls_Modify = "#" + string(ii_old_Column) + ".font.weight=~"400~""
	ls_Modify = "#" + string(ii_old_Column) + ".border=0"
	ls_Return = Modify(dw_calendar, ls_Modify)
	If ls_Return <> "" then MessageBox("Modify",ls_Return)
End If

//Set the Title
ls_Month = wf_get_month_string(ii_Month) + " " + string(ii_Year)
Modify(dw_calendar, "st_month.text=~"" + ls_Month + "~"")

//Enter the day numbers into the datawindow
wf_enter_day_numbers(li_FirstDayNum,li_DaysInMonth)

//????
SetItem(dw_calendar, 1, li_Cell, String(li_Day))

//Highlight the current date
ls_Modify = "#" + string(li_Cell) + ".font.weight=~"700~""
ls_Modify = "#" + string(li_Cell) + ".border=5"
ls_Return = Modify(dw_calendar, ls_Modify)
If ls_Return <> "" then MessageBox("Modify",ls_Return)

//Set the old column for next time
ii_old_column = li_Cell

//Reset the pointer and Redraw
SetPointer(Arrow!)
SetRedraw(dw_calendar, TRUE)


end subroutine

event key;GraphicObject	which_control

CommandButton 	cb_which

string 			ls_text

IF Keydown(KeyEnter!) then
	//this variable is set to force the execution of wf_draw_month()
	//before the window is closed
	ib_readytoclose	=	True
	which_control = GetFocus( )
	IF TypeOf(Which_control) = CommandButton! THEN
		cb_which = which_control
		ls_text = Trim(cb_which.text) 
		IF Upper(ls_text) = "CANCEL" THEN
			CloseWithReturn(this, "")
		ELSE
			CloseWithReturn(this, String(Date(ii_Year,ii_Month,ii_Day), &
														is_DateFormat))
		END IF
	END IF
ELSEIF Keydown(keyescape!) then
				CloseWithReturn(this, "")
END IF

If keydown(keyleftArrow!) then
	ii_day = ii_day - 1
	IF ii_day = 0 THEN
		wf_prevmonth()
		ii_day = wf_days_in_month(ii_month, ii_year)
	END IF
		wf_draw_month(ii_year,ii_month)
ELSEIF keydown(keyRightArrow!) then
	ii_day = ii_day + 1
	IF ii_day > wf_days_in_month(ii_month, ii_year) THEN
		wf_nextmonth()
		ii_day = 1
	END IF
		wf_draw_month(ii_year,ii_month)
ELSEIF keydown(keyDownArrow!) then
	ii_day = ii_day + 7
	IF ii_day > wf_days_in_month(ii_month, ii_year) THEN
		ii_day = ii_day - wf_days_in_month(ii_month, ii_year) 
		wf_nextmonth()
	END IF
		wf_draw_month(ii_year,ii_month)
ELSEIF keydown(keyUPArrow!) then
	ii_day = ii_day - 7
	IF ii_day <= 0 THEN
		wf_prevmonth()
		ii_day = wf_days_in_month(ii_month, ii_year) + ii_day
	END IF
		wf_draw_month(ii_year,ii_month)
END IF




end event

event open;str_parms	lstr_parms
lstr_parms = Message.PowerObjectParm

// Move this window so that it will come up exactly
// where the user clicked to open this window
Move(lstr_parms.integer_arg[1] - 125, lstr_parms.integer_arg[2])

// Call a function that will initialize the calendar
CHOOSE CASE DaysAfter(Today(), lstr_parms.Date_arg[1])
CASE -18000 to 18000
	wf_init_calendar(lstr_parms.date_arg[1])
CASE ELSE
	wf_init_calendar(Today())
END CHOOSE


//Timer(.1)
end event

on timer;// If the cursor move beyond this window, close the window
// and return and empty string in the Message object

If PointerX() < 0 OR PointerY() < 0 Then CloseWithReturn(This, "")

If PointerX() > width OR PointerY() > height Then CloseWithReturn(This, "")


end on

on w_calendar.create
this.cb_cancel=create cb_cancel
this.cb_next=create cb_next
this.cb_previous=create cb_previous
this.dw_calendar=create dw_calendar
this.Control[]={this.cb_cancel,&
this.cb_next,&
this.cb_previous,&
this.dw_calendar}
end on

on w_calendar.destroy
destroy(this.cb_cancel)
destroy(this.cb_next)
destroy(this.cb_previous)
destroy(this.dw_calendar)
end on

event closequery;IF ib_readytoclose THEN 
	wf_draw_month(ii_year,ii_month)
END IF
end event

type cb_cancel from commandbutton within w_calendar
int X=9
int Y=744
int Width=731
int Height=96
int TabOrder=40
string Text="Cancel"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;CloseWithReturn(Parent, "")
end on

type cb_next from commandbutton within w_calendar
int X=393
int Y=8
int Width=347
int Height=96
int TabOrder=20
string Text="Next>>"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;//Increment the month number, but if its 13, set back to 1 (January)
ii_Month = ii_Month + 1
If ii_Month = 13 then
	ii_Month = 1
	ii_Year = ii_Year + 1
End If

//Draw the month
wf_draw_month ( ii_Year, ii_Month )
end event

type cb_previous from commandbutton within w_calendar
int X=9
int Y=8
int Width=366
int Height=96
int TabOrder=10
string Text="<<Previous"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;// Decrement the month, if 0, set to 12 (December)

ii_Month = ii_Month - 1

If ii_Month = 0 Then
	ii_Month = 12
	ii_Year = ii_Year - 1
End If

// Draw the month
wf_draw_month ( ii_Year, ii_Month )
end on

type dw_calendar from datawindow within w_calendar
int X=9
int Y=112
int Width=731
int Height=624
int TabOrder=30
string DataObject="d_calendar"
boolean LiveScroll=true
end type

event doubleclicked;long		ll_clickedcol
string	ls_Modify, &
			ls_Return


IF dwo.type	=	'column' THEN ll_ClickedCol = long(dwo.id)
If ll_ClickedCol = 0 Then Return

// If there is an old column, reset the border
If ii_old_column <> 0 then
	ls_Modify = "#" + string(ll_ClickedCol) + ".font.weight=~"400~""
	ls_Return = Modify(ls_Modify)
	If ls_Return <> "" then MessageBox("Modify",ls_Return)
End If





end event

event clicked;long		ll_ClickedColumn
string	ls_Modify, ls_Return, ls_day


// Find which column was clicked on and return if it is not valid
IF dwo.type =	'column' THEN ll_ClickedColumn = Long(dwo.id)
If ll_ClickedColumn = 0 Then Return

// Set the local variable ls_day to the text of the clicked column
ls_Day = GetItemString(1, ll_ClickedColumn)
If ls_Day = "" then Return

SetItem(1, ll_ClickedColumn, ls_Day)

// Highlight the chosen day
ls_Modify = "#" + string(ll_ClickedColumn) + ".font.weight=~"700~""
ls_Modify = "#" + string(ll_ClickedColumn) + ".border=5"
ls_Return = Modify(ls_Modify)
If ls_Return <> "" Then MessageBox("Modify",ls_Return)

// If the highlight was on a previous column (i_old_column = 0)
// set the border of the old column back to normal
If ii_old_column <> 0 then
ls_Modify = "#" + string(ii_old_Column) + ".font.weight=~"400~""
	ls_Modify = "#" + string(ii_old_Column) + ".border=0"
	ls_Return = Modify(ls_Modify)
	If ls_Return <> "" then MessageBox("Modify",ls_Return)
End If

// Set the Day to the chosen column
ii_Day = Integer(ls_Day)

// Set the old column variable for next time
ii_old_column = ll_ClickedColumn

// Return the chosen date to the calling datawindow via the Message Object
//Message.StringParm = String(Date(ii_Year,ii_Month,ii_Day), is_DateFormat)
CloseWithReturn(Parent, String(Date(ii_Year,ii_Month,ii_Day), is_DateFormat))
end event

