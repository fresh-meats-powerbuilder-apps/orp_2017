﻿$PBExportHeader$w_base_open.srw
forward
global type w_base_open from w_base_response
end type
type dw_main from u_base_dw within w_base_open
end type
type tab_1 from tab within w_base_open
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tab_1 from tab within w_base_open
tabpage_1 tabpage_1
end type
end forward

global type w_base_open from w_base_response
int Width=1646
int Height=816
boolean TitleBar=true
string Title="Open Window"
long BackColor=79741120
dw_main dw_main
tab_1 tab_1
end type
global w_base_open w_base_open

forward prototypes
public subroutine wf_deny_access ()
end prototypes

public subroutine wf_deny_access ();// This is a stub function to handle security
end subroutine

event ue_base_ok;Boolean lb_flag
String  ls_window_name
Long	ll_Current_Row
Window	lw_temp

If dw_main.RowCount() < 1 Then return
ll_current_Row = dw_main.GetRow()
IF ll_current_Row = 0 then ll_current_Row =1
ls_window_name  = 	Trim(dw_main.GetItemString( ll_Current_Row,  "window_name"))
Close(This)

OpenSheetWithParm(lw_temp,'', ls_window_name, gw_base_frame, 0, gw_base_frame.im_base_menu.iao_arrangeOpen)

end event

event ue_base_cancel;Close(This)
end event

on w_base_open.create
int iCurrent
call super::create
this.dw_main=create dw_main
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
this.Control[iCurrent+2]=this.tab_1
end on

on w_base_open.destroy
call super::destroy
destroy(this.dw_main)
destroy(this.tab_1)
end on

event ue_postopen;String	ls_DefaultButton, &
			ls_Find

Int		li_ret

Long		ll_row, &
			ll_PageCount, &
			ll_Counter

Window	lw_ActiveSheet


gw_base_frame.SetMicroHelp("Wait..Checking security")
ll_PageCount = UpperBound(tab_1.Control[])

//Message.ib_netwise_error = FALSE

wf_deny_access()
lw_ActiveSheet = gw_base_frame.GetActiveSheet()
If IsValid(lw_ActiveSheet) Then
	/*****************************************************************
	**
	** If there is a current sheet, we want the open box to have the 
	** last used button down by default.  'ls_find' is a find statement that
	** searches lds_main to find the button that the window belongs to.  
	** In lds_main, parameters are tacked to the window_name column, not a 
	** separate column, so to find a match, we need to chop off parameters 
	** if they exist.
	**
	******************************************************************/

	ls_find = "Trim( Lower( Left( window_name, Pos( window_name, ',') - " + &
				 "If( Pos( window_name, ',') > 0, 1, 0 - Len( Trim( window_name) ) ) ) ) ) = '" +&
					Lower(Trim(ClassName(lw_ActiveSheet))) + "'"
	ll_row = dw_main.Find( ls_Find, 1, dw_main.RowCount() )
	If ll_row > 0 Then 
		ls_DefaultButton = dw_main.GetItemString(ll_row, 'window_type')
	Else
		If ll_PageCount > 0 Then ls_DefaultButton = tab_1.Control[1].Text
	End if
Else
	If ll_PageCount > 0 Then ls_DefaultButton = tab_1.Control[1].Text
End if


For ll_Counter = 1 to ll_PageCount
	If Lower(ls_DefaultButton) = tab_1.Control[ll_Counter].Text Then
			tab_1.SelectTab(ll_Counter)
	End if
Next

dw_Main.SetRedraw(True)
gw_base_Frame.SetMicroHelp("Ready")
dw_Main.SetFocus()
//cb_1.SetFocus()
end event

type cb_base_help from w_base_response`cb_base_help within w_base_open
int X=1275
int Y=276
int TabOrder=10
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_base_open
int X=1275
int Y=152
int TabOrder=50
end type

type cb_base_ok from w_base_response`cb_base_ok within w_base_open
int X=1275
int Y=28
int TabOrder=40
end type

type dw_main from u_base_dw within w_base_open
int X=23
int Y=12
int Width=1152
int Height=580
int TabOrder=20
boolean BringToTop=true
string DataObject="d_open"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event ue_keydown;call super::ue_keydown;Long	ll_CurrentRow,&
		ll_MaxRows
		
ll_MaxRows = This.RowCount()		
ll_CurrentRow = This.GetRow()
IF ll_CurrentRow = 0 Then 
	ll_CurrentRow = 1
	IF This.SetRow(ll_CurrentRow) < 1 Then Return
END IF

Choose Case Key
	Case KeyRightArrow!, KeyDownArrow!
		ll_CurrentRow++
	Case KeyLeftArrow!, KeyUpArrow!
		ll_CurrentRow --
	Case KeyHome!
		ll_CurrentRow = 1
	Case KeyEnd!
		ll_CurrentRow = ll_MaxRows
	Case KeyPageUp!
		ll_CurrentRow += - 10
	Case KeyPageDown!
		ll_CurrentRow += 10
End Choose	

IF ll_CurrentRow > ll_MaxRows Then ll_CurrentRow = ll_MaxRows
IF ll_CurrentRow < 1 then ll_CurrentRow = 1

This.SelectRow(0, False)
This.SelectRow( ll_CurrentRow, True)

 
end event

event constructor;call super::constructor;This.is_selection = '1'
This.DataObject = 'd_' + Message.nf_Get_App_ID() + 'Open'

end event

event doubleclicked;call super::doubleclicked;Parent.TriggerEvent("ue_base_ok")
end event

type tab_1 from tab within w_base_open
int X=23
int Y=588
int Width=1152
int Height=100
int TabOrder=30
boolean FocusOnButtonDown=true
boolean RaggedRight=true
int SelectedTab=1
TabPosition TabPosition=TabsOnBottom!
long BackColor=79741120
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_1 tabpage_1
end type

event selectionchanged;dw_main.SetRedraw(False)
dw_main.SetFilter("")
dw_main.Filter()
dw_main.SetFilter("Lower(window_type) = '" + Trim(Lower(tab_1.Control[newindex].Text)) + "'")
dw_main.Filter()
//If dw_main.GetSelectedRow(0) = 0 Then
	dw_main.SelectRow(0, False)
	dw_main.SelectRow(1, True)
//End If
dw_main.SetRedraw(True)
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.Control[]={this.tabpage_1}
end on

on tab_1.destroy
destroy(this.tabpage_1)
end on

type tabpage_1 from userobject within tab_1
int X=18
int Y=16
int Width=1115
long BackColor=79741120
string Text=" "
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=536870912
end type

