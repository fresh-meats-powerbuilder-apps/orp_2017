﻿$PBExportHeader$w_find.srw
forward
global type w_find from w_base_popup
end type
type st_findwhere from u_base_statictext within w_find
end type
type st_searchfor from u_base_statictext within w_find
end type
type ddlb_findwhere from u_base_dropdownlistbox within w_find
end type
type st_searchdirection from u_base_statictext within w_find
end type
type cbx_wholeword from u_base_checkbox within w_find
end type
type cbx_matchcase from u_base_checkbox within w_find
end type
type cb_findnext from u_base_commandbutton within w_find
end type
type cb_cancel from u_base_commandbutton within w_find
end type
type sle_findwhat from u_base_singlelineedit within w_find
end type
type ddlb_searchdirection from u_base_dropdownlistbox within w_find
end type
end forward

global type w_find from w_base_popup
int X=385
int Y=561
int Width=1838
int Height=609
boolean TitleBar=true
string Title="Find"
long BackColor=80263581
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
event ue_default ( )
st_findwhere st_findwhere
st_searchfor st_searchfor
ddlb_findwhere ddlb_findwhere
st_searchdirection st_searchdirection
cbx_wholeword cbx_wholeword
cbx_matchcase cbx_matchcase
cb_findnext cb_findnext
cb_cancel cb_cancel
sle_findwhat sle_findwhat
ddlb_searchdirection ddlb_searchdirection
end type
global w_find w_find

type variables
Public:
u_findattrib inv_findattrib

end variables

event ue_default;//////////////////////////////////////////////////////////////////////////////
//
//	Event: 			ue_default
//
//	Arguments: 		None
//
//	Returns:  		None
//
//	Description:  gather all inforamtion entered by the user and call event
// 					on requestor to search for the text
//
//////////////////////////////////////////////////////////////////////////////

//Set Find Where index (this index is the array number the user selected).
inv_findattrib.ii_lookindex = ddlb_findwhere.finditem(ddlb_findwhere.text,1)

//Set text to Find What.
inv_findattrib.is_find = sle_findwhat.text

//Set the Direction value.
inv_findattrib.is_direction = ddlb_searchdirection.text

//Set the WholeWord flag.
inv_findattrib.ib_wholeword = cbx_wholeword.Checked

//Set the MatchCase flag.
inv_findattrib.ib_matchcase = cbx_matchcase.Checked

//Call event to process.
If inv_findattrib.ipo_requestor.dynamic event ue_findnext(inv_findattrib) = 0 Then
	messagebox(this.Title, this.Title+" has finished searching.")
end if

end event

event open;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  open
//
//	Arguments: none
//	
// Returns:  none
//
//	Description:  	This event is used to initialize the window using the 
//						passed in nvo-structure.
//////////////////////////////////////////////////////////////////////////////
integer 	li_count=0
integer	li_i=0
integer	li_adjust=0

//Make a local copy of attributes.
inv_findattrib = message.powerobjectparm

/////////////////////////////////////////////////////////////////////////////
// Set the Enabled/Visible attributes for the appropriate controls.
//////////////////////////////////////////////////////////////////////////////

//The Whole Word control.
cbx_wholeword.Visible = inv_findattrib.ib_wholewordvisible
cbx_wholeword.Enabled = inv_findattrib.ib_wholewordenabled

//The Match Case control.
cbx_matchcase.Visible = inv_findattrib.ib_matchcasevisible
cbx_matchcase.Enabled = inv_findattrib.ib_matchcaseenabled

//The lookup controls.
ddlb_findwhere.Visible = inv_findattrib.ib_lookvisible
st_findwhere.Visible = inv_findattrib.ib_lookvisible
ddlb_findwhere.Enabled = inv_findattrib.ib_lookenabled
st_findwhere.Enabled = inv_findattrib.ib_lookenabled

//The direction controls.
ddlb_searchdirection.Visible = inv_findattrib.ib_directionvisible
st_searchdirection.Visible = inv_findattrib.ib_directionvisible
ddlb_searchdirection.Enabled = inv_findattrib.ib_directionenabled
st_searchdirection.Enabled = inv_findattrib.ib_directionenabled

//////////////////////////////////////////////////////////////////////////////
// Initialize controls with the appropriate data.
//////////////////////////////////////////////////////////////////////////////

//Set the lookup values.
If ddlb_findwhere.visible Then
	li_count = upperbound(inv_findattrib.is_lookdata)
	if li_count >0  THEN 
		for li_i=1 TO li_count
			ddlb_findwhere.additem(inv_findattrib.is_lookdisplay[li_i])
		next
	end if
	If inv_findattrib.ii_lookindex > 0 Then
		ddlb_findwhere.SelectItem(inv_findattrib.ii_lookindex)	
	Else
		ddlb_findwhere.SelectItem(1)	
	End If
End If

//Set text to Find What.
sle_findwhat.text = inv_findattrib.is_find

//Set the WholeWord flag.
If cbx_wholeword.Visible Then
	cbx_wholeword.Checked = inv_findattrib.ib_wholeword
End If

//Set the MatchCase flag.
If cbx_matchcase.Visible Then
	cbx_matchcase.Checked = inv_findattrib.ib_matchcase
End If	

//Set the Direction attribute.
If ddlb_searchdirection.visible Then
	If Lower(inv_findattrib.is_direction)= 'up' Then
		ddlb_searchdirection.Text = 'Up'
	Else
		ddlb_searchdirection.Text = 'Down'
	End If
End If

//////////////////////////////////////////////////////////////////////////////
// Resize window and Move controls, if appropriate.
//////////////////////////////////////////////////////////////////////////////

//If the lookup controls are not visible, moving of other controls is required.
if ddlb_findwhere.visible = False then
	// calculate Y position to adjust.
	li_adjust = sle_findwhat.y - ddlb_findwhere.y

	// move other controls up.
	cbx_matchcase.y = cbx_matchcase.y - li_adjust
	cbx_wholeword.y = cbx_wholeword.y - li_adjust
	ddlb_searchdirection.y = ddlb_searchdirection.y - li_adjust
	sle_findwhat.y = sle_findwhat.y - li_adjust
	st_searchdirection.y = st_searchdirection.y - li_adjust
	st_searchfor.y = st_searchfor.y - li_adjust

	//Resize the window to match the adjustment
	this.Height = this.Height - li_adjust	
	
	//Set focus on the appropriate control.
	sle_findwhat.SetFocus()	
end if

//If the wholeword is not visible, move the matchase control.
If cbx_wholeword.visible = False and cbx_matchcase.Visible Then
	cbx_matchcase.Y = cbx_wholeword.Y
	//Resize the window to match the adjustment
	this.Height = this.Height - (cbx_matchcase.Height /2)
End If

//If all bottom conrols are not visible, adjust the size of the window.
If ddlb_searchdirection.Visible=False And cbx_wholeword.visible = False And &
	cbx_matchcase.Visible= False Then
	this.Height = this.Height - (cbx_wholeword.Height *2)
End If
end event

on w_find.create
int iCurrent
call w_base_popup::create
this.st_findwhere=create st_findwhere
this.st_searchfor=create st_searchfor
this.ddlb_findwhere=create ddlb_findwhere
this.st_searchdirection=create st_searchdirection
this.cbx_wholeword=create cbx_wholeword
this.cbx_matchcase=create cbx_matchcase
this.cb_findnext=create cb_findnext
this.cb_cancel=create cb_cancel
this.sle_findwhat=create sle_findwhat
this.ddlb_searchdirection=create ddlb_searchdirection
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_findwhere
this.Control[iCurrent+2]=st_searchfor
this.Control[iCurrent+3]=ddlb_findwhere
this.Control[iCurrent+4]=st_searchdirection
this.Control[iCurrent+5]=cbx_wholeword
this.Control[iCurrent+6]=cbx_matchcase
this.Control[iCurrent+7]=cb_findnext
this.Control[iCurrent+8]=cb_cancel
this.Control[iCurrent+9]=sle_findwhat
this.Control[iCurrent+10]=ddlb_searchdirection
end on

on w_find.destroy
call w_base_popup::destroy
destroy(this.st_findwhere)
destroy(this.st_searchfor)
destroy(this.ddlb_findwhere)
destroy(this.st_searchdirection)
destroy(this.cbx_wholeword)
destroy(this.cbx_matchcase)
destroy(this.cb_findnext)
destroy(this.cb_cancel)
destroy(this.sle_findwhat)
destroy(this.ddlb_searchdirection)
end on

type st_findwhere from u_base_statictext within w_find
int X=33
int Y=49
int Width=270
string Text="Find wher&e"
long BackColor=79416533
end type

type st_searchfor from u_base_statictext within w_find
int X=33
int Y=177
int Width=266
string Text="Fi&nd what"
long BackColor=79416533
end type

type ddlb_findwhere from u_base_dropdownlistbox within w_find
int X=321
int Y=49
int Width=1057
int Height=309
int TabOrder=10
boolean Sorted=false
int Accelerator=101
end type

type st_searchdirection from u_base_statictext within w_find
int X=33
int Y=305
int Width=266
string Text="&Search"
long BackColor=79416533
end type

type cbx_wholeword from u_base_checkbox within w_find
int X=865
int Y=321
int Width=476
int TabOrder=40
string Text="Whole &Word"
long BackColor=79416533
int TextSize=-8
end type

type cbx_matchcase from u_base_checkbox within w_find
int X=865
int Y=409
int Width=453
int TabOrder=50
string Text="Match &Case  "
long BackColor=79416533
int TextSize=-8
end type

type cb_findnext from u_base_commandbutton within w_find
int X=1436
int Y=49
int Width=325
int TabOrder=60
string Text="&Find Next"
int TextSize=-8
end type

event clicked;//////////////////////////////////////////////////////////////////////////////
//
//	Event:  			clicked
//
//	Arguments: 		None
//	
//	Returns:  		None
//
//	Description:  	Perform the find next functionality.
//
//////////////////////////////////////////////////////////////////////////////

Parent.Event ue_Default()
end event

type cb_cancel from u_base_commandbutton within w_find
int X=1436
int Y=165
int Width=325
int TabOrder=70
string Text="Cancel"
int TextSize=-8
end type

event clicked;//////////////////////////////////////////////////////////////////////////////
//
//	Event: 		 	clicked
//
//	Arguments: 		None
//
//	Returns:  		None
//
//	Description:  	Close this window
//
//////////////////////////////////////////////////////////////////////////////

Close(Parent)
end event

type sle_findwhat from u_base_singlelineedit within w_find
int X=321
int Y=177
int Width=1057
int Height=81
int TabOrder=20
int Accelerator=110
end type

type ddlb_searchdirection from u_base_dropdownlistbox within w_find
int X=321
int Y=305
int Width=275
int Height=237
int TabOrder=30
boolean Sorted=false
int Accelerator=115
long BackColor=1090519039
string Item[]={"Down",&
"Up"}
end type

