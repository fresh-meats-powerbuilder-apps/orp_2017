﻿$PBExportHeader$u_internet.sru
forward
global type u_internet from nonvisualobject
end type
end forward

global type u_internet from nonvisualobject
end type
global u_internet u_internet

type prototypes
protected Function long InternetOpen(string as_agent, &
			ulong al_access_type, &
			string as_proxyname, &
			string as_proxybypass, &
			ulong al_flags) library "wininet.dll" alias for "InternetOpenA;Ansi"

protected Function long InternetOpenURL(long al_hInternetSession, &
			string as_Url, &
			string as_headers, &
			long al_header_length, &
			long al_flags, &
			long al_context) library "wininet.dll" alias for "InternetOpenUrlA;Ansi"

protected Function int InternetReadFile (Long al_hFile, &
			ref string as_buffer, &
			long al_total_bytes_read, &
			ref long al_bytes_read) &
			library "wininet.dll" alias for "InternetReadFile;Ansi"

protected Function long InternetCloseHandle (Long al_hInternet) library "wininet.dll"

end prototypes

forward prototypes
public function string nf_readurl (string as_appid, string as_url)
end prototypes

public function string nf_readurl (string as_appid, string as_url);ulong		lul_hInternetSession, &
			lul_hUrlFile
			
long		ll_BytesRead

String	ls_return, &
			ls_html

ls_return = ''

lul_hInternetSession = InternetOpen(as_appid, 0, '', '', 0)
If IsNull(lul_hInternetSession) Then
	return ls_return
End if

lul_hUrlFile = InterNetOpenUrl(lul_hInternetSession, as_url, '', 0, 2147483648, 0)
If IsNull(lul_hUrlFile) Then
	InternetCloseHandle(lul_hInternetSession)
	return ls_return
End if


Do 
	ls_html = Space(10000)
	InternetReadFile(lul_hUrlFile, ls_html, 10000, ll_BytesRead)
	if ll_BytesRead = 0 Then Exit
	
	If ll_BytesRead < 10000 Then
		ls_return += Left(ls_html, ll_BytesRead)
		Exit
	Else
		ls_return += ls_html
	End if
Loop while True

If Not IsNull(lul_hUrlFile) Then
	InternetCloseHandle(lul_hURLFile)
End if
If Not IsNull(lul_hInternetSession) Then
	InternetCloseHandle(lul_hInternetSession)
End If

return ls_return

end function

on u_internet.create
TriggerEvent( this, "constructor" )
end on

on u_internet.destroy
TriggerEvent( this, "destructor" )
end on

