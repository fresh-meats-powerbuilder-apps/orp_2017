﻿$PBExportHeader$orp.sra
$PBExportComments$This is the application object for Order Processing
forward
global type orp from application
end type
global u_base_transaction_ext sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global u_base_message_ext message
end forward

global variables
// A pointer the MDI Frame window
w_base_frame_ext	iw_frame

w_netwise_Frame	gw_NetWise_Frame
w_Base_Frame	gw_Base_Frame
 
u_netwise_transaction	gtr_netwise

string gs_Helpdesk_no //Added for HD0000000636867


end variables

global type orp from application
string appname = "orp"
long richtextedittype = 2
long richtexteditversion = 1
string richtexteditkey = ""
end type
global orp orp

type prototypes

end prototypes

type variables
string	is_date_format

Private:	
u_AbstractClassFactory	iu_ClassFactory
end variables

forward prototypes
public function integer af_getclassfactory (ref u_abstractclassfactory au_classfactory)
end prototypes

public function integer af_getclassfactory (ref u_abstractclassfactory au_classfactory);
/* --------------------------------------------------------
Application function int af_GetClassFactory( ref u_AbstractClassFactory au_ClassFactory)

<Usage>	This Application function is used to get a reference to a class factory.
			If the class factory is not instantiated it will create one and return the reference.
			0 Return code is successful -1 indicates an error
</usage>
			
<AUTH>	Jim Weier Created 11-05-1998	</AUTH>
<Modifications></Modifications>

--------------------------------------------------------- */
u_ErrorContext	lu_ErrorContext
u_ObjectList	lu_WlsObjectList
Connection	lu_WlsConnection



if not IsValid(This.iu_classfactory) Then
	lu_WlsObjectList	=	CREATE	u_ObjectList
	lu_WlsObjectList.uf_Initialize()

	iu_ClassFactory = CREATE u_ClassFactory
	iu_ClassFactory.uf_Initialize(lu_WlsObjectList,lu_WlsConnection)
	au_ClassFactory = iu_classfactory
ELse
	au_ClassFactory = iu_classfactory
End if
Return 0

end function

event systemerror;Open(w_system_error)
end event

event open;integer	li_return
li_return = RegistryGet("HKEY_CURRENT_USER\Control Panel\International","sShortDate",is_date_Format)
li_Return  = RegistrySet("HKEY_CURRENT_USER\Control Panel\International","sShortDate","MM/dd/yyyy")
li_return = RegistrySet("HKEY_USERS\.DEFAULT\Control Panel\International","sShortDate","MM/dd/yyyy")
Message.nf_set_app_id("ORP")
OpenWithParm(iw_frame, "ibp002.ini", "w_base_frame_ext")

end event

on orp.create
appname="orp"
message=create u_base_message_ext
sqlca=create u_base_transaction_ext
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on orp.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

