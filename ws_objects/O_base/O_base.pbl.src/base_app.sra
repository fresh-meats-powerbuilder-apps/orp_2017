﻿$PBExportHeader$base_app.sra
forward
global u_base_transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global u_base_message message
end forward

global variables
w_base_frame	gw_base_frame




end variables

global type base_app from application
 end type
global base_app base_app

on base_app.create
appname = "base_app"
message = create u_base_message
sqlca = create u_base_transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on base_app.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

event open;MessageBox("NO", "This application is only for regerating, not for running")
Halt Close
end event

