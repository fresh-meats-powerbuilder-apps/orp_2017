﻿$PBExportHeader$u_base_commandbutton.sru
$PBExportComments$Base Standard Command Button
forward
global type u_base_commandbutton from commandbutton
end type
end forward

global type u_base_commandbutton from commandbutton
int Width=247
int Height=109
int TabOrder=1
string Text="none"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global u_base_commandbutton u_base_commandbutton

type variables

end variables

