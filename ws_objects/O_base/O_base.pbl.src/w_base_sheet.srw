﻿$PBExportHeader$w_base_sheet.srw
$PBExportComments$Base Sheet Window for MDI sheets
forward
global type w_base_sheet from window
end type
type str_datawindows from structure within w_base_sheet
end type
end forward

type str_datawindows from structure
	datawindow		dw_control
	boolean		dw_modified
	string		dw_tablabel
end type

global type w_base_sheet from window
integer x = 389
integer y = 468
integer width = 2857
integer height = 1452
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
event ue_postopen pbm_custom75
event ue_clearwindow pbm_custom74
event ue_filesave pbm_custom73
event ue_filesaveas pbm_custom72
event ue_search pbm_custom71
event ue_query pbm_custom70
event ue_move pbm_move
event ue_fileprint pbm_custom69
event ue_filenew pbm_custom68
event ue_filedelete pbm_custom67
event ue_systemcommand pbm_syscommand
event ue_set_data ( string as_data_item,  string as_value )
event ue_get_data ( string as_value )
event ue_printwithsetup ( )
end type
global w_base_sheet w_base_sheet

type prototypes

end prototypes

type variables
// Keeps track of all the datawindows on the sheet
// along with some other information about the datawindow


// Update was successful
boolean	ib_UpdateSuccessful

// If sheet is in querymode
boolean ib_InQueryMode

// Holds the original width and height of the window
long	il_OriginalWidth, &
	il_OriginalHeight

// Holds WorkSpaceHeight and WorkSpaceWidth
long        il_WorkSpaceHeight, &
              il_WorkSpaceWidth
				  
long	il_BorderPaddingWidth = 75, &
		il_BorderPaddingHeight = 150
		
//variable used to activate faxing
boolean	ib_faxing_available

//variable used by faxing, has custid, type
string	is_customer_info, &
	is_typecode

//variables used for printing
Boolean	ib_print_ok

Private:
str_datawindows	istr_datawindows[]
end variables

forward prototypes
public function integer wf_modified ()
public subroutine wf_filter ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_print ()
public subroutine wf_filenew ()
public subroutine wf_delete ()
public function boolean wf_next ()
public function boolean wf_previous ()
public function boolean wf_import ()
public subroutine wf_save_win_coord ()
public subroutine wf_set_winpos ()
public subroutine wf_set_winsize ()
public subroutine wf_auto_vscrollbar ()
public subroutine wf_auto_hscrollbar ()
public function boolean wf_saveas ()
end prototypes

event ue_postopen;// Loop through the windows's control array and find all datawindows.
// We keep track of the datawindows in a structure array.

integer		li_ControlIndex, &
				li_ControlCount, &
				li_dwIndex, &
				li_tabcount, &
				li_pgindex,&
				li_tabcontrolcnt,&
				li_tabcontrolindex

tab			lgr_tab

//u_sdkCalls	lu_sdkCalls

// JAW - 12/171996 - Windows 95 does not have the resource problem like 3.xx
////Display warning box if low on system resources.
//lu_sdkCalls = Create u_sdkCalls
//li_resources = lu_sdkCalls.nf_GetFreeResourcePercent()
//Destroy lu_SdkCalls
//IF li_resources < 20 THEN
//	MessageBox('Warning', 'Low on System Resources' + ' (' + String(li_resources) + '%' + ')' + ', Please Close one or more applications')
//END IF
//
//
// Get the total number of controls on this sheet
li_ControlCount = UpperBound(This.Control[])

// Loop through the window's control array and keep track
// of all the datawindow controls on the sheet
FOR li_ControlIndex = 1 To li_ControlCount
	CHOOSE CASE TypeOf(This.Control[li_ControlIndex])
		CASE	DataWindow!
			li_dwIndex ++
			istr_datawindows[li_dwIndex].dw_control = This.Control[li_ControlIndex]
		CASE	tab!
			lgr_tab	= This.Control[li_ControlIndex]
			li_tabcount		= UpperBound(lgr_tab.Control[])
			FOR li_pgindex	=	1 to li_tabcount
				li_tabcontrolcnt	=	UpperBound(lgr_tab.Control[li_pgindex].Control[])
				FOR li_tabcontrolindex	=	1 to li_tabcontrolcnt
					IF Typeof(lgr_tab.Control[li_pgindex].Control[li_tabcontrolindex]) = DataWindow! THEN
						li_dwIndex++						
						istr_datawindows[li_dwIndex].dw_control = lgr_tab.Control[li_pgindex].Control[li_tabcontrolindex]
					END IF
				NEXT
			NEXT
	END CHOOSE	
NEXT

// This opens the window in the same size  and pos if the last saved 
// option is set to true in the settings window
IF gw_base_frame.iu_base_data.is_always_openas	=	'last saved' THEN
		wf_set_winpos ( )
		wf_set_winsize ( )
END IF

If This.WindowType = Main! Then
	// if it is a child, response or popup, don't display on last used list
	gw_base_frame.im_base_menu.mf_SetLastUsed(This.Title, ClassName(This))
End if


end event

on ue_clearwindow;// Loop through all controls on the window.  If they are
// datawindow controls, reset and insert a blank row.

Long			ll_ControlCount, &
				ll_Control
Datawindow	ldw_Control

ll_ControlCount = UpperBound(This.Control[])

FOR ll_Control = 1 TO ll_ControlCount
	IF TypeOf(This.Control[ll_Control]) = Datawindow! THEN
		ldw_Control = This.Control[ll_Control]
		ldw_Control.Reset()
		ldw_Control.InsertRow(0)
	END IF
NEXT

end on

event ue_query;ib_InQueryMode = TRUE
end event

event ue_fileprint;ib_print_ok=TRUE
end event

event ue_systemcommand;Window	lw_Window

IF Message.WordParm = 61504 OR Message.WordParm = 61520 Then 
	lw_Window = gw_base_Frame.GetFirstSheet()
	lw_Window = gw_base_Frame.GetNextSheet( lw_Window)
	IF IsValid( lw_Window) Then Open(w_ctrl_tab_window_switching)
	Message.processed = true
	Message.returnvalue = 0
END IF



end event

event ue_printwithsetup();//********************************************************************
//** IBDKEEM ** 04/18/2002 ** 
//********************************************************************
String	ls_printer_name
Boolean	lb_true = True, &
			lb_false = False

Integer	li_from = 1, &
			li_to = 1, &
			li_min = 1, &
			li_max = 1, &
			li_copies = 1
			
u_print_functions lu_print_functions	
lu_print_functions = create u_print_functions

IF lu_print_functions.uf_printdialog() Then 
	This.TriggerEvent("ue_FilePrint")
END IF

//********************************************************************
//** IBDKEEM ** 04/18/2002 ** 
//********************************************************************
end event

public function integer wf_modified ();////////////////////////////////////////////////////////////////////////
//		
//	Function:	wf_Modified()
//
//	Purpose:		Calls a function on each datawindow that performs an AcceptText
//					and checks to see if any data has been modified.  
//
//	Return Values:
//				Modified Datawindow(s):		1
//				No Modified Datawindow(s):	0
//				Error on AcceptText():		-1
//-----------------------------------------------------------------------
//
//////////////////////////////////////////////////////////////////////////

integer		li_index,	&
				li_ReturnValue, &
				li_DWCount

// Get the total number of datawindow controls on the sheet.
li_DWCount = UpperBound(istr_datawindows[])

// Loop through the array to see if any datawindows have been modified
For li_index = 1 To li_DWCount

		// Call a function to check if the datawindow was modified and
		// keep track of all modified datawindows via the iwstr_update_seq[].dw_modified
		CHOOSE CASE istr_datawindows[li_index].dw_control.function dynamic uf_modified()

			CASE 1  	// Datawindow was modified
				istr_datawindows[li_index].dw_modified = TRUE
				li_ReturnValue = 1

			CASE 0	// Datawindow was not modified.
				// Do nothing

			CASE -1
			// Datawindow had an error performing AcceptText().
			// Stop looping through the datawindows
				li_ReturnValue = -1
				Exit

		END CHOOSE

NEXT

// Return the return value
	Return li_ReturnValue

end function

public subroutine wf_filter ();datawindow		ldw_datawindow
graphicObject	lo_object
string			ls_filter


lo_object = GetFocus( ) 

If TypeOf(lo_object) = Datawindow! Then

	// Get a pointer to the current datawindow
	ldw_datawindow = lo_object

	// Pop up the default filter dialog for the current datawindow
	SetNull(ls_filter)
	ldw_datawindow.SetFilter(ls_filter)
	ldw_datawindow.Filter( )

Else
	// Tell the user that no datawindow has focus to sort
	MessageBox("Filter", "There is no datawindow selected to be filtered.", StopSign!, OK!)
End If


end subroutine

public function boolean wf_retrieve ();////////////////////////////////////////////////////////////////////////
//		
//	Function:	wf_Retrieve()
//
//	Purpose:		This is the default way in which to retrieve data into
//					datawindows on the sheet.  It will loop through and issue
//					a retrieve on each datawindow on the sheet.  Most likely
//					you will need a more specific way to retrieve data at a
//					descendant level, so create a function called wf_retrieve()
//					at the descendant level to overload this ancestor function.
//
//	Arguments:	None
//
//	Returns:		Boolean 	TRUE - 	All datawindows retrieved successfully.
//								FALSE - 	Problem retrieving datawindows	
//////////////////////////////////////////////////////////////////////////
integer		li_TotalControls, &
				li_ControlIndex
u_base_dw	ldw_datawindow


// Get the total number of controls on this sheet
li_TotalControls = UpperBound(This.Control[])

// Loop through the control array to find all datawindow controls
For li_ControlIndex = 1 to li_TotalControls

	// If this control is a datawindow, retrieve data into it
	If TypeOf(This.Control[li_ControlIndex]) = Datawindow! Then
		ldw_Datawindow = This.Control[li_ControlIndex]
//		If ldw_datawindow.TriggerEvent("ue_retrieve") Then
//			Return FALSE
//		End If
	End If	

Next

Return False
end function

public function boolean wf_update ();RETURN TRUE
end function

public subroutine wf_print ();
end subroutine

public subroutine wf_filenew ();// This will be file new
end subroutine

public subroutine wf_delete ();
end subroutine

public function boolean wf_next ();return true
end function

public function boolean wf_previous ();return true
end function

public function boolean wf_import ();GraphicObject		lo_focus


lo_focus = GetFocus()
If TypeOf(lo_focus) = DataWindow! Then
	DataWindow	ldw_focus

	ldw_focus = lo_focus
	ldw_focus.PostEvent("ue_import")
End if

Return True
end function

public subroutine wf_save_win_coord ();string		ls_win_state, &
				ls_win_class

//determine classname for the window
ls_win_class = This.ClassName()

//convert enumerated type to string
CHOOSE CASE This.WindowState
	CASE Minimized!
		ls_win_state = "Min"
	CASE Maximized!
		ls_win_state = "Max"
	CASE ELSE
		lS_win_state = "Normal"
END CHOOSE

//write the window state to the ini file
	SetProfileString(gw_base_frame.is_userini, "WinCoordinates", &
											ls_win_class + "State", ls_win_state)

//If the window is normal then save the coordinates to ini file
IF ls_win_state = "Normal" THEN
	SetProfileString(gw_base_frame.is_userini, "WinCoordinates", &
								ls_win_class + "X", String(This.X))
	SetProfileString(gw_base_frame.is_userini, "WinCoordinates", &
								ls_win_class + "Y", String(This.Y))
	SetProfileString(gw_base_frame.is_userini, "WinCoordinates", &
								ls_win_class + "Width", String(This.width))
	SetProfileString(gw_base_frame.is_userini, "WinCoordinates", &
								ls_win_class + "Height", String(This.height))
END IF




end subroutine

public subroutine wf_set_winpos ();string		ls_classname

integer		li_newX, &
				li_newY

//determine the classname
ls_classname = This.ClassName()

//get the coordinate values for the win from ini file
li_newX = ProfileInt(gw_base_frame.is_userini, "wincoordinates", &
										ls_classname + "X", -1)
li_newY = ProfileInt(gw_base_frame.is_userini, "wincoordinates", &
										ls_classname + "Y", -1)

IF li_newX <> -1 THEN
	This.Move(li_newX, li_newY)
END IF
end subroutine

public subroutine wf_set_winsize ();string		ls_classname, &
				ls_newstate

integer		li_newwidth, &
				li_newheight

windowstate	lws_windowstate


//get the classname
ls_classname = This.ClassName()

//get the values from ini file(coordinates and state)
ls_newstate = ProfileString(gw_base_frame.is_userini, "wincoordinates", &
											ls_classname + "State", "")
li_newwidth		= ProfileInt(gw_base_frame.is_userini, "wincoordinates", &
											ls_classname + "Width", -1)
li_newheight		= ProfileInt(gw_base_frame.is_userini, "wincoordinates", &
											ls_classname + "Height", -1)

//convert string to enumerated type
CHOOSE CASE ls_newstate
	CASE "Max"
		lws_windowstate = Maximized!
	CASE "Min"
		lws_windowstate = Minimized!
END CHOOSE

IF li_newwidth <> -1 THEN This.Resize(li_newwidth, li_newheight)

end subroutine

public subroutine wf_auto_vscrollbar ();//resize event for base
//to automate scollbars on windows
IF (il_WorkSpaceHeight = 0) and (il_WorkSpaceWidth = 0 ) THEN
	il_WorkSpaceHeight=WorkSpaceHeight()
	il_WorkSpaceWidth=WorkSpaceWidth()
	RETURN
END IF

IF (this.windowstate <> Minimized!) THEN
	IF (this.WorkSpaceHeight() < il_WorkSpaceHeight) and (this.Vscrollbar = false) THEN
		this.Vscrollbar = true
	END IF
	IF (this.WorkSpaceHeight() > il_WorkSpaceHeight) and (this.Vscrollbar = true) THEN
		this.Vscrollbar = false
	END IF
END IF





end subroutine

public subroutine wf_auto_hscrollbar ();//resize event for base
//to automate scollbars on windows
IF (il_WorkSpaceHeight = 0) and (il_WorkSpaceWidth = 0 ) THEN
	il_WorkSpaceHeight=WorkSpaceHeight()
	il_WorkSpaceWidth=WorkSpaceWidth()
	RETURN
END IF

IF (this.windowstate <> Minimized!) THEN
	IF (this.WorkSpaceWidth() < il_WorkSpaceWidth) and (this.Hscrollbar = false) THEN
		this.Hscrollbar = true
	END IF
	IF (this.WorkSpaceWidth() > il_WorkSpaceWidth) and (this.Hscrollbar = true) THEN
		this.Hscrollbar = false
	END IF
END IF


end subroutine

public function boolean wf_saveas ();GraphicObject		lo_focus


lo_focus = GetFocus()
If TypeOf(lo_focus) = DataWindow! Then
	DataWindow	ldw_focus

	ldw_focus = lo_focus
	ldw_focus.PostEvent("ue_saveas")
End if

Return True
end function

event closequery;//////////////////////////////////////////////////////////////////////
//		
//	Event:	Closequery
//
//	Purpose:	Before closing the window, check to see if any changes
//				have been made to any of the datawindows.  If so, prompt
//				the user to save the data. The window function wf_modified()
//				is used to determine if modifications have been made to any
//				of the datawindows on the sheet.
//
//////////////////////////////////////////////////////////////////////

integer	li_prompt_onsave

//gets the settings values
gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(li_prompt_onsave)
IF gw_base_frame.ib_exit and li_prompt_onsave = 0 THEN RETURN

// Do a security check for save and do nothing if the user does not have priviliges
IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN

CHOOSE CASE This.wf_modified( )

	CASE 1
		// Changes were made to one of the datawindows
			CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)

				CASE 1	// Save Changes
					If This.wf_update() = FALSE Then
						Message.ReturnValue = 1	 // Update failed - do not close window
					Else
						Return
					End If

				CASE 2	// Do not save changes

				CASE 3	// Cancel the closing of window
					Message.ReturnValue = 1
					Return
			END CHOOSE

	CASE 0
		// No modifications were made to any datawindows on this sheet...closing of sheet allowed

	CASE -1
		// Problem with the AcceptText() function against a datawindow.
		// The item error will handle setting focus to the correct column. 
		Message.ReturnValue = 1
		Return

END CHOOSE

end event

event resize;Boolean        lb_dw_scroll
Long           ll_ControlCount, &
               ll_Control

String 			ls_title

integer			li_hscrollbars, &
					li_vscrollbars

Datawindow     ldw_Control

IF This.WindowState = Maximized! Then
	Send(Handle(This), 276, 6, 0)
	Send(Handle(This), 277, 6, 0)
	ls_title  = gw_base_frame.Title + " - [" + This.Title +"]"
Else
	ls_title = gw_base_frame.Title
END IF
SetProfileString( gw_base_frame.is_userini, message.nf_Get_App_ID(), "FrameWindowTitle", ls_title)

//gets the settings values
IF IsValid(gw_base_frame.iu_base_data) THEN
	gw_base_frame.iu_base_data.Trigger Event ue_get_scrollsettings(&
													li_hscrollbars, li_vscrollbars)
END IF
// Displays scrollbars if needed (only if the option in settings win is set)
IF li_hscrollbars = 1 THEN wf_auto_hscrollbar()
IF li_vscrollbars = 1 THEN wf_auto_vscrollbar()

//determine if there is a scroll bar on the window, if so, adjust the size for Windows7 border
ll_ControlCount = UpperBound(This.Control[])

FOR ll_Control = 1 TO ll_ControlCount
   IF TypeOf(This.Control[ll_Control]) = Datawindow! Then
		 ldw_Control = This.Control[ll_Control]
    	 lb_dw_scroll = ldw_control.HScrollBar
		 If NOT lb_dw_scroll Then
		 	lb_dw_scroll = ldw_control.VScrollBar
		 End If
		 IF lb_dw_scroll  THEN
          ldw_Control.Resize(this.width - ldw_Control.x - il_BorderPaddingWidth, &
                             this.height - ldw_Control.y - il_BorderPaddingHeight)
       END IF
	END IF
NEXT

end event

on close;//save window coordinates to ini file
wf_save_win_coord ( )
end on

on open;// Get the original width and height of this window
il_OriginalWidth = This.Width
il_OriginalHeight = This.Height

// Post an event that handles post-opening processing
PostEvent("ue_postopen")
end on

event activate;String 			ls_title


IF This.WindowState = Maximized! Then
	ls_title  = gw_base_frame.Title + " - [" + This.Title +"]"
Else
	ls_title = gw_base_frame.Title
END IF
SetProfileString( gw_base_frame.is_userini, message.nf_Get_App_ID(), "FrameWindowTitle", ls_title)


end event

on w_base_sheet.create
end on

on w_base_sheet.destroy
end on

