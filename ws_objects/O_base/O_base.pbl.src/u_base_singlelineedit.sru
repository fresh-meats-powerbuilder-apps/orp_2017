﻿$PBExportHeader$u_base_singlelineedit.sru
$PBExportComments$Base Standard SingleLine edit
forward
global type u_base_singlelineedit from singlelineedit
end type
end forward

global type u_base_singlelineedit from singlelineedit
int Width=247
int Height=89
int TabOrder=1
BorderStyle BorderStyle=StyleLowered!
boolean AutoHScroll=false
long TextColor=33554432
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type
global u_base_singlelineedit u_base_singlelineedit

on getfocus;// When this object gets focus, highligh any text within it
This.SelectText(1, Len(This.Text))
end on

