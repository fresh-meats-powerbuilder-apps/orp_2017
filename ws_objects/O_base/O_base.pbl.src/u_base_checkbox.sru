﻿$PBExportHeader$u_base_checkbox.sru
$PBExportComments$Base Standard Check Box
forward
global type u_base_checkbox from checkbox
end type
end forward

global type u_base_checkbox from checkbox
integer width = 247
integer height = 72
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
end type
global u_base_checkbox u_base_checkbox

on u_base_checkbox.create
end on

on u_base_checkbox.destroy
end on

