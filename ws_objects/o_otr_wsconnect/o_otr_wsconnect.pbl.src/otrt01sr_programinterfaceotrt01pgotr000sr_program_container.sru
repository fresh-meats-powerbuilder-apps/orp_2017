﻿$PBExportHeader$otrt01sr_programinterfaceotrt01pgotr000sr_program_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterfaceOtrt01pgOtr000sr_program_container from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterfaceOtrt01pgOtr000sr_program_container from nonvisualobject
end type

type variables
    int otr000sr_rval
    string otr000sr_message
    ulong otr000sr_task_num
    ulong otr000sr_max_record_num
    ulong otr000sr_last_record_num
    uint otr000sr_version_number
end variables

on otrt01sr_ProgramInterfaceOtrt01pgOtr000sr_program_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterfaceOtrt01pgOtr000sr_program_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

