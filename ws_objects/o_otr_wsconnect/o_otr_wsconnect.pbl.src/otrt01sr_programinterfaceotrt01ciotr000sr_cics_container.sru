﻿$PBExportHeader$otrt01sr_programinterfaceotrt01ciotr000sr_cics_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type otrt01sr_ProgramInterfaceOtrt01ciOtr000sr_cics_container from nonvisualobject
    end type
end forward

global type otrt01sr_ProgramInterfaceOtrt01ciOtr000sr_cics_container from nonvisualobject
end type

type variables
    string otr000sr_req_tranid
    string otr000sr_req_program
    string otr000sr_req_userid
    string otr000sr_req_password
end variables

on otrt01sr_ProgramInterfaceOtrt01ciOtr000sr_cics_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on otrt01sr_ProgramInterfaceOtrt01ciOtr000sr_cics_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

