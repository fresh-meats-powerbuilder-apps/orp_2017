﻿$PBExportHeader$w_description_popup.srw
forward
global type w_description_popup from Window
end type
type dw_description from datawindow within w_description_popup
end type
end forward

global type w_description_popup from Window
int X=673
int Y=269
int Width=1194
int Height=109
long BackColor=15793151
WindowType WindowType=child!
dw_description dw_description
end type
global w_description_popup w_description_popup

type variables
window	iw_Parent
end variables

on w_description_popup.create
this.dw_description=create dw_description
this.Control[]={ this.dw_description}
end on

on w_description_popup.destroy
destroy(this.dw_description)
end on

event open;Long						ll_NewX, &
							ll_NewY,&
							ll_Width,&
							ll_Height,&
							ll_Temp
							
String					ls_data,&
							ls_String

ll_Temp = 8

u_string_functions	lu_strings

iw_parent = Message.PowerObjectParm


ll_NewX = iw_frame.PointerX() + 50
ll_NewY = iw_Frame.PointerY() + 50

If ll_NewX + This.Width > iw_Frame.Width Then
	ll_NewX = iw_Frame.Width - This.Width - 5
End if
If ll_NewY + This.Height > iw_Frame.Height Then
	ll_NewY = iw_Frame.Height - This.Height - 100
End if

This.Move(ll_NewX, ll_NewY)
iw_parent.TriggerEvent("ue_getdata",0,'Customer_Defined_Description')
ls_String = Message.StringParm 
ll_height = dw_description.ImportString(ls_String)
ll_height = ll_height * (Long(dw_description.Object.customer_description.Height)+ll_Temp)
//ll_width = dw_description.GetItemNumber(1,"c_max") * 25
dw_description.Resize(ll_width,ll_height)
This.Resize(ll_width,ll_height)





end event

type dw_description from datawindow within w_description_popup
int Y=5
int Width=837
int Height=65
int TabOrder=1
string DataObject="d_description_popup"
boolean Border=false
boolean LiveScroll=true
end type

event constructor;//This.insertRow(0)
end event

