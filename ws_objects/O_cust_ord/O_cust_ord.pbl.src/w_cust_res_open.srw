﻿$PBExportHeader$w_cust_res_open.srw
forward
global type w_cust_res_open from w_base_response_ext
end type
type dw_main from datawindow within w_cust_res_open
end type
end forward

global type w_cust_res_open from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1371
integer height = 572
long backcolor = 12632256
dw_main dw_main
end type
global w_cust_res_open w_cust_res_open

type variables
Private:
s_error		istr_error
u_orp001		iu_orp001

u_ws_orp3		iu_ws_orp3

Window		iw_parent

Boolean		ib_customerorder
end variables

forward prototypes
public function boolean wf_verify ()
end prototypes

public function boolean wf_verify ();Boolean	lb_IsSales
Int		li_rtn
String	ls_Input, ls_ClassName

If IsValid(iw_Parent) Then
	ls_ClassName = ClassName(iw_Parent)
Else
	iw_frame.SetMicroHelp("Parent Window is invalid")
	Return False
End If
//
If ls_ClassName = 'w_reservation_weekly_mass_update' Then
	dw_main.SetRedraw(False)
	dw_main.Object.gpo_date[1] = Date("01/01/0000")
End If
dw_main.AcceptText()
ls_Input = dw_main.Describe("DataWindow.Data")

If not ib_customerorder Then 
	ls_input = Left(ls_input, Len(ls_input) - 10) + '~t' + Right(ls_input, 10)
end if		

istr_error.se_user_id = SQLCA.userid
istr_error.se_window_name = "Cust_Res_Open"
istr_error.se_function_name = "wf_Verify"
istr_error.se_procedure_name = "nf.Orpo62fr"
istr_error.se_event_name = ""

//IF NOT ISVALID(iu_orp001) THEN iu_orp001 = CREATE u_orp001
//li_rtn = iu_orp001.nf_orpo62ar( istr_error, ls_Input, "V")
IF NOT ISVALID(iu_ws_orp3) THEN iu_ws_orp3 = CREATE u_ws_orp3
li_rtn = iu_ws_orp3.uf_orpo62fr( istr_error, ls_Input, "V")

If li_rtn = 4 Then
	dw_main.Object.gpo_date[1] = Today()
	dw_main.SetRedraw(TRUE)
	Return True
End If

If LEN(ls_Input) >= 1 Or li_rtn = 0 Then
	dw_main.SetRedraw(FALSE)
	dw_Main.Reset()
	dw_main.ImportString(ls_Input)
	dw_main.SetRedraw(TRUE)
Else
//
	If ls_ClassName = 'w_reservation_weekly_mass_update' Then
		dw_main.Object.gpo_date[1] = Today()
	End If
	dw_main.SetRedraw(True)
	Return FALSE
End If
dw_main.Enabled = TRUE

If ls_ClassName = 'w_customer_order' Then
	lb_isSales = True
Else
	lb_isSales = False
End If

Choose Case dw_main.GetItemString( 1, "Order_Status")
	CASE 'E'
		// EDI
		If Not lb_isSales Then
			iw_frame.SetMicroHelp(dw_main.GetItemString(1, 'customerorderid') + &
										" is not a reservation")
			Return False
		Else
			Return True
		End if
	Case 'T', 'F'
		// Single or Multiple Customer Order
		If Not lb_isSales Then
			iw_frame.SetMicroHelp(dw_main.GetItemString(1, 'customerorderid') + &
										" is not a reservation")
			Return False
		Else
			Return True
		End if
	CASE 'N','A','U','I','M'
		// Reservation
		If Not lb_isSales Then
			Return TRUE
		Else
			iw_frame.SetMicroHelp(dw_main.GetItemString(1, 'customerorderid') + &
										" is not a Customer Order")
			Return False
		End if
	CASE ELSE
		iw_frame.SetMicroHelp(dw_main.GetItemString(1, 'customerorderid') + &
										" is an invalid reference number")
		Return FALSE
END CHOOSE

RETURN FALSE
end function

event open;call super::open;String 	Ls_Temp

u_string_functions	lu_string_function

ls_Temp = Message.StringParm

IF lu_string_function.nf_IsEmpty(ls_Temp) Then
	dw_Main.SetItem( 1, "CustomerOrderId", "")
	dw_Main.SetItem( 1, "gpo_date", Today())
	dw_Main.SetItem( 1, "week", 'A')
ELSE
	dw_main.Reset()
	dw_Main.ImportString(ls_temp)
	// ADDED CJR to fix defect 752  Feb 18 1999
	if isnull(dw_Main.getItemdate(1, "gpo_date")) then dw_Main.SetItem( 1, "gpo_date", Today())
	cb_base_ok.Enabled = True
END IF

dw_Main.SetItem( 1, "SalesLocation", message.is_Smanlocation)

iu_orp001 = Create u_orp001

iw_Parent = iw_Frame.GetActiveSheet()
If IsValid(iw_Parent) Then
	This.Title = iw_parent.Title + ' Inquire'
	IF iw_parent.ClassName() = 'w_customer_order' Or &
			iw_parent.ClassName() = 'w_reservation_weekly_mass_update' Then 
		IF iw_parent.ClassName() = 'w_customer_order' Then ib_customerorder = True
		dw_main.Modify("week.BackGround.Color = 12632256 " + &
							"week.Protect = 1" + &
							"gpo_date.BackGround.Color = 12632256 " + &
							"gpo_date.Protect = 1")
		dw_main.SetItem(1, 'week', 'A')
	End IF
End if
dw_main.SetFocus()
end event

event close;call super::close;Destroy( iu_orp001)
Destroy( iu_ws_orp3)
end event

on w_cust_res_open.create
int iCurrent
call super::create
this.dw_main=create dw_main
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
end on

on w_cust_res_open.destroy
call super::destroy
destroy(this.dw_main)
end on

event ue_base_ok;String	ls_WeekNum

date     ld_gpo_date

u_string_functions	lu_string_function

If dw_main.AcceptText() = -1 Then return
IF lu_string_function.nf_IsEmpty(dw_Main.GetItemString( 1,"customerorderid")) Then
	iw_Frame.SetMicroHelp( "Reference Number is a required field")
	dw_main.SetFocus()
	Return 
End if 
IF LEN(TRIM(dw_main.GetItemString(1, "CustomerOrderId"))) > 0 THEN
	IF dw_main.GetItemString(1, "CustomerOrderId") = "0000000" THEN
		CloseWithReturn(This, "CANCEL")

		MessageBox("ERROR","Customer Oder Id  Is Invalid")
		Return 

	END IF
ELSE
	CloseWithReturn(This, "CANCEL")
END IF
ld_gpo_date = dw_Main.GetItemDate( 1, "gpo_date")
//if iw_parent.ClassName() <> 'w_customer_order' then
	if isnull(ld_gpo_date) then
		iw_Frame.SetMicroHelp( "Date is a required field")
		dw_main.SetFocus()
		Return 
	End if
//End if 

ls_WeekNum = dw_Main.GetItemString( 1, "week")
IF wf_verify() Then
	dw_Main.SetItem( 1, "week", ls_WeekNum)
	CloseWithReturn(This, dw_Main.Describe("DataWindow.Data"))
Else
	dw_main.SetColumn('CustomerOrderId')
	dw_main.SetFocus()
END IF

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "CANCEL")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cust_res_open
integer x = 1024
integer y = 288
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cust_res_open
integer x = 1024
integer y = 164
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cust_res_open
integer x = 1024
integer y = 44
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_cust_res_open
end type

type dw_main from datawindow within w_cust_res_open
integer x = 46
integer y = 76
integer width = 942
integer height = 304
integer taborder = 20
string dataobject = "d_res_order_open"
boolean border = false
boolean livescroll = true
end type

on editchanged;IF This.GetColumnName() = 'customerorderid' Then
	IF Len( This.GetText()) > 0 Then
		iw_Frame.SetMicroHelp( "Ready")
	ELSE
		iw_Frame.SetMicroHelp( "Reference Number is a required field")
	END IF
END IF
end on

on constructor;This.InsertRow(0)
end on

on getfocus;This.SelectText(1, Len(This.GetText()))
end on

