﻿$PBExportHeader$w_customer_order_split.srw
$PBExportComments$This is the Window to split a detail line into multiple lines
forward
global type w_customer_order_split from w_base_response_ext
end type
type em_split from editmask within w_customer_order_split
end type
type st_1 from statictext within w_customer_order_split
end type
end forward

global type w_customer_order_split from w_base_response_ext
int X=243
int Y=429
int Width=1185
int Height=533
boolean TitleBar=true
string Title="Split Line"
long BackColor=12632256
em_split em_split
st_1 st_1
end type
global w_customer_order_split w_customer_order_split

type variables
Boolean		ib_ValidToClose
end variables

on close;call w_base_response_ext::close;If Not ib_ValidToClose Then
	Message.StringParm = ''
End if
end on

on w_customer_order_split.create
int iCurrent
call w_base_response_ext::create
this.em_split=create em_split
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=em_split
this.Control[iCurrent+2]=st_1
end on

on w_customer_order_split.destroy
call w_base_response_ext::destroy
destroy(this.em_split)
destroy(this.st_1)
end on

event ue_base_ok;call super::ue_base_ok;If Integer(em_split.Text) < 1 Then
	iw_frame.SetMicroHelp("Please enter number of lines to split into")
	return
End if

ib_ValidToClose = True
CloseWithReturn(This, em_split.Text)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, '')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_customer_order_split
int X=828
int Y=281
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_customer_order_split
int X=828
int Y=157
int TabOrder=50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_customer_order_split
int X=828
int Y=37
end type

type em_split from editmask within w_customer_order_split
int X=307
int Y=53
int Width=348
int Height=93
int TabOrder=20
BorderStyle BorderStyle=StyleLowered!
string Mask="###,###"
boolean Spin=true
double Increment=1
string MinMax="0~~"
long TextColor=33554432
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on getfocus;This.SelectText(1, Len(This.Text))
end on

on constructor;This.Text = '2'
end on

type st_1 from statictext within w_customer_order_split
int X=51
int Y=61
int Width=252
int Height=73
boolean Enabled=false
string Text="Split into"
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

