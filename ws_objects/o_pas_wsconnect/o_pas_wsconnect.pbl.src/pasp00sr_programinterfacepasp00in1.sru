﻿$PBExportHeader$pasp00sr_programinterfacepasp00in1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00in1 from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00in1 from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on pasp00sr_ProgramInterfacePasp00in1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00in1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

