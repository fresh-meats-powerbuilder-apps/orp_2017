﻿$PBExportHeader$pasp00sr_programinterfacepasp00cipas000sr_cics_container1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00ciPas000sr_cics_container1 from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00ciPas000sr_cics_container1 from nonvisualobject
end type

type variables
    string pas000sr_req_tranid
    string pas000sr_req_program
    string pas000sr_req_userid
    string pas000sr_req_password
end variables

on pasp00sr_ProgramInterfacePasp00ciPas000sr_cics_container1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00ciPas000sr_cics_container1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

