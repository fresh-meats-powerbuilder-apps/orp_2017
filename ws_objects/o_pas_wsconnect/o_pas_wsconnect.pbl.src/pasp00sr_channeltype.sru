﻿$PBExportHeader$pasp00sr_channeltype.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_channelType from nonvisualobject
    end type
end forward

global type pasp00sr_channelType from nonvisualobject
end type

type variables
    boolean channel
end variables

on pasp00sr_channelType.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_channelType.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

