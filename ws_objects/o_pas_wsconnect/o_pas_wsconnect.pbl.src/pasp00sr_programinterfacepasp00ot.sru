﻿$PBExportHeader$pasp00sr_programinterfacepasp00ot.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00ot from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00ot from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on pasp00sr_ProgramInterfacePasp00ot.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00ot.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

