﻿$PBExportHeader$pasp00sr_programinterfacepasp00ci.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00ci from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00ci from nonvisualobject
end type

type variables
    pasp00sr_ProgramInterfacePasp00ciPas000sr_cics_container pas000sr_cics_container
    boolean structuredContainer
end variables

on pasp00sr_ProgramInterfacePasp00ci.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00ci.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

