﻿$PBExportHeader$pasp00sr_charcontainertypewithattribute.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_charContainerTypeWithAttribute from nonvisualobject
    end type
end forward

global type pasp00sr_charContainerTypeWithAttribute from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on pasp00sr_charContainerTypeWithAttribute.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_charContainerTypeWithAttribute.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

