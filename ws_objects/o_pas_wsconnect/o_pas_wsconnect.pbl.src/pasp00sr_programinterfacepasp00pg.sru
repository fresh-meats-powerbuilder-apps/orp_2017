﻿$PBExportHeader$pasp00sr_programinterfacepasp00pg.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00pg from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00pg from nonvisualobject
end type

type variables
    pasp00sr_ProgramInterfacePasp00pgPas000sr_program_container pas000sr_program_container
    boolean structuredContainer
end variables

on pasp00sr_ProgramInterfacePasp00pg.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00pg.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

