﻿$PBExportHeader$pasp00sr_programinterfacepasp00in.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type pasp00sr_ProgramInterfacePasp00in from nonvisualobject
    end type
end forward

global type pasp00sr_ProgramInterfacePasp00in from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on pasp00sr_ProgramInterfacePasp00in.create
call super::create
TriggerEvent( this, "constructor" )
end on

on pasp00sr_ProgramInterfacePasp00in.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

