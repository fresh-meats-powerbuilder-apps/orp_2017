HA$PBExportHeader$w_co_so.srw
$PBExportComments$sales orders by customer for Cust/Res
forward
global type w_co_so from w_base_sheet_ext
end type
type dw_co_so_detail from u_base_dw_ext within w_co_so
end type
end forward

global type w_co_so from w_base_sheet_ext
integer width = 3813
integer height = 1312
string title = "Customer/Reservation Orders By Customer"
event ue_edi_clicked pbm_custom01
event ue_build_loads ( )
dw_co_so_detail dw_co_so_detail
end type
global w_co_so w_co_so

type prototypes
function int orpo45ar_co_so( &
    ref s_error s_error_info, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp003.dll" alias for "orpo45ar_co_so;Ansi"

end prototypes

type variables
u_orp001		iu_orp001
u_orp204		iu_orp204

u_ws_orp3		iu_ws_orp3
u_ws_orp2		iu_ws_orp2

u_ws_orp4      iu_ws_orp4
String 	is_StringParm = ''

S_ERROR istr_error_info

Long		il_selected_row_count


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_inquire ()
public subroutine nf_filter_orders (character ac_filter)
end prototypes

event ue_build_loads();INTEGER	li_rtn

String	ls_order_id, ls_order_out, ls_temp

Long 	ll_row

ll_row = dw_co_so_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_co_so_detail.Getitemstring(ll_row, 'order_id') + '~t' + &
				dw_co_so_detail.Getitemstring(ll_row, 'order_type') + '~r~n'
	ls_order_id += ls_temp 
	ll_row = dw_co_so_detail.Getselectedrow(ll_row)
LOOP

istr_error_info.se_event_name = 'ue_build_loads'						
										
li_rtn = iu_ws_orp4.nf_ORPO91FR(istr_error_info, &
										ls_order_id, &
										ls_order_out)										

dw_co_so_detail.SelectRow(0, False)
il_selected_row_count = 0

IF li_rtn >= 0 THEN
	This.SetRedraw(TRUE)
	Return
END IF
end event

public function boolean wf_retrieve ();OpenWithParm(w_co_so_open, is_StringParm)

CHOOSE CASE Message.StringParm
CASE "CANCEL"
	RETURN FALSE
CASE ELSE
	is_StringParm = Message.StringParm
	RETURN wf_inquire() 
END CHOOSE
end function

public function boolean wf_inquire ();String  ls_output_string,ls_debug
integer li_rtn

ls_output_string = Space( 19000)


istr_error_info.se_window_name = 'w_co_so'
istr_error_info.se_function_name = 'wf_retieve'
istr_error_info.se_event_name = 'post_open'
istr_error_info.se_procedure_name = 'orpo45ar'

ls_debug = is_stringparm

//iu_orp003.nf_orpo45ar( istr_error_info, is_stringparm, ls_output_string)

iu_ws_orp2.nf_orpo45fr(is_stringparm, ls_output_string, istr_error_info)

dw_co_so_detail.Reset()
li_rtn = dw_co_so_detail.ImportString( Trim(ls_output_string))
This.SetRedraw( True)
dw_co_so_detail.SetRedraw( True)
il_selected_row_count = 0
dw_co_so_detail.SelectRow(0,FALSE)
Return TRUE
end function

public subroutine nf_filter_orders (character ac_filter);String ls_Filter_string

ls_filter_string = ""
dw_co_so_detail.SetFilter( ls_filter_string)
dw_co_so_detail.Filter()

IF ac_filter = 'B' THEN
   Return
ELSE
	ls_filter_string = "order_type = '" + ac_filter + "'"
	dw_co_so_detail.SetFilter( ls_filter_string)
	dw_co_so_detail.Filter()
END IF

end subroutine

event ue_postopen;call super::ue_postopen;u_string_functions	lu_string_functions
u_project_functions	lu_project_functions
iu_ws_orp4 = Create u_ws_orp4
iu_ws_orp2 = Create u_ws_orp2

IF Not IsValid( iu_orp003) Then
	lu_project_functions.nf_set_s_error( istr_error_info)
END IF
IF NOT(IsValid( iu_orp003)) THEN
	iu_orp003 = Create u_orp003  
END IF

IF lu_string_functions.nf_IsEmpty( is_StringParm ) Then
	wf_Retrieve()
ELSE
	wf_inquire()
END IF

end event

event close;call super::close;Destroy( iu_orp003)
Destroy( iu_orp204)
Destroy( iu_ws_orp3)
Destroy iu_ws_orp2
DESTROY iu_ws_orp4
end event

on w_co_so.create
int iCurrent
call super::create
this.dw_co_so_detail=create dw_co_so_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_co_so_detail
end on

on w_co_so.destroy
call super::destroy
destroy(this.dw_co_so_detail)
end on

event open;call super::open;is_StringParm = Message.StringParm

iu_orp204 = Create u_orp204
end event

event resize;call super::resize;//dw_co_so_detail.Resize(this.width - dw_co_so_detail.x - 50, this.height - dw_co_so_detail.y - 105)


long       ll_x = 50,  ll_y = 105

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_co_so_detail.Resize(this.width - dw_co_so_detail.x - ll_x, this.height - dw_co_so_detail.y - ll_y)


end event

event activate;call super::activate;iw_Frame.im_Menu.mf_enable( "m_build_loads")
end event

event deactivate;iw_Frame.im_Menu.mf_Disable( "m_build_loads")
end event

type dw_co_so_detail from u_base_dw_ext within w_co_so
integer x = 5
integer y = 4
integer width = 3762
integer height = 1208
string dataobject = "d_co_so_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;Int		li_rtn

String	ls_ref_num, &
			ls_status, &
			ls_res_date
			
u_string_functions	lu_string_functions

If row = 0 Then
	SetMicroHelp("DoubleClick on a detail row")
	Return
END IF

ls_res_date = String(This.GetItemdate(row, "res_date"), 'mm-dd-yyyy')
ls_ref_num = This.GetItemString( row, "order_id") + &
				'~t~t~t' + Message.is_SmanLocation + '~t~t~t' + ls_res_date
				
istr_error_info.se_window_name = "CO/SO"
istr_error_info.se_procedure_name = "nf_Orpo62fr"
istr_error_info.se_event_name = "doubleclicked"
	
//IF NOT ISVALID(iu_orp001) THEN iu_orp001 = CREATE u_orp001
//li_rtn = iu_orp001.nf_orpo62ar( istr_error_info, ls_ref_num, "V")
IF NOT ISVALID(iu_ws_orp3) THEN iu_ws_orp3 = CREATE u_ws_orp3
li_rtn = iu_ws_orp3.uf_orpo62fr( istr_error_info, ls_ref_num, "V")

ls_status = mid(ls_ref_num, lu_string_functions.nf_npos(ls_ref_num, '~t', 1, 4) + 1, 1)
Choose Case ls_status
	Case 'F', 'E'
		// Multiple Customer Order
		iw_Frame.wf_OpenSheet( "w_customer_order", ls_ref_num + '~t' + ls_status) 

	CASE 'N','A','U','I','M'
		// Reservation
		ls_ref_num = This.GetItemString( row, "order_id")&
						+ '~tA~t'+&
						Message.is_salesperson_code+'~t'+ &
						Message.is_smanlocation	+'~t' + &
						ls_status + '~t'+ &
						ls_res_date
		iw_Frame.wf_OpenSheet( "w_reservations", ls_ref_num ) 
	CASE ELSE
		Return
END CHOOSE


RETURN





end event

event clicked;//  This script overides ancestor

IF Row < 1 THEN RETURN

if this.IsSelected(row) then
	il_selected_row_count --
  	THIS.SelectRow(row, FALSE)
else	  
	il_selected_row_count ++
	IF il_selected_row_count > 10 THEN
		MessageBox("ERROR", "You can only select up to 10 customer orders")	
		THIS.SelectRow(row, FALSE)
		il_selected_row_count --
		Return
	END IF

	Call Super::Clicked
	THIS.SelectRow(row, TRUE)
END IF
end event

event constructor;call super::constructor;is_selection = '4'
end event

