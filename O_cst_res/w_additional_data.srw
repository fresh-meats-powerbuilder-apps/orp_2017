HA$PBExportHeader$w_additional_data.srw
forward
global type w_additional_data from w_base_response_ext
end type
type dw_instruction_ind from u_base_dw_ext within w_additional_data
end type
type dw_load_instruction from u_base_dw_ext within w_additional_data
end type
type dw_additional_data from u_base_dw_ext within w_additional_data
end type
type dw_dept_code from u_base_dw_ext within w_additional_data
end type
type dw_exp_country from u_base_dw_ext within w_additional_data
end type
type dw_store_door_deck_code from u_base_dw_ext within w_additional_data
end type
end forward

global type w_additional_data from w_base_response_ext
integer x = 37
integer y = 288
integer width = 2981
integer height = 1564
string title = "Additional Input Required"
long backcolor = 12632256
string icon = "Rectangle!"
dw_instruction_ind dw_instruction_ind
dw_load_instruction dw_load_instruction
dw_additional_data dw_additional_data
dw_dept_code dw_dept_code
dw_exp_country dw_exp_country
dw_store_door_deck_code dw_store_door_deck_code
end type
global w_additional_data w_additional_data

type variables
Private:
u_orp001			iu_orp001
s_error			istr_error_info
window			iw_parent
u_orp204	  		iu_orp204
u_ws_orp1			iu_ws_orp1
u_ws_orp3			iu_ws_orp3
u_ws_orp4           iu_ws_orp4


BOOLEAN	ib_optimize = FALSE, &
			ib_IsFromReservation,&
			ib_IsFromPAInquire, &
			ib_Filtered, &
			ib_move_rows, &
			ib_IsFromCustomerOrder

LONG		il_customer_default_row

String	is_OpenParm, &
			is_TransMode,&
			is_Business_rule,&
			is_Product_code,&
			is_Division,&
			is_Load_Status, &
			is_plant, &
			is_div_po, &
			is_corp_po, &
			is_microhelp, &
			is_pricing_default_hdr, &
			is_micro_test_product_found

Date		id_Delv_date, &
			id_ship_date


end variables

forward prototypes
public subroutine wf_fill_ddws ()
public function boolean wf_calculateshipdate ()
public function integer wf_get_deliv_time (integer ai_day, string as_division, string as_customer_id, ref time at_default_delivery_time)
end prototypes

public subroutine wf_fill_ddws ();//DataWindowChild	ldwc_TempStorage
//String	ls_TypeofCustomer,&
//			ls_Customer_ID
//
//Window	lw_ActiveSheet			
//
//dw_cust-to-sales_additional-data.GetChild( "plant",ldwc_TempStorage)
//message.iw_UtlData.wf_getlocations( ldwc_TempStorage) 
//
//// First need to check the reservation is corporate billto or shipto 
//lw_ActiveSheet = iw_Frame.GetActiveSheet()
//
//IF IsValid( lw_ActiveSheet) Then 
//	lw_ActiveSheet.TriggerEvent("ue_GetCustomertype")
//	ls_TypeOfCustomer = Message.StringParm
//	lw_ActiveSheet.TriggerEvent("ue_getcustomerid")
//	ls_Customer_ID = Message.StringParm
//end if
//dw_cust-to-sales_additional-data.GetChild( "customer_id",ldwc_TempStorage)
//IF f_IsEmpty( ls_TypeOfCustomer) Then
//	Message.iw_ProjectData.wf_getcustomerdata( ldwc_TempStorage)
//ELSE
//	Message.iw_ProjectData.wf_GetShipToCustomers( ldwc_TempStorage,&
//				ls_Customer_ID, ls_TypeofCustomer)
//END IF
//
//
//
end subroutine

public function boolean wf_calculateshipdate ();String	ls_required_info, &
			ls_ship_date, &
			ls_customer, &
			ls_plant, &
			ls_delivery_date,&
			ls_Time, &
			ls_type_of_sale, &
			ls_micro_test_ship_type, &
			ls_micro_test_product_found, &
			ls_new_or_copy_order
			
Integer   li_count			
			
u_string_functions	lu_string_functions			
			
s_error	lstr_error_info

dw_additional_data.AcceptText()

ls_customer = dw_additional_data.GetItemString(1, 'customer_id')
If lu_string_functions.nf_IsEmpty(ls_customer) Then Return False
ls_plant =  dw_additional_data.GetItemString(1, 'ship_plant')
If lu_string_functions.nf_IsEmpty(ls_plant) Then Return False
ls_delivery_date = String(dw_additional_data.GetItemDate(1, &
						'schd_delv_date'), "yyyy-mm-dd")
If lu_string_functions.nf_IsEmpty(ls_delivery_date) Then Return False

//slh SR25459
ls_type_of_sale = dw_additional_data.GetItemString(1,'type_of_sale')
ls_micro_test_ship_type = ' ' //placeholder
ls_new_or_copy_order = 'N' //New

ls_required_info = ls_customer + '~t' + &
						is_TransMode + '~t' + &
						ls_plant + '~t' + &
						ls_delivery_date + &
						'~t'

LS_TIME = STRING(dw_additional_data.GetItemTime(1,"schd_delv_time"),"HHMM")					
IF lu_string_functions.nf_IsEmpty( ls_Time) OR ls_Time = "0000" Then
	Return FALSE
ELSE
	ls_required_info += ls_Time
END IF

//slh SR25459
ls_required_info += '~t' + ls_type_of_sale + '~t' + &
 								  ls_micro_test_ship_type + '~t' + &
                                      is_micro_test_product_found + '~t' + &
                                      ls_new_or_copy_order

If Not IsValid(iu_orp001) Then
	iu_orp001 = Create u_orp001
End if

lstr_error_info.se_window_name = 'Add.Data'
lstr_error_info.se_event_name = 'wf_calculate_ship_date'
lstr_error_info.se_message = Space(70)

//iu_orp001.nf_orpo18ar(lstr_error_info, &
//							ls_required_info, &
//							ls_ship_date)
							
iu_ws_orp1.nf_orpo18fr(lstr_error_info, &
							ls_required_info, &
							ls_ship_date)							

IF Not lu_string_functions.nf_IsEmpty(ls_ship_date) Then
	dw_additional_data.SetItem(1, 'schd_ship_date', Date(ls_ship_date))
Else
	dw_additional_data.SetColumn('schd_ship_date')
	dw_additional_data.SetFocus()
End IF

return True
end function

public function integer wf_get_deliv_time (integer ai_day, string as_division, string as_customer_id, ref time at_default_delivery_time);Datetime	ldt_Default_Delivery_DateTime

Time		lt_Default_Delivery_Time

string ls_temp

IF Upper(Trim(SQLCA.DBMS)) = 'MSS MICROSOFT SQL SERVER 6.X' THEN

	Choose case ai_day
		Case 1
				Select customer_defaults.sunday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 2
				Select customer_defaults.monday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 3
			// Select Tues default time
			Select customer_defaults.tuesday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 4
			// Select weds default time
			Select customer_defaults.wednesday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
	
		Case 5
			Select customer_defaults.thursday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
	
		Case 6
			Select customer_defaults.friday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 7
			Select customer_defaults.saturday_delv_time
				INTO :ldt_Default_Delivery_DateTime
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
	End Choose
	
	If SQLCA.SQLCode = 100 Then
		Return -1
	End If
	
	at_default_delivery_time = Time(ldt_Default_Delivery_DateTime)
	
ELSE
	Choose case ai_day
		Case 1
				Select customer_defaults.sunday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 2
				Select customer_defaults.monday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 3
			// Select Tues default time
			Select customer_defaults.tuesday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 4
			// Select weds default time
			Select customer_defaults.wednesday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
	
		Case 5
			Select customer_defaults.thursday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
	
		Case 6
			Select customer_defaults.friday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
		Case 7
			Select customer_defaults.saturday_delv_time
				INTO :lt_Default_Delivery_Time
				FROM customer_defaults
				WHERE customer_defaults.sales_division = :as_division AND 
						customer_defaults.customer_id = :as_customer_id
				USING SQLCA ;
	End Choose
	
	If SQLCA.SQLCode = 100 Then
		Return -1
	End If

	at_default_delivery_time = lt_Default_Delivery_Time
	
END IF	

Return 0
end function

event ue_postopen;/***************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/11/2006  SR3802        Modified if loading instructions type = 6, shrink wrap and palletize = 'N',
												  if loading instructions type= '7', shrink wrap and palletize = 'Y'.
**********************************************************************************/

Boolean	lb_NeedShipDate

String	ls_temp, &
			ls_CustomerID,&
			ls_Location, &
			ls_smancode, &
			ls_trans_mode, &
			ls_load_instruction, &
			ls_delv_date_type, &
			ls_input, &
			ls_output_string, &
			ls_division, &
			ls_currency_code, &
			ls_temp_loc, &
			ls_purch_prod_status, &
			ls_type
			
Long		ll_rtn
			
Integer	li_rtn

String		ls_city, &
			ls_state, &
			ls_short_name, &
			ls_type_of_sale, &
			ls_sub_ind
			
Time		lt_Default_Delivery_Time, &
			lt_delivery_time

Date		ld_delivery_date

DataWindowChild	ldwc_temp

iu_ws_orp4 = Create u_ws_orp4
iu_ws_orp1 = Create u_ws_orp1

u_project_functions	lu_project_functions
u_string_functions	lu_string_functions
			
id_Delv_date = Date( lu_string_functions.nf_GetToken(is_openparm, '~t')) 

ls_Temp = lu_string_functions.nf_GetToken(is_openparm, '~t')

If (lu_string_functions.nf_IsEmpty(Trim(ls_temp))) or (ls_temp = '01/01/0001') Then
	lb_NeedShipDate = True
Else
	id_ship_date = Date(ls_temp)
End if

is_plant = lu_string_functions.nf_GetToken(is_openparm, '~t')
ls_CustomerID = lu_string_functions.nf_GetToken(is_openparm, '~t')
is_load_status = lu_string_functions.nf_GetToken(is_openparm, '~t')
is_TransMode = Trim(lu_string_functions.nf_GetToken(is_openparm, '~t'))
is_division = lu_string_functions.nf_GetToken(is_openparm, '~t')
is_div_po = lu_string_functions.nf_GetToken(is_openparm, '~t')
ls_smancode = lu_string_functions.nf_GetToken(is_openparm, '~t')
is_corp_po = lu_string_functions.nf_GetToken(is_openparm, '~t')

dw_additional_data.SetTransObject( SQLCA)

// added division code here, set local variable for retreive
if (is_division = '31') OR (is_division = '32') OR (is_division = '33') &
		OR (is_division = '35')  OR (is_division = '57') then
	ls_division = '31'
else
	ls_division = '11'
end if

If lu_project_functions.nf_isscheduler() or lu_project_functions.nf_isschedulermgr() then
	dw_additional_data.Retrieve(ls_CustomerID, '630', '11')
Else	
	dw_additional_data.Retrieve(ls_CustomerID, Message.is_smanlocation, ls_division)
End if

IF dw_additional_data.RowCount() < 1 Then dw_additional_data.InsertRow(0)

dw_additional_data.SetItem( 1, "customer_id", ls_CustomerID)
dw_additional_data.SetItem( 1, "sman_code", ls_smancode)

//revgll
ls_currency_code = dw_additional_data.getitemstring( 1, "currency_code")
//

SELECT salesman.smanloc  
  INTO :ls_location  
  FROM salesman  
WHERE salesman.smancode = :ls_smancode AND salesman.smantype = 's';

dw_additional_data.SetItem( 1, "sales_location", ls_location)

SELECT customers.purch_prod_status
	INTO :ls_purch_prod_status
	FROM customers
	WHERE customers.customer_id = :ls_CustomerID ;
	
dw_additional_data.SetITem( 1, "purch_prod_status", ls_purch_prod_status)	

dw_additional_data.TriggerEvent( "ue_Default_row")

If NOT lu_string_functions.nf_IsEmpty(is_plant) Then
	dw_additional_data.SetItem( 1, "ship_plant", is_plant) 
//ELSE
//	SELECT customer_defaults.Ship_plant
//	INTO :is_plant
//	FROM customer_defaults
//	WHERE customer_defaults.customer_id = :ls_CustomerID AND customer_defaults.sales_division = :is_division
//	USING SQLCA ;
End if

//rem comment out the defaulting of the ready to combine indicator.
//If message.is_smanlocation = '630' or lu_project_functions.nf_isscheduler() or lu_project_functions.nf_isschedulermgr() Then 
//	dw_additional_data.SetItem(1, 'ready_to_combine', 'N')
//End If	
//
ls_trans_mode = dw_additional_data.GetItemString(1, 'trans_mode')
IF (ls_trans_mode = 'B') or (ls_trans_mode = 'L') then
	dw_additional_data.SetItem(1, 'freight_pymt_plan', 'C')
	dw_additional_data.object.cust_target_date.protect = 0
	dw_additional_data.modify("cust_target_date.background.color = '16777215'")
ELSE
	dw_additional_data.object.cust_target_date.protect = 1
	dw_additional_data.modify("cust_target_date.background.color = '12632256'")
End if

//revgll//
if is_division  =  '57' then
	dw_additional_data.object.freight_rate.protect = 1
	dw_additional_data.modify("freight_rate.background.color = '12632256'")
else
	dw_additional_data.object.freight_rate.protect = 0
	dw_additional_data.modify("freight_rate.background.color = '16777215'")
end if
//

If (dw_additional_data.GetItemString(1, "type_of_sale") = 'T') or (dw_additional_data.GetItemString(1, "auto_assign") = 'Y') Then
	dw_additional_data.object.product_state.visible = 1
	dw_additional_data.object.product_state_t.visible = 1	
else
	dw_additional_data.object.product_state.visible = 0
	dw_additional_data.object.product_state_t.visible = 0	
End If

If (dw_additional_data.GetItemString(1, "type_of_sale") = 'T') Then
	dw_additional_data.object.purch_prod_status.protect = 0
	dw_additional_data.modify("purch_prod_status.background.color = '16777215'")	
//	dw_additional_data.object.t_26.visible = 1
else
	dw_additional_data.object.purch_prod_status.protect = 1
	dw_additional_data.modify("purch_prod_status.background.color = '12632256'")	
//	dw_additional_data.object.t_26.visible = 0
End If

Message.StringParm = ls_customerID
//If lb_NeedShipDate  Then
//	dw_additional_data.TriggerEvent("ue_CalculateShipDate")
//End if

ls_temp = iw_frame.wf_GetMicroHelp()
If Trim(Upper(ls_temp)) <> 'READY' Then
	is_microhelp = ls_temp + is_microhelp
End IF

istr_error_info.se_event_name = "ue_postopen"

if ib_IsFromCustomerOrder then
	//ignore plant from ue_postopen, orpo98br will assign
	setnull(is_plant)
end if
	
if NOT lu_string_functions.nf_IsEmpty(is_plant) then
	ls_input = 	ls_CustomerID + '~t' + 'S' + '~t' + is_division + '~t' + is_plant + '~t' + String(dw_additional_data.getitemdate(1,"schd_delv_date")) + '~t' + ls_currency_code + '~r~n' 
else
	ls_input = 	ls_CustomerID + '~t' + 'S' + '~t' + is_division + '~t' + '   ' + '~t' + String(dw_additional_data.getitemdate(1,"schd_delv_date")) + '~t' + ls_currency_code +'~r~n' 
end if
				
li_rtn = iu_ws_orp4.nf_orpo98fr(istr_error_info, ls_input, ls_output_string)
if li_rtn = 0 then
	dw_load_instruction.setitem(1, "load_instructions", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
	dw_load_instruction.AcceptText()
	ls_temp = dw_load_instruction.GetitemString(1, "load_instructions")

	//SR#26379
	SELECT TYPE_CODE
	INTO :ls_type
	FROM TUTLTYPES
	WHERE RECORD_TYPE = 'LDINPALL'
		AND TYPE_CODE = :ls_temp
		USING SQLCA;
			
	if sqlca.sqlcode = 0 then
		dw_additional_data.setitem(1,'palletize','Y')
	else					
	 	//not found
		dw_additional_data.setitem(1,'palletize','N')
	end if
			
	SELECT TYPE_CODE
	INTO :ls_type
	FROM TUTLTYPES
	WHERE RECORD_TYPE = 'LDINSHRK'
		AND TYPE_CODE = :ls_temp
		USING SQLCA;
			
	if sqlca.sqlcode = 0 then
		dw_additional_data.setitem(1,'shrink_wrap','Y')
	else					
	 	//not found
		dw_additional_data.setitem(1,'shrink_wrap','N')
	end if	
		
//	choose case ls_temp
//		case '1','2','5','7' //,'7' Added for SR3802
//			dw_additional_data.setitem(1,'palletize','Y')
//			dw_additional_data.setitem(1,'shrink_wrap','Y')
//		case else
//			dw_additional_data.setitem(1,'palletize','N')
//			dw_additional_data.setitem(1,'shrink_wrap','N')
//	end choose
	
	
	
	dw_additional_data.setitem(1, "ship_plant", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
	dw_additional_data.setitem(1, "schd_delv_date_type", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
	dw_additional_data.setitem(1, "cust_target_date", date(lu_string_functions.nf_gettoken(ls_output_string, '~t')))
	dw_additional_data.setitem(1, "gtl_split_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
	dw_additional_data.setitem(1, "freight_rate", dec(lu_string_functions.nf_gettoken(ls_output_string, '~t')))
	//dmk sr#5571
	dw_exp_country.setitem(1, "dom_export_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
	dw_exp_country.AcceptText()
	ls_temp = dw_exp_country.GetitemString(1, "dom_export_ind")
	choose case ls_temp
		case 'Y'
			dw_exp_country.Visible = True
		case else
			dw_exp_country.Visible = False
			dw_exp_country.TabOrder = 0
	end choose
	dw_store_door_deck_code.SetItem(1,"store_door_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
	dw_store_door_deck_code.SetItem(1,"deck_code", lu_string_functions.nf_gettoken(ls_output_string, '~t'))	
			
end if

If NOT lu_string_functions.nf_IsEmpty(is_plant) Then
	dw_additional_data.SetItem( 1, "ship_plant", is_plant) 
end if

li_rtn = dw_dept_code.GetChild('dept_code', ldwc_temp)
ldwc_temp.Reset()
ls_output_string = lu_string_functions.nf_righttrim(ls_output_string, TRUE, TRUE)
ll_rtn = ldwc_temp.ImportString('          ' + '~r~n' + ls_output_string)

If lb_NeedShipDate  Then
	dw_additional_data.TriggerEvent("ue_CalculateShipDate")
End if

//dw_load_instruction.enabled = false

//
if is_openparm <> '' then
	dw_additional_data.reset()
	dw_additional_data.importstring(is_openparm)
	if lu_project_functions.nf_get_additional_datal( ls_Customerid, &
									'S', is_division, ld_delivery_date, ls_city, ls_state, &
									ls_short_name, lt_delivery_time, ls_type_of_sale, ls_sub_ind) then
		dw_additional_data.SetItem( 1, "customers_state", ls_state)
		dw_additional_data.SetItem( 1, "customers_city", ls_city)
		dw_additional_data.SetItem( 1, "customers_short_name", ls_short_name)
	end if
	if  dw_additional_data.getitemtime( 1, "schd_delv_time") =  Time('00:00:00') then
		is_microhelp = "No default time found. Please Enter a Time."
	else
		is_microhelp = "Ready"
	end if
end if

iw_frame.SetMicroHelp(is_microhelp)
end event

event closequery;call super::closequery;//Date	ldt_temp
//Long	ll_x_pos
//
//Choose Case Mid(Message.StringParm, Pos(Message.StringParm, '~x') + 1)
//Case 'cb_cancel', ''
//	return
//End Choose
//
//// Make sure all required valies are filled in
//ldt_temp = dw_cust-to-sales_additional-data.GetItemDate(1, 'delivery_date')
//If ldt_temp < Today() or IsNull(ldt_temp) Then
//	iw_Frame.SetMicroHelp("Delivery Date is a required field and must be greater than or equal to today.")
//	dw_cust-to-sales_additional-data.SetColumn('delivery_date')
//	dw_cust-to-sales_additional-data.SetFocus()
//	Message.ReturnValue = 1
//	return
//End if
//
//ll_x_pos = Pos(Message.StringParm, "~x")
//IF Mid(Message.StringParm, ll_x_pos+1) <> "cb_pa" Then
//	If dw_cust-to-sales_additional-data.GetItemString(1, 'optimize_plant') = 'N' Then
//		ldt_temp = dw_cust-to-sales_additional-data.GetItemDate(1, 'ship_date')
//		If ldt_temp < Today() or IsNull(ldt_Temp) Then
//			iw_Frame.SetMicroHelp("Ship Date is a required field and must be greater than or equal to today.")
//			dw_cust-to-sales_additional-data.SetColumn('ship_date')
//			dw_cust-to-sales_additional-data.SetFocus()
//			Message.ReturnValue = 1
//			return
//		End if
//
//		If f_IsEmpty(dw_cust-to-sales_additional-data.GetItemString(1, 'plant')) Then
//				iw_Frame.SetMicroHelp("Plant is a required field")
//			dw_cust-to-sales_additional-data.SetColumn('plant')
//			dw_cust-to-sales_additional-data.SetFocus()
//	 		Message.ReturnValue = 1
//			return
//		End if
//	END IF
//End if
//
//If f_IsEmpty(dw_cust-to-sales_additional-data.GetItemString(1, 'customer_id')) Then
//	iw_Frame.SetMicroHelp("Customer is a required field")
//	dw_cust-to-sales_additional-data.SetColumn('customer_id')
//	dw_cust-to-sales_additional-data.SetFocus()
//	Message.ReturnValue = 1
//	return
//End if
//
//	
end event

event open;call super::open;String	ls_indicator

u_string_functions	lu_string_functions


iw_parent = Message.PowerObjectParm
iw_parent.DYNAMIC event ue_get_data("additonal_data_input")
is_OpenParm = Message.StringParm

ls_indicator = lu_string_functions.nf_GetToken(is_openparm, '~t')
ib_IsFromPAInquire = False

if upper(iw_parent.title) = 'PRODUCT MOVE TO NEW ORDER' then
	iw_parent.DYNAMIC event ue_get_data("pricing_default_hdr")
	is_pricing_default_hdr = Message.StringParm
	ib_move_rows = true
else
	ib_move_rows = false
end if

//SLH SR25459
iw_parent.DYNAMIC event ue_get_data("micro_test_product")
is_micro_test_product_found = Message.StringParm

iu_orp204 = Create u_orp204

//Layout of is_OpenParm tab seperated string
// Indicator of window opening - R reservation, C customer order, P pa inquiry
// Delivery Date
// Ship_date
// plant
// Customer id
// Load status
// Tran Mode
// Division
// Divisional PO

If lu_string_functions.nf_IsEmpty(is_OpenParm) Then
	//Start changes for HD0000000636867
	//iw_Frame.SetMicroHelp("Invalid Data passed to Additional Data Popup.  Contact Helpdesk at x3133")
	iw_Frame.SetMicroHelp("Invalid Data passed to Additional Data Popup.  Contact Helpdesk at "+gs_helpdesk_no )
	//End changes for HD0000000636867
	Close(This)
	return
End if
is_business_rule = 'UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU'
Choose Case ls_indicator
Case 'R'
	ib_IsFromReservation = True
	cb_browse.Visible = False
	// MODIFY THIS STRING IF THERE IS OTHER FIELDS THAT SHOULD NOT
	// BE CHANGED BY THE USER
	is_business_rule = 'UUVUUUUVVUUUUUUUUUUUUUUVUUUUUUUUUUUV'
Case 'C'
	ib_IsFromCustomerOrder = True
CASE 'P'
	ib_IsFromPAInquire = True
	is_business_rule = 'UUVUUUUVVUUUUUUUUUUUUUUUUUUUUUUUUUUV'
	cb_browse.Visible = False
End Choose
end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
If IsValid(iu_ws_orp3) Then Destroy iu_ws_orp3

DESTROY iu_ws_orp4
Destroy iu_ws_orp1
end event

on w_additional_data.create
int iCurrent
call super::create
this.dw_instruction_ind=create dw_instruction_ind
this.dw_load_instruction=create dw_load_instruction
this.dw_additional_data=create dw_additional_data
this.dw_dept_code=create dw_dept_code
this.dw_exp_country=create dw_exp_country
this.dw_store_door_deck_code=create dw_store_door_deck_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_instruction_ind
this.Control[iCurrent+2]=this.dw_load_instruction
this.Control[iCurrent+3]=this.dw_additional_data
this.Control[iCurrent+4]=this.dw_dept_code
this.Control[iCurrent+5]=this.dw_exp_country
this.Control[iCurrent+6]=this.dw_store_door_deck_code
end on

on w_additional_data.destroy
call super::destroy
destroy(this.dw_instruction_ind)
destroy(this.dw_load_instruction)
destroy(this.dw_additional_data)
destroy(this.dw_dept_code)
destroy(this.dw_exp_country)
destroy(this.dw_store_door_deck_code)
end on

event ue_base_ok;Time				lt_time

DataStore		lds_Temp

String			ls_ReturnString, &
					ls_text, &
					ls_temp, ls_temp1, &
					ls_type_of_order
					
integer li_rtn					

Long				ll_rowcount	

Decimal			ldc_freight_rate
					
u_string_functions	lu_string_functions					

dw_additional_data.AcceptText()

IF dw_additional_data.GetItemString(1, 'auto_assign') = 'N' Then
	ls_text = String(dw_additional_data.GetItemString(1, 'ship_plant'))
	If lu_string_functions.nf_IsEmpty(ls_text) then
		iw_frame.SetMicroHelp('Ship Plant is a required field.')
		dw_additional_data.SetColumn('ship_plant')
		Return 
	End If

	ls_text = String(dw_additional_data.GetItemDate(1, 'schd_ship_date'))
	If lu_string_functions.nf_IsEmpty(ls_text) Then
		iw_frame.SetMicroHelp('Ship Date is a required field.')
		dw_additional_data.SetColumn('schd_ship_date')
		Return 	
	End If
End IF

ls_text = String(dw_additional_data.GetItemDate(1, 'schd_delv_date'))
If lu_string_functions.nf_IsEmpty(ls_text) Then
	iw_frame.SetMicroHelp('Delivery Date is a required field.')
	dw_additional_data.SetColumn('schd_delv_date')
	Return 
End If

lt_time = dw_additional_data.GetItemTime(1, 'schd_delv_time')
If lt_time = Time('00:00:00') Then
	iw_frame.SetMicroHelp('Delivery Time is a required field.')
	dw_additional_data.SetColumn('schd_delv_time')
	Return 
End If

ls_type_of_order = dw_additional_data.GetItemString(1, 'type_of_order')
If lu_string_functions.nf_IsEmpty(ls_type_of_order) Then
	iw_frame.SetMicroHelp('Type of order is a required field.')
	dw_additional_data.SetColumn('type_of_order')
	Return
End If


//ldc_freight_rate = dw_additional_data.GetItemNumber(1, 'freight_rate') 
//If (is_division = '57') and (ldc_freight_rate <= 0.0000) then
//	iw_frame.SetMicroHelp('Freight rate is required for division 57')
//	dw_additional_data.SetColumn('freight_rate')
//	Return 
//End If	

ll_rowcount = dw_additional_data.Rowcount()

DO UNTIL ll_rowcount = 1
	dw_additional_data.DeleteRow(ll_rowcount)
	ll_rowcount = dw_additional_data.Rowcount()
LOOP

lds_Temp = Create DataStore
lds_Temp.DataObject = 'd_additional_data_rpc'
//lds_temp.Object.Data = dw_additional_data.Object.Data

lds_temp.ImportString(dw_additional_data.describe("DataWindow.Data")) 

ls_ReturnString  = lds_temp.Describe("DataWindow.Data") + "~t" &
	+ dw_instruction_ind.Describe("DataWindow.Data") &
	+ "~x"+ cb_base_ok.ClassName()
ls_temp = dw_load_instruction.getitemstring(1,1)
iw_parent.dynamic event ue_set_data("load_instruction", ls_temp)
ls_temp = dw_dept_code.GetItemString(1,1)
if isnull(ls_temp) then ls_temp = '          '
iw_parent.dynamic event ue_set_data("dept_code", ls_temp)
ls_temp = dw_load_instruction.getitemstring(1,1)

iw_parent.dynamic event ue_set_data("additonal_data_input",ls_ReturnString)
///
//ls_temp = string(dw_additional_data.getitemdate(1,"cust_target_date"))
//iw_parent.dynamic event ue_set_data("cust_target_date",ls_temp)

//ls_temp = dw_additional_data.getitemstring(1,"gtl_split_ind")
//iw_parent.dynamic event ue_set_data("gtl_split_ind",ls_temp)
//dmk sr5571
ls_temp = dw_exp_country.getitemstring(1,1)
if isnull(ls_temp) then ls_temp = '   '
iw_parent.dynamic event ue_set_data("export_country", ls_temp)

iw_parent.dynamic event ue_set_data("store_door", dw_store_door_deck_code.GetItemString(1, "store_door_ind"))
iw_parent.dynamic event ue_set_data("deck_code", dw_store_door_deck_code.GetItemString(1, "deck_code"))

Destroy lds_temp
Close(This)











end event

event ue_base_cancel;String	ls_Return_String
ls_Return_String	= "Ignore This Data" + "~xCancel"
iw_parent.dynamic event ue_set_data("additonal_data_input",ls_Return_String)
Close(This)


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_additional_data
integer x = 1422
integer y = 1260
integer taborder = 80
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_additional_data
integer x = 1143
integer y = 1260
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_additional_data
integer x = 859
integer y = 1260
integer width = 274
integer taborder = 60
string text = "&Gen S/O"
end type

type cb_browse from w_base_response_ext`cb_browse within w_additional_data
boolean visible = true
integer x = 1701
integer y = 1260
integer width = 315
integer taborder = 90
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = "&Res Browse"
end type

event cb_browse::clicked;DataStore	lds_Temp
String 		ls_ReturnString, &
				ls_text, &
				ls_temp
long			ll_rowcount				
u_string_functions	lu_string_functions				

dw_additional_data.AcceptText()

ls_text = String(dw_additional_data.GetItemDate(1, 'schd_ship_date'))
If lu_string_functions.nf_IsEmpty(ls_text) Then
	iw_frame.SetMicroHelp('Ship Date is a required field.')
	dw_additional_data.SetColumn('schd_ship_date')
	Return 	
End If

ll_rowcount = dw_additional_data.rowcount()
do while ll_rowcount > 1 
	dw_additional_data.deleterow(2)
	ll_rowcount = dw_additional_data.rowcount()
loop

ls_temp = dw_load_instruction.getitemstring(1,1)
iw_parent.dynamic event ue_set_data("load_instruction", ls_temp)

lds_Temp = Create DataStore
lds_Temp.DataObject = 'd_additional_data_rpc'
//lds_temp.Object.Data = dw_additional_data.Object.Datawindow.data

lds_temp.importstring(dw_additional_data.Object.DataWindow.Data)
//lds_temp.setitem(1,"schd_ship_date",ls_text)
ls_ReturnString  = lds_temp.Object.DataWindow.Data + "~t" &
	+ dw_instruction_ind.Describe("DataWindow.Data") + "~x"+ This.ClassName()
Destroy lds_temp
iw_parent.dynamic event ue_set_data("additonal_data_input",ls_ReturnString)
//dmk sr5571
ls_temp = dw_exp_country.getitemstring(1,1)
iw_parent.dynamic event ue_set_data("export_country", ls_temp)


Close(Parent)
end event

type dw_instruction_ind from u_base_dw_ext within w_additional_data
boolean visible = false
integer x = 1591
integer y = 516
integer width = 521
integer height = 68
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_instruction_ind"
boolean border = false
end type

type dw_load_instruction from u_base_dw_ext within w_additional_data
integer x = 2153
integer y = 308
integer width = 681
integer height = 72
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_load_instruction"
boolean border = false
end type

event itemchanged;call super::itemchanged;/***************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/11/2006      SR3802           Modified if loading instructions type = 6, shrink wrap and palletize = 'N',                           		if loading instructions type= '7', shrink wrap and palletize = 'Y'.
**********************************************************************************/
String ls_type

//this is the change ibdkcjr
//choose case dwo.name
//	case "load_instructions"
//		choose case data
//			case '1','2','5','7' //,'7' Added for SR3802
//				dw_additional_data.setitem(1,'palletize','Y')
//				dw_additional_data.setitem(1,'shrink_wrap','Y')
//			case else
//				dw_additional_data.setitem(1,'palletize','N')
//				dw_additional_data.setitem(1,'shrink_wrap','N')
//		end choose
//end choose

//SR#26379
choose case dwo.name
	case "load_instructions"
			//SR#26379
			SELECT TYPE_CODE
			INTO :ls_type
			FROM TUTLTYPES
			WHERE RECORD_TYPE = 'LDINPALL'
				AND TYPE_CODE = :data
				USING SQLCA;
			
			if sqlca.sqlcode = 0 then
				dw_additional_data.setitem(1,'palletize','Y')
			else					
			 	//not found
				dw_additional_data.setitem(1,'palletize','N')
			end if
			
			SELECT TYPE_CODE
			INTO :ls_type
			FROM TUTLTYPES
			WHERE RECORD_TYPE = 'LDINSHRK'
			AND TYPE_CODE = :data
			USING SQLCA;
			
			if sqlca.sqlcode = 0 then
				dw_additional_data.setitem(1,'shrink_wrap','Y')
			else					
			 	//not found
				dw_additional_data.setitem(1,'shrink_wrap','N')
			end if
	end choose
			//
end event

event constructor;call super::constructor;DataWindowChild			ldwc_temp

this.insertrow(0)
dw_load_instruction.GetChild('load_instructions', ldwc_temp)

IF ldwc_temp.RowCount()	= 0 THEN 
	ldwc_temp.SetTransObject(SQLCA)
	ldwc_temp.Retrieve()
end if


end event

type dw_additional_data from u_base_dw_ext within w_additional_data
event ue_calculateshipdate ( )
event ue_default_row ( )
integer x = 14
integer width = 2889
integer height = 1224
integer taborder = 100
string dataobject = "d_additional_data"
boolean border = false
end type

event ue_calculateshipdate;call super::ue_calculateshipdate;If Not ib_IsFromReservation Then
	wf_CalculateShipDate()
	SetMicroHelp( "Ready")
End if
end event

event ue_default_row();Integer	li_rtn

String	ls_Customer_id, &
			ls_micromessage, &
			ls_temp, &
			ls_division, &
			ls_city, &
			ls_state, &
			ls_short_name, &
			ls_business_rules, &
			ls_type_of_sale, &
			ls_sub_ind, &
			ls_salesman, &
			ls_division_header, &
			ls_pricing_default
			
Time		lt_Default_Delivery_Time, &
			lt_delivery_time

Date		ld_delivery_date

u_string_functions	lu_string_functions
u_project_functions	lu_project_functions

dw_instruction_ind.Visible = TRUE

ls_division = is_division

This.SetItem( 1, "div_po_id", is_div_po) 
This.SetItem( 1, "corp_po_id", is_corp_po) 
This.SetItem(1, 'business_rules', is_business_rule)
This.SetItem( 1, "load_status", is_Load_status)
This.SetItem( 1, "schd_delv_date", id_Delv_date)
This.SetItem( 1, "trans_mode", is_TransMode)
This.SetItem( 1, "sales_division", ls_division)
This.SetItem(1, 'business_rules', is_business_rule)

li_rtn = 0
iw_frame.SetMicroHelp('READY')

ls_Customer_id = This.GetItemString( 1, "customer_id")

IF lu_string_functions.nf_IsEmpty(dw_Additional_data.GetItemString( 1, "sman_code")) Then
	dw_Additional_data.SetItem( 1, "sman_code", message.is_salesperson_code)
	dw_Additional_data.SetItem( 1, "sales_location", message.is_smanlocation)
END IF

if ib_IsFromCustomerOrder then
	if id_ship_date > date(01/01/1900) then
		This.SetItem( 1, 'schd_ship_date', id_ship_date)	
	end if
end if

If ib_IsFromReservation or ib_IsFromPAInquire Then
	This.SetItem( 1, "ship_plant", is_plant)
	This.SetItem( 1, 'schd_ship_date', id_ship_date)
Else
	IF lu_string_functions.nf_IsEmpty(This.GetItemString( 1, "ship_plant")) Then
		Select customer_defaults.ship_plant
			INTO :is_Plant
			FROM customer_defaults
 			WHERE customer_defaults.sales_division = :ls_division AND 
					customer_defaults.customer_id = :ls_Customer_id
			USING SQLCA ;
			if SQLCA.SQLCode = 100 then
				ls_division = '11'
				Select customer_defaults.ship_plant
					INTO :is_Plant
					FROM customer_defaults
 					WHERE customer_defaults.sales_division = :ls_division AND 
							customer_defaults.customer_id = :ls_Customer_id
					USING SQLCA ;
					if SQLCA.SQLCode = 100 then
						li_rtn = -1
						ls_micromessage = "  No Division '"+ls_division+ &
							"' Service Center Data available for Customer ID '"+ &
							ls_Customer_id+"'."
					elseif SQLCA.SQLCode > 0 then
						MessageBox("Database Error", &
						SQLCA.SQLErrText, Exclamation!)
					End If
				End If
			This.SetItem( 1, "ship_plant", is_plant)
	END IF
//	wf_CalculateShipDate()  wait until plant assigned in ue_poston for window.
End IF

if ib_IsFromReservation then
	if not ib_move_rows then
		ls_business_rules = this.GetItemString(1,"business_rules")
		if lu_project_functions.nf_plt_based_on_delivery(is_plant) then
			// protect delivery date, unprotect ship date unless buyer
			if this.GetITemString(1,"trans_mode") = 'B' then
				ls_business_rules = Replace(ls_business_rules,14,1, "U")
				ls_business_rules = Replace(ls_business_rules,24,1, "V")
			else
				ls_business_rules = Replace(ls_business_rules,14,1, "V")
				ls_business_rules = Replace(ls_business_rules,24,1, "V") // Protect ship date also // 
			end if
		else
			// protect ship date, unprotect delivery
			ls_business_rules = Replace(ls_business_rules,14,1, "U")
			ls_business_rules = Replace(ls_business_rules,24,1, "V")
		end if
		this.SetItem(1, "business_rules", ls_business_rules)
	end if
end if		


if lu_string_functions.nf_isempty(this.GetItemString( 1, "customers_city")) and &
			 lu_string_functions.nf_isempty(this.GetItemString( 1, "customers_state")) then
	ld_delivery_date = this.GetItemDate( 1, "schd_delv_date") 
	SetPointer(HourGlass!)
	if lu_project_functions.nf_get_additional_datal( ls_Customer_id, &
									'S', is_division, ld_delivery_date, ls_city, ls_state, &
									ls_short_name, lt_delivery_time, ls_type_of_sale, ls_sub_ind) then
		this.SetItem( 1, "customers_state", ls_state)
		this.SetItem( 1, "customers_city", ls_city)
		this.SetItem( 1, "customers_short_name", ls_short_name)
		this.SetItem( 1, "type_of_sale", ls_type_of_sale)
		this.SetItem( 1, "sub", ls_sub_ind)
		lt_Default_Delivery_Time = lt_delivery_time
	end if
	SetPointer(Arrow!)
else
// getting delivery time if not found by cust or div then is retried again with div 11
	If wf_get_deliv_time(DayNumber(This.GetItemDate(1,"schd_delv_date")), &
					ls_division, ls_customer_id, lt_Default_Delivery_Time) = -1 Then
					
		wf_get_deliv_time(DayNumber(This.GetItemDate(1,"schd_delv_date")), &
					"11", ls_customer_id, lt_Default_Delivery_Time)
	End If
end if
This.SetItem(1, "schd_delv_time", lt_Default_Delivery_Time)

ls_division_header = This.GetItemString(1, "sales_division")

If ib_move_rows Then

	This.SetItem(1, "type_of_order", is_pricing_default_hdr)

Else
	
	If (ls_division_header = '99') Then
		ls_salesman = This.GetItemSTring(1, "sman_code")
		SELECT smandiv 
		INTO   :ls_division_header
		FROM   salesman
		WHERE  salesman.smancode = :ls_salesman
		  AND	 salesman.smantype = 'S'; 
	End If
	
	If (ls_division_header = '05') OR (ls_division_header = '11') OR (ls_division_header = '31') OR &
		(ls_division_header = '32') OR (ls_division_header = '57') Then
		If This.GetItemString(1, "type_of_sale") = 'T' Then								
			This.SetItem(1, "type_of_order", 'N')
		Else
			This.SetItem(1, "type_of_order", ' ')
		End If
	Else
		This.SetItem(1, "type_of_order", 'N')
	End IF
	
End If		

ls_temp = iw_frame.wf_getmicrohelp()
// li_rtn = -1 is a not found 0 says there data
IF li_rtn = 0 Then 
	If lt_Default_Delivery_Time = Time('00:00:00') then
		IF Trim(ls_temp) <> 'READY' Then
			is_microhelp = ls_temp + "  No default time found. Please Enter a Time."
		ELSE
			is_microhelp = "No default time found. Please Enter a Time."
		End IF
	End If
Else
	IF Trim(ls_temp) <> 'READY' Then
		ls_micromessage = ls_temp + ls_micromessage
	End IF
	is_microhelp = ls_micromessage
End If
end event

event ue_dwndropdown;DataWindowChild	ldwc_Temp
String 	ls_ColumnName,&
			ls_Location,&
			ls_Filter_String
			
u_string_functions	lu_string_function
			
ls_ColumnName = This.GetColumnName()

Choose Case ls_ColumnName
	Case 'customer_id','sman_code'
		This.GetChild( ls_ColumnName,ldwc_temp)
		IF ldwc_Temp.RowCount() < 2 Then
			ls_Location = message.nf_GetLocation()
			ldwc_temp.SetTransObject( SQLCA)
			ldwc_Temp.Retrieve( ls_Location, ls_Location)
		END IF
	Case 'ship_plant'
		IF Not ib_Filtered Then
			This.GetChild( ls_ColumnName,ldwc_temp)
			Choose Case is_Division
				Case '11'
					ls_Filter_String = "location_type = 'P' OR location_type = 'Y' OR location_type = 'X' OR location_type = 'W' OR location_type = 'F' OR location_type = 'Z'"
				Case '31'
					ls_Filter_String = "location_type = 'M' OR location_type = 'Y' OR location_type = 'X' OR location_type = 'W' OR location_type = 'F' OR location_type = 'Z'"
				Case Else
					ls_Filter_String = ''
			END CHOOSE
			ldwc_Temp.SetFilter( ls_Filter_String)
			ldwc_Temp.Filter()
			ib_Filtered = TRUE
		END IF
END CHoose
end event

event itemchanged;call super::itemchanged;/***************************************************************************************
ModifiedBy 	Date       	SR/TicketNo.  Description 
----------------------------------------------------------------------------------------
Kuppusamyc 	08/11/2006  SR3802        Modified if loading instructions type = 6, shrink wrap and palletize = 'N',
												  if loading instructions type= '7', shrink wrap and palletize = 'Y'.
**********************************************************************************/

Date		ldt_ship_date

INTEGER	li_Rtn

STRING	ls_text, &
			ls_parameter_info, &
			ls_Business_Rules, &
			ls_parameter, &
			ls_para_tmp, &
			ls_input, &
			ls_output_string, &
			ls_temp, &
			ls_new_cust, &
			ls_purch_prod_status, &
			ls_type_of_sale, &
			ls_type

Long	ll_row

u_string_functions	lu_string_functions

S_Error	lstr_Error_Info
data = TRIM(data)
ls_text = data

Choose Case dwo.Name
	Case 'div_po_id'
		is_div_po = ls_text
	Case 'corp_po_id'
		is_corp_po = ls_text
	Case 'schd_ship_date'
		ldt_ship_date = Date(ls_text)
		If ldt_ship_date < Today() Then
			iw_frame.SetMicroHelp('Ship Date must be greater than today.')
			Return 1
		End if
	Case 'schd_delv_date'
		id_Delv_date = Date(ls_text)
		IF Not ib_IsFromReservation Then
			wf_calculateshipdate()
		End IF
	Case 'ship_plant'
		IF Not ib_IsFromReservation Then
			wf_calculateshipdate()
		end if	
		ls_input = 	dw_additional_data.GetItemString(1,"customer_id") + &
		'~t' + 'S' + '~t' + is_division + '~t' + dw_additional_data.GetItemString(1,"ship_plant") + '~r~n' 
		li_rtn =iu_ws_orp4.nf_ORPO98FR(istr_error_info, ls_input, ls_output_string) 
		if li_rtn = 0 then
			dw_load_instruction.setitem(1, "load_instructions", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
			dw_load_instruction.AcceptText()
			ls_temp = dw_load_instruction.GetitemString(1, "load_instructions")
			
			//SR#26379
			SELECT TYPE_CODE
			INTO :ls_type
			FROM TUTLTYPES
			WHERE RECORD_TYPE = 'LDINPALL'
			AND TYPE_CODE = :ls_temp
			USING SQLCA;
			
			if sqlca.sqlcode = 0 then
				dw_additional_data.setitem(1,'palletize','Y')
			else					
			 	//not found
				dw_additional_data.setitem(1,'palletize','N')
			end if
			
			SELECT TYPE_CODE
			INTO :ls_type
			FROM TUTLTYPES
			WHERE RECORD_TYPE = 'LDINSHRK'
			AND TYPE_CODE = :ls_temp
			USING SQLCA;
			
			if sqlca.sqlcode = 0 then
				dw_additional_data.setitem(1,'shrink_wrap','Y')
			else					
			 	//not found
				dw_additional_data.setitem(1,'shrink_wrap','N')
			end if
			//			
//			choose case ls_temp
//				case '1','2','5','7' //,'7' Added for SR3802
//					dw_additional_data.setitem(1,'palletize','Y')
//					dw_additional_data.setitem(1,'shrink_wrap','Y')
//				case else
//					dw_additional_data.setitem(1,'palletize','N')
//					dw_additional_data.setitem(1,'shrink_wrap','N')
//			end choose

			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip out plant //
			dw_additional_data.setitem(1, "schd_delv_date_type", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
		//dmk sr#5571
			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip out delivery date //
			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip gtl split //
			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip out freight rate //
			dw_exp_country.setitem(1, "dom_export_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
			dw_exp_country.AcceptText()
			ls_temp = dw_exp_country.GetitemString(1, "dom_export_ind")
			choose case ls_temp
				case 'Y'
					dw_exp_country.Visible = True
				case else
					dw_exp_country.Visible = False
					dw_exp_country.TabOrder = 0
			end choose
			dw_store_door_deck_code.SetItem(1,"store_door_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
			dw_store_door_deck_code.SetItem(1,"deck_code", lu_string_functions.nf_gettoken(ls_output_string, '~t'))	
		end if			
	Case 'customer_id'
		This.SetRedraw(False)
		ll_row = dw_additional_data.Retrieve(ls_text, Message.is_smanlocation, is_division)
		IF ll_Row < 1 Then 
			This.InsertRow(0)
			This.SetItem(1, 'customer_id', ls_text)
		END IF
		this.PostEvent("ue_default_row")
		If lu_string_functions.nf_IsEmpty(is_plant) Then
			ls_input = 	this.GetItemString(1,"customer_id") + &
			'~t' + 'S' + '~t' + is_division + '~t' + '   ' + '~r~n' 
		else
			ls_input = 	this.GetItemString(1,"customer_id") + &
			'~t' + 'S' + '~t' + is_division + '~t' + is_plant + '~r~n' 
		end if	
		li_rtn =iu_ws_orp4.nf_ORPO98FR(istr_error_info, ls_input, ls_output_string) 
		
		if li_rtn = 0 then
			dw_load_instruction.setitem(1, "load_instructions", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
			dw_load_instruction.AcceptText()
			ls_temp = dw_load_instruction.GetitemString(1, "load_instructions")
			choose case ls_temp
				case '1','2','5','7' //,'7' Added for SR3802
					dw_additional_data.setitem(1,'palletize','Y')
					dw_additional_data.setitem(1,'shrink_wrap','Y')
				case else
					dw_additional_data.setitem(1,'palletize','N')
					dw_additional_data.setitem(1,'shrink_wrap','N')
			end choose
			
			If lu_string_functions.nf_IsEmpty(is_plant) Then
				dw_additional_data.SetItem( 1, "ship_plant", lu_string_functions.nf_gettoken(ls_output_string, '~t')) 
			ELSE
				ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip out plant //
			END IF
			dw_additional_data.setitem(1, "schd_delv_date_type", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
		//dmk sr#5571
			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip out delivery date //
			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip gtl split //
			ls_temp  = lu_string_functions.nf_gettoken(ls_output_string, '~t') //strip out freight rate //
			dw_exp_country.setitem(1, "dom_export_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
			dw_exp_country.AcceptText()
			ls_temp = dw_exp_country.GetitemString(1, "dom_export_ind")
			choose case ls_temp
				case 'Y'
					dw_exp_country.Visible = True
				case else
					dw_exp_country.Visible = False
					dw_exp_country.TabOrder = 0
			end choose
			dw_store_door_deck_code.SetItem(1,"store_door_ind", lu_string_functions.nf_gettoken(ls_output_string, '~t'))
			dw_store_door_deck_code.SetItem(1,"deck_code", lu_string_functions.nf_gettoken(ls_output_string, '~t'))	
		end if
		
		ls_new_cust = this.GetItemString(1,"customer_id")
		SELECT customers.purch_prod_status
		INTO :ls_purch_prod_status
		FROM customers
		WHERE customers.customer_id = :ls_new_cust ;
		
		This.SetITem( 1, "purch_prod_status", ls_purch_prod_status)	
		
		SELECT customer_defaults.type_of_sale 
		INTO :ls_type_of_sale
		FROM customer_defaults
		WHERE customer_defaults.customer_id = :ls_new_cust ;
		
		This.SetItem( 1, "type_of_sale", ls_type_of_sale)
		
		If ib_IsFromReservation AND (ls_purch_prod_status = 'M') and Not ib_move_rows Then
			MessageBox('Customer Error', 'Customers set as managed distribution cannot be associated with a reservation.')
			This.SetRedraw(True)
			Return 1		
		End IF
			
		If (This.GetItemString(1, "type_of_sale") = 'T') or (This.GetItemString(1, "auto_assign") = 	'Y') Then
			This.object.product_state.visible = 1
			This.object.product_state_t.visible = 1
		Else
			This.object.product_state.visible = 0
			This.object.product_state_t.visible = 0			
		End If
		
		If (This.GetItemString(1, "type_of_sale") = 'T') Then
			This.object.purch_prod_status.protect = 0
		//	This.object.purch_prod_status.enable = true
			This.modify("purch_prod_status.background.color = '16777215'")	
		//	This.object.t_26.visible = 1
		else
			This.object.purch_prod_status.protect = 1
	//	This.object.purch_prod_status.enable = false
			This.modify("purch_prod_status.background.color = '12632256'")	
		//	This.object.t_26.visible = 0
		End If
		
		
		This.SetRedraw(True)
	CASE 'auto_assign'
			IF ls_text = 'Y' Then
				dw_additional_data.object.product_state.visible = 1
				dw_additional_data.object.product_state_t.visible = 1				
				
				IF Not IsValid( iu_ws_orp3) Then iu_ws_orp3 = Create u_ws_orp3
				li_Rtn = iu_ws_orp3.uf_orpo52fr(Lstr_error_info, "I", ls_parameter_info)
				lu_string_functions.nf_parseleftright(ls_parameter_info, '~t', ls_parameter, &
															ls_para_tmp)
				CHOOSE CASE li_Rtn
					CASE 0
						This.SetItem( 1, "fill_rate", Dec(ls_parameter))
				END CHOOSE
			Else
				dw_additional_data.object.product_state.visible = 0
				dw_additional_data.object.product_state_t.visible = 0			

			End If
	CASE 'trans_mode'
		IF (ls_text = 'B') or (ls_text = 'L') then
			dw_additional_data.SetItem(1, 'freight_pymt_plan', 'C')
			dw_additional_data.object.cust_target_date.protect = 0
			dw_additional_data.modify("cust_target_date.background.color = '16777215'")
		ELSE
			dw_additional_data.SetItem(1, 'freight_pymt_plan', 'P')
			dw_additional_data.object.cust_target_date.protect = 1
			dw_additional_data.modify("cust_target_date.background.color = '12632256'")
		End if
	CASE 'gtl_split_ind'
	// added code for  gtl project ibdkdld
		If data = "Y" Then 
			If not dw_additional_data.getitemstring(1,"load_status") = "U" Then
				This.setfocus()
				iw_frame.setmicrohelp("The Load Status must be uncombined for GTL Orders")
				return 1
			End If
		End If
	CASE 'load_status'
	// added code for  gtl project ibdkdld
		If not data = "U" Then 
			If dw_additional_data.getitemstring(1,"gtl_split_ind") = "Y" Then
				This.setfocus()
				iw_frame.setmicrohelp("The Load Status must be uncombined for GTL Orders")
				ib_updateable	=	False
				return 1
			End If
		End If
	CASE 'type_of_sale'
		
		If data = 'T' Then
			This.object.product_state.visible = 1
			This.object.product_state_t.visible = 1			
			This.object.purch_prod_status.protect = 0
			This.modify("purch_prod_status.background.color = '16777215'")			
			//This.object.t_26.visible = 1
		Else
			This.object.purch_prod_status.protect = 1
			This.modify("purch_prod_status.background.color = '12632256'")	
//			This.object.t_26.visible = 0			
			If This.GetItemString(1, "auto_assign") = 'Y' Then
				This.object.product_state.visible = 1
				This.object.product_state_t.visible = 1				
			Else
				This.object.product_state.visible = 0
				This.object.product_state_t.visible = 0				
			End If
		End If		
End Choose

ls_business_rules	=	This.GetItemString(1, 'business_rules')
ls_Business_Rules += Fill("U",55 - Len(Trim(ls_Business_Rules)))
ls_business_rules	=	Left(ls_business_rules, Long(dwo.id) - 1 ) + &
'M' + Right(ls_business_rules, Len(ls_business_rules) - Long(dwo.id))
This.SetItem(1,'business_rules',ls_Business_Rules)
end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;DataWindowChild	ldwc_temp

dw_additional_data.getchild("purch_prod_status",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve("PPSTATUS")
end event

type dw_dept_code from u_base_dw_ext within w_additional_data
integer x = 2185
integer y = 528
integer width = 553
integer height = 72
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_dept_code"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_exp_country from u_base_dw_ext within w_additional_data
integer x = 1600
integer y = 1120
integer width = 1134
integer height = 76
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_exp_country"
boolean border = false
end type

event constructor;call super::constructor;this.insertrow(0)

DataWindowChild	ldwc_temp

This.getchild("export_country",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

end event

type dw_store_door_deck_code from u_base_dw_ext within w_additional_data
integer x = 2071
integer y = 592
integer width = 837
integer height = 140
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_store_door_deck_code"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.Rowcount() = 0 then this.InsertRow(0)

This.getchild("store_door_ind",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()

This.getchild("deck_code",ldwc_temp)
ldwc_temp.settransobject(sqlca)
ldwc_temp.retrieve()
end event

