HA$PBExportHeader$w_reservation_item_inquire.srw
forward
global type w_reservation_item_inquire from w_netwise_response
end type
type dw_main from u_base_dw_ext within w_reservation_item_inquire
end type
type dw_cust from u_base_dw_ext within w_reservation_item_inquire
end type
type uo_groups from u_group_list within w_reservation_item_inquire
end type
end forward

global type w_reservation_item_inquire from w_netwise_response
integer x = 101
integer y = 500
integer width = 2894
integer height = 1184
string title = "Reservation Item Release Inquire"
long backcolor = 67108864
event ue_group_changed ( )
dw_main dw_main
dw_cust dw_cust
uo_groups uo_groups
end type
global w_reservation_item_inquire w_reservation_item_inquire

type variables
Boolean						ib_ok_to_close, &
								ib_inquire

w_sold_by_product_group	iw_parentwindow

string	               is_group_owner, &
								is_open_string
		
end variables

forward prototypes
public subroutine wf_check_groups (string as_salesperson, string as_cust_group_ind)
end prototypes

event ue_group_changed();String	ls_salesperson, &
			ls_cust_group_ind

ls_salesperson = dw_main.GetItemString(1,"salesperson")
ls_cust_group_ind = dw_main.GetItemString(1,"cust_group_ind")

wf_check_groups(ls_salesperson, ls_cust_group_ind)
end event

public subroutine wf_check_groups (string as_salesperson, string as_cust_group_ind);String	ls_group_system, &
			ls_group_id

If (as_salesperson = '251') AND (as_cust_group_ind = 'G') Then
//	ls_group_system = string(ole_groups.object.systemname())
//	ls_group_id = string(ole_groups.object.groupID())
//	ls_group_desc = trim(string(ole_groups.object.GroupDescription()))
	
	ls_group_system = uo_groups.uf_get_owner()
	ls_group_id = string(uo_groups.uf_get_sel_id(ls_group_id))
	
	If (ls_group_system = 'SALES') AND (ls_group_id = '111') Then 
		dw_main.SetItem(1,"date_type","D")
		dw_main.object.date_type.Protect = True
		dw_main.Modify("date_type.RadioButtons.3D=No")
		dw_main.SetItem(1,"from_date",Today())
		dw_main.object.from_date.Protect = True
		dw_main.modify("from_date.background.color = '78682240'")		
		dw_main.SetItem(1,"to_date",RelativeDate(Today(),+3))
		dw_main.object.to_date.Protect = True
		dw_main.modify("to_date.background.color = '78682240'")		
	Else
		dw_main.object.date_type.Protect = False
		dw_main.object.from_date.Protect = False
		dw_main.object.to_date.Protect = False
		dw_main.Modify("date_type.RadioButtons.3D=Yes")
		dw_main.modify("from_date.background.color = '16777215'")		
		dw_main.modify("to_date.background.color = '16777215'")		
	End If
Else
	dw_main.object.date_type.Protect = False
	dw_main.object.from_date.Protect = False
	dw_main.object.to_date.Protect = False
	dw_main.Modify("date_type.RadioButtons.3D=Yes")
	dw_main.modify("from_date.background.color = '16777215'")		
	dw_main.modify("to_date.background.color = '16777215'")		
End If
		
end subroutine

on w_reservation_item_inquire.create
int iCurrent
call super::create
this.dw_main=create dw_main
this.dw_cust=create dw_cust
this.uo_groups=create uo_groups
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_main
this.Control[iCurrent+2]=this.dw_cust
this.Control[iCurrent+3]=this.uo_groups
end on

on w_reservation_item_inquire.destroy
call super::destroy
destroy(this.dw_main)
destroy(this.dw_cust)
destroy(this.uo_groups)
end on

event close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF
end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_postopen;DataWindowChild		ldwc_tsr, ldwc_cust

u_string_functions	lu_string_functions

String	ls_salesperson, &
			ls_cust_type, &
			ls_cust_id, &
			ls_cust_name, &
			ls_cust_city, & 
			ls_cust_state, &
			ls_cust_zip, &
			ls_date_type, &
			ls_from_date, &
			ls_to_date, &
			ls_cust_group_ind, &
			ls_group_system, &
			ls_group_id, &
			ls_group_desc
			

datawindowchild	ldw_DDDWChild

//ole_groups.object.GroupType(2)
//ole_Groups.object.LoadObject()
uo_groups.uf_load_groups('C')

dw_main.getchild( "salesperson", ldwc_tsr)
iw_frame.iu_project_functions.nf_gettsrs(ldwc_tsr, message.is_smanlocation)

If lu_string_functions.nf_isempty(is_open_string) Then 
	dw_cust.Visible = False
//	ole_groups.Visible = True
	uo_groups.Visible = True
Else
	ls_salesperson = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_type = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_id = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_name = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_city = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_state = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_zip = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_date_type = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_from_date = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_to_date = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_cust_group_ind = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_group_system = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_group_id = lu_string_functions.nf_GetToken(is_open_string, '~t')
	ls_group_desc = lu_string_functions.nf_GetToken(is_open_string, '~t')
	
	dw_main.SetItem(1,"salesperson",ls_salesperson)
	dw_cust.SetItem(1,"customer_type",ls_cust_type)
	dw_cust.SetItem(1,"customer_id",ls_cust_id)
	dw_cust.Object.customer_id_name.Text = ls_cust_name	
	dw_cust.Object.customer_id_city.Text = ls_cust_city
	dw_cust.Object.t_state.Text = ls_cust_state
	dw_cust.object.t_zip.text = ls_cust_zip
	dw_main.SetItem(1,"date_type", ls_date_type)
	dw_main.SetItem(1,"from_date", Date(ls_from_date))
	dw_main.SetItem(1,"to_date", Date(ls_to_date))
	dw_main.SetItem(1,"cust_group_ind", ls_cust_group_ind)
	
	If ls_cust_group_ind = "C"	Then
		dw_cust.SetItem(1,"customer_type",ls_cust_type)
		dw_cust.SetItem(1,"customer_id",ls_cust_id)
		dw_cust.Object.customer_id_name.Text = ls_cust_name	
		dw_cust.Object.customer_id_city.Text = ls_cust_city
		dw_cust.Object.t_state.Text = ls_cust_state
		dw_cust.object.t_zip.text = ls_cust_zip
		dw_cust.Visible = True
//		ole_groups.Visible = False
		uo_groups.Visible = False
	Else
		dw_cust.SetItem(1,"customer_type","")
		dw_cust.SetItem(1,"customer_id","")
		dw_cust.Object.customer_id_name.Text = ""	
		dw_cust.Object.customer_id_city.Text = ""
		dw_cust.Object.t_state.Text = ""
		dw_cust.object.t_zip.text = ""
		dw_cust.Visible = False
	//	ole_groups.Visible = True
		uo_groups.Visible = True
	End If
	
End If


dw_cust.SetColumn('customer_id')




end event

event activate;// ibdkdld uo_selectprodgroupbyowner.uf_initialize(1,sqlca.userid,sqlca.dbpass)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_return_string, &
			ls_cust_type, &
			ls_cust_id, &
			ls_group_system, &
			ls_group_id, &
			ls_group_desc

u_string_functions	lu_string_functions

dw_main.AcceptText()
dw_cust.AcceptText()

If lu_string_functions.nf_IsEmpty(dw_main.GetItemString(1,"salesperson")) Then
	MessageBox("Salesperson Error", "SalesPerson is required")
	Return
End If

If dw_main.GetItemString(1,"cust_group_ind") = 'C' Then
	If lu_string_functions.nf_IsEmpty(dw_cust.GetItemString(1,"customer_id")) Then
		MessageBox("Customer Error", "Customer Code is required if selecting by Customer")
		Return
	End If
	If lu_string_functions.nf_IsEmpty(dw_cust.GetItemString(1,"customer_type")) Then
		MessageBox("Customer Error", "Customer Type is required if selecting by Customer")
		Return
	End If
End If

If dw_main.GetItemDate(1,"from_date") > dw_main.GetItemDate(1,"to_date") then
	MessageBox("Date Error", "From Date is after To Date")
	Return
End If

If dw_main.GetItemDate(1,"from_date") > dw_main.GetItemDate(1,"to_date") then
	MessageBox("Date Error", "From Date is after To Date")
	Return
End If

If DaysAfter(dw_main.GetItemDate(1,"from_date"), dw_main.GetItemDate(1,"to_date")) > 4 Then
	MessageBox("Date Error", "To date is more than 4 days after From Date")
	Return
End If

If dw_main.GetItemString(1,"cust_group_ind") = 'C' Then
	ls_cust_type = dw_cust.GetItemString(1,"customer_type") 
	ls_cust_id = dw_cust.GetItemString(1,"customer_id") 
	ls_group_system = ''
	ls_group_id = '0'
	ls_group_desc = ''
Else
	ls_cust_type = ''
	ls_cust_id = ''
	
	dw_cust.Object.customer_id_name.Text = ""	
	dw_cust.Object.customer_id_city.Text = ""
	dw_cust.Object.t_state.Text = ""
	dw_cust.object.t_zip.text = ""
	
	//ls_group_system = string(ole_groups.object.systemname())
	ls_group_system = uo_groups.uf_get_owner()
	//ls_group_id = string(ole_groups.object.groupID())
	ls_group_id = string(uo_groups.uf_get_sel_id(ls_group_id))
	//ls_group_desc = string(ole_groups.object.GroupDescription())
	uo_groups.uf_get_sel_desc(ls_group_desc)
End if

ls_return_string = dw_main.GetItemString(1,"salesperson") + '~t' + &
						ls_cust_type + '~t' + &
						ls_cust_id + '~t' + &	
						dw_cust.Object.customer_id_name.Text + '~t' + &	
						dw_cust.Object.customer_id_city.Text + '~t' + &	
						dw_cust.Object.t_state.Text + '~t' + &	
						dw_cust.object.t_zip.text + '~t' + & 	
						dw_main.GetItemString(1,"date_type") + '~t' + &
						String(dw_main.GetItemDate(1,"from_date")) + '~t' + &
						String(dw_main.GetItemDate(1,"to_date")) + '~t' + &
						dw_main.GetItemString(1,"cust_group_ind") + '~t' + &
						ls_group_system + '~t' + &
						ls_group_id + '~t' + &
						ls_group_desc + '~t' 
						
//MessageBox("return string", "return string = " + ls_return_string) 						

CloseWithReturn(This, ls_return_string)
end event

event open;call super::open;u_string_functions	lu_string_functions

datawindowchild			ldwc_temp

String						ls_data

is_open_string = Message.StringParm
//MessageBox("ls_open_string",is_open_string)

If lu_string_functions.nf_isempty(is_open_string) Then 
	dw_cust.Visible = False
//	ole_groups.Visible = True
	uo_groups.Visible = True
End If

dw_cust.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_temp)

dw_cust.GetChild('billto_id', ldwc_temp)
iw_frame.iu_project_functions.ids_billtos.ShareData(ldwc_temp)

dw_cust.GetChild('corp_id', ldwc_temp)
iw_frame.iu_project_functions.ids_corps.ShareData(ldwc_temp)

If dw_cust.ImportString(ls_data) < 1 Then 
	dw_cust.InsertRow(0)
End If
end event

type cb_base_help from w_netwise_response`cb_base_help within w_reservation_item_inquire
integer x = 2286
integer y = 1440
integer taborder = 60
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_reservation_item_inquire
integer x = 2267
integer y = 220
integer taborder = 50
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_reservation_item_inquire
integer x = 2267
integer y = 60
integer taborder = 45
end type

type dw_main from u_base_dw_ext within w_reservation_item_inquire
integer x = 27
integer y = 28
integer width = 2158
integer height = 420
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_reservation_item_inq"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)

This.SetItem(1,"date_type",'S')
This.SetItem(1,"cust_group_ind", 'G')
This.SetItem(1,"from_date", Today())
This.SetItem(1,"to_date", Today())

end event

event ue_dwndropdown;call super::ue_dwndropdown;//DataWindowChild	ldwc_temp
//
//u_project_functions	lu_project_functions
//
//Choose Case This.GetColumnName()
//	Case 'customer_id'
//		This.GetChild('customer_id', ldwc_temp)
//		If ldwc_temp.RowCount() < 1 Then
//			lu_project_functions.nf_getcustomers(ldwc_temp, &
//					message.is_smanlocation)
//		End If
//End Choose
end event

event itemchanged;call super::itemchanged;String	ls_salesperson, &
			ls_cust_group_ind

Choose Case This.GetColumnName()
	Case 'cust_group_ind'

		Choose Case data
			Case 'C'
				dw_cust.Visible = True
		//		ole_groups.Visible = False
				uo_groups.Visible = False
				
				if dw_cust.GetItemString(1, "customer_type") = "" Then
					dw_cust.SetItem(1,"customer_type","S")
				End If
			Case 'G'
				dw_cust.Visible = False
	//			ole_groups.Visible = True
				uo_groups.Visible = True
				
		End Choose
		
		ls_salesperson = dw_main.GetItemString(1,"salesperson")
		Parent.wf_check_groups(ls_salesperson, data)
		
	Case 'salesperson'

		ls_cust_group_ind = dw_main.GetItemString(1,"cust_group_ind")
		Parent.wf_check_groups(data, ls_cust_group_ind)		
		
End Choose





end event

type dw_cust from u_base_dw_ext within w_reservation_item_inquire
integer x = 1422
integer y = 484
integer width = 1394
integer height = 536
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_reservation_item_inq_cust"
end type

event itemchanged;call super::itemchanged;String				ls_CustomerName,&
						ls_CustomerCity, &
						ls_state, &
						ls_zip, &
						ls_FindExpression , &
						ls_customer
Long					ll_RowFound

datawindowchild	ldw_childdw		
S_Error		lstr_Error_Info

Choose Case This.GetColumnName()
	Case 'customer_type'
		CHOOSE Case data 
			Case  'B' 
				This.SetItem( 1, "customer_id", "")
			
			Case  'C'
				This.SetItem( 1, "customer_id", "")
			Case  'S'
				This.SetItem( 1, "customer_id", "")
		END  CHOOSE
		This.Object.customer_id_name.Text = ''
		This.Object.customer_id_city.Text = ''
		This.Object.t_state.Text = ''
		this.object.t_zip.text = ''
	Case 'corp_id',"billto_id","customer_id"
		IF iw_frame.iu_project_functions.nf_ValidateCustomerState( Data, This.GetItemString(1,"customer_type"),&
																				ls_CustomerName,ls_CustomerCity,ls_state) < 1 Then
																				
			IF lstr_error_info.se_message = "" then iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
			This.Object.customer_id_name.Text = ''
			This.Object.customer_id_city.Text = ''
			This.Object.t_state.Text = ''
			this.setitem(1,'customer_id',"")
			This.SelectText(1, Len(data))
			This.SetColumn('customer_id')
													
			Return 1
		ELSE
			if isnull(ls_CustomerName) THEN
				This.Object.customer_id_name.Text = ''
			else
				This.Object.customer_id_name.Text = ls_CustomerName
			end if
			if isnull(ls_CustomerCity) THEN
				This.Object.customer_id_city.Text = ''
			else
				This.Object.customer_id_city.Text = ls_CustomerCity
			end if
			if isnull(ls_state) THEN
				This.Object.t_state.Text = ''
			else
				This.Object.t_state.text = ls_state
			end if
			this.SetTransObject(SQLCA)
			SELECT customers.zip_code
				INTO :ls_zip
				FROM customers
				WHERE customers.customer_id = :data
				USING SQLCA ;
			IF not isnull(ls_zip) THEN
				This.Object.t_zip.Text = ls_zip
			END IF 
		END IF
End Choose
end event

type uo_groups from u_group_list within w_reservation_item_inquire
integer x = 41
integer y = 468
integer taborder = 21
boolean bringtotop = true
end type

on uo_groups.destroy
call u_group_list::destroy
end on


Start of PowerBuilder Binary Data Section : Do NOT Edit
09w_reservation_item_inquire.bin 
2500000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000f61b936001caabf500000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000f61b936001caabf5f61b936001caabf5000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
2Bffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001ca1000800034757f20affffffe00065005f00740078006e00650079007400000d700073006e006c0020006e006f002000670070005b006d00620064005f00650064007800650063006500740075005d006500720000006d00650074006f006800650074006f0069006c006b006e00740073007200610020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f006500640064006100690076006500730000005d00650072006f006d00650074006f0068006c0074006e00690073006b006f00740020007000200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006e00750064006100690076006500730000005d00650072006f006d006500740065007200750071007300650020007400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006500720075007100730065005d007400720000006d00650074006f00730065006e00650020006400200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d00620064005f00650064006f007000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
19w_reservation_item_inquire.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
