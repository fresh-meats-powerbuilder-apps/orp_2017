HA$PBExportHeader$w_cust_res_notepad.srw
$PBExportComments$windows used to quickly select items out of a reservation
forward
global type w_cust_res_notepad from w_base_response_ext
end type
type em_1 from editmask within w_cust_res_notepad
end type
type dw_noted_pad from u_base_dw_ext within w_cust_res_notepad
end type
type st_1 from statictext within w_cust_res_notepad
end type
type dw_plant from u_plant within w_cust_res_notepad
end type
type st_2 from statictext within w_cust_res_notepad
end type
type cbx_cancel from checkbox within w_cust_res_notepad
end type
type cbx_massoveride from checkbox within w_cust_res_notepad
end type
type cb_1 from u_base_commandbutton_ext within w_cust_res_notepad
end type
type cb_2 from u_base_commandbutton_ext within w_cust_res_notepad
end type
end forward

global type w_cust_res_notepad from w_base_response_ext
integer x = 850
integer y = 276
integer width = 1842
integer height = 1452
string title = "NotePad"
long backcolor = 12632256
em_1 em_1
dw_noted_pad dw_noted_pad
st_1 st_1
dw_plant dw_plant
st_2 st_2
cbx_cancel cbx_cancel
cbx_massoveride cbx_massoveride
cb_1 cb_1
cb_2 cb_2
end type
global w_cust_res_notepad w_cust_res_notepad

type variables
Window iw_calling_parent
end variables

forward prototypes
public function integer wf_highlight_invalids (string as_invalid_products)
end prototypes

public function integer wf_highlight_invalids (string as_invalid_products);Int	li_Number_Of_Invalids


DO While Len( as_invalid_Products) > 1
	li_Number_Of_Invalids++	
	dw_noted_pad.SetItem(li_Number_Of_Invalids, "is_Valid_code",Left( as_invalid_Products,1))
	as_invalid_Products = Mid( as_invalid_Products, 3)
Loop
Return li_Number_Of_Invalids
end function

event ue_postopen;call super::ue_postopen;iw_frame.setmicrohelp("Ready")
em_1.SetFocus()
cbx_massoveride.visible = false
end event

on open;call w_base_response_ext::open;iw_calling_parent = Message.PowerObjectParm
dw_noted_pad.InsertRow(0)
dw_noted_pad.Ib_newRowOnChange = TRUE

end on

on w_cust_res_notepad.create
int iCurrent
call super::create
this.em_1=create em_1
this.dw_noted_pad=create dw_noted_pad
this.st_1=create st_1
this.dw_plant=create dw_plant
this.st_2=create st_2
this.cbx_cancel=create cbx_cancel
this.cbx_massoveride=create cbx_massoveride
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_1
this.Control[iCurrent+2]=this.dw_noted_pad
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.dw_plant
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.cbx_cancel
this.Control[iCurrent+7]=this.cbx_massoveride
this.Control[iCurrent+8]=this.cb_1
this.Control[iCurrent+9]=this.cb_2
end on

on w_cust_res_notepad.destroy
call super::destroy
destroy(this.em_1)
destroy(this.dw_noted_pad)
destroy(this.st_1)
destroy(this.dw_plant)
destroy(this.st_2)
destroy(this.cbx_cancel)
destroy(this.cbx_massoveride)
destroy(this.cb_1)
destroy(this.cb_2)
end on

event ue_base_ok;call super::ue_base_ok;Decimal	ldc_price

Int		li_RowCount,&
			li_org_count, &
			li_rtn, &
			li_index, &
			li_Counter
			
S_Error	lstr_Error

u_orp002	lu_orp002

u_ws_orp3	lu_ws_orp3

long		ll_Found_Row,&
			ll_This_dw_count, &
			ll_price

String	ls_Search,&
			ls_Plant,&
			ls_Age,&
			ls_header_string, &
			ls_detail_string, &
			ls_customerid, &
			ls_massoverride, &
			ls_Product_Code, &
			ls_overideind

u_string_functions	lu_string_function

This.SetRedraw(False)
lu_orp002  = Create u_orp002	
lu_ws_orp3 = Create u_ws_orp3
lstr_Error.se_app_name  = "orp"
lstr_Error.se_window_name = "Notepad"
lstr_Error.se_function_name = "Validate Products"
lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")
dw_plant.AcceptText()
ls_Plant = dw_plant.nf_get_plant_code()
If lu_string_function.nf_IsEmpty( ls_Plant) Then 
	iw_Frame.SetMicroHelp( "Plant is a required field")
	This.SetRedraw(True)
	Return
END IF
iw_calling_Parent.TriggerEvent( "ue_GetCustomerid")
ls_customerid	=	Message.StringParm
IF cbx_massoveride.Checked THEN 
	ls_massoverride	=	'Y'
ELSE
	ls_massoverride	=	'N'
END IF
li_RowCount = dw_noted_pad.RowCount()
dw_noted_pad.AcceptText()
IF li_RowCount > 0 THEN
	ls_header_string	=	ls_customerid + '~t' + ls_plant + '~t' + ls_massoverride
	ls_detail_string	=	dw_noted_pad.Describe('Datawindow.Data')							
//	li_rtn	= lu_orp002.nf_orpo75ar_validate_products( lstr_Error, ls_header_string, ls_detail_string)
	li_rtn	= lu_ws_orp3.uf_orpo75fr_validate_products( lstr_Error, ls_header_string, ls_detail_string)
	IF	li_rtn < 0 THEN
		This.SetRedraw(True)
		RETURN
	ELSE
		dw_noted_pad.Reset()
		dw_noted_pad.ImportString(ls_detail_string)
		li_RowCount = dw_noted_pad.RowCount()
	END IF
END IF	
Destroy Lu_Orp002
Destroy lu_ws_orp3


iw_calling_Parent.TriggerEvent( "Ue_Get_Detail_RowCount")
ll_This_dw_Count  = Message.LongParm 
li_org_count = li_RowCount

li_RowCount = dw_noted_pad.RowCount()

For li_Counter =  1 to li_RowCount
	//li_index = li_Counter
	If Mid(dw_noted_pad.Getitemstring(li_Counter, "business_rules") , 3, 1) = 'E' then
		cbx_massoveride.visible = true
	else
		For li_index = li_RowCount to li_RowCount
			If li_RowCount > 0 then 
			IF dw_noted_pad.GetItemString( li_index, "error_flag") = 'E' THEN
				IF(MID(dw_noted_pad.GetItemString( li_index, "business_rules") , 1, 1)) = 'V' THEN
					iw_frame.SetMicrohelp('Invalid Price for the Product, Do a Price Override')
				ELSE
					iw_frame.SetMicrohelp('Invalid Product/Product not available for order')
				END IF
				CONTINUE
			END IF
			ls_Product_Code = Trim(dw_noted_pad.GetItemString( li_index, &
						"sku_product_code"))
			IF Not lu_string_function.nf_IsEmpty( ls_Product_Code) Then
				ls_search = "product_code = '" + ls_Product_Code+ &
						"' AND String(Scheduled_Ship_date) = '" + em_1.Text + "' AND ordered_units > 0 AND plant = '"+ &
						ls_plant+ "' AND line_status <> 'C' AND queue_status = 'A'" 
				ls_age = dw_noted_pad.GetItemString( li_index, "age")
				IF Not lu_string_function.nf_IsEmpty( ls_Age) Then
					ls_search += " AND age = '" + ls_age +"'"
				END IF
				iw_calling_Parent.TriggerEvent( "ue_find_In_detail",0, ls_Search)
				ll_Found_Row = Message.LongParm
				IF ll_Found_Row > 0 Then
					iw_calling_Parent.TriggerEvent("ue_set_OrderedUnits", ll_Found_Row, String(dw_noted_pad.GetItemDecimal( li_index, "sales_Order_Units")))
					ll_price	=	dw_noted_pad.GetItemNumber( li_index, "sales_price")
					IF IsNull(ll_price) THEN
						ldc_price	=	0.00
					ELSE
						ldc_price = dw_noted_pad.GetItemDecimal( li_index, "sales_price")
						IF ldc_price > 0 THEN iw_calling_Parent.TriggerEvent("Ue_Set_Price", ll_Found_Row, String(ldc_price,"###.##"))				
					END IF
					iw_calling_Parent.TriggerEvent( "ue_DetailSelectLine", 1, ll_Found_Row)
					IF cbx_massoveride.Checked THEN
						ls_overideind	=	'Y'
					ELSE
						ls_overideind	=	dw_noted_pad.GetItemString( li_index, "price_override")
					END IF
					iw_calling_Parent.TriggerEvent("Ue_Set_Priceoveride", ll_Found_Row, ls_overideind )
					dw_noted_pad.DeleteRow( li_index)
					li_index --
				 	li_RowCount --
				ELSE
					li_index --
					li_RowCount --
				END IF
			END IF 
			li_index --
//			li_RowCount --
			end if
		next
	end if
next
		
//	else
//		dw_noted_pad.SelectRow(li_Counter, False)
//		IF dw_noted_pad.GetItemString( li_index, "error_flag") = 'E' THEN
//				IF(MID(dw_noted_pad.GetItemString( li_index, "business_rules") , 1, 1)) = 'V' THEN
//					iw_frame.SetMicrohelp('Invalid Price for the Product, Do a Price Override')
//				ELSE
//					iw_frame.SetMicrohelp('Invalid Product/Product not available for order')
//				END IF
//				CONTINUE
//				END IF
//			ls_Product_Code = Trim(dw_noted_pad.GetItemString( li_index, &
//						"sku_product_code"))
//			IF Not lu_string_function.nf_IsEmpty( ls_Product_Code) Then
//				ls_search = "product_code = '" + ls_Product_Code+ &
//						"' AND String(Scheduled_Ship_date) = '" + em_1.Text + "' AND ordered_units > 0 AND plant = '"+ &
//						ls_plant+ "' AND line_status <> 'C' AND queue_status = 'A'" 
//				ls_age = dw_noted_pad.GetItemString( li_index, "age")
//				IF Not lu_string_function.nf_IsEmpty( ls_Age) Then
//					ls_search += " AND age = '" + ls_age +"'"
//				END IF
//				iw_calling_Parent.TriggerEvent( "ue_find_In_detail",0, ls_Search)
//				ll_Found_Row = Message.LongParm
//				IF ll_Found_Row > 0 Then
//					iw_calling_Parent.TriggerEvent("ue_set_OrderedUnits", ll_Found_Row, String(dw_noted_pad.GetItemDecimal( li_index, "sales_Order_Units")))
//					ll_price	=	dw_noted_pad.GetItemNumber( li_index, "sales_price")
//					IF IsNull(ll_price) THEN
//						ldc_price	=	0.00
//					ELSE
//						ldc_price = dw_noted_pad.GetItemDecimal( li_index, "sales_price")
//						IF ldc_price > 0 THEN iw_calling_Parent.TriggerEvent("Ue_Set_Price", ll_Found_Row, String(ldc_price,"###.##"))				
//					END IF
//					iw_calling_Parent.TriggerEvent( "ue_DetailSelectLine", 1, ll_Found_Row)
//					IF cbx_massoveride.Checked THEN
//						ls_overideind	=	'Y'
//					ELSE
//						ls_overideind	=	dw_noted_pad.GetItemString( li_index, "price_override")
//					END IF
//					iw_calling_Parent.TriggerEvent("Ue_Set_Priceoveride", ll_Found_Row, ls_overideind )
//					dw_noted_pad.DeleteRow( li_index)
//					li_index --
//					li_RowCount --
//					li_Counter --
//				END IF
//			END IF 		
	//end if
//Next




// select valid rows on the parent window
//iw_calling_Parent.TriggerEvent( "Ue_Get_Detail_RowCount")
//ll_This_dw_Count  = Message.LongParm 
//li_org_count = li_RowCount
//
//FOR li_index = 2 to li_RowCount
//	IF dw_noted_pad.GetItemString( li_index, "error_flag") = 'E' THEN
//		IF(MID(dw_noted_pad.GetItemString( li_index, "business_rules") , 1, 1)) = 'V' THEN
//			iw_frame.SetMicrohelp('Invalid Price for the Product, Do a Price Override')
//		ELSE
//			iw_frame.SetMicrohelp('Invalid Product/Product not available for order')
//		END IF
//		CONTINUE
//	END IF
//	ls_Product_Code = Trim(dw_noted_pad.GetItemString( li_index, &
//				"sku_product_code"))
//	IF Not lu_string_function.nf_IsEmpty( ls_Product_Code) Then
//		ls_search = "product_code = '" + ls_Product_Code+ &
//				"' AND String(Scheduled_Ship_date) = '" + em_1.Text + "' AND ordered_units > 0 AND plant = '"+ &
//				ls_plant+ "' AND line_status <> 'C' AND queue_status = 'A'" 
//		ls_age = dw_noted_pad.GetItemString( li_index, "age")
//		IF Not lu_string_function.nf_IsEmpty( ls_Age) Then
//			ls_search += " AND age = '" + ls_age +"'"
//		END IF
//		iw_calling_Parent.TriggerEvent( "ue_find_In_detail",0, ls_Search)
//		ll_Found_Row = Message.LongParm
//		IF ll_Found_Row > 0 Then
//			iw_calling_Parent.TriggerEvent("ue_set_OrderedUnits", ll_Found_Row, String(dw_noted_pad.GetItemDecimal( li_index, "sales_Order_Units")))
//			ll_price	=	dw_noted_pad.GetItemNumber( li_index, "sales_price")
//			IF IsNull(ll_price) THEN
//				ldc_price	=	0.00
//			ELSE
//				ldc_price = dw_noted_pad.GetItemDecimal( li_index, "sales_price")
//				IF ldc_price > 0 THEN iw_calling_Parent.TriggerEvent("Ue_Set_Price", ll_Found_Row, String(ldc_price,"###.##"))				
//			END IF
//			iw_calling_Parent.TriggerEvent( "ue_DetailSelectLine", 1, ll_Found_Row)
//			IF cbx_massoveride.Checked THEN
//				ls_overideind	=	'Y'
//			ELSE
//				ls_overideind	=	dw_noted_pad.GetItemString( li_index, "price_override")
//			END IF
//			iw_calling_Parent.TriggerEvent("Ue_Set_Priceoveride", ll_Found_Row, ls_overideind )
//			dw_noted_pad.DeleteRow( li_index)
//			li_index --
//			li_RowCount --
//		END IF
//	END IF 
//NEXT



li_RowCount = dw_noted_pad.RowCount()

IF li_rtn	=	0 THEN
	IF li_org_count = li_RowCount  Then 
		iw_calling_Parent.TriggerEvent("ue_detail_is_selected")
		If Upper(Message.StringParm) = 'FALSE' Then
			This.SetRedraw(True)
			MessageBox( "Warning", "You must select at least one line from a reservation to generate a sales order")
			RETURN
		END IF
	END IF
END IF
Setredraw(TRUE)
IF li_rtn	<> 0 THEN 
	RETURN
END IF

IF	dw_noted_pad.RowCount() > 0 THEN
//Delete the empty row
	ls_product_code	=	Trim(dw_noted_pad.GetItemString( li_rowcount, "sku_product_code"))
	IF IsNull(ls_product_code) or ls_product_code = '' THEN dw_noted_pad.DeleteRow(li_rowcount)
END IF

IF li_rtn	=	0 and li_RowCount > 0 Then
	IF MessageBox("Warning", "Encountered Rows that could not be located, Do you want to add them to the sales order being generated", Question!, YESNO!) = 1 Then
		For li_index	=	1 to li_rowcount
			IF cbx_massoveride.Checked THEN
				dw_noted_pad.SetItem(li_index, 'price_override', 'Y')
			END IF
			dw_noted_pad.SetItem(li_index, 'business_rules', '')
		NEXT
		iw_calling_Parent.TriggerEvent("ue_SetAddedRows",dw_noted_pad.RowCount(), dw_noted_pad.Describe( "DataWindow.Data"))
		If cbx_cancel.Checked Then 
			CloseWithReturn( This,'Y')
		Else
			CloseWithReturn( This, 'N')
		End If
	END IF
ELSE
	If cbx_cancel.Checked Then 
		CloseWithReturn( This,'Y')
	ELSE
		CloseWithReturn( This,'N')
	End If
END IF
	
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, "Abort")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cust_res_notepad
integer x = 1458
integer y = 668
integer width = 293
integer taborder = 100
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cust_res_notepad
integer x = 1458
integer y = 308
integer width = 293
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cust_res_notepad
integer x = 1458
integer y = 188
integer width = 293
integer taborder = 60
string text = "&Select"
boolean default = false
end type

type cb_browse from w_base_response_ext`cb_browse within w_cust_res_notepad
integer taborder = 0
end type

type em_1 from editmask within w_cust_res_notepad
integer x = 224
integer y = 32
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "mm/dd/yyyy"
boolean spin = true
double increment = 1
string minmax = "~~"
end type

on constructor;em_1.SelectText(1, 12)
em_1.ReplaceText(String(Today(), 'mm/dd/yyyy'))

end on

on getfocus;This.SelectText(4, 2)
end on

type dw_noted_pad from u_base_dw_ext within w_cust_res_notepad
integer x = 82
integer y = 316
integer width = 1294
integer height = 992
integer taborder = 30
string dataobject = "d_noted_pad"
boolean vscrollbar = true
end type

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;This.SelectText(1,10)
end on

event itemchanged;call super::itemchanged;string 	ls_ship_customer, ls_plant

u_project_functions lu_project_functions

If dwo.name = "sku_product_code" Then
	iw_calling_Parent.TriggerEvent( "ue_GetCustomerid")
	ls_ship_customer	=	Message.StringParm
	ls_plant = dw_plant.GetItemString(1, "location_code")

	This.SetItem( row, "age", lu_project_functions.nf_get_age_code(Data, ls_ship_customer,ls_plant))	
END IF
end event

event constructor;call super::constructor;ib_firstcolumnonnextrow	= True
end event

type st_1 from statictext within w_cust_res_notepad
integer x = 82
integer y = 16
integer width = 142
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Ship"
boolean focusrectangle = false
end type

type dw_plant from u_plant within w_cust_res_notepad
integer x = 78
integer y = 208
integer width = 1294
integer taborder = 20
end type

event constructor;call super::constructor;This.ScrollToRow(This.RowCount()) // Select the empty plant at the end
end event

type st_2 from statictext within w_cust_res_notepad
integer x = 82
integer y = 72
integer width = 133
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Date"
boolean focusrectangle = false
end type

type cbx_cancel from checkbox within w_cust_res_notepad
integer x = 690
integer width = 686
integer height = 112
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Cancel Remaining Quantity"
boolean lefttext = true
end type

type cbx_massoveride from checkbox within w_cust_res_notepad
integer x = 969
integer y = 100
integer width = 407
integer height = 104
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Mass Override"
boolean lefttext = true
end type

event clicked;String ls_protected
Long ll_count
If cbx_massoveride.checked = True then
	for ll_count = 1 to dw_noted_pad.RowCount()
		ls_protected = dw_noted_pad.GetItemString(ll_count, "business_rules")
		if (Mid(ls_protected, 3, 1) = 'E') then
			dw_noted_pad.SetItem(ll_count, "price_override", 'Y')
		end if
	Next 
Else 
	for ll_count = 1 to dw_noted_pad.RowCount()
		ls_protected = dw_noted_pad.GetItemString(ll_count, "business_rules")
		if (Mid(ls_protected, 3, 1) = 'E') then
			dw_noted_pad.SetItem(ll_count, "price_override", 'N')
		end if
	Next 
End if
	
end event

type cb_1 from u_base_commandbutton_ext within w_cust_res_notepad
integer x = 1458
integer y = 548
integer width = 293
integer height = 108
integer taborder = 90
integer textsize = -8
integer weight = 400
string text = "&Delete Row"
end type

event clicked;call super::clicked;IF MessageBox("Delete Row", "Are you sure?", &
				Information!, OKCancel!, 2) = 1 then 
	dw_noted_pad.DeleteRow(dw_noted_pad.GetRow())
End IF	

end event

type cb_2 from u_base_commandbutton_ext within w_cust_res_notepad
integer x = 1458
integer y = 428
integer width = 293
integer height = 108
integer taborder = 80
integer textsize = -8
integer weight = 400
string text = "&Add Row"
end type

event clicked;call super::clicked;dw_noted_pad.InsertRow(dw_noted_pad.RowCount())
end event

