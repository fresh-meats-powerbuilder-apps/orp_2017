HA$PBExportHeader$w_res_browse-product.srw
$PBExportComments$new and improved res_browse.
forward
global type w_res_browse-product from w_base_response_ext
end type
type dw_control from u_base_dw_ext within w_res_browse-product
end type
type dw_main from u_base_dw_ext within w_res_browse-product
end type
type cb_others from u_base_commandbutton_ext within w_res_browse-product
end type
end forward

global type w_res_browse-product from w_base_response_ext
integer x = 87
integer y = 284
integer width = 2830
integer height = 1600
string title = "Reservation Browse"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
long backcolor = 12632256
dw_control dw_control
dw_main dw_main
cb_others cb_others
end type
global w_res_browse-product w_res_browse-product

type variables
	u_orp001			iu_orp001
	u_ws_orp3			iu_ws_orp3
	s_error			istr_error
	string			is_initstring, &
				is_requested_plant

end variables

on w_res_browse-product.create
int iCurrent
call super::create
this.dw_control=create dw_control
this.dw_main=create dw_main
this.cb_others=create cb_others
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_control
this.Control[iCurrent+2]=this.dw_main
this.Control[iCurrent+3]=this.cb_others
end on

on w_res_browse-product.destroy
call super::destroy
destroy(this.dw_control)
destroy(this.dw_main)
destroy(this.cb_others)
end on

event ue_postopen;call super::ue_postopen;SetPointer(HourGlass!)	

iu_orp001 = CREATE u_orp001
Message.StringParm = "C"
dw_main.TriggerEvent("ue_retrieve")

end event

event ue_base_ok;STRING 	ls_ReservationInfo

LONG		ll_Rtn

u_string_functions	lu_string_functions

SetPointer(HourGlass!)
ll_Rtn = dw_main.AcceptText()
IF ll_Rtn = -1 THEN 
	dw_main.SetFocus()
	RETURN
END IF

If lu_string_functions.nf_IsEmpty(is_requested_plant) Then &
		is_requested_plant = '   ' 
ls_ReservationInfo = ls_ReservationInfo + is_requested_plant

ls_ReservationInfo = dw_main.describe("datawindow.data")
 
CloseWithReturn(This, ls_ReservationInfo)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")

end event

event open;call super::open;is_initstring = message.stringparm
is_requested_plant = ''

dw_main.object.datawindow.HorizontalScrollSplit = 510
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_res_browse-product
integer x = 2510
integer y = 428
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_res_browse-product
integer x = 2510
integer y = 196
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_res_browse-product
integer x = 2510
integer y = 80
end type

type cb_browse from w_base_response_ext`cb_browse within w_res_browse-product
end type

type dw_control from u_base_dw_ext within w_res_browse-product
event ue_data pbm_custom05
integer x = 41
integer y = 40
integer width = 1705
integer height = 192
integer taborder = 60
string dataobject = "d_res_browse_control"
boolean border = false
end type

event ue_data;call super::ue_data;//Long				ll_columnCount
//
//Integer			li_Counter
//
//
//ll_ColumnCount = Long(This.Describe("DataWindow.Column.Count"))
//For li_Counter = 1 to ll_ColumnCount
//	// Get the column type of the clicked field
//	If Left( Lower(This.Describe("#" + String(li_Counter) + ".ColType")), 4) = 'date' Then
//		This.Modify("#" + String(li_Counter) + ".Edit.Format = 'yyyy-mm-dd'")
//	End if
//Next
//Message.StringParm = This.Describe("DataWindow.Data")
end event

event constructor;call super::constructor;graphicobject	lgr_object, &
					lgr_tab

// Get the pointer to the parent window of this UO
lgr_object	=	Parent
CHOOSE CASE TypeOf(lgr_object) 
	CASE userobject!
		lgr_tab	=	lgr_object.GetParent()	
		IF TypeOf(lgr_tab) = tab! THEN
			iw_parent	=	lgr_tab.GetParent()
		ELSE
			iw_parent	=	lgr_object.GetParent()
		END IF
	CASE window!
		iw_parent	=	Parent
END CHOOSE

This.Postevent("ue_postconstructor")
end event

type dw_main from u_base_dw_ext within w_res_browse-product
event ue_setmaxreq ( )
integer x = 37
integer y = 268
integer width = 2432
integer height = 1208
integer taborder = 70
string dataobject = "d_res_browse_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event ue_retrieve;INTEGER	 	li_Rtn

STRING		ls_header_info, &
				ls_detail_info,&
				ls_Temp
							
Double		ld_task_number

Integer		li_page_number

CHARACTER	lc_ModeIndicator

u_string_functions	lu_string_functions

ls_temp = is_initString

ls_header_info = lu_string_functions.nf_gettoken (  is_initString, "~x" )
ls_header_info += "~t"
ls_header_info += lu_string_functions.nf_gettoken ( is_initString, "~x") 
ls_detail_info = lu_string_functions.nf_gettoken ( is_initString, "~x" )

is_initString =  ls_temp

lc_ModeIndicator = Message.StringParm

//If Not IsValid(iu_orp001) Then iu_orp001 = Create u_orp001
//li_Rtn = iu_orp001.nf_orpo51ar(istr_error, ls_header_info, &
//         ls_detail_info, ld_task_number,  &
//			li_page_number, lc_ModeIndicator)
			
If Not IsValid(iu_ws_orp3) Then iu_ws_orp3 = Create u_ws_orp3
li_Rtn = iu_ws_orp3.uf_orpo51fr(istr_error, ls_header_info, &
         ls_detail_info, lc_ModeIndicator)			

CHOOSE CASE li_Rtn
CASE 0
	dw_Control.Reset()
	dw_Control.ImportString( ls_header_info)

	dw_main.Reset()
	dw_main.ImportString( ls_detail_info)
END CHOOSE   
is_initString = ls_temp
ls_temp = dw_main.Describe("DataWindow.Data")
end event

event itemchanged;call super::itemchanged;String			ls_column_name, &
					ls_number, &
					ls_text
					
Integer			li_count

Long				ll_row, &
					ll_total, &
					ll_quantity


ls_column_name = dwo.name
ls_text = data
ll_row = row

// ls_number will equal the column number as long as you don't have 
// more than ten columns.

ls_number = Right(ls_column_name, 1)
If ls_number = "0" Then ls_number = "10"

Choose Case ls_column_name
Case 'requested_1', 'requested_2', 'requested_3', 'requested_4', &
		'requested_5', 'requested_6', 'requested_7', 'requested_8', &
		'requested_9', 'requested_10'
//	If INTEGER(ls_text) < 0 Or INTEGER(ls_text) >  &
//			This.GetItemNumber(ll_row, 'available_' + ls_number) OR INTEGER(ls_text) >  &
//			This.GetItemNumber(ll_row, 'order_units') Then
//		iw_frame.SetMicroHelp("Requested units must be greater than 0 " + &
//		       ",less than available units and less than or equal to original ordered units.")
	If INTEGER(ls_text) >  &
			This.GetItemNumber(ll_row, 'available_' + ls_number) OR INTEGER(ls_text) >  &
			This.GetItemNumber(ll_row, 'order_units') Then
		iw_frame.SetMicroHelp("Requested units must be less than or equal to " + &
		       "available units and less than or equal to original ordered units.")
		Return 1
	ELSE
		iw_frame.SetMicroHelp("Ready")
	End If

	ll_quantity = INTEGER(ls_text)
	ll_total = ll_quantity
	For li_count = 1 to 10
		IF li_count <> Integer(ls_number) Then 
			ll_total +=  This.GetItemNumber(ll_row, 'requested_' + String(li_count))
		End If
	Next
	if ll_quantity > 0 then
		If is_requested_plant = '' Then
			is_requested_plant =dw_main.GetItemString(1, 'plant_' + ls_number)
		Else
			If is_requested_plant <>dw_main.GetItemString(1, 'plant_' + ls_number) Then
				iw_frame.SetMicroHelp('This Reservation is for a different plant than ' + &
					'the previous Reservation you have entered a quantity.')
//				Return 1
			End If
		End If	
	End If
End Choose
end event

event itemerror;call super::itemerror;Return 1
end event

type cb_others from u_base_commandbutton_ext within w_res_browse-product
event clicked pbm_bnclicked
integer x = 2510
integer y = 312
integer width = 270
integer height = 108
integer taborder = 50
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = "O&thers"
end type

event clicked;call super::clicked;Message.StringParm = "N"
dw_main.TriggerEvent("ue_retrieve")
end event

