HA$PBExportHeader$w_reservation_item_browse_updt.srw
forward
global type w_reservation_item_browse_updt from w_base_sheet_ext
end type
type cb_2 from u_base_commandbutton_ext within w_reservation_item_browse_updt
end type
type cb_1 from u_base_commandbutton_ext within w_reservation_item_browse_updt
end type
type dw_parameters from u_base_dw_ext within w_reservation_item_browse_updt
end type
type dw_detail from u_base_dw_ext within w_reservation_item_browse_updt
end type
end forward

global type w_reservation_item_browse_updt from w_base_sheet_ext
integer width = 3950
integer height = 1636
string title = "Reservation Item Release"
cb_2 cb_2
cb_1 cb_1
dw_parameters dw_parameters
dw_detail dw_detail
end type
global w_reservation_item_browse_updt w_reservation_item_browse_updt

type variables
String	is_openingstring

u_orp001		iu_orp001

u_ws_orp3		iu_ws_orp3

s_error	istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();String					ls_header_string, &
							ls_detail_string, &
							ls_customer_id, &
							ls_customer_type, &
							ls_group_id, &
							ls_group_owner

Integer					li_rtn

u_string_functions	lu_string_functions 
			
This.TriggerEvent('closequery')

OpenWithParm( w_reservation_item_inquire, is_openingstring)

If message.StringParm = 'Cancel' Then 
	Return False
Else
	is_openingstring = message.StringParm
	dw_parameters.Reset()
	li_rtn = dw_parameters.ImportString( is_openingstring)
End If
	
istr_error_info.se_event_name = "wf_retrieve"		

If lu_string_functions.nf_IsEmpty(dw_parameters.GetItemString(1, "customer_id")) Then
	ls_customer_id = ""
Else
	ls_customer_id = dw_parameters.GetItemString(1, "customer_id")
End If

If lu_string_functions.nf_IsEmpty(dw_parameters.GetItemString(1, "customer_type")) Then
	ls_customer_type = ""
Else
	ls_customer_type = dw_parameters.GetItemString(1, "customer_type")
End If

If lu_string_functions.nf_IsEmpty(dw_parameters.GetItemString(1, "group_owner")) Then
	ls_group_owner = ""
Else
	ls_group_owner = dw_parameters.GetItemString(1, "group_owner")
End If

If IsNull(dw_parameters.GetItemNumber(1, "group_id")) Then
	ls_group_id = ""
Else
	ls_group_id = String(dw_parameters.GetItemNumber(1, "group_id")) 	
End IF
	
ls_header_string =	ls_customer_id + '~t' + &
							ls_customer_type + '~t' + &
							dw_parameters.GetItemString(1, "date_type") + '~t' + &
							String(dw_parameters.GetItemDate(1, "from_date"), "yyyy-mm-dd") + '~t' + &
							String(dw_parameters.GetItemDate(1, "to_date"), "yyyy-mm-dd") + '~t' + &
							dw_parameters.GetItemString(1, "cust_group_ind") + '~t' + &
							ls_group_owner + '~t' + &
							ls_group_id + '~t' 							
							
//li_rtn = iu_orp001.nf_orpo02cr_inq_reservatiion_item(istr_error_info, &
//																		ls_header_string, &
//																		ls_detail_string)
																		
li_rtn = iu_ws_orp3.uf_orpo02gr_inq_reservatiion_item(istr_error_info, &
																		ls_header_string, &
																		ls_detail_string)										

dw_detail.Reset()
li_rtn = dw_detail.Importstring(ls_detail_string)

//MessageBox("detail_string", "ls_detail_string =" + ls_detail_string)

Return True
end function

public function boolean wf_update ();Integer	li_rtn, li_item_count

Long		ll_RowCount, ll_count

String	ls_detail_string

istr_error_info.se_event_name = "wf_update"	

ll_RowCount = dw_detail.RowCount()

li_item_count = 0

For ll_count = 1 to ll_RowCount
	If dw_detail.IsSelected(ll_count) And (dw_detail.GetItemNumber(ll_count, "quantity") = 0) Then
		li_item_count = li_item_count + 1
		ls_detail_string +=  dw_detail.GetItemString(ll_count, "reservation_number") + '~t' + &
									String(dw_detail.GetItemNumber(ll_count, "gpo_date")) + '~t' + &
									String(dw_detail.GetItemNumber(ll_count, "item_number")) + '~t'
		If li_item_count >= 100 Then
//			li_rtn = iu_orp001.nf_orpo03cr_upd_reservation_item(istr_error_info, &
//													ls_detail_string)
			li_rtn = iu_ws_orp3.uf_orpo03gr_upd_reservation_item(istr_error_info, &
													ls_detail_string)
			li_item_count = 0
			ls_detail_string = ""
		End If											
	End If	
Next	

If ls_detail_string > "" Then
//	li_rtn = iu_orp001.nf_orpo03cr_upd_reservation_item(istr_error_info, &
//													ls_detail_string)
	li_rtn = iu_ws_orp3.uf_orpo03gr_upd_reservation_item(istr_error_info, &
													ls_detail_string)													
End If																
	
Return True
end function

on w_reservation_item_browse_updt.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_parameters=create dw_parameters
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_parameters
this.Control[iCurrent+4]=this.dw_detail
end on

on w_reservation_item_browse_updt.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_parameters)
destroy(this.dw_detail)
end on

event ue_postopen;call super::ue_postopen;iu_orp001 = CREATE u_orp001
iu_ws_orp3 = CREATE u_ws_orp3

wf_retrieve()
end event

event close;call super::close;If IsValid( iu_Orp001) Then Destroy( iu_orp001)
If IsValid( iu_ws_orp3) Then Destroy( iu_ws_orp3)
end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 60, this.height - dw_detail.y - 110)


long       ll_x = 60,  ll_y = 110

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)



end event

type cb_2 from u_base_commandbutton_ext within w_reservation_item_browse_updt
integer x = 3296
integer y = 268
integer width = 553
integer height = 108
integer taborder = 30
string text = "Zero All Selected"
end type

event clicked;call super::clicked;Long	ll_RowCount, &
		ll_sub

ll_RowCount = dw_detail.RowCount()

For ll_sub = 1 to ll_RowCount
	If dw_detail.IsSelected(ll_sub) Then
		dw_detail.SetItem(ll_sub, "quantity", 0) 
	End If
Next	
end event

type cb_1 from u_base_commandbutton_ext within w_reservation_item_browse_updt
integer x = 2875
integer y = 264
integer width = 379
integer height = 108
integer taborder = 20
fontcharset fontcharset = ansi!
string facename = "MS Sans Serif"
string text = "Select All"
end type

event clicked;call super::clicked;Long	ll_RowCount, &
		ll_sub

ll_RowCount = dw_detail.RowCount()

For ll_sub = 1 to ll_RowCount
	dw_detail.SelectRow(ll_sub, TRUE) 
Next	
end event

type dw_parameters from u_base_dw_ext within w_reservation_item_browse_updt
integer width = 3899
integer height = 392
integer taborder = 10
string dataobject = "d_reservation_item_parameters"
boolean border = false
end type

type dw_detail from u_base_dw_ext within w_reservation_item_browse_updt
integer x = 5
integer y = 396
integer width = 3899
integer height = 956
integer taborder = 10
string dataobject = "d_reservation_item_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;// Datawindow Row Selection (0,1,2,3)
// 	0 - No rows selected (default)
//	1 - One row selected
//	2 - Multiple rows selected
//	3 - Multiple rows with CTRL and ALT support
is_selection= "3"
end event

