HA$PBExportHeader$w_res_rollout.srw
$PBExportComments$This is the window to copy rows from one reservation to another
forward
global type w_res_rollout from w_base_response_ext
end type
type dw_main from datawindow within w_res_rollout
end type
type dw_copy from datawindow within w_res_rollout
end type
end forward

global type w_res_rollout from w_base_response_ext
int X=627
int Y=161
int Width=1802
int Height=1609
boolean TitleBar=true
string Title="Reservation Copy"
long BackColor=12632256
dw_main dw_main
dw_copy dw_copy
end type
global w_res_rollout w_res_rollout

type variables
STRING	is_CurrentYear, &
	is_CurrentMonth, &
	is_CurrentWeek,&
	is_Origianal_GPO_Date

w_reservations	iw_ParentWindow
end variables

event open;call super::open;Int		li_Counter

STRING	ls_StringParm

LONG		ll_Row, &
			ll_RowCount
			
u_string_functions	lu_string_function

iw_ParentWindow = Message.PowerObjectParm
iw_ParentWindow.TriggerEvent("ue_getdate")
ls_StringParm = Message.StringParm
is_CurrentYear = LEFT(ls_StringParm, 4)
is_CurrentMonth = MID(ls_StringParm, 5, 2)
is_CurrentWeek = MID(ls_StringParm, 7, 1)

dw_main.InsertRow(0)

IF Upper( is_CurrentWeek) = 'A' Then dw_main.SetItem( 1, "copy_type", 'M')

dw_main.SetItem(1, "reservation_year", is_currentyear)
dw_main.SetItem(1, "reservation_month", is_currentmonth)
dw_main.SetItem(1, "reservation_week", is_CurrentWeek)

is_Origianal_GPO_Date = is_Currentyear+is_currentmonth

dw_main.PostEvent("ue_typechanged")
iw_ParentWindow.TriggerEvent( "Ue_GetDetail")
dw_copy.ImportString( Message.StringParm)

IF lu_string_function.nf_IsEmpty(dw_copy.GetItemString(dw_copy.RowCount(), "product_code")) THEN
	dw_copy.DeleteRow(dw_copy.RowCount())
END IF

ll_RowCount = dw_Copy.RowCount()
For li_Counter = 1 to ll_RowCount
	dw_Copy.SetItem(li_Counter, 'detail_Errors', ' ' + Fill('V', 22))
Next

ll_Row = dw_copy.Find("ordered_units = 0.00 AND gen_units > 0.00", 1, dw_copy.RowCount())

DO WHILE ll_Row > 0
	dw_copy.SetItem(ll_Row, "ordered_units", dw_copy.GetItemNumber(ll_Row, "gen_units"))
	ll_Row = dw_copy.Find("ordered_units = 0.00 AND gen_units > 0.00", ll_Row + 1, dw_copy.RowCount())
LOOP
end event

on w_res_rollout.create
int iCurrent
call w_base_response_ext::create
this.dw_main=create dw_main
this.dw_copy=create dw_copy
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_main
this.Control[iCurrent+2]=dw_copy
end on

on w_res_rollout.destroy
call w_base_response_ext::destroy
destroy(this.dw_main)
destroy(this.dw_copy)
end on

event ue_base_ok;call super::ue_base_ok;Int		li_Counter

Long		ll_RowCount

String	ls_NewGPO

IF dw_main.GetItemString( 1, "copy_type") = 'M' Then
	ls_NewGPO = dw_main.GetItemString( 1, "reservation_year")+&
		dw_main.GetItemString( 1, "reservation_month") 
	IF ls_NewGPO <=  is_origianal_gpo_date  Then
		MessageBox( " Input Greater Date", "NEW Reservation date must be greater than original")
		Return
	END IF
END IF
IF dw_main.GetItemString( 1,"reservation_week") <> 'A' Then
	dw_main.SetItem( 1, "copy_type", 'W')
END IF
If dw_main.AcceptText() = -1 Then Return
If dw_copy.AcceptText() = -1 Then Return

dw_Copy.SetRedraw(False)
ll_RowCount = dw_Copy.RowCount()
For li_Counter = 1 to ll_RowCount
	dw_copy.SetItem(li_Counter, 'detail_Errors', dw_copy.GetItemString(li_Counter, &
				'detail_errors', Primary!, True))

Next
dw_Copy.SetRedraw(True)
// I know this is really really bad but there is no other way
This.iw_ParentWindow.dw_detail.Reset( )
This.iw_ParentWindow.dw_detail.ImportString( dw_copy.Describe("DataWindow.Data"))
CloseWithReturn( This, dw_Main.Describe("DataWindow.Data")+"~x")

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"Abort")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_res_rollout
int X=1418
int Y=253
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_res_rollout
int X=1418
int Y=141
int TabOrder=15
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_res_rollout
int X=1418
int Y=29
int TabOrder=10
end type

type dw_main from datawindow within w_res_rollout
int X=51
int Y=49
int Width=1276
int Height=365
int TabOrder=5
string DataObject="d_res_rollout_save"
boolean Border=false
boolean LiveScroll=true
end type

on itemchanged;CHOOSE CASE This.GetColumnName()
CASE	"reservation_year", "reservation_month", "reservation_week"
	This.PostEvent("ue_datechanged")
CASE 	"copy_type"
	This.PostEvent("ue_typechanged")
END CHOOSE
end on

on editchanged;This.AcceptText()

CHOOSE CASE This.GetColumnName()
CASE	"reservation_year", "reservation_month", "reservation_week"
	This.PostEvent("ue_datechanged")
CASE 	"copy_type"
	This.PostEvent("ue_typechanged")
END CHOOSE
end on

type dw_copy from datawindow within w_res_rollout
int X=51
int Y=417
int Width=1674
int Height=1053
int TabOrder=7
string DataObject="d_reservation_detail_copy"
boolean Border=false
boolean VScrollBar=true
boolean LiveScroll=true
end type

on itemfocuschanged;This.SelectText(1, 100)
end on

