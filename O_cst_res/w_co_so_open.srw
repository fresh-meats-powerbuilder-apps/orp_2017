HA$PBExportHeader$w_co_so_open.srw
$PBExportComments$Inquire response window for w_co_so
forward
global type w_co_so_open from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_co_so_open
end type
end forward

global type w_co_so_open from w_base_response_ext
integer x = 133
integer y = 392
integer width = 2656
integer height = 776
long backcolor = 12632256
dw_1 dw_1
end type
global w_co_so_open w_co_so_open

type variables
STRING 		is_window, &
		is_other_info

w_co_so		iw_parent

u_orp204		iu_orp204
u_ws_orp4		iu_ws_orp4

s_error		istr_error_info
end variables

forward prototypes
public function boolean nf_populate_dddws ()
end prototypes

public function boolean nf_populate_dddws ();//DATAWINDOWCHILD	ldw_DDDWChild
//
//dw_1.GetChild("customer_id", ldw_DDDWChild)
//w_Project_Data.wf_GetCustomerData(ldw_DDDWChild)
//
//// Populate Sales People drop down
//dw_1.GetChild("smancode", ldw_DDDWChild)
//w_Project_Data.wf_Getsalespeople(ldw_DDDWChild, message.is_smanlocation)
//
RETURN True
end function

event open;call super::open;DataWindowChild		ldwc_temp

String					ls_smanname, &
							ls_smancode, &
							ls_output
							
Integer					li_rtn		

u_string_functions	lu_string_functions

is_Window = "w_co_so"

iw_frame.wf_WindowIsValid(iw_parent, "w_co_so")

IF LEN(TRIM(Message.StringParm)) > 0 THEN
	dw_1.ImportString(Message.StringParm)
	ls_smancode = dw_1.GetItemString(1, 'smancode')
ELSE
   dw_1.InsertRow(0)
	dw_1.SetItem(1, "start_delv_date", Today())
	dw_1.SetItem(1, "end_delv_date", Today())
	ls_smancode = message.is_salesperson_code
END IF

dw_1.GetChild("smancode", ldwc_temp)
		
SELECT salesman.smanname  
	INTO :ls_smanname  
	FROM salesman  
WHERE salesman.smancode = :ls_smancode AND salesman.smantype = 'S';

ldwc_temp.Insertrow(0)
ldwc_temp.SetItem(1, "smancode", ls_smancode)
ldwc_temp.SetItem(1, "smanname", ls_smanname)
dw_1.SetItem(1, "smancode", ls_smancode)

This.Title = iw_parent.Title + ' Inquire'

istr_error_info.se_event_name = "constructor"

iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4
						
li_rtn = iu_ws_orp4.nf_orpo97fr(istr_error_info, &
											"C",ls_output)														
				
If li_rtn = 0 Then
	If lu_string_functions.nf_isempty(ls_output) Then
		messagebox("Informational","No Customer Groups available at this time.")
	else
		dw_1.GetChild('cfm_group_id', ldwc_temp)
		ls_output = '0' + '~t' + SPACE(40) + '~t' + SPACE(5) + '~t' + SPACE(10) + '~t' &
						+ SPACE(8) + '~t' + SPACE(3) + '~r~n' + ls_output
		ldwc_temp.ImportString(ls_output)
		ldwc_temp.SetSort("groupid A")
		ldwc_temp.Sort()
	end if
end if


end event

on w_co_so_open.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_co_so_open.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_base_ok;call super::ue_base_ok;dw_1.AcceptText()
is_other_info = dw_1.Describe("DataWindow.Data")

IF LEN(TRIM(String( dw_1.GetItemDate( 1, "start_delv_date"),"mm/yy/yy"))) < 1 OR &
		LEN(TRIM(String(dw_1.GetItemDate( 1, "end_delv_date"),"mm/yy/yy"))) < 1 Then
	MessageBox("User Error", "Please Enter a Delv Date > then today")
	Return
END IF

IF LEN( TRIM(dw_1.GetItemString( 1, "smancode"))) < 3 THEN
	MessageBox("User Error", "Please emter a TSR")
	Return
END IF

IF (dw_1.GetItemString( 1, "specific_orders_for_specific_customers") = 'Y' AND &
      LEN( TRIM(dw_1.GetItemString( 1, "customer_id"))) < 7) OR &
	(dw_1.GetItemString( 1, "specific_orders_for_specific_customers") = 'Y' AND &
	IsNull(dw_1.GetItemString( 1, "customer_id")) )  THEN
	MessageBox("User Error", "IF you check Specific Customer Type and Customer Please choose a customer")
	Return
End if    

IF IsNull( is_other_info) Then
	is_other_info = " "
END IF 

CloseWithReturn(This, is_other_info)

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "CANCEL")
end event

event ue_postopen;integer					li_rtn
string					ls_output
u_string_functions	lu_string_functions

DataWindowChild		ldwc_temp

dw_1.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_Temp)

dw_1.GetChild('billto_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_billtos.ShareData(ldwc_Temp)

dw_1.GetChild('corporate_customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_corps.ShareData(ldwc_Temp)

//istr_error_info.se_event_name = "constructor"
//				
//li_rtn = iu_orp204.nf_orpo97br_grp_maintenance_inq(istr_error_info, &
//												"C"," ",ls_output)			
//				
//If li_rtn = 0 Then
//	If lu_string_functions.nf_isempty(ls_output) Then
//		messagebox("Informational","No Customer Groups available at this time.")
//	else
//		dw_1.GetChild('cfm_group_id', ldwc_temp)
//		ls_output = '0' + '~t' + SPACE(40) + '~t' + SPACE(5) + '~t' + SPACE(10) + '~t' &
//						+ SPACE(8) + '~t' + SPACE(3) + '~r~n' + ls_output
//		ldwc_temp.ImportString(ls_output)
//		ldwc_temp.SetSort("groupid A")
//		ldwc_temp.Sort()
//	end if
//end if

//dw_1.SetItem(1,"cfm_group_id", 1)
dw_1.SetRedraw(True)


end event

event close;Destroy( iu_orp204)
Destroy u_ws_orp4
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_co_so_open
integer x = 2258
integer y = 292
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_co_so_open
integer x = 2258
integer y = 168
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_co_so_open
integer x = 2258
integer y = 48
end type

type cb_browse from w_base_response_ext`cb_browse within w_co_so_open
end type

type dw_1 from u_base_dw_ext within w_co_so_open
integer width = 2176
integer height = 644
integer taborder = 20
string dataobject = "d_co_so_open"
boolean border = false
end type

event itemchanged;call super::itemchanged;
Choose Case dwo.name
Case 'customer_type'
	CHOOSE CASE data
		CASE 'C'
			This.Setitem(1, 'corporate_customer_id', "")
			This.Object.corporate_customer_id.Visible = 1
			This.Object.billto_customer_id.Visible = 0
			This.Object.customer_id.Visible = 0
		CASE 'B'
			This.Setitem(1, 'billto_customer_id', "")
			This.Object.corporate_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 1
			This.Object.customer_id.Visible = 0
		CASE 'S'
			This.Setitem(1, 'customer_id', "")
			This.Object.corporate_customer_id.Visible = 0
			This.Object.billto_customer_id.Visible = 0
			This.Object.customer_id.Visible = 1
	END CHOOSE

	CASE 'customer_id'
		IF TRIM(data) > '' THEN
			SetItem(1, 'specific_orders_for_specific_customers', 'Y')
		ELSE
			SetItem(1, 'specific_orders_for_specific_customers', 'N')
		END IF
	END CHOOSE

end event

event ue_dwndropdown;DataWindowChild		ldwc_temp

u_project_functions	lu_project_functions

Choose Case This.GetColumnName()
	Case 'smancode'
		This.GetChild('smancode', ldwc_Temp)
		lu_project_functions.nf_gettsrs(ldwc_temp, &
				message.is_smanlocation)
	Case Else
End Choose
				

end event

