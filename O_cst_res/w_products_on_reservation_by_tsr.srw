HA$PBExportHeader$w_products_on_reservation_by_tsr.srw
forward
global type w_products_on_reservation_by_tsr from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_products_on_reservation_by_tsr
end type
type dw_header from u_base_dw_ext within w_products_on_reservation_by_tsr
end type
end forward

global type w_products_on_reservation_by_tsr from w_base_sheet_ext
integer width = 3237
integer height = 1564
string title = "Products on Reservation by TSR"
dw_detail dw_detail
dw_header dw_header
end type
global w_products_on_reservation_by_tsr w_products_on_reservation_by_tsr

type variables
String	is_openingstring

u_orp001		iu_orp001

u_ws_orp4       iu_ws_orp4

s_error	istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();String	ls_header_string, &
			ls_detail_string
		
Integer	li_rtn		
			
This.TriggerEvent('closequery')

OpenWithParm( w_products_on_res_by_tsr_resp, is_openingstring)

IF message.StringParm = 'Cancel' Then 
	Return False
ELSE
	is_openingstring = message.StringParm
	dw_header.Reset()
	dw_header.ImportString( is_openingstring)
END IF

							
//li_rtn = iu_orp001.nf_orpo04cr_prod_on_res_inq(istr_error_info, &
//																is_openingstring, &
//																ls_detail_string)										

li_rtn = iu_ws_orp4.nf_orpo04gr(istr_error_info, &
												is_openingstring, &
												ls_detail_string)	



dw_detail.Reset()
li_rtn = dw_detail.Importstring(ls_detail_string)


Return True
end function

on w_products_on_reservation_by_tsr.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_products_on_reservation_by_tsr.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;iu_orp001 = CREATE u_orp001

iu_ws_orp4 = CREATE u_ws_orp4

wf_retrieve()



end event

event close;call super::close;If IsValid( iu_Orp001) Then Destroy( iu_orp001)

If IsValid( iu_ws_orp4) Then Destroy( iu_ws_orp4)


end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 60, this.height - dw_detail.y - 150)
//

long       ll_x = 60,  ll_y = 150

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)




end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_Save')

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_Save')
end event

type dw_detail from u_base_dw_ext within w_products_on_reservation_by_tsr
integer x = 9
integer y = 556
integer width = 3182
integer taborder = 20
string dataobject = "d_product_on_res_by_tsr_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_header from u_base_dw_ext within w_products_on_reservation_by_tsr
integer x = 5
integer y = 12
integer width = 3191
integer height = 508
integer taborder = 10
string dataobject = "d_products_on_res_by_tsr_hdr"
boolean border = false
end type

