HA$PBExportHeader$w_reservations.srw
forward
global type w_reservations from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_reservations
end type
type dw_print from u_base_dw_ext within w_reservations
end type
type dw_header from u_base_dw_ext within w_reservations
end type
type str_datawindoworder from structure within w_reservations
end type
end forward

type str_DataWindowOrder from structure
    integer Prev_Array_Position
    integer row_number_in_DataWindow
    integer Next_Array_Position
end type

global type w_reservations from w_base_sheet_ext
integer width = 3081
integer height = 1420
string title = "Reservations"
boolean clientedge = true
event ue_get_detail_rowcount pbm_custom01
event ue_find_in_detail pbm_custom02
event ue_set_orderedunits pbm_custom03
event ue_detailselectline pbm_custom04
event ue_setaddedrows pbm_custom05
event ue_complete_order pbm_custom06
event ue_cancel pbm_custom07
event ue_set_price pbm_custom08
event ue_getdetail pbm_custom09
event ue_getdate pbm_custom10
event ue_setdetail pbm_custom11
event ue_setdate pbm_custom12
event ue_check_shipto pbm_custom14
event ue_detail_is_selected pbm_custom15
event ue_notepad ( )
event ue_gensales ( )
event ue_copy_rows ( )
event ue_set_priceoveride ( )
dw_detail dw_detail
dw_print dw_print
dw_header dw_header
end type
global w_reservations w_reservations

type variables
Private:
// CheckStatus will be removed later

Boolean	ib_JustInquired, &
			ib_generating_from_notepad 

String	is_OpenParm

s_Error	istr_s_Error

u_orp001		iu_orp001
u_orp002		iu_orp002

u_ws_orp3		iu_ws_orp3

u_ws_orp2		iu_ws_orp2

w_Sales_Order_Detail	iw_Sales_ThisOpened

//Vars to ddw data
String	is_agecode,&
	is_plant,&
	is_customers,&
	is_OUM,&
	is_Added_Rows,&
	is_LastCustomerID,&
	is_Roll_ind, &
	is_load_instruction, &
	is_additional_data_input, &
	is_dept_code, &
	is_dest_country, &
	is_store_door, &
	is_deck_code, &
	is_temp_additional_data


Long	il_RowsUpdated[],&
	il_NumberRowsSelected,&
	il_Number_Insert_Rows
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_change_view (character ac_view)
public subroutine wf_set_info (string as_string_to_parse)
public subroutine wf_filenew ()
public subroutine wf_defaultrow (long al_rowtodefault)
public function integer wf_reserv_replacerow (ref datawindow adw_destination, string as_source)
public function string wf_BuildHeaderString (ref datawindow adw_tobuild)
public function string wf_buildupdatestring (ref datawindow adw_datawindow)
public function integer wf_copy_rows ()
public subroutine wf_cancel ()
public function boolean wf_setcustomer_name_city (string data)
public function integer wf_reserv_replacerow_roll (ref datawindow adw_destination, string as_source)
public function boolean wf_update ()
public function boolean wf_notepad ()
public function integer wf_gensales ()
end prototypes

on ue_get_detail_rowcount;call w_base_sheet_ext::ue_get_detail_rowcount;Message.LongParm = dw_detail.RowCount()
end on

event ue_find_in_detail;call super::ue_find_in_detail;String	ls_Find_Value
long		ll_row

ls_Find_Value = String( Message.LongParm, "Address")
ll_row = dw_detail.Find( ls_Find_Value, 1, dw_Detail.RowCount())
IF ll_row > 0 THEN dw_detail.SetItem(ll_row, 'update_flag', 'U')
Message.LongParm	=	ll_row
end event

on ue_set_orderedunits;call w_base_sheet_ext::ue_set_orderedunits;Long	ll_Row,&
		ll_Value


ll_Row   = Message.WordParm
ll_Value = Long(String(Message.LongParm, "Address"))
dw_detail.SetItem( ll_Row, "sales_order_unit", ll_Value)

end on

on ue_detailselectline;call w_base_sheet_ext::ue_detailselectline;Boolean	lb_Select
Long		ll_Row

ll_Row = Message.LongParm
IF Message.WordParm > 0 Then 
	lb_Select = TRUE
ELSE
	lb_Select = False
END IF

dw_Detail.SelectRow( ll_Row, lb_Select)

end on

on ue_setaddedrows;call w_base_sheet_ext::ue_setaddedrows;il_Number_Insert_Rows = Message.WordParm
is_added_rows = String(Message.LongParm, "address")
end on

event ue_complete_order;Int		li_PageNumber,&
			li_rtn

Double	ld_Task_Number

Long		ll_current_row, &
			ll_shortage_row

String	ls_InPut_Header,&
			ls_Input_Detail,&
			ls_Sales_Info,&
			ls_MicroHelp, &
			ls_roll_info	

If dw_header.AcceptText() = -1 Then return 1
If dw_detail.AcceptText() = -1 Then return 1

ll_current_row = dw_detail.GetRow()

// Allow "accept as is" option
ll_shortage_row = dw_detail.Find("ordered_units > short_units and queue_status = 'A' and line_status = 'S'",1,dw_detail.RowCount() + 1)
If ll_shortage_row > 0 Then 
	OpenWithParm(w_res_shortage_response, 'S')
	IF Message.StringParm	=	'A' THEN
		Do
			dw_detail.SetItem(ll_shortage_row,"ordered_units",dw_detail.GetItemNumber(ll_shortage_row,"short_units"))
			dw_detail.SetItem(ll_shortage_row,"detail_errors","M"+mid(dw_detail.GetItemString(ll_shortage_row,"detail_errors"),2))   
			dw_detail.SetItemStatus(ll_shortage_row,0,Primary!,DataModified!)
			dw_detail.SetItem(ll_shortage_row,"update_flag","U")
			ll_shortage_row = dw_detail.Find("ordered_units > short_units and queue_status = 'A' and line_status = 'S'", ll_shortage_row, dw_detail.RowCount() + 1)
		Loop While ll_shortage_row > 0
	End If
	is_Roll_ind = 'N'
End If

If Not wf_update() Then return 1
is_Roll_ind = 'Y'

This.SetRedraw( False)
dw_Header.SetItem( 1, "update_flag", 'S')
dw_Header.AcceptText()

ls_Input_Header = This.wf_BuildHeaderString( dw_Header)
ls_Input_detail = This.wf_BuildUpdateString( dw_Detail)


istr_s_error.se_function_name = ""
istr_s_error.se_event_name = "ue_complete_order"
istr_s_error.se_procedure_name = "orpo57fr"

//li_rtn = iu_orp001.nf_orpo57ar( istr_s_error, ls_InPut_Header,&
//											ls_Input_Detail,&
//											ls_Sales_Info,&
//											ld_Task_Number,&
//  											li_PageNumber,&
//											ls_roll_info )

li_rtn = iu_ws_orp3.uf_orpo57fr( istr_s_error, ls_InPut_Header,&
											ls_Input_Detail,&
											ls_Sales_Info,&
											ls_roll_info )

IF li_Rtn  = 10 Then
	This.SetRedraw( True)
	Return  1
END IF

dw_Header.Reset()
dw_Header.ImportString( ls_InPut_Header)
ls_MicroHelp = iw_frame.wf_GetMicroHelp()
ls_input_Detail =  dw_header.GetItemString( 1, "customer_order_id")
ls_Input_Detail += "~t"+dw_header.GetItemString( 1, "week_num")
ls_Input_Detail += "~t"+dw_header.GetItemString( 1, "sales_person_code")
ls_Input_Detail += "~t"+dw_header.GetItemString( 1, "sales_location_code")
ls_Input_Detail += '~t'
If dw_detail.Find("queue_status <> ' '",1,dw_detail.RowCount()+1) > 0 Then
	ls_Input_Detail += "M"
Else
	ls_Input_detail += "N"
End If
ls_Input_Detail += "~t"+String(dw_header.GetItemDate( 1, "month"), 'mm-dd-yyyy')

This.TriggerEvent("ue_query", 0 , ls_Input_Detail)
dw_detail.ScrollToRow(ll_current_row)
SetMicroHelp( ls_MicroHelp)
This.SetRedraw( True)
Return 0
end event

on ue_cancel;call w_base_sheet_ext::ue_cancel;wf_cancel()
end on

event ue_set_price;call super::ue_set_price;Long	  ll_Row
Decimal ld_Value

String ls_Value

ll_Row   = Message.WordParm
ls_Value = String(Message.LongParm, "Address")
ld_Value = Dec(ls_Value)
dw_detail.SetItem( ll_Row, "sales_price", ld_Value)

end event

on ue_getdetail;call w_base_sheet_ext::ue_getdetail;Message.StringParm = dw_detail.Describe("DataWindow.Data")
end on

on ue_getdate;call w_base_sheet_ext::ue_getdate;String	ls_Month,&
			ls_Week,&
			ls_CurMonth


ls_Month = String(dw_header.GetItemDate(1, "month"),"YYYYMM")
ls_Week = dw_header.GetItemString(1, "week_num")
ls_CurMonth = ls_Month + ls_Week
Message.StringParm = ls_CurMonth

end on

on ue_setdetail;call w_base_sheet_ext::ue_setdetail;dw_detail.Reset()
dw_detail.ImportString(String(Message.LongParm,"Address"))
end on

event ue_detail_is_selected;call super::ue_detail_is_selected;if dw_detail.GetSelectedRow(0) > 0 tHEN 
	Message.StringParm = 'TRUE'
ELSE
	Message.StringParm = 'FALSE'
END IF

end event

event ue_notepad;call super::ue_notepad;wf_notepad()
end event

event ue_gensales;call super::ue_gensales;wf_gensales()
end event

event ue_copy_rows;call super::ue_copy_rows;wf_copy_rows()
end event

event ue_set_priceoveride;call super::ue_set_priceoveride;Long		ll_Row
String	ls_Value


ll_Row   = Message.WordParm
ls_Value = String(Message.LongParm, "Address")
dw_detail.SetItem( ll_Row, "price_override", ls_Value)

end event

public function boolean wf_retrieve ();String	ls_Return_Value, &
			ls_customerorderid, &
			ls_outputstring
			
u_string_functions	lu_string_functions

is_temp_additional_data = ''

This.TriggerEvent(CloseQuery!)
IF Message.ReturnValue = 1 Then
	Return False
ELSE
	ls_customerorderid = dw_header.GetItemString(1, 'customer_order_id')
	If lu_string_functions.nf_IsEmpty(ls_customerorderid) or &
			ls_customerorderid = '0000000' Then
		ls_outputstring = ''
	Else
		ls_outputstring = ls_customerorderid + '~t' + &
								dw_header.GetItemString(1, 'week_num') + '~t~t~t~t' + &
								String(dw_header.GetItemDate(1, 'month'), 'mm-dd-yyyy')
	End If
	OpenWithParm( w_cust_res_open, ls_outputstring)
	ls_Return_Value = Message.StringParm
	IF ls_Return_Value = "CANCEL" Then
		Return FALSE
	ELSE
		Return This.PostEvent("ue_Query", 0, ls_Return_Value)
	END IF
END IF
end function

public subroutine wf_change_view (character ac_view);IF dw_Header.Rowcount() < 1 then return

Choose Case ac_view
	Case 'M', 'I', 'A','N'
		dw_detail.DataObject = "d_reservation_long"
		dw_header.Modify("auto_rollover_ind.protect = 1")
		dw_header.Modify("auto_rollover_ind.Background.Color = 12632256")
	Case Else
		dw_detail.DataObject = "d_reservation_Short_op"
		dw_header.Modify("auto_rollover_ind.protect = 0")
		dw_header.Modify("auto_rollover_ind.Background.Color = 16777215")
End Choose

//This is to remove the single row that is retained on save
DataWindowChild ldwc_temp
dw_detail.GetChild('customer_id',ldwc_temp)
ldwc_temp.Reset()

// I am doing this here because it is the only column not showing the code
dw_detail.GetChild('ordered_units_uom', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve('OUOM')

end subroutine

public subroutine wf_set_info (string as_string_to_parse);Date		ldt_gpo_date

Long		ll_Tab_Pos

u_string_functions	lu_string_functions

ll_Tab_pos = lu_string_functions.nf_nPos( as_String_To_Parse, "~t", 1, 1)
dw_header.SetItem( 1, "customer_order_id", &
						Left( as_String_To_Parse, ll_Tab_pos - 1))
as_String_To_Parse = Mid( as_String_To_Parse, ll_Tab_pos + 1)

ll_Tab_pos = lu_string_functions.nf_nPos( as_String_To_Parse, "~t", 1, 1)
dw_header.SetItem( 1, "week_num", &
						Left( as_String_To_Parse, ll_Tab_pos - 1))
as_String_To_Parse = Mid( as_String_To_Parse, ll_Tab_pos + 1)

ll_Tab_pos = lu_string_functions.nf_nPos( as_String_To_Parse, "~t", 1, 1)
dw_header.SetItem( 1, "sales_person_code", &
						Left( as_String_To_Parse, ll_Tab_pos - 1))
as_String_To_Parse = Mid( as_String_To_Parse, ll_Tab_pos + 1)

ll_Tab_pos = lu_string_functions.nf_nPos( as_String_To_Parse, "~t", 1, 1)
dw_header.SetItem( 1, "sales_location_code", &
						Left( as_String_To_Parse, ll_Tab_pos - 1))
as_String_To_Parse = Mid( as_String_To_Parse, ll_Tab_pos + 1)

ll_Tab_pos = lu_string_functions.nf_nPos( as_String_To_Parse, "~t", 1, 1)
as_String_To_Parse = Mid( as_String_To_Parse, ll_Tab_pos + 1)

ll_Tab_pos = lu_string_functions.nf_nPos( as_String_To_Parse, "~t", 1, 1)
If ll_tab_pos = 0 Then
	ldt_gpo_date = Date(as_string_to_parse)
Else
	ldt_gpo_date = Date(Left( as_String_To_Parse, ll_Tab_pos - 1))
End If

dw_header.SetItem( 1, "month", ldt_gpo_date)

as_String_To_Parse = Mid( as_String_To_Parse, ll_Tab_pos + 1)
end subroutine

public subroutine wf_filenew ();Int	li_Year,&
		li_Month

String	ls_TempDate, &
			ls_customer_type

This.Title = 'Reservations'
IF Message.ReturnValue = 1 Then	Return 

If dw_header.RowCount() > 0 Then 
	ls_Customer_Type = "~t"+dw_Header.GetItemString(1,"customer_type")+"~t~t~t~t~t~t~tA"
Else
	ls_Customer_type = "~tS~t~t~t~t~t~t~tA"
End If

dw_header.Reset()
dw_Header.ImportString(ls_Customer_Type)

dw_detail.DataObject = "d_reservation_Short_op"
// This is to remove the single row that is retained on save
DataWindowChild	ldwc_temp
dw_detail.GetChild('customer_id',ldwc_temp)
ldwc_temp.Reset()

// I am doing this here because it is the only column not showing the code
dw_detail.GetChild('ordered_units_uom', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve('OUOM')

dw_Detail.InsertRow(0)

dw_Header.SetItem( 1, "sales_person_code", message.is_salesperson_code)

wf_defaultrow(1)

IF Month( Today()) + 1 > 12 then
	li_Year = Year(Today()) +1
	li_Month = 1
ELSE
	li_Month = Month( Today()) + 1
	li_Year = Year(Today())
END IF
ls_TempDate = +String(li_Month,"##")+'/01/'+String(li_Year,"####")

dw_Header.SetItem( 1, "month", Date(ls_TempDate))

dw_header.Modify("auto_rollover_ind.protect = 0")
dw_header.Modify("auto_rollover_ind.Background.Color = 16777215")

dw_Header.SetItem(1, "type_of_Order", 'R')
dw_Header.SetColumn(3)
dw_Header.SetFocus()
dw_header.ResetUpdate()
dw_detail.ResetUpdate()

is_temp_additional_data = ''

end subroutine

public subroutine wf_defaultrow (long al_rowtodefault);
IF 	dw_detail.GetItemString( al_Rowtodefault, "age") >	' ' Then Return
 
dw_detail.SetItem( al_Rowtodefault, "ordered_units", 0 )
dw_detail.SetItem( al_Rowtodefault, "product_code",	' ')
dw_detail.SetItem( al_Rowtodefault, "ordered_units_uom", ' ')
dw_detail.SetItem( al_Rowtodefault, "customer_id", ' ')
dw_detail.SetItem( al_Rowtodefault, "line_number", 0 )
dw_detail.SetItem( al_Rowtodefault, "queue_status", ' ')
IF al_rowtodefault < 2 Then
	dw_detail.SetItem( al_Rowtodefault, "age",	'B')
	dw_detail.SetItem( al_Rowtodefault, "delivery_date", '')
	dw_detail.SetItem( al_Rowtodefault, "plant", ' ')
	dw_detail.SetItem( al_Rowtodefault, "delivery_day",  '1' )
	dw_detail.SetItem( al_Rowtodefault, "scheduled_ship_date", Today())
	dw_detail.SetItem( al_Rowtodefault, "customer_id", dw_header.GetItemString( 1, "customer_id"))
ELSE
	dw_detail.SetItem( al_Rowtodefault, "age", dw_detail.GetItemString( al_Rowtodefault - 1, "age"))
	dw_detail.SetItem( al_Rowtodefault, "plant", dw_detail.GetItemString( al_rowtodefault -1 , "plant"))
	dw_detail.SetItem( al_Rowtodefault, "delivery_date", dw_detail.GetItemDate( al_rowtodefault - 1, "delivery_date"))
	dw_detail.SetItem( al_Rowtodefault, "scheduled_ship_date", dw_detail.GetItemDate( al_rowtodefault - 1, "scheduled_ship_date"))
	dw_detail.SetItem( al_Rowtodefault, "customer_id", dw_detail.GetItemString( al_rowtodefault - 1, "customer_id"))
END IF
dw_detail.SetItem( al_Rowtodefault, "update_flag", 'A')
dw_detail.SetItem( al_Rowtodefault, "detail_errors", '')
dw_detail.SetItem( al_Rowtodefault, "sales_price", 0)
dw_detail.SetItem( al_Rowtodefault, "div_po_num", '')
dw_detail.SetItem( al_Rowtodefault, "division_code", '')
dw_detail.SetItem( al_Rowtodefault, "line_status", '')
dw_detail.SetItem( al_Rowtodefault, "sales_order_unit", 0)
dw_detail.SetItem( al_Rowtodefault, "short_units", 0)
dw_detail.SetItem( al_Rowtodefault, "gen_units", 0)
dw_detail.SetItem( al_Rowtodefault, "sales_order_id", '')
dw_detail.SetItem( al_Rowtodefault, "change_ord_units", 0)
dw_detail.SetItem( al_Rowtodefault, "change_plant_code", '')
dw_detail.SetRedraw( true)
end subroutine

public function integer wf_reserv_replacerow (ref datawindow adw_destination, string as_source);Long		ll_CRLF,&
			ll_Tab,&
			ll_RowFound,&
			ll_Rows_Replaced,&
			ll_UpperBounds

String 	ls_Detail_Row,&
			ls_ordered_units,&
			ls_product_code,&
			ls_age,& 
			ls_ordered_units_uom,&
			ls_customer_id,&
			ls_line_number,&
			ls_queue_status,&
			ls_plant,&
			ls_delivery_day,&
			ls_update_flag,&
			ls_detail_errors,& 
			ls_sales_price,& 
			ls_delivery_date,& 
			ls_div_po_num,&
			ls_division_code,&
			ls_line_status,&
			ls_sales_order_unit,&
			ls_short_units,&
			ls_gen_units,&
			ls_sales_order_id,&
			ls_scheduled_ship_date,&
			ls_change_ord_units,&
			ls_change_plant_code
			
u_string_functions	lu_string_functions		
			
IF Not IsValid( adw_Destination) Then return -1
IF lu_string_functions.nf_IsEmpty( as_Source) Then Return -2
ll_Rows_Replaced = 1
ll_UpperBounds = UpperBound( il_rowsupdated)

If ll_UpperBounds = 1 Then
	If il_rowsupdated[1] = 0 Then Return 0
End If

Do 
	ll_CRLF = POS( as_Source,"~r~n")
	IF ll_CRLF > 0 Then
		ls_Detail_Row = Left( as_Source, ll_CRLF -1)
		as_Source = Mid( as_Source, ll_CRLF  + 1)	
	ELSE
		ls_Detail_Row = Trim( as_Source)
	END IF	

	ll_Tab = POS( ls_Detail_Row,"~t")
	ls_ordered_units = Left( ls_Detail_Row, ll_tab - 1)
	ls_Detail_Row = Mid( ls_Detail_Row, ll_Tab + 1)

	ll_Tab = POS( ls_Detail_Row,"~t")
	ls_product_code = Left( ls_Detail_Row, ll_tab - 1)
	ls_Detail_Row = Mid( ls_Detail_Row, ll_Tab + 1)
	ll_tab = POS( ls_Detail_Row, "~t")
	ls_age = Left( ls_Detail_Row, ll_Tab - 1)
	ls_Detail_Row = Mid( ls_detail_row, ll_Tab +1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_ordered_units_uom = Left( ls_Detail_Row, ll_Tab - 1)
	ls_Detail_Row = Mid( ls_detail_row, ll_Tab +1)
	ll_tab = POS( ls_Detail_Row, "~t")	
	ls_customer_id = Left( ls_Detail_Row, ll_Tab - 1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t") 
	ls_line_number = Left( ls_detail_row, ll_Tab - 1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)
	
	ll_tab = POS( ls_Detail_Row, "~t")
	ls_queue_status = Left( ls_detail_row, ll_Tab - 1)
	ls_Detail_Row = MID( ls_Detail_row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_plant = Left( ls_detail_row, ll_Tab - 1 )
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_delivery_day = Left( ls_Detail_Row, ll_Tab -1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_update_flag = Left( ls_detail_row, ll_Tab -1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	IF adw_Destination.DataObject = "d_reservation_long" Then 
		ll_tab = POS( ls_Detail_Row, "~t")
		ls_detail_errors =  Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)
	ELSE
		ls_detail_errors = Trim( ls_Detail_row)
	END IF
	
	IF adw_Destination.DataObject = "d_reservation_long" Then
		ll_tab = POS( ls_Detail_Row, "~t")
		ls_sales_price =  Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_delivery_date = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_div_po_num  = Left( ls_Detail_Row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_division_code  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_line_status  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_sales_order_unit  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_short_units  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_gen_units  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_sales_order_id  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_scheduled_ship_date  = Left( ls_detail_row, ll_Tab - 1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_change_ord_units  = Left( ls_detail_row, ll_Tab - 1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)
	
		ll_tab = POS( ls_Detail_Row, "~t")
		ls_change_plant_code  = Left( ls_detail_row, ll_Tab - 1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	END IF

	ll_RowFound = il_rowsupdated[ ll_Rows_Replaced] 

	IF ll_RowFound < 1 Then ll_RowFound = adw_Destination.InsertRow(0)
	adw_Destination.SetItem( ll_RowFound, "ordered_units",Long(ls_ordered_units))
	adw_Destination.SetItem( ll_RowFound, "product_code",	ls_product_code)
	adw_Destination.SetItem( ll_RowFound, "age",	ls_age)
	adw_Destination.SetItem( ll_RowFound, "ordered_units_uom", ls_ordered_units_uom)
	adw_Destination.SetItem( ll_RowFound, "customer_id", ls_customer_id)
	adw_Destination.SetItem( ll_RowFound, "line_number", Integer(ls_line_number))
	adw_Destination.SetItem( ll_RowFound, "queue_status", ls_queue_status)
	adw_Destination.SetItem( ll_RowFound, "plant", ls_plant)
	adw_Destination.SetItem( ll_RowFound, "delivery_day", ls_delivery_day)
	adw_Destination.SetItem( ll_RowFound, "update_flag", ls_update_flag)
	adw_Destination.SetItem( ll_RowFound, "detail_errors", ls_detail_errors)
	IF adw_Destination.DataObject = "d_reservation_long" Then
		adw_Destination.SetItem( ll_RowFound, "sales_price", Dec(ls_sales_price))
		adw_Destination.SetItem( ll_RowFound, "delivery_date", Date(ls_delivery_date))
		adw_Destination.SetItem( ll_RowFound, "div_po_num", ls_div_po_num)
		adw_Destination.SetItem( ll_RowFound, "division_code", ls_division_code)
		adw_Destination.SetItem( ll_RowFound, "line_status", ls_line_status)
		adw_Destination.SetItem( ll_RowFound, "sales_order_unit", Long(ls_sales_order_unit))
		adw_Destination.SetItem( ll_RowFound, "short_units", Long(ls_short_units))
		adw_Destination.SetItem( ll_RowFound, "gen_units", Long(ls_gen_units))
		adw_Destination.SetItem( ll_RowFound, "sales_order_id", ls_sales_order_id)
		adw_Destination.SetItem( ll_RowFound, "scheduled_ship_date", Date(ls_scheduled_ship_date))
		adw_Destination.SetItem( ll_RowFound, "change_ord_units", Long(ls_change_ord_units))
		adw_Destination.SetItem( ll_RowFound, "change_plant_code", ls_change_plant_code)
	END IF

	ll_Rows_Replaced ++

Loop While ll_CRLF > 0 AND LEN(TRIM(as_Source)) > 2 AND ll_Rows_Replaced <= ll_UpperBounds 

Return ll_Rows_Replaced -1

/*************************************************************************
* Written By: 	Jim Weier																 *
* Date:			05/22/96																	 *
* Description:																				 *
* Function takes as_Source and replaces existing rows in adw_Destination * 
* IF the rows do not exist in adw_Destination then they will be appended *
* Returns number of rows replaced ( or Appended)   							 *
* This Specifically works for the long form Dw for reservations			 *
* Return Codes																			 	 *
* -1 	Invalid dw																		 	 *
* -2 	Empty String 																	 	 *
*  0+ Number of rows inserted														    *
**************************************************************************/

end function

public function string wf_BuildHeaderString (ref datawindow adw_tobuild);Return dw_header.Describe("DataWindow.Data")

end function

public function string wf_buildupdatestring (ref datawindow adw_datawindow);Int	li_ArrayPos

Long	ll_StartPos,&
		ll_EndPos

String	ls_UpdateFlag

u_string_functions	lu_string_functions

il_rowsupdated = {0}
ll_StartPos = 1
ll_EndPos = Dw_detail.RowCount()

For ll_StartPos = 1 to ll_EndPos
	ls_UpdateFlag = adw_datawindow.GetItemString( ll_StartPos, "update_Flag")
	IF ls_UpDateFlag = 'U' OR ls_UpDateFlag = 'A'Then
		li_ArrayPos++
		il_rowsupdated[ li_arrayPos] = ll_StartPos
	END IF
Next

Return  lu_string_functions.nf_BuildUpdateString( dw_Detail)
end function

public function integer wf_copy_rows ();Int		li_return

Long		ll_x_pos,&
			ll_RowCount,&
			ll_LoopCount

String	ls_Month,&
			ls_Week,&
			ls_Detail,&
			ls_CurMonth,&
			ls_Header,&
			ls_DetailSaved,&
			ls_Saved_MicroHelp,&
			ls_Line_NUmber,&
			ls_Ordered_units


If dw_detail.AcceptText() = -1 Then return -1
If dw_header.AcceptText() = -1 Then return -1

ls_Month = String(dw_header.GetItemDate(1, "month"),"YYYYMM")
ls_Week = dw_header.GetItemString(1, "week_num")
ls_Detail = dw_detail.Describe("DataWindow.Data")
ls_CurMonth = ls_Month + ls_Week
Message.StringParm = ls_CurMonth

OpenWithPArm(w_res_rollout, This)

ls_Month = Trim(Message.StringParm)

IF ls_Month = "Abort" THEN RETURN -1

ll_x_pos = Pos( Message.StringParm, "~x")
ls_Header = dw_Header.Describe("dataWindow.Data")+Left( Message.StringParm, ll_x_Pos - 1 )
ls_DetailSaved  = dw_Detail.Describe("DataWindow.Data")
dw_Detail.SetRedraw(False)
ls_Detail = ''
ll_RowCOunt = dw_detail.Rowcount()
For ll_LoopCount = 1 to ll_RowCount
	ls_Line_NUmber = String(dw_detail.GetItemNumber( ll_LoopCount, "line_number")) 
	ls_Ordered_units = String(dw_detail.GetItemNumber(ll_LoopCount,"ordered_units"))
	ls_Detail+= ls_Line_NUmber+"~t"+ls_Ordered_units+"~r~n"
Next
istr_s_error.se_function_Name = "wf_copy"
istr_s_error.se_event_name = ""
istr_s_error.se_procedure_name = "orpo43fr"

//li_return = iU_orp003.nf_orpo43ar(istr_s_error, ls_Header, ls_Detail)

li_return = iu_ws_orp2.nf_orpo43fr(ls_Header, ls_Detail, istr_s_error)

IF li_return  = 0 Then
	ls_Saved_MicroHelp = iw_frame.wf_getmicrohelp()
	This.TriggerEvent("ue_query", 0, ls_Header)
	SetMicroHelp( ls_Saved_MicroHelp)
END IF
dw_detail.SetRedraw(True)
Return 1

end function

public subroutine wf_cancel ();Int	li_ret, &
		li_PageNumber

Double	ld_Task_Number

String	ls_header, &
			ls_detail, &
			ls_sales,&
			ls_week_num, &
			ls_roll_info

If dw_header.GetItemString(1, 'update_flag') = 'N' Then
	MessageBox("Cancel", "New Reservations cannot be canceled.")
	return 
End if


ls_week_num = dw_header.GetItemString(1, 'week_num')
If ls_week_num = 'A' Then
	If MessageBox("Warning!", "You will not be able to modify this Reservation again.  " + &
					"Are you sure?", Question!, YesNo!, 2) = 2 Then return
Else
	If MessageBox("Warning!", "You will not be able to modify " + &
			"existing lines for week " + ls_week_num + &
			" of this Reservation again.  " + &
			"Are you sure?", Question!, YesNo!, 2) = 2 Then return
End If

istr_s_error.se_event_name = "wf_cancel"

dw_header.SetItem(1, "update_flag", "C")

ls_Header = dw_header.Describe("DataWindow.Data")
ls_Detail = ""

//ar( istr_s_error, ls_Header,&
//											ls_Detail,&
//											ls_sales,&
//											ld_Task_Number,&
//  											li_PageNumber,&
//											ls_roll_info )

li_ret = iu_ws_orp3.uf_orpo57fr( istr_s_error, ls_Header,&
											ls_Detail,&
											ls_sales,&
											ls_roll_info )

IF li_ret = 10 Then return

This.PostEvent("ue_Query", 0, '')
return
end subroutine

public function boolean wf_setcustomer_name_city (string data);String	ls_customerName, ls_CustomerCity, ls_customerstate

u_project_functions	lu_project_functions

IF lu_project_functions.nf_ValidateCustomerstate( Data, dw_Header.GetItemString(1,"customer_type"),&
																			ls_CustomerName,ls_CustomerCity, ls_customerstate) < 1 Then
	dw_header.Object.customer_name.Text = ''
	dw_header.Object.customer_city.Text = ''	
	dw_header.Object.customer_state.Text = ''
	Return False
ELSE
	if isnull(ls_CustomerName) then
		dw_Header.Object.customer_name.Text = ''
	else
		dw_Header.Object.customer_name.Text = ls_CustomerName
	end if
	if isnull(ls_CustomerCity) then
		dw_Header.Object.customer_city.Text = ''
	else
		dw_Header.Object.customer_city.Text = ls_CustomerCity
	end if
	if isnull(ls_customerstate) then
		dw_Header.Object.customer_state.Text = ''
	else
		dw_Header.Object.customer_state.Text = ls_customerstate
	end if

	iw_frame.SetMicroHelp("Ready")
END IF
Return True
end function

public function integer wf_reserv_replacerow_roll (ref datawindow adw_destination, string as_source);Long		ll_CRLF,&
			ll_Tab,&
			ll_RowFound,&
			ll_Rows_Replaced,&
			ll_UpperBounds, &
			ll_RowCount

String 	ls_Detail_Row,&
			ls_ordered_units,&
			ls_product_code,&
			ls_age,& 
			ls_ordered_units_uom,&
			ls_customer_id,&
			ls_line_number,&
			ls_queue_status,&
			ls_plant,&
			ls_delivery_day,&
			ls_update_flag,&
			ls_detail_errors,& 
			ls_sales_price,& 
			ls_delivery_date,& 
			ls_div_po_num,&
			ls_division_code,&
			ls_line_status,&
			ls_sales_order_unit,&
			ls_short_units,&
			ls_gen_units,&
			ls_sales_order_id,&
			ls_scheduled_ship_date,&
			ls_change_ord_units,&
			ls_change_plant_code, &
			ls_find_string
			
u_string_functions	lu_string_functions			
			
IF Not IsValid( adw_Destination) Then return -1
IF lu_string_functions.nf_IsEmpty( as_Source) Then Return -2

ll_RowCount = adw_Destination.RowCount()

Do 
	ll_CRLF = POS( as_Source,"~r~n")
	IF ll_CRLF > 0 Then
		ls_Detail_Row = Left( as_Source, ll_CRLF -1)
		as_Source = Mid( as_Source, ll_CRLF  + 1)	
	ELSE
		ls_Detail_Row = Trim( as_Source)
	END IF	

	ll_Tab = POS( ls_Detail_Row,"~t")
	ls_ordered_units = Left( ls_Detail_Row, ll_tab - 1)
	ls_Detail_Row = Mid( ls_Detail_Row, ll_Tab + 1)

	ll_Tab = POS( ls_Detail_Row,"~t")
	ls_product_code = Left( ls_Detail_Row, ll_tab - 1)
	ls_Detail_Row = Mid( ls_Detail_Row, ll_Tab + 1)
	ll_tab = POS( ls_Detail_Row, "~t")
	ls_age = Left( ls_Detail_Row, ll_Tab - 1)
	ls_Detail_Row = Mid( ls_detail_row, ll_Tab +1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_ordered_units_uom = Left( ls_Detail_Row, ll_Tab - 1)
	ls_Detail_Row = Mid( ls_detail_row, ll_Tab +1)
	ll_tab = POS( ls_Detail_Row, "~t")	
	ls_customer_id = Left( ls_Detail_Row, ll_Tab - 1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t") 
	ls_line_number = Left( ls_detail_row, ll_Tab - 1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)
	
	ll_tab = POS( ls_Detail_Row, "~t")
	ls_queue_status = Left( ls_detail_row, ll_Tab - 1)
	ls_Detail_Row = MID( ls_Detail_row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_plant = Left( ls_detail_row, ll_Tab - 1 )
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_delivery_day = Left( ls_Detail_Row, ll_Tab -1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	ll_tab = POS( ls_Detail_Row, "~t")
	ls_update_flag = Left( ls_detail_row, ll_Tab -1)
	ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	IF adw_Destination.DataObject = "d_reservation_long" Then 
		ll_tab = POS( ls_Detail_Row, "~t")
		ls_detail_errors =  Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)
	ELSE
		ls_detail_errors = Trim( ls_Detail_row)
	END IF
	
	IF adw_Destination.DataObject = "d_reservation_long" Then
		ll_tab = POS( ls_Detail_Row, "~t")
		ls_sales_price =  Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_delivery_date = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_div_po_num  = Left( ls_Detail_Row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_division_code  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_line_status  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_sales_order_unit  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_short_units  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_gen_units  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_sales_order_id  = Left( ls_detail_row, ll_Tab -1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_scheduled_ship_date  = Left( ls_detail_row, ll_Tab - 1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

		ll_tab = POS( ls_Detail_Row, "~t")
		ls_change_ord_units  = Left( ls_detail_row, ll_Tab - 1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)
	
		ll_tab = POS( ls_Detail_Row, "~t")
		ls_change_plant_code  = Left( ls_detail_row, ll_Tab - 1)
		ls_Detail_Row = MID( ls_Detail_Row, ll_Tab + 1)

	END IF

	ls_find_string = "line_number = " + ls_line_number + " AND " &
						  + "product_code = '" + ls_product_code + "' AND " &
						  + "plant = '" + ls_plant + "'"
   
	ll_RowFound = adw_Destination.Find(ls_find_string, 1, ll_RowCount)
	
	IF ll_RowFound < 1 Then ll_RowFound = adw_Destination.InsertRow(0)
	adw_Destination.SetItem( ll_RowFound, "ordered_units",Long(ls_ordered_units))
	adw_Destination.SetItem( ll_RowFound, "product_code",	ls_product_code)
	adw_Destination.SetItem( ll_RowFound, "age",	ls_age)
	adw_Destination.SetItem( ll_RowFound, "ordered_units_uom", ls_ordered_units_uom)
	adw_Destination.SetItem( ll_RowFound, "customer_id", ls_customer_id)
	adw_Destination.SetItem( ll_RowFound, "line_number", Integer(ls_line_number))
	adw_Destination.SetItem( ll_RowFound, "queue_status", ls_queue_status)
	adw_Destination.SetItem( ll_RowFound, "plant", ls_plant)
	adw_Destination.SetItem( ll_RowFound, "delivery_day", ls_delivery_day)
	adw_Destination.SetItem( ll_RowFound, "update_flag", ls_update_flag)
	adw_Destination.SetItem( ll_RowFound, "detail_errors", ls_detail_errors)
	IF adw_Destination.DataObject = "d_reservation_long" Then
		adw_Destination.SetItem( ll_RowFound, "sales_price", Dec(ls_sales_price))
		adw_Destination.SetItem( ll_RowFound, "delivery_date", Date(ls_delivery_date))
		adw_Destination.SetItem( ll_RowFound, "div_po_num", ls_div_po_num)
		adw_Destination.SetItem( ll_RowFound, "division_code", ls_division_code)
		adw_Destination.SetItem( ll_RowFound, "line_status", ls_line_status)
		adw_Destination.SetItem( ll_RowFound, "sales_order_unit", Long(ls_sales_order_unit))
		adw_Destination.SetItem( ll_RowFound, "short_units", Long(ls_short_units))
		adw_Destination.SetItem( ll_RowFound, "gen_units", Long(ls_gen_units))
		adw_Destination.SetItem( ll_RowFound, "sales_order_id", ls_sales_order_id)
		adw_Destination.SetItem( ll_RowFound, "scheduled_ship_date", Date(ls_scheduled_ship_date))
		adw_Destination.SetItem( ll_RowFound, "change_ord_units", Long(ls_change_ord_units))
		adw_Destination.SetItem( ll_RowFound, "change_plant_code", ls_change_plant_code)
	END IF

	ll_Rows_Replaced ++

Loop While ll_CRLF > 0 AND LEN(TRIM(as_Source)) > 2 

ll_RowFound = adw_Destination.Find("line_number = 0 and pos(detail_errors, 'E',1)>0",1,ll_RowCount +1)
if ll_RowFound > 0 then Return 1

ll_RowFound = adw_Destination.Find("line_number = 0 and pos(detail_errors, 'E',1)=0",1,ll_RowCount +1)
do while (ll_RowFound > 0) 
		adw_destination.DeleteRow(ll_RowFound)
		ll_RowCount --
		ll_RowFound = adw_Destination.Find("line_number = 0 and pos(detail_errors, 'E',1)=0",1,ll_RowCount +1)
loop 

Return ll_Rows_Replaced -1

/*************************************************************************
* Written By: 	Jim Weier																 *
* Date:			05/22/96																	 *
* Description:																				 *
* Function takes as_Source and replaces existing rows in adw_Destination * 
* IF the rows do not exist in adw_Destination then they will be appended *
* Returns number of rows replaced ( or Appended)   							 *
* This Specifically works for the long form Dw for reservations			 *
* Return Codes																			 	 *
* -1 	Invalid dw																		 	 *
* -2 	Empty String 																	 	 *
*  0+ Number of rows inserted														    *
**************************************************************************/

end function

public function boolean wf_update ();Boolean	lb_firsttime, &
			lb_roll_selected

Int		li_PageNumber,&
			li_rtn

Double	ld_Task_Number

Long		ll_99th_Row,&
			ll_Current_Row, &
			ll_Pos_x, &
			ll_modified

String	ls_InPut_Header,&
			ls_Input_Detail,&
			ls_Sales_Info,&
			ls_Saved_Header,&
			ls_Saved_Detail,&
			ls_RPC_Detail_Returned,&
			ls_RPC_Detail, &
			ls_roll_info, &
			ls_button_ClassName, &
			ls_ordered_units_modified, &
			ls_detail_errors
			
u_string_functions lu_string_functions				

ll_Current_Row = Dw_detail.GetRow()

IF dw_Header.GetItemString( 1, "update_flag") = 'I' Then dw_Header.SetItem( 1, "update_flag", ' ')

If dw_header.AcceptText() = -1 Then return False
If dw_detail.AcceptText() = -1 Then return False

IF lu_string_functions.nF_IsEmpty( dw_Header.GetItemString( 1, "customer_id")) Then 
	SetMicroHelp("Please Enter a customer")
	This.SetRedraw( True)	
	Return False
END IF	

IF lu_string_functions.nF_IsEmpty( String(dw_Header.GetItemDate( 1, "month"))) Then 
	SetMicroHelp("Please Enter a GPO Date")
	This.SetRedraw( True)	
	Return False
END IF

This.SetRedraw( False)

dw_Header.SetItem( 1, "app_ind", "V")

ls_Saved_Detail = dw_detail.Describe("DataWindow.Data")
ls_Saved_Header = dw_Header.Describe("DataWindow.Data")

ls_Input_Header = This.wf_BuildHeaderString( dw_Header)
ls_Input_detail = wf_BuildUpdateString( dw_Detail)

IF lu_string_functions.nF_IsEmpty( ls_Input_detail) And lu_string_functions.nF_IsEmpty( ls_Input_header) Then 
	This.SetRedraw( True)	
	Return TRUE
END IF

ls_ordered_units_modified = 'N'
ll_modified = dw_detail.GetNextModified(0,Primary!) 
do while ll_modified > 0
	ls_detail_errors = dw_detail.GetItemString(ll_modified, "detail_errors")
	if mid(ls_detail_errors,1,1) = 'M' then
		ls_ordered_units_modified = 'Y'
	end if
	ls_detail_errors = lu_string_functions.nf_globalreplace( ls_detail_errors, 'E', 'M')
	dw_detail.SetItem(ll_modified, "detail_errors",ls_detail_errors) 
	ll_modified = dw_detail.GetNextModified(ll_modified,Primary!) 
loop 
	
if	(dw_Header.GetItemString(1, "update_flag") = ' ') and (ls_ordered_units_modified = 'Y') and &
		(is_Roll_ind = 'Y') then
	open (w_reservation_roll_resp)
	ll_Pos_x = POS( Message.StringParm, "~x")
	ls_button_ClassName = Mid( Message.StringParm, ll_Pos_x + 1)
	if ls_button_ClassName = "cb_base_cancel" then
		This.SetRedraw( True)	
		Return False
	else
		ls_roll_info = Left(Message.StringParm,ll_Pos_x - 1)
		lb_roll_selected = TRUE
	end if
else
	lb_roll_selected = FALSE
end if

istr_s_error.se_function_name = "wf_update"
istr_s_error.se_event_name = ""
istr_s_error.se_procedure_name = "orpo57fr"
lb_firsttime = True
DO 

	ll_99th_Row = lu_string_functions.nf_nPos( ls_Input_Detail, "~r~n",1,99)
	IF ll_99th_Row > 0 Then
		ls_RPC_Detail = Left( ls_Input_Detail, ll_99th_Row + 1)
		ls_Input_Detail = Mid( ls_Input_Detail, ll_99th_Row + 2)
	ELSE
		ls_Rpc_detail = ls_Input_Detail
		ls_Input_Detail = ''
	END IF

//	li_rtn = iu_orp001.nf_orpo57ar( istr_s_error, ls_InPut_Header,&
//											ls_RPC_Detail,&
//											ls_Sales_Info,&
//											ld_Task_Number,&
//  											li_PageNumber, &
//											ls_roll_info )

	li_rtn = iu_ws_orp3.uf_orpo57fr( istr_s_error, ls_InPut_Header,&
											ls_RPC_Detail,&
											ls_Sales_Info,&
											ls_roll_info )

	IF (li_rtn < 0) OR (li_Rtn  = 10) or (li_Rtn = 1) Then
		dw_detail.reset()
		dw_Header.Reset()
		dw_Header.ImportString(  ls_Saved_Header)
		dw_detail.ImportString( ls_Saved_Detail)
		This.SetRedraw( True)
		dw_detail.setRow( ll_Current_Row)
		dw_detail.Scrolltorow( ll_Current_Row)
		if len(ls_RPC_Detail_Returned) > 0 then
			dw_Header.Reset()
			dw_Header.ImportString( ls_InPut_Header)
			wf_reserv_replacerow( dw_detail, ls_RPC_Detail_Returned)
		end if	
//		Return False
	END IF
	ls_RPC_Detail_Returned = ls_RPC_Detail_Returned + ls_RPC_Detail //+ '~r~n'
	IF lb_firsttime Then
		dw_Header.Reset()
		dw_Header.ImportString( ls_InPut_Header)
		lb_firsttime = False
	End If
	//	ls_Input_Header = This.wf_BuildHeaderString( dw_Header)
Loop While Not lu_string_functions.nf_IsEmpty( ls_Input_Detail)

dw_detail.DeleteRow( dw_detail.RowCount())

if lb_roll_selected then
	wf_reserv_replacerow_roll( dw_detail, ls_RPC_Detail_Returned)
else
	wf_reserv_replacerow( dw_detail, ls_RPC_Detail_Returned)
end if

dw_Header.Reset()
dw_Header.ImportString( ls_InPut_Header)

If dw_detail.Find("queue_status = 'A'",1,dw_detail.RowCount()) > 0 Then
	iw_Frame.im_menu.mf_Enable('m_generatesalesorder')
Else
	iw_Frame.im_menu.mf_Enable('m_generatesalesorder')
End if

dw_Header.ResetUpdate()
dw_Detail.ResetUpdate()
dw_detail.InsertRow( 0)
This.SetRedraw( True)

dw_detail.Scrolltorow( ll_Current_Row)
is_customer_info = dw_header.GetItemString(1,"customer_id")+dw_header.GetItemString(1,"customer_type")
Return (li_rtn = 0)
end function

public function boolean wf_notepad ();String	ls_Return_String, &
			ls_salesid

Long		ll_selected_row

W_Sales_Order_Detail	lw_temp

IF dw_detail.Find("queue_status = 'A'",1,dw_detail.RowCount()) < 1 Then 
	MessageBox("Incorrect Order Status", "No lines are accepted.")
	Return FALSE
END IF
dw_detail.SelectRow(0,False)		
SetMicroHelp("All previously highlighted lines are deselected")
OpenWithParm(w_cust_res_notepad, This)

ls_Return_String = Message.StringParm
IF ls_Return_String = 'Abort' Then
	Return False
ELSE
	ib_generating_from_notepad = True
	CHOOSE CASE This.wf_GenSales()
	CASE 1
		Return FALSE
	CASE 2,5
		If ls_return_string = 'Y' Then
			// Cancel remaining quantity
			ll_selected_row = dw_detail.GetSelectedRow(0)
			Do   // There are always selected rows here
				dw_detail.SetItem(ll_selected_row,"ordered_units",0)
				dw_detail.SetItem(ll_selected_row,"update_flag",'U')
				ll_selected_row = dw_detail.GetSelectedRow(ll_selected_row)
			Loop While ll_selected_row > 0
			is_Roll_ind = 'N'
		 	wf_update()
			is_Roll_ind = 'Y' 
			// moved this from wf_gensales where in case 0,2,3 depending 
			//on li_rtn <> 0			 
			dw_detail.SelectRow(0,False)
		else
			dw_detail.SelectRow(0,False)
		End If
		
		if LEN(is_added_rows) > 0 THEN
			IF Not(IsValid( iw_sales_ThisOpened)) Then
				OpenSheetWithParm( iw_sales_ThisOpened, dw_detail.GetItemString(il_RowsUpdated[1],"sales_order_id"), "w_sales_order_detail",&
				 iw_frame,0, iw_Frame.im_Menu.iao_ArrangeOpen)				
			END IF
			iw_sales_ThisOpened.wf_add_rows( is_added_rows, 1, il_Number_Insert_Rows,1,3,4)
		END IF
		is_added_rows = ""
		RETURN TRUE
	CASE 0
		If ls_return_string = 'Y' Then
			// Cancel remaining quantity
			ll_selected_row = dw_detail.GetSelectedRow(0)
			Do  // There are always selected rows here
				dw_detail.SetItem(ll_selected_row,"ordered_units",0)
				dw_detail.SetItem(ll_selected_row,"update_flag",'U')
				ll_selected_row = dw_detail.GetSelectedRow(ll_selected_row)
			Loop While ll_selected_row > 0
			is_Roll_ind = 'N'
			wf_update()
			is_Roll_ind = 'Y'
			// moved this from wf_gensales where in case 0,2,3 depending 
			//on li_rtn <> 0			 
			dw_detail.SelectRow(0,False)
		else
			dw_detail.SelectRow(0,False)
		End If
		
		if LEN(is_added_rows) > 0 THEN
			ls_salesid	=	dw_detail.GetItemString(il_RowsUpdated[1],"sales_order_id")
			OpenSheetWithParm( lw_temp, ls_salesid, "w_sales_order_detail",&
			 						 iw_frame,0,Original!)				
			lw_temp.wf_add_rows( is_added_rows, 1, il_Number_Insert_Rows,1,3,4)
		END IF
		is_added_rows = ""
		RETURN TRUE
	CASE -1
		//USER ABORTED
		RETURN FALSE
	CASE ELSE
		MessageBox("No Lines Selected", "There are no lines selected to generate a sales order. You must create a new order.")
		Return FALSE
	END CHOOSE   
END IF
Return True



end function

public function integer wf_gensales ();Char								lc_Full_Load

DataWindowChild				ldwc_sales_id

Int								li_rtn, &
									li_counter,&
									li_Opened_window,&
									li_import, &
									li_count

Long								ll_POS_x, &
									ll_rowcount, &
									ll_new_row, &
									ll_current_Row,&
									ll_Row,&
									ll_line_number,&
									ll_Number_of_Selected_Rows,&
									ll_Last_Selected_Row, &
									ll_modified_row, &
									ll_3rdtab, &
									ll_4thtab, &
									ll_scroll_row, &
									ll_division_row
								

String							ls_Header,&
									ls_Detail,&
									ls_add_data, &
									ls_MessageStringParm,&
									ls_button_ClassName,&
									ls_Default_Values, &
									ls_plant, &
									ls_delivery_date, &
									ls_ship_Date, &
									ls_sales_id, &
									ls_customer_id, &
									ls_TransMode, &
									ls_division, &
									ls_gpo_date, &
									ls_temp_date, &
									ls_div_po, &
									ls_instruction_ind, &
									ls_load_instructions, & 
									ls_temp, &
									ls_division_priority, &
									ls_type_code, &
									ls_detail_errors, ls_protected
									
w_Sales_Order_Detail			lw_Sales_Detail

u_string_functions			lu_string_functions
u_project_functions			lu_project_functions

ll_current_row = dw_detail.GetRow()
ll_Row = dw_detail.GetSelectedRow(0)
ll_scroll_row = dw_detail.GetRow()

IF ll_row < 1 Then
	SetMicroHelp("No lines selected to generate order.")
	Return -1
End if
is_Roll_ind = 'N'
If Not This.wf_Update() Then Return -1
is_Roll_ind = 'Y'

ll_rowcount = dw_detail.RowCount()

ls_plant = dw_detail.GetItemString(ll_row, 'plant')

If lu_string_functions.nf_IsEmpty(ls_plant) Then
	MessageBox("Generate Reservation", &
		"The Plant is a Required Field to Generate a Reservation.")
	return -1
End if
	
ls_ship_Date = String(dw_detail.GetItemDate(ll_row, 'scheduled_ship_date'), &
		'mm/dd/yyyy')


If lu_string_functions.nf_IsEmpty(ls_ship_Date) Then
	MessageBox("Generate Reservation", &
		"The Ship Date is a Required Field to Generate a Reservation.")
	return -1
End if
	
ls_delivery_Date = String(dw_detail.GetItemDate(ll_row, 'delivery_date'), &
		'mm/dd/yyyy')

If lu_string_functions.nf_IsEmpty(ls_ship_Date) Then
	MessageBox("Generate Reservation", &
		"The Delivery Date is a Required Field to Generate a Reservation.")
	return -1
End if

//Make sure that all selected rows do not have a line status of modified
ll_modified_row	=dw_Detail.Find( "IsSelected() and line_status = '" + "M'" ,ll_Row, ll_RowCount)
IF ll_modified_row > 0 Then
	MessageBox("Generate Sales Order", &
			"Cannot Generate Sales Order as Modified Rows have been detected " + &
			"Line Numbers in Green Background are the Modified Rows that have been Deselected.")
	DO
		dw_Detail.SelectRow(ll_modified_row, False)
		ll_modified_row	=	dw_Detail.Find( "IsSelected() and line_status = '" + "M'" ,ll_modified_Row, ll_RowCount)	
	LOOP WHILE ll_modified_row > 0 			
	Return -1
END IF	

// Make sure that all selected rows have the same plant
// and the same ship date and the same delivery date.
IF dw_Detail.Find( "IsSelected() and (plant <> '" + ls_plant + &
		"' Or scheduled_ship_date <> Date('" + ls_ship_Date + &
		"') Or delivery_date <> Date('" + ls_delivery_Date + &
		"'))", ll_Row, ll_RowCount) > 0 Then
	MessageBox("Generate Sales Order", &
			"The Plant, the Ship Date, and the Delivery Date must be " + &
			"the same for all selected rows.")
	Return -1
END IF	

lc_Full_Load = 'U'			
IF ll_Number_of_Selected_Rows = 1 Then
	IF NOT IsNull(dw_Detail.GetItemNumber(ll_Last_Selected_Row, "ordered_units")) THEN
		IF dw_Detail.GetItemDecimal(	ll_Last_Selected_Row, "ordered_units") = 1.00 Then
			IF dw_detail.GetItemString( ll_Last_Selected_Row, "ordered_units_uom") = '03' Then
				lc_Full_Load = 'F'			
			END IF
		END IF
	END IF
END IF

ls_customer_id = dw_detail.GetItemString( ll_row, "customer_id")
//change to determine the division using the division from all the lines.
ls_division_priority = ' '
ll_division_row = dw_detail.Getselectedrow(ll_division_row)

Do While ll_division_row > 0
	ls_temp = dw_detail.Getitemstring(ll_division_row, 'division_code') 
	SELECT tutltypes.type_code  
    INTO :ls_type_code
    FROM tutltypes  
   WHERE ( tutltypes.type_short_desc = :ls_temp ) AND  
         ( tutltypes.record_type = 'MULTIDIV' );
	
	if ls_type_code < ' ' then
		if ls_division_priority = ' ' then
			if ls_temp = '99' then
				//do nothing
			else
				ls_division = ls_temp 
			end if
		end if
   else
		if ls_division_priority = ' ' then
			ls_division_priority = ls_type_code
			ls_division = ls_temp 
		else
			if ls_type_code < ls_division_priority then
				ls_division_priority = ls_type_code
				ls_division = ls_temp
			end if
		end if 
	end if 
	
	ll_division_row = dw_detail.Getselectedrow(ll_division_row)
LOOP

if ls_division > ' ' Then
   //do nothing
else
  ls_division = dw_detail.GetItemString( ll_row, "division_code")
end if

ls_div_po = dw_detail.GetItemString( ll_row, "div_po_num")

ls_TransMode = dw_header.GetItemString( 1, "trans_mode")

is_additional_data_input = "R" + "~t" + ls_delivery_date + '~t' + &
							ls_ship_date + '~t' + &
							ls_plant + '~t' + &
							ls_customer_id + '~t' + &
							lc_Full_Load + '~t' + &
							ls_TransMode + '~t' + &
							ls_division + '~t' + &
							ls_div_po + '~t' + &
							dw_header.GetItemString( 1, 'Sales_person_code')
							
if is_temp_additional_data <> '' then
	is_additional_data_input += '~t~t' + is_temp_additional_data
end if

OpenWithParm(w_Additional_Data, this)
ls_MessageStringParm = is_additional_data_input

// Message.StringParm is populated with the data+ "~x"+Classname 
// of the button pressed

ll_Pos_x = POS( ls_MessageStringParm, "~x")
ls_button_ClassName = Mid( ls_MessageStringParm, ll_Pos_x + 1)
ls_add_data = Left(ls_MessageStringParm,ll_Pos_x - 1) 
ls_instruction_ind = Mid(ls_messageStringParm,ll_Pos_x - 1,1)

If ls_button_ClassName = "Cancel" Then return -1

istr_s_error.se_function_name = "wf_GenSales"
istr_s_error.se_event_name = ""
istr_s_error.se_procedure_name = "orpo39ar"

iw_sales_ThisOpened = lw_Sales_Detail

DataStore lds_temp
lds_temp = Create DataStore
lds_temp.DataObject = dw_detail.DataObject

lds_temp.Object.Data = dw_detail.Object.Data.Selected
ls_detail = lds_temp.Object.DataWindow.Data
ls_header = dw_header.Object.DataWindow.Data

// This changes gpo_date from a date format into "yyyymm" format for the RPC
ll_3rdtab = lu_string_functions.nf_npos(ls_header,"~t",1,3)
ll_4thtab = lu_string_functions.nf_npos(ls_header,"~t",1,4)

ls_gpo_date = Mid(ls_header,ll_3rdtab + 1,ll_4thtab - ll_3rdtab - 1)
ls_temp_date = String(Date(ls_gpo_date),"yyyymm")
ls_header = Left(ls_header,ll_3rdtab) + ls_temp_date + Mid(ls_header,ll_4thtab)
//dmk sr5571 add dest country
ls_add_data += '~t' + is_load_instruction + '~t' + is_dept_code + '~t' + is_dest_country  + '~t' &
								+ is_store_door + '~t' + is_deck_code + '~t'
//
is_temp_additional_data = ls_add_data
//								
//li_rtn = iu_orp003.nf_orpo39ar( istr_s_error, 'R', ls_Header, ls_Detail, ls_add_data, '')

li_rtn = iu_ws_orp2.nf_orpo39fr('R', ls_Header, ls_Detail, ls_add_data,'',istr_s_error)

/*return code 1	=	0	Successful Gen
1	Information/No Gen
2	Over Under Wt/ Succesful Gen (going into S/O)
3	Error No Gen
5	PA Resolve
6  Open on sales order instruction tab
10	Deadlock No Gen */

Choose Case li_rtn
	Case 0,1,3
		lds_temp.Reset()
		li_import  = lds_temp.ImportString(ls_detail)
		IF li_import > 0 Then 
//			dw_detail.Object.Data.Selected = lds_temp.Object.Data
			lu_project_functions.nf_replace_rows(dw_detail, ls_detail)
		END IF
		dw_detail.GetChild('sales_order_id', ldwc_sales_id)
		li_counter = dw_detail.GetSelectedRow(0)
		If li_counter > 0 Then
			ls_sales_id = dw_detail.GetItemString(li_counter,'sales_order_id')
		End If
		li_count = 0
		Do While li_counter > 0		
			ll_new_row = ldwc_sales_id.InsertRow(0)
			IF ll_new_row > 0 Then
				ll_line_number = dw_detail.GetItemNumber(li_counter, 'line_number')
				ldwc_sales_id.SetItem(ll_new_row, 'Line_number', ll_line_number)
				ldwc_sales_id.SetItem(ll_new_row, 'sales_id', ls_sales_id)
			End IF
			//
			li_counter = dw_detail.GetSelectedRow(li_counter)
		Loop

		This.SetRedraw( True)
		// Successful GEN protect everything in the header
		dw_Header.SetItem( 1, "fields_in_error",Fill("V",20))
		dw_detail.ReSetUpdate()
		dw_header.ReSetUpdate()
		dw_Detail.ScrolltoRow(ll_Current_Row)
		// RPC returns 1 if price override needed
//    keep rows selected since notepad-cancel remaining needs selected rows 		
		if ib_generating_from_notepad then
		// leave lines selected
		else
			dw_detail.SelectRow(0, False)
			ib_generating_from_notepad = False
			
			//
//			ll_RowCount  = dw_detail.RowCount()
//			
//			For li_Counter =  1 to ll_RowCount
//				If Mid(dw_detail.Getitemstring(li_Counter, "detail_errors") , 18, 1) = 'E' then
//					dw_detail.SelectRow(li_Counter, True)
//				else
//					dw_detail.SelectRow(li_Counter, False)
//				end if
//			Next
			//
		end if
		//
		if li_rtn = 0 then
			is_temp_additional_data = ''
		end if
		//
		Destroy lds_temp
		If dw_detail.RowCount() > 0 then
			For li_counter = 1 to dw_detail.RowCount()
				If Mid(dw_detail.GetItemString(li_counter, "detail_errors"), 18, 1) = 'E' then
					dw_header.modify("price_override.Visible = '1'")
					dw_header.modify("reference_num_t.Visible = '1'")
				end if
				If Mid(dw_detail.GetItemString(li_counter, "detail_errors"), 18, 1) = 'M' then
					dw_header.modify("price_override.Visible = '1'")
					dw_header.modify("reference_num_t.Visible = '1'")
				end if
			Next
		End if
		Return li_rtn
	Case 2, 5, 6
		lds_temp.Reset()
		li_import = lds_temp.ImportString(ls_detail)
		IF li_import > 0 Then
//			dw_detail.Object.Data.Selected = lds_temp.Object.Data
			lu_project_functions.nf_replace_rows(dw_detail, ls_detail)
		END IF 
		dw_detail.GetChild('sales_order_id', ldwc_sales_id)
		li_counter = dw_detail.GetSelectedRow(0)
		If li_counter > 0 Then
			ls_sales_id = dw_detail.GetItemString(li_counter,'sales_order_id')
		End If
		Do While li_counter > 0		
			ll_new_row = ldwc_sales_id.InsertRow(0)
			IF ll_new_row > 0 Then
				ll_line_number = dw_detail.GetItemNumber(li_counter, 'line_number')
				ldwc_sales_id.SetItem(ll_new_row, 'Line_number', ll_line_number)
				ldwc_sales_id.SetItem(ll_new_row, 'sales_id', ls_sales_id)
			End IF
			li_counter = dw_detail.GetSelectedRow(li_counter)
		Loop
		
		is_temp_additional_data = ''

		li_counter = dw_detail.GetSelectedRow(0)
		
		if (ls_instruction_ind = 'Y') and (li_rtn = 6) then
			ls_sales_id = ls_sales_id + '~t' + 'I'
		end if
		
		li_Opened_window = OpenSheetWithParm( iw_sales_ThisOpened, &
									ls_sales_id, "w_sales_order_detail", &
								 iw_frame, 0, iw_Frame.im_Menu.iao_ArrangeOpen)				
      IF li_Opened_window < 0 Then
			MessageBox ( "Window Failure", "The application was unable to " + &
							 "open a sales window, please manually open a sales " + &
							 "window and inquire on " + &
							 dw_detail.GetItemString(li_counter, "sales_order_id") + ".") 
		END IF
		This.SetRedraw(True)
		dw_detail.ReSetUpdate()
		dw_header.ReSetUpdate()
		dw_Detail.ScrolltoRow(ll_Current_Row)
//    keep rows selected since notepad-cancel remaining needs selected rows		
		if ib_generating_from_notepad then
		// leave lines selected
		else
			dw_detail.SelectRow(0, False)
			ib_generating_from_notepad = False
		end if
		Destroy lds_temp
		return li_rtn
	Case ELSE
		This.SetRedraw(True)	
		dw_detail.ReSetUpdate()
		dw_header.ReSetUpdate()
		dw_Detail.ScrolltoRow(ll_Current_Row)
		Destroy lds_temp
		Return li_rtn
END CHOOSE

IF ll_scroll_row > 0 And ll_scroll_row <= dw_detail.RowCount() Then
	dw_detail.SetRow(ll_scroll_row)
	dw_detail.ScrollToRow(ll_scroll_row)
End If


		




Destroy lds_temp
Return 1
end function

event ue_query;call super::ue_query;DataWindowChild	ldwc_TempStorage
Int		li_PageNumber,&
			li_rtn, li_rowcount, li_count

String	ls_OpenString,&
			ls_InPut_Header,&
			ls_Input_Detail,&
			ls_Sales_Info,&
			ls_Saved_Header, &
			ls_roll_info, ls_detail_error, ls_fields_in_error, ls_fieldserror
			
Double	ld_Task_Number

u_string_functions	lu_string_functions

ls_OpenString = String(Message.LongParm, "address")

If Not lu_string_functions.nf_IsEmpty(ls_openstring) Then 
	wf_change_view( mid( ls_OpenString,len(ls_openstring) - 11, 1))
	wf_set_info( ls_OpenString)
End If

This.SetRedraw(False)

istr_s_error.se_event_name = "Ue_Query"
istr_s_error.se_procedure_name = "orpo57fr"

dw_header.SetItem( 1, "update_flag", "I")
dw_Header.SetItem( 1, "app_ind", "V")
dw_Header.AcceptText()
Long ll_temp
ll_temp = dw_header.RowCount()
ls_Input_Header = dw_Header.Describe("DataWindow.Data")
dw_detail.GetChild( "sales_order_id" ,ldwc_TempStorage)
dw_Detail.Reset()

//Do
//	li_rtn = iu_orp001.nf_orpo57ar( istr_s_error, ls_InPut_Header,&
//											ls_Input_Detail,&
//											ls_Sales_Info,&
//											ld_Task_Number,&
//											li_PageNumber,&
//											ls_roll_info)
	li_rtn = iu_ws_orp3.uf_orpo57fr( istr_s_error, ls_InPut_Header,&
											ls_Input_Detail,&
											ls_Sales_Info,&
											ls_roll_info)
	IF li_Rtn  = 10 Then
		dw_detail.ResetUpdate()
		dw_Header.ResetUpdate()
	END IF

	li_Rtn = dw_Detail.ImportString( ls_Input_Detail)
	ldwc_TempStorage.ImportString( ls_Sales_Info)
	IF lu_string_functions.nf_IsEmpty( ls_Saved_Header) Then ls_Saved_Header = ls_Input_Header
//Loop While ld_Task_Number > 0 AND li_PageNumber > 0

//
//li_rowcount = dw_Detail.RowCount()
//if li_rowcount > 0 then
//	for li_count = 1 to li_rowcount
//		ls_detail_error = Mid(dw_Detail.GetItemString(li_count, 'detail_errors'), 27, 1)
//		if ls_detail_error = 'V'  then
//			ls_fields_in_error = dw_header.GetItemString(1, 'fields_in_error')
//			ls_fieldserror = Replace(ls_fields_in_error, 8, 1, 'V')
//			dw_header.SetItem(1, 'fields_in_error', ls_fieldserror)
//		end if 
//	next
//end if 
//


IF Not lu_string_functions.nf_IsEmpty( ls_Saved_Header) Then
	dw_header.Reset()
	dw_Header.ImportString( ls_Saved_Header)
	This.Title = 'Reservations - ' + dw_header.GetItemString( 1, 'customer_order_id')
END IF

String	ls_salesman, &
			ls_salesloc, &
			ls_text

ls_text = dw_header.GetItemstring(1,"sales_person_code");			
SELECT salesman.smanname, 
			salesman.smanloc
	INTO :ls_salesman, 
			:ls_salesloc
	FROM salesman  
WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';

dw_header.object.sales_person_name.Text = ls_salesman
dw_header.object.sales_location_code.Text = ls_salesloc
	
String ls_service_center
SELECT locations.location_name
  INTO :ls_service_center
  FROM locations
WHERE locations.location_code = :ls_salesloc;
	
dw_header.Object.sales_location_name.Text = ls_service_center

// Change to insert a blank row no matter what -- IBDKCDE 01/11/1997
//IF ls_LineStatus = 'U' Or ls_LineStatus = 'A' Then
dw_detail.InsertRow( 0)
//END IF

If dw_detail.Find("queue_status = 'A'",1,dw_detail.RowCount()) > 0 Then
	iw_frame.im_menu.mf_Enable('m_generatesalesorder')
Else
	iw_frame.im_menu.mf_Disable('m_generatesalesorder')
End if
ib_JustInquired = TRUE
dw_Header.SetItem(1,"update_flag"," ")
dw_detail.ResetUpdate()
dw_Header.ResetUpdate()
is_customer_info = dw_header.GetItemString(1,"customer_id")+dw_header.GetItemString(1,"customer_type")
This.wf_setcustomer_name_city ( dw_header.GetItemString(1,"customer_ID") )
This.SetRedraw(True)
end event

on ue_getcustomerid;call w_base_sheet_ext::ue_getcustomerid;Message.StringParm = dw_header.GetItemString(1, "customer_id")

end on

on ue_getcustomertype;call w_base_sheet_ext::ue_getcustomertype;Message.StringParm = dw_header.GetItemString(1, "customer_type")
end on

event open;call super::open;is_openparm = Message.StringParm
ib_faxing_available = TRUE

is_typecode = Message.is_Smanlocation

ib_generating_from_notepad = False 

end event

event closequery;call super::closequery;Long  ll_RowCOunt
ll_RowCount = dw_detail.RowCount()

IF dw_detail.Find( "(line_status='M' OR line_status = 'N' OR line_status = 'S') And (queue_status = 'A' or queue_status = ' ')",1, &
		ll_RowCount) > 0 Then 
	IF MessageBox( "Complete Reservation","Reservation must be sent to Scheduling.  Do you wish to complete the reservation?", StopSign!, YesNo!,1) = 1 Then
		This.TriggerEvent("ue_Complete_order")
		Return Message.ReturnValue
	ELSE
		Return 1
	END IF
END IF

end event

event ue_postopen;call super::ue_postopen;u_string_functions	lu_string_functions
u_project_functions	lu_project_functions

iu_orp001 = Create u_orp001
iu_orp003 = Create u_orp003
iu_ws_orp2 = Create u_ws_orp2
iu_ws_orp3 = Create u_ws_orp3
lu_project_functions.nf_set_s_error( istr_s_error)
is_Roll_ind = 'Y'

SetMicroHelp("Ready")
IF lu_string_functions.nf_IsEmpty(is_openparm) Then
	IF NOT wf_retrieve() Then This.wf_fileNew()
ELSE
	This.PostEvent("ue_Query",0, is_OpenParm)
END IF
end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_disable('m_cancelorder')

iw_frame.im_menu.mf_disable('m_print')

iw_frame.im_Menu.mf_Disable( "m_copyrows")
iw_Frame.Im_Menu.mf_Disable( "m_notepad")
iw_Frame.Im_Menu.mf_Disable( "m_completeorder")
iw_Frame.im_Menu.mf_Disable("m_generatesalesorder")


end on

event close;call super::close;Destroy( iu_Orp001)
Destroy( iu_orp003)
Destroy(u_ws_orp2)
end event

on w_reservations.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_print=create dw_print
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_print
this.Control[iCurrent+3]=this.dw_header
end on

on w_reservations.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_print)
destroy(this.dw_header)
end on

event activate;call super::activate;iw_frame.im_menu.mf_enable('m_cancelorder')


iw_frame.im_menu.mf_enable('m_print')

iw_frame.im_Menu.mf_enable( "m_copyrows")
iw_Frame.Im_Menu.mf_Enable( "m_notepad")
iw_Frame.im_Menu.mf_Enable( "m_completeorder")
If dw_detail.Find("queue_status = 'A'",1,dw_detail.RowCount()) > 0 Then 
		iw_Frame.im_Menu.mf_Enable("m_generatesalesorder")
End if

end event

event ue_fileprint;call super::ue_fileprint;DataWindowChild	ldwc_print_header, &
						ldwc_print_detail

Int		li_Counter

Long		ll_RowCount

String	ls_products, &
			ls_product_code, &
			ls_product_description, &
			ls_customer, &
			ls_customer_city, &
			ls_temp, &
			ls_transmode, &
			ls_TSR_Name, &
			ls_TSR_Location, &
			ls_plant, &
			ls_customer_name, &
			ls_LastPlant, &
			ls_PlantDescr
			
u_string_functions	lu_string_functions			

IF Not ib_print_ok Then Return
SetMicroHelp("Preparing Reservation for printing ...")
SetPointer(HourGlass!)

////////////////////////////////////
//	Get Detail                    //
//////////////////////////////////
dw_print.GetChild('dw_detail', ldwc_print_detail)

ldwc_print_detail.ImportString( dw_detail.Describe( "DataWindow.Data" ))

If Not IsValid(iu_orp002) Then 
	iu_orp002 = Create u_orp002
End if

If Not IsValid(iu_ws_orp3) Then 
	iu_ws_orp3 = Create u_ws_orp3
End if

ls_products = ''

ll_RowCount = ldwc_print_detail.RowCount()
ldwc_print_detail.DeleteRow( ll_RowCount )
ll_RowCount --
//ldwc_print_detail.Sort()

For li_Counter = 1 to ll_RowCount
	If ldwc_print_detail.GetItemString( li_Counter, 'line_status') = 'C' Or &
		ldwc_print_detail.GetItemNumber( li_Counter, 'ordered_units') < 1 Then
		ldwc_print_detail.RowsDiscard(li_Counter, li_Counter, Primary!)
		li_Counter --
		ll_RowCount --
	End if
Next

For li_Counter = 1 to ll_RowCount
	ls_product_code = ldwc_print_detail.GetItemString(li_Counter, 'product_code')
	If Not lu_string_functions.nf_IsEmpty(ls_product_code) Then
		ls_Products += ls_product_code + '~t~r~n'
	End if
Next

//If Not iu_orp002.nf_orpo70ar_inq_sku_prod(istr_s_error, &
//														ls_products, ls_products) Then return

If Not iu_ws_orp3.uf_orpo70fr_inq_sku_prod(istr_s_error, &
														ls_products, ls_products) Then return

dw_print.GetChild('dw_header', ldwc_print_header)

ldwc_print_header.ImportString( dw_header.Describe( "DataWindow.Data" ))

ls_customer = dw_header.GetItemString(1, 'customer_id')

  SELECT customers.city, 
  			customers.short_name
    INTO :ls_customer_city, 
	 		:ls_customer_name
    FROM customers  
   WHERE customers.customer_id = :ls_customer;

ldwc_print_header.SetItem(1, 'customer_name', ls_customer_name)
ldwc_print_header.SetItem(1, 'customer_city', ls_Customer_city)

ls_temp = dw_header.GetItemString(1, 'trans_mode')
  
  SELECT tutltypes.type_short_desc  
    INTO :ls_transmode  
    FROM tutltypes  
   WHERE ( tutltypes.type_code = :ls_temp ) AND  
         ( tutltypes.record_type = 'TRANMODE' );

ldwc_print_header.SetItem(1, 'trans_mode_text', ls_transmode)

ls_temp = ldwc_print_header.GetItemString(1, 'sales_person_code')

  SELECT salesman.smanname,   
         salesman.smanloc  
    INTO :ls_TSR_Name,   
         :ls_TSR_Location  
    FROM salesman  
   WHERE salesman.smancode = :ls_temp AND salesman.smantype = 's';

ldwc_print_header.SetItem(1, 'tsr_name', ls_TSR_Name)
ldwc_print_header.SetItem(1, 'tsr_location', ls_TSR_Location)

For li_Counter = 1 to ll_RowCount
	lu_string_functions.nf_ParseLeftRight( ls_products, '~t', ls_Product_code, ls_Products)
	lu_string_functions.nf_ParseLeftRight( ls_products, '~r~n', ls_product_description, ls_products)

	ldwc_print_detail.SetItem(li_Counter, 'short_sku_descr', ls_product_description)

	ls_Plant = ldwc_print_detail.GetItemString(li_Counter, 'plant')
	If ls_plant <> ls_LastPlant Then
		SELECT locations.location_name  
	    INTO :ls_PlantDescr  
	    FROM locations  
	   WHERE locations.location_code = :ls_plant   ;
	End if
	ldwc_print_detail.SetItem(li_Counter, 'plant_description', ls_PlantDescr)
	ls_LastPlant = ls_Plant

Next
SetMicroHelp("Printing Reservation ...")

dw_print.Print()
ldwc_print_header.Reset()
ldwc_print_detail.Reset()
SetMicroHelp(iw_Frame.ia_application.MicroHelpDefault)
return
end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 50, this.height - dw_detail.y - 105)

long       ll_x = 50,  ll_y = 105

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)



end event

event ue_set_data;choose case as_data_item
	case "load_instruction"
		is_load_instruction = as_value
	case "dept_code"
		is_dept_code = as_value	
	case "additonal_data_input"
		is_additional_data_input = as_value
	case "export_country"
		is_dest_country = as_value
	case "store_door"
  		is_store_door = as_value
	case "deck_code"
  		is_deck_code = as_value		
end choose
end event

event ue_get_data;String  ls_product_code, ls_micro_test_ind, ls_micro_test_product_found
Long	ll_RowCount, ll_Row

choose case as_value
	case "load_instruction"
		Message.StringParm = is_load_instruction
	case "additonal_data_input"
		Message.StringParm = is_additional_data_input
	case "export_country"
		Message.StringParm = is_dest_country
	Case "micro_test_product"  //slh SR25459
		ls_micro_test_product_found = "N"
	     ll_RowCount = dw_detail.RowCount()
		For ll_Row = 1 to ll_RowCount
				If dw_detail.IsSelected(ll_Row) Then
					ls_product_code = dw_detail.object.product_code[ll_Row]
						
						SELECT micro_test_ind
						INTO :ls_micro_test_ind
						FROM sku_products 
                           WHERE sku_product_code = :ls_product_code
						USING SQLCA;
					
						If ls_micro_test_ind = "Y" Then
							ls_micro_test_product_found = "Y"
							EXIT
						End If
				End If
		Next
		Message.StringParm = ls_micro_test_product_found				
end choose
end event

type dw_detail from u_base_dw_ext within w_reservations
integer y = 384
integer width = 2971
integer height = 904
integer taborder = 20
string dataobject = "d_reservation_long"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;DataWindowCHild	ldwc_Temp

Long				ll_LoopCount,&
					ll_Sched_Units,&
					ll_Ordered_Units
	
String			ls_Status, &
					line_status,&
					ls_Line_Number




il_ClickedRow = row
IF il_clickedrow < 1 THEN RETURN

is_ClickedColumnName = dwo.name
IF is_ClickedColumnName <> 'line_number' THEN 
	This.SelectRow( row, False)
	If dwo.name = 'sales_order_id' Then
		this.getchild('sales_order_id',ldwc_Temp)
		ls_line_number = String(This.GetItemNumber( row,"line_number"))
		if IsNull(ls_Line_Number) Then Return
		ldwc_Temp.setfilter("line_number ='"+ls_Line_number+"'")
		Ldwc_Temp.Filter()
	END IF
	RETURN
Else
	This.AcceptText()
END IF

ls_Status = dw_detail.GetItemString( row, "queue_status")
IF ls_Status <> 'A' THEN RETURN

line_status	=	This.GetItemString( row,"line_status")
ll_Sched_Units  = This.GetItemNumber( row, "short_units")
ll_Ordered_Units = This.GetItemNumber( row, "ordered_units")

IF (line_status = 'C' or line_status = 'M') OR ll_Sched_Units < 1 OR ll_Ordered_Units < 1  THEN
	IF IsSelected(il_clickedrow) THEN This.SelectRow(il_clickedrow, False)
	RETURN
END IF

IF KeyDown(KeyShift!) THEN
	SetRedraw(FALSE)
	SelectRow(0, FALSE)
	IF il_last_clicked_row > il_clickedrow THEN
		FOR ll_loopcount = il_last_clicked_row To il_clickedrow STEP -1
			line_status	=	This.GetItemString( ll_loopcount,"line_status")
			ll_Sched_Units  = This.GetItemNumber( ll_LoopCount, "short_units")
			ll_Ordered_Units = This.GetItemNumber( ll_LoopCount, "ordered_units")
			IF (line_status <> 'C' or line_status <> 'M') AND ll_Sched_Units > 0 AND ll_Ordered_Units > 0 THEN
				SelectRow(ll_loopcount, TRUE)	
			END IF
		END FOR
	ELSEIF il_last_clicked_row = 0 THEN
		line_status	=	This.GetItemString( row,"line_status")
		ll_Sched_Units  = This.GetItemNumber( row, "short_units")
		ll_Ordered_Units = This.GetItemNumber( row, "ordered_units")
		IF (line_status <> 'C' or line_status <> 'M') AND ll_Sched_Units > 0  AND ll_Ordered_Units > 0 THEN
			SelectRow(il_clickedrow, TRUE)	
		END IF
	ELSE
		FOR ll_loopcount = il_last_clicked_row To il_clickedrow
			line_status	=	This.GetItemString( ll_loopcount,"line_status")
			ll_Sched_Units  = This.GetItemNumber( ll_LoopCount, "short_units")
			ll_Ordered_Units = This.GetItemNumber( ll_LoopCount, "ordered_units")
			IF (line_status <> 'C' or line_status <> 'M') AND ll_Sched_Units > 0  AND ll_Ordered_Units > 0 THEN
				SelectRow(ll_loopcount, TRUE)	
			END IF
		NEXT	
	END IF
	SetRedraw(TRUE)
	il_last_clicked_row = il_clickedrow
	RETURN
END IF

il_last_clicked_row = il_clickedrow
SelectRow(il_clickedrow, Not IsSelected(il_clickedrow))

end event

event itemchanged;call super::itemchanged;Int		li_column

Long		ll_RowCount,&
			ll_Row			

String	ls_fields,&
			ls_ColumnName,&
			ls_DataValue

string 	ls_ship_customer, ls_plant


u_project_functions lu_project_functions

ll_row = row
ll_RowCount = This.RowCount() - 1
ls_ColumnName = dwo.Name
ls_fields = This.GetItemString( ll_row, "detail_errors")
li_column = This.getcolumn()
If li_column > 10 Then
	li_column --
End If
ls_fields = Left( ls_fields, li_column - 1) + "M" + &
					Mid(ls_fields, li_column + 1)

This.SetItem( ll_row, "detail_errors", ls_fields)

IF (ls_ColumnName = "ordered_units" OR  &
	ls_ColumnName = "delivery_day") AND &
	ll_row = ll_RowCount Then 
	wf_defaultrow( ll_RowCount)
	IF ls_columnName = "ordered_units"  Then
 		This.SetItem( ll_RowCount + 1, "delivery_day",&
		This.GetItemString( ll_RowCount , &
		"delivery_day"))
	End IF						
End If	

IF ls_ColumnName = 'product_code' Then
	ls_ship_customer	=	dw_header.GetItemString(1, "customer_id")
	ls_plant				= 	this.GetItemString(ll_row, "plant")
	IF iw_frame.iu_string.nf_IsEmpty(ls_plant) THEN
		This.SetItem( ll_row, 'age', lu_project_functions.nf_get_age_code(Data, ls_ship_customer, TRUE))	
	ELSE
		This.SetItem( ll_row, 'age', lu_project_functions.nf_get_age_code(Data, ls_ship_customer, ls_plant))	
	END IF
END IF
end event

event constructor;call super::constructor;DataWindowChild	ldwc_Temp
This.InsertRow(0)
This.ib_NewRowOnChange = TRUE
This.ib_firstcolumnonnextrow = TRUE
is_selection = '4'
ib_updateable = TRUE

// I am doing this here because it is the only column not showing the code
dw_detail.GetChild('ordered_units_uom', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve('OUOM')

// This removes the single row that is retained on save
dw_detail.GetChild('customer_id',ldwc_temp)
ldwc_temp.Reset()



end event

event doubleclicked;call super::doubleclicked;String	ls_so_id

Window	lw_detail

u_string_functions	lu_string_functions

If dwo.name = 'sales_order_id' Then
	SetPointer(HourGlass!)
	ls_so_id = This.GetItemString( Row, 'sales_order_id')
	If lu_string_functions.nf_IsEmpty(ls_so_id) Then return
	OpenSheetWithParm(lw_detail, ls_so_id, 'w_sales_order_detail', &
							iw_frame, 0, iw_Frame.im_menu.iao_ArrangeOpen)
End if
end event

event ue_dwndropdown;DataWindowChild	ldwc_temp

String		ls_column, &
				ls_temp, &
				ls_header_customer, &
				ls_parent_id

Long			ll_row

u_project_functions	lu_project_functions

ls_column = This.GetColumnName()
ll_row = This.GetRow()

Choose Case  ls_column
	Case 'sales_order_id'
		This.GetChild('sales_order_id', ldwc_temp)
			
		ldwc_temp.SetFilter("")
		ldwc_Temp.Filter()
		ls_Temp = String(This.GetItemNumber(This.GetRow(), 'line_number'))
		If IsNull(ls_Temp) Then ls_temp = '0'
		ldwc_Temp.SetFilter("line_number = " + ls_Temp)
		ldwc_temp.Filter()

	Case 'customer_id'
		Choose Case dw_Header.GetItemString( 1, "customer_type")
			Case 'S'
				ls_Header_Customer = Trim(dw_header.GetItemString( 1, "customer_id"))
			Case 'B'
				ls_Header_Customer = Trim(dw_header.GetItemString( 1, "billto_customer_id"))
			Case 'C'
				ls_Header_Customer = Trim(dw_header.GetItemString( 1, "corporate_customer_id"))
		End Choose
		This.GetChild('customer_id', ldwc_temp)
		IF ISNUll(ls_Header_Customer) Then 
			ldwc_Temp.Reset()
			Return
		END IF
		IF dw_header.GetItemString(1,"customer_type") = 'B' Then
			IF ldwc_Temp.RowCount() > 0 Then
				ls_Parent_ID = ldwc_Temp.GetItemString( 1, "bill_to_custid")
				if IsNull( ls_Parent_id) then ls_Parent_id = ''
			ELSE
				ls_Parent_id = ''
			END IF	
			IF ls_Header_Customer <> ls_Parent_ID Then
				ldwc_temp.SetTransObject( SQLCA)
				ldwc_Temp.Retrieve( dw_header.GetItemString(1, 'customer_id'),Message.nf_GetLocation())
			END IF
		ELSE
			IF dw_header.GetItemString(1,"customer_type") = 'C' Then
				IF ldwc_Temp.RowCount() > 0 Then
					ls_Parent_ID = ldwc_Temp.GetItemString( 1, "corp_custid")
				   if IsNull( ls_Parent_id) then ls_Parent_id = ''
				ELSE
					ls_Parent_id = ''
				END IF
				IF ls_Header_Customer <> ls_Parent_id Then
					ldwc_temp.SetTransObject( SQLCA)
					ldwc_Temp.Retrieve( dw_header.GetItemString(1, 'customer_id'),Message.nf_GetLocation())
				END IF
			ELSE
				ldwc_Temp.Reset()
				ldwc_Temp.InsertRow(0)
				ldwc_Temp.SetItem(1,"customer_id", ls_Header_Customer)
			END IF
		END IF
Case 'plant'
		This.GetChild('plant', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getlocations(ldwc_temp, '')
		End If
End Choose
end event

event rbuttondown;call super::rbuttondown;m_orp_pext_popup NewMenu

NewMenu = CREATE m_orp_pext_popup
NewMenu.m_file.m_generate.Enabled=True
NewMenu.m_file.m_completeorder.Enabled=True

NewMenu.M_file.m_cascadeplants.enabled=False
NewMenu.M_file.m_cascadeplants.visible=False
NewMenu.M_file.m_pasummary.enabled=False
NewMenu.M_file.m_pasummary.visible=False
NewMenu.M_file.m_padisplay.enabled=False
NewMenu.M_file.m_padisplay.visible=False
NewMenu.M_file.m_reinquire.enabled=False
NewMenu.M_file.m_reinquire.visible=False

NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())

end event

event resize;//if this.width > 3296 then this.width = 3296
end event

type dw_print from u_base_dw_ext within w_reservations
boolean visible = false
integer x = 370
integer y = 192
integer width = 2450
integer height = 1000
string dataobject = "d_reservation_print"
boolean hscrollbar = true
boolean vscrollbar = true
boolean resizable = true
end type

on clicked;call u_base_dw_ext::clicked;This.BringToTop = True
end on

type dw_header from u_base_dw_ext within w_reservations
event ue_post_itemchanged pbm_custom46
integer width = 2706
integer height = 364
string dataobject = "d_reservation_header"
boolean border = false
end type

event constructor;call super::constructor;This.is_Selection = '0'
This.Object.sales_location_name.Visible = True
This.InsertRow(0)


end event

event itemchanged;call super::itemchanged;Int		li_ref_Point, li_Row_Count, li_Loop_Count

String	ls_fields,&
			ls_ColumnName, &
			ls_text, &
			ls_salesman, &
			ls_salesloc,&
			ls_location_name, &
			ls_service_center, &
			ls_prod_purch_stat, &
			ls_customer_id, &
			ls_CustomerName,ls_CustomerCity, ls_customer_state
			
u_project_functions	lu_project_functions			

ls_text = data
ls_ColumnName =  dwo.name
Choose Case ls_ColumnName
CASE 'sales_person_code'

	SELECT salesman.smanname, 
			salesman.smanloc
		INTO :ls_salesman, 
				:ls_salesloc
		FROM salesman  
	WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';

	if isnull(ls_salesman) then
			This.object.sales_person_name.Text = ''
		else
			This.object.sales_person_name.Text = ls_salesman
		end if
		if isnull(ls_location_name) then
			This.object.sales_location_name.Text = '' 
		else
			This.object.sales_location_name.Text = ls_location_name 
		end if 
	
	SELECT locations.location_name
	  INTO :ls_service_center
	  FROM locations
	WHERE locations.location_code = :ls_salesloc;
	
	if isnull(ls_service_center) then
		This.Object.sales_location_name.Text = ''
	else
		This.Object.sales_location_name.Text = ls_service_center
	end if
Case 'billto_customer_id', 'corporate_customer_id'
	IF lu_project_functions.nf_validatecustomerstate( Data, This.GetItemString(1,"customer_type"),&
																			ls_CustomerName,ls_CustomerCity, ls_customer_state) < 1 Then
		SetMicroHelp("Invalid/Inactive Customer")	
		Return 1
	ELSE
		Parent.wf_setcustomer_name_city (  data )
	end if
Case 'customer_id'
//	li_Row_Count = dw_detail.RowCOunt()
//	For li_Loop_Count = 1 to li_Row_Count
//		dw_Detail.SetItem( li_Loop_Count, "customer_id", data )
//	NEXT
	IF lu_project_functions.nf_validatecustomerstate( Data, This.GetItemString(1,"customer_type"),&
																			ls_CustomerName,ls_CustomerCity, ls_customer_state) < 1 Then
		SetMicroHelp("Invalid/Inactive Customer")	
		Return 1
	ELSE
//12-12 jac
	   ls_customer_id = ls_text
      SELECT customers.purch_prod_status 
		INTO 	:ls_prod_purch_stat
      FROM customers		
      WHERE customers.customer_id = :ls_customer_id;
		if ls_prod_purch_stat = 'M'  Then
		   SetMicroHelp("Customers flagged for managed distribution product cannot have reservations")	
    	   Return 1
	   end if
//			
		IF Parent.wf_setcustomer_name_city ( data ) Then
			ls_Text = lu_project_functions.nf_getdefault_tsr_for_customer ( Data )
			this.setitem( 1, "sales_person_code", ls_Text)
			
			SELECT salesman.smanname, 
					 salesman.smanloc
				INTO :ls_salesman, 
					  :ls_salesloc
				FROM 	salesman  
				WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';
	
		  SELECT locations.location_name  
			INTO :ls_location_name  
			FROM locations  
			WHERE locations.location_code = :ls_salesloc;
			
			if isnull(ls_salesman) then
				This.object.sales_person_name.Text = ''
			else
				This.object.sales_person_name.Text = ls_salesman
			end if
			if isnull(ls_location_name) then
				This.object.sales_location_name.Text = '' 
			else
				This.object.sales_location_name.Text = ls_location_name 
			end if 
		END IF
	end if
CASE ELSE
End Choose

ls_fields = This.GetItemString( row, "fields_in_error")
li_ref_Point = This.getcolumn()
ls_fields = Left(ls_fields, li_ref_Point - 1) + "M" + &
					Mid(ls_fields, li_ref_Point + 1)

This.SetItem(row, "fields_in_error", ls_fields)
end event

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;IF This.GetColumnName() = 'month' Then
	This.SelectText(1,2)
END IF


end on

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_customer, &
						ldwc_customer_name, &
						ldwc_customer_city, &
						ldwc_corporate_customer, &
						ldwc_corporate_customer_name, &
						ldwc_corporate_customer_city, &
						ldwc_billto_customer, &
						ldwc_billto_customer_name, &
						ldwc_billto_customer_city, &
						ldwc_salespeople, &
						ldwc_trans_mode, &
						ldwc_customer_state, &
						ldwc_corporate_customer_state, &
						ldwc_billto_customer_state

String				ls_salesman, &
						ls_service_center
						
u_project_functions	lu_project_functions						

This.GetChild('sales_person_code', ldwc_salespeople)
ldwc_salespeople.Reset()  // This to remove the one line retained on save in the DW

This.GetChild('customer_id', ldwc_customer)
ldwc_customer.Reset()		// This to remove the one line retained on save in the DW
This.GetChild('customer_id', ldwc_customer)

This.GetChild('customer_name', ldwc_customer_name)
This.GetChild('customer_city', ldwc_customer_city)
This.GetChild('customer_state', ldwc_customer_state)

iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer_name)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer_city)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer_state)

//lu_project_functions.nf_getcustomers(ldwc_customer, &
//		message.is_smanlocation)

This.GetChild('corporate_customer_id', ldwc_corporate_customer)
This.GetChild('corporate_customer_name', ldwc_corporate_customer_name)
This.GetChild('corporate_customer_city', ldwc_corporate_customer_city)
This.GetChild('corporate_customer_state', ldwc_corporate_customer_state)

ldwc_corporate_customer.ShareData(ldwc_corporate_customer_name)
ldwc_corporate_customer.ShareData(ldwc_corporate_customer_city)
ldwc_corporate_customer.ShareData(ldwc_corporate_customer_state)

This.GetChild('billto_customer_id', ldwc_billto_customer)
This.GetChild('billto_customer_name', ldwc_billto_customer_name)
This.GetChild('billto_customer_city', ldwc_billto_customer_city)
This.GetChild('billto_customer_state', ldwc_billto_customer_state)

ldwc_billto_customer.ShareData(ldwc_billto_customer_name)
ldwc_billto_customer.ShareData(ldwc_billto_customer_city)
ldwc_billto_customer.ShareData(ldwc_billto_customer_state)

  SELECT salesman.smanname
    INTO :ls_salesman  
    FROM salesman  
  WHERE salesman.smancode = :Message.is_salesperson_code AND salesman.smantype = 's';

This.SetItem(1, 'sales_person_code', Message.is_salesperson_code)
This.object.sales_person_name.Text = ls_salesman
This.object.sales_location_code.Text = Message.is_smanlocation

	SELECT locations.location_name
	  INTO :ls_service_center
	  FROM locations
	WHERE locations.location_code = :Message.is_smanlocation;
This.Object.sales_location_name.Text = ls_service_center

This.GetChild('trans_mode', ldwc_trans_mode)
lu_project_functions.nf_gettutltype(ldwc_trans_mode, 'TRANMODE')
This.SetItem(1, 'trans_mode', 'T')

This.GetChild('auto_rollover_ind', ldwc_trans_mode)
lu_project_functions.nf_gettutltype(ldwc_trans_mode, 'RESROLL')
This.SetItem(1, 'auto_rollover_ind', 'Y')
end event

event ue_dwndropdown;DataWindowChild	ldwc_temp

u_project_functions	lu_project_functions

Choose Case This.GetColumnName()
	Case 'customer_id'
		This.GetChild('customer_id', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getcustomers(ldwc_temp, &
					message.is_smanlocation)
		End If
	Case 'billto_customer_id'
		This.GetChild('billto_customer_id', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getbillto(ldwc_temp)
		End If
	Case 'corporate_customer_id'
		This.GetChild('corporate_customer_id', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getcorps(ldwc_temp)
		End If
	Case 'customer_name'
		This.GetChild('customer_id', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getcustomers(ldwc_temp, &
					message.is_smanlocation)
		End If
	Case 'billto_customer_name'
		This.GetChild('billto_customer_id', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getbillto(ldwc_temp)
		End If
	Case 'corporate_customer_name'
		This.GetChild('corporate_customer_id', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_getcorps(ldwc_temp)
		End If
	Case 'sales_person_code'
		This.GetChild('sales_person_code', ldwc_Temp)
		If ldwc_temp.RowCount() < 1 Then
			lu_project_functions.nf_gettsrs(ldwc_temp, &
					message.is_smanlocation)
		End If
End Choose
end event

event itemerror;call super::itemerror;Return 1
end event

event clicked;call super::clicked;String ls_protected
Long ll_count
IF Row > 0 Then
	Choose Case dwo.name
		Case 'price_override'
			dw_header.AcceptText()
			If dw_header.GetItemString(1, 'price_override') = 'N' then
				For ll_count = 1 to dw_detail.RowCount()
					ls_protected = dw_detail.GetItemString(ll_count, 'detail_errors')
					If (Mid(ls_protected, 18, 1) = 'E' ) then
						dw_detail.SetItem(ll_count, 'price_override', 'Y')
					end if
					If (Mid(ls_protected, 18, 1) = 'M' ) then
						dw_detail.SetItem(ll_count, 'price_override', 'Y')
					end if
				Next 
			Else
				For ll_count = 1 to dw_detail.RowCount()
					ls_protected = dw_detail.GetItemString(ll_count, 'detail_errors')
					If (Mid(ls_protected, 18, 1) = 'E' ) then
						dw_detail.SetItem(ll_count, 'price_override', 'N')
					end if
					If (Mid(ls_protected, 18, 1) = 'M' ) then
						dw_detail.SetItem(ll_count, 'price_override', 'N')
					end if
				Next 
			End if
	End Choose
End if
			
end event

