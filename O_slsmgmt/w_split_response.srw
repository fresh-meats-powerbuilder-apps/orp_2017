HA$PBExportHeader$w_split_response.srw
$PBExportComments$Split Window - used by Res Review
forward
global type w_split_response from w_netwise_response
end type
type em_number_of_rows from editmask within w_split_response
end type
type st_1 from statictext within w_split_response
end type
type st_2 from statictext within w_split_response
end type
type dw_detail from u_netwise_dw within w_split_response
end type
type cbx_split_evenly from checkbox within w_split_response
end type
type cb_split from commandbutton within w_split_response
end type
end forward

global type w_split_response from w_netwise_response
int X=174
int Y=217
int Width=2716
boolean TitleBar=true
string Title="Split Line"
long BackColor=12632256
em_number_of_rows em_number_of_rows
st_1 st_1
st_2 st_2
dw_detail dw_detail
cbx_split_evenly cbx_split_evenly
cb_split cb_split
end type
global w_split_response w_split_response

type variables
Integer	ii_Original_Count

String	is_ImportString, &
	is_OriginalDetailErrors
end variables

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_TempStorage

Int	li_Counter

Long	ll_NumRows

String	ls_Errors


ll_NumRows = dw_detail.ImportString( is_importstring)

dw_detail.GetChild("ordered_units_uom", ldwc_TempStorage)
//message.iw_UtlData.wf_gettutltype( "OUOM", ldwc_TempStorage)
ldwc_TempStorage.SetTransObject( SQlCA)
ldwc_TempStorage.Retrieve( "OUOM")

If ll_NumRows > 0 Then
	ii_original_count = dw_detail.GetItemNumber( 1, "ordered_units")
	em_number_of_rows.Text = "2"
	is_OriginalDetailErrors = dw_detail.GetItemString(1, 'detail_errors')
ELSE
	MessageBox("No Rows to Split", "Contact App devlopment")
	Close(This)
END IF

ls_Errors = ' VVVVV ' + Fill('V', 16)
For li_Counter = 1 to ll_NumRows
	dw_detail.SetItem(li_Counter, 'detail_errors', ls_Errors)
Next

end event

event open;call super::open;is_importstring = Message.StringParm

end event

on w_split_response.create
int iCurrent
call w_netwise_response::create
this.em_number_of_rows=create em_number_of_rows
this.st_1=create st_1
this.st_2=create st_2
this.dw_detail=create dw_detail
this.cbx_split_evenly=create cbx_split_evenly
this.cb_split=create cb_split
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=em_number_of_rows
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=dw_detail
this.Control[iCurrent+5]=cbx_split_evenly
this.Control[iCurrent+6]=cb_split
end on

on w_split_response.destroy
call w_netwise_response::destroy
destroy(this.em_number_of_rows)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.dw_detail)
destroy(this.cbx_split_evenly)
destroy(this.cb_split)
end on

event ue_base_ok;call super::ue_base_ok;Int	li_Counter

Long	ll_LoopCount,&
		ll_RowCOunt,&
		ll_TotalOrdered


dw_Detail.AcceptText()
ll_RowCount = dw_Detail.RowCount()

For ll_LoopCount = 1 to ll_RowCount
	ll_TotalOrdered += dw_detail.GetItemNumber( ll_LoopCount, "ordered_units")
Next

IF ll_TotalOrdered <> ii_original_count Then
	MessageBox( "Invalid split ordered" ,"The Total Split must equal the original ordered.") 
	Return
ELSE
	// Quantity is the first column
	is_OriginalDetailErrors = Replace(is_OriginalDetailErrors, 1, 1, 'W')
	For li_Counter = 1 to ll_RowCount
		dw_detail.SetItem(li_Counter, 'detail_errors', is_OriginalDetailErrors)
	Next
	CloseWithReturn( This, dw_Detail.Describe("DataWindow.Data"))
END IF
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, "Cancel")
end event

type cb_base_help from w_netwise_response`cb_base_help within w_split_response
int X=2364
int Y=541
int TabOrder=10
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_split_response
int X=2359
int Y=285
int TabOrder=40
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_split_response
int X=2359
int Y=165
int TabOrder=20
end type

type em_number_of_rows from editmask within w_split_response
int X=293
int Y=41
int Width=311
int Height=97
int TabOrder=30
string Mask="###"
boolean Spin=true
double Increment=1
string MinMax="2~~999"
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_split_response
int X=55
int Y=53
int Width=238
int Height=73
boolean Enabled=false
string Text="Split Into "
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_split_response
int X=618
int Y=45
int Width=247
int Height=73
boolean Enabled=false
string Text="Rows"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_detail from u_netwise_dw within w_split_response
int X=14
int Y=161
int Width=2327
int Height=1197
int TabOrder=50
string DataObject="d_reservation_short_op"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

on itemchanged;call u_netwise_dw::itemchanged;Long		ll_row

String	ls_errors, &
			ls_UpdateFlag


ll_row = This.GetRow()

IF Long(This.Describe("update_flag.id")) > 0 THEN
	ls_UpdateFlag = Trim(This.GetItemString(ll_Row, "update_flag"))
	If ISNULL(ls_UpdateFlag) or TRIM(ls_UpdateFlag) = "" Then
		CHOOSE CASE This.GetItemStatus(ll_Row, 0, Primary!)
	 	CASE NewModified!, New!
			This.SetItem(ll_Row, "update_flag", "A")
		CASE DataModified!, NotModified!
			This.SetItem	(ll_Row, "update_flag", "U")
		END CHOOSE		
	End if
END IF

ls_errors = Replace(This.GetItemString(ll_row, 'detail_errors'), &
				This.GetColumn(), 1, 'M')

This.SetItem(ll_row, 'detail_errors', ls_errors)
end on

type cbx_split_evenly from checkbox within w_split_response
int X=869
int Y=49
int Width=577
int Height=73
string Text="Split Rows Evenly "
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;IF This.Checked Then
	dw_detail.Modify("ordered_units.protect = '1'")
ELSE
	dw_detail.Modify("ordered_units.protect = '0'")
END IF
end on

type cb_split from commandbutton within w_split_response
int X=2364
int Y=413
int Width=279
int Height=109
int TabOrder=60
string Text="&Split"
int TextSize=-10
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked; String ls_Value
Long	ll_LoopCount,&
		ll_Number_Of_Rows,&
		ll_Number_To_Insert,&
		ll_RowInserted,&
		ll_EvenSplit,&
		ll_Value
		
Decimal ld_Largest
		
ls_Value =  em_number_of_rows.Text
ll_value = Integer(ls_Value)
IF ll_Value < 2 Then
	SetMicroHelp( "Value must be greater than 1")
	ls_Value = ''
	Return
END IF

dw_detail.SetRedraw(False)
SetPointer( HourGlass!)

ll_Number_Of_Rows = dw_detail.RowCount()
ld_Largest = ii_original_count/ll_Value
ll_EvenSplit = Truncate(ii_original_count/ll_Value, 0 )
IF ld_Largest > ll_EvenSplit Then &
				ld_Largest = Truncate(ii_original_count/ll_Value, 0 ) +1


IF ll_Number_Of_Rows <  Integer( ls_Value) Then
	ll_Number_To_Insert = ll_value - ll_Number_Of_rows 
	For ll_LoopCount = 1 to ll_Number_To_Insert
		dw_detail.RowsCopy( dw_detail.RowCount(), dw_detail.RowCount(),&
								  Primary!, dw_detail, 1000000, Primary!)
	Next
ELSE	
	DO WHILE dw_detail.RowCount() > ll_Value
		dw_detail.DeleteRow( dw_Detail.RowCount())	
	Loop
END IF


If cbx_split_evenly.Checked Then 
	ll_Number_To_Insert = dw_detail.RowCount()
	dw_detail.SetItem( 1, "ordered_units", ll_EvenSplit)
	FOR ll_LoopCount = 2 TO ll_Number_To_Insert
			dw_detail.SetItem( LL_LoopCount, "ordered_units", &
																ll_EvenSplit)
			dw_Detail.SetItem( ll_LoopCount, "update_Flag", "A")
	Next
	ll_Number_to_Insert =ii_original_count - (ll_Number_to_Insert*ll_EvenSplit)
	For ll_LoopCount = 1 to ll_Number_To_Insert
		dw_detail.SetItem( LL_LoopCount, "ordered_units", &
																ld_Largest)

	Next
ELSE
	ll_Number_To_Insert = dw_detail.RowCount()
	dw_detail.SetItem( 1, "ordered_units", ii_original_count)
	For ll_LoopCount = 2 TO ll_Number_To_Insert
			dw_detail.SetItem( LL_LoopCount, "ordered_units", 0)
			dw_Detail.SetItem( ll_LoopCount, "update_flag", "A")
	Next
END IF

dw_detail.SetRedraw( True)
end on

