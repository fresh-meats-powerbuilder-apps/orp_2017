HA$PBExportHeader$w_product_cancellation_report.srw
forward
global type w_product_cancellation_report from w_base_sheet_ext
end type
type dw_division_list from u_base_dw_ext within w_product_cancellation_report
end type
type cb_1 from commandbutton within w_product_cancellation_report
end type
type dw_grp_description_list from u_base_dw_ext within w_product_cancellation_report
end type
type dw_prod_cancel_report_qualifers from u_base_dw_ext within w_product_cancellation_report
end type
end forward

global type w_product_cancellation_report from w_base_sheet_ext
integer x = 14
integer y = 64
integer width = 2894
integer height = 1092
string title = "Product Cancellation Report"
event ue_ok ( )
dw_division_list dw_division_list
cb_1 cb_1
dw_grp_description_list dw_grp_description_list
dw_prod_cancel_report_qualifers dw_prod_cancel_report_qualifers
end type
global w_product_cancellation_report w_product_cancellation_report

type variables
u_dwselect	iu_dwselect

s_error		istr_error_info

u_orp204	  	iu_orp204
u_ws_orp4 		iu_ws_orp4
end variables

event ue_ok();long		ll_row, ll_rowfound

string	ls_inquire_output, &
			ls_group, &
			ls_tsr, &
			ls_from_date, &
			ls_to_date, &
			ls_FindExpression

Integer	li_rtn

u_string_functions	lu_string_functions
datawindowchild		ldw_ChildDW

ll_row = dw_division_list.getselectedrow(0)
if ll_row = 0 then
//	messagebox("Required","You must have at least one divison selected(Max 4) to continue.")
	messagebox("Required","You must have at least one divison selected to continue.")
	return
end if

ls_inquire_output = SQLCA.userid + "~t" + Message.is_smanlocation 

do while ll_row >0
	ls_inquire_output += "~t" + dw_division_list.getitemstring(ll_row,"type_code") 
	ll_row = dw_division_list.getselectedrow(ll_row)
loop

ls_inquire_output += "~r~n"

ll_row = dw_grp_description_list.GetSelectedRow(0)

if ll_row > 0 then
	ls_group = dw_grp_description_list.Getitemstring(ll_row,"GroupId")
	ll_row = dw_grp_description_list.getselectedrow(ll_row)
	do while ll_row >0
		ls_group +=  "~t" + dw_grp_description_list.Getitemstring(ll_row,"GroupId")
		ll_row = dw_grp_description_list.getselectedrow(ll_row)
	loop
else
	ls_group = " "
end if 

ls_group += "~r~n"

dw_prod_cancel_report_qualifers.AcceptText()
If lu_string_functions.nf_IsEmpty(dw_prod_cancel_report_qualifers.GetItemString(1, 'tsr')) Then
	ls_tsr = " "
else
	dw_prod_cancel_report_qualifers.GetChild('tsr', ldw_ChildDW)
	ll_rowfound	=	ldw_ChildDW.RowCount()
	ls_FindExpression = "smancode = '" + dw_prod_cancel_report_qualifers.GetItemString(1, 'tsr') + "'"
	ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
	If  ll_RowFound <= 0 Then 
		iw_frame.SetMicroHelp(dw_prod_cancel_report_qualifers.GetItemString(1, 'tsr') + ' is an invalid TSR')
		dw_prod_cancel_report_qualifers.SetColumn('tsr')
		dw_prod_cancel_report_qualifers.SetFocus()
		dw_prod_cancel_report_qualifers.SelectText(1, Len(dw_prod_cancel_report_qualifers.GetItemString(1, 'tsr')))
		return
	END IF
	ls_tsr = dw_prod_cancel_report_qualifers.GetItemString(1, 'tsr')
end if

ls_tsr += "~r~n"

ls_from_date = string(dw_prod_cancel_report_qualifers.GetItemDate(1, 'fromdate'))
ls_to_date = string(dw_prod_cancel_report_qualifers.GetItemDate(1, 'todate'))
if isnull(ls_from_date) and isnull(ls_to_date) then
	ls_from_date = " "
	ls_to_date = " "
else
	if dw_prod_cancel_report_qualifers.GetItemDate(1, 'fromdate') >  &
		dw_prod_cancel_report_qualifers.GetItemDate(1, 'todate') or &
		isnull(dw_prod_cancel_report_qualifers.GetItemDate(1, 'todate')) or &
		isnull(dw_prod_cancel_report_qualifers.GetItemDate(1, 'fromdate')) or &
		date(ls_from_date) < relativedate(today(), -7) or &
		date(ls_to_date) > today() then
		if isnull(ls_from_date) then
			iw_frame.SetMicroHelp("A From Date must be entered.")
			dw_prod_cancel_report_qualifers.SetColumn('fromdate')
			dw_prod_cancel_report_qualifers.SetFocus()
			return
		end if
		if isnull(ls_to_date) then
			iw_frame.SetMicroHelp("A To Date must be entered.")
			dw_prod_cancel_report_qualifers.SetColumn('todate')
			dw_prod_cancel_report_qualifers.SetFocus()
			return
		end if
		if date(ls_from_date) < relativedate(today(), -7) then
			iw_frame.SetMicroHelp("The From Date must be equal to or greater than " + &
															string(relativedate(today(), -7),"m/d/yyyy"))
			dw_prod_cancel_report_qualifers.SetColumn('fromdate')
			dw_prod_cancel_report_qualifers.SetFocus()
			return
		end if
		if date(ls_to_date) > today() then
			iw_frame.SetMicroHelp("The To Date must be equal to or less than " + string(today(),"m/d/yyyy"))
			dw_prod_cancel_report_qualifers.SetColumn('todate')
			dw_prod_cancel_report_qualifers.SetFocus()
			return
		end if
		iw_frame.SetMicroHelp("The From Date can not be greater than the To Date")
		dw_prod_cancel_report_qualifers.SetColumn('fromdate')
		dw_prod_cancel_report_qualifers.SetFocus()
		return
	end if
end if

istr_error_info.se_event_name = "ue_base_ok"

//messagebox("Data",ls_inquire_output)

li_rtn = iu_ws_orp4.nf_orpo92fr(istr_error_info, ls_inquire_output, ls_group, ls_tsr, ls_from_date, ls_to_date)

end event

on w_product_cancellation_report.create
int iCurrent
call super::create
this.dw_division_list=create dw_division_list
this.cb_1=create cb_1
this.dw_grp_description_list=create dw_grp_description_list
this.dw_prod_cancel_report_qualifers=create dw_prod_cancel_report_qualifers
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division_list
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_grp_description_list
this.Control[iCurrent+4]=this.dw_prod_cancel_report_qualifers
end on

on w_product_cancellation_report.destroy
call super::destroy
destroy(this.dw_division_list)
destroy(this.cb_1)
destroy(this.dw_grp_description_list)
destroy(this.dw_prod_cancel_report_qualifers)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')

iw_frame.im_menu.mf_disable('m_print')
end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

iw_frame.im_menu.mf_disable('m_print')
end event

event close;call super::close;Destroy( iu_orp204)
Destroy (iu_ws_orp4)
end event

event open;call super::open;iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4
end event

event resize;call super::resize;dw_division_list.width = 1198
dw_division_list.height = 664

dw_grp_description_list.width = 1600
dw_grp_description_list.height = 664
end event

type dw_division_list from u_base_dw_ext within w_product_cancellation_report
integer x = 18
integer y = 16
integer width = 1198
integer height = 664
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_division_list"
boolean vscrollbar = true
end type

event clicked;call super::clicked;if row > 0 then
	iu_dwselect.uf_select(row,4)
end if
end event

event constructor;call super::constructor;datawindow		ldw_this

ldw_this = this

//<USAGE>	Call this function to Initialize the object
//			Valid selection types are:
//			0 - Do not select any rows
//			1 - Select only one row
//			2 - Select multiple rows, one at a time
//			3 - Select multiple rows with CTRL and SHIFT support for blocks
//			4 - Select multiple rows with SHIFT support for blocks
//</USAGE>

iu_dwselect.uf_initialize(ldw_this, 1)

THIS.SetTransObject(SQLCA)
THIS.Retrieve()
end event

type cb_1 from commandbutton within w_product_cancellation_report
integer x = 2551
integer y = 732
integer width = 247
integer height = 108
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&OK"
boolean default = true
end type

event clicked;Parent.TriggerEvent ("ue_ok")
end event

type dw_grp_description_list from u_base_dw_ext within w_product_cancellation_report
event ue_postopen ( )
integer x = 1225
integer y = 16
integer width = 1600
integer height = 664
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_grp_description_list"
boolean vscrollbar = true
end type

event ue_postopen();integer						li_rtn
string						ls_output
u_string_functions		lu_string_functions
datastore					lds_datastore


istr_error_info.se_event_name = "constructor"
				
//li_rtn = iu_orp204.nf_orpo97br_grp_maintenance_inq(istr_error_info, &
//												"P"," ",ls_output)			

li_rtn = iu_ws_orp4.nf_orpo97fr(istr_error_info, "P", ls_output)
				
If li_rtn = 0 Then
	If lu_string_functions.nf_isempty(ls_output) Then
		messagebox("Informational","No Product Groups available at this time.")
	else
		lds_datastore = create datastore
		lds_datastore.Dataobject = "d_grp_description_list"
		lds_datastore.ImportString(ls_output)
		lds_datastore.SetSort("groupowner A, groupdescription A, groupid A")
		lds_datastore.Sort()
		this.SetRedraw(False)
		this.ImportString(lds_datastore.Object.DataWindow.Data)
		this.SetRedraw(True)
	end if
end if


end event

event clicked;call super::clicked;//if row > 0 then
//	iu_dwselect.uf_select(row,4)
//end if
long  ll_rowselected


if row > 0 then
	ll_rowselected = this.GetSelectedRow(0)
	choose case ll_rowselected
		case 0 
			this.selectrow(row,true)
		case row
			this.selectrow(row,false)
		case else
			this.selectrow(0, false)
			this.selectrow(row,true)
	end choose
end if




end event

event constructor;call super::constructor;PostEvent("ue_postopen")
end event

type dw_prod_cancel_report_qualifers from u_base_dw_ext within w_product_cancellation_report
integer x = 18
integer y = 688
integer width = 1815
integer height = 248
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_prod_cancel_report_qualifers"
end type

event constructor;call super::constructor;u_project_functions		lu_project_functions
datawindowchild 			ldwc_Temp

this.insertrow(0)

dw_prod_cancel_report_qualifers.GetChild('tsr', ldwc_Temp)

If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_temp, &
			message.is_smanlocation)
End If

dw_prod_cancel_report_qualifers.GetChild('tsrname', ldwc_Temp)
If ldwc_temp.RowCount() <= 1 Then
	lu_project_functions.nf_gettsrs(ldwc_temp, &
			message.is_smanlocation)
End If

dw_prod_cancel_report_qualifers.setitem(1,"fromdate",relativedate(today(),-1))
dw_prod_cancel_report_qualifers.setitem(1,"todate",relativedate(today(),-1))

end event

event itemchanged;call super::itemchanged;choose case dwo.name
	case "tsr"
		this.setitem(1, "tsrname", data)
end choose
end event

