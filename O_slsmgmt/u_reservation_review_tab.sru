HA$PBExportHeader$u_reservation_review_tab.sru
forward
global type u_reservation_review_tab from userobject
end type
type st_1 from statictext within u_reservation_review_tab
end type
type cbx_showall from checkbox within u_reservation_review_tab
end type
type dw_detail from u_netwise_dw within u_reservation_review_tab
end type
type dw_header from u_netwise_dw within u_reservation_review_tab
end type
end forward

shared variables

end variables

global type u_reservation_review_tab from userobject
integer width = 2798
integer height = 1276
long backcolor = 12632256
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
event ue_cascadeplant ( )
st_1 st_1
cbx_showall cbx_showall
dw_detail dw_detail
dw_header dw_header
end type
global u_reservation_review_tab u_reservation_review_tab

type variables
Boolean	ib_retrieved = FALSE
Boolean	ib_modified = FALSE
String	is_order_status
Date	id_gpo_date
String	is_filtered = 'N'
end variables

forward prototypes
public subroutine uf_selected ()
public subroutine uf_filldropdowns ()
public function boolean uf_update ()
public subroutine uf_cascadeplants ()
public subroutine uf_completeorder ()
public function integer uf_replacerows (string as_datatoimport, ref long al_rowstoreplace[], ref datawindow adw_datawindowtarget)
public subroutine uf_showall (boolean ab_showall)
end prototypes

event ue_cascadeplant;This.uf_cascadeplants()
end event

public subroutine uf_selected ();Int		li_Page_number, &
			li_ret

Double	ld_task_number
			

String	ls_ResNum, &
			ls_header, &
			ls_detail, &
			ls_sales_info, &
			ls_roll_info
			
s_error lstr_error_info

u_string_functions		lu_string_functions

If ib_retrieved then return

ib_retrieved = TRUE

w_reservation_review_sales	lw_parent
lw_parent = Parent.GetParent()

Choose Case is_order_status
	Case 'N'
		dw_detail.DataObject = 'd_reservation_short_op'
		uf_showall(True)
	Case 'M'
		dw_detail.DataObject = 'd_reservation_change_op'
		uf_showall(False)
	Case Else
		dw_detail.DataObject = 'd_reservation_long_op'
		uf_showall(True)
End Choose
uf_FillDropDowns()

ls_resnum = This.Text

dw_detail.SetRedraw(False)

ls_header = ls_ResNum + '~t~t~t' + String(id_gpo_date,"mm/dd/yyyy") + '~t~t~tA~t~tI~t~t' + 'M' + '~t'

lstr_error_info.se_event_name = 'wf_retrieve_reservation'
lstr_error_info.se_User_Id = SQLCA.UserID
ld_task_number = 0
li_page_number = 0
dw_header.Reset()
// This insertrow is just for looks.  If the rpc takes awhile, this makes it look better
dw_header.InsertRow(0)
dw_header.SetRedraw(False)

//Do
//	li_ret = lw_parent.iu_orp001.nf_orpo57ar(lstr_error_info, &
//											ls_header, &
//											ls_detail, &
//											ls_sales_info, &
//											ld_task_number, &
//											li_page_number, &
//											ls_roll_info)
											
	li_ret = lw_parent.iu_ws_orp3.uf_orpo57fr(lstr_error_info, &
											ls_header, &
											ls_detail, &
											ls_sales_info, &
											ls_roll_info)


	If Not lu_string_functions.nf_IsEmpty(ls_header) Then
		dw_header.Reset()
		dw_header.ImportString(ls_header)
	End if
	dw_detail.ImportString(ls_detail)

//Loop While ld_Task_Number <> 0 And li_page_number <> 0

String ls_person
ls_person = dw_header.GetItemString(1,"sales_person_code")

String ls_salesman, ls_salesloc
	SELECT salesman.smanname, 
			salesman.smanloc
		INTO :ls_salesman, 
				:ls_salesloc
		FROM salesman  
	WHERE salesman.smancode = :ls_person AND salesman.smantype = 's';

dw_header.object.sales_person_name.Text = ls_salesman
dw_header.object.sales_location_code.Text = ls_salesloc
	
	String ls_service_center
	SELECT locations.location_code
	  INTO :ls_service_center
	  FROM locations
	WHERE locations.location_code = :ls_salesloc;
	
dw_header.Object.sales_location_name.Text = ls_service_center

dw_detail.Filter()
If dw_detail.RowCount() < 1 Then uf_showall(True)

dw_header.ResetUpdate()

dw_detail.ResetUpdate()

dw_Detail.SetColumn( "ordered_units")

dw_Detail.SetFocus()
dw_Detail.SetRow(1)
dw_header.SetRedraw(True)
dw_detail.SetRedraw(True)

end subroutine

public subroutine uf_filldropdowns ();//DataWindowChild	dwc_temp
//
//
//dw_detail.GetChild('plant', dwc_temp)
//If IsValid(dwc_temp) Then
//	iw_frame.iu_netwise_data.nf_GetLocations(dwc_temp)
//End if
//
//dw_detail.GetChild('change_plant_code', dwc_temp)
//If IsValid(dwc_temp) Then
//	iw_frame.iu_netwise_data.nf_GetLocations(dwc_temp)
//End if
//
//dw_detail.GetChild('ordered_units_uom', dwc_temp)
//iw_frame.iu_netwise_data.nf_gettutltype_dwc('OUOM', dwc_temp)
//
return
end subroutine

public function boolean uf_update ();Boolean	lb_ErrorOccured

Int		li_page_number, &
			li_ret, &
			li_ChangedCounter, &
			li_Counter, &
			li_temp

Double	ld_task_number
			
Long		ll_row, &
			ll_RowCount, &
			lla_ChangedRows[], &
			ll_end, &
			ll_find

String	ls_header, &
			ls_detail, &
			ls_RPC_Detail, &
			ls_Returned_Detail, &
			ls_sales_info, &
			ls_temp, &
			ls_temp2, &
			ls_roll_info
			
Date		ld_null_date

u_string_functions		lu_string_functions

s_error	lstr_error_info 
u_orp003		lu_orp003

u_ws_orp2       iu_ws_orp2

w_reservation_review_sales	lw_parent
lw_parent = Parent.GetParent()
If dw_detail.AcceptText() = -1 Then return False
// Loop Through the detail and make sure that all plants are filled in
ll_Row = dw_detail.Find("plant <= '000' or IsNull(plant)", 1, 10000)
If ll_row > 0 Then
	iw_Frame.SetMicroHelp("All plants must be valid before updating")
	dw_detail.SetRow(ll_Row)
	dw_detail.SetColumn('plant')
	dw_detail.SetFocus()
	dw_detail.Filter()
	return False
End if
SetPointer(Hourglass!)
ls_header = dw_header.Describe("DataWindow.Data")
SetNull(ld_null_date)
ll_RowCount = dw_detail.RowCount()
FOR li_Counter = 1 TO ll_RowCount
	ls_temp2=String(dw_detail.GetItemDate(li_Counter, 'change_ship_date'), &
				"yyyy-mm-dd")
	IF NOT IsDate(ls_temp2) THEN
		dw_detail.SetItem(li_Counter,'change_ship_date',ld_null_date)
	END IF
NEXT
ls_detail = lu_string_functions.nf_BuildUpdateString(dw_detail)
ll_RowCount = dw_detail.RowCount()
For li_Counter = 1 to ll_RowCount
	ls_temp = dw_detail.GetItemString(li_counter, 'update_flag')
	If ls_temp = 'A' or ls_temp = 'U' Then
		li_ChangedCounter ++
		lla_ChangedRows[li_ChangedCounter] = li_Counter
	End if
Next
If Not lu_string_functions.nf_IsEmpty(ls_Detail) Then
	lw_parent.istr_error_info.se_event_name = 'uf_update'
	ld_task_number = 0
	li_page_number = 0
	ls_Returned_detail = ''
	Do
		li_temp = lu_string_functions.nf_nPos(ls_Detail, '~r~n', 1, 99)
		If li_temp > 0 Then
			ls_RPC_Detail = Left(ls_Detail, li_Temp + 1)
			ls_Detail = Mid(ls_Detail, li_Temp + 2)
		Else
			ls_RPC_Detail = ls_detail
			ls_detail = ''
		End if
//			li_ret = lw_parent.iu_orp001.nf_orpo57ar(lstr_error_info, &
//														ls_header, &
//														ls_RPC_detail, &
//														ls_sales_info, &
//														ld_task_number, &
//														li_page_number, &
//														ls_roll_info)
			li_ret = lw_parent.iu_ws_orp3.uf_orpo57fr(lstr_error_info, &
														ls_header, &
														ls_RPC_detail, &
														ls_sales_info, &
														ls_roll_info)
//		If Left(ls_RPC_Detail, 2) <> '~r~n' And Not lu_string_functions.nf_IsEmpty(ls_RPC_Detail) Then
//			ls_RPC_Detail += '~r~n'
//		End if
		ls_Returned_Detail += ls_RPC_detail
		If li_ret < 0 Then 
			uf_ReplaceRows(ls_Returned_Detail, lla_ChangedRows, dw_detail)
			dw_detail.Filter()
			return False
		End if
		If li_ret > 0 Then lb_ErrorOccured = True
	Loop while Not lu_string_functions.nf_IsEmpty(ls_detail)
End if
uf_ReplaceRows(ls_Returned_Detail, lla_ChangedRows, dw_detail)
dw_detail.ResetUpdate()
If lb_ErrorOccured Then 
	// Find the first column with an error
	// The end value is one greater than the row count
	ll_end = dw_detail.RowCount( ) + 1
	ll_find = 1
	ll_find = dw_detail.Find("Pos(detail_errors,'E') > 0",  &
			ll_find, ll_end)
	If ll_find > 0 Then
		//Set position to that of the field with the error
		dw_detail.SetRow(ll_find)
		dw_detail.ScrollToRow(ll_find)
		dw_detail.SetColumn( &
				Pos(dw_detail.GetItemString(ll_find,"detail_errors"),'E') )
		dw_detail.SelectText(1,Len(dw_detail.GetText()))
	End If
	dw_detail.Filter()
	return False
End If
If dw_detail.DataObject = 'd_reservation_short_pa' Then
	// If this is the first time it was saved, blow it out
//	lu_orp003 = Create u_orp003

	iu_ws_orp2 = Create u_ws_orp2
	If Message.ReturnValue = -1 Then
		iw_Frame.SetMicroHelp("Reservation Could not be rolled out")
		dw_detail.Filter()
		return False
	End if	
	ls_header = This.Text + '~t~t~t~t~t~t~t~t~t~t~t~t~t~t~tR~t'
	ls_detail = ""
	lw_parent.istr_error_info.se_event_name = 'uf_update'
//	li_ret = lu_orp003.nf_orpo43ar( lw_parent.istr_error_info, &
//									ls_header, &
//									ls_detail)
	
	li_ret = iu_ws_orp2.nf_orpo43fr(ls_header, &
									ls_detail,lw_parent.istr_error_info)
	
	Destroy iu_ws_orp2
	//Destroy lu_orp003
	If li_ret = 0 Then
		lw_parent.ids_res_list.SetItem(lw_parent.tab_1.SelectedTab, 'order_status', 'I')
		This.ib_retrieved = FALSE
		lw_parent.tab_1.SelectTab(lw_parent.tab_1.SelectedTab)
	Else
		dw_detail.Filter()
		return False
	End if	
End if
This.ib_modified = FALSE
dw_detail.Filter()
return True
end function

public subroutine uf_cascadeplants ();Long	ll_ClickedRow,&
		ll_Tabpos,&
		ll_LoopCount,&
		ll_RowCount

String	ls_CopyPlant,&
			ls_Errors, &
			ls_ClickedCol
			
If dw_detail.AcceptText() = -1 Then return

ll_TabPos = Pos(dw_Detail.is_ObjectAtPointer, "~t")
ll_ClickedRow = Long(Mid(dw_Detail.is_ObjectAtPointer, ll_TabPos + 1))

// This should be either plant or change_plant_code
ls_ClickedCol = Left(dw_detail.is_ObjectAtPointer,ll_TabPos - 1)
ll_RowCount = dw_Detail.RowCount()
ls_CopyPlant = dw_Detail.GetItemString( ll_ClickedRow, ls_ClickedCol)

dw_Detail.SetRedraw( False)
IF dw_Detail.IsSelected( ll_ClickedRow) Then
	For ll_LoopCount = ll_ClickedRow to ll_RowCount
	 	IF dw_Detail.IsSelected( ll_LoopCount) Then 
				ls_Errors = dw_Detail.GetItemString( ll_LoopCount, "detail_errors")
				Choose Case ls_ClickedCol
					Case "plant"
						If Mid(ls_Errors,7,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,6) + 'M' + Mid( ls_Errors, 8))
					Case "change_plant_code"
						If Mid(ls_Errors,14,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,13) + 'M' + Mid( ls_Errors, 15))
				End Choose   						
				dw_detail.SetItem( ll_LoopCount, ls_ClickedCol , ls_CopyPlant)
				IF dw_Detail.GetItemStatus( ll_LoopCount, 0, Primary!) = NewModified!  OR &
						dw_detail.GetItemString( ll_LoopCount,"update_flag") = 'A' Then
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'A')
				ELSE
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'U')
				END IF
		END IF		
	Next
ELSE
	For ll_LoopCount = ll_ClickedRow to ll_RowCount
		IF Not(dw_Detail.IsSelected( ll_LoopCount)) Then 
				ls_Errors = dw_Detail.GetItemString( ll_LoopCount, "detail_errors")
				Choose Case ls_ClickedCol
					Case "plant"
						If Mid(ls_Errors,7,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,6) + 'M' + Mid( ls_Errors, 8))
					Case "change_plant_code"
						If Mid(ls_Errors,14,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,13) + 'M' + Mid( ls_Errors, 15))
				End Choose   										
				dw_detail.SetItem( ll_LoopCount, ls_ClickedCol, ls_CopyPlant)
				IF dw_Detail.GetItemStatus( ll_LoopCount, 0, Primary!) = NewModified!  OR &
						dw_detail.GetItemString( ll_LoopCount,"update_flag") = 'A' Then
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'A')
				ELSE
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'U')
				END IF
		END IF
	Next
END IF
dw_Detail.SetRedraw( True)
end subroutine

public subroutine uf_completeorder ();Int		li_page_number, &
			li_ret
Double	ld_task_number

String	ls_ResNum, &
			ls_header, &
			ls_detail, &
			ls_sales_info, &
			ls_roll_info
			
s_error	lstr_error_info

w_reservation_review_sales	lw_parent
lw_parent = Parent.GetParent()

// Need to update first
If Not uf_update() Then return 

This.SetRedraw(False)

ls_resnum = This.Text

ls_header = ls_ResNum + '~t~t~t' + String(id_gpo_date,"mm/dd/yyyy") + '~t~t~t~t~tX~t~t' + 'S' + '~t~t'

lw_parent.istr_error_info.se_event_name = 'uf_completeorder'

ld_task_number = 0
li_page_number = 0

//li_ret = lw_parent.iu_orp001.nf_orpo57ar(lstr_error_info, &
//										ls_header, &
//										ls_detail, &
//										ls_sales_info, &
//										ld_task_number, &
//										li_page_number, &
//										ls_roll_info)
										
li_ret = lw_parent.iu_ws_orp3.uf_orpo57fr(lstr_error_info, &
										ls_header, &
										ls_detail, &
										ls_sales_info, &
										ls_roll_info)

This.SetRedraw(True)

lw_parent.Trigger wf_remove_reservation()

return

end subroutine

public function integer uf_replacerows (string as_datatoimport, ref long al_rowstoreplace[], ref datawindow adw_datawindowtarget);Int	lia_SelectedRows[], &
		li_SelectedUpperBound, &
		li_Counter, &
		li_RowsToReplaceUpperBound

Long	ll_StartRow,&
		ll_EndRow,&
		ll_CRLF,&
		ll_OldCRLF, &
		ll_ArrayPos
		

String	ls_Replace
ll_EndRow = adw_DataWindowTarget.RowCount()

adw_DataWindowTarget.SetRedraw(False)

For li_Counter = 1 to ll_EndRow
	If adw_DataWindowTarget.IsSelected(li_Counter) Then
		li_SelectedUpperBound ++
		lia_SelectedRows[li_SelectedUpperBound] = li_counter
	End if
Next

li_RowsToReplaceUpperBound = UpperBound(al_rowstoreplace)

ll_ArrayPos = 1

ll_OldCRLF = 1

For li_Counter = 1 to li_RowsToReplaceUpperBound
	ll_CRLF = POS( as_DataToImport, "~r~n", ll_CRLF + 1)
	IF ll_crlf > 0 Then
		ls_Replace = Mid( as_DataToImport, ll_OldCRLF, ll_CRLF - ll_OldCRLF) 
	ELSE
		LS_replace = Mid(as_DataToImport, ll_OldCRLF)
	END IF
	ll_StartRow = al_RowsToReplace[ll_ArrayPos] + 1
	IF ll_StartRow <= ll_EndRow Then
		adw_DataWindowTarget.RowsMove( ll_StartRow, ll_EndRow, Primary!, &
												adw_DataWindowTarget, 1, Filter! )	

		adw_DataWindowTarget.RowsDiscard( al_RowsToReplace[ll_ArrayPos], &
											al_RowsToReplace[ll_ArrayPos], Primary!) 
		adw_DataWindowTarget.ImportString( ls_Replace)
		adw_DataWindowTarget.RowsMove( 1, adw_DataWindowTarget.FilteredCount(), Filter!, &
												adw_DataWindowTarget, adw_datawindowtarget.RowCount() +1 , Primary! )	
		ll_ArrayPos ++
	ELSE
		adw_DataWindowTarget.RowsDiscard( al_RowsToReplace[ll_ArrayPos], &
											al_RowsToReplace[ll_ArrayPos], Primary!) 
		adw_DataWindowTarget.ImportString( ls_Replace)
	END IF
	ll_OldCRLF = ll_CRLF + 2

Next


adw_DataWindowTarget.SelectRow(0, False)
For li_Counter = 1 to li_SelectedUpperBound
	adw_DataWindowTarget.SelectRow(lia_SelectedRows[li_counter], True)
Next

adw_DataWindowTarget.SetRedraw(True)
Return ll_ArrayPos
end function

public subroutine uf_showall (boolean ab_showall);If ab_showall Then
	is_filtered = 'N'
	cbx_showall.Checked = True
	dw_detail.SetFilter("")
	dw_detail.Filter()
	dw_detail.Sort()
Else
	is_filtered = 'Y'
	cbx_showall.Checked = False
//	dw_detail.SetFilter("line_status = 'S' or line_status = 'M' or line_status = 'N'")
	dw_detail.SetFilter("queue_status = 'M'")
	dw_detail.Filter()
	dw_detail.Sort()
End If
end subroutine

on u_reservation_review_tab.create
this.st_1=create st_1
this.cbx_showall=create cbx_showall
this.dw_detail=create dw_detail
this.dw_header=create dw_header
this.Control[]={this.st_1,&
this.cbx_showall,&
this.dw_detail,&
this.dw_header}
end on

on u_reservation_review_tab.destroy
destroy(this.st_1)
destroy(this.cbx_showall)
destroy(this.dw_detail)
destroy(this.dw_header)
end on

type st_1 from statictext within u_reservation_review_tab
integer x = 2528
integer y = 80
integer width = 247
integer height = 76
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Lines"
boolean focusrectangle = false
end type

event clicked;If is_filtered = 'Y' Then
	uf_showall(True)
Else
	uf_showall(False)
End If
end event

type cbx_showall from checkbox within u_reservation_review_tab
integer x = 2441
integer y = 16
integer width = 311
integer height = 68
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Show All"
end type

event clicked;If is_filtered = 'Y' Then
	uf_showall(True)
Else
	uf_showall(False)
End If
end event

type dw_detail from u_netwise_dw within u_reservation_review_tab
event ue_dwndropdown pbm_dwndropdown
integer x = 9
integer y = 364
integer width = 2761
integer height = 904
integer taborder = 40
string dataobject = "d_reservation_long_op"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dwndropdown;call super::ue_dwndropdown;DataWindowChild ldwc_plant
String ls_plant

This.GetChild("plant",ldwc_plant)
ls_plant = This.GetItemString(This.GetRow(),"plant")
ldwc_plant.Sort()
ldwc_plant.ScrollToRow(ldwc_plant.Find("location_code = '" + ls_plant + "'",1,ldwc_plant.RowCount()))
If This.DataObject = 'd_reservation_change_pa' Then
	This.GetChild("change_plant_code",ldwc_plant)
	ls_plant = This.GetItemString(This.GetRow(),"change_plant_code")
	ldwc_plant.Sort()
	ldwc_plant.ScrollToRow(ldwc_plant.Find("location_code = '" + ls_plant + "'",1,ldwc_plant.RowCount()))
End if
		
end event

event itemerror;call super::itemerror;
// If change_ship_date is spaces, allow it to be empty.
// The text color is changed to be the same as the background color.
IF Trim(data) = "" then
	return 2
end if

return 1
end event

event itemchanged;call super::itemchanged;DataWindowChild		ldwc_Plant

String	ls_errors
Decimal	ldc_value


IF This.is_ColumnName = 'plant' Then
	This.GetChild('plant', ldwc_plant)
	IF ldwc_Plant.Find( "location_code = '" +Data+"'", 1 ,&
							 ldwc_Plant.RowCount() ) < 1 Then
		iw_frame.SetMicroHelp("Invalid plant, please enter a valid plant")
		This.SelectText(1, Len(data))
		Return 1
	Else
		iw_frame.SetMicroHelp("Ready")
	END IF
END iF

IF This.is_ColumnName = 'ordered_units'Then
	IF Dec(Data)  < 0 Then
		iw_frame.SetMicroHelp("Ordered units cannot be negative")
		This.SelectText(1, Len(data))
		Return 1
	ELSE
		IF IsNull(This.GetItemNumber( row, 'ordered_units')) THEN
			ldc_value	=	0
		ELSE
			ldc_value	=	This.GetItemDecimal( row, 'ordered_units')
		END IF
		IF Dec(Data) >  ldc_value	Then
			iw_frame.SetMicroHelp("Ordered units cannot be greater than original")
			This.SelectText(1, Len(data))
			Return 1
		Else
			iw_frame.SetMicroHelp("Ready")
		End if
	END IF
END IF	


IF Long(This.Describe("update_flag.id")) > 0 THEN
	// This is specific for this window.  Since the only way 
	//	to add rows is through splitting them, and splitting them 
	// already puts the 'A' there, everything else must be an
	// existing row, and thus have a status of 'U'
	If This.GetItemString(Row, "update_flag") <> 'A' Then
		This.SetItem	(Row, "update_flag", "U")
	End if
END IF

ls_errors = Replace(This.GetItemString(row, 'detail_errors'), &
				This.GetColumn(), 1, 'M')
ib_modified = True

This.SetItem(row, 'detail_errors', ls_errors)
end event

event constructor;call super::constructor;
ib_updateable = True
is_Selection = '3'

end event

event clicked;String	ls_ColumnName

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

IF ls_ColumnName = "line_number" Then	
	Call Super::Clicked
ELSE
	This.SelectRow(0,False)
END IF
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end event

event rbuttondown;call super::rbuttondown;m_reservation_review_popup	lm_PopMenu
String	ls_ColName, &
			ls_errors
Long		ll_Row

ls_ColName = Left(is_ObjectAtPointer,Pos(is_ObjectAtPointer,"~t") -1)
ll_Row = Long(Mid(is_ObjectAtPointer,Pos(is_ObjectAtPointer,"~t") +1))
If ll_row < 1 Then return
ls_errors = This.GetItemString(ll_row,"detail_errors")

Choose Case ls_ColName
	Case "plant"
		If Mid(ls_errors,7,1) = 'V' Then return		
	Case "change_plant_code"
		If Mid(ls_errors,14,1) = 'V' Then return
	Case Else
		return
End Choose

lm_PopMenu = Create m_reservation_review_popup
lm_PopMenu.m_file.PopMenu(iw_frame.PointerX(), iw_Frame.PointerY())
Destroy lm_PopMenu

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild lds_Temp

This.GetChild( "ordered_units_uom", lds_Temp)
lds_Temp.SetTransObject( SQLCA)
lds_Temp.Retrieve()

end event

type dw_header from u_netwise_dw within u_reservation_review_tab
integer x = 9
integer width = 2779
integer height = 360
integer taborder = 10
string dataobject = "d_reservation_header_for_review"
boolean border = false
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_customer_name, &
						ldwc_customer_city, &
						ldwc_salesman_loc

String				ls_salesman, & 
						ls_service_center

w_reservation_review_sales	lw_parent
lw_parent = Parent.GetParent().GetParent()
  SELECT salesman.smanname
    INTO :ls_salesman  
    FROM salesman  
  WHERE salesman.smancode = :Message.is_salesperson_code AND salesman.smantype = 's';
This.object.sales_person_name.Text = ls_salesman
This.object.sales_location_code.Text = Message.is_smanlocation
	SELECT locations.location_name
	  INTO :ls_service_center
	  FROM locations
	WHERE locations.location_code = :Message.is_smanlocation;
This.Object.sales_location_name.Text = ls_service_center
This.Object.sales_location_name.Visible = True
This.GetChild('trans_mode', ldwc_salesman_loc)
iw_frame.iu_project_functions.nf_gettutltype( ldwc_salesman_loc, 'TRANMODE')
This.GetChild('customer_name', ldwc_customer_name)
This.GetChild('customer_city', ldwc_customer_city)
lw_parent.ids_customer.ShareData(ldwc_customer_name)
lw_parent.ids_customer.ShareData(ldwc_customer_city)
end event

