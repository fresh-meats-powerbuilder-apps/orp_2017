HA$PBExportHeader$w_pricing_review.srw
$PBExportComments$Pricing Review Queue
forward
global type w_pricing_review from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_pricing_review
end type
type dw_header from u_base_dw_ext within w_pricing_review
end type
end forward

global type w_pricing_review from w_base_sheet_ext
integer width = 4297
integer height = 1520
string title = "Pricing Review Queue"
dw_detail dw_detail
dw_header dw_header
end type
global w_pricing_review w_pricing_review

type variables
s_error 		istr_error_info
u_orp002		iu_orp002
u_ws_orp3		iu_ws_orp3
String		is_inquire, &
		is_cust_name, &
		is_store_id
Char		ic_auto_ind
TransAction	itr_WindowTransAction
application 	ia_apptool
w_tooltip_orp	iw_tooltip
long		il_xpos, &
		il_ypos
Datastore	ids_pricing_review_queue		
		
end variables
forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();String  						ls_header_in, &
								ls_detail_in, &
								ls_detail_out, &
								ls_header_out, &
								ls_temp_alltsrs, ls_temp_tsr
								
u_string_functions	lu_string_functions

is_inquire = dw_header.Describe("DataWindow.Data")
IF ic_auto_ind = "n" THEN
	OpenWithParm(w_pricing_review_inq, is_inquire)
	is_inquire = Message.StringParm
END IF
IF Len(is_inquire) = 0 THEN
	return false
END IF

dw_header.SetRedraw(false)
dw_header.Reset()
dw_header.ImportString(is_inquire)
ls_temp_alltsrs = dw_header.GetItemString(1,'alltsrs')
ls_temp_tsr = dw_header.GetItemString(1,'tsr')
IF ls_temp_alltsrs = 'Y' Then
	dw_header.SetItem(1,'tsr',"ALL")
END IF
If ic_auto_ind = "y" THEN
	dw_header.SetItem(1,'tsr',"CLK")
	ic_auto_ind = "n"
End If

dw_header.SetItem(1, 'alltsrs', 'I')
ls_header_in = dw_header.Describe("DataWindow.Data")
dw_header.SetItem(1, 'alltsrs', ls_temp_alltsrs)

//IF iu_orp002.nf_orpo69ar_pricing_review_inq(istr_error_info, &
//				ls_header_in, ls_detail_in, &
//				ls_detail_out, ls_header_out)  THEN

IF iu_ws_orp3.uf_orpo69fr_pricing_review_inq(istr_error_info, &
				ls_header_in, ls_detail_in, &
				ls_detail_out, ls_header_out)  THEN

	IF NOt lu_string_functions.nf_isempty(ls_header_out) THEN
		dw_header.Reset()
		dw_header.ImportString(ls_header_out)
		dw_header.SetItem(1, 'alltsrs', ls_temp_alltsrs)
	END IF
						  
	dw_header.SetItem(1, 'tsr', ls_temp_tsr)
	dw_detail.Reset()
 	dw_detail.ImportString(ls_detail_out)
	dw_header.SetRedraw(true)
	ids_pricing_review_queue.Reset()
	ids_pricing_review_queue.ImportString(ls_detail_out)
	return true
ELSE
	dw_header.SetItem(1, 'tsr', ls_temp_tsr)
	dw_header.SetRedraw(true)
	return false
END IF
end function

public function boolean wf_update ();String							ls_detail_in, &
									ls_detail_out, &
									ls_header_in, &
									ls_header_out, &
									ls_temp_alltsrs, &
									ls_temp, &
									ls_price_changed, &
									ls_sales_mgr_id

Integer							li_Counter, &
									li_ChangedCounter

Long								ll_RowCount, &
									lla_ChangedRows[]
Boolean							lb_RowUpdated									

u_string_functions	lu_string_functions
u_project_functions	lu_project_functions
									
SetPointer(HourGlass!)
If dw_detail.AcceptText() = -1 Then return False

lb_RowUpdated	 = False
ll_RowCount = dw_detail.RowCount()
FOR li_Counter = 1 TO ll_RowCount
	ls_temp = dw_detail.GetItemString(li_counter, 'update_flag')
	ls_price_changed = 'N'
	IF ls_temp = 'A' OR ls_temp = 'U' THEN
		// check 3 fields to see if they have changed from the original data value
		IF trim(ids_pricing_review_queue.GetItemString( li_Counter, 'sales_mgr_id' )) <> trim(dw_detail.GetItemString(li_counter, 'sales_mgr_id')) THEN
			lb_RowUpdated = True
		END IF
		IF trim(ids_pricing_review_queue.GetItemString( li_Counter,  'comments'  )) <> trim(dw_detail.GetItemString(li_counter,  'comments' )) THEN
			lb_RowUpdated = True
		END IF		
		IF ids_pricing_review_queue.GetItemDecimal( li_Counter, 'current_sales_price' ) <> dw_detail.GetItemDecimal( li_Counter, 'current_sales_price' ) THEN
			lb_RowUpdated = True
			dw_detail.SetItem(li_counter, 'price_changed_ind', 'Y')
		END IF	        			
		
		// Do NOT allow update if price is changed AND sales mgr id is not null
		ls_price_changed = dw_detail.GetItemString(li_counter, 'price_changed_ind')
		ls_sales_mgr_id = trim(dw_detail.GetItemString(li_counter, 'sales_mgr_id'))
		IF ls_price_changed = 'Y' and ls_sales_mgr_id <> "" Then
			iw_frame.SetMicroHelp("Changing Price and Entering Initials on the same line is not allowed.")
			This.SetRedraw(True)
			Return False
		END IF
		
		IF lb_RowUpdated THEN
			li_ChangedCounter ++
			lla_ChangedRows[li_ChangedCounter] = li_Counter
	    ELSE
		  	dw_detail.SetItem(li_counter, 'update_flag', '')
		END IF
	END IF
NEXT

dw_detail.AcceptText()

This.SetRedraw(False)
ls_detail_in = lu_project_functions.nf_buildUpdateString(dw_detail)
IF IsNull(ls_detail_in) THEN ls_detail_in = " "

ls_temp_alltsrs = dw_header.GetItemString(1,'alltsrs')
dw_header.SetItem(1,'alltsrs',"U")
ls_header_in = dw_header.Describe("DataWindow.Data")

IF lu_string_functions.nf_isempty(ls_detail_in) THEN
	iw_frame.SetMicroHelp("No Update Necessary")
	This.SetRedraw(True)
	return True
END IF

istr_error_info.se_event_name = "wf_update"

//IF iu_orp002.nf_orpo69ar_pricing_review_inq(istr_error_info, &
//				ls_header_in, ls_detail_in, &
//				ls_detail_out, ls_header_out)  THEN

IF iu_ws_orp3.uf_orpo69fr_pricing_review_inq(istr_error_info, &
				ls_header_in, ls_detail_in, &
				ls_detail_out, ls_header_out)  THEN

IF li_ChangedCounter > 0 THEN 
	lu_string_functions.nf_ReplaceRows(ls_detail_out, lla_ChangedRows, dw_detail)
ELSE
   	iw_frame.SetMicroHelp("No Update Necessary")
	This.SetRedraw(True)
END IF

//	ll_RowCount = dw_detail.RowCount()
//	FOR li_Counter = 1 TO ll_RowCount 
//		dw_detail.SetItem( li_Counter, "update_flag", " ")
//		IF dw_detail.GetItemString(li_Counter, "review_flag") <> "R" THEN
//			dw_detail.DeleteRow(li_Counter)
//			li_Counter --
//			ll_RowCount -- 
//		END IF
//	NEXT
	dw_header.ResetUpdate()
	dw_detail.ResetUpdate()

	This.SetRedraw(true)
	return true
ELSE
	This.SetRedraw(true)
	return false
END IF
end function

event ue_postopen;call super::ue_postopen;String 		ls_sman, &
				ls_location


message.triggerEvent("ue_getsmancode")
ls_sman = message.stringparm

message.triggerEvent("ue_getlocation")
ls_location = message.stringparm

dw_header.SetItem(1,"tsr", ls_sman)
dw_header.SetItem(1,"location", ls_location)

dw_header.Object.tsr.Protect = 1
//dw_header.Object.location.Protect = 1

iu_orp002 = Create u_orp002
iu_ws_orp3 = Create u_ws_orp3
is_inquire = ""
ic_auto_ind = "y"
iw_frame.im_menu.m_file.m_inquire.PostEvent(Clicked!)






end event

on w_pricing_review.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_pricing_review.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event close;call super::close;IF IsValid(iu_orp002) THEN
	Destroy iu_orp002
END IF

IF IsValid(iu_ws_orp3) THEN
	Destroy iu_ws_orp3
END IF
end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 75, this.height - dw_detail.y - 150)

long       ll_x = 75,  ll_y = 150

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)



end event

event ue_get_data;choose case as_value
	case 'ToolTip'
		Message.StringParm = Trim( is_cust_name + " " + is_store_id)
	case  "xpos"
		Message.StringParm = string(il_xpos)
	case  "ypos"
		Message.StringParm = string(il_ypos)
	case else
		Message.StringParm = ""
end choose
end event

event deactivate;if IsValid(iw_ToolTip) Then Close(iw_ToolTip)
end event

type dw_detail from u_base_dw_ext within w_pricing_review
integer y = 256
integer width = 5339
integer height = 1120
integer taborder = 20
string dataobject = "d_pricing_review_detail"
boolean hscrollbar = true
boolean vscrollbar = true
string icon = "AppIcon!"
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;STRING		ls_SalesID


CHOOSE CASE dwo.name
CASE	"order_num"
	ls_SalesID = This.GetItemString(row,"order_num")
	OPenWithPArm( w_list_of_orders, ls_SalesID)
CASE ELSE
	iw_Frame.SetMicroHelp("Please double click the order number")
END CHOOSE
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_Temp_Storage


dw_detail.GetChild("customer_name",ldwc_temp_storage)

iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_temp_storage)


end event

event clicked;call super::clicked;IF dwo.name = 'customer_name' Then
	is_cust_name = dw_detail.GetItemString(row, "customer_name")
	if len(trim(is_cust_name)) > 0 then
		il_xpos = gw_netwise_frame.PointerX() + 50
		il_ypos = gw_netwise_frame.PointerY() + 50
		openwithparm(iw_tooltip, parent, iw_frame)
	ELSE
		iw_frame.SetMicroHelp("Ready")
	END IF
ELSE	
	IF IsValid(iw_tooltip) Then
		close(iw_tooltip)
	END IF
END IF	

end event

event ue_mousemove;call super::ue_mousemove;ia_apptool = GetApplication()

IF ia_apptool.ToolBarTips = TRUE THEN
	IF dwo.name = 'customer_name' THEN
		is_cust_name = dw_detail.GetItemString(row, "customer_name")
		is_store_id = dw_detail.GetItemString(row, "store_id")
		IF len(trim(is_cust_name)) > 0 or len(trim(is_store_id)) > 0 THEN
			il_xpos = gw_netwise_frame.PointerX() + 50
			il_ypos = gw_netwise_frame.PointerY() + 50
			openwithparm(iw_tooltip, parent, iw_frame)
		ELSE
			iw_frame.SetMicroHelp("Ready")
		END IF
	ELSE	
		IF IsValid(iw_tooltip) THEN
			CLOSE(iw_tooltip)
		END IF
	END IF	
END IF

end event

event constructor;call super::constructor;ids_pricing_review_queue = create datastore
ids_pricing_review_queue.dataobject = 'd_pricing_review_detail'

end event

event ue_sort;call super::ue_sort;String		ls_detail_data

ls_detail_data = dw_detail.Describe("DataWindow.Data")

ids_pricing_review_queue.Reset()
ids_pricing_review_queue.ImportString(ls_detail_data)
end event

event ue_filter;call super::ue_filter;String		ls_detail_data

ls_detail_data = dw_detail.Describe("DataWindow.Data")

ids_pricing_review_queue.Reset()
ids_pricing_review_queue.ImportString(ls_detail_data)
end event

type dw_header from u_base_dw_ext within w_pricing_review
integer width = 1097
integer height = 228
integer taborder = 0
string dataobject = "d_pricing_review_header"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
This.Object.tsr.Background.Color = 12632256
//This.Object.location.Background.Color = 12632256
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild 	ldwc_temp_storage

u_project_functions	lu_project_functions

dw_header.GetChild("location",ldwc_temp_storage)
lu_project_functions.nf_getlocations(ldwc_temp_storage, '')

dw_header.GetChild("tsr",ldwc_temp_storage)
lu_project_functions.nf_gettsrs(ldwc_temp_storage, '')


end event

