HA$PBExportHeader$w_customer_markup.srw
$PBExportComments$window for hides customer markup
forward
global type w_customer_markup from w_netwise_sheet
end type
type dw_header from u_base_dw_ext within w_customer_markup
end type
type dw_detail from u_base_dw_ext within w_customer_markup
end type
end forward

global type w_customer_markup from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 2235
integer height = 1544
string title = "Customer Markup "
long backcolor = 79741120
event type boolean ue_save ( )
event type boolean ue_inquire ( )
dw_header dw_header
dw_detail dw_detail
end type
global w_customer_markup w_customer_markup

type variables


s_error 		istr_error_info
u_orp003		iu_orp003
u_ws_orp4       iu_ws_orp4

			
end variables
forward prototypes
public function boolean wf_update ()
public function boolean wf_retrieve ()
public subroutine wf_delete ()
end prototypes

event type boolean ue_save();//return ole_hides.object.save()
Return True
end event

event type boolean ue_inquire();boolean blnReturn

setPointer(HourGlass!)
This.Event Trigger CloseQuery()

//blnReturn = ole_hides.object.Inquire(True)

//ole_hides.SetRedraw ( True )
return  blnReturn
end event

public function boolean wf_update ();String							ls_header_string, &
									ls_input_string, &
									ls_output_string

Integer							li_rtn
									



If dw_detail.AcceptText() = -1 Then return False

istr_error_info.se_event_name = "wf_update"


ls_header_string = dw_header.Describe("DataWindow.Data") + '~t' + 'U' + '~t'
ls_input_string = dw_detail.Describe("DataWindow.Data")

//li_rtn = iu_orp003.nf_orpo08cr(istr_error_info, &
//										ls_header_string, &
//										ls_input_string, &
//										ls_output_string)	

li_rtn = iu_ws_orp4.nf_orpo08gr(istr_error_info, &
										ls_header_string, &
										ls_input_string, &
										ls_output_string)


If li_rtn = 0 Then
	Return True
Else
	Return False
End If





end function

public function boolean wf_retrieve ();boolean	blnReturn

string	ls_header_string, &
			ls_input_string, &
			ls_output_string, &
			ls_hide_plant, &
			ls_hide_desc
			
integer	li_rtn			
			
This.TriggerEvent("closequery")

istr_error_info.se_event_name = "wf_retrieve"

OpenWithParm( w_customer_markup_inq, This)

If Message.StringParm = "Cancel" Then
	Return False
End If

If dw_header.GetItemString(1,"hide_plant") = "ALL" then
	dw_header.SetItem(1, "hide_description", "ALL")
Else
	If not isnull(dw_header.GetItemString(1,"hide_plant")) Then
		ls_hide_plant = dw_header.GetItemString(1,"hide_plant")
		SELECT locations.location_name
		INTO :ls_hide_desc
		FROM Locations
		WHERE locations.location_code = :ls_hide_plant
		USING SQLCA ; 
		dw_header.SetItem(1, "hide_description", ls_hide_desc)
	End If
End IF


ls_header_string = dw_header.Describe("DataWindow.Data") + '~t' + 'I' + '~t'

//li_rtn = iu_orp003.nf_orpo08cr(istr_error_info, &
//										ls_header_string, &
//										ls_input_string, &
//										ls_output_string)	
										
li_rtn = iu_ws_orp4.nf_orpo08gr(istr_error_info, &
										ls_header_string, &
										ls_input_string, &
										ls_output_string)	

dw_detail.Reset()
dw_detail.ImportString(ls_output_string)
dw_detail.SetRedraw(True)

Return True
end function

public subroutine wf_delete ();String	ls_header_string, &
			ls_input_string, &
			ls_output_string
			
Integer	li_rtn			

istr_error_info.se_event_name = "wf_delete"


ls_header_string = dw_header.Describe("DataWindow.Data") + '~t' + 'D' + '~t'
ls_input_string = dw_detail.Describe("DataWindow.Data")

//li_rtn = iu_orp003.nf_orpo08cr(istr_error_info, &
//										ls_header_string, &
//										ls_input_string, &
//										ls_output_string)	
										
li_rtn = iu_ws_orp4.nf_orpo08gr(istr_error_info, &
										ls_header_string, &
										ls_input_string, &
										ls_output_string)	



end subroutine

event activate;call super::activate;boolean	lb_rtn
string	ls_classname, &
			ls_Add_Access,&
			ls_Del_Access,&
			ls_Mod_Access,&
			ls_Inq_Access

//// Set Security on ActiveX Control.
//ls_classname = UPPER(this.ClassName()) + "_EXT"
//lb_rtn	=	gw_netwise_frame.iu_netwise_data.nf_getaccess(ls_classname, &
//					ls_Add_Access, ls_Del_Access, ls_Mod_Access, ls_Inq_Access)
//					
//IF NOT lb_rtn THEN
//	// if the window is not listed in the security at all, then allow no permissions
//	ls_Add_Access =  'N'
//End if

//Rights to Add New Rows
IF UPPER(ls_Add_Access) = 'N' THEN
	iw_frame.im_menu.mf_Disable('m_addrow')
END IF	

iw_frame.im_menu.mf_Disable('m_new')
//iw_frame.im_menu.mf_Disable('m_delete')
//iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_delete')

iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

iw_frame.im_menu.mf_Disable('m_generatesales')		
iw_frame.im_menu.mf_Disable('m_complete')		
	
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_new')	
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

iw_frame.im_menu.mf_Enable('m_generatesales')		
iw_frame.im_menu.mf_Enable('m_complete')		



end event

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_temp

String				ls_data, ls_type_short_desc

If NOT IsValid(iu_orp003) Then
	iu_orp003 = Create u_orp003
End If

If NOT IsValid(iu_ws_orp4) Then
	iu_ws_orp4 = Create u_ws_orp4
End If


dw_detail.GetChild("premium_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMPR")

dw_detail.GetChild("wet_blue_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMWB")

dw_detail.GetChild("freight_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMFR")

dw_detail.GetChild("split_line_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMSL")

dw_detail.GetChild("split_line_credit_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMSC")

dw_detail.GetChild("lime_fleshed_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMLF")

dw_detail.GetChild("discount_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMDC")

dw_detail.GetChild("drop_split_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMDS")

dw_detail.GetChild("misc_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMMS")

dw_detail.GetChild("price_list_price_units", ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("HDPUOMPL")

ls_type_short_desc = 'N'

SELECT type_short_desc
	INTO :ls_type_short_desc
	FROM tutltypes
	WHERE tutltypes.record_type = 'HIDESHOW' and tutltypes.type_code = 'MARKET'
	USING SQLCA ; 

If mid(ls_type_short_desc,1,1) = 'N' Then 
	dw_detail.Object.auto_price_wet_blue_ind.Visible = False
	dw_detail.Object.converted_average_ind.Visible = False
	dw_detail.Object.cents_per_pound_ind.Visible = False
	dw_detail.Object.drop_split_price_units.Visible = False
	dw_detail.Object.drop_split_rate.Visible = False	
	dw_detail.Object.drop_split_price_units_t.Text = ""
	dw_detail.Object.auto_price_wet_blue_ind_t.Text = ""
	dw_detail.Object.converted_average_ind_t.Text = ""
	dw_detail.Object.cents_per_pound_ind_t.Text = ""
Else
	dw_detail.Object.auto_price_wet_blue_ind.Visible = True
	dw_detail.Object.converted_average_ind.Visible = True
	dw_detail.Object.cents_per_pound_ind.Visible = True
	dw_detail.Object.drop_split_price_units.Visible = True
	dw_detail.Object.drop_split_rate.Visible = True	
	dw_detail.Object.drop_split_price_units_t.Text = "Drop Split:" 
	dw_detail.Object.auto_price_wet_blue_ind_t.Text = "Auto Price Wet Blue:"
	dw_detail.Object.converted_average_ind_t.Text = "Converted Average:"
	dw_detail.Object.cents_per_pound_ind_t.Text = "Cents Per Pound:"
	
End If


This.wf_retrieve()
end event

on w_customer_markup.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_detail
end on

on w_customer_markup.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_detail)
end on

event resize;//if newheight > 10 then
//	ole_hides.Height = newheight 
//end if
//
//if newwidth> 10 then
//	ole_hides.width = newwidth 
//end if

//ole_chg_order_prod_dates.object.refresh
//ole_chg_order_prod_dates.setredraw(true)
//this.setredraw(true)
//wf_auto_hscrollbar()
end event

event closequery;//If ole_hides.object.CloseQuery Then
//	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
//	CASE 1	// Save Changes
//		If Not This.Event ue_save() Then
////			Message.ReturnValue = 1	 // Update failed - do not close window
//			Return 1
//		Else
////			Message.ReturnValue = 0
//		End If
//
//	CASE 2	// Do not save changes
////		Message.ReturnValue = 0
//
//	CASE 3	// Cancel the closing of window
////		Message.ReturnValue = 1
//		Return 1
//	END CHOOSE
//End If
//Return 0
end event

event ue_addrow;call super::ue_addrow;//ole_chg_order_prod_dates.object.AddRow()
end event

event ue_filenew;//ole_chg_order_prod_dates.object.AddRow()
end event

event close;call super::close;IF IsValid(iu_orp003) THEN
	Destroy iu_orp003
END IF
IF IsValid(iu_ws_orp4) THEN
	Destroy iu_ws_orp4
END IF

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case "hide_plant"
			Message.StringParm = dw_header.GetItemString(1,"hide_plant")
	Case "hide_customer"
			Message.StringParm = dw_header.GetItemString(1,"hide_customer")
	Case "hide_customer_type"
			Message.StringParm = dw_header.GetItemString(1,"hide_customer_type")
			If isnull(Message.StringParm) or (Message.StringParm = "") Then
				Message.StringParm = "S"
			End If
	Case "hide_product"
			Message.StringParm = dw_header.GetItemString(1,"hide_product")
End Choose
end event

event ue_set_data;call super::ue_set_data;choose case as_data_item
	case "hide_plant"
		dw_header.SetItem(1,"hide_plant", as_value)
	case "hide_customer"
		dw_header.SetItem(1,"hide_customer", as_value)
	case "hide_customer_type"
		dw_header.SetItem(1,"hide_customer_type", as_value)
	case "hide_product"
		dw_header.SetItem(1,"hide_product", as_value)		
end choose
end event

type dw_header from u_base_dw_ext within w_customer_markup
integer x = 64
integer y = 12
integer height = 196
integer taborder = 10
string dataobject = "d_customer_markup_hdr"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_detail from u_base_dw_ext within w_customer_markup
integer x = 64
integer y = 196
integer width = 1966
integer height = 996
integer taborder = 10
string dataobject = "d_customer_markup_dtl"
boolean hscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

