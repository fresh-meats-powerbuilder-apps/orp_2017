HA$PBExportHeader$w_sales_order_variance_inq.srw
$PBExportComments$Inquire window for Sales Order Variance Report
forward
global type w_sales_order_variance_inq from w_base_response_ext
end type
type dw_header from u_base_dw_ext within w_sales_order_variance_inq
end type
end forward

global type w_sales_order_variance_inq from w_base_response_ext
int X=480
int Y=680
int Width=1582
int Height=556
boolean TitleBar=true
string Title="Sales Order Variance Report Inquire"
long BackColor=12632256
dw_header dw_header
end type
global w_sales_order_variance_inq w_sales_order_variance_inq

type variables
// How to determine initial inquire data
String is_openstring
end variables

event ue_postopen;String						ls_sman, &
								ls_location
								
u_string_functions		lu_string_functions

IF NOT lu_string_functions.nf_IsEmpty(is_openstring) THEN 
	dw_header.Reset()
	dw_header.ImportString(is_openstring)
	dw_header.SetColumn("location")
	dw_header.SetColumn("review_date")
ELSE
	message.triggerEvent("ue_getsmancode")
	ls_sman = message.stringparm

	message.triggerEvent("ue_getlocation")
	ls_location = message.stringparm

	dw_header.SetItem(1,"location",ls_location)
	dw_header.SetItem(1,"tsr",ls_sman)
	dw_header.SetItem(1,"review_date",today())
	dw_header.SetItem(1,"alllocations","N")
	dw_header.SetItem(1,"alltsrs","N")
	dw_header.SetColumn("location")
	dw_header.SetColumn("review_date")
END IF


end event

on open;call w_base_response_ext::open;is_openstring = Message.StringParm
end on

on w_sales_order_variance_inq.create
int iCurrent
call super::create
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
end on

on w_sales_order_variance_inq.destroy
call super::destroy
destroy(this.dw_header)
end on

event ue_base_ok;u_string_functions		lu_string_functions

IF dw_header.AcceptText() = -1 THEN
	return
END IF

IF lu_string_functions.nf_isempty(dw_header.GetItemString(1,"tsr")) AND &
			dw_header.GetItemString(1,"alltsrs") = "N" AND &
			dw_header.GetItemString(1,"alllocations") = "N" THEN
	MessageBox("No TSR","TSR is a required field.")
	Return
END IF

IF lu_string_functions.nf_isempty(dw_header.GetItemString(1,"location")) AND &
			dw_header.GetItemString(1,"alllocations") = "N" THEN
	MessageBox("No Location","Location is a required field.")
	Return
END IF

IF lu_string_functions.nf_isempty(String(dw_header.GetItemDate(1,"review_date")) ) THEN
	MessageBox("No Review Date","Review Date is a required field.")
	Return
END IF

CloseWithReturn(This,dw_header.Describe("DataWindow.data"))
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_sales_order_variance_inq
int X=1175
int Y=316
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_sales_order_variance_inq
int X=1175
int Y=192
int TabOrder=50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_sales_order_variance_inq
int X=1175
int Y=68
int TabOrder=10
end type

type cb_browse from w_base_response_ext`cb_browse within w_sales_order_variance_inq
int TabOrder=20
end type

type dw_header from u_base_dw_ext within w_sales_order_variance_inq
event ue_dropdown pbm_dwndropdown
int X=59
int Y=52
int Width=960
int Height=392
int TabOrder=30
string DataObject="d_sales_order_variance_header"
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_temp_storage
 
u_project_functions	lu_project_functions

dw_header.GetChild("location",ldwc_temp_storage)
lu_project_functions.nf_getlocations(ldwc_temp_storage, '')

dw_header.GetChild("tsr",ldwc_temp_storage)
lu_project_functions.nf_gettsrs(ldwc_temp_storage,'')




end event

event ue_dwndropdown;//DataWindowChild	ldwc_temp_storage
//
//String				ls_location, &
//						ls_temp
//Integer					li_temp
//
//u_project_functions	lu_project_functions 
//
//dw_header.GetChild("tsr",ldwc_temp_storage)
//ls_location = dw_header.GetItemString(1, "location")
//
//lu_project_functions.nf_gettsrs(ldwc_temp_storage, ls_location)
//ldwc_temp_storage.SetTransObject(SQLCA)
//ldwc_temp_storage.Retrieve(ls_location, ls_location)
//
//
end event

