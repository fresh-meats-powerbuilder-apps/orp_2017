HA$PBExportHeader$u_abstractrunningobjectlist.sru
forward
global type u_abstractrunningobjectlist from u_abstractobjectlist
end type
end forward

global type u_abstractrunningobjectlist from u_abstractobjectlist
end type
global u_abstractrunningobjectlist u_abstractrunningobjectlist

forward prototypes
public function powerobject uf_queryinterface (string as_theClassName, string as_where)
public function boolean uf_getframe (ref Window aw_Frame)
public subroutine uf_addref (string as_theclassname, string as_class, powerobject ao_theobject, string as_where, string as_windowtype)
end prototypes

public function powerobject uf_queryinterface (string as_theClassName, string as_where);PowerObject	lpo_null

Return lpo_null

end function

public function boolean uf_getframe (ref Window aw_Frame);SetNull(aw_Frame)
return False
end function

public subroutine uf_addref (string as_theclassname, string as_class, powerobject ao_theobject, string as_where, string as_windowtype);
end subroutine

on u_abstractrunningobjectlist.create
TriggerEvent( this, "constructor" )
end on

on u_abstractrunningobjectlist.destroy
TriggerEvent( this, "destructor" )
end on

