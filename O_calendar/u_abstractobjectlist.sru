HA$PBExportHeader$u_abstractobjectlist.sru
forward
global type u_abstractobjectlist from nonvisualobject
end type
end forward

global type u_abstractobjectlist from nonvisualobject
end type
global u_abstractobjectlist u_abstractobjectlist

forward prototypes
public function boolean uf_initialize ()
public function string uf_lookupclass (string as_theClassName)
public function string uf_lookupwhere (string as_theClassName)
public function string uf_lookupduration (string as_theClassName)
public function string uf_lookupwindowtype (string as_class)
end prototypes

public function boolean uf_initialize ();Return True
end function

public function string uf_lookupclass (string as_theClassName);Return ""
end function

public function string uf_lookupwhere (string as_theClassName);return ""
end function

public function string uf_lookupduration (string as_theClassName);return ""
end function

public function string uf_lookupwindowtype (string as_class);return ''
end function

on u_abstractobjectlist.create
TriggerEvent( this, "constructor" )
end on

on u_abstractobjectlist.destroy
TriggerEvent( this, "destructor" )
end on

