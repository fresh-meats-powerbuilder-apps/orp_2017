HA$PBExportHeader$u_orp003.sru
$PBExportComments$Acts as communicating object with netwise for "orp003.dll"
forward
global type u_orp003 from u_netwise_transaction
end type
end forward

global type u_orp003 from u_netwise_transaction
end type
global u_orp003 u_orp003

type prototypes
// PowerBuilder Script File: c:\ibp\orp203\orp203.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Mon Mar 16 11:05:19 2015
// Source Interface File: c:\ibp\orp203\orp203.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: orpo08cr_inq_upd_customer_markup
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo08cr_inq_upd_customer_markup( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo08cr_inq_upd_customer_markup;Ansi"


//
// Declaration for procedure: orpo09cr_product_to_global_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo09cr_product_to_global_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo09cr_product_to_global_inq;Ansi"


//
// Declaration for procedure: orpo10cr_product_to_global_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo10cr_product_to_global_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo10cr_product_to_global_upd;Ansi"


//
// Declaration for procedure: orpo11cr_edi_trans_by_cust_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo11cr_edi_trans_by_cust_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    ref string header_string_out, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo11cr_edi_trans_by_cust_inq;Ansi"


//
// Declaration for procedure: orpo12cr_edi_trans_by_cust_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo12cr_edi_trans_by_cust_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    string detail_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo12cr_edi_trans_by_cust_upd;Ansi"


//
// Declaration for procedure: orpo28br_copy_order_retrieve
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo28br_copy_order_retrieve( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo28br_copy_order_retrieve;Ansi"


//
// Declaration for procedure: orpo29br_copy_order_update
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo29br_copy_order_update( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    string detail_string_in, &
    ref string detail_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo29br_copy_order_update;Ansi"


//
// Declaration for procedure: orpo30br_retrieve_edi_comments
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo30br_retrieve_edi_comments( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo30br_retrieve_edi_comments;Ansi"


//
// Declaration for procedure: orpo31br_validate_customer
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo31br_validate_customer( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo31br_validate_customer;Ansi"


//
// Declaration for procedure: orpo32br_GetHeaderInfo
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo32br_GetHeaderInfo( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    ref string header_info_out, &
    char Action_ind, &
    string weight_overide_string_in, &
    ref string weight_overide_string_out, &
    string weight_cost_string_in, &
    ref string weight_cost_string_out, &
    char addition_in, &
    ref char addition_out, &
    string age_type_code_in, &
    ref string age_type_code_out, &
    string pss_ind_in, &
    ref string pss_ind_out, &
    string load_instr_in, &
    ref string load_instr_out, &
    string protect_in, &
    ref string protect_out, &
    string pa_option, &
    ref string shortage_info, &
    string gtl_split_code_in, &
    ref string gtl_split_code_out, &
    string dept_code_info_in, &
    ref string dept_code_info_out, &
    ref string domestic_exporter_ind, &
    string export_country_in, &
    ref string export_country_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo32br_GetHeaderInfo;Ansi"


//
// Declaration for procedure: orpo33br_get_order_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo33br_get_order_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Sales_order_id, &
    string detail_info_in, &
    ref string detail_info_out, &
    string weight_str_in, &
    ref string weight_str_out, &
    string res_reductions_in, &
    ref string pa_resolve_out, &
    string action_ind, &
    ref string output_string, &
    ref double Task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string dept_codes_out, &
    string ck_dup_products_ind, &
    int CommHnd &
) library "orp203.dll" alias for "orpo33br_get_order_detail;Ansi"


//
// Declaration for procedure: orpo34br_Get_Order_Cust_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo34br_Get_Order_Cust_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string customer_info_in, &
    ref string customer_info_out, &
    string Weight_string_in, &
    ref string Weight_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo34br_Get_Order_Cust_info;Ansi"


//
// Declaration for procedure: orpo35br_accept_shortage
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo35br_accept_shortage( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string sales_order_id, &
    char ac_autoresolve, &
    string as_shortage_lines, &
    int CommHnd &
) library "orp203.dll" alias for "orpo35br_accept_shortage;Ansi"


//
// Declaration for procedure: orpo36br_get_cust_order
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo36br_get_cust_order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    string detail_string_in, &
    ref double task_number, &
    ref int Page_number, &
    ref string header_string_out, &
    ref string detail_string_out, &
    ref string so_id_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo36br_get_cust_order;Ansi"


//
// Declaration for procedure: orpo37br_get_Instructions
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo37br_get_Instructions( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Perm_Inst_in, &
    ref string Perm_Inst_out, &
    string Order_Instruction_In, &
    ref string Order_Instruction_Out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo37br_get_Instructions;Ansi"


//
// Declaration for procedure: orpo38br_pa_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo38br_pa_summary( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char process_option, &
    char availability_date[10], &
    string page_descr_in, &
    string plant_data_in, &
    string detail_data_in, &
    ref string plant_data_out, &
    ref string detail_data_out, &
    ref string file_descr_out, &
    string customer_id_in, &
    string date_type_ind_in, &
    string detail_string_in, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo38br_pa_summary;Ansi"


//
// Declaration for procedure: orpo39br_Gen_sales_ord
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo39br_Gen_sales_ord( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char Calling_window_ind, &
    string Header_in, &
    string Detail_in, &
    string Additional_data_in, &
    string res_reductions_in, &
    ref string header_out, &
    ref string detail_out, &
    ref string Additional_data_out, &
    ref string bm_start_time, &
    ref string bm_end_time, &
    ref string bm_task_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo39br_Gen_sales_ord;Ansi"


//
// Declaration for procedure: orpo40br_valid_product_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo40br_valid_product_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char sku_product_code[10], &
    ref string valid_product_info, &
    int CommHnd &
) library "orp203.dll" alias for "orpo40br_valid_product_info;Ansi"


//
// Declaration for procedure: orpo41br_Get_Sale_person_default
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo41br_Get_Sale_person_default( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string userid, &
    ref string sales_person_info_string, &
    int CommHnd &
) library "orp203.dll" alias for "orpo41br_Get_Sale_person_default;Ansi"


//
// Declaration for procedure: orpo42br_so_by_cust
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo42br_so_by_cust( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    ref string header_info_out, &
    string detail_info_in, &
    ref string detail_info_out, &
    ref double task_number, &
    ref int page_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo42br_so_by_cust;Ansi"


//
// Declaration for procedure: orpo43br_Roll_out
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo43br_Roll_out( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string_in, &
    string Detail_String_in, &
    ref string header_string_out, &
    ref string detail_string_out, &
    int CommHnd &
) library "orp203.dll" alias for "orpo43br_Roll_out;Ansi"


//
// Declaration for procedure: orpo44br_pa_inq_inquire
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo44br_pa_inq_inquire( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_info_in, &
    string main_info_in, &
    char ind_process_option, &
    char window_ind, &
    ref string output_string, &
    double task_number_in, &
    double page_number_in, &
    ref double task_number_out, &
    ref double page_number_out, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo44br_pa_inq_inquire;Ansi"


//
// Declaration for procedure: orpo45br_co_so
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo45br_co_so( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo45br_co_so;Ansi"


//
// Declaration for procedure: orpo46br_res_util
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo46br_res_util( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string detail_info, &
    ref string perc_String, &
    ref double task_number, &
    ref int page_number, &
    int CommHnd &
) library "orp203.dll" alias for "orpo46br_res_util;Ansi"


//
// Declaration for procedure: orpo47br_res_short
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo47br_res_short( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    ref string header_info_out, &
    ref string detail_info_out, &
    string as_shortage_lines, &
    char rpc_indicator, &
    int CommHnd &
) library "orp203.dll" alias for "orpo47br_res_short;Ansi"


//
// Declaration for procedure: orpo48br_update_shortage
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo48br_update_shortage( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    string detail_info_in, &
    ref string detail_info_out, &
    char rpc_ind, &
    int CommHnd &
) library "orp203.dll" alias for "orpo48br_update_shortage;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "orp203.dll"
function int WBorp203CkCompleted( int CommHnd ) &
    library "orp203.dll"
//
// ***********************************************************


end prototypes

type variables
Private:
int ii_orp003_commhandle
u_Ole_Com	iu_OleProphet


end variables

forward prototypes
public function integer nf_orpo45ar (ref s_error astr_error_info, string as_input_string, ref string as_output)
public function integer nf_orpo40ar (ref s_error astr_error_info, character ac_sku_product_code[10], ref string as_valid_product_info)
public function integer nf_orpo43ar (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function integer nf_orpo37ar (ref s_error astr_error_info, ref string as_instruction_key, string as_perm_ins_in, ref string as_perm_out)
public function integer nf_orpo36ar (ref string as_header_string, ref s_error astr_error_info, ref string as_detail_string, ref string as_so_id_string, ref double ad_tasknumber, ref integer ai_page_number)
public function integer nf_orpo46ar (ref s_error astr_error_info, ref string as_detail_info, ref string as_per_string, ref double ad_task_number, ref integer ai_page_number)
public function integer nf_orpo39ar (ref s_error astr_error_info, character ac_calling_window_ind, ref string as_rescus_header, ref string as_rescus_detail, ref string as_additional_data, string as_res_info)
public function integer nf_orpo35ar (ref s_error astr_error_info, ref string as_sales_id, character ac_auto_resolve, string as_shortage_lines)
public function integer nf_orpo47ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, string as_shortage_lines, character ac_ind)
public function integer nf_orpo48ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, character ac_ind)
public function boolean nf_write_benchmark2 (time at_starttime, time at_endtime, string as_function_name, time at_start_rpc, time at_end_rpc, string as_other_info, string as_task_number)
public function integer nf_orpo34ar (ref s_error astr_error_info, ref string as_customer_info, ref string as_weight_string)
public function integer nf_orpo31br (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo28br (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo29br (ref s_error astr_error_info, string as_header_string_in, string as_detail_string_in, ref string as_detail_string_out)
public function integer nf_orpo30br (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo42ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber)
public function integer nf_orpo32ar (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country)
public function integer wbckcompleted (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country)
public function integer wborp203ckcompleted (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country)
public function integer nf_orpo08cr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_output_string)
public function integer nf_orpo41ar (ref s_error astr_error_info, string as_userid, ref string as_sales_person_info_string)
public function integer nf_orpo10cr_product_to_global_upd (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo09cr_product_to_global_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_detail_string)
public function integer nf_orpo38ar (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out, string as_customer_id, string as_date_type_ind, string as_detail_string, ref string as_prod_data)
public function integer nf_orpo44ar (ref s_error astr_error_info, string as_input_info_in, string as_main_info_in, character ac_ind_process_option, ref string as_main_info_out, ref string as_so_info_out, ref string as_age_info_out, ref string as_production_dates, double ad_task_number_in, double ad_page_number_in, ref double ad_task_number_out, ref double ad_page_number_out, string as_window_ind)
public function integer nf_orpo11cr_edi_trans_by_cust_inq (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string)
public function integer nf_orpo33ar (ref s_error astr_error_info, string as_order_id, ref string as_detail_info, ref string as_weight_str, ref string as_res_reduce_in, ref string as_pa_resolve_out, string as_action_ind, ref string as_production_dates, ref string as_dept_codes, string as_dup_products_ind)
public function integer nf_orpo12cr_edi_trans_by_cust_upd (ref s_error astr_error_info, string as_header_string, string as_detail_string)
end prototypes

public function integer nf_orpo45ar (ref s_error astr_error_info, string as_input_string, ref string as_output);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, ls_output
			
Double	ld_task_number, &
			ld_last_number, &
			ld_max_number


SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Wait...PC communicating with Mainframe")

as_output = ""
ld_task_number = 0
ld_last_number = 0
ld_max_number = 0
ls_output = SPACE(20000)
ls_output = Fill(Char(0), 20000)
as_input_string = Trim( as_Input_String)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message)
// /// //// /////
Do
//	li_rtn = orpo45br_co_so(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message, &
//									as_input_string, &
//									ls_output, &
//									ld_task_number, &
//									ld_last_number, &
//									ld_max_number, &
//									ii_orp003_commhandle) 
	as_output += ls_output
Loop While ld_last_number <> ld_max_number
// /// //// /////
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo40ar (ref s_error astr_error_info, character ac_sku_product_code[10], ref string as_valid_product_info);int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



as_valid_product_info = Fill( char(0), 474)

gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


// Call a Netwise external function to get the product information
//li_rtn = orpo40br_valid_product_info(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, ac_sku_product_code, &
//			as_valid_product_info, ii_orp003_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 



// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)


return li_rtn

end function

public function integer nf_orpo43ar (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);Int 	li_rtn

String 	ls_detail_string_in,&
			ls_detail_string_out,&
		 	ls_header_string_in,&
		 	ls_header_string_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



gw_NetWise_Frame.SetMicroHelp( "Wait ... Rolling Out Reservation")
SetPointer(HourGlass!)

ls_detail_string_out = Space(16999)
ls_header_string_out = Space(208)

ls_detail_string_out = Fill( Char(0), 16999)
ls_header_string_out = Fill(Char(0), 208)


ls_detail_string_in  = Trim( as_detail_string)
ls_header_string_in  = Trim( as_header_string)

astr_error_info.se_procedure_name = 'orpo43br_Roll_out'
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =  orpo43br_Roll_out( &
//    ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//    ls_header_string_in, &
//    ls_detail_string_in, &
//    ls_header_string_out,&
//    ls_detail_string_out,&
//    ii_orp003_commhandle) 

as_header_string =  TRIM(ls_header_string_out)
as_detail_string =  TRIM(ls_detail_string_out)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn

end function

public function integer nf_orpo37ar (ref s_error astr_error_info, ref string as_instruction_key, string as_perm_ins_in, ref string as_perm_out);int li_rtn

STRING	ls_instruction_out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



ls_MicroHelp = "Wait...PC communicating with Mainframe"

ls_instruction_out = SPACE(216)
as_Perm_out = Space(216)

ls_instruction_out = Fill(Char(0),216)
as_Perm_out = Fill(char(0),216)
as_instruction_key = Trim(as_instruction_key)
as_Perm_ins_in = Trim( as_Perm_ins_in)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
//li_rtn = orpo37br_get_instructions(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, as_instruction_key, ls_instruction_out, &
//			as_Perm_ins_in, as_Perm_out, ii_orp003_commhandle)
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

as_instruction_key = ls_instruction_out


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)


Return li_rtn

end function

public function integer nf_orpo36ar (ref string as_header_string, ref s_error astr_error_info, ref string as_detail_string, ref string as_so_id_string, ref double ad_tasknumber, ref integer ai_page_number);int 		li_rtn

String 	ls_header_string_in, &
			ls_header_string_out,&
			ls_detail_string_in, &
			ls_detail_string_out, &
			ls_so_id_string_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_header_string_in = as_header_string
ls_detail_string_in = as_detail_string

astr_error_info.se_procedure_name = "nf_orpo36br"
astr_error_info.se_message = Space(71)
astr_error_info.se_message = Fill(char(0),100)


ls_header_string_out = Space(208)
ls_so_id_string_out = Space(12871)
ls_detail_string_out = Space(21483)
ls_header_string_out = Fill(CHAR(0),208)
ls_so_id_string_out = Fill(CHAR(0),12871)
ls_detail_string_out = Fill(CHAR(0),21483)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


// Call a Netwise external function to get the required information
//li_rtn  = orpo36br_get_cust_order( ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_header_string_in,&
//		  		   ls_detail_string_in, &
//					ad_Tasknumber, &
//					ai_page_number,&
//			 		ls_header_string_out, &
//					ls_detail_string_out,&
//					ls_so_id_string_out, &
//					ii_orp003_commhandle)



nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)



as_header_string = Trim(ls_header_string_out )
as_detail_string = Trim(ls_detail_string_out)
as_so_id_string  = Trim(ls_so_id_string_out)

return li_rtn




end function

public function integer nf_orpo46ar (ref s_error astr_error_info, ref string as_detail_info, ref string as_per_string, ref double ad_task_number, ref integer ai_page_number);Boolean  lb_message_val

Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



SetPointer(HourGlass!)

as_per_string	= Space(21)
as_Per_String	= Fill(Char(0),21)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =  orpo46br_res_util( &
//	ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//	as_detail_info, &
//	as_per_String,&
//	ad_task_number,&
//	ai_page_number,&
//	ii_orp003_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
lb_message_val =THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
Return li_rtn
end function

public function integer nf_orpo39ar (ref s_error astr_error_info, character ac_calling_window_ind, ref string as_rescus_header, ref string as_rescus_detail, ref string as_additional_data, string as_res_info);Char			lc_Region
DataStore 	lds_Tmp, lds_temp_data

TIME		lt_startTime, &
			lt_endTime, &
			lt_start_RPC, &
			lt_end_RPC

int 		li_rtn

Long		ll_RowCount,&
			ll_LoopCount,&
			ll_OleRtn, ll_row
			
Decimal	ldec_Price,&
			ldec_quantity
			

String 	ls_rescus_detail_out, &
		 	ls_rescus_Header_out, &
			ls_Additional_data_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message,&
			ls_QueueString,&
			ls_QueueName,&
			ls_Age, &
			ls_other_info, &
			ls_task_number, &
			ls_start_time, &
			ls_end_time

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_rescus_detail_out = Space(60000)
ls_rescus_Header_out = Space(60000)
ls_rescus_detail_out = FILL(CHAR(0), 60000)
ls_rescus_Header_out = FILL(CHAR(0), 60000)
ls_Additional_data_out = Space(60000)
ls_Additional_data_out = FILL(Char(0),60000)
ls_start_time = Space(8)
ls_start_time = FILL(Char(42), 8)
ls_end_time = Space(8)
ls_end_time = FILL(Char(42), 8)
ls_task_number = Space(10)
ls_task_number = FILL(Char(42), 10)


lc_region = Right(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Netwise Server Info", "ServerSuffix", "t"), 1)
Choose Case lower(lc_region)
	Case 'p'
		ls_QueueName = "ProfitQueue"
	Case 'z'
				ls_QueueName = "ProfitQueueQA"
	Case Else
		ls_QueueName = "ProfitQueueTest"
End Choose

//lds_Tmp = Create DataStore
//lds_Tmp.DataObject = 'd_customer_order'
//ll_RowCount  = lds_Tmp.ImportString(as_rescus_detail)
//ls_QueueString = ''
//For ll_LoopCount = 1 to ll_RowCount
//	   ldec_Price = lds_tmp.GetItemDecimal(ll_LoopCount,"sales_price")
//		ldec_Quantity = lds_Tmp.GetItemDecimal(ll_LoopCount,"ordered_units") 
//		ls_Age = lds_Tmp.GetItemString(ll_LoopCount,"ordered_age")
//		ls_QueueString += lds_Tmp.GetItemString(ll_LoopCount,"sku_product_code")+"~t"+String(ldec_Quantity)+"~t"+String(ldec_Price)+"~t"+ls_Age+"~r~n"
//End for
ls_QueueString = "orpo39b" + "~v" + string(ac_calling_window_ind) + "~v" + as_additional_data + "~v" &
				+ as_res_info + "~v" + as_rescus_header + "~v" + as_rescus_detail
IF Len(Trim(ls_QueueString)) > 0 Then
	IF Not IsValid(iu_oleprophet) Then 
		iu_oleprophet = Create u_Ole_Com
		ll_OleRtn = iu_OleProphet.ConnectToNewObject("ProphetQueue.ProphetSender.1")
		IF ll_OleRtn = 0 Then
			iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
		Else
			Destroy iu_oleprophet
		End if
	Else
		iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
	End if
End if

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
lt_startTime = Now()

// Call a Netwise external function to get the required information
//li_rtn = orpo39br_gen_sales_ord(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ac_Calling_window_ind, &
//    				as_rescus_header, &
//    				as_rescus_detail, &
//    				as_additional_Data, &
//    				as_res_info, &
//    				ls_rescus_header_out, &
//    				ls_rescus_detail_out, &
//    				ls_Additional_data_out, &
//					ls_start_time, &
//					ls_end_time, &
//					ls_task_number, &
//    				ii_orp003_commhandle) 
			
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

lt_endTime = Now()
ls_procedure_name = 'nf_orp39ar'
//lt_start_RPC = Time(ls_start_time)
//lt_end_RPC = Time(ls_end_time)

lt_start_RPC = Time(ls_window_name)
lt_end_RPC = Time(ls_function_name)
lds_temp_data = create Datastore
choose case ac_calling_window_ind
	case 'R'
		lds_temp_data.dataobject = 'd_reservation_long'
	case 'C'
		lds_temp_data.dataobject = 'd_customer_order_rpc'
end choose
ll_row = lds_temp_data.importstring(ls_rescus_detail_out)
ls_other_info = "Number of detail lines = " + string(ll_row)
ls_task_number = ls_app_name

//benchmark call
nf_write_benchmark2(lt_startTime, lt_endtime, ls_procedure_name, &
					lt_start_RPC, lt_end_RPC, ls_other_info, ls_task_number)
			
as_rescus_detail = ls_rescus_detail_out 
as_rescus_header = ls_rescus_Header_out 
as_additional_Data = ls_Additional_data_out

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
IF li_rtn = 8 Then
	MessageBox("DB2 ERROR","* * * A DeadLock has occurred * * *")
	Return 5
End If
Return li_rtn

end function

public function integer nf_orpo35ar (ref s_error astr_error_info, ref string as_sales_id, character ac_auto_resolve, string as_shortage_lines);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message			 

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

//li_rtn  = orpo35br_accept_shortage(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message ,&
//					as_sales_id, &
//					ac_Auto_Resolve ,&
//					as_shortage_lines, &
//					ii_orp003_commhandle ) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
return li_rtn
end function

public function integer nf_orpo47ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, string as_shortage_lines, character ac_ind);Boolean  lb_message_val

Int 		li_rtn

STRING	ls_header_info_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

ls_header_info_out = SPACE(34)
as_detail_info = SPACE(21000)
as_detail_info = Fill(Char(0),21000)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Retrieving data. Please Wait.")

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =  orpo47br_res_short( &
//    	ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//		as_header_info, &
//		ls_header_info_out, &	
//    	as_detail_info, &
//		as_shortage_lines, &
//		ac_ind, &
//	   ii_orp003_commhandle)

nf_set_s_error (astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

as_header_info = TRIM(ls_header_info_out)
as_detail_info = TRIM(as_detail_info)

THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo48ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, character ac_ind);Boolean  lb_message_val

Int 		li_rtn

STRING 	ls_detail_info_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

ls_detail_info_out = SPACE(21000)
ls_detail_info_Out = Fill(Char(0),21000)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Recalculating scheduled quantities. Please Wait.")

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =  orpo48br_Update_Shortage( &
//    	ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//		as_header_info, &
//    	as_detail_info, &
//		ls_detail_info_out, &
//		ac_ind, &
//	   ii_orp003_commhandle)

as_detail_info = TRIM(ls_detail_info_out)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
	
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function boolean nf_write_benchmark2 (time at_starttime, time at_endtime, string as_function_name, time at_start_rpc, time at_end_rpc, string as_other_info, string as_task_number);char 					lc_PCIP_Address[100], &
						lc_region

Integer				li_fileNum, &
						li_TimeVal
						
Long					ll_SecondsAfterPC, &
						ll_secondsAfterRPC

String				ls_FileName, &
						ls_ipaddr, &
						ls_pcip_address, &
						ls_path

u_ip_functions		lu_ip_address_functions


li_TimeVal = ProfileInt(gw_netwise_frame.is_WorkingDir + "ibp002.ini",&
								"BenchMark",&
								Message.nf_Get_App_ID(),6)
								
IF li_timeVal = 0 Then Return False								

ll_SecondsAfterPC = SecondsAfter(at_starttime, at_endtime) 
ll_secondsAfterRPC = SecondsAfter(at_start_rpc, at_end_rpc)

IF ll_SecondsAfterPC < li_TimeVal Then Return False 

lc_region = Right(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "nw_server", "server" + &
			String(1), ""), 1)
Choose Case lc_region
	Case 'p'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "f:\software\pb\pblog101\")
//		ls_path = "f:\software\pb\pblog101\"
	Case 't'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "j:\test\pb\")
//		ls_path = "j:\pb\test\exe32\"
	Case 'z'
		ls_path = ProfileString(gw_netwise_frame.is_WorkingDir + 'ibp002.ini', "BenchMark", 'PATH', "\\NTWHQ03\NTWHQ03\software\pb\pblog101\")
//		ls_path = "j:\software\pb\qa\pblog101\"
	Case ''
		Return False
End Choose

ls_fileName = ls_path + "BM" + &
					String(Month(Today()), '00') + &
					String(Day(Today()), '00') + &
					".log"

li_FileNum = FileOpen(ls_FileName,  &
	lineMode!, Write!, LockWrite! , Append! )
	
ls_ipaddr = lu_ip_address_functions.nf_get_ip_address()

FileWrite(li_FileNum, Message.nf_Get_App_ID() + &
			"~tUser: " + Trim(SQLCA.Userid) + &
			"~tIP Addr: " + Trim(ls_ipaddr) + &
			"~tTask Number: " + Trim(as_task_number) + &
			"~tProcedure Name: " + Trim(as_function_name) + &
			"~tPC Start: " + Trim(String(At_StartTime,"hh:mm:ss")) + &
			"~tPC End: " + Trim(String(At_EndTime,"hh:mm:ss")) + &
			"~tPC Elapsed: " + Trim(String(ll_SecondsAfterPC)) + &
			"~tRPC Start: " + Trim(String(at_start_rpc,"hh:mm:ss")) + &
			"~tRPC End: " + Trim(String(at_end_rpc,"hh:mm:ss")) + &
			"~tRPC Elapsed: " + Trim(String(ll_secondsAfterRPC)) + &
			"~t" + Trim(as_other_info))

FileClose(li_FileNum)

Return True
end function

public function integer nf_orpo34ar (ref s_error astr_error_info, ref string as_customer_info, ref string as_weight_string);Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number
				
int 		li_rtn
	

String	ls_customer_info_out,&
			ls_Weight_string_out
			
			
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_production_dates
			
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

ls_production_dates = space(3000)
ls_customer_info_out = Space(30000)
ls_Weight_string_out = Space(30000)

ls_production_dates = fill(char(0),3000)
ls_customer_info_out = Fill(Char(0),30000)
ls_Weight_string_out = Fill(Char(0),30000)
as_Weight_string		= Trim(as_Weight_string)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

do

	// Call a Netwise external function to get the customer information
//	li_rtn = orpo34br_Get_Order_Cust_info( &
//		 ls_app_name, &
//		 ls_window_name,&
//		 ls_function_name,&
//		 ls_event_name, &
//		 ls_procedure_name, &
//		 ls_user_id, &
//		 ls_return_code, &
//		 ls_message, &
//		 as_customer_info, &
//		 ls_customer_info_out, &
//		 as_Weight_string, &
//		 ls_Weight_string_out, &
//		 ii_orp003_commhandle)
	 							
									
Loop while ld_last_record_number <> ld_max_record_number and li_rtn > -1

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

as_customer_info 	= Trim(ls_customer_info_out)
as_weight_string	= Trim( ls_Weight_string_out)
return li_rtn

end function

public function integer nf_orpo31br (ref s_error astr_error_info, string as_input_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Wait...PC communicating with Mainframe")


as_input_string = Trim( as_Input_String)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message)
// /// //// /////
//li_rtn = orpo31br_validate_customer(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message, &
//									as_input_string, &
//									ii_orp003_commhandle) 

// /// //// /////
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo28br (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Wait...PC communicating with Mainframe")


as_input_string = Trim( as_Input_String)

as_output_string 	      = SPACE(1000)
as_output_string 			= Fill(Char(0),1000) 

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message)
// /// //// /////
//li_rtn = orpo28br_copy_order_retrieve(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message, &
//									as_input_string, &
//									as_output_string, &
//									ii_orp003_commhandle) 

// /// //// /////
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo29br (ref s_error astr_error_info, string as_header_string_in, string as_detail_string_in, ref string as_detail_string_out);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Wait...PC communicating with Mainframe")

as_header_string_in = Trim( as_header_string_in)
as_detail_string_in = Trim( as_detail_string_in)


as_detail_string_out      = SPACE(20000)
as_detail_string_out		= Fill(Char(0),20000) 

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message)

//li_rtn = orpo29br_copy_order_update(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message, &
//									as_header_string_in, &
//									as_detail_string_in, &
//									as_detail_string_out, &
//									ii_orp003_commhandle) 
//
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo30br (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message, &
			ls_output
			
Double	ld_task_number, &
			ld_last_number, &
			ld_max_number


SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "orpo30br_edi_comments_detail"
astr_error_info.se_message = space(71)

as_output_string = ""

ld_task_number = 0
ld_last_number = 0
ld_max_number = 0

ls_output = SPACE(20000)
ls_output = Fill(Char(0), 20000)
as_input_string = Trim( as_Input_String)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

Do
//    li_rtn = orpo30br_retrieve_edi_comments( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//    as_input_string, &
//    ls_output, &
//    ld_task_number, &
//    ld_last_number, &
//    ld_max_number, &
//    ii_orp003_commhandle)
	 
 	as_output_string += ls_output

Loop While ld_last_number <> ld_max_number and li_rtn >= 0

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo42ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref double ad_tasknumber, ref integer ai_pagenumber);INTEGER 	li_Rtn

STRING 	ls_Header_Out, &
			ls_Detail_Out,&
			ls_MicroHelp, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



ls_MicroHelp = "Wait...PC communicating with Mainframe"

ls_Header_Out = SPACE(1000)
ls_Detail_Out = SPACE(50000)

ls_Header_Out = Fill(Char(0), 1000)
ls_Detail_Out = Fill(Char(0), 50000)

as_Header_Info = Trim(as_Header_Info)
as_Detail_Info = Trim( as_Detail_Info)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_Rtn =  orpo42br_so_by_cust(&
//   	ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//    	as_Header_Info, &
//		ls_Header_Out, &
//    	as_Detail_Info, &
//		ls_Detail_Out, &
//		ad_tasknumber, &
//		ai_pagenumber, &
//	   ii_ORP003_CommHandle) 

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

THIS.nf_Display_Message(li_Rtn, astr_Error_Info, ii_ORP003_CommHandle)

as_header_info = ls_Header_Out
as_detail_info = ls_Detail_Out

Return li_Rtn

end function

public function integer nf_orpo32ar (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country);Int 		li_rtn
String	ls_header_out, &
			ls_weight_overide_out, &
			ls_weight_cost_out, &
			ls_age_type_code_out, &
			ls_pss_ind_out, &
			ls_loading_instr_out, &
			ls_protect_out, &
			ls_shortage_out, &
			ls_exp_country

char     lc_addition_out

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_gtl_code, &
			ls_dept_code
			
ls_header_out 				= SPACE(30000)
ls_weight_overide_out 	= SPACE(30000)
ls_weight_cost_out		= SPACE(30000)	
ls_shortage_out			= SPACE(10000)
ls_header_out 				= Fill(Char(0),30000)
ls_weight_overide_out 	= Fill(char(0),30000)
ls_weight_cost_out		= FILL(char(0),30000)
ls_shortage_out			= Fill(char(0),10000)
lc_addition_out			= SPACE(1)
ls_age_type_code_out		= SPACE(1)
ls_pss_ind_out				= SPACE(1)
ls_loading_instr_out		= space(1)
ls_protect_out				= space(1)
//dld
ls_gtl_code 				= space(1)
ls_dept_code				= space(10)
//dmk
as_dom_exp_ind				= space(1)
ls_exp_country				= space(3)

as_header_info 			= Trim(as_Header_info)
as_weight_overide_info 	= Trim( as_weight_overide_info)
as_weight_cost_info		= Trim(as_weight_cost_info)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)

// Call a Netwise external function to get the customer information
//li_rtn = orpo32br_GetHeaderInfo(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_header_info, &
//					ls_header_out, &
//					ac_action_ind, &
//					as_weight_overide_info, &
//					ls_weight_overide_out, &
//					as_weight_cost_info, &
//					ls_weight_cost_out, &
//					ac_addition_in, &
//					lc_addition_out, &
//					as_age_type_code, &
//					ls_age_type_code_out, &
//					as_pss_ind, &
//					ls_pss_ind_out, &
//					as_load_instr, &
//					ls_loading_instr_out, &
//					as_protect,&
//					ls_protect_out, &
//					as_pa_option, &
//					ls_shortage_out, & 
//					as_gtl_code, &
//					ls_gtl_code, &
//					as_dept_code, &
//					ls_dept_code, &
//					as_dom_exp_ind, &
//					as_exp_country, &
//					ls_exp_country, &
//					ii_orp003_commhandle)

as_weight_overide_info	=	ls_weight_overide_out
as_weight_cost_info		=	ls_weight_cost_out
as_header_info				=	ls_header_out
ac_addition_in				=  lc_addition_out
as_age_type_code 			= 	ls_age_type_code_out
as_pss_ind					=  ls_pss_ind_out
as_load_instr				= 	ls_loading_instr_out
as_protect					=	ls_protect_out
as_shortage_out 			= 	ls_shortage_out
as_gtl_code			   	=  ls_gtl_code
as_dept_code				=  ls_dept_code
as_exp_country				= 	ls_exp_country

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
return li_rtn
end function

public function integer wbckcompleted (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country);Int 		li_rtn
String	ls_header_out, &
			ls_weight_overide_out, &
			ls_weight_cost_out, &
			ls_age_type_code_out, &
			ls_pss_ind_out, &
			ls_loading_instr_out, &
			ls_protect_out, &
			ls_shortage_out

char     lc_addition_out

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_gtl_code, &
			ls_dept_code, &
			ls_exp_country

ls_header_out 				= SPACE(30000)
ls_weight_overide_out 	= SPACE(30000)
ls_weight_cost_out		= SPACE(30000)	
ls_shortage_out			= SPACE(10000)
ls_header_out 				= Fill(Char(0),30000)
ls_weight_overide_out 	= Fill(char(0),30000)
ls_weight_cost_out		= FILL(char(0),30000)
ls_shortage_out			= Fill(char(0),10000)
lc_addition_out			= SPACE(1)
ls_age_type_code_out		= SPACE(1)
ls_pss_ind_out				= SPACE(1)
ls_loading_instr_out		= space(1)
ls_protect_out				= space(1)
//dld
ls_gtl_code 				= space(1)
ls_dept_code			 	= space(10)
//dmk
as_dom_exp_ind				= space(1)
ls_exp_country				= space(3)

as_header_info 			= Trim(as_Header_info)
as_weight_overide_info 	= Trim( as_weight_overide_info)
as_weight_cost_info		= Trim(as_weight_cost_info)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)

// Call a Netwise external function to get the customer information
//li_rtn = orpo32br_GetHeaderInfo(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_header_info, &
//					ls_header_out, &
//					ac_action_ind, &
//					as_weight_overide_info, &
//					ls_weight_overide_out, &
//					as_weight_cost_info, &
//					ls_weight_cost_out, &
//					ac_addition_in, &
//					lc_addition_out, &
//					as_age_type_code, &
//					ls_age_type_code_out, &
//					as_pss_ind, &
//					ls_pss_ind_out, &
//					as_load_instr, &
//					ls_loading_instr_out, &
//					as_protect,&
//					ls_protect_out, &
//					as_pa_option, &
//					ls_shortage_out, & 
//					as_gtl_code, &
//					ls_gtl_code, &
//					as_dept_code, &
//					ls_dept_code, & 
//					as_dom_exp_ind, &
//					as_exp_country, &
//					ls_exp_country, &
//					ii_orp003_commhandle)
//
as_weight_overide_info	=	ls_weight_overide_out
as_weight_cost_info		=	ls_weight_cost_out
as_header_info				=	ls_header_out
ac_addition_in				=  lc_addition_out
as_age_type_code 			= 	ls_age_type_code_out
as_pss_ind					=  ls_pss_ind_out
as_load_instr				= 	ls_loading_instr_out
as_protect					=	ls_protect_out
as_shortage_out 			= 	ls_shortage_out
as_gtl_code			   	=  ls_gtl_code
as_dept_code				=  ls_dept_code
as_exp_country				=	ls_exp_country

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
return li_rtn
end function

public function integer wborp203ckcompleted (ref s_error astr_error_info, ref string as_header_info, character ac_action_ind, ref string as_weight_overide_info, ref string as_weight_cost_info, ref character ac_addition_in, ref string as_age_type_code, ref string as_pss_ind, ref string as_load_instr, ref string as_protect, string as_pa_option, ref string as_shortage_out, ref string as_gtl_code, ref string as_dept_code, ref string as_dom_exp_ind, ref string as_exp_country);Int 		li_rtn
String	ls_header_out, &
			ls_weight_overide_out, &
			ls_weight_cost_out, &
			ls_age_type_code_out, &
			ls_pss_ind_out, &
			ls_loading_instr_out, &
			ls_protect_out, &
			ls_shortage_out

char     lc_addition_out

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_gtl_code, &
			ls_dept_code, &
			ls_exp_country

ls_header_out 				= SPACE(30000)
ls_weight_overide_out 	= SPACE(30000)
ls_weight_cost_out		= SPACE(30000)	
ls_shortage_out			= SPACE(10000)
ls_header_out 				= Fill(Char(0),30000)
ls_weight_overide_out 	= Fill(char(0),30000)
ls_weight_cost_out		= FILL(char(0),30000)
ls_shortage_out			= Fill(char(0),10000)
lc_addition_out			= SPACE(1)
ls_age_type_code_out		= SPACE(1)
ls_pss_ind_out				= SPACE(1)
ls_loading_instr_out		= space(1)
ls_protect_out				= space(1)
//dld
ls_gtl_code 				= space(1)
ls_dept_code 				= space(10)
//dmk
as_dom_exp_ind				= space(1)
ls_exp_country				= space(3)


as_header_info 			= Trim(as_Header_info)
as_weight_overide_info 	= Trim( as_weight_overide_info)
as_weight_cost_info		= Trim(as_weight_cost_info)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)

// Call a Netwise external function to get the customer information
//li_rtn = orpo32br_GetHeaderInfo(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_header_info, &
//					ls_header_out, &
//					ac_action_ind, &
//					as_weight_overide_info, &
//					ls_weight_overide_out, &
//					as_weight_cost_info, &
//					ls_weight_cost_out, &
//					ac_addition_in, &
//					lc_addition_out, &
//					as_age_type_code, &
//					ls_age_type_code_out, &
//					as_pss_ind, &
//					ls_pss_ind_out, &
//					as_load_instr, &
//					ls_loading_instr_out, &
//					as_protect,&
//					ls_protect_out, &
//					as_pa_option, &
//					ls_shortage_out, & 
//					as_gtl_code, &
//					ls_gtl_code, &
//					as_dept_code, &
//					ls_dept_code, &
//					as_dom_exp_ind, &
//					as_exp_country, &
//					ls_exp_country, &
//					ii_orp003_commhandle)

as_weight_overide_info	=	ls_weight_overide_out
as_weight_cost_info		=	ls_weight_cost_out
as_header_info				=	ls_header_out
ac_addition_in				=  lc_addition_out
as_age_type_code 			= 	ls_age_type_code_out
as_pss_ind					=  ls_pss_ind_out
as_load_instr				= 	ls_loading_instr_out
as_protect					=	ls_protect_out
as_shortage_out 			= 	ls_shortage_out
as_gtl_code			   	=  ls_gtl_code
as_dept_code				=  ls_dept_code

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
return li_rtn
end function

public function integer nf_orpo08cr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_output_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message
			
SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "orpo08cr_inq_upd_customer_markup"
astr_error_info.se_message = space(71)

as_output_string = SPACE(10000)
as_output_string = Fill(Char(0), 10000)
as_input_string = Trim( as_Input_String)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

//li_rtn = orpo08cr_inq_upd_customer_markup( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//	 as_header_string, &
//    as_input_string, &
//	 as_output_string, &
//    ii_orp003_commhandle)
//	 

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo41ar (ref s_error astr_error_info, string as_userid, ref string as_sales_person_info_string);Integer	li_rtn
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

as_sales_person_info_string = SPACE(12)
as_sales_person_info_string = Fill(Char(0), 12)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
// Call a Netwise external function to get the required information
//li_rtn = orpo41br_get_sale_person_default( &
//														ls_app_name, &
//														ls_window_name, &
//														ls_function_name, &
//														ls_event_name, &
//														ls_procedure_name, &
//														ls_user_id, &
//														ls_return_code, &
//														ls_message, &
//														as_userid, &	
//														as_sales_person_info_string, &
//														ii_orp003_commhandle)
///////////////////////////////////////////////////////////////
nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn

end function

public function integer nf_orpo10cr_product_to_global_upd (ref s_error astr_error_info, string as_input_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message
			
SetPointer(HourGlass!)
//
astr_error_info.se_procedure_name = "orpo10cr_product_to_global_upd"
astr_error_info.se_message = space(71)



as_input_string = Trim( as_Input_String)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

//li_rtn = orpo10cr_product_to_global_upd( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//    as_input_string, &
//    ii_orp003_commhandle)
	 

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
//
Return li_rtn
end function

public function integer nf_orpo09cr_product_to_global_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_detail_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message, &
			ls_outputstring, &
			ls_stringind, &
			ls_output
			
u_string_functions	lu_string

SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "orpo09cr_product_to_global_inq"
astr_error_info.se_message = space(71)

as_output_string = SPACE(10000)
as_output_string = Fill(Char(0), 10000)
as_input_string = Trim( as_Input_String)
ls_outputstring = SPACE(10000)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

//li_rtn = orpo09cr_product_to_global_inq( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//    as_input_string, &
//	 ls_outputstring, &
//    ii_orp003_commhandle)


ls_StringInd = Mid(ls_OutputString, 2, 1)
ls_OutputString = Mid(ls_OutputString, 3)

Do While Len(Trim(ls_OutputString)) > 0
		ls_output = lu_string.nf_GetToken(ls_OutputString, '~f')
		
		Choose Case ls_StringInd
			Case 'P'
				as_output_string += ls_output
			Case 'D'
				as_Detail_String += ls_output
			
		End Choose

		ls_StringInd = Left(ls_OutputString, 1)
		ls_OutputString = Mid(ls_OutputString, 2)
	Loop


nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo38ar (ref s_error astr_error_info, character ac_process_option, character ac_availability_date[10], string as_page_descr_in, ref string as_plant_data, ref string as_detail_data, ref string as_file_descr_out, string as_customer_id, string as_date_type_ind, string as_detail_string, ref string as_prod_data);INTEGER 	li_rtn, &
			li_other_info
			
Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number


STRING	ls_plants_out, &
			ls_detail_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_start_RPC, &
			ls_end_RPC, &
			ls_other_info, &
			ls_task_number, &
			ls_prod_data_out, &
			ls_detail_string, &
			ls_first_time
			
TIME		lt_startTime, &
			lt_endTime, &
			lt_start_RPC, &
			lt_end_RPC




ls_message = SPACE(71)
ls_plants_out = SPACE(75)
ls_detail_out = SPACE(12000)
as_file_descr_out = SPACE(10000)
//ls_prod_data_out = SPACE(20000)

ls_plants_out = Fill( Char(0), 75)
ls_detail_out = Fill( char(0), 12000)
as_file_descr_out = Fill( char(0), 10000)
ls_message = Fill( char(0), 71)

SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

//as_plant_data = ""
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_prod_data_out = SPACE(20000)
ls_prod_data_out = Fill(Char(0), 20000)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_startTime = Now()
ls_first_time = 'Y'
										
// Call a Netwise external function to get the required information
do
//li_rtn = orpo38br_pa_summary(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ac_process_option, &
//					ac_availability_date, &
//					as_page_descr_in, &
//					as_plant_data, &
//					as_detail_data, &
//					ls_plants_out, &
//					ls_detail_out, &
//					as_file_descr_out, &
//					as_customer_id, &
//					as_date_type_ind, &
//					as_detail_string, &
//					ls_prod_data_out, &
//					ld_task_number, &
//   			   ld_last_record_number, &
// 				   ld_max_record_number, &
//					ii_orp003_commhandle)
	if ls_first_time = 'Y' then
		as_plant_data = ""
		as_detail_data = ""
		ls_first_time = 'N'
	end if
	as_prod_data += ls_prod_data_out
//	as_detail_string += ls_detail_string
	as_plant_data += ls_plants_out
	as_detail_data += ls_detail_out
	Loop While ld_last_record_number <> ld_max_record_number

lt_endTime = Now()
ls_procedure_name = 'nf_orp38ar'
lt_start_RPC = Time(ls_window_name)
lt_end_RPC = Time(ls_function_name)
ls_other_info = "Products/Plants: " + ls_event_name
ls_task_number = ls_app_name

//benchmark call
nf_write_benchmark2(lt_startTime, lt_endtime, ls_procedure_name, &
					lt_start_RPC, lt_end_RPC, ls_other_info, ls_task_number)

//as_plant_data = TRIM(ls_plants_out)
//as_detail_data = TRIM(ls_detail_out)
TRIM(as_plant_data)
TRIM(as_detail_data)
TRIM(as_prod_data)
TRIM(as_detail_string)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

return li_rtn
end function

public function integer nf_orpo44ar (ref s_error astr_error_info, string as_input_info_in, string as_main_info_in, character ac_ind_process_option, ref string as_main_info_out, ref string as_so_info_out, ref string as_age_info_out, ref string as_production_dates, double ad_task_number_in, double ad_page_number_in, ref double ad_task_number_out, ref double ad_page_number_out, string as_window_ind);Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number

Int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_detail_info_out, &
			ls_production_dates, &
			ls_main_info_out, &
			ls_output_string, &
			ls_string_ind, &
			ls_output

u_string_functions	lu_string			
			
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ad_task_number_out = 0
ad_page_number_out = 0

as_main_info_out = ''
as_so_info_out = ''
as_age_info_out = ''

astr_error_info.se_Message = Space(71)
astr_error_info.se_procedure_name = 'orpo44br_pa_inq_inquire'

gw_NetWise_Frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

do

	ls_output_string = SPACE(60000)
	ls_output_string = FILL(CHAR(0), 60000)

//	li_rtn =  orpo44br_pa_inq_inquire(ls_app_name, &
//			ls_window_name, &
//			ls_function_name, &
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id,&
//			ls_return_code,&
//			ls_message, &
//			as_input_info_in, &
//			as_main_info_in, &
//			ac_ind_process_option, &
//			as_window_ind, &
//			ls_output_string, &
//			ad_task_number_in, &
//			ad_page_number_in, &
//			ad_task_number_out, &
//			ad_page_number_out, &
//			ld_task_number, &
//			ld_last_record_number, &
//    		ld_max_record_number, &
//			ii_orp003_commhandle)
	
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
		Choose Case ls_string_ind
			Case 'S'
				as_so_info_out += ls_output
			Case 'M'
				as_main_info_out += ls_output
			Case 'A'
				as_age_info_out += ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
			
									
Loop while ld_last_record_number <> ld_max_record_number and li_rtn > -1

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo11cr_edi_trans_by_cust_inq (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_detail_string
			
Double	ld_task_number, &
			ld_last_number, &
			ld_max_number


SetPointer(HourGlass!)
gw_NetWise_Frame.SetMicroHelp("Wait...PC communicating with Mainframe")

as_output_string = ""

ld_task_number = 0
ld_last_number = 0
ld_max_number = 0
as_header_string_out = SPACE(1000)
as_header_string_out = Fill(Char(0), 1000)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message)

Do
	ls_detail_string = SPACE(20000)
	ls_detail_string = Fill(Char(0), 20000)	
//	li_rtn = orpo11cr_edi_trans_by_cust_inq(ls_app_name, &
//									ls_window_name, &
//									ls_function_name, &
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id,&
//									ls_return_code,&
//									ls_message,&
//									as_header_string_in, &
//									as_header_string_out, &
//									ls_detail_string, &
//									ld_task_number, &
//									ld_last_number, &
//									ld_max_number, &
//									ii_orp003_commhandle) 
	as_output_string += ls_detail_string
Loop While ld_last_number <> ld_max_number

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)

Return li_rtn
end function

public function integer nf_orpo33ar (ref s_error astr_error_info, string as_order_id, ref string as_detail_info, ref string as_weight_str, ref string as_res_reduce_in, ref string as_pa_resolve_out, string as_action_ind, ref string as_production_dates, ref string as_dept_codes, string as_dup_products_ind);Char lc_Region
DataStore lds_Tmp 
int li_rtn

STRING	ls_detail_out, &
			ls_weight_out,&
			ls_MicroHelp,&
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_order_id,&
			ls_QueueString,&
			ls_QueueName,&
			ls_Age, &
			ls_production_dates
			
Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number

Long		ll_RowCount,&
			ll_LoopCount,&
			ll_OleRtn

Decimal	ld_Price,&
			ld_Quantity
			
ls_MicroHelp = "Wait...PC communicating with Mainframe"
gw_NetWise_Frame.SetMicroHelp( ls_MicroHelp)
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_production_dates = space(3000)
ls_production_dates = fill(char(0),3000)
ls_Detail_Out = SPACE(50000)
ls_Detail_Out = Fill(Char(0),50000)
ls_Weight_Out = SPACE(300)
ls_Weight_Out = Fill(Char(0),300)
ls_order_id   = Left(as_order_id,5)
as_pa_resolve_out = SPACE(4455)
as_pa_resolve_out = Fill(Char(0),4455)
as_dept_codes = Space(200)
as_dept_codes = fill(char(0),200)

as_Order_ID = Trim( As_Order_Id)
as_detail_info = Trim( as_detail_info)
as_weight_str = Trim(as_weight_str)
as_res_reduce_in = Trim( as_res_reduce_in)

lc_region = Right(ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Netwise Server Info", "ServerSuffix", "t"), 1)
Choose Case lower(lc_region)
	Case 'p'
		ls_QueueName = "ProfitQueue"
	Case 'z'
				ls_QueueName = "ProfitQueueQA"
	Case Else
		ls_QueueName = "ProfitQueueTest"
End Choose

//lds_Tmp = Create DataStore
//lds_Tmp.DataObject = 'd_sales_Detail_Part1'
//ll_RowCount  = lds_Tmp.ImportString(as_Detail_Info)
//ls_QueueString = ''
//For ll_LoopCount = 1 to ll_RowCount
//	   ld_Price = lds_tmp.GetItemDecimal(ll_LoopCount,"sales_price")
//		ld_Quantity = lds_Tmp.GetItemDecimal(ll_LoopCount,"ordered_units") 
//		ls_Age = lds_Tmp.GetItemString(ll_LoopCount,"ordered_age")
//		ls_QueueString += lds_Tmp.GetItemString(ll_LoopCount,"product_code")+"~t"+String(ld_Quantity)+"~t"+String(ld_Price)+"~t"+ls_Age+"~r~n"
//End for
if as_action_ind = 'U' then
	ls_Queuestring = "orpo33b" + "~v" + as_order_id + "~v" + as_weight_str + "~v" + as_res_reduce_in + &
								 "~v" + as_detail_info
	IF Len(Trim(ls_QueueString)) > 0 Then
		IF Not IsValid(iu_oleprophet) Then 
			iu_oleprophet = Create u_Ole_Com
			ll_OleRtn = iu_OleProphet.ConnectToNewObject("ProphetQueue.ProphetSender.1")
			IF ll_OleRtn = 0 Then
				iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
			Else
				Destroy iu_oleprophet
			End if
		Else
			iu_OleProphet.Send(ls_QueueName,"Excel Data for Prophet",ls_QueueString)
		End if
	End if
End If 

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)

//do
	

	// Call a Netwise external function to get the required information
//	li_rtn = orpo33br_get_order_detail(	ls_app_name, &
//													ls_window_name,&
//													ls_function_name,&
//													ls_event_name, &
//													ls_procedure_name, &
//													ls_user_id, &
//													ls_return_code, &
//													ls_message, &
//													ls_order_id,&
//													as_detail_info, &
//													ls_Detail_Out, &
//													as_weight_str, &
//													ls_Weight_Out, &
//													as_res_reduce_in, &
//													as_pa_resolve_out, &
//													as_action_ind, &
//													ls_production_dates, &
//													ld_task_number, &
//													ld_last_record_number, &
//													ld_max_record_number, &
//													as_dept_codes, &
//													as_dup_products_ind, &
//													ii_orp003_commhandle)
				
				
	as_production_dates = ls_production_dates
	as_detail_info = 	ls_Detail_Out
	as_weight_str =	ls_Weight_Out

//Loop while ld_last_record_number <> ld_max_record_number and li_rtn > -1

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
					
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)


return li_rtn
end function

public function integer nf_orpo12cr_edi_trans_by_cust_upd (ref s_error astr_error_info, string as_header_string, string as_detail_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message
			
SetPointer(HourGlass!)
//
astr_error_info.se_procedure_name = "orpo10cr_product_to_global_upd"
astr_error_info.se_message = space(71)

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

//li_rtn = orpo12cr_edi_trans_by_cust_upd( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//	 as_header_string, &
//    as_detail_string, &
//    ii_orp003_commhandle)
	 

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_orp003_commhandle)
//
Return li_rtn
end function

event constructor;call super::constructor;STRING	ls_MicroHelp

ls_MicroHelp = gw_NetWise_Frame.wf_GetMicroHelp()

gw_NetWise_Frame.SetMicroHelp( "Opening MainFrame Connections")

ii_orp003_commhandle = SQLCA.nf_GetCommHandle("orp003")

If ii_orp003_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_NetWise_Frame.SetMicroHelp(ls_MicroHelp)

end event

on u_orp003.create
call super::create
end on

on u_orp003.destroy
call super::destroy
end on

