HA$PBExportHeader$u_edi111.sru
forward
global type u_edi111 from u_netwise_transaction
end type
end forward

global type u_edi111 from u_netwise_transaction
end type
global u_edi111 u_edi111

type prototypes
// PowerBuilder Script File: c:\ibp\src\edi111.pbf
// Target Environment:  Netwise Application/Integrator
// Script File Creation Time: Wed Jun 05 10:49:45 1996
// Source Interface File: j:\pb\test\src\edi111.ntf
//
// Script File Created By:
//
//     Netwise Application/Integrator WORKBENCH v.2.1
//
//     Netwise, Inc.
//     2477 55th Street
//     Boulder, Colorado 80301
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: edie00ar_inq_cross_ref
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie00ar_inq_cross_ref( &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie00ar_inq_cross_ref;Ansi"


//
// Declaration for procedure: edie01ar_upd_cross_ref
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie01ar_upd_cross_ref( &
    ref s_error s_error_info, &
    string update_string_in, &
    ref string update_string_out, &
    int CommHnd &
) library "edi111.dll" alias for "edie01ar_upd_cross_ref;Ansi"


//
// Declaration for procedure: edie02ar_inq_product_cross_ref
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie02ar_inq_product_cross_ref( &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie02ar_inq_product_cross_ref;Ansi"


//
// Declaration for procedure: edie03ar_upd_product_cross_ref
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie03ar_upd_product_cross_ref( &
    ref s_error s_error_info, &
    string update_string_in, &
    ref string update_string_out, &
    int CommHnd &
) library "edi111.dll" alias for "edie03ar_upd_product_cross_ref;Ansi"


//
// Declaration for procedure: edie04ar_inq_asn_control
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie04ar_inq_asn_control( &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie04ar_inq_asn_control;Ansi"


//
// Declaration for procedure: edie05ar_upd_asn_control
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie05ar_upd_asn_control( &
    ref s_error s_error_info, &
    string update_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie05ar_upd_asn_control;Ansi"


//
// Declaration for procedure: edie06ar_inq_incoming_po
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie06ar_inq_incoming_po( &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string customer_id, &
    ref string output_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie06ar_inq_incoming_po;Ansi"


//
// Declaration for procedure: edie07ar_inq_roe
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie07ar_inq_roe( &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string customer_id, &
    ref string output_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie07ar_inq_roe;Ansi"


//
// Declaration for procedure: edie08ar_inq_doc_status
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int edie08ar_inq_doc_status( &
    ref s_error s_error_info, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    string inter_status, &
    ref string output_string, &
    int CommHnd &
) library "edi111.dll" alias for "edie08ar_inq_doc_status;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the Netwise 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "edi111.dll"
function int WBedi111CkCompleted( int CommHnd ) &
    library "edi111.dll"
//
// ***********************************************************


end prototypes

type variables
integer	ii_edi111_commhandle


end variables

forward prototypes
public function integer nf_upd_shipto_xref (ref s_error astr_error_info, string as_update_string_in, ref string as_update_string_out, integer ai_commhnd)
public function integer nf_upd_product_xref (ref s_error astr_error_info, ref string as_update_string_in, ref string as_update_string_out, integer ai_commhnd)
public function boolean nf_inq_product_xref (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_output_string, integer ai_commhnd)
public function boolean nf_inq_shipto_xref (ref s_error astr_error_info, ref double adb_tasknumber, ref double adb_last_rec_number, ref double adb_max_rec_number, ref string as_output_string, integer ai_commhnd)
public function boolean nf_inq_asn_control (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_output_string, integer ai_commhnd)
public function boolean nf_inq_incoming_po (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_customerid, ref string as_output_string, integer ai_commhnd)
public function boolean nf_upd_asn_control (ref s_error astr_error_info, ref string as_update_string, integer ai_commhnd)
public function boolean nf_inq_roe (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_customerid, ref string as_output_string, integer ai_commhnd)
public function boolean nf_inq_doc_status (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_inter_status, ref string as_output_string, integer ai_commhnd)
end prototypes

public function integer nf_upd_shipto_xref (ref s_error astr_error_info, string as_update_string_in, ref string as_update_string_out, integer ai_commhnd);Int li_rtn

li_rtn = edie01ar_upd_cross_ref(astr_error_info, as_update_string_in, &
									as_update_string_out, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)
Return li_rtn
end function

public function integer nf_upd_product_xref (ref s_error astr_error_info, ref string as_update_string_in, ref string as_update_string_out, integer ai_commhnd);Int li_rtn

li_rtn = edie03ar_upd_product_cross_ref(astr_error_info, as_update_string_in, &
									as_update_string_out, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)
Return li_rtn
end function

public function boolean nf_inq_product_xref (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_output_string, integer ai_commhnd);Int li_rtn

li_rtn = edie02ar_inq_product_cross_ref(astr_error_info, &
				adbl_task_number, adbl_last_rec_number, adbl_max_rec_number, &
									as_output_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)


end function

public function boolean nf_inq_shipto_xref (ref s_error astr_error_info, ref double adb_tasknumber, ref double adb_last_rec_number, ref double adb_max_rec_number, ref string as_output_string, integer ai_commhnd);Int li_rtn

li_rtn = edie00ar_inq_cross_ref(astr_error_info, adb_tasknumber, &
									adb_last_rec_number, adb_max_rec_number, &
									as_output_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)

end function

public function boolean nf_inq_asn_control (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_output_string, integer ai_commhnd);Int li_rtn

li_rtn = edie04ar_inq_asn_control(astr_error_info, &
				adbl_task_number, adbl_last_rec_number, adbl_max_rec_number, &
									as_output_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)


end function

public function boolean nf_inq_incoming_po (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_customerid, ref string as_output_string, integer ai_commhnd);Int li_rtn

li_rtn = edie06ar_inq_incoming_po(astr_error_info, &
				adbl_task_number, adbl_last_rec_number, adbl_max_rec_number, &
					as_customerid, as_output_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)


end function

public function boolean nf_upd_asn_control (ref s_error astr_error_info, ref string as_update_string, integer ai_commhnd);Int li_rtn

li_rtn = edie05ar_upd_asn_control(astr_error_info, &
									as_update_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
RETURN nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)

end function

public function boolean nf_inq_roe (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_customerid, ref string as_output_string, integer ai_commhnd);Int li_rtn

li_rtn = edie07ar_inq_roe(astr_error_info, adbl_task_number, &
					adbl_last_rec_number, adbl_max_rec_number, &
					as_customerid, as_output_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)


end function

public function boolean nf_inq_doc_status (ref s_error astr_error_info, ref double adbl_task_number, ref double adbl_last_rec_number, ref double adbl_max_rec_number, ref string as_inter_status, ref string as_output_string, integer ai_commhnd);Int li_rtn

li_rtn = edie08ar_inq_doc_status(astr_error_info, adbl_task_number, &
					adbl_last_rec_number, adbl_max_rec_number, &
					as_inter_status, as_output_string, ii_edi111_commhandle) 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return nf_display_message(li_rtn, astr_error_info, ii_edi111_commhandle)


end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows
ii_edi111_commhandle = SQLCA.nf_getcommhandle("edi111")
If ii_edi111_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end event

on u_edi111.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_edi111.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

