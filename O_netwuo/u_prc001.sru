HA$PBExportHeader$u_prc001.sru
forward
global type u_prc001 from u_netwise_transaction
end type
end forward

global type u_prc001 from u_netwise_transaction
end type
global u_prc001 u_prc001

type prototypes
private:
// PowerBuilder Script File: j:\pb\test\src32\prc001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Fri Jun 27 11:39:54 1997
// Source Interface File: j:\pb\test\src32\prc001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: prcr01ar_inq_sold_postion
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int prcr01ar_inq_sold_postion( &
    ref s_error s_error_info, &
    string Product_code, &
    ref string Out_PutString, &
    int CommHnd &
) library "prc001.dll" alias for "prcr01ar_inq_sold_postion;Ansi"


//
// Declaration for procedure: prcr02ar_inq_price_range
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int prcr02ar_inq_price_range( &
    ref s_error s_error_info, &
    string product_code, &
    ref string date_string, &
    ref string value_string, &
    int CommHnd &
) library "prc001.dll" alias for "prcr02ar_inq_price_range;Ansi"


//
// Declaration for procedure: prcr03ar_inq_price_drill_dwn
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int prcr03ar_inq_price_drill_dwn( &
    ref s_error s_error_info, &
    string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "prc001.dll" alias for "prcr03ar_inq_price_drill_dwn;Ansi"


//
// Declaration for procedure: prcr04ar_inq_paswkav
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int prcr04ar_inq_paswkav( &
    ref s_error s_error_info, &
    string division_code, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref double task_number, &
    ref string date_string, &
    ref string output_string, &
    int CommHnd &
) library "prc001.dll" alias for "prcr04ar_inq_paswkav;Ansi"


//
// Declaration for procedure: prcr05ar_inq_invt_by_age
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int prcr05ar_inq_invt_by_age( &
    ref s_error s_error_info, &
    string product_code, &
    ref string age_string, &
    int CommHnd &
) library "prc001.dll" alias for "prcr05ar_inq_invt_by_age;Ansi"


//
// Declaration for procedure: prcr06ar_inq_forward_proj
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int prcr06ar_inq_forward_proj( &
    ref s_error s_error_info, &
    string product_code, &
    ref string output_string, &
    int CommHnd &
) library "prc001.dll" alias for "prcr06ar_inq_forward_proj;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "prc001.dll"
function int WBprc001CkCompleted( int CommHnd ) &
    library "prc001.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
Int ii_commhandle
end variables

forward prototypes
public function boolean nf_prcr01ar_inq_sold_position (ref s_error astr_error_info, string as_product_code, ref string as_output)
public function boolean nf_prcr02ar_inq_price_range (ref s_error as_error_info, string as_product_code, ref string as_dates, ref string as_values)
public function boolean nf_prcr03ar_inq_prc_drill_down (ref s_error astr_error_info, string as_product_code, ref string as_output)
public function boolean nf_prcr04ar_inq_tpaswkav (ref s_error astr_error_info, string as_division_code, ref string as_dates, ref datawindow ad_output)
public function boolean nf_prcr06ar_inq_forward_proj (ref s_error astr_error_info, string as_product_code, ref string as_output)
end prototypes

public function boolean nf_prcr01ar_inq_sold_position (ref s_error astr_error_info, string as_product_code, ref string as_output);Int	li_ret


as_output = Space(500)
astr_error_info.se_message = Space(72)
astr_error_info.se_procedure_name = 'prcr01ar_inq_sold_postion'
li_ret = this.prcr01ar_inq_sold_postion(astr_error_info, &
													as_product_code, &
													as_output, &
													ii_commhandle)

Return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

public function boolean nf_prcr02ar_inq_price_range (ref s_error as_error_info, string as_product_code, ref string as_dates, ref string as_values);Int	li_ret


as_dates = Space(2000)
as_values = Space(20000)

as_error_info.se_message = Space(72)
as_error_info.se_procedure_name = 'prcr02ar_inq_price_range'
li_ret = this.prcr02ar_inq_price_range(as_error_info, &
													as_product_code, &
													as_dates, &
													as_values, &
													ii_commhandle)

Return nf_display_message(li_ret, as_error_info, ii_commhandle)
end function

public function boolean nf_prcr03ar_inq_prc_drill_down (ref s_error astr_error_info, string as_product_code, ref string as_output);Double	ld_TaskNumber = 0, &
			ld_LastRecordNumber = 0, &
			ld_MaxRecordNumber = 0

Int		li_ret

String	ls_output

as_output = ''
astr_error_info.se_procedure_name = 'prcr03ar_inq_price_drill_dwn'

Do
	ls_output = Space(59977)
	astr_error_info.se_message = Space(72)
	li_ret = this.prcr03ar_inq_price_drill_dwn( &
													astr_error_info, &
													as_product_code, &
													ld_TaskNumber, &
													ld_LastRecordNumber, &
													ld_MaxRecordNumber, &
													ls_output, &
													ii_commhandle)
	If Not This.nf_display_message(li_ret, astr_error_info, ii_commhandle) Then Return False
	as_output += ls_output
Loop while ld_LastRecordNumber <> ld_MaxRecordNumber

Return True


end function

public function boolean nf_prcr04ar_inq_tpaswkav (ref s_error astr_error_info, string as_division_code, ref string as_dates, ref datawindow ad_output);Boolean	lb_first_time

Double	ld_TaskNumber, &
			ld_LastRecordNumber, &
			ld_MaxRecordNumber
			
Int		li_ret

String	ls_output, &
			ls_dates


ld_TaskNumber = 0
ld_LastRecordNumber = 0
ld_MaxRecordNumber = 0

ls_dates = Space(56)
lb_first_time = True

ad_output.Reset()

Do
	ls_output = Space(59641)
	astr_error_info.se_message = Space(72)
	astr_error_info.se_procedure_name = 'prcr04ar_inq_paswkav'
	
	li_ret = prcr04ar_inq_paswkav(astr_error_info, &
										as_division_code, &
										 ld_LastRecordNumber, &
										 ld_MaxRecordNumber, &
										 ld_TaskNumber, &
										 ls_dates, &
										 ls_output, &
										 ii_commhandle)
	If Not This.nf_display_message(li_ret, astr_error_info, ii_commhandle) Then Return False
	If lb_first_time Then
		as_dates = ls_dates
		lb_first_time = False
	End if
	ad_output.ImportString(ls_output)
Loop while ld_LastRecordNumber <> ld_MaxRecordNumber


Return True

end function

public function boolean nf_prcr06ar_inq_forward_proj (ref s_error astr_error_info, string as_product_code, ref string as_output);Int	li_ret


as_output = Space(10000)

astr_error_info.se_message = Space(72)
astr_error_info.se_procedure_name = 'prcr06ar_inq_forward_proj'

li_ret = this.prcr06ar_inq_forward_proj(astr_error_info, &
													as_product_code, &
													as_output, &											 
													ii_commhandle)

Return nf_display_message(li_ret, astr_error_info, ii_commhandle)
end function

on u_prc001.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_prc001.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;ii_commhandle = SQLCA.nf_GetCommHandle("prc001")

If ii_commhandle = 0 Then
	Message.ReturnValue = -1
End if

end event

