HA$PBExportHeader$u_paf001.sru
forward
global type u_paf001 from u_netwise_transaction
end type
end forward

type s_super_product from structure
    character se_super_product_id[10]
    character se_super_product_desc[30]
end type

type s_sku_product from structure
    character se_sku_product_id[10]
    character se_sku_product_desc[30]
    character se_prod_age_modeled
end type

type s_supers_and_skus from structure
    character se_super_product_id[10]
    character se_super_product_desc[30]
    character se_sku_product_id[10]
    character se_prod_age_modeled
end type

global type u_paf001 from u_netwise_transaction
end type
global u_paf001 u_paf001

type prototypes
// PowerBuilder Script File: J:\PB\TEST\SRC\PAF001.PBf
// Target Environment:  Netwise Application/Integrator
// Script File Creation Time: Tue Feb 06 16:48:34 1996
// Source Interface File: j:\pb\test\src\paf001.ntf
//
// Script File Created By:
//
//     Netwise Application/Integrator WORKBENCH v.2.1
//
//     Netwise, Inc.
//     2477 55th Street
//     Boulder, Colorado 80301
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: paff01ar_paf_parameter
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff01ar_paf_parameter( &
    ref s_error s_error_info, &
    char process_option, &
    char pa_sortid[19], &
    char control_key[15], &
    ref string control_data, &
    int CommHnd &
) library "paf001.dll" alias for "paff01ar_paf_parameter;Ansi"


//
// Declaration for procedure: paff02ar_get_super_product_info
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff02ar_get_super_product_info( &
    ref s_error s_error_info, &
    ref int super_product_rec_occurs, &
    ref s_super_product s_super_product_info[50], &
    ref char refetch_super_product_id[10], &
    int CommHnd &
) library "paf001.dll" alias for "paff02ar_get_super_product_info;Ansi"


//
// Declaration for procedure: paff03ar_get_sku_product_info
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff03ar_get_sku_product_info( &
    ref s_error s_error_info, &
    char req_super_product_id[10], &
    ref int sku_product_rec_occurs, &
    ref s_sku_product s_sku_product_info[50], &
    ref char refetch_sku_product_id[10], &
    int CommHnd &
) library "paf001.dll" alias for "paff03ar_get_sku_product_info;Ansi"


//
// Declaration for procedure: paff04ar_upd_supers_and_skus
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff04ar_upd_supers_and_skus( &
    ref s_error s_error_info, &
    ref int upd_supers_skus_rec_occurs, &
    ref s_supers_and_skus s_supers_and_skus_info[50], &
    char update_indicator, &
    int CommHnd &
) library "paf001.dll" alias for "paff04ar_upd_supers_and_skus;Ansi"


//
// Declaration for procedure: paff05ar_validate_sku_product
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff05ar_validate_sku_product( &
    ref s_error s_error_info, &
    char req_sku_product_id[10], &
    ref s_sku_product s_sku_product_info, &
    int CommHnd &
) library "paf001.dll" alias for "paff05ar_validate_sku_product;Ansi"


//
// Declaration for procedure: paff06ar_inq_plant_capacity_info
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff06ar_inq_plant_capacity_info( &
    ref s_error s_error_info, &
    char req_plant[3], &
    char req_start_date[10], &
    ref string s_capacity_header_string, &
    ref string s_capacity_detail_string, &
    int CommHnd &
) library "paf001.dll" alias for "paff06ar_inq_plant_capacity_info;Ansi"


//
// Declaration for procedure: paff07ar_upd_plant_capacity_info
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff07ar_upd_plant_capacity_info( &
    ref s_error s_error_info, &
    char req_plant[3], &
    char req_start_date[10], &
    ref string s_capacity_header_string, &
    ref string s_capacity_detail_string, &
    int CommHnd &
) library "paf001.dll" alias for "paff07ar_upd_plant_capacity_info;Ansi"


//
// Declaration for procedure: paff08ar_add_plant_capacity_info
// Network connection style : Non-Persistent
// Procedure attributes: authen, retry
//
function int paff08ar_add_plant_capacity_info( &
    ref s_error s_error_info, &
    char req_plant[3], &
    char req_start_date[10], &
    ref string s_capacity_header_string, &
    ref string s_capacity_detail_string, &
    int CommHnd &
) library "paf001.dll" alias for "paff08ar_add_plant_capacity_info;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the Netwise 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "paf001.dll"
function int WBpaf001CkCompleted( int CommHnd ) &
    library "paf001.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type
//
// User Structure:  s_sku_product
//
// global type s_sku_product from structure
//     char        se_sku_product_id[10]         
//     char        se_prod_age_modeled           
//     char        se_sku_product_desc[30]       
// end type
//
// User Structure:  s_super_product
//
// global type s_super_product from structure
//     char        se_super_product_id[10]       
//     char        se_super_product_desc[30]     
// end type
//
// User Structure:  s_supers_and_skus
//
// global type s_supers_and_skus from structure
//     char        se_super_prod_id[10]          
//     char        se_super_prod_descr[30]       
//     char        se_sku_prod_id[10]            
//     char        se_prod_age_modeled           
// end type

end prototypes

type variables
integer ii_paf001_commhandle
end variables

forward prototypes
public function boolean nf_paf03ar (ref s_error astr_error_info, ref character ac_super_id[10], ref integer ai_rec_occurs, ref s_sku_product astr_sku_product_info[50], ref character ac_refetch_sku_id[10], integer ai_commhnd)
public function boolean nf_paf04ar (ref s_error astr_error_info, ref integer ai_upd_super_skus_rec_occurs, ref s_supers_and_skus astr_supers_and_skus_info[50], character ac_update_indicator, integer ai_commhnd)
public function boolean nf_paf05ar (ref s_error astr_error_info, character ac_sku_product_id[10], ref s_sku_product astr_sku_product_info, integer ai_commhnd)
public function boolean nf_paf06ar (ref s_error astr_error_info, character ac_plant[3], character ac_start_date[10], ref string as_header_string, ref string as_detail_string, integer ai_commhnd)
public function boolean nf_paf07ar (ref s_error astr_error_info, character ac_plant[3], character ac_start_date[10], ref string as_header_string, ref string as_detail_string, integer ai_commhnd)
public function boolean nf_paf08ar (ref s_error astr_error_info, character ac_plant[3], character ac_start_date[10], ref string as_header_string, ref string as_detail_string, integer ai_commhnd)
public function boolean nf_paf02ar (ref s_error astr_error_info, ref integer ai_rec_occurs, ref s_super_product astr_super_product_info[50], ref character ac_refetch_super_id[10], integer ai_commhnd)
public function boolean nf_paf01ar (ref s_error astr_error_info, ref character ac_process_option, ref character ac_sortid[19], ref character ac_control_key[15], string as_data_output, integer ai_commhnd)
end prototypes

public function boolean nf_paf03ar (ref s_error astr_error_info, ref character ac_super_id[10], ref integer ai_rec_occurs, ref s_sku_product astr_sku_product_info[50], ref character ac_refetch_sku_id[10], integer ai_commhnd);Int li_rtn

li_rtn =paff03ar_get_sku_product_info(astr_error_info, &
											ac_super_id, &
											ai_rec_occurs, &
											astr_sku_product_info, &
											ac_refetch_sku_id, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)

end function

public function boolean nf_paf04ar (ref s_error astr_error_info, ref integer ai_upd_super_skus_rec_occurs, ref s_supers_and_skus astr_supers_and_skus_info[50], character ac_update_indicator, integer ai_commhnd);Int li_rtn

li_rtn =   paff04ar_upd_supers_and_skus(astr_error_info, &
											ai_upd_super_skus_rec_occurs, &
											astr_supers_and_skus_info, &
											ac_update_indicator, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

public function boolean nf_paf05ar (ref s_error astr_error_info, character ac_sku_product_id[10], ref s_sku_product astr_sku_product_info, integer ai_commhnd);Int li_rtn

li_rtn =   paff05ar_validate_sku_product(astr_error_info, &
											ac_sku_product_id, &
											astr_sku_product_info, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

public function boolean nf_paf06ar (ref s_error astr_error_info, character ac_plant[3], character ac_start_date[10], ref string as_header_string, ref string as_detail_string, integer ai_commhnd);Int li_rtn



li_rtn =  paff06ar_inq_plant_capacity_info(astr_error_info, &
											ac_plant, &
											ac_start_date, &
											as_header_string, &
											as_detail_string, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

public function boolean nf_paf07ar (ref s_error astr_error_info, character ac_plant[3], character ac_start_date[10], ref string as_header_string, ref string as_detail_string, integer ai_commhnd);Int li_rtn

li_rtn =  paff07ar_upd_plant_capacity_info(astr_error_info, &
											ac_plant, &
											ac_start_date, &
											as_header_string, &
											as_detail_string, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

public function boolean nf_paf08ar (ref s_error astr_error_info, character ac_plant[3], character ac_start_date[10], ref string as_header_string, ref string as_detail_string, integer ai_commhnd);Int li_rtn

li_rtn =  paff08ar_add_plant_capacity_info(astr_error_info, &
											ac_plant, &
											ac_start_date, &
											as_header_string, &
											as_detail_string, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

public function boolean nf_paf02ar (ref s_error astr_error_info, ref integer ai_rec_occurs, ref s_super_product astr_super_product_info[50], ref character ac_refetch_super_id[10], integer ai_commhnd);Int li_rtn

li_rtn =  paff02ar_get_super_product_info(astr_error_info, &
											ai_rec_occurs, &
											astr_super_product_info, &
											ac_refetch_super_id, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

public function boolean nf_paf01ar (ref s_error astr_error_info, ref character ac_process_option, ref character ac_sortid[19], ref character ac_control_key[15], string as_data_output, integer ai_commhnd);Int li_rtn

li_rtn =  paff01ar_paf_parameter(astr_error_info, &
											ac_process_option, &
											ac_sortid, &
											ac_control_key, &
											as_data_output, &
											ii_paf001_commhandle) 


return SQLCA.nf_display_message(li_rtn, astr_error_info, ii_paf001_commhandle)


end function

on constructor;call u_netwise_transaction::constructor;// Get the CommHandle to be used for windows
ii_paf001_commhandle= SQLCA.nf_getcommhandle("paf001")
If ii_paf001_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end on

on u_paf001.create
TriggerEvent( this, "constructor" )
end on

on u_paf001.destroy
TriggerEvent( this, "destructor" )
end on

