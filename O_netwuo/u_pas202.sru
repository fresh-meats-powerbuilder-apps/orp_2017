HA$PBExportHeader$u_pas202.sru
$PBExportComments$Planned Demand
forward
global type u_pas202 from u_netwise_transaction
end type
end forward

global type u_pas202 from u_netwise_transaction
end type
global u_pas202 u_pas202

type prototypes
// PowerBuilder Script File: c:\ibp\pas202\pas202.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Fri Oct 18 14:38:35 2002
// Source Interface File: C:\ibp\pas202\PAS202.NTF
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: pasp09br_inq_src_char
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp09br_inq_src_char( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "pas202.dll" alias for "pasp09br_inq_src_char;Ansi"


//
// Declaration for procedure: pasp10br_inq_source
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp10br_inq_source( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas202.dll" alias for "pasp10br_inq_source;Ansi"


//
// Declaration for procedure: pasp11br_upd_source
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp11br_upd_source( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string source_update_string, &
    int CommHnd &
) library "pas202.dll" alias for "pasp11br_upd_source;Ansi"


//
// Declaration for procedure: pasp12br_inq_inventory
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp12br_inq_inventory( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string product_string, &
    ref string inventory_string, &
    int CommHnd &
) library "pas202.dll" alias for "pasp12br_inq_inventory;Ansi"


//
// Declaration for procedure: pasp13br_upd_inventory
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp13br_upd_inventory( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string inventory_header, &
    string inventory_detail, &
    int CommHnd &
) library "pas202.dll" alias for "pasp13br_upd_inventory;Ansi"


//
// Declaration for procedure: pasp14br_inq_inst_char
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp14br_inq_inst_char( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref char product_type, &
    ref string fab_product_descr, &
    ref string characteristic_string, &
    int CommHnd &
) library "pas202.dll" alias for "pasp14br_inq_inst_char;Ansi"


//
// Declaration for procedure: pasp15br_inq_instances
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp15br_inq_instances( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref string valid_plant_string, &
    ref string piece_count_string, &
    ref string time_stamp_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas202.dll" alias for "pasp15br_inq_instances;Ansi"


//
// Declaration for procedure: pasp16br_upd_instances
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp16br_upd_instances( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    string instance_string, &
    ref string timestamp_string, &
    int CommHnd &
) library "pas202.dll" alias for "pasp16br_upd_instances;Ansi"


//
// Declaration for procedure: pasp17br_inq_pa_view
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp17br_inq_pa_view( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_values, &
    ref string qty_by_age_code, &
    ref string shift_production, &
    ref string sold_qty_by_age, &
    ref string sold_qty_by_type, &
    ref double p24b_task_number, &
    ref double p24b_last_record, &
    ref double p24b_max_record, &
    int CommHnd &
) library "pas202.dll" alias for "pasp17br_inq_pa_view;Ansi"


//
// Declaration for procedure: pasp18br_inq_projections
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp18br_inq_projections( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "pas202.dll" alias for "pasp18br_inq_projections;Ansi"


//
// Declaration for procedure: pasp19br_inq_order
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int pasp19br_inq_order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    ref string output_string, &
    int CommHnd &
) library "pas202.dll" alias for "pasp19br_inq_order;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "pas202.dll"
function int WBpas202CkCompleted( int CommHnd ) &
    library "pas202.dll"
//
// ***********************************************************

//
// User Structure:  s_error
//
// global type s_error from structure
//     char        se_app_name[8]                
//     char        se_window_name[8]             
//     char        se_function_name[8]           
//     char        se_event_name[32]             
//     char        se_procedure_name[32]         
//     char        se_user_id[8]                 
//     char        se_return_code[6]             
//     char        se_message[71]                
// end type

end prototypes

type variables
Int	ii_pas202_commhandle
end variables

forward prototypes
public function boolean nf_upd_source (ref s_error astr_error_info, string as_source)
public function boolean nf_upd_inventory (ref s_error astr_error_info, string as_header, string as_detail)
public function boolean nf_inq_inventory (ref s_error astr_error_info, string as_input, ref string as_product, ref string as_inventory)
public function integer nf_upd_instances (ref s_error astr_error_info, string as_header, string as_instances, ref string as_timestamp)
public function boolean nf_inq_order_detail (ref s_error astr_error_info, string as_input, ref string as_order)
public function boolean nf_inq_pa_view (ref s_error astr_error_info, ref string as_input, ref string as_output_values, ref string as_qty_by_age_code, ref string as_shift_production, ref string as_sold_qty_by_age, ref string as_sold_qty_by_type, ref double ad_p24b_task_number, ref double ad_p24b_last_record, ref double ad_p24b_max_record)
public function boolean nf_inq_where_produced (ref s_error astr_error_info, string as_input, ref datawindow adw_projections)
public function boolean nf_inq_instances_chars (ref s_error astr_error_info, string as_input_string, ref character ac_product_type, ref string as_fab_product_descr, ref string as_characteristic_string)
public function boolean nf_inq_srce_char (ref s_error astr_error_info, string as_input, ref string as_source)
public function boolean nf_inq_instances (ref s_error astr_error_info, string as_input, ref string as_mfg_steps, ref string as_instances, ref string as_parameters, ref string as_plants, ref string as_piece_count, ref string as_timestamp_string, ref string as_ratios)
public function boolean nf_inq_source (ref s_error astr_error_info, string as_input, ref datawindow adw_source)
end prototypes

public function boolean nf_upd_source (ref s_error astr_error_info, string as_source);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message 


astr_error_info.se_procedure_name = "pasp11br_upd_source"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp11br_upd_source(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_source, &
										ii_pas202_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)

end function

public function boolean nf_upd_inventory (ref s_error astr_error_info, string as_header, string as_detail);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message


astr_error_info.se_procedure_name = "pasp13br_upd_inventory"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp13br_upd_inventory(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header, &
											as_detail, &
											ii_pas202_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)

end function

public function boolean nf_inq_inventory (ref s_error astr_error_info, string as_input, ref string as_product, ref string as_inventory);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message


as_inventory = Space(17501)
as_product   = Space(43)

astr_error_info.se_procedure_name = "pasp12br_inq_inventory"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp12br_inq_inventory(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
											as_product, &
											as_inventory, &
											ii_pas202_commhandle)

lt_endtime = Now()

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)

end function

public function integer nf_upd_instances (ref s_error astr_error_info, string as_header, string as_instances, ref string as_timestamp);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message
								

astr_error_info.se_procedure_name = "pasp16br_inq_instances"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp16br_upd_instances(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header, &
								as_instances, &
								as_timestamp, &
											ii_pas202_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

 nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)
 
 return li_ret


end function

public function boolean nf_inq_order_detail (ref s_error astr_error_info, string as_input, ref string as_order);Double	ld_task_number, &
			ld_last_record_number, &
			ld_max_record_number

Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_data, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message

as_order = ""
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

astr_error_info.se_procedure_name = "pasp19br_inq_order"
astr_error_info.se_message = Space(70)
Do
	ls_data = Space(12101)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp19br_inq_order(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
										ld_task_number, &
										ld_last_record_number, &
										ld_max_record_number, &
										ls_data, &
										ii_pas202_commhandle)

	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	
	nf_set_s_error ( astr_error_info,&
						ls_app_name, &
						ls_window_name,&
						ls_function_name,&
						ls_event_name, &
						ls_procedure_name, &
						ls_user_id, &
						ls_return_code, &
						ls_message )	

	If Not nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle) Then return False

	as_order += ls_data

Loop While ld_last_record_number <> ld_max_record_number

return True
end function

public function boolean nf_inq_pa_view (ref s_error astr_error_info, ref string as_input, ref string as_output_values, ref string as_qty_by_age_code, ref string as_shift_production, ref string as_sold_qty_by_age, ref string as_sold_qty_by_type, ref double ad_p24b_task_number, ref double ad_p24b_last_record, ref double ad_p24b_max_record);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message
								

as_output_values    = Space(701)
as_qty_by_age_code  = Space(4700)
as_shift_production = Space(969)
as_sold_qty_by_age  = Space(33700)
as_sold_qty_by_type = Space(435)
// ibdkdld Ext pa dates 0810/2002
as_input = as_input + space(300 - len(as_input))

astr_error_info.se_procedure_name = "pasp17br_inq_pa_view"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

// Changed the input string to be an argument string in/out to send back the boxes per pallet. dld

li_ret = pasp17br_inq_pa_view(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
										as_output_values, &
										as_qty_by_age_code, &
										as_shift_production, &
										as_sold_qty_by_age, &
										as_sold_qty_by_type, &
										ad_p24b_task_number, &
										ad_p24b_last_record, &
										ad_p24b_max_record, &
										ii_pas202_commhandle)

lt_endtime = Now()

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
					
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)
end function

public function boolean nf_inq_where_produced (ref s_error astr_error_info, string as_input, ref datawindow adw_projections);Double	ld_Task, &
			ld_LastRecord, &
			ld_MaxRecord

Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_Projection, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message


ls_projection = Space(20000)

ld_Task = 0
ld_LastRecord = 0
ld_MaxRecord = 0

astr_error_info.se_procedure_name = "pasp18br_inq_projections"
astr_error_info.se_message = Space(70)

adw_projections.Reset()

Do
	nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

	lt_starttime = Now()

	li_ret = pasp18br_inq_projections(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
								ls_projection, &
								ld_Task, &
								ld_LastRecord, &
								ld_MaxRecord, &
								ii_pas202_commhandle)

	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	
	nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

	If Not nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle) Then
		return False
	End if

	adw_projections.ImportString(ls_Projection)

Loop While ld_LastRecord <> ld_MaxRecord

Return True
end function

public function boolean nf_inq_instances_chars (ref s_error astr_error_info, string as_input_string, ref character ac_product_type, ref string as_fab_product_descr, ref string as_characteristic_string);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
string						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message


as_fab_product_descr = Space(30)
as_characteristic_string = Space(5000)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = pasp14br_inq_inst_char(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input_string, &
								ac_product_type, &
								as_fab_product_descr, &
								as_characteristic_string, &
								ii_pas202_commhandle)
	 
lt_endtime = Now()

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)

end function

public function boolean nf_inq_srce_char (ref s_error astr_error_info, string as_input, ref string as_source);Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message


as_source = Space(20000)
 
astr_error_info.se_procedure_name = " pasp09br_inq_src_char"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret =  pasp09br_inq_src_char(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
								as_source, &
								ii_pas202_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)

end function

public function boolean nf_inq_instances (ref s_error astr_error_info, string as_input, ref string as_mfg_steps, ref string as_instances, ref string as_parameters, ref string as_plants, ref string as_piece_count, ref string as_timestamp_string, ref string as_ratios);// add comment
Boolean					lb_first_time

Double					ld_last_record_number, &
							ld_max_record_number, &
							ld_task_number

Int	li_ret

Time							lt_starttime, &
								lt_endtime

Long						ll_len

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message, &
							ls_output_string, &
							ls_output, &
							ls_string_ind, &
							ls_plants, &
							ls_piece_count, &
							ls_timestamp_string
u_string_functions	lu_string


as_mfg_steps 	= ''
as_instances 	= ''
as_parameters 	= ''
as_ratios 	= ''
as_plants    	= ''
as_piece_count	= ''
as_timestamp_string	= ''

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0

astr_error_info.se_procedure_name = "pasp15br_inq_instances"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )
								
lb_first_time = True

Do
	ls_output_string = Space(30000)
	ls_plants      = Space(501)
	ls_piece_count = Space(379)
	ls_timestamp_string = Space(27)

	lt_starttime = Now()
	
	li_ret = pasp15br_inq_instances(ls_app_name, &
									ls_window_name, &
									ls_function_name, &
									ls_event_name, &
									ls_procedure_name, &
									ls_user_id,&
									ls_return_code,&
									ls_message,&
									as_input, &
									ls_output_string, &
									ls_plants, &
									ls_piece_count, &
									ls_timestamp_string, &
									ld_task_number, &
									ld_last_record_number, &
									ld_max_record_number, &
									ii_pas202_commhandle)

	ll_len = Len(ls_output_string)
	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

	If lb_first_time Then
		as_plants = ls_plants
		as_piece_count = ls_piece_count
		as_timestamp_string = ls_timestamp_string
		lb_first_time = False
	End If
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
		Choose Case ls_string_ind
			Case 'I'
				as_instances += ls_output
			Case 'P'
				as_parameters += ls_output
			Case 'S'
				as_mfg_steps += ls_output
			Case 'R'
				as_ratios += ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle)

end function

public function boolean nf_inq_source (ref s_error astr_error_info, string as_input, ref datawindow adw_source);Double	ld_task, &
			ld_max, &
			ld_last

Int	li_ret

Time							lt_starttime, &
								lt_endtime
								
String						ls_source, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message


ls_source = Space(20000)
 
astr_error_info.se_procedure_name = "pasp10br_inq_source"
astr_error_info.se_message = Space(70)

ld_task = 0
ld_max = 0
ld_last = 0
adw_source.Reset()

Do
	nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

	lt_starttime = Now()

	li_ret = pasp10br_inq_source(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_input, &
								ls_source, &
								ld_task, &
								ld_last, &
								ld_max, &								
								ii_pas202_commhandle)

	lt_endtime = Now()
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	
	nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

	If Not nf_display_message(li_ret, astr_error_info, ii_pas202_commhandle) Then
		return false
	End if

	adw_source.ImportString(ls_source)
Loop while ld_max <> ld_last and li_ret >= 0

return true
end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows
ii_pas202_commhandle = sqlca.nf_getcommhandle("pas202")
If ii_pas202_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end event

on u_pas202.create
call super::create
end on

on u_pas202.destroy
call super::destroy
end on

