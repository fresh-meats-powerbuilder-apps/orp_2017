HA$PBExportHeader$u_utl001.sru
$PBExportComments$Acts as communicating object with netwise for "utl001" This is regeisteres IN the OP app temporarily
forward
global type u_utl001 from u_netwise_transaction
end type
type s_tutlgrup from structure within u_utl001
end type
type s_tutlloc_short from structure within u_utl001
end type
type s_tutlprof from structure within u_utl001
end type
end forward

type s_tutlgrup from structure
    character se_user_id[8]
    character se_group_id[3]
    character se_job_id[3]
    character se_last_update_time[26]
    character se_system_id[3]
end type

type s_tutlloc_short from structure
    character se_location_code[3]
    character se_complex_code[3]
    character se_location_type
    string se_name
    character se_std_point_loc_code[9]
end type

type s_tutlprof from structure
    character se_group_id[3]
    character se_window_name[8]
    character se_appl_id[3]
    character se_inquire_auth
    character se_add_auth
    character se_modify_auth
    character se_delete_auth
    character se_last_update_time[26]
    character se_last_update_userid[8]
end type

global type u_utl001 from u_netwise_transaction
end type
global u_utl001 u_utl001

type prototypes
// PowerBuilder Script File: j:\pb\test\src32\utl001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Sat Mar 22 12:20:31 1997
// Source Interface File: j:\pb\test\src32\utl001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: utlu00ar_get_security_code
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu00ar_get_security_code( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Name_System_input, &
    ref string group_id, &
    int CommHnd &
) library "utl001.dll" alias for "utlu00ar_get_security_code;Ansi"


//
// Declaration for procedure: utlu02ar_get_all_window_access
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu02ar_get_all_window_access( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double task_number, &
    ref double page_number, &
    ref double Max_page_number, &
    ref string window_access_string, &
    int CommHnd &
) library "utl001.dll" alias for "utlu02ar_get_all_window_access;Ansi"


//
// Declaration for procedure: utlu03ar_get_tutltype
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu03ar_get_tutltype( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref int tutltype_rec_occurs, &
    ref string tutltype_info, &
    char refetch_record_type[8], &
    char refetch_type_code[8], &
    int CommHnd &
) library "utl001.dll" alias for "utlu03ar_get_tutltype;Ansi"


//
// Declaration for procedure: utlu04ar_get_loc_codes
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu04ar_get_loc_codes( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref int tutlloc_rec_occurs, &
    ref string tutlloc_info, &
    int CommHnd &
) library "utl001.dll" alias for "utlu04ar_get_loc_codes;Ansi"


//
// Declaration for procedure: utlu05ar_Get_Sale_people
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu05ar_Get_Sale_people( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string sales_people_string, &
    ref int sales_people_count, &
    ref string smancode, &
    ref string smantype, &
    int CommHnd &
) library "utl001.dll" alias for "utlu05ar_Get_Sale_people;Ansi"


//
// Declaration for procedure: utlu06ar_get_windows_to_access
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu06ar_get_windows_to_access( &
    string se_app_name, &
    string se_window_name, &
    string se_function_name, &
    string se_event_name, &
    string se_procedure_name, &
    string se_use_id, &
    string se_return_code, &
    string se_message, &
    string groupid_name_string, &
    ref string window_info_String, &
    ref int num_recs_rtn, &
    int CommHnd &
) library "utl001.dll" alias for "utlu06ar_get_windows_to_access;Ansi"


//
// Declaration for procedure: utlu07ar_Get_Security
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu07ar_Get_Security( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string name_password, &
    ref char Action_Indicator, &
    int CommHnd &
) library "utl001.dll" alias for "utlu07ar_Get_Security;Ansi"


//
// Declaration for procedure: utlu09ar_Header
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu09ar_Header( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref long msg_number, &
    ref string header_String, &
    string exception_String, &
    string UserId_in, &
    int CommHnd &
) library "utl001.dll" alias for "utlu09ar_Header;Ansi"


//
// Declaration for procedure: utlu10ar_get_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu10ar_get_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref long msg_number, &
    ref long msg_detail_number, &
    ref string Detail_String, &
    string UserID_in, &
    int CommHnd &
) library "utl001.dll" alias for "utlu10ar_get_detail;Ansi"


//
// Declaration for procedure: utlu11ar_Delete_msg
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu11ar_Delete_msg( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string msg_number_string, &
    int CommHnd &
) library "utl001.dll" alias for "utlu11ar_Delete_msg;Ansi"


//
// Declaration for procedure: utlu12ar_check_message
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu12ar_check_message( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string ip_address, &
    ref char check_message, &
    int CommHnd &
) library "utl001.dll" alias for "utlu12ar_check_message;Ansi"


//
// Declaration for procedure: utlu14ar_GetMessageCounts
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu14ar_GetMessageCounts( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_reurn_code, &
    ref string se_message, &
    ref string message_count_and_type, &
    string UserID_in, &
    int CommHnd &
) library "utl001.dll" alias for "utlu14ar_GetMessageCounts;Ansi"


//
// Declaration for procedure: utlu15ar_GetUsers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu15ar_GetUsers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string List_Of_Users, &
    int CommHnd &
) library "utl001.dll" alias for "utlu15ar_GetUsers;Ansi"


//
// Declaration for procedure: utlu16ar_Alias_Extract
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu16ar_Alias_Extract( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Req_user_id, &
    ref string Alias_String, &
    int CommHnd &
) library "utl001.dll" alias for "utlu16ar_Alias_Extract;Ansi"


//
// Declaration for procedure: utlu17ar_Alias_Update
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu17ar_Alias_Update( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_String, &
    int CommHnd &
) library "utl001.dll" alias for "utlu17ar_Alias_Update;Ansi"


//
// Declaration for procedure: utlu18ar_get_shipto_customers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu18ar_get_shipto_customers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_number, &
    ref double Page_number, &
    ref double max_page_number, &
    ref string Shipto_Customers, &
    int CommHnd &
) library "utl001.dll" alias for "utlu18ar_get_shipto_customers;Ansi"


//
// Declaration for procedure: utlu19ar_get_billto_customers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu19ar_get_billto_customers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_Number, &
    ref double Page_number, &
    ref string Billto_customers, &
    int CommHnd &
) library "utl001.dll" alias for "utlu19ar_get_billto_customers;Ansi"


//
// Declaration for procedure: utlu20ar_Get_Corporate_customers
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu20ar_Get_Corporate_customers( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_Number, &
    ref double Page_number, &
    ref string Corporate_customers, &
    int CommHnd &
) library "utl001.dll" alias for "utlu20ar_Get_Corporate_customers;Ansi"


//
// Declaration for procedure: utlu21ar_get_serviceCenters
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu21ar_get_serviceCenters( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string sales_location_and_names, &
    int CommHnd &
) library "utl001.dll" alias for "utlu21ar_get_serviceCenters;Ansi"


//
// Declaration for procedure: utlu22ar_Get_Carrier_info
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu22ar_Get_Carrier_info( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string carrier_type, &
    string carrier_status, &
    ref string carrier_info_string, &
    ref string refetch_carrier_code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu22ar_Get_Carrier_info;Ansi"


//
// Declaration for procedure: utlu23ar_get_carrier_pref_ext
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu23ar_get_carrier_pref_ext( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string carrier_pref_string, &
    ref string refetch_customer_id, &
    ref string refetch_customer_type, &
    ref string refetch_carrier_code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu23ar_get_carrier_pref_ext;Ansi"


//
// Declaration for procedure: utlu24ar_get_customer_defaults
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu24ar_get_customer_defaults( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Location, &
    ref string customer_defaults_out, &
    ref double Task_number, &
    ref double Page_number, &
    ref double Max_Page_number, &
    int CommHnd &
) library "utl001.dll" alias for "utlu24ar_get_customer_defaults;Ansi"


//
// Declaration for procedure: utlu25ar
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu25ar( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Recipient_id, &
    string App_ID, &
    string Type_code, &
    ref string Telephone_Number, &
    ref string cover_form, &
    ref string Reciepient_name, &
    ref string Reciepient_attn, &
    int CommHnd &
) library "utl001.dll" alias for "utlu25ar;Ansi"


//
// Declaration for procedure: utlu26ar
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu26ar( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string product_codes_out, &
    ref double max_page_number, &
    ref double page_number, &
    ref double task_number, &
    int CommHnd &
) library "utl001.dll" alias for "utlu26ar;Ansi"

//
// Declaration for procedure: utlu27ar_Load_Address
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu27ar_Load_Address( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string Address_Data_String, &
    ref string Refetch_Address_Type, &
    ref string Refetch_Address_Code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu27ar_Load_Address;Ansi"

//
// Declaration for procedure: utlu28ar_Load_Tskuplt
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int utlu28ar_Load_Tskuplt( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string Product_data_String, &
    ref string Refetch_product_code, &
    ref string Refetch_plant_code, &
    int CommHnd &
) library "utl001.dll" alias for "utlu28ar_Load_Tskuplt;Ansi"


// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "utl001.dll"
function int WButl001CkCompleted( int CommHnd ) &
    library "utl001.dll"
//
// ***********************************************************


end prototypes

type variables
int ii_utl001_commhandle
ConnectionInfo	ici_Myinf[]
Environment	ienv_Environment

end variables

forward prototypes
public function integer nf_utlu07ar (ref s_error astr_error_info, ref string as_name_password, ref character ac_action_indicator)
public function boolean nf_utlu11ar (ref s_error astr_error_info, ref string as_message_string, integer ai_commhnd)
public function boolean nf_utlu04ar (ref s_error astr_error_info, ref integer ai_tutlloc_rec_occurs, ref string as_tutlloc_info)
public function boolean nf_utlu06ar (ref s_error astr_error_info, string as_groupid_name_string, ref string as_window_info_string, ref integer ai_num_recs_rtn)
public function boolean nf_utlu12ar (ref s_error astr_error_info, ref character as_check_message)
public function boolean nf_utlu09ar (ref s_error astr_error_info, ref long al_msg_number, ref string as_header_string, string as_exception_string, string as_userid_in, integer ai_commhnd)
public function boolean nf_utlu10ar (ref s_error astr_error_info, ref long al_message_number, ref long al_line_number, ref string as_detail_string, string as_userid_in, integer ai_commhnd)
public function boolean nf_utlu14ar (ref s_error astr_error_info, ref string as_message_count_type, string as_userid_in, integer ai_commhnd)
public function boolean nf_utlu15ar (ref s_error astr_error_info, ref string as_list_of_users, integer ai_commhnd)
public function boolean nf_utlu17ar (ref s_error astr_error_info, string as_input_string, integer ai_commhnd)
public function boolean nf_utlu16ar (ref s_error astr_error_info, string as_userid, ref string as_alias_string, integer ai_commhnd)
public function integer nf_utlu03ar (ref datastore adw_tutltype, ref s_error astr_error_info)
public function integer nf_utlu00ar (ref s_error astr_error_info, string as_name_type, ref string as_group_id)
public function integer nf_utlu02ar_get_window_access (ref s_error astr_error_info, ref datastore adw_datastore)
public function integer nf_utlu18ar_getshipto_customers (ref s_error astr_error_info, ref datastore adw_datastore)
public function integer nf_utlu19ar_getbillto_customers (ref s_error s_error_info, ref datastore adw_datastore)
public function integer nf_utlu20ar_get_corp_customers (ref s_error astr_error_info, ref datastore adw_datastore)
public function integer nf_utlu22ar_getcarriers (string as_carrier_type, string as_carrier_status, ref string as_carrier_info_string, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_utlu23ar_getcarrier_pref (ref string as_carrier_pref_string, ref string as_refetch_customer_id, ref string as_refetch_customer_type, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd)
public function integer nf_utlu21ar_get_service_centers (ref s_error astr_error_info, ref string as_output_info)
public function integer nf_utlu24ar_load_customer_defaults (ref datastore ads_datastore, string as_location)
public function integer nf_utlu26ar_load_productcodes (ref datastore ads_datastore)
public function integer nf_utlu25ar_getfaxing_info (string as_recipient_id, ref string as_type_code, ref string as_telephone_number, ref string as_cover_form, ref string as_recipient_name, ref string as_recipient_attn)
public function integer nf_utlu27ar_get_addresses (ref datastore ads_datastore)
public function integer nf_utlu28ar_load_tskuplt (ref datastore ads_datastore)
public function integer nf_utlu05ar (ref s_error astr_error_info, ref string as_sales_people_string, ref integer ai_sales_people_count, ref string as_smancode, ref string as_smantype)
end prototypes

public function integer nf_utlu07ar (ref s_error astr_error_info, ref string as_name_password, ref character ac_action_indicator);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


// Call a Netwise external function to get the required information
astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu07ar_get_security'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
//li_rtn = utlu07ar_get_security(		ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//		as_name_password, &
//		ac_action_indicator, &
//		ii_utl001_commhandle)



nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)


Return li_rtn
end function

public function boolean nf_utlu11ar (ref s_error astr_error_info, ref string as_message_string, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


// Call a Netwise external function to get the required information

SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu11ar_delete_msg'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn = utlu11ar_delete_msg(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_message_string, &
//					ii_utl001_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function boolean nf_utlu04ar (ref s_error astr_error_info, ref integer ai_tutlloc_rec_occurs, ref string as_tutlloc_info);Int li_rtn


String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

SetPointer(HourGlass!)

as_TutlLoc_Info = Space(9401)

astr_error_info.se_app_name = Message.nf_Get_App_ID()
astr_error_info.se_procedure_name = 'utlu04ar_get_loc_codes'
astr_error_info.se_user_id = sqlca.userid
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


// Call a Netwise external function to get the required information
//li_rtn = utlu04ar_get_loc_codes( ls_app_name, &
//											ls_window_name,&
//											ls_function_name,&
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id, &
//											ls_return_code, &
//											ls_message , &
//											ai_tutlloc_rec_occurs, &
//											as_tutlloc_info, &
//											ii_utl001_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)

end function

public function boolean nf_utlu06ar (ref s_error astr_error_info, string as_groupid_name_string, ref string as_window_info_string, ref integer ai_num_recs_rtn);Int li_rtn


String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



SetPointer(HourGlass!)
as_Window_Info_String = Space(9901)

astr_error_info.se_app_name = Message.nf_Get_App_ID()
astr_error_info.se_user_id = sqlca.userid
astr_error_info.se_procedure_name = 'utlu06ar_get_windows_to_access'
astr_error_info.se_message = Space(70)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// li_num_recs_rtn is put in for refetch logic, App Currently returns 100
// Windows Max

//li_rtn = utlu06ar_get_windows_to_access(ls_app_name, &
//													ls_window_name, &
//													ls_function_name, &
//													ls_event_name, &
//													ls_procedure_name, &
//													ls_user_id,&
//													ls_return_code,&
//													ls_message, &
//													as_groupid_name_string, &
//													as_window_info_string, &
//													ai_num_recs_rtn, &
//													ii_utl001_commhandle)
													
													
													
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)

end function

public function boolean nf_utlu12ar (ref s_error astr_error_info, ref character as_check_message);Int							li_ret

String						ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message, &
								ls_ip_addr

u_ip_functions	lu_ip_address_functions


If ienv_Environment.OSType = WindowsNT! Then Return False

ls_ip_addr = lu_ip_address_functions.nf_get_ip_address()

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu12ar_check_message'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_ret = utlu12ar_check_message(ls_app_name, &
//											ls_window_name,&
//											ls_function_name,&
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id, &
//											ls_return_code, &
//											ls_message, &
//											ls_ip_addr, &
//											as_check_message, &
//											ii_utl001_commhandle)
											
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )											

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_ret, astr_error_info, ii_utl001_commhandle)




end function

public function boolean nf_utlu09ar (ref s_error astr_error_info, ref long al_msg_number, ref string as_header_string, string as_exception_string, string as_userid_in, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



// Call a Netwise external function to get the required information

SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu09ar_get_detail'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


//li_rtn = utlu09ar_header(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					al_msg_number, &
//					as_header_string, &
//					as_exception_string, &
//					as_userid_in, &
//					ii_utl001_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 



// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function boolean nf_utlu10ar (ref s_error astr_error_info, ref long al_message_number, ref long al_line_number, ref string as_detail_string, string as_userid_in, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


// Call a Netwise external function to get the required information

SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu10ar_get_detail'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn = utlu10ar_get_detail(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					al_message_number, &
//					al_line_number, &
//					as_detail_string, &
//					as_userid_in, &
//					ii_utl001_commhandle)



nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function boolean nf_utlu14ar (ref s_error astr_error_info, ref string as_message_count_type, string as_userid_in, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


// Call a Netwise external function to get the required information
astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu14ar_get_messagecounts'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
//li_rtn = utlu14ar_GetMessageCounts(ls_app_name, &
//												ls_window_name,&
//												ls_function_name,&
//												ls_event_name, &
//												ls_procedure_name, &
//												ls_user_id, &
//												ls_return_code, &
//												ls_message, &
//												as_message_count_type, &
//												as_userid_in, &
//												ii_utl001_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function boolean nf_utlu15ar (ref s_error astr_error_info, ref string as_list_of_users, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

// Call a Netwise external function to get the required information
astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu15ar_get_messagecounts'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
//li_rtn = utlu15ar_GetUsers(ls_app_name, &
//									ls_window_name,&
//									ls_function_name,&
//									ls_event_name, &
//									ls_procedure_name, &
//									ls_user_id, &
//									ls_return_code, &
//									ls_message, &
//									as_list_of_users, &
//									ii_utl001_commhandle)
//
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function boolean nf_utlu17ar (ref s_error astr_error_info, string as_input_string, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


// Call a Netwise external function to get the required information
astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu17ar_Alias_Update'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
//li_rtn = utlu17ar_Alias_Update(	ls_app_name, &
//											ls_window_name,&
//											ls_function_name,&
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id, &
//											ls_return_code, &
//											ls_message, &
//											as_input_string, &
//											ii_utl001_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function boolean nf_utlu16ar (ref s_error astr_error_info, string as_userid, ref string as_alias_string, integer ai_commhnd);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


// Call a Netwise external function to get the required information
astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu16ar_Alias_Extract'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
//li_rtn = utlu16ar_Alias_Extract(	ls_app_name, &
//											ls_window_name,&
//											ls_function_name,&
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id, &
//											ls_return_code, &
//											ls_message, &
//											as_userid, &
//											as_alias_string, ii_utl001_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
Return sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)



end function

public function integer nf_utlu03ar (ref datastore adw_tutltype, ref s_error astr_error_info);Char	lc_refetch_record_type[8], &
		lc_refetch_type_code[8]

Int 	li_rtn, &
		li_rec_count, &
		li_tutltype_rec_occurs

Long		ll_row_count

String	ls_tutltype_info, &
			ls_temp


String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu03ar_get_tutltype'


Do
	SetPointer(HourGlass!)

	ls_TutlType_Info = Space(14601)

	ll_row_count = adw_tutltype.RowCount()
	If ll_row_count > 0 Then
		ls_temp = adw_tutltype.GetItemString(adw_tutltype.RowCount(), "record_type")
		If Not IsNull(ls_temp) Then
			lc_refetch_record_type = ls_temp
		Else
			lc_refetch_record_type = Space(8)
		End if
	
		ls_temp = adw_tutltype.GetItemString(adw_tutltype.RowCount(), "type_code")
		If Not IsNull(ls_temp) Then 
			lc_refetch_type_code = ls_temp
		Else
			lc_refetch_type_code = Space(8)
		End if
	Else
		lc_refetch_record_type = Space(8)
		lc_refetch_type_code = Space(8)
	End if

	// Call a Netwise external function to get the required information
//	li_rtn = utlu03ar_get_tutltype(ls_app_name, &
//											ls_window_name,&
//											ls_function_name,&
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id, &
//											ls_return_code, &
//											ls_message, &
//											li_tutltype_rec_occurs, &
//											ls_tutltype_info, &
//											lc_refetch_record_type, &
//											lc_refetch_type_code, &
//											ii_utl001_commhandle)
//

	nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )			


	// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
	IF li_Rtn < 0 Then return -1
	sqlca.nf_display_message(li_rtn, &
							astr_error_info, ii_utl001_commhandle)


	li_rec_count = adw_tutltype.ImportString(Trim(ls_TutlType_Info))
	If li_rec_count < 1 Then
		MessageBox("Programmer Error - w_utl_data", "Problem populating " + &
				"data into the dw_type_description datawindow.", StopSign!)
		Return li_rtn
	End If

Loop While li_tutltype_rec_occurs = 200

Return li_rtn
end function

public function integer nf_utlu00ar (ref s_error astr_error_info, string as_name_type, ref string as_group_id);Int li_rtn

String 		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message


// Call a Netwise external function to get the required information
SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu00ar_get_security'


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )



//li_rtn =  utlu00ar_get_security_code( &
//    			ls_app_name, &
//				ls_window_name, &
//				ls_function_name, &
//				ls_event_name, &
//				ls_procedure_name, &
//				ls_user_id,&
//				ls_return_code,&
//				ls_message,&
//   			as_Name_Type, &
//    			as_Group_id, &
//    			ii_utl001_commhandle )
				 
				 
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)


Return li_rtn
end function

public function integer nf_utlu02ar_get_window_access (ref s_error astr_error_info, ref datastore adw_datastore);Double		   ld_task_number, &
				   ld_page_number, &
			   	ld_Max_page_number
					
Int li_rtn

String		   ls_window_access_string

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

ls_window_access_string = Space(19880)

SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu02ar_get_loc_codes'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


// Call a Netwise external function to get the required information
DO
//	li_rtn =  utlu02ar_get_all_window_access( &
//					ls_app_name, &
//					ls_window_name, &
//					ls_function_name, &
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id,&
//					ls_return_code,&
//					ls_message,&
//				   ld_task_number, &
//				   ld_page_number, &
//			   	ld_Max_page_number, &
//				   ls_window_access_string, &
//				   ii_utl001_commhandle)
					
					
					
	nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 
				
	// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
	IF sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle) Then
		adw_DataStore.ImportString( ls_window_access_string)
	Else
		exit
	END IF	
LOOP WHILE ld_page_Number <> ld_Max_Page_Number

Return li_rtn
end function

public function integer nf_utlu18ar_getshipto_customers (ref s_error astr_error_info, ref datastore adw_datastore);Double		   ld_task_number, &
				   ld_page_number, &
			   	ld_Max_page_number
					
Int li_rtn

String ls_Shipto_Customers

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


ls_Shipto_Customers= Space(19880)

SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu02ar_get_loc_codes'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
 
// Call a Netwise external function to get the required information
DO
//	li_rtn = utlu18ar_get_shipto_customers( &
//	    			ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message,&
//					ld_Task_number, &
//					ld_Page_number, &
//					ld_max_page_number, &
//					ls_Shipto_Customers, &
//					ii_utl001_commhandle)
		 
	nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	 
		 
		 
   // Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
	IF sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle) Then
		adw_datastore.ImportString( ls_Shipto_Customers)
	Else
		exit
	END IF	
LOOP WHILE ld_page_Number <> ld_Max_Page_Number

Return li_rtn
end function

public function integer nf_utlu19ar_getbillto_customers (ref s_error s_error_info, ref datastore adw_datastore);Double		   ld_task_number, &
				   ld_page_number

					
Int li_rtn

String ls_Billto_customers

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

s_error	lstr_error_info

ls_Billto_customers= Space(20010)

SetPointer(HourGlass!)

lstr_error_info.se_app_name       = Message.nf_Get_App_ID()
lstr_error_info.se_user_id        = sqlca.userid
lstr_error_info.se_Message        = Space(70)
lstr_error_info.se_procedure_name = 'utlu19ar_get_billto'

nf_get_s_error_values ( &
		Lstr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
DO
	//Place call to get customers
//	li_rtn	=	utlu19ar_get_billto_customers(ls_app_name, &
//															ls_window_name,&
//										  					ls_function_name,&
//															ls_event_name, &
//										 					ls_procedure_name, &
//															ls_user_id, &
//										 					ls_return_code, &
//															ls_message, &
//    														ld_Task_Number, &
//														   ld_Page_number, &
//														   ls_Billto_customers, &
//															ii_utl001_commhandle)
   
	
	nf_set_s_error ( Lstr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
	
	// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
	IF sqlca.nf_display_message(li_rtn, lstr_error_info, ii_utl001_commhandle) Then
		adw_datastore.ImportString( ls_Billto_customers)
	Else
		exit
	END IF	
LOOP WHILE ld_page_Number <> 0 AND ld_Task_Number <> 0 

Return li_rtn
end function

public function integer nf_utlu20ar_get_corp_customers (ref s_error astr_error_info, ref datastore adw_datastore);Double		   ld_task_number, &
				   ld_page_number
			   
					
Int li_rtn

String ls_Corporate_customers

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

ls_Corporate_customers= Space(20010)

SetPointer(HourGlass!)

astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu20ar_get_corp_customers'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
DO
	//Place call to get customers
//	li_rtn = utlu20ar_Get_Corporate_customers( &
//    				ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//   				ld_Task_Number, &
//					ld_Page_number, &
//    				ls_Corporate_customers, &
//					ii_utl001_commhandle)
	
	
	nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
	
	// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
	IF sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle) Then
		adw_datastore.ImportString( ls_Corporate_customers)
	Else
		exit
	END IF	
LOOP WHILE ld_page_Number <> 0 AND ld_Task_Number <> 0

Return li_rtn
end function

public function integer nf_utlu22ar_getcarriers (string as_carrier_type, string as_carrier_status, ref string as_carrier_info_string, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//call an external netwise function to get the required information
//li_rtn = utlu22ar_Get_Carrier_info( ls_app_name, &
//												ls_window_name,&
//												ls_function_name,&
//												ls_event_name, &
//												ls_procedure_name, &
//												ls_user_id, &
//												ls_return_code, &
//												ls_message,&
//												as_carrier_type, &
//												as_carrier_status, &
//									         as_carrier_info_string, &
//												as_refetch_carrier_code, &
//										 		ii_utl001_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
					
//check for any RPC errors to display messages
SQLCA.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)

return li_rtn
end function

public function integer nf_utlu23ar_getcarrier_pref (ref string as_carrier_pref_string, ref string as_refetch_customer_id, ref string as_refetch_customer_type, ref string as_refetch_carrier_code, ref s_error astr_error_info, integer ai_commhnd);int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


//call an external netwise function to get the required information
//li_rtn = utlu23ar_get_carrier_pref_ext(	ls_app_name, &
//														ls_window_name,&
//														ls_function_name,&
//														ls_event_name, &
//														ls_procedure_name, &
//														ls_user_id, &
//														ls_return_code, &
//														ls_message,&
//														as_carrier_pref_string, &
//											         as_refetch_customer_id, &
//														as_refetch_customer_type, &
//											         as_refetch_carrier_code, &
//											         ii_utl001_commhandle)


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

//check for any RPC errors to display messages
SQLCA.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)

return li_rtn    
end function

public function integer nf_utlu21ar_get_service_centers (ref s_error astr_error_info, ref string as_output_info);int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

as_OutPut_Info = Space(3101)
as_OutPut_Info = Fill(Char(0),3101)
SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the customer information

//li_rtn = utlu21ar_get_serviceCenters( &
//			ls_app_name, &
//			ls_window_name,&
//			Ls_function_name,&
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id, &
//			ls_return_code, &
//			ls_message, &
//			as_output_info, &
//			ii_utl001_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_utl001_commhandle)



return li_rtn
end function

public function integer nf_utlu24ar_load_customer_defaults (ref datastore ads_datastore, string as_location);Double	ld_Page_Number,&
		 	ld_Task_Number,&
			ld_Max_PageNumber
			
Int	li_rtn

String ls_customer_defaults_out

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

s_error	lstr_error_info

lstr_error_info.se_app_name       = Message.nf_Get_App_ID()
lstr_error_info.se_user_id        = sqlca.userid
lstr_error_info.se_Message        = Space(70)
lstr_error_info.se_procedure_name = 'utlu24ar_get_customer_defaults'

nf_get_s_error_values ( &
		Lstr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


ls_customer_defaults_out = Space(61200)
ls_customer_defaults_out = Fill(CHAR(0),61200)

Do 
//	li_rtn = utlu24ar_get_customer_defaults( &
//   	ls_app_name, &
//		ls_window_name,&
//		ls_function_name,&
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id, &
//		ls_return_code, &
//		ls_message, &
//	   as_location, &
//	   ls_customer_defaults_out, &
//	   ld_Task_number, &
//	   ld_Page_number, &
//	   ld_Max_PageNumber,&
//		ii_utl001_commhandle) 
		 
	nf_set_s_error ( Lstr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
					
	IF sqlca.nf_display_message(li_rtn, lstr_error_info, ii_utl001_commhandle) Then
		li_rtn = ads_DataStore.ImportString( ls_customer_defaults_out)
		gw_netwise_frame.SetMicroHelp(String( li_rtn))
	ELSE
		EXIT
	END IF
Loop While ld_Page_Number <> 0 AND ld_Task_Number <> 0 AND ld_Max_PageNumber <> 0
Return ads_DataStore.RowCount()






end function

public function integer nf_utlu26ar_load_productcodes (ref datastore ads_datastore);Double	ld_Page_Number,&
		 	ld_Task_Number,&
			ld_Max_PageNumber
			
Int	li_rtn

String	ls_productcodes_out,&
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

s_error	lstr_error_info

lstr_error_info.se_app_name       = Message.nf_Get_App_ID()
lstr_error_info.se_user_id        = sqlca.userid
lstr_error_info.se_Message        = Space(70)
lstr_error_info.se_procedure_name = 'utlu26ar_load_productcodes'

nf_get_s_error_values ( &
		lstr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


ls_productcodes_out = Space(61200)
ls_productcodes_out = Fill(CHAR(0),61200)

Do 
//	li_rtn = utlu26ar(ls_app_name, &
//							ls_window_name,&
//							ls_function_name,&
//							ls_event_name, &
//							ls_procedure_name, &
//							ls_user_id, &
//							ls_return_code, &
//							ls_message, &
//						   ls_productcodes_out, &
//						   ld_Task_number, &
//						   ld_Page_number, &
//						   ld_Max_PageNumber,&
//							ii_utl001_commhandle) 
							 
	nf_set_s_error ( lstr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
					
	IF sqlca.nf_display_message(li_rtn, lstr_error_info, ii_utl001_commhandle) Then
		ads_DataStore.ImportString( ls_productcodes_out)
	ELSE
		EXIT
	END IF
Loop While ld_Page_Number <> 0 AND ld_Task_Number <> 0 AND ld_Max_PageNumber <> 0
Return li_rtn






end function

public function integer nf_utlu25ar_getfaxing_info (string as_recipient_id, ref string as_type_code, ref string as_telephone_number, ref string as_cover_form, ref string as_recipient_name, ref string as_recipient_attn);Int	li_rtn

String ls_app_id

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

s_error	lstr_error_info
ls_App_id								 = Message.nf_Get_App_ID()
lstr_error_info.se_app_name       = ls_App_id
lstr_error_info.se_user_id        = sqlca.userid
lstr_error_info.se_Message        = Space(70)
lstr_error_info.se_procedure_name = 'utlu25ar'

nf_get_s_error_values ( &
		lstr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )


as_Telephone_Number 	= 	Space(35)
as_Telephone_Number 	= 	Fill(CHAR(0),35)
as_recipient_name		=	Space(31)
as_recipient_name		=	Fill(CHAR(0),31)
as_recipient_attn		=	Space(31)
as_recipient_attn		=	Fill(CHAR(0),31)
//
//li_rtn = utlu25ar(ls_app_name, &
//						ls_window_name,&
//						ls_function_name,&
//						ls_event_name, &
//						ls_procedure_name, &
//						ls_user_id, &
//						ls_return_code, &
//						ls_message, &
//						as_Recipient_id, &
//						ls_App_id, &
//						as_Type_Code, &
//						as_Telephone_Number, &
//						as_Cover_Form, &
//						as_Recipient_name, &
//    					as_Recipient_attn, &
//		 				ii_utl001_commhandle)
//						 



nf_set_s_error ( lstr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
					
sqlca.nf_display_message(li_rtn, lstr_error_info, ii_utl001_commhandle)
as_cover_form	=	Trim(as_cover_form)

RETURN li_rtn






end function

public function integer nf_utlu27ar_get_addresses (ref datastore ads_datastore);Int	li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_Address_Data_String, &
			ls_Refetch_Address_Type, &
			ls_Refetch_Address_Code


s_error	lstr_error_info

lstr_error_info.se_app_name       = Message.nf_Get_App_ID()
lstr_error_info.se_user_id        = sqlca.userid
lstr_error_info.se_Message        = Space(70)
lstr_error_info.se_procedure_name = 'utlu27ar_load_Addresses'

nf_get_s_error_values ( &
		lstr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

    ls_Address_Data_String = Space(60000)
    ls_Refetch_Address_Type = Space(8)
    ls_Refetch_Address_Code = Space(10)





Do 
//li_rtn  = utlu27ar_Load_Address( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//    ls_Address_Data_String, &
//    ls_Refetch_Address_Type, &
//    ls_Refetch_Address_Code, &
//    ii_utl001_commhandle) 

	nf_set_s_error ( lstr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
	
	if li_rtn < 1 then EXIT
	sqlca.nf_display_message(li_rtn, lstr_error_info, ii_utl001_commhandle) 
	li_rtn  = ads_DataStore.ImportString( ls_Address_Data_String)
Loop While Len(Trim(ls_Refetch_address_type)) > 0 AND Len(Trim(ls_Refetch_Address_Code)) > 0 

Return ads_DataStore.RowCount()






end function

public function integer nf_utlu28ar_load_tskuplt (ref datastore ads_datastore);Int	li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message,&
			ls_product_Data_String, &
			ls_Refetch_product_code, &
			ls_Refetch_plant_code


s_error	lstr_error_info

lstr_error_info.se_app_name       = Message.nf_Get_App_ID()
lstr_error_info.se_user_id        = sqlca.userid
lstr_error_info.se_Message        = Space(70)
lstr_error_info.se_procedure_name = 'utlu28ar_Load_Tskuplt'

nf_get_s_error_values ( &
		lstr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

    ls_product_data_String = Space(60000)
    ls_Refetch_product_code = Space(10)
    ls_Refetch_plant_code = Space(3)


Do 
//li_rtn  = utlu28ar_Load_Tskuplt( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//    ls_Product_Data_String, &
//    ls_Refetch_Product_code, &
//    ls_Refetch_Plant_Code, &
//    ii_utl001_commhandle) 

	nf_set_s_error ( lstr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )
	
	if li_rtn < 1 then EXIT
	sqlca.nf_display_message(li_rtn, lstr_error_info, ii_utl001_commhandle) 
	li_rtn  = ads_DataStore.ImportString( ls_Product_Data_String)
Loop While Len(Trim(ls_Refetch_Product_Code)) > 0 AND Len(Trim(ls_Refetch_plant_code)) > 0 

Return ads_DataStore.RowCount()

end function

public function integer nf_utlu05ar (ref s_error astr_error_info, ref string as_sales_people_string, ref integer ai_sales_people_count, ref string as_smancode, ref string as_smantype);Int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


SetPointer(HourGlass!)
astr_error_info.se_app_name       = Message.nf_Get_App_ID()
astr_error_info.se_user_id        = sqlca.userid
astr_error_info.se_Message        = Space(70)
astr_error_info.se_procedure_name = 'utlu05ar_get_sale_people'

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
//li_rtn = utlu05ar_get_sale_people(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_sales_people_string, &
//					ai_sales_people_count, &
//					as_smancode, &
//					as_smantype, &
//					ii_utl001_commhandle)
//			
			
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 


// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
sqlca.nf_display_message(li_rtn, astr_error_info, ii_utl001_commhandle)


Return li_rtn
end function

event constructor;call super::constructor;// Get the CommHandle to be used for windows

gw_netwise_frame.SetMicroHelp( "Opening MainFrame Connections")

ii_utl001_commhandle = sqlca.nf_getcommhandle("utl001")

gw_netwise_frame.SetMicroHelp( "Ready")

IF ii_utl001_commhandle < 0 Then
   message.ReturnValue = -1
END IF
GetEnvironment(ienv_environment)

end event

on u_utl001.create
call super::create
end on

on u_utl001.destroy
call super::destroy
end on

