HA$PBExportHeader$u_orp001.sru
$PBExportComments$User Object for orp001.dll
forward
global type u_orp001 from u_netwise_transaction
end type
end forward

global type u_orp001 from u_netwise_transaction
end type
global u_orp001 u_orp001

type prototypes
// PowerBuilder Script File: c:\ibp\orp201\orp201.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Thu Jun 16 11:13:26 2016
// Source Interface File: c:\ibp\orp201\orp201.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: orpo02cr_reservation_item_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo02cr_reservation_item_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo02cr_reservation_item_inq;Ansi"


//
// Declaration for procedure: orpo03cr_reservation_item_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo03cr_reservation_item_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo03cr_reservation_item_upd;Ansi"


//
// Declaration for procedure: orpo04cr_prod_on_res_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo04cr_prod_on_res_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo04cr_prod_on_res_inq;Ansi"


//
// Declaration for procedure: orpo05br_complete_order
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo05br_complete_order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char order_id[5], &
    char load_status, &
    ref char order_status, &
    ref char weight_error_flag, &
    ref char send_to_sched_ind, &
    char weight_override, &
    char net_order_flag, &
    ref string detail_data, &
    ref int shtg_line_count, &
    int CommHnd &
) library "orp201.dll" alias for "orpo05br_complete_order;Ansi"


//
// Declaration for procedure: orpo05cr_inq_upd_deliv_sched
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo05cr_inq_upd_deliv_sched( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo05cr_inq_upd_deliv_sched;Ansi"


//
// Declaration for procedure: orpo06br_edi_customer_list
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo06br_edi_customer_list( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo06br_edi_customer_list;Ansi"


//
// Declaration for procedure: orpo06cr_sales_prod_sub_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo06cr_sales_prod_sub_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo06cr_sales_prod_sub_inq;Ansi"


//
// Declaration for procedure: orpo07br_edi_cust_prod_det_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo07br_edi_cust_prod_det_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo07br_edi_cust_prod_det_inq;Ansi"


//
// Declaration for procedure: orpo07cr_sales_prod_sub_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo07cr_sales_prod_sub_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo07cr_sales_prod_sub_upd;Ansi"


//
// Declaration for procedure: orpo08br_edi_cust_prod_det_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo08br_edi_cust_prod_det_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    string header_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo08br_edi_cust_prod_det_upd;Ansi"


//
// Declaration for procedure: orpo09br_edi_cust_prod_exc_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo09br_edi_cust_prod_exc_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo09br_edi_cust_prod_exc_inq;Ansi"


//
// Declaration for procedure: orpo10br_edi_cust_prod_exc_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo10br_edi_cust_prod_exc_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo10br_edi_cust_prod_exc_upd;Ansi"


//
// Declaration for procedure: orpo11br_print_edi_reports
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo11br_print_edi_reports( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo11br_print_edi_reports;Ansi"


//
// Declaration for procedure: orpo12br_edi_cust_prod_inact
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo12br_edi_cust_prod_inact( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo12br_edi_cust_prod_inact;Ansi"


//
// Declaration for procedure: orpo13br_review_edi_orders_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo13br_review_edi_orders_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo13br_review_edi_orders_inq;Ansi"


//
// Declaration for procedure: orpo13cr_linked_orders_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo13cr_linked_orders_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo13cr_linked_orders_inq;Ansi"


//
// Declaration for procedure: orpo14br_review_edi_orders_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo14br_review_edi_orders_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo14br_review_edi_orders_upd;Ansi"


//
// Declaration for procedure: orpo14cr_linked_orders_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo14cr_linked_orders_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo14cr_linked_orders_upd;Ansi"


//
// Declaration for procedure: orpo15br_retrieve_email_address
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo15br_retrieve_email_address( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo15br_retrieve_email_address;Ansi"


//
// Declaration for procedure: orpo18br_calc_ship_date
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo18br_calc_ship_date( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string as_required_info, &
    ref string as_ship_date, &
    int CommHnd &
) library "orp201.dll" alias for "orpo18br_calc_ship_date;Ansi"


//
// Declaration for procedure: orpo19br_inquire_pallet_defaults
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo19br_inquire_pallet_defaults( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_String, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo19br_inquire_pallet_defaults;Ansi"


//
// Declaration for procedure: orpo20br_update_pallet_defaults
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo20br_update_pallet_defaults( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    string output_string_in, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo20br_update_pallet_defaults;Ansi"


//
// Declaration for procedure: orpo22br_move_rows_existing_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo22br_move_rows_existing_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string to_sales_Id, &
    string from_sales_id, &
    string detail_data_in, &
    ref string detail_data_out, &
    int CommHnd &
) library "orp201.dll" alias for "orpo22br_move_rows_existing_inq;Ansi"


//
// Declaration for procedure: orpo25br_cust_temp_settings_inq
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo25br_cust_temp_settings_inq( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo25br_cust_temp_settings_inq;Ansi"


//
// Declaration for procedure: orpo26br_cust_temp_settings_upd
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo26br_cust_temp_settings_upd( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    string header_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo26br_cust_temp_settings_upd;Ansi"


//
// Declaration for procedure: orpo50br_move_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo50br_move_product( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string To_sales_Id, &
    string to_plant_code, &
    string to_ship_date, &
    string se_order_num_from, &
    string detail_data_in, &
    string additional_data, &
    string shortage_string_in, &
    ref string shortage_string_out, &
    string dup_product_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo50br_move_product;Ansi"


//
// Declaration for procedure: orpo51br_res_brws_product
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo51br_res_brws_product( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    string product_info_in, &
    ref string header_info_out, &
    ref string detail_info_out, &
    ref double task_number, &
    ref int page_number, &
    char mode_indicator, &
    int CommHnd &
) library "orp201.dll" alias for "orpo51br_res_brws_product;Ansi"


//
// Declaration for procedure: orpo52br_mngr_parameters
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo52br_mngr_parameters( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    char mode_indicator, &
    string parameter_info_in, &
    ref string parameter_info_out, &
    int CommHnd &
) library "orp201.dll" alias for "orpo52br_mngr_parameters;Ansi"


//
// Declaration for procedure: orpo53br_delete_sales_notify
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo53br_delete_sales_notify( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string delete_detail_info, &
    int CommHnd &
) library "orp201.dll" alias for "orpo53br_delete_sales_notify;Ansi"


//
// Declaration for procedure: orpo54br_Product_Browse_Update
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo54br_Product_Browse_Update( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string customer_info_in, &
    string main_info_in_1, &
    string main_info_in_2, &
    string main_info_in_3, &
    ref string main_info_out_1, &
    ref string main_info_out_2, &
    ref string main_info_out_3, &
    ref string dup_order_string, &
    string dup_product_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo54br_Product_Browse_Update;Ansi"


//
// Declaration for procedure: orpo55br
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo55br( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string sales_ids, &
    ref double task_number, &
    ref int page_number, &
    ref string detail_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo55br;Ansi"


//
// Declaration for procedure: orpo56br_get_so_status
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo56br_get_so_status( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string sales_info_in, &
    ref string sales_info_out, &
    int CommHnd &
) library "orp201.dll" alias for "orpo56br_get_so_status;Ansi"


//
// Declaration for procedure: orpo57br_reservation
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo57br_reservation( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_info_in, &
    string detail_info_in, &
    ref string header_info_out, &
    ref string detail_info_out, &
    ref string sales_info_out, &
    ref double task_number, &
    ref int page_number, &
    string roll_info, &
    int CommHnd &
) library "orp201.dll" alias for "orpo57br_reservation;Ansi"


//
// Declaration for procedure: Orpo58br_res_util_tsr_cust
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int Orpo58br_res_util_tsr_cust( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Tsk_number, &
    ref int page_number, &
    string Input_info, &
    ref string OutPut_Info, &
    int CommHnd &
) library "orp201.dll" alias for "Orpo58br_res_util_tsr_cust;Ansi"


//
// Declaration for procedure: orpo59br_resutil_custres
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo59br_resutil_custres( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Tsk_number, &
    ref int page_number, &
    string Input_info, &
    ref string customer_id, &
    ref string OutPut_Info, &
    int CommHnd &
) library "orp201.dll" alias for "orpo59br_resutil_custres;Ansi"


//
// Declaration for procedure: orpo60br_resutil_resproduct
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo60br_resutil_resproduct( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref double Task_number, &
    ref double last_record_number, &
    ref double Max_record_number, &
    string Input_info, &
    ref string customer_id, &
    ref string OutPut_Info, &
    int CommHnd &
) library "orp201.dll" alias for "orpo60br_resutil_resproduct;Ansi"


//
// Declaration for procedure: Orpo61br_Weekly_res_Util
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int Orpo61br_Weekly_res_Util( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_string, &
    ref string Output_String, &
    ref string header_out, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "Orpo61br_Weekly_res_Util;Ansi"


//
// Declaration for procedure: orpo62br_cr_verify
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo62br_cr_verify( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string detail_info_in, &
    ref string detail_info_out, &
    char action_indicator, &
    int CommHnd &
) library "orp201.dll" alias for "orpo62br_cr_verify;Ansi"


//
// Declaration for procedure: orpo63br_Cancel_Sales_Order
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo63br_Cancel_Sales_Order( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string order_id, &
    int CommHnd &
) library "orp201.dll" alias for "orpo63br_Cancel_Sales_Order;Ansi"


//
// Declaration for procedure: Orpo64br_Servc_ProductRes
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int Orpo64br_Servc_ProductRes( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_string, &
    ref string Output_String, &
    ref string header_out, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "Orpo64br_Servc_ProductRes;Ansi"


//
// Declaration for procedure: Orpo65br_Get_SalesLocations
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int Orpo65br_Get_SalesLocations( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string Sales_Location_and_Names, &
    int CommHnd &
) library "orp201.dll" alias for "Orpo65br_Get_SalesLocations;Ansi"


//
// Declaration for procedure: orpo66br_inq_product_sub
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo66br_inq_product_sub( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    int CommHnd &
) library "orp201.dll" alias for "orpo66br_inq_product_sub;Ansi"


//
// Declaration for procedure: orpo67br_resutl_tsrres
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int orpo67br_resutl_tsrres( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string Input_string, &
    ref string Output_String, &
    ref string header_out, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "orp201.dll" alias for "orpo67br_resutl_tsrres;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "orp201.dll"
function int WBorp201CkCompleted( int CommHnd ) &
    library "orp201.dll"
//
// ***********************************************************


end prototypes

type variables
Int ii_commhandle
end variables

forward prototypes
public function integer nf_orpo18ar (ref s_error astr_error, ref string as_required_info, ref string as_ship_date)
public function integer nf_orpo56ar (ref s_error astr_error_info, ref string as_sales_info)
public function integer nf_orpo62ar (ref s_error astr_error_info, ref string as_detail_info, character ac_action_ind)
public function integer nf_orpo63ar_cancel_sales_order (ref s_error astr_error_info, string as_order_id)
public function integer nf_orpo66ar_getsubs (ref s_error astr_error_info, string as_product_code, ref string as_output)
public function integer nf_orpo65ar_get_servicecenters (s_error astr_error_info, ref string as_output_info)
public function integer nf_orpo67ar_sc_resutl_tsrres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number)
public function integer nf_orpo60ar_resutil_resproduct (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number)
public function integer nf_orpo61ar_sc_resutl_weekres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_week_out, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number)
public function integer nf_orpo58ar_resutil_tsrcust (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref integer ai_page_number, ref double ad_task_number)
public function integer nf_orpo55ar_late_note (ref s_error astr_error_info, string as_input, ref double ad_tasknumber, ref integer ai_pagenumber, ref string as_output)
public function integer nf_orpo59ar_resutil_custres (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info, ref integer ai_page_number, ref double ad_task_number)
public function integer nf_orpo64ar_sc_resutl_scres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out, ref double ad_task_number, ref double ad_last_number, ref double ad_max_number)
public function integer nf_orpo52ar (ref s_error astr_error_info, character ac_mode_indicator, ref string as_parameter_info)
public function integer nf_orpo53ar_delete_sales_notify (ref s_error astr_error_info, string product_string_in)
public function integer nf_orpo51ar (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref double ad_task_number, ref integer ai_page_number, character ac_mode_indicator)
public function integer nf_orpo57ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_sales_info, ref double ad_task_number, ref integer ai_page_number, string as_roll_info)
public function integer nf_orpo05ar (ref s_error astr_error_info, ref character ac_order_id[5], ref character ac_load_status, ref character ac_order_status, ref character ac_weight_error_flag, ref character ac_weight_override, ref character ac_net_order_flag, ref string as_detail_data, ref integer ai_shtg_line_count, ref character ac_send_to_sched_ind)
public function integer nf_orpo19br (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo20br (ref s_error astr_error_info, string as_input_string, string as_output_string_in)
public function integer nf_orpo15br (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo54ar_product_browse_update (ref s_error astr_error_info, ref string as_customer_info_in, ref string as_main_info_in_1, ref string as_main_info_in_2, ref string as_main_info_in_3, ref string as_main_info_out_1, ref string as_main_info_out_2, ref string as_main_info_out_3, ref string as_dup_orders, string as_dup_product_string)
public function integer nf_orpo22br_check_duplicates (s_error astr_error_info, string as_to_id, string as_from_id, string as_detail_in, ref string as_detail_out)
public function integer nf_orpo02cr_inq_reservatiion_item (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo03cr_upd_reservation_item (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo50ar_move_product (s_error astr_error_info, string as_from_id, ref string as_to_id, string as_to_plant, string as_to_ship_date, string as_detail, string as_additional_data, string as_shortage_string_in, ref string as_shortage_string_out, string as_dup_product_string)
public function integer nf_orpo04cr_prod_on_res_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo05cr_inq_upd_ltl_sched (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo07cr_sales_prod_sub_upd (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo06cr_sales_prod_sub_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo06br_inq_edi_customer (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_orpo11br_init_edi_prod_rpts (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo07br_edi_cust_prod_det_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo08br_edi_cust_prod_det_upd (ref s_error astr_error_info, string as_input_string, ref string as_output_string, string as_header_string)
public function integer nf_orpo12br_edi_cust_prod_inact (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo25br_cust_temp_settings_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo26br_cust_temp_settings_upd (ref s_error astr_error_info, string as_input_string, string as_header_string, ref string as_output_string)
public function integer nf_orpo14br_review_edi_orders_upd (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo13br_review_edi_orders_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo09br_edi_cust_prod_exc_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo10br_edi_cust_prod_exc_upd (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo13cr_linked_orders_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo14cr_linked_orders_upd (ref s_error astr_error_info, string as_input_string)
end prototypes

public function integer nf_orpo18ar (ref s_error astr_error, ref string as_required_info, ref string as_ship_date);INTEGER 	li_Rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


SetPointer(HourGlass!)
gw_netwise_Frame.SetMicroHelp("Wait.. Calculating ship date.")

as_ship_date = SPACE(11)
as_Ship_date = Fill(Char(0),11)

nf_get_s_error_values ( &
		astr_error, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
//
//li_Rtn = orpo18br_calc_ship_date(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_required_info, &
//					as_ship_date, &
//					ii_commhandle)

nf_set_s_error ( astr_error,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
This.nf_display_message(li_rtn, astr_error, ii_commhandle)

RETURN li_Rtn
end function

public function integer nf_orpo56ar (ref s_error astr_error_info, ref string as_sales_info);int 		li_rtn
String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


STRING	ls_sales_info_out

ls_sales_info_out = SPACE(8615)
ls_sales_info_out = FILL(CHAR(0), 8615)

gw_netwise_frame.SetMicroHelp( "Wait... Refreshing sales order information.")
SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =orpo56br_get_so_status( 	ls_app_name, &
//											ls_window_name, &
//											ls_function_name, &
//											ls_event_name, &
//											ls_procedure_name, &
//											ls_user_id,&
//											ls_return_code,&
//											ls_message, &
//    										as_sales_info, &
//    										ls_sales_info_out, &
//	    									ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)

as_sales_info = ls_sales_info_out

return li_rtn
end function

public function integer nf_orpo62ar (ref s_error astr_error_info, ref string as_detail_info, character ac_action_ind);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


STRING	ls_detail_info_out

ls_detail_info_out = SPACE(20000)
ls_detail_info_out = FILL(CHAR(0), 20000)

gw_netwise_frame.SetMicroHelp( "Wait... Validating reference number.")
SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the customer information
//
//li_rtn = orpo62br_cr_verify( 	ls_app_name, &
//										ls_window_name, &
//										ls_function_name, &
//										ls_event_name, &
//										ls_procedure_name, &
//										ls_user_id,&
//										ls_return_code,&
//										ls_message, &
//    									as_detail_info, &
//    									ls_detail_info_out, &
//										ac_action_ind, &
//	    								ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)

as_detail_info = ls_detail_info_out

return li_rtn
end function

public function integer nf_orpo63ar_cancel_sales_order (ref s_error astr_error_info, string as_order_id);Int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


SetPointer(HourGlass!)
nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
//
//LI_RTN = orpo63br_Cancel_Sales_Order( &
//    ls_app_name, &
//	ls_window_name,&
//	ls_function_name,&
//	ls_event_name, &
//	ls_procedure_name, &
//	ls_user_id, &
//	ls_return_code, &
//	ls_message, &
//    as_Order_id, &
//    ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
return li_rtn
end function

public function integer nf_orpo66ar_getsubs (ref s_error astr_error_info, string as_product_code, ref string as_output);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_output = Space(2501)
as_output = Fill(CHAR(0), 2501)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information

//li_rtn = orpo66br_inq_product_sub( &
//			ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//			as_product_code, &
//			as_output, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)


return li_rtn
end function

public function integer nf_orpo65ar_get_servicecenters (s_error astr_error_info, ref string as_output_info);int li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_OutPut_Info = Space(3101)
as_OutPut_Info = Fill(Char(0),3101)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the customer information

//li_rtn = Orpo65br_Get_SalesLocations( &
//			ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//			as_output_info, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo67ar_sc_resutl_tsrres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_Input_info = Trim( as_Input_info)
as_OutPut_Info = Space(20000)
as_OutPut_Info = Fill(Char(0),20000)
as_header_out = Space(140)
as_header_out = Fill(Char(0),140)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the customer information

//li_rtn = Orpo67br_resutl_tsrres( &
//			ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//			as_input_info, &
//			as_output_info, &
//			as_header_out, &
//    		ad_task_number, &
//    		ad_last_record_number, &
//    		ad_max_record_number, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo60ar_resutil_resproduct (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_Input_info = Trim( as_Input_info)
as_customer_id = Space(8)
as_customer_id = Fill(Char(0),8)
as_OutPut_Info = Space(20000)
as_OutPut_Info = Fill(Char(0),20000)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information

//li_rtn =orpo60br_resutil_resproduct( &
//			ls_app_name, &
//			ls_window_name, &
//			ls_function_name, &
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id,&
//			ls_return_code,&
//			ls_message, &
//			ad_Task_Number, &
//			ad_Last_Record_Number, &
//			ad_Max_Record_Number, &
//			as_input_info, &
//			as_customer_id, &
//			as_output_info, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo61ar_sc_resutl_weekres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_week_out, ref double ad_task_number, ref double ad_last_record_number, ref double ad_max_record_number);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_Input_info = Trim( as_Input_info)
as_OutPut_Info = Space(20000)
as_OutPut_Info = Fill(Char(0),20000)
as_week_out = Space(125)
as_week_out = Fill(Char(0),125)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information

//li_rtn = Orpo61br_weekly_res_util( &
//			ls_app_name, &
//			ls_window_name,&
//			ls_function_name,&
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id, &
//			ls_return_code, &
//			ls_message, &
//			as_input_info, &
//			as_output_info, &
//			as_week_out, &
//			ad_task_number, &
//			ad_last_record_number, &
//			ad_max_record_number, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo58ar_resutil_tsrcust (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref integer ai_page_number, ref double ad_task_number);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_Input_info = Trim( as_Input_info)
as_OutPut_Info = Space(20000)
as_OutPut_Info = Fill(Char(0),20000)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information
//
//li_rtn = Orpo58br_res_util_tsr_cust( &
//			ls_app_name, &
//			ls_window_name, &
//			ls_function_name, &
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id,&
//			ls_return_code,&
//			ls_message, &
//			ad_Task_Number, &
//			ai_Page_number, &
//			as_input_info, &
//			as_output_info, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo55ar_late_note (ref s_error astr_error_info, string as_input, ref double ad_tasknumber, ref integer ai_pagenumber, ref string as_output);Int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


SetPointer(HourGlass!)

as_output = Space(18499)
as_output = Fill(Char(0),18499)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//LI_RTN = orpo55br( &
//    ls_app_name, &
//	 ls_window_name,&
//	ls_function_name,&
//	ls_event_name, &
//	ls_procedure_name, &
//	ls_user_id, &
//	ls_return_code, &
//	ls_message, &
//    as_input, &
//    ad_tasknumber, &
//    ai_pagenumber, &
//    as_output, &
//   ii_commhandle) 

   
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
return li_rtn
end function

public function integer nf_orpo59ar_resutil_custres (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info, ref integer ai_page_number, ref double ad_task_number);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message



as_Input_info = Trim( as_Input_info)
as_customer_id = Space(8)
as_customer_id = Fill(Char(0),8)
as_OutPut_Info = Space(20000)
as_OutPut_Info = Fill(Char(0),20000)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information

//li_rtn = orpo59br_resutil_custres( &
//			ls_app_name, &
//			ls_window_name,&
//			ls_function_name,&
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id, &
//			ls_return_code, &
//			ls_message, &
//			ad_Task_Number, &
//			ai_Page_number, &
//			as_input_info, &
//			as_customer_id, &
//			as_output_info, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo64ar_sc_resutl_scres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out, ref double ad_task_number, ref double ad_last_number, ref double ad_max_number);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_Input_info = Trim( as_Input_info)
as_OutPut_Info = Space(20000)
as_OutPut_Info = Fill(Char(0),20000)
as_header_out = Space(88)
as_header_out = Fill(Char(0),88)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information

//li_rtn = Orpo64br_Servc_ProductRes( &
//			ls_app_name, &
//			ls_window_name, &
//			ls_function_name, &
//			ls_event_name, &
//			ls_procedure_name, &
//			ls_user_id,&
//			ls_return_code,&
//			ls_message, &
//			as_input_info, &
//			as_output_info, &
//			as_header_out, &
//			ad_task_number, &
//			ad_last_number, &
//			ad_max_number, &
//			ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info,  ii_commhandle)



return li_rtn
end function

public function integer nf_orpo52ar (ref s_error astr_error_info, character ac_mode_indicator, ref string as_parameter_info);Int  li_rtn
STRING	ls_parameter_info_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


gw_netwise_Frame.SetMicroHelp("Wait.. PC communicating with mainframe.")
SetPointer(HourGlass!)

ls_parameter_info_out = SPACE(100)
ls_parameter_info_out = Fill(char(0),100)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_Rtn =  orpo52br_mngr_parameters( &
//   ls_app_name, &
//	ls_window_name, &
//	ls_function_name, &
//	ls_event_name, &
//	ls_procedure_name, &
//	ls_user_id,&
//	ls_return_code,&
//	ls_message, &
//   ac_mode_indicator, &
//   as_parameter_info, &
//   ls_parameter_info_out, &
//   ii_commhandle )

	as_parameter_info = ls_parameter_info_out

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

Return li_rtn
end function

public function integer nf_orpo53ar_delete_sales_notify (ref s_error astr_error_info, string product_string_in);int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


product_string_in = Trim( product_string_in)

gw_netwise_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)
// Call a Netwise external function to get the customer information
nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
//
//li_rtn =orpo53br_delete_sales_notify( &
//    	ls_app_name, &
//		ls_window_name,&
//		ls_function_name,&
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id, &
//		ls_return_code, &
//		ls_message, &
//	product_string_in,&
//	ii_commhandle )


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info ,  ii_commhandle)


return li_rtn
end function

public function integer nf_orpo51ar (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref double ad_task_number, ref integer ai_page_number, character ac_mode_indicator);Int  li_rtn

STRING	ls_header_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_detail_out

SetPointer(HourGlass!)
ls_header_out = SPACE(700)
ls_header_out = Fill(char(0),700)

ls_detail_out = SPACE(60000)
ls_detail_out = Fill(char(0),60000)
 
gw_netwise_Frame.SetMicroHelp("Wait.. PC communicating with mainframe.")
SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
//
//li_Rtn = orpo51br_res_brws_product( &
//	ls_app_name, &
//	ls_window_name,&
//	ls_function_name,&
//	ls_event_name, &
//	ls_procedure_name, &
//	ls_user_id, &
//	ls_return_code, &
//	ls_message, &
//	as_header_in, &
//   as_detail_in, &
//	ls_header_out, &
//	ls_detail_out, &
//   ad_task_number, &
//   ai_page_number, &
//   ac_mode_indicator, &
//   ii_commhandle)

	as_header_in = ls_header_out
	as_detail_in = ls_detail_out
	
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

Return li_rtn
end function

public function integer nf_orpo57ar (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_sales_info, ref double ad_task_number, ref integer ai_page_number, string as_roll_info);int li_rtn
String 	ls_header_string_in, &
			ls_header_string_out,&
			ls_detail_string_in, &
			ls_detail_string_out, &
			ls_so_id_string_out, &
			ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_roll_info


SetPointer(HourGlass!)
gw_netwise_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")

ls_header_string_in = as_header_info
ls_detail_string_in = as_detail_info
ls_roll_info = as_roll_info

ls_header_string_out = Space(100)
ls_so_id_string_out = Space(60000)
ls_detail_string_out = Space(23000)
ls_header_string_out = Fill(CHAR(0),100)
ls_so_id_string_out = Fill(CHAR(0),60000)
ls_detail_string_out = Fill(CHAR(0),23000)


nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

// Call a Netwise external function to get the required information
//li_rtn  = orpo57br_reservation( ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_header_string_in,  &
//			 		ls_detail_string_in, &
//					 ls_header_string_out, &
//					 ls_detail_string_out,&
//			 		ls_so_id_string_out, &
//			 		ad_task_number, &
//			 		ai_page_number, &
//					ls_roll_info, &
//			 		ii_commhandle)

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
IF Trim(ls_Header_String_out) = "" Then li_rtn = -1
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

as_header_info = Trim(ls_header_string_out )
as_detail_info = Trim(ls_detail_string_out)
as_sales_info  = Trim(ls_so_id_string_out)

return li_rtn
end function

public function integer nf_orpo05ar (ref s_error astr_error_info, ref character ac_order_id[5], ref character ac_load_status, ref character ac_order_status, ref character ac_weight_error_flag, ref character ac_weight_override, ref character ac_net_order_flag, ref string as_detail_data, ref integer ai_shtg_line_count, ref character ac_send_to_sched_ind);Int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message, &
			ls_detail_data

SetPointer(HourGlass!)

as_detail_data = TRIM(as_detail_data) + Space(5500 - Len(Trim(as_Detail_Data)))

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//LI_RTN = orpo05br_complete_order(ls_app_name, &
//	ls_window_name, &
//	ls_function_name, &
//	ls_event_name, &
//	ls_procedure_name, &
//	ls_user_id,&
//	ls_return_code,&
//	ls_message,&
//   ac_order_id, &
//   ac_load_status, &
//   ac_order_Status, &
//   ac_weight_error_Flag, &
//	ac_send_to_sched_ind, &
//   ac_weight_Override, &
//   ac_net_order_flag,&
//	as_detail_data, &
//	ai_shtg_line_count, &
//	ii_commhandle)
	
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
return li_rtn
end function

public function integer nf_orpo19br (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message, &
			input_string, &
			output_string
			
			
Double	ld_task_number, &
			ld_last_number, &
			ld_max_number


SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "orpo19br_inquire_pallet_defaults"
astr_error_info.se_message = space(71)

as_output_string = ""

ld_task_number = 0
ld_last_number = 0
ld_max_number = 0

output_string = SPACE(20000)
output_string = Fill(Char(0), 20000)
input_string = Trim( as_Input_String)

astr_error_info.se_procedure_name = "nf_orpo19br"

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

Do
//    li_rtn = orpo19br_inquire_pallet_defaults( &
//    ls_app_name, &
//    ls_window_name, &
//    ls_function_name, &
//    ls_event_name, &
//    ls_procedure_name, &
//    ls_user_id, &
//    ls_return_code, &
//    ls_message, &
//    input_string, &
//    output_String, &
//    ld_task_number, &
//    ld_last_number, &
//    ld_max_number, &
//    ii_commhandle)

as_output_string += output_string

Loop While ld_last_number <> ld_max_number and li_rtn >= 0

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

Return li_rtn


end function

public function integer nf_orpo20br (ref s_error astr_error_info, string as_input_string, string as_output_string_in);Int 		li_rtn

String 	ls_procedure_name, &
         se_app_name, &
         se_window_name, &
         se_function_name, &
         se_event_name, &
         se_procedure_name, &
         se_user_id, &
         se_return_code, &
         se_message

double   task_number, &
         last_record_number, &
         max_record_number
			
SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "orpo20br_update_pallet_defaults"
astr_error_info.se_message = space(71)

task_number = 0
last_record_number = 0
max_record_number = 0

nf_get_s_error_values ( &
		astr_error_info, &
		se_app_name, &
		se_window_name, &
		se_function_name, &
		se_event_name, &
		se_procedure_name, &
		se_user_id,&
		se_return_code,&
		se_message )

//li_rtn = orpo20br_update_pallet_defaults( &
//	 se_app_name, &
//    se_window_name, &
//    se_function_name, &
//    se_event_name, &
//    se_procedure_name, &
//    se_user_id, &
//    se_return_code, &
//    se_message, &
//	 as_input_string, &
//	 as_output_string_in, &
//    task_number, &
//    last_record_number, &
//    max_record_number, &
//    ii_CommHandle) 

ls_procedure_name = astr_error_info.se_procedure_name

nf_set_s_error(astr_error_info,&
					se_app_name, &
					se_window_name,&
					se_function_name,&
					se_event_name, &
					se_procedure_name, &
					se_user_id, &
					se_return_code, &
					se_message)		

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

Return li_rtn
	 
end function

public function integer nf_orpo15br (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int 		li_rtn

String 	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id, &
			ls_return_code, &
			ls_message
			
SetPointer(HourGlass!)

astr_error_info.se_procedure_name = "orpo15br_retrieve_email_address"
astr_error_info.se_message = space(71)

as_output_string = ""
as_output_string = SPACE(10000)
as_output_string = Fill(Char(0), 10000)

astr_error_info.se_procedure_name = "nf_orpo15br"

nf_get_s_error_values ( astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id, &
								ls_return_code, &
								ls_message)

//li_rtn = orpo15br_retrieve_email_address( &
//ls_app_name, &
//ls_window_name, &
//ls_function_name, &
//ls_event_name, &
//ls_procedure_name, &
//ls_user_id, &
//ls_return_code, &
//ls_message, &
//as_input_string, &
//as_output_String, &
//ii_commhandle)

nf_set_s_error(astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message)				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

return li_rtn
end function

public function integer nf_orpo54ar_product_browse_update (ref s_error astr_error_info, ref string as_customer_info_in, ref string as_main_info_in_1, ref string as_main_info_in_2, ref string as_main_info_in_3, ref string as_main_info_out_1, ref string as_main_info_out_2, ref string as_main_info_out_3, ref string as_dup_orders, string as_dup_product_string);int li_rtn

STRING	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

as_main_info_out_1 = SPACE(27000)
as_main_info_out_1 = FILL(CHAR(0), 27000)
as_main_info_out_2 = SPACE(60000)
as_main_info_out_2 = FILL(CHAR(0), 60000)
as_main_info_out_3 = SPACE(50000)
as_main_info_out_3 = FILL(CHAR(0), 50000)
as_dup_orders = SPACE(100)
as_dup_orders = FILL(CHAR(0), 100)

gw_netwise_frame.SetMicroHelp( "Wait...PC communicating with Mainframe")
SetPointer(HourGlass!)

astr_error_info.se_procedure_name = 'orpo54ar_product_browse_update'
astr_error_info.se_message = Space(70)

// Call a Netwise external function to get the customer information
nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =orpo54br_product_browse_update( &
//  	ls_app_name, &
//	ls_window_name, &
//	ls_function_name, &
//	ls_event_name, &
//	ls_procedure_name, &
//	ls_user_id,&
//	ls_return_code,&
//	ls_message, &
//	as_customer_info_in, &
//  	as_main_info_in_1, &
//	as_main_info_in_2, &
//	as_main_info_in_3, &
//	as_main_info_out_1, &
//	as_main_info_out_2, &
//	as_main_info_out_3, &
//	as_dup_orders, &
//	as_dup_product_string, &
//	ii_commhandle )

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
This.nf_display_message(li_rtn, astr_error_info ,  ii_commhandle)
//if li_rtn > 0 and li_rtn < 10 then
//	MessageBox('Message',ls_message)
//end if
return li_rtn
end function

public function integer nf_orpo22br_check_duplicates (s_error astr_error_info, string as_to_id, string as_from_id, string as_detail_in, ref string as_detail_out);Int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message


as_detail_out = Space(35000)
as_detail_out = Fill(Char(0),35000)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =  orpo22br_move_rows_existing_inq( &
//		ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//		as_to_id, &
//		as_from_id, &
//		as_detail_in, &
//		as_detail_out, &
//		ii_commhandle )

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

Return li_rtn
end function

public function integer nf_orpo02cr_inq_reservatiion_item (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo02cr_reservation_item_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo02cr_reservation_item_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo03cr_upd_reservation_item (ref s_error astr_error_info, string as_input_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
astr_error_info.se_procedure_name = "orpo03cr_upd_reservation_item"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

//li_rtn = orpo03cr_reservation_item_upd(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ii_commhandle)
//	
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo50ar_move_product (s_error astr_error_info, string as_from_id, ref string as_to_id, string as_to_plant, string as_to_ship_date, string as_detail, string as_additional_data, string as_shortage_string_in, ref string as_shortage_string_out, string as_dup_product_string);Int 		li_rtn

String	ls_app_name, &
			ls_window_name, &
			ls_function_name, &
			ls_event_name, &
			ls_procedure_name, &
			ls_user_id,&
			ls_return_code,&
			ls_message

if as_to_id <= '     ' Then
	as_to_id = Space(5)
	as_to_id = Fill(Char(0),5)	
end if

as_shortage_string_out = Space(5000)
as_shortage_string_out = Fill(Char(0),5000)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//li_rtn =  orpo50br_move_product( &
//		ls_app_name, &
//		ls_window_name, &
//		ls_function_name, &
//		ls_event_name, &
//		ls_procedure_name, &
//		ls_user_id,&
//		ls_return_code,&
//		ls_message, &
//		as_to_id, &
//		as_to_plant, &
//		as_to_ship_date, & 
//		as_from_id, &
//		as_detail, &
//		as_additional_data, &
//		as_shortage_string_in, &
//		as_shortage_string_out, &
//		as_dup_product_string, &
//		ii_commhandle)
//

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	
			 
This.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

Return li_rtn
end function

public function integer nf_orpo04cr_prod_on_res_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo02cr_reservation_item_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo04cr_prod_on_res_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo05cr_inq_upd_ltl_sched (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo05cr_inq_upd_ltl_sched"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo05cr_inq_upd_deliv_sched(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo07cr_sales_prod_sub_upd (ref s_error astr_error_info, string as_input_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
astr_error_info.se_procedure_name = "orpo07cr_sales_prod_sub_upd"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
//
//Call a Netwise external function to get the required information
//
//li_rtn = orpo07cr_sales_prod_sub_upd(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ii_commhandle)
	
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
//


Return li_rtn
end function

public function integer nf_orpo06cr_sales_prod_sub_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo06cr_sales_prod_sub_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo06cr_sales_prod_sub_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo06br_inq_edi_customer (ref s_error astr_error_info, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo06br_inq_edi_customer"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo06br_edi_customer_list(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo11br_init_edi_prod_rpts (ref s_error astr_error_info, string as_input_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output


astr_error_info.se_procedure_name = "orpo11br_init_edi_prod_rpts"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
//
//li_rtn = orpo11br_print_edi_reports(ls_app_name, &
//				ls_window_name,&
//				ls_function_name,&
//				ls_event_name, &
//				ls_procedure_name, &
//				ls_user_id, &
//				ls_return_code, &
//				ls_message, &
//				as_input_string, &
//				ii_commhandle)
	
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo07br_edi_cust_prod_det_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo07br_edi_cust_prod_det_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo07br_edi_cust_prod_det_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)


Return li_rtn
end function

public function integer nf_orpo08br_edi_cust_prod_det_upd (ref s_error astr_error_info, string as_input_string, ref string as_output_string, string as_header_string);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "orpo08br_edi_cust_prod_det_upd"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()
//
//li_rtn = orpo08br_edi_cust_prod_det_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								as_output_string, &
//								as_header_string, &
//								ii_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
//


Return li_rtn

end function

public function integer nf_orpo12br_edi_cust_prod_inact (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output


astr_error_info.se_procedure_name = "orpo12br_edi_cust_prod_inact"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information

//li_rtn = orpo12br_edi_cust_prod_inact(ls_app_name, &
//				ls_window_name,&
//				ls_function_name,&
//				ls_event_name, &
//				ls_procedure_name, &
//				ls_user_id, &
//				ls_return_code, &
//				ls_message, &
//				as_input_string, &
//				as_output_string, &
//				ii_commhandle)
	
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo25br_cust_temp_settings_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo25br_cust_temp_settings_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do
//
//	li_rtn = orpo25br_cust_temp_settings_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)

//messagebox('orp201 li rtn',string(li_rtn))

Return li_rtn
end function

public function integer nf_orpo26br_cust_temp_settings_upd (ref s_error astr_error_info, string as_input_string, string as_header_string, ref string as_output_string);Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
astr_error_info.se_procedure_name = "orpo26br_cust_temp_settings_upd"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
//
//Call a Netwise external function to get the required information
//
//li_rtn = orpo26br_cust_temp_settings_upd(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					as_header_string, &
//					ii_commhandle)
	
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
//

as_output_string = ls_output

Return li_rtn
end function

public function integer nf_orpo14br_review_edi_orders_upd (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message

as_output_string = Space(20000)
as_output_string = Fill(char(0),20000)

astr_error_info.se_procedure_name = "orpo14br_review_edi_orders_upd"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_rtn = orpo14br_review_edi_orders_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								as_output_string, &
//								ii_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
//


Return li_rtn

end function

public function integer nf_orpo13br_review_edi_orders_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo13br_review_edi_orders_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo13br_review_edi_orders_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)


Return li_rtn
end function

public function integer nf_orpo09br_edi_cust_prod_exc_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "orpo09br_edi_cust_prod_exc_inq"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo09br_edi_cust_prod_exc_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)


Return li_rtn
end function

public function integer nf_orpo10br_edi_cust_prod_exc_upd (ref s_error astr_error_info, string as_input_string);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "orpo10br_edi_cust_prod_exc_upd"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

//li_rtn = orpo10br_edi_cust_prod_exc_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								ii_commhandle)

lt_endtime = Now()

nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
//


Return li_rtn

end function

public function integer nf_orpo13cr_linked_orders_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
					

as_output_string = ''

astr_error_info.se_procedure_name = "nf_orpo13cr_linked_orders_inq"

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )

//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do

//	li_rtn = orpo13cr_linked_orders_inq(ls_app_name, &
//					ls_window_name,&
//					ls_function_name,&
//					ls_event_name, &
//					ls_procedure_name, &
//					ls_user_id, &
//					ls_return_code, &
//					ls_message, &
//					as_input_string, &
//					ls_output, &
//					ld_task_number, &
//					ld_last_record_number, &
//					ld_max_record_number, &
//					ii_commhandle)
	as_output_string += ls_output
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)


Return li_rtn
end function

public function integer nf_orpo14cr_linked_orders_upd (ref s_error astr_error_info, string as_input_string);Int	li_rtn

Time							lt_starttime, &
								lt_endtime
								

String					ls_app_name, &
							ls_window_name, &
							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_return_code,&
							ls_message


astr_error_info.se_procedure_name = "orpo14cr_linked_orders_upd"
astr_error_info.se_message = Space(70)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()
//
//li_rtn = orpo14cr_linked_orders_upd(ls_app_name, &
//								ls_window_name, &
//								ls_function_name, &
//								ls_event_name, &
//								ls_procedure_name, &
//								ls_user_id,&
//								ls_return_code,&
//								ls_message,&
//								as_input_string, &
//								ii_commhandle)

//lt_endtime = Now()

//nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_commhandle)
//


Return li_rtn

end function

event constructor;call super::constructor;STRING ls_MicroHelp

ls_MicroHelp = gw_netwise_Frame.wf_GetMicroHelp()

gw_netwise_frame.SetMicroHelp( "Opening MainFrame Connections")

ii_commhandle = SQLCA.nf_GetCommHandle("orp001")

If ii_commhandle = 0 Then
	Message.ReturnValue = -1
End if

gw_netwise_Frame.SetMicroHelp(ls_MicroHelp)
end event

on u_orp001.create
call super::create
end on

on u_orp001.destroy
call super::destroy
end on

