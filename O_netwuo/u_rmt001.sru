HA$PBExportHeader$u_rmt001.sru
$PBExportComments$IBDKDLD Raw Material Tracking Communication Object
forward
global type u_rmt001 from u_netwise_transaction
end type
end forward

global type u_rmt001 from u_netwise_transaction
event ue_revisions ( )
end type
global u_rmt001 u_rmt001

type prototypes
// PowerBuilder Script File: c:\ibp\rmt001\rmt001.pbf
// Target Environment:  TransAccess Application/Integrator
// Script File Creation Time: Mon Apr 16 14:03:35 2001
// Source Interface File: c:\ibp\rmt001\rmt001.ntf
//
// Script File Created By:
//
//     TransAccess Application/Integrator WORKBENCH v.2.1
//
//
// To import this file into your Power Builder application
// use the Declare/Global External Functions dialog.
//
// Any user structures are shown last for reference purposes.
// You can select and delete these structures after pasting  
// into your application.  Use the *.srs files to import  
// structures into your Power Builder application.

//
// Declaration for procedure: rmtr01mr_inq_cattle_char
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr01mr_inq_cattle_char( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr01mr_inq_cattle_char;Ansi"


//
// Declaration for procedure: rmtr02mr_upd_cattle_char
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr02mr_upd_cattle_char( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr02mr_upd_cattle_char;Ansi"


//
// Declaration for procedure: rmtr03mr_inq_dddw
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr03mr_inq_dddw( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr03mr_inq_dddw;Ansi"


//
// Declaration for procedure: rmtr04mr_inq_manifest_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr04mr_inq_manifest_summary( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr04mr_inq_manifest_summary;Ansi"


//
// Declaration for procedure: rmtr05mr_upd_manifest_summary
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr05mr_upd_manifest_summary( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr05mr_upd_manifest_summary;Ansi"


//
// Declaration for procedure: rmtr06mr_inq_hot_box_inv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr06mr_inq_hot_box_inv( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr06mr_inq_hot_box_inv;Ansi"


//
// Declaration for procedure: rmtr07mr_upd_pa_cattle_char
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr07mr_upd_pa_cattle_char( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr07mr_upd_pa_cattle_char;Ansi"


//
// Declaration for procedure: rmtr08mr_inq_manifest_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr08mr_inq_manifest_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr08mr_inq_manifest_detail;Ansi"


//
// Declaration for procedure: rmtr09mr_upd_manifest_detail
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr09mr_upd_manifest_detail( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr09mr_upd_manifest_detail;Ansi"


//
// Declaration for procedure: rmtr10mr_inq_stand_sat_sort
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr10mr_inq_stand_sat_sort( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr10mr_inq_stand_sat_sort;Ansi"


//
// Declaration for procedure: rmtr11mr_upd_stand_sat_sort
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr11mr_upd_stand_sat_sort( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string sat_input_string, &
    ref string plant_input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr11mr_upd_stand_sat_sort;Ansi"


//
// Declaration for procedure: rmtr12mr_inq_sch_est_sat_sort
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr12mr_inq_sch_est_sat_sort( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr12mr_inq_sch_est_sat_sort;Ansi"


//
// Declaration for procedure: rmtr13mr_upd_sch_est_sat_sort
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr13mr_upd_sch_est_sat_sort( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string sat_input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr13mr_upd_sch_est_sat_sort;Ansi"


//
// Declaration for procedure: rmtr14mr_inq_car_grade_forecast
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr14mr_inq_car_grade_forecast( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr14mr_inq_car_grade_forecast;Ansi"


//
// Declaration for procedure: rmtr15mr_upd_car_grade_forecast
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr15mr_upd_car_grade_forecast( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string sat_input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr15mr_upd_car_grade_forecast;Ansi"


//
// Declaration for procedure: rmtr16mr_inq_car_wgt_estimate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr16mr_inq_car_wgt_estimate( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr16mr_inq_car_wgt_estimate;Ansi"


//
// Declaration for procedure: rmtr17mr_upd_car_wgt_estimate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr17mr_upd_car_wgt_estimate( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr17mr_upd_car_wgt_estimate;Ansi"


//
// Declaration for procedure: rmtr18mr_inq_car_grade_estimate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr18mr_inq_car_grade_estimate( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr18mr_inq_car_grade_estimate;Ansi"


//
// Declaration for procedure: rmtr19mr_upd_car_grade_estimate
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr19mr_upd_car_grade_estimate( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr19mr_upd_car_grade_estimate;Ansi"


//
// Declaration for procedure: rmtr20mr_inq_car_tracking_view
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr20mr_inq_car_tracking_view( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr20mr_inq_car_tracking_view;Ansi"


//
// Declaration for procedure: rmtr21mr_upd_car_tracking_view
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr21mr_upd_car_tracking_view( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr21mr_upd_car_tracking_view;Ansi"


//
// Declaration for procedure: rmtr22mr_inq_cooler_inv
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr22mr_inq_cooler_inv( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr22mr_inq_cooler_inv;Ansi"


//
// Declaration for procedure: rmtr23mr_inq_sel_percent
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr23mr_inq_sel_percent( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr23mr_inq_sel_percent;Ansi"


//
// Declaration for procedure: rmtr24mr_upd_sel_percent
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr24mr_upd_sel_percent( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string header_string, &
    ref string input_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr24mr_upd_sel_percent;Ansi"


//
// Declaration for procedure: rmtr25mr_inq_hot_box_actual
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr25mr_inq_hot_box_actual( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr25mr_inq_hot_box_actual;Ansi"


//
// Declaration for procedure: rmtr26mr_inq_cooler_actual
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr26mr_inq_cooler_actual( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr26mr_inq_cooler_actual;Ansi"


//
// Declaration for procedure: rmtr27mr_inq_cartracking_whatif
// Network connection style : Non-Persistent
// Procedure attributes: authen
//
function int rmtr27mr_inq_cartracking_whatif( &
    ref string se_app_name, &
    ref string se_window_name, &
    ref string se_function_name, &
    ref string se_event_name, &
    ref string se_procedure_name, &
    ref string se_user_id, &
    ref string se_return_code, &
    ref string se_message, &
    ref string input_string, &
    string char_string, &
    string whatif_string, &
    ref string output_string, &
    ref double task_number, &
    ref double last_record_number, &
    ref double max_record_number, &
    int CommHnd &
) library "rmt001.dll" alias for "rmtr27mr_inq_cartracking_whatif;Ansi"

// ***********************************************************
//
// If you are using multiple interfaces in your application
// refer to the 'Guide for Power Builder' manual for an
// explanation of issues related to the following function(s).
// 
function int WBCkCompleted( int CommHnd ) &
    library "rmt001.dll"
function int WBrmt001CkCompleted( int CommHnd ) &
    library "rmt001.dll"
//
// ***********************************************************


end prototypes

type variables
Int		ii_rmt001_commhandle

end variables

forward prototypes
public function integer uf_rmtr01mr_inq_cattle_char (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string)
public function boolean uf_rmtr02mr_upd_cattle_char (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function integer uf_rmtr04mr_inq_manifest_summary (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string)
public function boolean uf_rmtr05mr_upd_manifest_summary (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function boolean uf_rmtr07mr_upd_pa_cattle_char (s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function boolean uf_rmtr08mr_inq_mainifest_detail (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header)
public function boolean uf_rmtr09mr_upd_manifest_detail (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function boolean uf_rmtr10mr_inq_stand_sat_sort (s_error astr_error_info, ref string as_detail, ref string as_header_columns, ref string as_header)
public function boolean uf_rmtr11mr_upd_stand_sat_sort (ref s_error astr_error_info, ref string as_header_string, ref string as_sat_input_string, ref string as_plant_input_string)
public function boolean uf_rmtr12mr_inq_sch_est_sat_sort (s_error astr_error_info, ref string as_detail, ref string as_header_columns, ref string as_header, ref string as_est_production)
public function boolean uf_rmtr13mr_upd_sch_est_sat_sort (ref s_error astr_error_info, ref string as_header_string, ref string as_sat_input_string)
public function boolean uf_rmtr15mr_upd_car_grade_forecast (ref s_error astr_error_info, ref string as_header_string, ref string as_sat_input_string)
public function boolean uf_rmtr14mr_inq_car_grade_forecast (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header)
public function boolean uf_rmtr17mr_upd_car_wgt_estimate (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function integer uf_rmtr16mr_inq_car_wgt_estimate (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function boolean uf_rmtr19mr_upd_car_grade_estimate (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function integer uf_rmtr18mr_inq_car_grade_estimate (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function boolean uf_rmtr21mr_upd_car_tracking_view (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function boolean uf_rmtr20mr_inq_car_tracking_view (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header)
public function integer uf_rmtr23mr_inq_sel_percent (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function boolean uf_rmtr24mr_upd_sel_percent (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function boolean uf_rmtr03mr_inq_dddw (s_error astr_error_info, ref string as_grade, ref string as_yields, ref string as_desc, ref string as_program)
public function integer uf_rmtr06mr_inq_hot_box_perpetual (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function integer uf_rmtr22mr_inq_cooler_perpetual (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function integer uf_rmtr25mr_inq_hot_box_actual (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function boolean uf_rmtr26mr_inq_cooler_actual (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header)
public function boolean uf_rmtr27mr_inq_cartracking_whatif (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header)
end prototypes

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: Rev#01
**   PROJECT NUMBER:  Support
**   DATE:				 January 2000            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Redesign of Window(rmtr03mr)
**                    Remove Sex & Destination, Added Program Type 
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public function integer uf_rmtr01mr_inq_cattle_char (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr01mr_inq_cattle_char"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do
	lt_starttime = Now()

	li_rtn = rmtr01mr_inq_cattle_char(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	


	//	messagebox('output',string(as_output_string))
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean uf_rmtr02mr_upd_cattle_char (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr02mr_upd_cattle_char"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr02mr_upd_cattle_char(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function integer uf_rmtr04mr_inq_manifest_summary (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr04mr_inq_manifest_summary"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)

Do
	lt_starttime = Now()

	li_rtn = rmtr04mr_inq_manifest_summary(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					as_header_string, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_output_string += ls_output
	lt_endtime = Now()
	


//	messagebox('output',string(as_output_string))
	
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn

end function

public function boolean uf_rmtr05mr_upd_manifest_summary (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr05mr_upd_manifest_summary"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr05mr_upd_manifest_summary(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr07mr_upd_pa_cattle_char (s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr07mr_upd_pa_cattle_char"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr07mr_upd_pa_cattle_char(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr08mr_inq_mainifest_detail (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header);Int						li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "rmtr08mr_inq_manifest_detail"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_header_columns	= ''
as_detail 	    	= ''
ls_output_string 	= space(20000)
ls_output_string 	= fill(char(0),20000)
as_header += space(33)
ls_header = as_header 

Do
		lt_starttime = Now()
		li_ret = rmtr08mr_inq_manifest_detail(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
		lt_endtime = Now()
	
	
//		messagebox('output',string(ls_output_string))

		as_header = ls_header
		ls_string_ind = Mid(ls_output_string, 2, 1)
		ls_output_string = Mid(ls_output_string, 3)
		Do While Len(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
			Choose Case ls_string_ind
				Case 'C'
					as_header_columns += ls_output
				Case 'D'
					as_detail			+= ls_output
			End Choose
			ls_string_ind = Left(ls_output_string, 1)
			ls_output_string = Mid(ls_output_string, 2)
		Loop 

		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)


end function

public function boolean uf_rmtr09mr_upd_manifest_detail (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr09mr_upd_manifest_detail"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr09mr_upd_manifest_detail(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr10mr_inq_stand_sat_sort (s_error astr_error_info, ref string as_detail, ref string as_header_columns, ref string as_header);Int						li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "rmtr10mr_inq_stand_sat_sort"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_header_columns	= ''
as_detail 	    	= ''
ls_output_string 	= space(20000)
ls_output_string 	= fill(char(0),20000)
as_header += space(14)
ls_header = as_header 
	
//	messagebox('length header',String(len(ls_header)))
//	messagebox('length detail',String(len(ls_output_string)))

Do 
		lt_starttime = Now()
		li_ret = rmtr10mr_inq_stand_sat_sort(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
		lt_endtime = Now()
	
	
//		messagebox('output',string(ls_output_string))

		as_header = ls_header
		ls_string_ind = Mid(ls_output_string, 2, 1)
		ls_output_string = Mid(ls_output_string, 3)
		Do While Len(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
			Choose Case ls_string_ind
				Case 'C'
					as_header_columns += ls_output
				Case 'D'
					as_detail			+= ls_output
			End Choose
			ls_string_ind = Left(ls_output_string, 1)
			ls_output_string = Mid(ls_output_string, 2)
		Loop 

		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)


end function

public function boolean uf_rmtr11mr_upd_stand_sat_sort (ref s_error astr_error_info, ref string as_header_string, ref string as_sat_input_string, ref string as_plant_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr11mr_upd_stand_sat_sort"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr11mr_upd_stand_sat_sort(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_sat_input_string, &
								as_plant_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr12mr_inq_sch_est_sat_sort (s_error astr_error_info, ref string as_detail, ref string as_header_columns, ref string as_header, ref string as_est_production);Int						li_ret
Time						lt_starttime, &
							lt_endtime
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header
u_string_functions	lu_string				

astr_error_info.se_procedure_name = "rmtr12mr_inq_sch_est_sat_sort"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_header_columns	= ''
as_detail 	    	= ''
as_est_production = ''
ls_output_string 	= space(20000)
ls_output_string 	= fill(char(0),20000)
as_header += space(14)
ls_header = as_header 
	
//	messagebox('length header',String(len(ls_header)))
//	messagebox('length detail',String(len(ls_output_string)))

Do 
	lt_starttime = Now()
	li_ret = rmtr12mr_inq_sch_est_sat_sort(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				ls_header, &
				ls_output_string, &
				ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number, &
				ii_rmt001_commhandle)
	lt_endtime = Now()


//	messagebox('output',string(ls_output_string))

	as_header = ls_header
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail			+= ls_output
			Case 'E'
				as_est_production	+= ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr13mr_upd_sch_est_sat_sort (ref s_error astr_error_info, ref string as_header_string, ref string as_sat_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr13mr_upd_sch_est_sat_sort"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr13mr_upd_sch_est_sat_sort(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_sat_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr15mr_upd_car_grade_forecast (ref s_error astr_error_info, ref string as_header_string, ref string as_sat_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr15mr_upd_car_grade_forecast"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr15mr_upd_car_grade_forecast(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_sat_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr14mr_inq_car_grade_forecast (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header);Int						li_ret
Time						lt_starttime, &
							lt_endtime
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header
u_string_functions	lu_string				

astr_error_info.se_procedure_name = "rmtr14mr_inq_car_grade_forecast"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_header_columns	= ''
as_detail 	    	= ''
as_header_columns = ''
ls_output_string 	= space(20000)
ls_output_string 	= fill(char(0),20000)
as_header += space(26)
ls_header = as_header 
	
//	messagebox('length header',String(len(ls_header)))
//	messagebox('length detail',String(len(ls_output_string)))

Do 
	lt_starttime = Now()
	li_ret = rmtr14mr_inq_car_grade_forecast(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				ls_header, &
				ls_output_string, &
				ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number, &
				ii_rmt001_commhandle)
	lt_endtime = Now()


//	messagebox('output',string(ls_output_string))

	as_header = ls_header
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail			+= ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr17mr_upd_car_wgt_estimate (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr17mr_upd_car_wgt_estimate"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr17mr_upd_car_wgt_estimate(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function integer uf_rmtr16mr_inq_car_wgt_estimate (ref s_error astr_error_info, ref string as_header, ref string as_detail);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_header,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr16mr_inq_car_wgt_estimate"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)
as_header += space(15)
ls_header = as_header 


Do
	lt_starttime = Now()

	li_rtn = rmtr16mr_inq_car_wgt_estimate(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_detail += ls_output
	lt_endtime = Now()
	
	as_header = ls_header
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	//	messagebox('output',string(as_output_string))
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn


end function

public function boolean uf_rmtr19mr_upd_car_grade_estimate (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr19mr_upd_car_grade_estimate"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr19mr_upd_car_grade_estimate(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function integer uf_rmtr18mr_inq_car_grade_estimate (ref s_error astr_error_info, ref string as_header, ref string as_detail);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_header,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr18mr_inq_car_grade_estimate"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)
as_header += space(15)
ls_header = as_header 


Do
	lt_starttime = Now()

	li_rtn = rmtr18mr_inq_car_grade_estimate(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_detail += ls_output
	lt_endtime = Now()
	
	as_header = ls_header
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	//	messagebox('output',string(as_output_string))
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn


end function

public function boolean uf_rmtr21mr_upd_car_tracking_view (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr21mr_upd_car_tracking_view"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr21mr_upd_car_tracking_view(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr20mr_inq_car_tracking_view (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header);Int						li_ret
Time						lt_starttime, &
							lt_endtime
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header
u_string_functions	lu_string				

astr_error_info.se_procedure_name = "rmtr20mr_inq_car_tracking_view"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_header_columns	= ''
as_detail 	    	= ''
as_header_columns = ''
ls_output_string 	= space(20000)
ls_output_string 	= fill(char(0),20000)
as_header += space(15)
ls_header = as_header 
	
//	messagebox('length header',String(len(ls_header)))
//	messagebox('length detail',String(len(ls_output_string)))

Do 
	lt_starttime = Now()
	li_ret = rmtr20mr_inq_car_tracking_view(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				ls_header, &
				ls_output_string, &
				ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number, &
				ii_rmt001_commhandle)
	lt_endtime = Now()


//	messagebox('output',string(ls_output_string))

	as_header = ls_header
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail			+= ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function integer uf_rmtr23mr_inq_sel_percent (ref s_error astr_error_info, ref string as_header, ref string as_detail);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_header,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr23mr_inq_sel_percent"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(130)
ls_output = Fill(char(0),130)
as_header += space(5)
ls_header = as_header 


Do
	lt_starttime = Now()

	li_rtn = rmtr23mr_inq_sel_percent(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_detail += ls_output
	lt_endtime = Now()
	
	as_header = ls_header
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	//	messagebox('output',string(as_output_string))
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn


end function

public function boolean uf_rmtr24mr_upd_sel_percent (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number
Int			li_ret

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_return_code,&
				ls_message

Time			lt_starttime, &
				lt_endtime
								

astr_error_info.se_procedure_name = "rmtr24mr_upd_sel_percent"
astr_error_info.se_message = Space(71)

ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0


nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

lt_starttime = Now()

li_ret = rmtr24mr_upd_sel_percent(ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message,&
								as_header_string, &
								as_input_string, &
								ld_task_number, &
								ld_last_record_number, &
								ld_max_record_number, &
								ii_rmt001_commhandle)

lt_endtime = Now()

ls_procedure_name = astr_error_info.se_procedure_name
nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr03mr_inq_dddw (s_error astr_error_info, ref string as_grade, ref string as_yields, ref string as_desc, ref string as_program);Int						li_ret

Time						lt_starttime, &
							lt_endtime
								
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
					
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message

u_string_functions	lu_string				



astr_error_info.se_procedure_name = "rmtr03mr_inq_dddw"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


	as_grade				= ''
	as_yields 	    	= ''
// Rev#01 Removed Sex and Destination added Program	
//	as_destination    = ''
//	as_sex				= ''
	as_desc				= ''
	as_program			= '' 
	ls_output_string 	= space(20000)
 	ls_output_string 	= fill(char(0),20000)

Do
		lt_starttime = Now()
		li_ret = rmtr03mr_inq_dddw(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_output_string, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
		lt_endtime = Now()
	
	
//		messagebox('output',string(ls_output_string))

		ls_string_ind = Mid(ls_output_string, 2, 1)
		ls_output_string = Mid(ls_output_string, 3)
		Do While Len(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		
			Choose Case ls_string_ind
				Case 'G'
					as_grade += ls_output
				Case 'Y'
					as_yields += ls_output
//				Case 'D'
//					as_destination += ls_output
//				Case 'S'
//					as_sex += ls_output
				Case 'X'
					as_desc += ls_output
				Case 'P'
					as_program += ls_output
			End Choose
			ls_string_ind = Left(ls_output_string, 1)
			ls_output_string = Mid(ls_output_string, 2)
		Loop 

		nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')

Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)


end function

public function integer uf_rmtr06mr_inq_hot_box_perpetual (ref s_error astr_error_info, ref string as_header, ref string as_detail);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_header,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr06mr_inq_hot_box_perpetual"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)
as_header += space(6)
ls_header = as_header 


Do
	lt_starttime = Now()

	li_rtn = rmtr06mr_inq_hot_box_inv(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_detail += ls_output
	lt_endtime = Now()
	
	as_header = ls_header
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	//	messagebox('output',string(as_output_string))
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn


end function

public function integer uf_rmtr22mr_inq_cooler_perpetual (ref s_error astr_error_info, ref string as_header, ref string as_detail);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_header,&
				ls_return_code,&
				ls_message, &
				ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr22mr_inq_cooler_perpetual"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(20000)
ls_output = Fill(char(0),20000)
as_header += space(6)
ls_header = as_header 


Do
	lt_starttime = Now()

	li_rtn = rmtr22mr_inq_cooler_inv(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	as_detail += ls_output
	lt_endtime = Now()
	
	as_header = ls_header
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	//	messagebox('output',string(as_output_string))
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
  
nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn


end function

public function integer uf_rmtr25mr_inq_hot_box_actual (ref s_error astr_error_info, ref string as_header, ref string as_detail);Double		ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number

Int			li_rtn
Boolean		lb_first_time_back

String		ls_app_name, &
				ls_window_name, &
				ls_function_name, &
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id,&
				ls_header,&
				ls_return_code,&
				ls_message, &
				ls_hold_avg,	ls_output
				
Time			lt_starttime, &
				lt_endtime
								



astr_error_info.se_procedure_name = "rmtr25mr_inq_hot_box_actual"
astr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

nf_get_s_error_values ( &
		astr_error_info, &
		ls_app_name, &
		ls_window_name, &
		ls_function_name, &
		ls_event_name, &
		ls_procedure_name, &
		ls_user_id,&
		ls_return_code,&
		ls_message )
		
//Call a Netwise external function to get the required information
ld_task_number = 0
ld_last_record_number = 0
ld_max_record_number = 0
ls_output = Space(3240)
ls_output = Fill(char(0),3240)
//as_header += space(5)
ls_header = as_header 

as_detail = ''
Do
	lt_starttime = Now()

	li_rtn = rmtr25mr_inq_hot_box_actual(ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message, &
					ls_header, &
					ls_output, &
					ld_task_number, &
					ld_last_record_number, &
					ld_max_record_number, &
					ii_rmt001_commhandle)
	
	// I am sending back the avg in the header string
	If not lb_first_time_back then
		ls_hold_avg = ls_header
		ls_header = as_header
		lb_first_time_back = True
	End if
	as_detail += ls_output
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
	//	messagebox('output',string(as_output_string))
	
Loop while ld_last_record_number <> ld_max_record_number and li_rtn >= 0
//as_detail = ls_output
as_header = ls_hold_avg  

nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )				 

// Check RPC ERROR AND DISPLAYS NEEDED MESSAGES
THIS.nf_display_message(li_rtn, astr_error_info, ii_rmt001_commhandle)

//messagebox('Rmt001 li rtn',string(li_rtn))

Return li_rtn


end function

public function boolean uf_rmtr26mr_inq_cooler_actual (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header);Int						li_ret
Time						lt_starttime, &
							lt_endtime
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header
u_string_functions	lu_string				

astr_error_info.se_procedure_name = "rmtr26mr_inq_cooler_actual"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )


as_header_columns	= ''
as_detail 	    	= ''
as_header_columns = ''
ls_output_string 	= space(20000)
ls_output_string 	= fill(char(0),20000)
//as_header += space(15)
ls_header = as_header 
	
//	messagebox('length header',String(len(ls_header)))
//	messagebox('length detail',String(len(ls_output_string)))

Do 
	lt_starttime = Now()
	li_ret = rmtr26mr_inq_cooler_actual(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				ls_header, &
				ls_output_string, &
				ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number, &
				ii_rmt001_commhandle)
	lt_endtime = Now()


//	messagebox('output',string(ls_output_string))

	as_header = ls_header
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail			+= ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

public function boolean uf_rmtr27mr_inq_cartracking_whatif (s_error astr_error_info, ref string as_header_columns, ref string as_detail, ref string as_header);Int						li_ret
Long						ll_string_length
Time						lt_starttime, &
							lt_endtime
Double					ld_task_number, &
							ld_last_record_number, &
							ld_max_record_number
String					ls_app_name, &
							ls_window_name, &
 							ls_function_name, &
							ls_event_name, &
							ls_procedure_name, &
							ls_user_id,&
							ls_output,&
							ls_chars,&
							ls_string_ind,&
							ls_output_string,&
							ls_return_code,&
							ls_message,ls_header,ls_what_if_string
u_string_functions	lu_string				

astr_error_info.se_procedure_name = "rmtr27mr_inq_cartracking_whatif"
astr_error_info.se_message = Space(71)

nf_get_s_error_values (astr_error_info, &
								ls_app_name, &
								ls_window_name, &
								ls_function_name, &
								ls_event_name, &
								ls_procedure_name, &
								ls_user_id,&
								ls_return_code,&
								ls_message )

ls_output_string 	= space(20000)
ll_string_length = len(as_detail)
ls_what_if_string = as_detail  
as_header += space(15)


ls_header = as_header 
ls_chars = as_header_columns	
//	messagebox('length header',String(len(ls_header)))
//	messagebox('length detail',String(len(ls_output_string)))

Do 
	lt_starttime = Now()
	li_ret = rmtr27mr_inq_cartracking_whatif(ls_app_name, &
				ls_window_name,&
				ls_function_name,&
				ls_event_name, &
				ls_procedure_name, &
				ls_user_id, &
				ls_return_code, &
				ls_message, &
				ls_header, &
				ls_chars, &
				ls_what_if_string, &
				ls_output_string, &
				ld_task_number, &
				ld_last_record_number, &
				ld_max_record_number, &
				ii_rmt001_commhandle)
	lt_endtime = Now()

//	messagebox('output',string(ls_output_string))
	as_detail = ''
	as_header = ls_header
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
	Do While Len(Trim(ls_output_string)) > 0
		ls_output = lu_string.nf_GetToken(ls_output_string, '~f')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail			+= ls_output
		End Choose
		ls_string_ind = Left(ls_output_string, 1)
		ls_output_string = Mid(ls_output_string, 2)
	Loop 
	nf_write_benchmark(lt_starttime, lt_endtime, ls_procedure_name, '')
Loop while ld_last_record_number <> ld_max_record_number and li_ret >= 0


nf_set_s_error ( astr_error_info,&
					ls_app_name, &
					ls_window_name,&
					ls_function_name,&
					ls_event_name, &
					ls_procedure_name, &
					ls_user_id, &
					ls_return_code, &
					ls_message )	

return nf_display_message(li_ret, astr_error_info, ii_rmt001_commhandle)

end function

on u_rmt001.create
call transaction::create
TriggerEvent( this, "constructor" )
end on

on u_rmt001.destroy
call transaction::destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;// Get the CommHandle to be used for windows
ii_rmt001_commhandle = sqlca.nf_getcommhandle("rmt001")
If ii_rmt001_commhandle = 0 Then
	// an error occured
	Message.ReturnValue = -1
End if
end event

