HA$PBExportHeader$w_gen_order_response.srw
forward
global type w_gen_order_response from w_base_response_ext
end type
type dw_sales_detail_base from u_base_dw_ext within w_gen_order_response
end type
type st_2 from statictext within w_gen_order_response
end type
type sle_from_sales_order from singlelineedit within w_gen_order_response
end type
type dw_1 from u_base_dw_ext within w_gen_order_response
end type
end forward

global type w_gen_order_response from w_base_response_ext
integer x = 690
integer y = 368
integer width = 1801
integer height = 1328
string title = "Product Move to New Order"
long backcolor = 12632256
dw_sales_detail_base dw_sales_detail_base
st_2 st_2
sle_from_sales_order sle_from_sales_order
dw_1 dw_1
end type
global w_gen_order_response w_gen_order_response

type variables
u_orp001	iu_orp001
u_ws_orp2 	iu_ws_orp2

s_error	s_error_info

w_sales_order_detail iw_sales_order_detail

string	is_load_instruction, &
			is_additional_data_input, &
			is_dept_code, &
			is_dest_country_code, & 
			is_pricing_default_hdr, &
			is_store_door, &
			is_deck_code




end variables

forward prototypes
public function integer wf_check_pa (string as_shortage_string_in, string as_return_val)
public function integer wf_gen_sales (string as_shortage_string_in)
end prototypes

public function integer wf_check_pa (string as_shortage_string_in, string as_return_val);string 	ls_plant, &
			ls_ship_date, &
			ls_to_order, &
			ls_additional_data, &
			ls_returnval, &
			ls_shortage_string_in, &
			ls_shortage_string_out, &
			ls_all_ind, &
			ls_dup_string, &
			ls_option
			
long 		ll_rval		

window 	lw_detail

//dw_1.Accepttext()
//dw_sales_detail_base.AcceptText()
//
ls_plant = dw_1.GetItemString(1, "ship_plant") 
ls_ship_date = String(dw_1.GetItemDate(1,"ship_date"),"MM/DD/YYYY")

iw_sales_order_detail.Event ue_get_data("all")
ls_all_ind = Message.StringParm

s_error_info.se_app_name = "ORP"
s_error_info.se_event_name="CLICKED"
s_error_info.se_function_name="move product"
s_error_info.se_procedure_name = "nf_orpo50ar"
s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
s_error_info.se_window_name = "u_orp001"
//dmk check the return value to set the option for ORPO50BR can compare.
//retry just checks PA again, OK will go ahead with the order creation. 
If as_return_val = 'R' Then
	ls_shortage_string_in = as_shortage_string_in
	ls_additional_data = is_additional_data_input
	ls_Additional_Data += '~t' + is_load_instruction + '~t' + is_dept_code + '~t' + is_dest_country_code + '~t' &
								+ is_store_door + '~t' + is_deck_code + '~t'

	ls_option = 'R'
Else
	If as_return_val = 'OK' Then
		ls_shortage_string_in = as_shortage_string_in
		ls_additional_data = is_additional_data_input
		ls_Additional_Data += '~t' + is_load_instruction + '~t' + is_dept_code + '~t' + is_dest_country_code + '~t' &
								+ is_store_door + '~t' + is_deck_code + '~t'

		ls_option = 'M'
	Else 
		ls_option = 'P'
	End If
End If
//ll_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, & 
//															sle_from_sales_order.Text, & 
//															ls_to_order, &
//															ls_plant, & 
//															ls_ship_date, & 
//															ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//															ls_additional_data, &
//															ls_shortage_string_in, &
//															ls_shortage_string_out, &
//															ls_dup_string) 
//
	ll_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, & 
												sle_from_sales_order.Text, & 
												ls_to_order, &
												ls_plant, & 
												ls_ship_date, & 
												ls_option + '~t' + ls_all_ind + '~t' + dw_sales_detail_base.Describe("DataWindow.Data"), & 
												ls_additional_data, &
												ls_shortage_string_in, &
												ls_shortage_string_out, &
												ls_dup_string) 




ls_shortage_string_in = ls_shortage_string_out

Choose Case ll_rval
	Case 0
		If ls_option = 'P' Then
			wf_gen_sales(ls_shortage_string_in)
		Else
			CloseWithReturn(This,"OK")
		End If
	Case 1
		// do nothing - Fatal Error from ORPO50BR
	Case 2, 5, 6
		//if new order is incomplete display new order
		OpenSheetWithParm(lw_detail, ls_to_order, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)
	Case 3
		//if pa has shortages display short response window.  
		//call function again if Retry is chosen, call wf_gen_sales if OK
		OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
		ls_returnval = Message.StringParm
		If ls_Returnval = 'R' Then
			wf_check_pa(ls_shortage_string_in, ls_Returnval)
		Else
			If ls_Returnval = 'OK' Then
				wf_gen_sales(ls_shortage_string_in)
			Else
				CloseWithReturn(w_move_prod_short_response,"Cancel")
			End if
		End if
End Choose		

Return 1
end function

public function integer wf_gen_sales (string as_shortage_string_in);String	ls_OpenParm, &	
			ls_customer_id, &
			ls_delivery_date, &
			ls_ship_date, &
			ls_division_code, &
			ls_salesperson_code, &
			ls_trans_mode, &
			ls_MessageStringParm, &
			ls_button_classname, &
			ls_Additional_Data, &
			ls_to_order, &
			ls_returnval, &
			ls_plant, &
			ls_instruction_ind, &
			ls_shortage_string_in, &
			ls_shortage_string_out, &
			ls_all_ind, &
			ls_load_instructions, &
			ls_dup_string, &
			ls_option
						
Long 		ll_pos_x, &
			ll_rval	

window 	lw_detail
							
							
dw_1.Accepttext()

ls_OpenParm = ''
iw_sales_order_detail.Event ue_get_data("customer_id")
ls_customer_id = Message.StringParm
iw_sales_order_detail.Event ue_get_data("delivery_date")
ls_delivery_date = Message.StringParm
iw_sales_order_detail.Event ue_get_data("division_code")
ls_division_code = Message.StringParm
iw_sales_order_detail.Event ue_get_data("salesperson_code")
ls_salesperson_code = Message.StringParm
iw_sales_order_detail.Event ue_get_data("all")
ls_all_ind = Message.StringParm

ls_shortage_string_in = as_shortage_string_in
ls_plant = dw_1.GetItemString(1, "ship_plant") 
ls_ship_date = String(dw_1.GetItemDate(1,"ship_date"),"MM/DD/YYYY")

SELECT trans_mode   
   		INTO :ls_trans_mode
   		FROM customer_defaults  
 			WHERE customer_id = :ls_customer_id;
			 
IF len(trim(ls_trans_mode)) = 0 THEN 
		ls_trans_mode = 'T'
END IF
			
//Layout of is_OpenParm tab seperated string
// Indicator of window opening - R reservation, C customer order, P pa inquiry
// Delivery Date
// Ship_date
// plant
// Customer id
// Load status
// Tran Mode
// Division
// Divisional PO
// salesperson
							
							
is_additional_data_input = "R" + "~t" + ls_delivery_date + '~t' + &
							ls_ship_date + '~t' + &
							ls_plant + '~t' + &
							ls_customer_id + '~t' + &
							'U' + '~t' + &
							ls_trans_mode + '~t' + &
							ls_division_code + '~t' + &
							"                  " + '~t' + &
							ls_salesperson_code
							
OpenWithParm( w_additional_data, this)

ls_MessageStringParm = is_additional_data_input

dw_1.SetFocus()

// Message.StringParm is populated with the data+ "~x"+Classname 
// of the button pressed

ll_Pos_x = POS( ls_MessageStringParm, "~x")
ls_button_ClassName = Mid( ls_MessageStringParm, ll_Pos_x + 1)
ls_Additional_Data = Left(ls_MessageStringParm,ll_Pos_x - 1)
ls_instruction_ind = Mid(ls_messageStringParm,ll_Pos_x - 1,1)

//is_additional_data = ls_Additional_Data	
Choose Case ls_button_ClassName 
	Case "Cancel" 
			//  do nothing -- generate was cancelled
	Case "cb_browse"
		MessageBox("Invalid Option", "Reservation Browse not allowed if coming from move rows")
		
	CASE "cb_base_ok"
		s_error_info.se_app_name = "ORP"
		s_error_info.se_event_name="CLICKED"
		s_error_info.se_function_name="move product"
		s_error_info.se_procedure_name = "nf_orpo50ar"
		s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
		s_error_info.se_window_name = "u_orp001"
		//dmk add dest country code
		ls_Additional_Data += '~t' + is_load_instruction + '~t' + is_dept_code + '~t' + is_dest_country_code + '~t' &
								+ is_store_door + '~t' + is_deck_code + '~t'

		
//		ll_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, &
//																	sle_from_sales_order.Text, & 
//																	ls_to_order, & 
//																	ls_plant, & 
//																	ls_ship_date, & 
//																	'M' + '~t' + ls_all_ind + "~t"  + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//																	ls_additional_data, &
//																	ls_shortage_string_in, &
//																	ls_shortage_string_out, &
//																	ls_dup_string) 

				ll_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, &
															sle_from_sales_order.Text, & 
															ls_to_order, & 
															ls_plant, & 
															ls_ship_date, & 
															'M' + '~t' + ls_all_ind + "~t"  + dw_sales_detail_base.Describe("DataWindow.Data"), & 
															ls_additional_data, &
															ls_shortage_string_in, &
															ls_shortage_string_out, &
															ls_dup_string) 



		
		ls_shortage_string_in = ls_shortage_string_out
		
		Choose Case ll_rval
			Case 0
				 CloseWithReturn(This,"OK")
				
			Case 1
				// Error occurred.  Leave window open
				
			Case 2, 5, 6
				if (ls_instruction_ind = 'Y') and (ll_rval = 6) then
					ls_to_order = ls_to_order + '~t' + 'I'
				end if	
				OpenSheetWithParm(lw_detail, ls_to_order, 'w_sales_order_detail', iw_frame, 0, &
						iw_Frame.im_menu.iao_ArrangeOpen)
				CloseWithReturn(This,"OK")
		//dmk	rval of 3 is short PA, display short window and call 
		//check pa function to either go ahead with the shortage or retry
			Case 3
				OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
				ls_returnval = Message.StringParm
   			IF ls_returnval = 'OK' or ls_returnval = 'R' Then
				Do
					IF ls_returnval = 'OK' or ls_returnval = 'R' Then
	   				s_error_info.se_app_name = "ORP"
						s_error_info.se_event_name="CLICKED"
						s_error_info.se_function_name="move product"
						s_error_info.se_procedure_name = "nf_orpo50ar"
						s_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
						s_error_info.se_window_name = "u_orp001"
						If ls_returnval = 'OK' Then
							ls_option = 'M'
						Else
							ls_option = 'R'
						End IF
//						ll_rval = iu_orp001.nf_orpo50ar_move_product ( s_error_info, &
//																	sle_from_sales_order.Text, & 
//																	ls_to_order, & 
//																	ls_plant, & 
//																	ls_ship_date, & 
//																	ls_option + '~t' + ls_all_ind + "~t"  + dw_sales_detail_base.Describe("DataWindow.Data"), & 
//																	ls_additional_data, &
//																	ls_shortage_string_in, &
//																	ls_shortage_string_out, &
//																	ls_dup_string) 

					ll_rval = iu_ws_orp2.nf_orpo50fr( s_error_info, &
																sle_from_sales_order.Text, & 
																ls_to_order, & 
																ls_plant, & 
																ls_ship_date, & 
																ls_option + '~t' + ls_all_ind + "~t"  + dw_sales_detail_base.Describe("DataWindow.Data"), & 
																ls_additional_data, &
																ls_shortage_string_in, &
																ls_shortage_string_out, &
																ls_dup_string) 





		
						ls_shortage_string_in = ls_shortage_string_out	
						If ll_rval = 3 Then 
							OpenWithParm( w_move_prod_short_response, ls_shortage_string_out)
							ls_returnval = Message.StringParm
						End If
					End If
				Loop until ll_rval <> 3 or ls_returnval = 'Cancel'	
				End if 
				Choose Case ll_rval				
					Case 0
						CloseWithReturn(This,"OK")
					Case 1
						//Error in move.  Response window stays open
					Case 2, 5, 6
						if (ls_instruction_ind = 'Y') and (ll_rval = 6) then
							ls_to_order = ls_to_order + '~t' + 'I'
						end if	
						OpenSheetWithParm(lw_detail, ls_to_order, 'w_sales_order_detail', iw_frame, 0, &
							iw_Frame.im_menu.iao_ArrangeOpen)
						CloseWithReturn(This,"OK")
				end choose		
			Case 5
//			OpenWithParm(w_pa_resolve_response, ls_pa_resolve_out) 
//			CHOOSE CASE Message.StringParm
//			CASE	"R"
//				This.PostEvent('ue_update')
//			CASE	"C"
//				ib_resolve_running = True
//			//check what should be here	wf_complete_order()
//			END CHOOSE

		End Choose
End Choose
Return 1


end function

event close;call super::close;IF IsValid( iu_orp001) Then Destroy( iu_orp001)
Destroy iu_ws_orp2
end event

event open;call super::open;iu_orp001 = Create u_orp001
iu_ws_orp2 = Create u_ws_orp2

String 	ls_Import
DataWindowChild	ldwc_DropDown
long	ll_error

iw_sales_order_detail = Message.PowerObjectParm

sle_from_sales_order.Text = iw_sales_order_detail.is_orderid
//em_order_id.Text	= iw_sales_order_detail.is_to_order_id
//iw_sales_Order_Detail.is_orderid	=	''
ls_Import = iw_sales_order_detail.wf_get_selected_rows()
dw_sales_detail_base.ImportString( ls_Import)
dw_1.InsertRow(0)
dw_1.GetChild('ship_plant', ldwc_DropDown)

IF ldwc_DropDown.RowCount() < 1 then
	ll_error = ldwc_DropDown.SetTransObject( SQLCA)
	ll_error=ldwc_DropDown.Retrieve()
END IF	

iw_sales_order_detail.Event ue_get_data("ship_date")
dw_1.SetItem(1, "ship_date", Date(Message.StringParm))

iw_sales_order_detail.Event ue_get_data("ship_plant")
dw_1.SetItem(1, "ship_plant", Message.StringParm)

iw_sales_order_detail.Event ue_get_data("pricing_default_hdr")
is_pricing_default_hdr = Message.StringParm



end event

on w_gen_order_response.create
int iCurrent
call super::create
this.dw_sales_detail_base=create dw_sales_detail_base
this.st_2=create st_2
this.sle_from_sales_order=create sle_from_sales_order
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_detail_base
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.sle_from_sales_order
this.Control[iCurrent+4]=this.dw_1
end on

on w_gen_order_response.destroy
call super::destroy
destroy(this.dw_sales_detail_base)
destroy(this.st_2)
destroy(this.sle_from_sales_order)
destroy(this.dw_1)
end on

event ue_base_ok;call super::ue_base_ok;if (dw_1.Accepttext() = -1) or (dw_sales_detail_base.AcceptText() = -1) then	
	return
else
//dmk added spaces for shortage string to function and P for returnval
	wf_check_pa(' ', 'P')
	//CloseWithReturn(This,"OK")
end if
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This,"Cancel")
end event

event ue_get_data;String  ls_product_code, ls_micro_test_ind, ls_micro_test_product_found
Long	ll_RowCount, ll_Row

choose case as_value
	case "load_instruction"
		Message.StringParm = is_load_instruction
	case "additonal_data_input"
		Message.StringParm = is_additional_data_input
	case "export_country"
		Message.StringParm = is_dest_country_code
	case "pricing_default_hdr"
		Message.StringParm = is_pricing_default_hdr	
	Case "micro_test_product"  //slh SR25459
		ls_micro_test_product_found = "N"
	     ll_RowCount = dw_sales_detail_base.RowCount()
		For ll_Row = 1 to ll_RowCount
				If dw_sales_detail_base.GetItemNumber(ll_Row, 'ordered_units') > 0 Then
					ls_product_code = dw_sales_detail_base.GetItemString(ll_Row, 'product_code')
						
						SELECT micro_test_ind
						INTO :ls_micro_test_ind
						FROM sku_products 
                           WHERE sku_product_code = :ls_product_code
						USING SQLCA;
					
						If ls_micro_test_ind = "Y" Then
							ls_micro_test_product_found = "Y"
							EXIT
						End If
				End If
		Next
		Message.StringParm = ls_micro_test_product_found						
end choose

return "OK"


end event

event ue_set_data;choose case as_data_item
	case "load_instruction"
		is_load_instruction = as_value
	case "dept_code"
		is_dept_code = as_value	
	case "additonal_data_input"
		is_additional_data_input = as_value
	case "export_country"
		is_dest_country_code = as_value
	case "store_door"
  		is_store_door = as_value
	case "deck_code"
  		is_deck_code = as_value		
end choose
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_gen_order_response
boolean visible = false
integer x = 1143
integer y = 48
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_gen_order_response
integer x = 1225
integer y = 488
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_gen_order_response
integer x = 1225
integer y = 360
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_gen_order_response
integer x = 1225
integer y = 500
integer width = 270
integer taborder = 60
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
boolean enabled = false
end type

type dw_sales_detail_base from u_base_dw_ext within w_gen_order_response
integer x = 37
integer y = 360
integer width = 1179
integer height = 824
integer taborder = 20
string dataobject = "d_sales_detail_base"
boolean vscrollbar = true
end type

event itemerror;call super::itemerror;IF GetColumnName()  = "ordered_units" Then
	Messagebox( "Ordered Units", "Ordered units must be > 0, but can not be greater than the original scheduled units")
	RETURN 1	
END IF
end event

event itemchanged;call super::itemchanged;Long	ll_ordered,&
	ll_sch_units
String ls_columnName
ls_columnName = GetColumnName()
IF    ls_columnName = "ordered_units" Then
	ll_ordered = Long(This.GetText())
	ll_sch_units = This.GetItemDecimal( This.GetRow(), "scheduled_units") 
	IF ll_ordered > ll_sch_units Then 
		RETURN 1
	END IF
END IF
end event

type st_2 from statictext within w_gen_order_response
integer y = 40
integer width = 485
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "From Sales Order #"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_from_sales_order from singlelineedit within w_gen_order_response
integer x = 59
integer y = 132
integer width = 325
integer height = 88
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean autohscroll = false
textcase textcase = upper!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from u_base_dw_ext within w_gen_order_response
integer x = 530
integer y = 32
integer width = 1225
integer height = 304
integer taborder = 10
string dataobject = "d_ship_plant_date"
end type

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1,100)	
end event

event getfocus;call super::getfocus;This.SelectText(1,100)	

end event

event itemchanged;call super::itemchanged;Long			ll_RowFound

String 		ls_FindExpression , &
				ls_columnname

datawindowchild	ldw_childdw

if dwo.Name = "ship_plant" then
	This.GetChild('ship_plant', ldw_ChildDW)
	ll_rowfound	=	ldw_ChildDW.RowCount()
	ls_FindExpression = "location_code = ~"" + Trim(data) + "~""
	ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
	If  ll_RowFound <= 0 Then 
		MessageBox('Invalid Data', data + ' is an invalid plant code')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF	
END IF
end event

event itemerror;call super::itemerror;RETURN 1
end event

