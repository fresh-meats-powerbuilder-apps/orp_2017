HA$PBExportHeader$u_ship_date.sru
forward
global type u_ship_date from datawindow
end type
end forward

global type u_ship_date from datawindow
int Width=649
int Height=92
int TabOrder=10
string DataObject="d_ship_date"
boolean Border=false
boolean LiveScroll=true
end type
global u_ship_date u_ship_date

type variables
Window		iw_parentwindow
end variables

forward prototypes
public function date uf_get_ship_date ()
public subroutine uf_set_ship_date (date adt_ship_date)
end prototypes

public function date uf_get_ship_date ();Return This.GetItemDate(1, 'ship_date')
end function

public subroutine uf_set_ship_date (date adt_ship_date);This.SetItem(1,"ship_date",adt_ship_date)
end subroutine

event constructor;String	ls_text

iw_parentwindow = Parent
This.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Orp", "Lastshipdate","")

if ls_text = '' then
	ls_text = String(Today())
end if

This.SetItem( 1, "ship_date", Date(ls_text))

end event

event doubleclicked;String		ls_date

str_parms	lstr_parms

Application	lu_App

u_AbstractParameterStack	lu_Stack
u_AbstractErrorContext		lu_ErrorContext
u_AbstractClassFactory		lu_ClassFactory

// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = gw_base_Frame.PointerX() + gw_base_Frame.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = gw_base_Frame.PointerY() + gw_base_Frame.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

ls_Date = String(This.GetItemDate(1, 'ship_date'),"mm/yy/yyyy")
IF NOT(ISDate(ls_Date)) Then ls_Date =String( Today(), "mm/dd/yyyy")
lu_App = GetApplication()	
IF lu_App.Dynamic af_GetClassFactory(lu_ClassFactory) <> 0 then return
lu_ClassFactory.uf_GetObject('u_ErrorContext', lu_ErrorContext)
lu_ClassFactory.uf_GetObject('u_ParameterStack',lu_Stack,lu_ErrorContext)

// Set up the parameters for the calendar window
lu_Stack.uf_Push("u_AbstractClassFactory",lu_ClassFactory)
lu_stack.uf_Push("String", ls_Date)
lu_Stack.uf_Push("String", "d_GPO_calendar")
lu_Stack.uf_Push("u_AbstractErrorContext",lu_ErrorContext)

// Open the calendar window
lu_ClassFactory.uf_GetResponseWindow("w_orp_calendar",lu_Stack)
lu_Stack = Message.PowerObjectParm
If Not Isvalid( lu_stack ) Then Return
lu_Stack.uf_Pop("u_AbstractErrorContext",lu_ErrorContext)

// Was it successful?  If so, get the date passed back and set the column.
If lu_ErrorContext.uf_IsSuccessful() Then
	lu_Stack.uf_Pop("String",ls_Date)
	If Len(Trim(ls_Date)) = 0 Then Return
//	This.SetItem(row, String(dwo.Name), Date(ls_Date))
	this.settext(ls_date)
	this.accepttext()
End if


end event

event itemchanged;String	ls_date

ls_date = String(Date(data),"mm/dd/yyyy") 
SetProfileString( iw_frame.is_UserINI, "Orp", "Lastshipdate",ls_date)
end event

event getfocus;This.SelectText(1, Len(This.GetText()))
end event

