HA$PBExportHeader$w_sales_detail_inq.srw
$PBExportComments$popup used to inq fro sales order detail
forward
global type w_sales_detail_inq from w_base_response_ext
end type
type st_1 from statictext within w_sales_detail_inq
end type
type em_sales_id from editmask within w_sales_detail_inq
end type
end forward

type wstr_instruction_type from structure
    boolean show_Instruction
    string Instruction_List
end type

global type w_sales_detail_inq from w_base_response_ext
int X=55
int Y=553
int Width=1281
int Height=373
boolean TitleBar=true
string Title="Sales Order Inquire"
long BackColor=12632256
st_1 st_1
em_sales_id em_sales_id
end type
global w_sales_detail_inq w_sales_detail_inq

type variables

end variables

event open;call super::open;Boolean lb_Incomplete_Order

String	ls_boolean, &
			ls_orderid_string

u_conversion_functions	lu_conversion

ls_orderid_string	=	Message.Stringparm
ls_boolean	=	iw_frame.iu_string.nf_gettoken(ls_orderid_string, '~t')
lb_Incomplete_Order = lu_conversion.nf_boolean( ls_boolean)
em_sales_id.text	=	ls_orderid_string
IF lb_Incomplete_Order Then
	em_sales_id.Enabled = FALSE
	em_sales_ID.backcolor = 16761024
ELSE
	em_sales_id.SetFocus()
	em_sales_id.SelectText( 1, Len(em_sales_id.Text))
END IF
end event

on w_sales_detail_inq.create
int iCurrent
call w_base_response_ext::create
this.st_1=create st_1
this.em_sales_id=create em_sales_id
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=em_sales_id
end on

on w_sales_detail_inq.destroy
call w_base_response_ext::destroy
destroy(this.st_1)
destroy(this.em_sales_id)
end on

event ue_base_ok;call super::ue_base_ok;String ls_Return_String 
IF LEN(TRIM(em_sales_id.Text))  = 0 Then
	Messagebox("Sales Order Inquire", "Please enter a sales order number to inquire on")
	Return
END IF
IF LEN(TRIM(em_sales_id.Text))  = 00000 Then
	Messagebox("Sales Order Inquire", "Please enter a valid sales order number to inquire on")
	Return
END IF

ls_Return_String = Upper(em_sales_id.Text)
CloseWithReturn(This, ls_Return_String)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,'Abort')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_sales_detail_inq
int X=540
int Y=173
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_sales_detail_inq
int X=970
int Y=149
int TabOrder=50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_sales_detail_inq
int X=970
int Y=21
int TabOrder=40
end type

type cb_browse from w_base_response_ext`cb_browse within w_sales_detail_inq
int TabOrder=20
end type

type st_1 from statictext within w_sales_detail_inq
int X=28
int Y=101
int Width=407
int Height=73
boolean Enabled=false
string Text="&Sales Order ID"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_sales_id from editmask within w_sales_detail_inq
int X=435
int Y=81
int Width=494
int Height=97
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
string Mask="!####"
MaskDataType MaskDataType=StringMask!
int Accelerator=83
long TextColor=33554432
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event modified;//IF (NOT(IsNumber(Left(ls_value,1))) OR Not(f_IsAlpha(left(ls_value,1),1,1))) AND Not(IsNumber(Mid(ls_value,2,4)))  THEN
//	MessageBox("Invalid Order Number - "+ls_value,"All order Ids must start with one letter or number and end with 4 numbers")
//	SetActionCode(1)
//END IF
//
end event

