HA$PBExportHeader$w_exchange_rate_response.srw
forward
global type w_exchange_rate_response from w_base_response_ext
end type
type dw_detail from u_base_dw_ext within w_exchange_rate_response
end type
type dw_header from u_base_dw_ext within w_exchange_rate_response
end type
type st_1 from statictext within w_exchange_rate_response
end type
type st_2 from statictext within w_exchange_rate_response
end type
type p_next from picture within w_exchange_rate_response
end type
type p_previous from picture within w_exchange_rate_response
end type
end forward

global type w_exchange_rate_response from w_base_response_ext
integer x = 87
integer y = 252
integer width = 2793
integer height = 1128
string title = "Monthly Exchange Rates"
long backcolor = 12632256
dw_detail dw_detail
dw_header dw_header
st_1 st_1
st_2 st_2
p_next p_next
p_previous p_previous
end type
global w_exchange_rate_response w_exchange_rate_response

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user.exe"

end prototypes

type variables
Private:
Boolean		ib_ValidToClose

Int		ii_RowsToInsert, &
		ii_SelectedDate

Long		il_SelectedColor, &
		il_SelectedTextColor

u_orp002		iu_orp002

u_ws_orp3		iu_ws_orp3

String		is_CountryCode

s_error		istr_error_info

end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_next ()
public function integer wf_previous ()
end prototypes

public function boolean wf_retrieve ();Date		ldt_Display

Int		li_Month, &
			li_Year, &
			li_Counter

String	ls_Input, &
			ls_Output


SetPointer(HourGlass!)
If ii_RowsToInsert < ii_SelectedDate Then ii_SelectedDate -= ii_RowsToInsert

dw_detail.Reset()

ldt_Display = dw_header.GetItemDate(1, 'display_date')
li_Month = Month(ldt_display)
li_Year = Year(ldt_Display)

 
// Find what day of the week the 1st is on
ii_RowsToInsert = DayNumber( Date( li_Year, li_Month, 1) ) - 1
For li_Counter = 1 to ii_RowsToInsert
	dw_detail.InsertRow(0)
Next
If ii_SelectedDate > 0 Then ii_SelectedDate += ii_RowsToInsert

ls_Input = is_CountryCode + '~t' + String(li_Month, '00') + '~t' + &
				String(li_Year)

istr_error_info.se_event_name = 'wf_retrieve'


//If Not iu_orp002.nf_orpo73ar_inq_monthly_exch_rates(istr_error_info, &
//						ls_Input, &
//						ls_Output) Then return False
						
If Not iu_ws_orp3.uf_orpo73fr_inq_monthly_exch_rates(istr_error_info, &
						ls_Input, &
						ls_Output) Then return False						

If ii_SelectedDate = 0 Then
	ii_SelectedDate = Day(Today()) + ii_RowsToInsert
End if
If ii_SelectedDate < ii_RowsToInsert Then
	ii_SelectedDate = ii_RowsToInsert + 1
End if


If dw_detail.ImportString(ls_Output) < ii_SelectedDate Then
	dw_detail.SetRow(dw_detail.RowCount())
Else
	dw_detail.SetRow(ii_SelectedDate)
End if



return True
end function

public function integer wf_next ();Date	ldt_Display

Int	li_Month, &
		li_Year


ldt_Display = dw_header.GetItemDate(1, 'display_date')
li_Month = Month(ldt_Display)
li_Year = Year(ldt_Display)

If li_Month = 12 Then
	li_Month = 1
	li_Year ++
Else
	li_Month ++
End if

dw_Header.SetItem(1, 'display_date', Date(li_year, li_Month, 1))
wf_retrieve()
return 0

end function

public function integer wf_previous ();Date	ldt_Display

Int	li_Month, &
		li_Year


ldt_Display = dw_header.GetItemDate(1, 'display_date')
li_Month = Month(ldt_Display)
li_Year = Year(ldt_Display)

If li_Month = 1 Then
	li_Month = 12
	li_Year --
Else
	li_Month --
End if

dw_Header.SetItem(1, 'display_date', Date(li_year, li_Month, 1))
wf_retrieve()
return 0

end function

event open;call super::open;String	ls_Inputs, &
			ls_Price


ls_Inputs = Message.StringParm
// This should be in the following form:  Currency Code~tPrice in that Currency

iw_frame.iu_string.nf_ParseLeftRight(ls_Inputs, '~t', is_CountryCode, ls_Price)

If is_CountryCode = 'USD' Then
	dw_header.SetItem(1, 'currency_code', is_CountryCode)
	dw_header.SetItem(1, 'us_amount', Dec(ls_price))
Else
	dw_header.SetItem(1, 'foreign_amount', Dec(ls_price))
End if	


end event

event close;call super::close;If IsValid(iu_Orp002) Then Destroy iu_orp002

If IsValid(iu_ws_orp3) Then Destroy iu_ws_orp3

If Not ib_ValidToClose Then
	Message.StringParm = ''
End if
end event

event ue_postopen;call super::ue_postopen;iu_orp002 = Create u_orp002
iu_ws_orp3 = Create u_ws_orp3

istr_error_info.se_window_name = 'ExchRate'
istr_error_info.se_user_id = Message.nf_GetUserID()
istr_error_info.se_App_name = Message.nf_Get_App_ID()

wf_retrieve()
end event

on w_exchange_rate_response.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
this.st_1=create st_1
this.st_2=create st_2
this.p_next=create p_next
this.p_previous=create p_previous
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.p_next
this.Control[iCurrent+6]=this.p_previous
end on

on w_exchange_rate_response.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.p_next)
destroy(this.p_previous)
end on

event ue_base_ok;call super::ue_base_ok;string	ls_amount

ib_ValidToClose = True

dw_header.AcceptText()

If is_CountryCode = 'USD' Then
	IF IsNull(dw_header.GetItemNumber(1, 'us_amount')) THEN
		ls_amount	=	'0.00'
	ELSE
		ls_amount	=	String(dw_header.GetItemDecimal(1, 'us_amount'))
	END IF
Else
	IF IsNull(dw_header.GetItemNumber(1, 'foreign_amount')) THEN
		ls_amount	=	'0.00'
	ELSE
		ls_amount	=	String(dw_header.GetItemDecimal(1, 'foreign_amount'))
	END IF
End if
CloseWithReturn(This, ls_amount)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,'Cancel')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_exchange_rate_response
integer x = 1431
integer y = 888
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_exchange_rate_response
integer x = 1106
integer y = 892
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_exchange_rate_response
integer x = 786
integer y = 888
end type

type cb_browse from w_base_response_ext`cb_browse within w_exchange_rate_response
end type

type dw_detail from u_base_dw_ext within w_exchange_rate_response
integer x = 18
integer y = 244
integer width = 2427
integer height = 600
integer taborder = 0
string dataobject = "d_exchange_rate_calendar"
boolean border = false
end type

event clicked;call super::clicked;ii_SelectedDate = row
This.SetRow(row)

end event

event rowfocuschanged;call super::rowfocuschanged;
Decimal	ldc_Exchange, &
			ldc_US

Long	ll_SelectedRow

ll_SelectedRow = This.GetRow()
If ll_SelectedRow > 0 Then
	This.SelectRow(0, False)
	This.SelectRow(ll_SelectedRow, True)
	
	IF IsNull(This.GetItemNumber(ll_SelectedRow, 'exchange_rate')) THEN
		ldc_Exchange	=	0
	ELSE
		ldc_Exchange = This.GetItemDecimal(ll_SelectedRow, 'exchange_rate')
	END IF
	If ldc_Exchange <> 0 Then
		ldc_US = dw_header.GetItemDecimal(1, 'foreign_amount') / ldc_Exchange
	Else
		ldc_us = 0
	End if
	dw_header.SetItem(1, 'us_amount', ldc_US)

End if

end event

on constructor;call u_base_dw_ext::constructor;is_selection = '0'
end on

type dw_header from u_base_dw_ext within w_exchange_rate_response
integer x = 50
integer y = 24
integer width = 1655
integer height = 196
integer taborder = 20
string dataobject = "d_exchange_rate_header"
boolean border = false
end type

event editchanged;call super::editchanged;Decimal		ldc_Exchange

Long			ll_Row


Choose Case This.GetColumnName() 
Case 'foreign_amount' 

	ll_Row = dw_detail.GetRow()
	If ll_Row < 1 Then return
	IF IsNull(dw_detail.GetItemNumber(ll_row, 'exchange_rate')) THEN
		ldc_Exchange	=	0
	ELSE
		ldc_Exchange = dw_detail.GetItemDecimal(ll_row, 'exchange_rate')
	END IF
	If ldc_Exchange = 0 Then return

	This.SetItem(1, 'us_amount', Dec(This.GetText()) / ldc_Exchange)

Case 'us_amount'

	ll_Row = dw_detail.GetRow()
	If ll_Row < 1 Then return
	IF IsNull(dw_detail.GetItemNumber(ll_row, 'exchange_rate')) THEN
		ldc_Exchange	=	0
	ELSE
		ldc_Exchange = dw_detail.GetItemDecimal(ll_row, 'exchange_rate')
	END IF
	If ldc_Exchange = 0 Then return

	This.SetItem(1, 'foreign_amount', Dec(This.GetText()) * ldc_Exchange)
End Choose



end event

event ue_postconstructor;call super::ue_postconstructor;//DataWindowChild	ldwc_currency
//
//
//dw_header.GetChild("currency_code", ldwc_Currency)
//If IsValid( ldwc_currency ) Then
//	Message.iw_UtlData.wf_gettutltype_dwc("CURRCODE", ldwc_currency)
//End if
end event

on constructor;call u_base_dw_ext::constructor;This.InsertRow(0)
This.SetItem(1, 'Display_Date', Today())
end on

type st_1 from statictext within w_exchange_rate_response
integer x = 1842
integer y = 40
integer width = 288
integer height = 72
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Next Month"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_exchange_rate_response
integer x = 1733
integer y = 148
integer width = 398
integer height = 60
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "Previous Month"
alignment alignment = center!
boolean focusrectangle = false
end type

type p_next from picture within w_exchange_rate_response
integer x = 2153
integer y = 20
integer width = 114
integer height = 96
integer taborder = 40
string picturename = "spin1.bmp"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

on clicked;This.BorderStyle = StyleLowered!

Parent.wf_Next()

This.BorderStyle = StyleRaised!

end on

type p_previous from picture within w_exchange_rate_response
integer x = 2153
integer y = 124
integer width = 114
integer height = 96
integer taborder = 60
string picturename = "spin2.bmp"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

on clicked;This.BorderStyle = StyleLowered!

Parent.wf_Previous()

This.BorderStyle = StyleRaised!

end on

