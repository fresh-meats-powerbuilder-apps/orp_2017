HA$PBExportHeader$w_copy_order.srw
forward
global type w_copy_order from w_base_sheet_ext
end type
type st_1 from statictext within w_copy_order
end type
type sle_total_copies from singlelineedit within w_copy_order
end type
type dw_header from u_base_dw_ext within w_copy_order
end type
type dw_detail from u_base_dw_ext within w_copy_order
end type
type cb_generate from u_base_commandbutton_ext within w_copy_order
end type
end forward

global type w_copy_order from w_base_sheet_ext
integer width = 3497
integer height = 1560
string title = "Copy Order"
event ue_gensales ( )
st_1 st_1
sle_total_copies sle_total_copies
dw_header dw_header
dw_detail dw_detail
cb_generate cb_generate
end type
global w_copy_order w_copy_order

type variables
string		is_openparm, &
		is_colname


S_ERROR	istr_error_info

u_orp001		iu_orp001
u_ws_orp1 		iu_ws_orp1
u_ws_orp2       iu_ws_orp2

end variables

forward prototypes
public function boolean wf_calc_ship_date (integer as_row)
public function boolean wf_retrieve ()
public subroutine wf_default_new_row ()
public function boolean wf_update ()
public function boolean wf_validate (integer ai_row)
end prototypes

event ue_gensales;wf_update()
end event

public function boolean wf_calc_ship_date (integer as_row);String	ls_required_info, &
			ls_ship_date, &
			ls_customer, &
			ls_plant, &
			ls_delivery_date,&
			ls_Time, &
			ls_transmode, &
			ls_micro_test_product, &
			ls_micro_test_ship_type, &
			ls_type_of_sale, &
			ls_new_or_copy_order
			
u_string_functions	lu_string_functions			
			
s_error	lstr_error_info

if dw_header.GetItemString(1, "compute_ship_dates") = 'N' then return False

dw_detail.AcceptText()

ls_customer = dw_header.GetItemString(1, 'customer')
ls_transmode = dw_header.GetItemString(1, 'trans_mode')
ls_plant =  dw_detail.GetItemString(as_row, 'plant')
ls_delivery_date = String(dw_detail.GetItemDate(as_row, 'delivery_date'), "yyyy-mm-dd")
ls_time = dw_header.GetItemString(1, 'delivery_time')

//slh SR25459 
ls_micro_test_ship_type = dw_header.GetItemString(1, 'micro_test_ship_type')
ls_type_of_sale = dw_header.GetItemString(1, 'type_of_sale') 
ls_micro_test_product = 'N' //place holder
ls_new_or_copy_order = 'C' //Copy

If lu_string_functions.nf_IsEmpty(ls_plant) Then Return False
If lu_string_functions.nf_IsEmpty(ls_delivery_date) Then Return False

//slh SR25459 - add ls_type_of_sale, ls_micro_test_ship_type, ls_micro_test_product, ls_new_or_copy_order
ls_required_info = ls_customer + '~t' + &
						ls_TransMode + '~t' + &
						ls_plant + '~t' + &
						ls_delivery_date + '~t' + &
						ls_time + '~t' + &
						ls_type_of_sale + '~t' + &
						ls_micro_test_ship_type + '~t' + &
						ls_micro_test_product + '~t' + &
						ls_new_or_copy_order

If Not IsValid(iu_orp001) Then
	iu_orp001 = Create u_orp001
End if

lstr_error_info.se_window_name = 'Add.Data'
lstr_error_info.se_event_name = 'wf_calculate_ship_date'
lstr_error_info.se_message = Space(70)

//iu_orp001.nf_orpo18ar(lstr_error_info, &
//							ls_required_info, &
//							ls_ship_date)
							
iu_ws_orp1.nf_orpo18fr(lstr_error_info, &
							ls_required_info, &
							ls_ship_date)																		
							
SetPointer(Arrow!)							

IF Not lu_string_functions.nf_IsEmpty(ls_ship_date) Then
	dw_detail.SetItem(as_row, 'ship_date', Date(ls_ship_date))
Else
	dw_detail.SetItem(as_row, 'protect_ind', 'N')
End IF

return True
end function

public function boolean wf_retrieve ();			
This.TriggerEvent(CloseQuery!)

if Message.ReturnValue = 1 Then
	Return False
end if
			
OpenWithParm( w_copy_order_inq, is_openparm)
is_openparm = Message.StringParm

IF Upper(is_openparm) = "ABORT" Then
	Close(This)
	Return FALSE
ELSE
	This.PostEvent("ue_Query", 0, is_openparm)
	Return TRUE
END IF


end function

public subroutine wf_default_new_row ();

dw_detail.SetItem(dw_detail.RowCount(),'protect_ind',dw_header.GetItemString(1,'compute_ship_dates'))

dw_detail.SetItem(dw_detail.RowCount(), 'price_date_ind', &
                     dw_header.GetItemString(1, 'price_date_ind'))
							
If dw_header.GetItemString(1, 'price_date_ind') = 'P' & 
        or dw_header.GetItemString(1, 'price_date_ind') = 'D' Then
	dw_detail.SetItem(dw_detail.RowCount(), 'price_date', dw_header.GetItemDate(1,'price_date'))
end if
end subroutine

public function boolean wf_update ();DataWindowChild	ldwc_messages

String		ls_header_string_in, &
				ls_detail_string_in, &
				ls_detail_string_out, &
				ls_temp, &
				ls_find_string	
				
Integer		li_rtn, & 
				li_sub, &
				li_count

Long			ll_RowCount, &
				li_sum_total, &
				ll_rtn, &
				ll_found, &
				ll_total_copies

If dw_detail.AcceptText() = -1 Then return False

ll_RowCount = dw_detail.RowCount()

//For li_sub = 1 to ll_RowCount
//	ls_temp = dw_detail.GetItemString(li_sub, 'plant')
//		if ((ls_Temp  <= '   ') or isnull(ls_temp)) then
//			MessageBox('Plant error', 'Plant is a required field')
//			dw_detail.setColumn('plant')
//			dw_detail.SetRow(li_sub)
//			Return False
//		end if
//Next

For li_sub = 1 to ll_RowCount
	if not isnull(dw_detail.GetItemNumber(li_sub, "copies")) then
			li_sum_total = li_sum_total + dw_detail.GetItemNumber(li_sub, "copies")
	end if
Next

if integer(sle_total_copies.text) <= 0 then
	MessageBox("Total copies error", "Total copies must be greater than zero")
	Return False
end if

if li_sum_total = Integer(sle_total_copies.Text) then
	// continue
else
	MessageBox ("Total copies error", "Number of copies " + &
	String(li_sum_total) + " does not match total copies " + sle_total_copies.text)
	Return False
end if	

//dmk - validate price date
If dw_header.GetItemString(1, 'price_date_ind') = 'P' Then
	//do nothing
else
	for li_count = 1 to ll_RowCount
		If Not wf_validate(li_count) Then 
			This.SetRedraw(True) 
			Return False
		end if
		if dw_detail.getItemDate(li_count, "price_date") <> dw_header.getItemDate(1, "price_date") Then
			dw_detail.Setitem(li_count, 'price_date_ind', 'M')
		end if
	next
end if

ls_header_string_in = dw_header.Describe("DataWindow.Data") + '~t' + sle_total_copies.Text + '~t'

ls_detail_string_in = dw_detail.Describe("DataWindow.Data")

dw_detail.GetChild( "messages" ,ldwc_messages)

dw_detail.SetRedraw(false)

//li_rtn = iu_orp003.nf_orpo29br(istr_error_info, &
//										ls_header_string_in, &
//										ls_detail_string_in, & 
//										ls_detail_string_out)

li_rtn = iu_ws_orp2.nf_orpo29fr(ls_header_string_in, &
										ls_detail_string_in, & 
										ls_detail_string_out, &
										istr_error_info)
										
										
										
ldwc_messages.Reset()											
											
ll_rtn = ldwc_messages.ImportString(ls_detail_string_out)

For li_sub = 1 to ll_RowCount - 1
	ls_find_string = "line_number = " + string(li_sub)
	ll_found = ldwc_messages.Find(ls_find_string,1, 100)
	if ll_found > 0 then
		dw_detail.SetItem(li_sub, "freight_rate", ldwc_messages.GetItemNumber(ll_found, "sales_freight_rate"))
		dw_detail.SetItem(li_sub, "messages", ldwc_messages.GetItemString(ll_found, "sales_message"))
		dw_detail.SetItem(li_sub, "error_indicators", ldwc_messages.GetITemString(ll_found, "error_flags"))
	end if
	if not isnull(dw_detail.GetItemSTring(li_sub,"messages")) then
		if mid(dw_detail.GetItemSTring(li_sub,"messages"),1,5) > '     ' then
			ls_temp = dw_detail.GetItemSTring(li_sub,"messages")
			if dw_detail.GetItemSTring(li_sub,"error_indicators") = 'NNNNNN' then
				dw_detail.SetItem(li_sub, "copies", 0)
			end if
		end if
	end if	
Next

ll_total_copies = 0
For li_sub = 1 to ll_RowCount
	If not isnull(dw_detail.getitemnumber(li_sub,"copies")) then
		ll_total_copies += dw_detail.getitemnumber(li_sub,"copies")
	end if
Next
sle_total_copies.text = String(ll_total_copies)

dw_detail.object.messages.Protect = 0

dw_detail.SetItem(dw_detail.RowCount(), 'price_date_ind', &
                     dw_header.GetItemString(1, 'price_date_ind'))
							
If dw_header.GetItemString(1, 'price_date_ind') = 'P' & 
        or dw_header.GetItemString(1, 'price_date_ind') = 'D' Then
	dw_detail.SetItem(dw_detail.RowCount(), 'price_date', dw_header.GetItemDate(1,'price_date'))
end if

dw_detail.SetRedraw(true)

SetPointer(Arrow!)

Return True
end function

public function boolean wf_validate (integer ai_row);Date		ldt_price_date
String 	ls_header_price_date
			
u_string_functions	lu_string_functions			
			
s_error	lstr_error_info

dw_detail.AcceptText()

ldt_price_date = dw_detail.getItemDate(ai_row, "price_date")

If dw_header.GetItemString(1, 'price_date_ind') = 'D' Then
	if ldt_price_date < dw_header.getItemDate(1, "price_date") &
		and String(ldt_price_date, 'MM/DD/YYYY') <> '00/00/0000' Then
		ls_header_price_date = String(dw_header.getItemDate(1, "price_date"),'MM/DD/YYYY') 
		iw_frame.setmicrohelp("Price Date must be equal to or greater than original order price date," + &
				ls_header_price_date + " or equal to '00/00/0000'.")
		This.SetRedraw(False)
		dw_detail.ScrollToRow(ai_row)
		dw_detail.SelectRow(ai_row, True)
		dw_detail.SetColumn("price_date")
		dw_detail.SetFocus()
		This.SetRedraw(True)
		return false		
	end if
Else 

	if ldt_price_date < today() and String(ldt_price_date, 'MM/DD/YYYY') <> '00/00/0000' then
		iw_frame.setmicrohelp("Price Date must be equal to or greater than current date or equal to '00/00/0000'.")
		This.SetRedraw(False)
		dw_detail.ScrollToRow(ai_row)
		dw_detail.SelectRow(ai_row, True)
		dw_detail.SetColumn("price_date")
		dw_detail.SetFocus()
		This.SetRedraw(True)
		return false		
	end if
end if


Return True

end function

on w_copy_order.create
int iCurrent
call super::create
this.st_1=create st_1
this.sle_total_copies=create sle_total_copies
this.dw_header=create dw_header
this.dw_detail=create dw_detail
this.cb_generate=create cb_generate
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.sle_total_copies
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.dw_detail
this.Control[iCurrent+5]=this.cb_generate
end on

on w_copy_order.destroy
call super::destroy
destroy(this.st_1)
destroy(this.sle_total_copies)
destroy(this.dw_header)
destroy(this.dw_detail)
destroy(this.cb_generate)
end on

event ue_query;call super::ue_query;DataWindowChild	ldwc_messages

Integer	li_rtn

String	ls_input, &
			ls_output
			
u_project_functions	lu_project_functions			

istr_error_info.se_event_name = "Ue_Query"
istr_error_info.se_procedure_name = "orpo28fr"

sle_total_copies.Text = '' 

ls_input = is_openparm

if ls_input > '' then
	//li_rtn = iu_orp003.nf_orpo28br( istr_error_info, &
	//									ls_input, &
	//									ls_output)
	
	li_rtn = iu_ws_orp2.nf_orpo28fr(ls_input, &
										ls_output, &
										istr_error_info)
										
end if										
										
if li_rtn = 0 then
	dw_header.Reset()
	dw_header.Importstring(ls_output)
	dw_header.AcceptText()
	dw_header.ResetUpdate()
else
	MessageBox("Sales Order ID Error", "Sales Order ID is not valid")
	wf_Retrieve()
	Return
end if

IF (dw_header.GetItemString( 1, "sales_location") <> Message.is_smanlocation) &
  	AND (Message.is_smanlocation <> '630' AND Message.is_smanlocation <> '606' AND Message.is_smanlocation <> '006') & 
   AND ( NOT lu_project_functions.nf_isscheduler() )	&
	AND ( NOT lu_project_functions.nf_isschedulermgr() )	&
	AND ( NOT lu_project_functions.nf_issalesmgr() ) THEN 

	dw_header.Reset()
	dw_header.InsertRow(0)
	MessageBox("Warning", "Your location does not match the orders location.~r~nYou may not generate copies.")

	Return
end if

dw_detail.Reset()
dw_detail.InsertRow(0)

dw_detail.GetChild( "messages" ,ldwc_messages)

ldwc_messages.Reset()

dw_detail.object.messages.Protect = 1
											
dw_detail.SetFocus()
dw_detail.SetColumn(1)
dw_detail.SetRow(1)


dw_detail.SetItem(dw_detail.RowCount(), 'price_date_ind', &
                     dw_header.GetItemString(1, 'price_date_ind'))
							
If dw_header.GetItemString(1, 'price_date_ind') = 'P' & 
        or dw_header.GetItemString(1, 'price_date_ind') = 'D' Then
	dw_detail.SetItem(dw_detail.RowCount(), 'price_date', dw_header.GetItemDate(1,'price_date'))
end if

//slh sr25459
If dw_header.GetItemString(1, 'micro_test_ship_type') = 'H' Then
	dw_header.SetItem(1, 'compute_ship_dates', 'Y')
end if

dw_detail.AcceptText()

SetMicroHelp("Ready")

	
	
end event

event open;call super::open;iu_orp003 = Create u_orp003
iu_ws_orp2 = Create u_ws_orp2

end event

event close;call super::close;Destroy( iu_orp003)
Destroy(iu_ws_orp2)
Destroy iu_ws_orp1
end event

event ue_postopen;call super::ue_postopen;
istr_error_info.se_window_name = 'SODetail'
istr_error_info.se_User_ID = SQLCA.UserID
istr_error_info.se_app_name = Message.nf_Get_App_ID()

iu_ws_orp1 = Create u_ws_orp1
iu_ws_orp2 = Create u_ws_orp2

wf_retrieve()


end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_cancelorder')
iw_frame.im_menu.mf_enable('m_generatesalesorder')
iw_frame.im_menu.mf_disable('m_padisplay')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable("m_complete")
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_cancelorder')
iw_frame.im_menu.mf_disable('m_generatesalesorder')
iw_frame.im_menu.mf_enable('m_padisplay')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable("m_complete")
end event

event resize;call super::resize;dw_detail.Resize(	3497, 612)
						

end event

type st_1 from statictext within w_copy_order
integer x = 704
integer y = 1292
integer width = 320
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Total Copies"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_total_copies from singlelineedit within w_copy_order
integer x = 1061
integer y = 1280
integer width = 128
integer height = 92
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean autohscroll = false
integer limit = 2
borderstyle borderstyle = stylelowered!
end type

type dw_header from u_base_dw_ext within w_copy_order
integer x = 14
integer width = 2848
integer height = 560
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_copy_order_header"
end type

event constructor;call super::constructor;u_project_functions	lu_project_functions

DataWindowChild		ldwc_trans_mode, &
							ldwc_credit_status, &
							ldwc_type_of_order
							
This.InsertRow(0)							

This.GetChild('trans_mode', ldwc_trans_mode)
lu_project_functions.nf_gettutltype(ldwc_trans_mode, 'TRANMODE')
This.SetItem(1, 'trans_mode', 'T')

This.GetChild('credit_status', ldwc_credit_status)
lu_project_functions.nf_gettutltype(ldwc_credit_status, 'CREDSTAT')
This.SetItem(1, 'credit_status', 'A')

This.GetChild('type_of_order', ldwc_type_of_order)
ldwc_type_of_order.SetTransObject(SQLCA)
ldwc_type_of_order.Retrieve()
This.SetItem(1, 'type_of_order', 'N')


end event

event itemchanged;call super::itemchanged;Long	 ll_RowCount

Integer	li_sub

if This.GetColumnName() = 'compute_ship_dates' then
	ll_RowCount = dw_detail.RowCount()
	
	for li_sub = 1 to ll_RowCount
		dw_detail.SetItem(li_sub, 'protect_ind', data)	
	next
	
	dw_detail.AcceptText()
end if

end event

type dw_detail from u_base_dw_ext within w_copy_order
integer y = 576
integer width = 3433
integer height = 528
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_copy_order_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;This.InsertRow(0)

ib_updateable = True
ib_NewRowOnChange = True
ib_firstcolumnonnextrow = True

is_colname = 'plant'


end event

event ue_dwndropdown;call super::ue_dwndropdown;DataWindowChild	ldwc_temp

long					ll_error

String				ls_column, &
						ls_temp
						
Integer				li_rtn							

u_project_functions lu_project_functions

ls_column = This.GetColumnName()
Choose Case  ls_column
Case 'plant'
		This.GetChild('plant', ldwc_temp)
		If ldwc_temp.RowCount() < 1 Then
			ll_error = ldwc_temp.SetTransObject( SQLCA)
			ll_error = ldwc_temp.Retrieve()
		End If
		
Case 'messages'
		This.GetChild('messages', ldwc_temp)
		ldwc_temp.SetFilter("")
		ldwc_Temp.Filter()
		ls_Temp = String(This.GetRow())
		ldwc_Temp.SetFilter("line_number = " + ls_Temp)
		ldwc_temp.Filter()
	
End Choose



end event

event clicked;
// overriding ancester code


DataWindowCHild	ldwc_Temp

String 				ls_line_number

Long					ll_clickedrow

ll_clickedrow = row

IF ll_clickedrow < 1 THEN RETURN

If dwo.name = 'messages' Then
//	MessageBox('PointerX', String(PointerX(this)))
   if PointerX(This) < 2684 then
		Return
	else
//		MessageBox('hit ue_dwndropdown', 'hit ue_dwndropdown')
		This.TriggerEvent("ue_dwndropdown")
	end if
END IF
end event

event itemchanged;call super::itemchanged;// Overrides ancester script //

String				ls_ColumnName, &
						ls_temp
DataWindowChild	ldwc_temp	

long					ll_row, &
						ll_RowFound, &
						ll_num_rows, &
						ll_counter, &
						ll_total_copies
						
Integer				li_copies						
						
ls_ColumnName = dwo.Name

IF This.ib_NewRowOnChange AND (This.il_CurrentRow + 1 = This.RowCount()) THEN
	wf_default_new_row()
END IF

if ls_ColumnName = 'plant' then
	This.GetChild('plant', ldwc_temp)
	
	// Find the entered plant code in the table -- 
	ll_row =  ldwc_temp.RowCount()
	if ll_row = 0 then
		ldwc_temp.SetTransObject( SQLCA)
		ldwc_temp.Retrieve()
	end if
	ls_temp = "location_code = ~"" + Trim(data) + "~""
	ll_RowFound = ldwc_temp.Find(ls_temp, 0, ldwc_temp.RowCount())
	If  ll_RowFound <= 0 Then 
		MessageBox('Invalid Data', data + ' is an invalid plant code')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF	
end if

if ls_ColumnName = 'plant' then
	Parent.wf_calc_ship_date(row)
end if

if ls_ColumnName = 'delivery_date' then
	Parent.wf_calc_ship_date(row)
end if

if ls_Columnname = 'copies' then
	li_copies = integer(data)
	if li_copies < 0 then
		MessageBox('Invalid Data', 'Copies cannot be negative')
		This.SelectText(1, Len(data))
		RETURN	1
	else
		ll_num_rows = This.RowCount() 
		For ll_counter = 1 to ll_num_rows
			if ll_counter = row then
				ll_total_copies += integer(data)
			else
				If not isnull(this.getitemnumber(ll_counter,"copies")) then
					ll_total_copies += this.getitemnumber(ll_counter,"copies")
				end if
			end if
		Next
		sle_total_copies.text = String(ll_total_copies)
	end if
end if




end event

event ue_insertrow;call super::ue_insertrow;String	ls_temp
Long		ll_long1, &
			ll_long2	

ls_temp = dw_header.GetItemString(1, 'calc_ship_dates')
ll_long1 = lparam
ll_long2 = This.RowCount()

//this.SetItem(This.RowCount(),'protect_ind',dw_header.GetItemString(1, 'calc_ship_dates'))
wf_default_new_row()
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;String	ls_temp

IF row > 0 THEN

	if is_colname = 'plant' and ((dwo.name = 'delivery_date') or (dwo.name = 'ship_date')) then
		ls_temp = this.GetItemString(row, 'plant')
		if (this.GetItemString(row, 'plant') <= '   ') or isnull(this.GetItemString(row, 'plant')) then
			MessageBox('Plant error', 'Plant is a required field')
			This.setColumn('plant')
			This.SetRow(row)
			Return 1
		end if
	end if
end if
	
is_colname = dwo.Name	
end event

event doubleclicked;call super::doubleclicked;String	ls_so_id

u_string_functions	lu_string_functions

Window	lw_detail

Choose Case Lower( dwo.Name)
	Case 'messages'
		ls_so_id = mid(This.GetItemString(Row, 'messages'),1,5)
		If lu_string_functions.nf_IsEmpty(ls_so_id) Then return
		SetPointer(HourGlass!)
		OpenSheetWithParm(lw_detail, ls_so_id, 'w_sales_order_detail', &
							iw_frame, 8, iw_Frame.im_menu.iao_ArrangeOpen)
		
End Choose		


end event

type cb_generate from u_base_commandbutton_ext within w_copy_order
integer x = 1618
integer y = 1276
integer width = 535
integer height = 108
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
string facename = "MS Sans Serif"
string text = "Generate Orders"
end type

event clicked;call super::clicked;parent.wf_update()
end event

