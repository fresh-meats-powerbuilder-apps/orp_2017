HA$PBExportHeader$uo_d_customer_info.sru
$PBExportComments$Display common Customer Information. (has logic to automaitcally change customer name, city, cust id). Used on w_sales_order_header, w_sales_order_detail. w_instructions
forward
global type uo_d_customer_info from u_base_dw_ext
end type
end forward

global type uo_d_customer_info from u_base_dw_ext
integer width = 2917
integer height = 252
string dataobject = "d_customer_info"
boolean border = false
event ue_postopen pbm_custom01
end type
global uo_d_customer_info uo_d_customer_info

type prototypes

end prototypes

type variables
String 	is_order_id, &
	is_weight_string,&
	is_Location

u_ws_orp2	iu_ws_orp2
	

end variables

forward prototypes
public function string uof_getsalesid ()
public function boolean uof_retrieve ()
public function boolean uof_set_sales_id (string arg_sales_orderid)
public function string uof_get_customer_id ()
public function boolean is_incomplete ()
public function string uof_get_weight_string ()
public subroutine uof_gettypedesc (ref datawindowchild adw_dddwchild)
public function string uof_get_location ()
public function string uof_getstatus ()
end prototypes

event ue_postopen;InsertRow(0)


end event

public function string uof_getsalesid ();Message.StringParm = Fill(Char(0), 10)
Message.StringParm = is_order_id

RETURN is_order_id
end function

public function boolean uof_retrieve ();DataStore	lds_Temp
integer				li_rtn

Long					ll_Row
 
s_error				lstr_ErrorInfo

string				ls_CustomerInfo

w_base_sheet_ext	lw_parent 

u_project_functions	lu_project_functions	

iu_ws_orp2 = Create u_ws_orp2

lw_parent = iw_parent

This.SetRedraw(FALSE)
lds_Temp = Create DataStore
lds_Temp.DataObject = 'd_customer_info'
// Initialize the string variables that will be passed by reference
// to the Netwise external functions
ls_CustomerInfo = Space(101)
// iNIT NETWISE ERROR MESSAGING
lstr_errorinfo.se_event_name = "uof_retrieve"
lstr_errorinfo.se_window_name = "uo_d_customer_info"
lstr_errorinfo.se_App_Name = "ORP"
lstr_errorinfo.se_Function_Name = "orpo34ar_Get_Order_Cust_Info"
ls_customerinfo	=	is_order_id
// Call a Netwise external function to get the customer information

//li_rtn = lw_parent.iu_orp003.nf_orpo34ar( lstr_ErrorInfo, ls_CustomerInfo, is_weight_string)

li_rtn =iu_ws_orp2.nf_orpo34fr(ls_CustomerInfo, is_weight_string, lstr_ErrorInfo)

// Before importing the tab-delimited strings into the datawindows,
// reset them to have no rows in them
This.Reset()
ll_Row = lds_Temp.ImportString( ls_CustomerInfo)
IF ll_Row < 1 Then 
	IF li_rtn = 1 THEN
		messagebox("ERROR","Invalid Sales Order Id")
	ELSE 
	MessageBox("Failure to import",'s_CustomerInfo') 
	END IF
ELSE
	is_location = lds_Temp.GetItemString( 1,"smanlocaton")
END IF
Destroy( lds_Temp)
// Import the tab-delimets strings into the datawindows
ll_row = This.ImportString(Trim(ls_CustomerInfo))
IF li_rtn = 1 THEN
	ll_Row = -1
END IF 
IF  ll_Row < 1 THEN
   This.InsertRow(0)
	This.SetRedraw(True) 
	Return FALSE
ELSE
	This.ResetUpdate()
END IF
This.SetRedraw(True) 

if lu_project_functions.nf_isscheduler() then
	iw_frame.im_menu.mf_disable("m_save")
else
	Choose Case This.uof_GetStatus() 
		Case 'N','M','C'
			iw_frame.im_menu.mf_disable("m_save")
		Case Else
			iw_frame.im_menu.mf_Enable("m_save")
	END Choose
end if

Return True
end function

public function boolean uof_set_sales_id (string arg_sales_orderid);int li_set_item_rtn
is_order_id = arg_sales_orderid
li_set_item_rtn = This.SetItem(1,"order_id", arg_sales_orderID)
IF li_set_item_rtn = -1 THEN
   RETURN FALSE
ELSE
   RETURN TRUE
END IF


end function

public function string uof_get_customer_id ();Return This.GetItemString( 1,"customer_id")

end function

public function boolean is_incomplete ();u_string_functions	lu_string_functions

If lu_string_functions.nf_IsEmpty( This.GetItemString( 1, "order_id")) Then 	Return False

IF lu_string_functions.nf_IsEmpty(This.GetItemString( 1, "order_status")) Then
	Return True
ELSE
	Return False
END IF
end function

public function string uof_get_weight_string ();
RETURN Trim(is_weight_string)
end function

public subroutine uof_gettypedesc (ref datawindowchild adw_dddwchild);IF adw_dddwchild.RowCount()	> 0 THEN RETURN
adw_dddwchild.SetTransObject(SQLCA)
adw_dddwchild.Retrieve()

end subroutine

public function string uof_get_location ();return is_location

end function

public function string uof_getstatus ();Return This.GetItemString(1,"order_status")
end function

event itemchanged;call super::itemchanged;datawindowchild	ldw_ChildDW

integer				li_rtn

Long					ll_RowFound

String 				ls_FindExpression 

is_ColumnName = This.GetColumnName()
CHOOSE CASE Upper(is_ColumnName)
	CASE	"CUSTOMER_ID"
		This.GetChild(is_ColumnName, ldw_ChildDW)
		ls_FindExpression = "Trim(Upper(customer_id)) = ~"" + Trim(Upper(data)) + "~""
		ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
		If  ll_RowFound > 0 Then
			li_rtn = This.SetItem(row, "short_name", &
				ldw_ChildDW.GetItemString(ll_RowFound, "short_name"))
			li_rtn = This.SetItem(row, "city", &
				ldw_ChildDW.GetItemString(ll_RowFound, "city"))
			li_rtn = This.SetItem(row, "state", &
				ldw_ChildDW.GetItemString(ll_RowFound, "state"))
		End If
	CASE "SHORT_NAME"
		This.GetChild(is_ColumnName, ldw_ChildDW)
		ls_FindExpression = "Trim(Upper(short_name)) = ~"" + Trim(Upper(data)) + "~""
		ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
		If  ll_RowFound > 0 Then
			li_rtn = This.SetItem(row, "customer_id", &
				ldw_ChildDW.GetItemString(ll_RowFound, "customer_id"))
			li_rtn = This.SetItem(row, "city", &
				ldw_ChildDW.GetItemString(ll_RowFound, "city"))
		End If
	CASE	'ORDER_ID'
		is_Order_ID = data		
	CASE	'SMANNAME'
		This.SetItem( 1, "smancode", data)
END CHOOSE
end event

event ue_dwndropdown;call super::ue_dwndropdown;String				ls_ColumnName, &
						ls_location

DataWindowChild	ldw_DDDWChild

ls_ColumnName	=	Upper(This.GetColumnName())
ls_location		=	Message.nf_getlocation()
CHOOSE CASE ls_ColumnName
	CASE	'SMANNAME'
		This.GetChild("smanname", ldw_DDDWChild)
		IF ldw_DDDWChild.RowCount() > 1 THEN RETURN
		ldw_DDDWChild.SetTransObject(SQLCA)
		ldw_DDDWChild.Retrieve(ls_location, ls_location)
	CASE	'CUSTOMER_ID'
		This.GetChild("customer_id", ldw_DDDWChild)
		IF ldw_DDDWChild.RowCount() > 1 THEN RETURN
		ldw_DDDWChild.SetTransObject(SQLCA)
		iw_frame.iu_project_functions.ids_customers.ShareData(ldw_DDDWChild)
	CASE	'SHORT_NAME'
		This.GetChild("short_name", ldw_DDDWChild)
		IF ldw_DDDWChild.RowCount() > 1 THEN RETURN
		ldw_DDDWChild.SetTransObject(SQLCA)
		iw_frame.iu_project_functions.ids_customers.ShareData(ldw_DDDWChild)
END CHOOSE
end event

event ue_postconstructor;call super::ue_postconstructor;datawindowchild	ldw_dddwchild

This.GetChild("currency_code", ldw_DDDWChild)
uof_gettypedesc(ldw_DDDWChild)
end event

on uo_d_customer_info.create
call super::create
end on

on uo_d_customer_info.destroy
call super::destroy
end on

