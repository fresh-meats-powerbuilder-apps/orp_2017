HA$PBExportHeader$uo_ext_sdk_calls.sru
$PBExportComments$uo to hold sdk calls
forward
global type uo_ext_sdk_calls from nonvisualobject
end type
end forward

global type uo_ext_sdk_calls from nonvisualobject
end type
global uo_ext_sdk_calls uo_ext_sdk_calls

type prototypes
PRIVATE:
Function Uint SetFocus( Uint WindowHandle) Library "User.Exe"
Function Uint FindWindow( String Classname, String WindowTitle) Library "User.Exe" alias for "FindWindow;Ansi"
Function int EnableHardwareInput(Boolean lb_val) library "user.exe"
Function Uint SetActiveWindow( Uint ActiveWindowHandle) Library "User.exe"
end prototypes

forward prototypes
public function integer uof_mouse_enabled (boolean ab_switch)
public function boolean uof_setactiveapp (string app_name, string as_ini)
end prototypes

public function integer uof_mouse_enabled (boolean ab_switch);// This function disables the mouse and keyboard use with caution
Int li_ret_val
IF Ab_Switch Then
   SetPointer( Arrow!)
ELSE
	SetPointer( HourGlass!)
END IF
li_ret_val =  EnableHardwareInput( ab_switch)
Return li_ret_val

end function

public function boolean uof_setactiveapp (string app_name, string as_ini);Uint	lu_WindowHandle
String ls_ClassName,&
	  ls_WindowTitle

SetNull( ls_classname)
ls_WindowTitle = Trim(ProfileString ( iw_frame.is_userini, message.nf_Get_App_ID(), "FrameWindowTitle", ""))
lu_WindowHandle  = FindWindow( ls_classname, ls_WindowTitle)
IF IsNull( lu_WindowHandle) Then Return False
IF SetActiveWindow( lu_WindowHandle) = lu_WindowHandle Then Return FALSE
IF IsNUll( SetFocus( lu_WindowHandle)) Then Return False
Return TRUE
end function

on uo_ext_sdk_calls.create
TriggerEvent( this, "constructor" )
end on

on uo_ext_sdk_calls.destroy
TriggerEvent( this, "destructor" )
end on

