HA$PBExportHeader$w_tla_inquiry_inq.srw
forward
global type w_tla_inquiry_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_tla_inquiry_inq
end type
type dw_division from u_division within w_tla_inquiry_inq
end type
type dw_ship_date from u_ship_date within w_tla_inquiry_inq
end type
end forward

global type w_tla_inquiry_inq from w_base_response_ext
int X=425
int Y=496
int Width=2455
int Height=404
boolean TitleBar=true
string Title="Trim Load Alert Inquiry Inquire"
long BackColor=12632256
dw_plant dw_plant
dw_division dw_division
dw_ship_date dw_ship_date
end type
global w_tla_inquiry_inq w_tla_inquiry_inq

type variables
w_base_sheet_ext		iw_parentwindow

Boolean	ib_valid_return
end variables

on w_tla_inquiry_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_division=create dw_division
this.dw_ship_date=create dw_ship_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_ship_date
end on

on w_tla_inquiry_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_division)
destroy(this.dw_ship_date)
end on

event open;call super::open;iw_parentwindow = Message.PowerObjectParm
end event

event ue_base_ok;String 	ls_location_code, &
			ls_location_text, &
			ls_division_code, &
			ls_division_text, &
			ls_ship_date

If dw_division.AcceptText() = -1 then return
If dw_plant.AcceptText() = -1 then return
If dw_ship_date.AcceptText() = -1 then return

ls_division_code = dw_division.uf_get_division()
ls_division_text = dw_division.uf_get_division_descr()

ls_location_code = dw_plant.uf_get_plant_code()
ls_location_text = dw_plant.uf_get_plant_descr()

ls_ship_date = String(dw_ship_date.uf_get_ship_date(), 'mm/dd/yyyy')

iw_parentwindow.Event ue_set_data('location_code',ls_location_code)
iw_parentwindow.Event ue_set_data('location_text',ls_location_text)
iw_parentwindow.Event ue_set_data('division_code',ls_division_code)
iw_parentwindow.Event ue_set_data('division_text',ls_division_text)
iw_parentwindow.Event ue_set_data('ship_date',ls_ship_date)

ib_valid_return = True

CloseWithReturn(This, "OK")
end event

event ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_tla_inquiry_inq
int X=1856
int TabOrder=70
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_tla_inquiry_inq
int X=2149
int Y=176
int TabOrder=60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_tla_inquiry_inq
int X=2149
int Y=60
int TabOrder=50
end type

type dw_plant from u_plant within w_tla_inquiry_inq
int X=37
int Y=176
int Width=1362
int Height=80
int TabOrder=30
boolean BringToTop=true
end type

type dw_division from u_division within w_tla_inquiry_inq
int X=9
int Y=64
int Width=1390
int TabOrder=20
boolean BringToTop=true
end type

type dw_ship_date from u_ship_date within w_tla_inquiry_inq
int X=1454
int Y=68
int TabOrder=40
boolean BringToTop=true
end type

