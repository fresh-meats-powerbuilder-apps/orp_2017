HA$PBExportHeader$w_weight_criteria.srw
$PBExportComments$window popup with ou for weight criteria
forward
global type w_weight_criteria from w_base_response_ext
end type
type dw_main from datawindow within w_weight_criteria
end type
end forward

global type w_weight_criteria from w_base_response_ext
int Width=1111
int Height=693
boolean TitleBar=true
string Title="Product Shortage Response"
long BackColor=12632256
dw_main dw_main
end type
global w_weight_criteria w_weight_criteria

event open;call super::open;dw_main.InsertRow(0)

end event

on w_weight_criteria.create
int iCurrent
call w_base_response_ext::create
this.dw_main=create dw_main
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_main
end on

on w_weight_criteria.destroy
call w_base_response_ext::destroy
destroy(this.dw_main)
end on

event ue_base_ok;call super::ue_base_ok;
CloseWithReturn(This, dw_main.GetItemString(1, "send_to"))

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "ABORT")

end event

event ue_postopen;call super::ue_postopen;dw_main.SetItem(1, 'gross_weight', Message.StringParm)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_weight_criteria
int X=709
int Y=473
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_weight_criteria
int X=412
int Y=473
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_weight_criteria
int X=115
int Y=473
end type

type dw_main from datawindow within w_weight_criteria
event ue_lbuttondown pbm_lbuttondown
event ue_lbuttonup pbm_lbuttonup
event ue_optionchanged pbm_custom23
int Y=9
int Width=1084
int Height=437
int TabOrder=1
string DataObject="d_weight_criteria"
boolean Border=false
boolean LiveScroll=true
end type

on ue_lbuttondown;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE UPPER(ls_ObjectName)
CASE	"OK", "CANCEL"
	This.Modify(ls_ObjectName + ".Border='5'")
	This.PostEvent("ue_" + ls_ObjectName)
END CHOOSE
end on

on ue_lbuttonup;STRING	ls_ObjectString, &
			ls_ObjectName

ls_ObjectString = This.GetObjectAtPointer()
ls_ObjectName = LEFT(ls_ObjectString, POS(ls_ObjectString, "~t") - 1)

CHOOSE CASE ls_ObjectName
CASE	"Ok", "Cancel"
	This.Modify(ls_ObjectName + ".Border='6'")
END CHOOSE
end on

event ue_optionchanged;CHOOSE CASE	This.GetItemString(1, "send_to")
CASE "Y"
	iw_Frame.SetMicroHelp("Send to Scheduling.")
CASE "S"
	iw_Frame.SetMicroHelp("Send to Weight Que.")
END CHOOSE

end event

event itemchanged;TriggerEvent('ue_optionchanged')
end event

