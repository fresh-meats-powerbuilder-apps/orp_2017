HA$PBExportHeader$w_linked_orders_popup.srw
forward
global type w_linked_orders_popup from w_base_popup_ext
end type
type cb_close from commandbutton within w_linked_orders_popup
end type
type dw_orders from u_base_dw_ext within w_linked_orders_popup
end type
end forward

global type w_linked_orders_popup from w_base_popup_ext
integer y = 485
integer width = 2747
integer height = 1448
string title = "Linked Orders for Order "
cb_close cb_close
dw_orders dw_orders
end type
global w_linked_orders_popup w_linked_orders_popup

type variables
s_error		istr_error_info

u_orp001		iu_orp001

u_ws_orp3		iu_ws_orp3

String		is_original_order
end variables

forward prototypes
public subroutine wf_retrieve ()
public subroutine wf_add (string as_order, string as_new_order)
public subroutine wf_delete (string as_order)
end prototypes

public subroutine wf_retrieve ();String	ls_input, &
			ls_output_string
			
			
Integer	li_rtn

Long		ll_rtn, &
			ll_sub, &
			ll_newrow

dw_orders.SetRedraw(False)
dw_orders.Reset()

ls_input = is_original_order + "~t"
//li_rtn = iu_orp001.nf_orpo13cr_linked_orders_inq (istr_error_info, ls_input, ls_output_string)
li_rtn = iu_ws_orp3.uf_orpo13gr_linked_orders_inq (istr_error_info, ls_input, ls_output_string)

ll_rtn = dw_orders.ImportString(ls_output_string)
For ll_sub = 1 to ll_rtn
	dw_orders.SetItem(ll_sub, "command", "Delete")
Next

ll_newrow = dw_orders.InsertRow(0)
dw_orders.SetItem(ll_newrow, "command", "Add")

dw_orders.ResetUpdate()
dw_orders.SetRedraw(True)

dw_orders.SetFocus()
dw_orders.SetRow(dw_orders.RowCount())
dw_orders.SetColumn(1)

SetPointer(Arrow!)

end subroutine

public subroutine wf_add (string as_order, string as_new_order);String	ls_input

Integer	li_rtn

ls_input = 'A' + '~t' + as_order + '~t' + as_new_order + '~t' 

SetPointer(HourGlass!)

//li_rtn = iu_orp001.nf_orpo14cr_linked_orders_upd (istr_error_info, ls_input)

li_rtn = iu_ws_orp3.uf_orpo14gr_linked_orders_upd (istr_error_info, ls_input)

If li_rtn = 0 Then
	wf_retrieve()
Else
	SetPointer(Arrow!)
	MessageBox("Add Error", istr_error_info.se_message)
	dw_orders.SetFocus()
	dw_orders.SetRow(dw_orders.RowCount())
	dw_orders.SetColumn(1)	
End If


end subroutine

public subroutine wf_delete (string as_order);String	ls_input

Integer	li_rtn

ls_input = 'D' + '~t' + as_order + '~t'  

SetPointer(HourGlass!)

//li_rtn = iu_orp001.nf_orpo14cr_linked_orders_upd (istr_error_info, ls_input)

li_rtn = iu_ws_orp3.uf_orpo14gr_linked_orders_upd (istr_error_info, ls_input)

If li_rtn = 0 Then
	wf_retrieve()
Else
	SetPointer(Arrow!)
	MessageBox("Add Error", istr_error_info.se_message)
	dw_orders.SetFocus()
	dw_orders.SetRow(dw_orders.RowCount())
	dw_orders.SetColumn(1)	
End If
end subroutine

on w_linked_orders_popup.create
int iCurrent
call super::create
this.cb_close=create cb_close
this.dw_orders=create dw_orders
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_close
this.Control[iCurrent+2]=this.dw_orders
end on

on w_linked_orders_popup.destroy
call super::destroy
destroy(this.cb_close)
destroy(this.dw_orders)
end on

event ue_postopen;call super::ue_postopen;wf_retrieve()
end event

event open;call super::open;is_original_order = Message.StringParm

This.Title = This.Title + ' ' + is_original_order

iu_orp001 = Create u_orp001

iu_ws_orp3 = Create u_ws_orp3
end event

event close;call super::close;If IsValid(iu_orp001) Then Destroy iu_orp001
end event

type cb_close from commandbutton within w_linked_orders_popup
integer x = 2240
integer y = 1164
integer width = 343
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Close"
end type

event clicked;Close (w_linked_orders_popup)
end event

type dw_orders from u_base_dw_ext within w_linked_orders_popup
integer x = 37
integer y = 32
integer width = 2601
integer height = 1056
integer taborder = 10
string dataobject = "d_linked_orders"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event clicked;call super::clicked;String	ls_text

u_string_functions	lu_string_functions

if row > 0 then
	
	if dwo.name = 'b_delete' then
		wf_delete(This.GetItemString(row, "order_number"))
	end if
	
	if dwo.name = 'b_add' then
		This.AcceptText()
		if lu_string_functions.nf_IsEmpty(This.GetItemString(row, "order_number")) Then
			MessageBox("Add Error", "Please enter an order number for the Add function")
			Return
		else	
			wf_add(This.GetItemString(1, "order_number"), This.GetItemString(row, "order_number"))
		End If
	end if	
	
end if

end event

