HA$PBExportHeader$w_sales-orders-by-customer.srw
$PBExportComments$Sales Orders By Customer Window
forward
global type w_sales-orders-by-customer from w_base_sheet_ext
end type
type dw_sales-orders-by-customer_header from u_base_dw_ext within w_sales-orders-by-customer
end type
type dw_sales-orders-by-customer_detail from u_base_dw_ext within w_sales-orders-by-customer
end type
type dw_customer_data from u_base_dw_ext within w_sales-orders-by-customer
end type
end forward

global type w_sales-orders-by-customer from w_base_sheet_ext
integer width = 3913
integer height = 1604
string title = "Sales Orders by Customer"
event ue_retrieve pbm_custom02
event ue_print_fax ( )
event ue_print ( )
event ue_fax ( )
event ue_edi ( )
event ue_tla ( )
event type integer ue_prod_shrtg_que ( )
event ue_shortage_que ( )
event ue_split_orders ( )
event ue_email ( )
event ue_tla_email ( )
event ue_tla_local_print ( )
event ue_tla_view ( )
dw_sales-orders-by-customer_header dw_sales-orders-by-customer_header
dw_sales-orders-by-customer_detail dw_sales-orders-by-customer_detail
dw_customer_data dw_customer_data
end type
global w_sales-orders-by-customer w_sales-orders-by-customer

type prototypes

end prototypes

type variables
Boolean	ib_nv_created

String 	is_from_window,&
			is_OpeningString ,&
			is_Sales_ID, &
			is_shortage_ind,&
			is_shtg_qu_ind, &
			is_location_code

// Please do not touch
String	is_customer_id, & 
			is_divisional_po, &
			is_corporate_po, &
			is_division_code, &
			is_ship_date_ind, &
			is_ship_date, &
			is_to_ship_date, &
			is_product_short_que, &
			is_delivery_date_ind, &
			is_delivery_date, &
			is_to_delivery_date, &
			is_load_status_ind, &
			is_load_status, &
			is_ship_plant_ind, &
			is_ship_plant, &
			is_linked_orders_ind

Long		il_current_column

CHAR		ic_process_option_in, &
			ic_process_tla_option_in
			
Window	iw_sales_detail
u_orp002	iu_orp002
s_error	istr_error_info

u_ws_orp4    iu_ws_orp4
u_ws_orp1	  iu_ws_orp1


DataStore	ids_sales_orders_by_cust
end variables

forward prototypes
public function boolean wf_update ()
public function string wf_get_current_setting ()
public function boolean wf_notepad ()
public function string wf_getselectedorders ()
public subroutine wf_set_iw_sales_detail (w_sales_order_detail iw_window)
public function boolean wf_split_orders ()
public function boolean wf_inquire (double ad_tasknumber, integer ai_pagenumber)
public function boolean wf_retrieve ()
public subroutine wf_set_header_instance ()
end prototypes

event ue_print_fax();INTEGER	li_rtn

String	ls_order_id, ls_order_out, ls_temp, ls_customer_email

Long 	ll_row

ll_row = dw_sales-orders-by-customer_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_sales-orders-by-customer_detail.Getitemstring(ll_row, 'order_status') 
	IF ls_temp = 'A' THEN
			ls_order_id += dw_sales-orders-by-customer_detail.Getitemstring(ll_row, 'order_id') + '~t'
	END IF
	ll_row = dw_sales-orders-by-customer_detail.Getselectedrow(ll_row)
LOOP

istr_error_info.se_event_name = 'ue_print_fax'

IF NOT isValid(iu_orp002) THEN
	iu_orp002 = Create u_orp002
End IF

li_rtn = iu_ws_orp4.nf_ORPO80FR(istr_error_info, &
										ic_process_option_in, &
										ls_order_id, &
										ls_order_out, &
										ls_customer_email, &
										ic_process_tla_option_in)

dw_sales-orders-by-customer_detail.SelectRow(0, False)
il_current_column = 0
dw_sales-orders-by-customer_detail.SetColumn(6)

IF li_rtn >= 0 THEN
	This.SetRedraw(TRUE)
	Return
END IF
end event

event ue_print;call super::ue_print;ic_process_option_in = 'P'

This.triggerevent('ue_print_fax')
end event

event ue_fax;call super::ue_fax;ic_process_option_in = 'F'

This.triggerevent('ue_print_fax')
end event

event ue_edi;call super::ue_edi;ic_process_option_in = 'E'

This.triggerevent('ue_print_fax')
end event

event ue_tla();ic_process_option_in = 'A'
ic_process_tla_option_in = 'F'

This.triggerevent('ue_print_fax')

end event

event type integer ue_prod_shrtg_que();Return 1

end event

event ue_shortage_que();OpenWithParm(w_product_shortage_response, THIS)
end event

event ue_split_orders();wf_split_orders()

end event

event ue_email();string	ls_temp, ls_order_id

Long 	ll_row

ll_row = dw_sales-orders-by-customer_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_sales-orders-by-customer_detail.Getitemstring(ll_row, 'order_status') 
	IF ls_temp = 'A' THEN
			ls_order_id += dw_sales-orders-by-customer_detail.Getitemstring(ll_row, 'order_id') + '~t'
	END IF
	ll_row = dw_sales-orders-by-customer_detail.Getselectedrow(ll_row)
LOOP

if ls_order_id > ' ' then
	Openwithparm(w_email_order_confirmations, ls_order_id)
else
	iw_frame.SetMicroHelp( "Sales Order must be Accepted")
end if
end event

event ue_tla_email();ic_process_option_in = 'A'
ic_process_tla_option_in = 'E'

This.triggerevent('ue_print_fax')

end event

event ue_tla_local_print();ic_process_option_in = 'A'
ic_process_tla_option_in = 'P'

This.triggerevent('ue_print_fax')

end event

event ue_tla_view();ic_process_option_in = 'A'
ic_process_tla_option_in = 'V'

This.triggerevent('ue_print_fax')

end event

public function boolean wf_update ();s_Error	ls_Error

Double	ld_TaskNumber

Long		ll_Row, &
			ll_RowCount,&
			ll_CurrentRow

INTEGER 	li_PageNumber, &
			li_Rtn

BOOLEAN 	lb_ValidReference

STRING	ls_Header, &
			ls_Detail

ll_RowCount = dw_sales-orders-by-customer_Detail.RowCount()

IF dw_sales-orders-by-customer_detail.AcceptText() = -1 OR &
	dw_sales-orders-by-customer_header.AcceptText() = -1 Then
	lb_ValidReference = FALSE
	Return FALSE
END IF

ll_CurrentRow = dw_sales-orders-by-customer_detail.GetRow() 

ls_Error.se_User_ID = Message.nf_getuserid()
ls_Header = dw_sales-orders-by-customer_Header.Describe("DataWindow.Data")
ls_Header = ls_Header + "~tU"
dw_sales-orders-by-customer_Detail.SetRedraw(False)
dw_sales-orders-by-customer_Detail.RowsMove(1, dw_sales-orders-by-customer_Detail.RowCount(), Primary!, dw_sales-orders-by-customer_Detail,10000, Filter!)
FOR ll_Row = 1 TO ll_RowCount
	IF dw_sales-orders-by-customer_Detail.GetItemStatus(ll_Row, 0, Filter!) = DataModified! THEN
		dw_sales-orders-by-customer_Detail.RowsCopy(ll_Row, ll_Row, Filter!, dw_sales-orders-by-customer_Detail,10000, Primary!)
	END IF
NEXT		
ls_Detail = dw_sales-orders-by-customer_Detail.Describe("DataWindow.Data")
dw_sales-orders-by-customer_Detail.RowsDiscard(1, dw_sales-orders-by-customer_Detail.RowCount(), Primary!)
dw_sales-orders-by-customer_Detail.RowsMove(1, dw_sales-orders-by-customer_Detail.FilteredCount(), Filter!, dw_sales-orders-by-customer_Detail,10000, Primary!)
dw_Sales-orders-by-customer_Detail.SetRedraw(TRUE)

//li_Rtn = iu_orp003.nf_orpo42ar(ls_Error, ls_Header, ls_Detail, ld_TaskNumber, li_PageNumber)
li_Rtn = iu_ws_orp1.nf_orpo42fr(ls_Error, ls_Header, ls_Detail, li_PageNumber)

dw_sales-orders-by-customer_detail.ScrolltoRow(ll_CurrentRow )
CHOOSE CASE li_Rtn
CASE 0
	dw_sales-orders-by-customer_detail.uf_changerowstatus ( 0, NotModified! )
	lb_ValidReference = TRUE
	RETURN TRUE
CASE ELSE
	lb_ValidReference = FALSE
	RETURN FALSE
END CHOOSE
end function

public function string wf_get_current_setting ();String	ls_Return_String

ls_Return_String  = String(dw_sales-orders-by-customer_header.GetItemDate( 1,"ship_date*")) + "~t"&
				 +dw_sales-orders-by-customer_header.GetItemString( 1,"shipto_customer_id")
Return ls_Return_String
end function

public function boolean wf_notepad ();String  ls_order_numbers
ls_order_numbers  = This.wf_GetSelectedOrders()
IF Len(Trim( ls_order_numbers)) > 0 Then
	Open(w_salesordersbycustomernotepad)
	IF Message.StringParm = 'Abort' Then
		Return False
	Else
		iw_frame.wf_OpenSheet("w_orderbrowsebyproduct",Message.StringParm +","+ls_order_numbers )
		RETURN TRUE
	END IF
ELSE
	MessageBox( "No lines selected", "You must select at least one line to go into the notepad")
	Return FALSE
END IF
RETURN FALSE
end function

public function string wf_getselectedorders ();String 			ls_order_numbers_to_Return
Long 				ll_rowCount,&
					ll_nextselected


ll_nextselected = dw_sales-orders-by-customer_detail.GetSelectedRow(0)

Do While ll_nextselected > 0
	ls_order_numbers_to_Return+= dw_sales-orders-by-customer_detail.GetItemString( ll_nextselected,"order_id")+"~t"
	ls_order_numbers_to_Return+= dw_sales-orders-by-customer_detail.GetItemString( ll_nextselected,"order_status")+"~t"
	ls_order_numbers_to_Return+= "UnKnown~t"
	ls_order_numbers_to_Return+= dw_sales-orders-by-customer_detail.GetItemString( ll_nextselected,"ship_plant_code")+"~t"
	ls_order_numbers_to_Return+= String(dw_sales-orders-by-customer_detail.GetItemDate( ll_nextselected,"ship_date"),"MM/DD/YYYY")+"~t"
	ls_order_numbers_to_Return+= String(dw_sales-orders-by-customer_detail.GetItemDate( ll_nextselected,"delivery_date"), "MM/DD/YYYY")+"~t~r~n"
	ll_nextselected = dw_sales-orders-by-customer_detail.GetSelectedRow(ll_nextselected)
Loop

Return ls_order_numbers_to_Return
end function

public subroutine wf_set_iw_sales_detail (w_sales_order_detail iw_window);iw_sales_detail = iw_window
end subroutine

public function boolean wf_split_orders ();/* added this Function for gtl split loads functionality ibdkdld*/ 

s_Error	ls_Error

Double	ld_TaskNumber

Long		ll_Row, &
			ll_RowCount,&
			ll_CurrentRow,ll_selectedrow

INTEGER 	li_PageNumber, &
			li_Rtn,li_tmp

STRING	ls_Header, &
			ls_Detail,ls_order

DataStore lds_Tmp

ll_RowCount = dw_sales-orders-by-customer_Detail.RowCount()

IF dw_sales-orders-by-customer_detail.AcceptText() = -1 OR &
	dw_sales-orders-by-customer_header.AcceptText() = -1 Then
	Return FALSE
END IF

ll_CurrentRow = dw_sales-orders-by-customer_detail.GetRow() 
ls_Error.se_User_ID = Message.nf_getuserid()
ls_Header = dw_sales-orders-by-customer_Header.Describe("DataWindow.Data")
ls_Header = ls_Header + "~tS"
dw_sales-orders-by-customer_Detail.SetRedraw(False)
lds_tmp = Create DataStore
lds_tmp.dataobject = "d_sales-order-by-customer_detail"

FOR ll_Row = 1 TO ll_RowCount
	If ll_selectedrow > ll_row Then
		ll_SelectedRow = dw_sales-orders-by-customer_detail.GetSelectedRow(ll_selectedrow)
	Else
		If ll_row = 1 Then
			ll_SelectedRow = dw_sales-orders-by-customer_detail.GetSelectedRow(ll_row - 1)
		Else
			ll_SelectedRow = dw_sales-orders-by-customer_detail.GetSelectedRow(ll_selectedrow)
		End If	
	End If
	If ll_selectedrow > 0 Then
		IF dw_sales-orders-by-customer_Detail.getitemstring(ll_selectedrow,"gtl_split_ind") = "Y" Then
			If dw_sales-orders-by-customer_Detail.getitemstring(ll_selectedrow,"load_status") = "U" Then
				dw_sales-orders-by-customer_Detail.Rowscopy(ll_selectedrow, ll_selectedrow, Primary!, lds_tmp,10000, Primary!)
				li_tmp += 1
			Else
				dw_sales-orders-by-customer_detail.ScrolltoRow(ll_selectedrow )
				dw_sales-orders-by-customer_detail.setfocus()
				ls_order = dw_sales-orders-by-customer_detail.getitemstring(ll_selectedrow,"order_id")
				iw_frame.setmicrohelp("The Load Status for Order: " + ls_order + " must be uncombined")
				dw_Sales-orders-by-customer_Detail.SetRedraw(TRUE)
				return false
			End If
		Else
			dw_sales-orders-by-customer_detail.ScrolltoRow(ll_selectedrow )
			dw_sales-orders-by-customer_detail.SetColumn ("gtl_split_ind")
			dw_sales-orders-by-customer_detail.setfocus()
			ls_order = dw_sales-orders-by-customer_detail.getitemstring(ll_selectedrow,"order_id")
			iw_frame.setmicrohelp("The GTL indicator for Order: " + ls_order + " must be checked")
			dw_Sales-orders-by-customer_Detail.SetRedraw(TRUE)
			return false
		End If
	Else
		exit
	End IF	
NEXT		
if li_tmp > 0 then
	ls_Detail = lds_tmp.Object.DataWindow.Data
//	messagebox("detail", ls_detail)
Else
	dw_Sales-orders-by-customer_Detail.SetRedraw(TRUE)
	dw_Sales-orders-by-customer_Detail.setfocus()
	iw_frame.setmicrohelp("You must have rows selected to Split Orders")
	return false
End IF
dw_Sales-orders-by-customer_Detail.SetRedraw(TRUE)

//li_Rtn = iu_orp003.nf_orpo42ar(ls_Error, ls_Header, ls_Detail, ld_TaskNumber, li_PageNumber)
li_Rtn = iu_ws_orp1.nf_orpo42fr(ls_Error, ls_Header, ls_Detail, li_PageNumber)

dw_sales-orders-by-customer_detail.ScrolltoRow(ll_CurrentRow )
CHOOSE CASE li_Rtn
CASE 0
	iw_frame.setmicrohelp("The Split GTL Orders process has been successfully started")
	RETURN TRUE
CASE ELSE
	RETURN FALSE
END CHOOSE

end function

public function boolean wf_inquire (double ad_tasknumber, integer ai_pagenumber);INTEGER 	li_Rtn
STRING	ls_Header, ls_Detail
s_Error	ls_Error

ls_Error.se_User_ID = Message.nf_getuserid()
ls_Header = dw_sales-orders-by-customer_Header.Describe("DataWindow.Data")
ls_Header = ls_Header + "~tI" 

wf_set_header_instance()	

dw_sales-orders-by-customer_header.SetRedraw(False)
dw_sales-orders-by-customer_detail.SetRedraw(False)

//li_Rtn = iu_orp003.nf_orpo42ar(ls_Error, ls_Header, ls_Detail, ad_TaskNumber, ai_PageNumber)
li_Rtn = iu_ws_orp1.nf_orpo42fr(ls_Error, ls_Header, ls_Detail, ai_PageNumber)

CHOOSE CASE li_Rtn
CASE 0
	dw_sales-orders-by-customer_detail.ImportString(TRIM(ls_Detail))
	IF ai_PageNumber <> 0 THEN	wf_Inquire(ad_TaskNumber, ai_PageNumber)
	dw_sales-orders-by-customer_detail.uf_changerowstatus ( 0, NotModified! )
	dw_sales-orders-by-customer_header.Reset()
	dw_sales-orders-by-customer_header.ImportString(TRIM(ls_Header))
	dw_sales-orders-by-customer_header.SetRedraw(TRUE)
	dw_sales-orders-by-customer_detail.SetRedraw(TRUE)
	il_current_column = 0
	RETURN TRUE
CASE ELSE
	dw_sales-orders-by-customer_header.SetRedraw(TRUE)
	dw_sales-orders-by-customer_detail.SetRedraw(TRUE)
	RETURN FALSE
END CHOOSE

//is_OpeningString = dw_sales-orders-by-customer_Header.Describe("DataWindow.Data")


end function

public function boolean wf_retrieve ();String	ls_input_string, &
			ls_smanlocation, &
			ls_debug, &
			ls_customer_id, &
			ls_customer_name, &
			ls_divisional_po, &
			ls_corporate_po, &
			ls_division_code, &
			ls_ship_date_ind, &
			ls_ship_date, &
			ls_to_ship_date, &
			ls_product_short_que, &
			ls_load_status_ind, &
			ls_load_status, &
			ls_delivery_date_ind, &
			ls_delivery_date, &
			ls_to_delivery_date, &
			ls_ship_plant_ind, &
			ls_ship_plant, &
			ls_ship_plant_desc, &
			ls_show_linked_orders_ind
			
Long		ll_rows, ll_header_rows

This.TriggerEvent('closequery')

//ll_header_rows = dw_sales-orders-by-customer_Header.RowCount()
//
//If ll_header_rows > 0 Then
//	is_customer_id = dw_sales-orders-by-customer_Header.GetItemString(1, "customer_id")
//	is_divisional_po = dw_sales-orders-by-customer_Header.GetItemString(1, "divisional_po")
//	is_corporate_po = dw_sales-orders-by-customer_Header.GetItemString(1, "corporate_po")
//	is_division_code = dw_sales-orders-by-customer_Header.GetItemString(1, "division_code")
//	is_ship_date_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_date_ind")
//	is_ship_date = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_date")
//	is_to_ship_date = dw_sales-orders-by-customer_Header.GetItemString(1, "to_ship_date")
//	is_product_short_que = dw_sales-orders-by-customer_Header.GetItemString(1, "product_short_que")
//	is_delivery_date_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "delivery_date_ind")
//	is_delivery_date = dw_sales-orders-by-customer_Header.GetItemString(1, "delivery_date")
//	is_to_delivery_date = dw_sales-orders-by-customer_Header.GetItemString(1, "to_delivery_date")
//	is_load_status_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "load_status_ind")
//	is_load_status = dw_sales-orders-by-customer_Header.GetItemString(1, "load_status")	
//	is_ship_plant_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_plant_ind")
//	is_ship_plant = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_plant")
//
//	If IsNull(is_customer_id) Then
//		is_customer_id = Char(7)
//	End If
//	
//	If IsNull(is_divisional_po) Then
//		is_divisional_po = Char(12)
//	End If
//	
//	If IsNull(is_corporate_po) Then
//		is_corporate_po = Char(12)
//	End If
//	
//	is_openingstring = 	is_customer_id 		+ '~t' + &
//								is_divisional_po 		+ '~t' + &
//								is_corporate_po 		+ '~t' + &
//								is_division_code 		+ '~t' + &
//								is_ship_date_ind 		+ '~t' + &
//								is_ship_date 			+ '~t' + &
//								is_to_ship_date 		+ '~t' + &
//								is_product_short_que + '~t' + &
//								is_delivery_date_ind + '~t' + &
//								is_delivery_date 		+ '~t' + &
//								is_to_delivery_date 	+ '~t' + &
//								is_load_status_ind 	+ '~t' + &
//								is_load_status 		+ '~t' + &
//								is_ship_plant_ind 	+ '~t' + &
//								is_ship_plant 
//End If

wf_set_header_instance()

OpenWithParm( w_sales_order_by_customer_inq, is_openingstring)

IF Message.StringParm = "Cancel" then return False

ls_input_string = Message.StringParm

ids_sales_orders_by_cust.Reset()
ll_rows = ids_sales_orders_by_cust.ImportString(ls_input_string)
ls_debug = ids_sales_orders_by_cust.describe("DataWindow.Data")

ls_customer_id = ids_sales_orders_by_cust.GetItemString(1, "customer_id")
ls_customer_name = ids_sales_orders_by_cust.GetItemString(1, "customer_name")
ls_divisional_po = ids_sales_orders_by_cust.GetItemString(1, "divisional_po")
ls_corporate_po = ids_sales_orders_by_cust.GetItemString(1, "corporate_po")
ls_division_code = ids_sales_orders_by_cust.GetItemString(1, "division_code")
ls_ship_date_ind = ids_sales_orders_by_cust.GetItemString(1, "ship_date_ind")
ls_ship_date = ids_sales_orders_by_cust.GetItemString(1, "ship_date")
ls_to_ship_date = ids_sales_orders_by_cust.GetItemString(1, "to_ship_date")
ls_product_short_que = ids_sales_orders_by_cust.GetItemString(1, "product_short_que")
ls_load_status_ind = ids_sales_orders_by_cust.GetItemString(1, "load_status_ind")
ls_load_status = ids_sales_orders_by_cust.GetItemString(1, "load_status")
ls_delivery_date_ind = ids_sales_orders_by_cust.GetItemString(1, "delivery_date_ind")
ls_delivery_date = ids_sales_orders_by_cust.GetItemString(1, "delivery_date")
ls_to_delivery_date = ids_sales_orders_by_cust.GetItemString(1, "to_delivery_date")
ls_ship_plant_ind = ids_sales_orders_by_cust.GetItemString(1, "ship_plant_ind")
ls_ship_plant = ids_sales_orders_by_cust.GetItemString(1, "ship_plant")
ls_ship_plant_desc = ids_sales_orders_by_cust.GetItemString(1, "ship_plant_desc")
ls_show_linked_orders_ind = ids_sales_orders_by_cust.GetItemString(1, "show_linked_orders_ind")

dw_sales-orders-by-customer_Header.Reset()
dw_sales-orders-by-customer_Detail.Reset()
dw_sales-orders-by-customer_Header.insertrow(0)

dw_sales-orders-by-customer_header.SetItem(1, "shipto_customer_id", ls_customer_id)
dw_sales-orders-by-customer_header.SetItem(1, "shipto_shortname", ls_customer_name)
dw_sales-orders-by-customer_header.SetItem(1, "divisional_po", ls_divisional_po)
dw_sales-orders-by-customer_header.SetItem(1, "corporate_po", ls_corporate_po)
dw_sales-orders-by-customer_header.SetItem(1, "div_code", ls_division_code)
dw_sales-orders-by-customer_header.SetItem(1, "ship_date_ind", ls_ship_date_ind)
dw_sales-orders-by-customer_header.SetItem(1, "ship_date", Date(ls_ship_date))
dw_sales-orders-by-customer_header.SetItem(1, "to_ship_date", Date(ls_to_ship_date))
dw_sales-orders-by-customer_header.SetItem(1, "prod_short_que_ind", ls_product_short_que)
dw_sales-orders-by-customer_header.SetItem(1, "load_status_ind", ls_load_status_ind)
dw_sales-orders-by-customer_header.SetItem(1, "load_status", ls_load_status)
dw_sales-orders-by-customer_header.SetItem(1, "delivery_date_ind", ls_delivery_date_ind)
dw_sales-orders-by-customer_header.SetItem(1, "delivery_date", Date(ls_delivery_date))
dw_sales-orders-by-customer_header.SetItem(1, "to_delivery_date", Date(ls_to_delivery_date))
dw_sales-orders-by-customer_header.SetItem(1, "ship_plant_ind", ls_ship_plant_ind)
dw_sales-orders-by-customer_header.SetItem(1, "ship_plant", ls_ship_plant)
dw_sales-orders-by-customer_header.SetItem(1, "ship_plant_desc", ls_ship_plant_desc)
dw_sales-orders-by-customer_header.SetItem(1, "show_linked_orders_ind", ls_show_linked_orders_ind)

If dw_sales-orders-by-customer_header.AcceptText() = -1 Then Return False

ls_input_string = dw_sales-orders-by-customer_Header.Describe("DataWindow.Data")

Return This.wf_Inquire(0,0)

//IF message.StringParm = 'Abort' Then 
//	Return False
//ELSE
//	is_ openingstring = message.StringParm
//	ls_input_string = Message.StringParm
////	Messagebox(Message.StringParm,'')		//RevGLL
//	dw_sales-orders-by-customer_Header.SetRedraw(False)
//	dw_sales-orders-by-customer_Header.Reset()
//	dw_sales-orders-by-customer_Detail.Reset()
//	dw_sales-orders-by-customer_header.ImportString( is_ openingstring)
//	dw_sales-orders-by-customer_header.SetItem(1, "sales_location", Message.is_smanlocation)
//	dw_sales-orders-by-customer_Header.SetRedraw( True)
//	Return This.wf_Inquire(0,0)
//END IF

end function

public subroutine wf_set_header_instance ();Long	ll_header_rows

ll_header_rows = dw_sales-orders-by-customer_Header.RowCount()

If ll_header_rows > 0 Then
	is_customer_id = dw_sales-orders-by-customer_Header.GetItemString(1, "shipto_customer_id")
	is_divisional_po = dw_sales-orders-by-customer_Header.GetItemString(1, "divisional_po")
	is_corporate_po = dw_sales-orders-by-customer_Header.GetItemString(1, "corporate_po")
	is_division_code = dw_sales-orders-by-customer_Header.GetItemString(1, "div_code")
	is_ship_date_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_date_ind")
	is_ship_date = String(dw_sales-orders-by-customer_Header.GetItemDate(1, "ship_date"))
	is_to_ship_date = String(dw_sales-orders-by-customer_Header.GetItemDate(1, "to_ship_date"))
	is_product_short_que = dw_sales-orders-by-customer_Header.GetItemString(1, "prod_short_que_ind")
	is_delivery_date_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "delivery_date_ind")
	is_delivery_date = String(dw_sales-orders-by-customer_Header.GetItemDate(1, "delivery_date"))
	is_to_delivery_date = String(dw_sales-orders-by-customer_Header.GetItemDate(1, "to_delivery_date"))
	is_load_status_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "load_status_ind")
	is_load_status = dw_sales-orders-by-customer_Header.GetItemString(1, "load_status")	
	is_ship_plant_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_plant_ind")
	is_ship_plant = dw_sales-orders-by-customer_Header.GetItemString(1, "ship_plant")
	is_linked_orders_ind = dw_sales-orders-by-customer_Header.GetItemString(1, "show_linked_orders_ind")

	If IsNull(is_customer_id) Then
		is_customer_id = Space(7)
	End If
	
	If IsNull(is_divisional_po) Then
		is_divisional_po = Space(12)
	End If
	
	If IsNull(is_corporate_po) Then
		is_corporate_po = Space(12)
	End If
	
	If IsNull(is_ship_date_ind) Then
		is_ship_date_ind = "Y"
	End If
	
	if IsNull(is_to_ship_date) then
		is_to_ship_date = string(RelativeDate(date(is_ship_date),365),"MM/DD/YYYY")
	End If	
	
	If IsNull(is_delivery_date_ind) Then
		is_delivery_date_ind = "N"
	End If
	
	If IsNull(is_division_code) Then
		is_division_code = ""
	End If
	
	If IsNull(is_product_short_que) Then
		is_product_short_que = ""
	End If
	
	If IsNull(is_delivery_date) Then
		is_delivery_date = ""
	End If
	
	If IsNull(is_to_delivery_date) Then
		is_to_delivery_date = ""
	End If
	
	If IsNull(is_load_status_ind) Then
		is_load_status_ind = ""
	End If
		
	If IsNull(is_load_status) Then
		is_load_status = ""
	End If
	
		If IsNull(is_ship_plant_ind) Then
		is_ship_plant_ind = ""
	End If
	
	If IsNull(is_ship_plant) Then
		is_ship_plant = ""
	End If
	
	If IsNull(is_linked_orders_ind) Then
		is_linked_orders_ind = ""
	End If
	
	is_openingstring = 	is_customer_id 		+ '~t' + &
								is_divisional_po 		+ '~t' + &
								is_corporate_po 		+ '~t' + &
								is_division_code 		+ '~t' + &
								is_ship_date_ind 		+ '~t' + &
								is_ship_date 			+ '~t' + &
								is_to_ship_date 		+ '~t' + &
								is_product_short_que + '~t' + &
								is_delivery_date_ind + '~t' + &
								is_delivery_date 		+ '~t' + &
								is_to_delivery_date 	+ '~t' + &
								is_load_status_ind 	+ '~t' + &
								is_load_status 		+ '~t' + &
								is_ship_plant_ind 	+ '~t' + &
								is_ship_plant			+ '~t' + &
								is_linked_orders_ind

End IF
end subroutine

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_currency,	ldwc_child
STRING 	ls_JobID,ls_UserID, ls_salesloc

iu_ws_orp4 = CREATE u_ws_orp4
iu_ws_orp1 = Create u_ws_orp1

IF UPPER(LEFT(SQLCA.UserID,4)) <> 'IBDK' THEN iw_Frame.im_Menu.m_Options.m_NotePad.Enabled = FALSE  

IF NOT ib_nv_created Then
	iu_orp003 = Create u_orp003
   ib_nv_created =  TRUE
END IF

ls_salesloc = Message.is_smanlocation
dw_sales-orders-by-customer_header.Reset()
dw_sales-orders-by-customer_detail.Reset()
dw_sales-orders-by-customer_detail.GetChild("currency_code", ldwc_Currency)

If IsValid( ldwc_currency ) Then
	iw_frame.iu_project_functions.nf_gettutltype(ldwc_currency, "CURRCODE")
End if

IF LEFT(is_OpeningString, 5) = "POPUP" Then
	is_OpeningString = ''
	IF NOT wf_Retrieve() Then CLOSE( This)
ELSE
	dw_sales-orders-by-customer_Header.ImportString(is_OpeningString)
	dw_sales-orders-by-customer_header.SetItem(1, "sales_location", Message.is_smanlocation)
	is_from_window = "POPUP"
	wf_Inquire( 0,0)
END IF


end event

event open;call super::open;ids_sales_orders_by_cust = create Datastore
ids_sales_orders_by_cust.dataobject = 'd_sales-order-by-customer_header_2'

iw_sales_detail = Message.PowerObjectParm

//IF IsValid(iw_sales_detail) Then
//	
//  is_OpeningString = iw_sales_detail.dynamic wf_getshipdate( ) + "~t" + &
//				  		   iw_sales_detail.dynamic wf_get_customer( ) + "~t" + &
//		  		 			iw_sales_detail.dynamic wf_getdivision( ) +  "~t"
//ELSE
is_OpeningString = Message.StringParm
//END IF




end event

event close;call super::close;Destroy( iu_orp003)
If IsValid(iu_orp002) Then Destroy iu_orp002

DESTROY iu_ws_orp4
Destroy iu_ws_orp1

end event

on w_sales-orders-by-customer.create
int iCurrent
call super::create
this.dw_sales-orders-by-customer_header=create dw_sales-orders-by-customer_header
this.dw_sales-orders-by-customer_detail=create dw_sales-orders-by-customer_detail
this.dw_customer_data=create dw_customer_data
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales-orders-by-customer_header
this.Control[iCurrent+2]=this.dw_sales-orders-by-customer_detail
this.Control[iCurrent+3]=this.dw_customer_data
end on

on w_sales-orders-by-customer.destroy
call super::destroy
destroy(this.dw_sales-orders-by-customer_header)
destroy(this.dw_sales-orders-by-customer_detail)
destroy(this.dw_customer_data)
end on

event ue_get_data;call super::ue_get_data;String ls_indicator

Choose Case as_value
	Case "Order_ID"
		Message.StringParm = is_sales_id
	CAse "customer_info"
		Message.StringParm = is_customer_info		
	CASE "indicator"
		Message.StringParm = is_shortage_ind
	Case "shortage_ind"
		Message.StringParm = is_shtg_qu_ind
	Case "load_status_ind"
		Message.StringParm = is_load_status_ind
	Case "load_status"
		Message.StringParm = is_load_status
	Case "delivery_date_ind"
		Message.StringParm = is_delivery_date_ind
	Case "delivery_date"
		Message.StringParm = is_delivery_date
	Case "ship_plant_ind"
		Message.StringParm = is_ship_plant_ind
	Case "location_code"
		Message.StringParm = is_location_code
End Choose
end event

event activate;call super::activate;u_project_functions lu_project_functions

If lu_project_functions.nf_isscheduler() Then
	iw_Frame.Im_Menu.M_options.m_CancelOrder.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_applicationoptions.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_viewmessages.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_messagealias.Enabled = FALSE
	iw_Frame.Im_Menu.M_options.m_browsereservations.Enabled = FALSE
End If
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_splitgtlorders.Enabled = True
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = True
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_generatesales.Enabled = False 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_complete.Enabled = False
//iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = True
end event

event deactivate;call super::deactivate;iw_Frame.im_Menu.m_Options.m_applicationoptions.m_splitgtlorders.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_generatesales.Enabled = True 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_complete.Enabled = True 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = False
end event

event resize;call super::resize;long	ll_x = 50, ll_y = 105

if il_BorderPaddingWidth > ll_x Then
	ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
	ll_y = il_BorderPaddingHeight
End If

dw_sales-orders-by-customer_detail.Resize(&
						this.width - dw_sales-orders-by-customer_detail.x - ll_x, &
						this.height - dw_sales-orders-by-customer_detail.y - ll_y)
//dw_sales-orders-by-customer_detail.Resize(&
//						this.width - dw_sales-orders-by-customer_detail.x - 50, &
//						this.height - dw_sales-orders-by-customer_detail.y - 105)
end event

type dw_sales-orders-by-customer_header from u_base_dw_ext within w_sales-orders-by-customer
integer width = 2811
integer height = 508
integer taborder = 20
string dataobject = "d_sales-order-by-customer_header"
boolean border = false
end type

type dw_sales-orders-by-customer_detail from u_base_dw_ext within w_sales-orders-by-customer
integer y = 508
integer width = 3854
integer height = 940
integer taborder = 10
string dataobject = "d_sales-order-by-customer_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;STRING							ls_SalesID, ls_ColumnName

Long								ll_row

w_sales_order_detail			lw_Temp

// This has to be clickedrow because if you are are double clicking on 
// a protected row the row focus does change
ll_row = row

// You must get clicked column because order ID is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name

CHOOSE CASE ls_ColumnName
CASE	"order_id"
	ls_SalesID = This.GetItemString(ll_row, "order_id")
	IF IsValid( iw_sales_detail) Then
		iw_sales_detail.dynamic wf_set_to_order_id( ls_SalesID)
		iw_sales_detail.SetFocus() 
		iw_sales_detail.PostEvent("Ue_Move_Rows")			
		iw_sales_detail = lw_temp
	ELSE
		iw_frame.iu_project_functions.nf_list_sales_orders(ls_salesid)
	END IF
END CHOOSE
end event

event constructor;call super::constructor;ib_updateable = true
end event

event rbuttondown;call super::rbuttondown;m_sales_orders_popup NewMenu
m_gtl_cascade lm_pop

u_project_functions lu_project_functions
// dld Added for gtl Project
If dwo.name = 'gtl_split_ind' then
	lm_pop = CREATE m_gtl_cascade
	dw_sales-orders-by-customer_detail.SetRow ( row )
	lm_pop.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
	Destroy lm_pop
	Return
End If
 
If lu_project_functions.nf_isscheduler() Then Return

If Row > 0 Then
	is_Sales_ID = This.GetItemString(row,"order_id")
	is_shtg_qu_ind = This.GetItemString(row,"sht_queue_ind")
End IF

NewMenu = CREATE m_sales_orders_popup
NewMenu.M_file.m_orderconfirmation.enabled = TRUE
NewMenu.M_file.m_shortagequeue.enabled = TRUE
NewMenu.M_file.m_automaticprice.enabled = FALSE
NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
end event

event clicked;IF Row < 1 THEN RETURN

Choose Case dwo.name

	Case "order_id" 
		il_current_column ++
		IF il_current_column >= 50 THEN
			MessageBox("ERROR", "You can only select 50 Order_id")	
			THIS.SelectRow(row, FALSE)
			il_current_column --
			Return
		END IF
	
		is_selection = '4'
		Call Super::Clicked
		THIS.SelectRow(row, TRUE)
		
	Case 'linked_orders'
			OpenWithParm(w_linked_orders_popup, This.GetItemString(row, "order_id"))
			
	Case Else
		il_current_column --
		THIS.SelectRow(row, FALSE)

End Choose
end event

event ue_keydown;call super::ue_keydown;//override
	

end event

event itemfocuschanged;call super::itemfocuschanged;If Row > 0 Then
	is_Sales_ID = This.GetItemString(row,"order_id")
	is_shtg_qu_ind = This.GetItemString(row,"sht_queue_ind")
End If
end event

event itemchanged;call super::itemchanged;string ls_temp
IF dwo.name = "gtl_split_ind" Then
	IF data = "Y" Then
		IF Not dw_sales-orders-by-customer_detail.getitemstring(row,"load_status") = "U" Then
			ls_temp=dw_sales-orders-by-customer_detail.getitemstring(row,"order_id")
			dw_sales-orders-by-customer_detail.ScrolltoRow(ROW )
			dw_sales-orders-by-customer_detail.SetColumn ("gtl_split_ind")
			dw_sales-orders-by-customer_detail.setfocus()
			iw_frame.setmicrohelp("The Load Status for Order: " + ls_TEMP + " must be uncombined for GTL Orders")
			return 1
		End if
	End If
eND iF
			
end event

event itemerror;call super::itemerror;RETURN 2
end event

type dw_customer_data from u_base_dw_ext within w_sales-orders-by-customer
boolean visible = false
integer x = 18
integer y = 372
integer width = 46
integer height = 56
integer taborder = 30
string dataobject = "d_customer_data"
end type

