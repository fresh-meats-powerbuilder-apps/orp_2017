HA$PBExportHeader$w_tooltip_instr.srw
forward
global type w_tooltip_instr from window
end type
type dw_instructions from datawindow within w_tooltip_instr
end type
end forward

global type w_tooltip_instr from window
string tag = "Instructions"
integer x = 672
integer y = 268
integer width = 2784
integer height = 224
windowtype windowtype = child!
long backcolor = 15793151
dw_instructions dw_instructions
end type
global w_tooltip_instr w_tooltip_instr

type variables
w_base_sheet	iw_Parent
end variables

on w_tooltip_instr.create
this.dw_instructions=create dw_instructions
this.Control[]={this.dw_instructions}
end on

on w_tooltip_instr.destroy
destroy(this.dw_instructions)
end on

event open;Long						ll_NewX, &
							ll_NewY,&
							ll_Width,&
							ll_Height,&
							ll_Temp, &
							ll_BackColor, &
							ll_Color, &
							ll_xpos, &
							ll_ypos
							
String					ls_data,&
							ls_String

u_string_functions	lu_strings

u_sdkcalls				lu_sdk


//ll_Temp = 15

ls_String = Message.StringParm

//iw_parent = Message.PowerObjectParm
//IF IsValid(iw_parent) Then
////	iw_parent.Event ue_get_data("Instructions")
//	ls_String = Message.StringParm 
////	iw_parent.Event ue_get_data("xpos")
////	ll_xpos = long(Message.StringParm)
////	iw_parent.Event ue_get_data("ypos")
////	ll_ypos = long(Message.StringParm)
//ELSE
//	ls_String = Message.StringParm 
//END IF

dw_instructions.SetItem(1, "line_1", mid(ls_string,1,100))
dw_instructions.SetItem(1, "line_2", mid(ls_string,101,100))


//ll_height = dw_description.ImportString(ls_String)
//ll_height = ll_height * (Long(dw_description.Object.tooltip.Height)+ll_Temp)
//ll_width = (dw_description.GetItemNumber(1,"c_max")+1) * 29

//dw_description.Resize(ll_width,ll_height)
//Window should be a tiny bit larger then the DW
//This.Resize(ll_width+5,ll_height)

//ll_NewX = gw_netwise_frame.PointerX() + 50
//ll_NewY = gw_netwise_frame.PointerY() + 50
//
//If ll_NewX + This.Width > gw_netwise_frame.Width Then
//	ll_NewX = gw_netwise_frame.Width - This.Width - 5
//End if
//If ll_NewY + This.Height > gw_netwise_frame.Height Then
//	ll_NewY = gw_netwise_frame.Height - This.Height - 100
//End if

//This.Move(ll_NewX, ll_NewY)
This.Move(1100, 1000)

lu_sdk = Create u_sdkcalls
ll_BackColor = lu_sdk.nf_GetSysColor(24)
ll_Color = lu_sdk.nf_GetSysColor(23)
Destroy(lu_sdk)

This.BackColor = ll_BackColor
//dw_description.Object.tooltip.background.color = ll_BackColor
//dw_description.Object.tooltip.color = ll_Color
//dw_description.Object.DataWindow.Detail.Color = ll_BackColor

Timer(5, This)
IF IsValid(iw_parent) Then
	iw_parent.setfocus()
end if

end event

event timer;close(this)
end event

event mousemove;Close(This)
end event

type dw_instructions from datawindow within w_tooltip_instr
event ue_mousemove pbm_dwnmousemove
integer width = 2766
integer height = 212
integer taborder = 1
string dataobject = "d_tooltip_instr"
boolean border = false
boolean livescroll = true
end type

event ue_mousemove;Close(Parent)
end event

event constructor;This.insertRow(0)
end event

