HA$PBExportHeader$w_sales_order_tsr.srw
forward
global type w_sales_order_tsr from w_base_sheet_ext
end type
type dw_input from u_base_dw_ext within w_sales_order_tsr
end type
type dw_detail from u_base_dw_ext within w_sales_order_tsr
end type
type dw_customer_data from u_base_dw_ext within w_sales_order_tsr
end type
end forward

global type w_sales_order_tsr from w_base_sheet_ext
integer width = 3355
integer height = 1536
string title = "Sales Orders by TSR"
event ue_retrieve pbm_custom02
event ue_print_fax ( )
event ue_print ( )
event ue_fax ( )
event ue_edi ( )
event ue_tla ( )
event type integer ue_prod_shrtg_que ( )
event ue_email ( )
event ue_tla_email ( )
event ue_tla_local_print ( )
event ue_tla_view ( )
dw_input dw_input
dw_detail dw_detail
dw_customer_data dw_customer_data
end type
global w_sales_order_tsr w_sales_order_tsr

type prototypes

end prototypes

type variables
// Netwise Instance variables
Boolean		ib_nv_created

String 	is_from_window,&
	is_OpeningString ,&
	is_Sales_ID,&
	is_shtg_qu_ind

Window	iw_sales_detail

Long	il_current_column

u_orp002		iu_orp002
u_orp204		iu_orp204
u_ws_orp4       iu_ws_orp4

s_error		istr_error_info

CHAR	ic_process_option_in, ic_process_tla_option_in
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function string wf_get_current_setting ()
public function boolean wf_notepad ()
public function string wf_getselectedorders ()
public subroutine wf_set_iw_sales_detail (w_sales_order_detail iw_window)
public function boolean wf_inquire (double ad_tasknumber, integer ai_pagenumber)
end prototypes

event ue_print_fax();INTEGER	li_rtn

String	ls_order_id, ls_order_out, ls_temp, ls_customer_email

Long 	ll_row

ll_row = dw_detail.Getselectedrow(ll_row)

Do While ll_row > 0
	ls_temp = dw_detail.Getitemstring(ll_row, 'order_status') 
	IF ls_temp = 'A' THEN
			ls_order_id += dw_detail.Getitemstring(ll_row, 'order_id') + '~t'
	END IF
	ll_row = dw_detail.Getselectedrow(ll_row)
LOOP

istr_error_info.se_event_name = 'ue_print_fax'

IF NOT isValid(iu_orp002) THEN
	iu_orp002 = Create u_orp002
End IF

//li_rtn = iu_orp002.nf_orpo80br_print_fax(istr_error_info, &
//										ic_process_option_in, &
//										ls_order_id, &
//										ls_order_out, &
//										ls_customer_email, &
//										ic_process_tla_option_in)
										
										
li_rtn = iu_ws_orp4.nf_orpo80fr(istr_error_info, &
										ic_process_option_in, &
										ls_order_id, &
										ls_order_out, &
										ls_customer_email, &
										ic_process_tla_option_in)

dw_detail.SelectRow(0, False)
il_current_column = 0
dw_detail.SetColumn(6)

IF li_rtn >= 0 THEN
	This.SetRedraw(TRUE)
	Return
END IF
end event

event ue_print();ic_process_option_in = 'P'

This.triggerevent('ue_print_fax')
end event

event ue_fax;call super::ue_fax;ic_process_option_in = 'F'

This.triggerevent('ue_print_fax')
end event

event ue_edi;call super::ue_edi;ic_process_option_in = 'E'

This.triggerevent('ue_print_fax')
end event

event ue_tla();ic_process_option_in = 'A'
ic_process_tla_option_in = 'F'

This.triggerevent('ue_print_fax')

end event

event ue_prod_shrtg_que;call super::ue_prod_shrtg_que;
return 1

end event

event ue_email();string 	ls_temp, ls_order_id

long		ll_row

ll_row = dw_detail.getselectedrow(ll_row)

Do While ll_row > 0
		ls_temp = dw_detail.Getitemstring(ll_row, 'order_status')
		if ls_temp = 'A' then
			ls_order_id += dw_detail.getitemstring(ll_row, 'order_id') + '~t'
		end if
		ll_row = dw_detail.getselectedrow(ll_row)
loop

if ls_order_id > ' ' then
	Openwithparm(w_email_order_confirmations, ls_order_id)
else
	iw_frame.SetMicroHelp( "Sales Order must be Accepted")
end if
end event

event ue_tla_email();ic_process_option_in = 'A'
ic_process_tla_option_in = 'E'

This.triggerevent('ue_print_fax')
end event

event ue_tla_local_print();ic_process_option_in = 'A'
ic_process_tla_option_in = 'P'

This.triggerevent('ue_print_fax')
end event

event ue_tla_view;ic_process_option_in = 'A'
ic_process_tla_option_in = 'V'

This.triggerevent('ue_print_fax')
end event

public function boolean wf_retrieve ();String Temp_String
// checks to see if there were any changes
//This.TriggerEvent('closequery')

OpenWithParm( w_sales_order_tsr_inq, is_openingstring)

IF message.StringParm = 'Abort' Then 
	Return False
ELSE
	is_openingstring = message.StringParm
	Temp_string = Message.StringParm 
	dw_input.SetRedraw( False)
	dw_input.Reset()
	dw_Detail.Reset()
	dw_input.ImportString( is_openingstring)
	dw_input.SetRedraw( True)
	Return This.wf_Inquire(0,0)
END IF
  
end function

public function boolean wf_update ();//s_Error	ls_Error
//
//Double	ld_TaskNumber
//
//Long		ll_Row, &
//			ll_RowCount,&
//			ll_CurrentRow
//
//INTEGER 	li_PageNumber, &
//			li_Rtn
//
//BOOLEAN 	lb_ValidReference
//
//STRING	ls_Header, &
//			ls_Detail
//
//
//ll_RowCount = dw_Detail.RowCount()
//
//IF dw_detail.AcceptText() = -1 OR &
//	dw_input.AcceptText() = -1 Then
//	lb_ValidReference = FALSE
//	Return FALSE
//END IF
//
//ll_CurrentRow = dw_detail.GetRow() 
//
//ls_Error.se_User_ID = Message.nf_getuserid()
//ls_Header = dw_input.Describe("DataWindow.Data")
//ls_Header = ls_Header + "~tU"
//dw_Detail.SetRedraw(False)
//dw_Detail.RowsMove(1, dw_Detail.RowCount(), Primary!, dw_Detail,10000, Filter!)
//FOR ll_Row = 1 TO ll_RowCount
//	IF dw_Detail.GetItemStatus(ll_Row, 0, Filter!) = DataModified! THEN
//		dw_Detail.RowsCopy(ll_Row, ll_Row, Filter!, dw_Detail,10000, Primary!)
//	END IF
//NEXT		
//ls_Detail = dw_Detail.Describe("DataWindow.Data")
//dw_Detail.RowsDiscard(1, dw_Detail.RowCount(), Primary!)
//dw_Detail.RowsMove(1, dw_Detail.FilteredCount(), Filter!, dw_Detail,10000, Primary!)
//dw_Detail.SetRedraw(TRUE)
//
//li_Rtn = iu_orp003.nf_orpo42ar(ls_Error, ls_Header, ls_Detail, ld_TaskNumber, li_PageNumber)
//
//
//
//dw_detail.ScrolltoRow(ll_CurrentRow )
//CHOOSE CASE li_Rtn
//CASE 0
//	dw_detail.uf_changerowstatus ( 0, NotModified! )
//	lb_ValidReference = TRUE
//	RETURN TRUE
//CASE ELSE
//	lb_ValidReference = FALSE
//	RETURN FALSE
//END CHOOSE
//
RETURN FALSE
end function

public function string wf_get_current_setting ();String	ls_Return_String

ls_Return_String  = String(dw_input.GetItemDate( 1,"ship_date*")) + "~t"&
				 +dw_input.GetItemString( 1,"shipto_customer_id")
Return ls_Return_String
end function

public function boolean wf_notepad ();String  ls_order_numbers
ls_order_numbers  = This.wf_GetSelectedOrders()
IF Len(Trim( ls_order_numbers)) > 0 Then
	Open(w_salesordersbycustomernotepad)
	IF Message.StringParm = 'Abort' Then
		Return False
	Else
		iw_frame.wf_OpenSheet("w_orderbrowsebyproduct",Message.StringParm +","+ls_order_numbers )
		RETURN TRUE
	END IF
ELSE
	MessageBox( "No lines selected", "You must select at least one line to go into the notepad")
	Return FALSE
END IF
RETURN FALSE
end function

public function string wf_getselectedorders ();String 			ls_order_numbers_to_Return
Long 				ll_rowCount,&
					ll_nextselected


ll_nextselected = dw_detail.GetSelectedRow(0)

Do While ll_nextselected > 0
	ls_order_numbers_to_Return+= dw_detail.GetItemString( ll_nextselected,"order_id")+"~t"
	ls_order_numbers_to_Return+= dw_detail.GetItemString( ll_nextselected,"order_status")+"~t"
	ls_order_numbers_to_Return+= "UnKnown~t"
	ls_order_numbers_to_Return+= dw_detail.GetItemString( ll_nextselected,"ship_plant_code")+"~t"
	ls_order_numbers_to_Return+= String(dw_detail.GetItemDate( ll_nextselected,"ship_date"),"MM/DD/YYYY")+"~t"
	ls_order_numbers_to_Return+= String(dw_detail.GetItemDate( ll_nextselected,"delivery_date"), "MM/DD/YYYY")+"~t~r~n"
	ll_nextselected = dw_detail.GetSelectedRow(ll_nextselected)
Loop

Return ls_order_numbers_to_Return
end function

public subroutine wf_set_iw_sales_detail (w_sales_order_detail iw_window);iw_sales_detail = w_sales_order_detail
end subroutine

public function boolean wf_inquire (double ad_tasknumber, integer ai_pagenumber);s_Error	ls_Error

INTEGER 	li_Rtn

STRING	ls_input, &
			ls_Detail

ls_Error.se_User_ID = Message.nf_getuserid()
ls_input = dw_input.Describe("DataWindow.Data")
//ls_input = ls_input + "~tI" 
dw_detail.SetRedraw(False)

li_Rtn = iu_ws_orp4.nf_orpo83fr(ls_Error, ls_input, ls_Detail)

CHOOSE CASE li_Rtn
CASE 0
	dw_detail.ImportString(TRIM(ls_Detail))
	IF ai_PageNumber <> 0 THEN	wf_Inquire(ad_TaskNumber, ai_PageNumber)
	dw_detail.uf_changerowstatus ( 0, NotModified! )
	dw_detail.SetRedraw(TRUE)
	il_current_column = 0
	RETURN TRUE
CASE ELSE
	dw_detail.SetRedraw(TRUE)
	RETURN FALSE
END CHOOSE
end function

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_currency

STRING 	ls_JobID,ls_UserID, ls_salesloc

IF UPPER(LEFT(SQLCA.UserID,4)) <> 'IBDK' THEN iw_Frame.im_Menu.m_Options.m_NotePad.Enabled = FALSE  

IF NOT ib_nv_created Then
	iu_orp002 = Create u_orp002
	iu_orp204 = Create u_orp204
	iu_ws_orp4 = Create u_ws_orp4
   ib_nv_created =  TRUE
END IF
ls_salesloc = Message.is_smanlocation
dw_input.Reset()
dw_detail.Reset()

IF LEFT(is_OpeningString, 4) = "MENU" Then
	is_OpeningString = ''
	
	message.triggerEvent("ue_getsmancode")
	is_OpeningString = message.stringparm + '~t'
	is_OpeningString += 'ALL     ~t  ~t'
	is_OpeningString += string(today())
	is_OpeningString += '~tN'
	
	IF NOT wf_Retrieve() Then CLOSE( This)
ELSE
//	dw_input.ImportString( is_OpeningString)
	message.triggerEvent("ue_getsmancode")
	is_OpeningString = message.stringparm + '~t'
	is_OpeningString += 'ALL     ~t   ~t'
	is_OpeningString += string(today())
	is_OpeningString += '~tN'
	is_from_window = "POPUP"
	wf_Retrieve()
END IF
end event

event open;call super::open;iw_sales_detail = Message.PowerObjectParm
//IF IsValid( iw_sales_detail) Then
//  is_OpeningString = iw_sales_detail.dynamic wf_getshipdate( ) &
//		  		 +"~t"+  iw_sales_detail.dynamic wf_get_customer( ) &
//		  		 +"~t"+  iw_sales_detail.dynamic wf_getdivision( )
//ELSE
	is_OpeningString = Message.StringParm
//END IF

end event

event close;call super::close;Destroy( iu_orp204)
If IsValid(iu_orp002) Then Destroy iu_orp002

end event

on w_sales_order_tsr.create
int iCurrent
call super::create
this.dw_input=create dw_input
this.dw_detail=create dw_detail
this.dw_customer_data=create dw_customer_data
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_input
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_customer_data
end on

on w_sales_order_tsr.destroy
call super::destroy
destroy(this.dw_input)
destroy(this.dw_detail)
destroy(this.dw_customer_data)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case "Order_ID"
		Message.StringParm = is_sales_id
	CAse "customer_info"
		Message.StringParm = is_customer_info
	Case "shortage_ind"
		Message.StringParm = is_shtg_qu_ind
End Choose
end event

event activate;call super::activate;iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = True
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_generatesales.Enabled = False 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_complete.Enabled = False
//iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = True

end event

event deactivate;call super::deactivate;iw_Frame.im_Menu.m_Options.m_applicationoptions.m_shortagequeue.Enabled = False
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_generatesales.Enabled = True 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_complete.Enabled = True 
iw_Frame.im_Menu.m_Options.m_applicationoptions.m_automaticprice.Enabled = False
end event

event resize;call super::resize;//dw_detail.Resize(this.width - dw_detail.x - 50, this.height - dw_detail.y - 140)
//

long       ll_x = 50,  ll_y = 140

if il_BorderPaddingWidth > ll_x Then
                ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
                ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(this.width - dw_detail.x - ll_x, this.height - dw_detail.y - ll_y)


end event

type dw_input from u_base_dw_ext within w_sales_order_tsr
integer x = 5
integer width = 1787
integer height = 84
integer taborder = 0
boolean enabled = false
string dataobject = "d_sales_order_tsr_inq"
boolean border = false
end type

event constructor;call super::constructor;This.Modify('tsr_id.Protect = 1 tsr_id.BackGround.Color = 12632256 ' + &
				'cust_id.Protect = 1 cust_id.Background.Color = 12632256 ' + &
				'ship_date.Protect = 1 ship_date.Background.Color = 12632256 ')
end event

type dw_detail from u_base_dw_ext within w_sales_order_tsr
integer y = 108
integer width = 3264
integer height = 1268
integer taborder = 0
string dataobject = "d_sales_order_tsr_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;STRING							ls_SalesID, ls_ColumnName

Long								ll_row

w_sales_order_detail			lw_Temp

// This has to be clickedrow because if you are are double clicking on 
// a protected row the row focus does change
ll_row = row

// You must get clicked column because order ID is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name

CHOOSE CASE ls_ColumnName
CASE	"order_id"
	ls_SalesID = This.GetItemString(ll_row, "order_id")
	IF IsValid( iw_sales_detail) Then
		iw_sales_detail.dynamic wf_set_to_order_id( ls_SalesID)
		opensheet(lw_Temp,"w_sales_order_detail", &
	  					iw_frame,0,iw_frame.im_menu.iao_arrangeOpen)
//		iw_sales_detail.SetFocus() 
//		iw_sales_detail.PostEvent("Ue_Move_Rows")			
//		iw_sales_detail = lw_temp
	ELSE
		iw_frame.iu_project_functions.nf_list_sales_orders(ls_salesid)
	END IF
END CHOOSE
end event

event constructor;call super::constructor;ib_updateable = true
end event

event rbuttondown;call super::rbuttondown;m_sales_orders_popup 		NewMenu


If This.RowCount() > 0 Then
	IF Row > 0 Then
		is_Sales_ID = This.GetItemString(row,"order_id")
		is_Shtg_qu_ind = This.GetItemString(row,"short_queue")
	Else
		is_Sales_ID = ''
		is_Shtg_qu_ind = ''
	End IF
	
	NewMenu = CREATE m_sales_orders_popup
	NewMenu.M_file.m_orderconfirmation.enabled = TRUE
	//NewMenu.M_file.m_productshortagequeue.enabled = TRUE
	NewMenu.M_file.m_automaticprice.enabled = FALSE
	NewMenu.m_file.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
End If
end event

event clicked;Long	ll_row, ll_previous

IF Row < 1 THEN RETURN

IF dwo.name = "order_id" THEN
	il_current_column ++
	IF il_current_column >= 50 THEN
		MessageBox("ERROR", "You can only select 50 Order_id")	
		THIS.SelectRow(row, FALSE)
		il_current_column --
		Return
	END IF

	is_selection = '4'
	Call Super::Clicked
	THIS.SelectRow(row, TRUE)
ELSE
	il_current_column --
  	THIS.SelectRow(row, FALSE)
END IF
// last row that was clicked and selected is the one
// or last row selected or line 1
If IsSelected(row) Then
	is_Sales_ID = This.GetItemString(row,"order_id")
	is_Shtg_qu_ind = This.GetItemString(row,"short_queue")
Else
	ll_row = This.GetSelectedRow(0)
	If ll_row = 0 Then
		is_Sales_ID = This.GetItemString(1,"order_id")
		is_Shtg_qu_ind = This.GetItemString(1,"short_queue")
	Else
		Do
		ll_previous = ll_row
		ll_row = This.GetSelectedRow(ll_row)
		Loop until ll_row = 0
		is_Sales_ID = This.GetItemString(ll_previous,"order_id")
		is_Shtg_qu_ind = This.GetItemString(ll_previous,"short_queue")
	End If
End If
end event

event ue_keydown;call super::ue_keydown;//override
end event

type dw_customer_data from u_base_dw_ext within w_sales_order_tsr
boolean visible = false
integer x = 18
integer y = 372
integer width = 46
integer height = 56
integer taborder = 20
string dataobject = "d_customer_data"
end type

