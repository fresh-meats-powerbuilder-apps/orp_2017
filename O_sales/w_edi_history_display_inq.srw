HA$PBExportHeader$w_edi_history_display_inq.srw
forward
global type w_edi_history_display_inq from w_base_response_ext
end type
type dw_parameters from u_base_dw_ext within w_edi_history_display_inq
end type
end forward

global type w_edi_history_display_inq from w_base_response_ext
integer x = 498
integer y = 700
integer width = 2839
integer height = 516
string title = "Sales EDI Change PO History Display Inq"
long backcolor = 12632256
dw_parameters dw_parameters
end type
global w_edi_history_display_inq w_edi_history_display_inq

type variables
String	is_open_string
end variables

on w_edi_history_display_inq.create
int iCurrent
call super::create
this.dw_parameters=create dw_parameters
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parameters
end on

on w_edi_history_display_inq.destroy
call super::destroy
destroy(this.dw_parameters)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_input_string

dw_parameters.AcceptText()

ls_input_string = dw_parameters.Describe("DataWindow.Data")

CloseWithReturn(This, ls_input_string)
end event

event open;call super::open;is_open_string = Message.StringParm
//MessageBox("ls_open_string",is_open_string)
end event

event ue_postopen;call super::ue_postopen;u_string_functions	lu_string_functions

If NOT lu_string_functions.nf_isempty(is_open_string) Then 
	dw_parameters.Reset()
	dw_parameters.ImportString(is_open_string)
End If
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_edi_history_display_inq
integer x = 923
integer y = 832
integer taborder = 0
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_edi_history_display_inq
integer x = 1394
integer y = 272
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_edi_history_display_inq
integer x = 827
integer y = 272
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_edi_history_display_inq
end type

type dw_parameters from u_base_dw_ext within w_edi_history_display_inq
integer x = 50
integer y = 28
integer width = 2734
integer height = 224
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_edi_parameters_inq"
boolean border = false
boolean ib_scrollable = false
end type

event itemchanged;call super::itemchanged;Int			li_row_count, &
				li_loop_count, &
				li_column

String		ls_temp, &
				ls_salesloc, &
				ls_salesman, &
				ls_text, &
				ls_location_name,&
				ls_CustomerName,&
				ls_CustomerCity, &
				ls_customer_state

long			ll_count, ll_rowcount, ll_count2, ll_linenumber
				
u_project_functions	lu_project_functions				
				
li_column = This.GetColumn()
ls_text = data

If IsNull(ls_text) or (ls_text = '') Then Return

Choose Case This.GetColumnName()
Case 'customer'
	IF lu_project_functions.nf_validatecustomerstate( Data, 'S',&
				ls_CustomerName,ls_CustomerCity, ls_customer_state) < 1 Then
			This.SetItem(1, "customer_name", '')
		Return 1
	Else
		If isnull(ls_CustomerName) then
			This.SetItem(1, "customer_name", '')
		else
			This.SetItem(1, "customer_name", ls_CustomerName)
		end if
		
//		ls_Text = lu_project_functions.nf_getdefault_tsr_for_customer ( Data )
//		this.setitem( 1, "salesperson", ls_Text)
//		SELECT salesman.smanname, 
//				 salesman.smanloc
//			INTO :ls_salesman, 
//			     :ls_salesloc
//			FROM 	salesman  
//			WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';
//
//		This.SetItem(1, "salesperson_name", ls_salesman)

	END IF
	
CASE 'salesperson'
	SELECT salesman.smanname, 
			salesman.smanloc
		INTO :ls_salesman, 
				:ls_salesloc
		FROM salesman  
	WHERE salesman.smancode = :ls_text AND salesman.smantype = 's';

	This.SetItem(1, "salesperson_name", ls_salesman)
	
	cb_base_ok.SetFocus( ) 

End Choose

Return



end event

event constructor;call super::constructor;DataWindowChild	ldwc_temp

String					ls_location

u_string_functions		lu_string_functions

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData( ldwc_temp)

This.GetChild( "salesperson",ldwc_temp) 
ls_location = message.is_smanlocation

//IF Not iw_frame.iu_project_functions.nf_issalesmgr() Then
//	If Not lu_string_functions.nf_IsEmpty(ls_location) Then
//		iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, ls_location)
//	End If
//Else	
iw_frame.iu_project_functions.nf_gettsrs(ldwc_temp, '')
//End IF

//ib_scrollable = FALSE
//ib_NewRowOnChange = FALSE
//ib_firstcolumnonnextrow = FALSE
//
end event

event itemerror;call super::itemerror;Return 1
end event

