HA$PBExportHeader$w_list_of_orders.srw
forward
global type w_list_of_orders from w_base_response_ext
end type
type lb_order_numbers from listbox within w_list_of_orders
end type
end forward

global type w_list_of_orders from w_base_response_ext
int X=1344
int Y=400
int Width=823
int Height=1416
boolean TitleBar=true
string Title="Go to Sales Order"
long BackColor=12632256
lb_order_numbers lb_order_numbers
end type
global w_list_of_orders w_list_of_orders

type variables
Window	lw_Sales_Windows[]
String is_New_order
end variables

event ue_postopen;call super::ue_postopen;Window	ls_sheet
String 	ls_Order_ID
Long		ll_Number_of_Windows

ls_Sheet = iw_frame.GetFirstSheet( )
ll_Number_Of_Windows ++
Do While IsValid( ls_Sheet)
	If ls_Sheet.ClassName()  = 'w_sales_order_detail' Then
		ls_sheet.TriggerEvent( "ue_getinstances")
		ls_Order_ID  = Message.StringParm
		IF Not iw_frame.iu_string.nf_IsEmpty( ls_Order_ID) tHEN
			ll_Number_of_Windows = lb_order_numbers.AddItem( ls_Order_ID)
			lw_sales_windows[ ll_Number_Of_Windows] = ls_Sheet
		END IF
	END IF
	ls_Sheet = iw_frame.GetNextSheet( ls_Sheet)
Loop

lb_order_numbers.SelectItem(1)
lb_order_numbers.SetFocus()
end event

on w_list_of_orders.create
int iCurrent
call super::create
this.lb_order_numbers=create lb_order_numbers
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.lb_order_numbers
end on

on w_list_of_orders.destroy
call super::destroy
destroy(this.lb_order_numbers)
end on

on open;call w_base_response_ext::open;is_new_order = Message.StringParm
end on

event ue_base_ok;window	lw_OrderWindow
Long	ll_SelectedIndex
w_sales_order_detail lw_Temp
SetPointer(HourGlass!)
ll_SelectedIndex = lb_order_numbers.SelectedIndex()
IF  ll_SelectedIndex < 2 Then
	// OPen a new window
	//iw_frame.wf_OpenSheet("w_sales_order_detail", is_new_order)
	opensheetwithparm(lw_Temp, is_new_order,"w_sales_order_detail", &
						  iw_frame,15, Original!)
	lw_Temp.SetFocus()
	lw_Temp.BringToTop = TRUE				  
ELSE
	lw_OrderWindow = lw_sales_windows[ ll_SelectedIndex ]
	Message.StringParm	=	is_new_order
  	lw_OrderWindow.TriggerEvent('ue_Set_Order_id')
	lw_OrderWindow.SetFocus()
	lw_OrderWindow.BringToTop = TRUE
END IF
close(This)
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_list_of_orders
int X=603
int Y=336
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_list_of_orders
int X=507
int Y=212
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_list_of_orders
int X=507
int Y=88
end type

type lb_order_numbers from listbox within w_list_of_orders
int X=73
int Y=72
int Width=370
int Height=1188
int TabOrder=40
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean Sorted=false
long BackColor=16777215
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string Item[]={"Sales Order#"}
end type

event doubleclicked;Parent.TriggerEvent('ue_base_ok')
end event

