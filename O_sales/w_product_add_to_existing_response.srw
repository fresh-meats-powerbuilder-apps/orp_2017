HA$PBExportHeader$w_product_add_to_existing_response.srw
forward
global type w_product_add_to_existing_response from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_product_add_to_existing_response
end type
end forward

global type w_product_add_to_existing_response from w_base_response_ext
integer width = 2853
integer height = 1240
string title = "Product Add to an Existing LIne"
long backcolor = 67108864
dw_1 dw_1
end type
global w_product_add_to_existing_response w_product_add_to_existing_response

type variables
u_orp001	iu_orp001
u_ws_orp1 	iu_ws_orp1

s_error	is_error_info

w_base_sheet	iw_ParentWindow

String	is_open_data


end variables

forward prototypes
public function boolean wf_check_units (integer ai_sub)
end prototypes

public function boolean wf_check_units (integer ai_sub);Decimal{4}	ld_total_qty, ld_move_qty  
Integer		li_sub, li_RowCount 
DataStore	lds_move_rows_data
String		ls_find_string, ls_line_number
Long			ll_found
Boolean		lb_count_on

If mid(is_open_data,1,1) = 'M' Then
	ls_line_number = dw_1.GetItemString(ai_sub, "from_line_number")
	If (ls_line_number = '    ') Then
		li_sub = 1 
		DO UNTIL ls_line_number > '    '
			ls_line_number = dw_1.GetItemString(ai_sub - li_sub, "from_line_number")
			li_sub ++
		LOOP
	End If
	lb_count_on = False
	li_RowCount = dw_1.RowCount()
	For li_sub = 1 to li_RowCount
		If dw_1.GetItemString(li_sub, "from_line_number") = ls_line_number Then
			lb_count_on = True
		End If
		If lb_count_on Then
			If (dw_1.GetItemString(li_sub, "from_line_number") > ls_line_number) Then
				lb_count_on = False
			Else
				ld_total_qty += dw_1.GetItemNumber(li_sub, "from_ord_units")
			End If
		End If
	Next 	
//		MessageBox('Total Qty', 'Total Quantity = ' + String(ld_total_qty))
	
	lds_move_rows_data = Create DataStore
	lds_move_rows_data.DataObject = 'd_sales_detail_base'
	lds_move_rows_data.importstring(mid(is_open_data,3))
		
	ls_find_string = "order_detail_number = '" + ls_line_number + "'"
	ll_found = lds_move_rows_data.Find(ls_find_string, 1, lds_move_rows_data.RowCount())
	ld_move_qty = lds_move_rows_data.GetItemNumber(ll_found, "ordered_units")

	If (ld_total_qty <> ld_move_qty) Then
		MessageBox('Quantity Error', 'Total Ordered Units for line ' + & 
		ls_line_number + ' does not match move quantity of ' + &
		string(ld_move_qty))
		Return False
	End If
Else
	Return True
END IF

Return True


end function

on w_product_add_to_existing_response.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_product_add_to_existing_response.destroy
call super::destroy
destroy(this.dw_1)
end on

event ue_postopen;call super::ue_postopen;String	ls_from_order, &
			ls_to_order, &
			ls_detail_out, &
			ls_temp, &
			ls_option
Integer	li_ret			
			
u_string_functions	lu_string_functions

ls_from_order = lu_string_functions.nf_gettoken(is_open_data, '~t')
ls_to_order = lu_string_functions.nf_gettoken(is_open_data, '~t')
ls_option = lu_string_functions.nf_gettoken(is_open_data, '~t')


ls_temp = dw_1.Object.t_8.Text

dw_1.Object.t_8.Text = dw_1.Object.t_8.Text + ' ' + ls_from_order 
dw_1.Object.t_9.Text = dw_1.Object.t_9.Text + ' ' + ls_to_order

is_error_info.se_app_name = "ORP"
is_error_info.se_event_name="CLICKED"
is_error_info.se_function_name="check duplicates"
is_error_info.se_procedure_name = "nf_orpo22br"
is_error_info.se_user_id = Upper(Trim(Message.nf_getUserID()))
is_error_info.se_window_name = "u_orp001"

//li_ret = iu_orp001.nf_orpo22br_check_duplicates (is_error_info, & 
//																ls_to_order, &
//																ls_from_order, &
//																is_open_data, &
//																ls_detail_out)
																
li_ret = iu_ws_orp1.nf_orpo22fr(is_error_info, & 
										ls_to_order, &
										ls_from_order, &
										ls_option,&
										is_open_data, &
										ls_detail_out)																
																
																
																
dw_1.Reset()
li_ret = dw_1.ImportString(ls_detail_out)




 			


end event

event open;call super::open;is_open_data = Message.StringParm

iu_orp001 = Create u_orp001
iu_ws_orp1 = Create u_ws_orp1


end event

event close;call super::close;IF IsValid( iu_orp001) Then Destroy( iu_orp001)
Destroy iu_ws_orp1
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"Cancel")
end event

event ue_base_ok;call super::ue_base_ok;Integer		li_sub, li_RowCount

If dw_1.AcceptText() = -1 Then Return

li_RowCount = dw_1.RowCount()

For li_sub = 1 to li_RowCount
	If dw_1.GetItemNumber(li_sub, "from_ord_units") > 0 Then
		If wf_check_units(li_sub) = False Then
			Return 
		End If
	End If
Next

CloseWithReturn(This,dw_1.Describe("DataWindow.Data"))

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_product_add_to_existing_response
integer x = 1477
integer y = 1512
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_product_add_to_existing_response
integer x = 1376
integer y = 988
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_product_add_to_existing_response
integer x = 791
integer y = 988
end type

type cb_browse from w_base_response_ext`cb_browse within w_product_add_to_existing_response
end type

type dw_1 from u_base_dw_ext within w_product_add_to_existing_response
integer width = 2793
integer height = 920
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_move_rows_existing_lines"
boolean hscrollbar = true
boolean vscrollbar = true
end type

