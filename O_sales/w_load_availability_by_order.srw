HA$PBExportHeader$w_load_availability_by_order.srw
forward
global type w_load_availability_by_order from w_base_sheet_ext
end type
type dw_ship_date from u_ship_date within w_load_availability_by_order
end type
type sle_1 from u_base_singlelineedit_ext within w_load_availability_by_order
end type
type st_1 from statictext within w_load_availability_by_order
end type
type st_2 from statictext within w_load_availability_by_order
end type
type cb_1 from commandbutton within w_load_availability_by_order
end type
end forward

global type w_load_availability_by_order from w_base_sheet_ext
integer width = 2363
integer height = 500
string title = "PA - Load Inquiry by Order Inquire"
event ue_ok ( )
dw_ship_date dw_ship_date
sle_1 sle_1
st_1 st_1
st_2 st_2
cb_1 cb_1
end type
global w_load_availability_by_order w_load_availability_by_order

type variables
s_error		istr_error_info

u_orp204	  	iu_orp204

u_ws_orp4      iu_ws_orp4
end variables

forward prototypes
public subroutine wf_gen_report ()
end prototypes

event ue_ok();string	ls_inquire_output

Integer	li_rtn

if dw_ship_date.AcceptText() = -1 then return

ls_inquire_output = sle_1.Text + "~t" + String(dw_ship_date.GetItemDate(1,"ship_date"),"mm/dd/yyyy") + "~r~n" 
	  
istr_error_info.se_event_name = "ue_base_ok"

//messagebox("Data",ls_inquire_output)

li_rtn = iu_ws_orp4.nf_ORPO93FR(istr_error_info, &
												ls_inquire_output)
end event

public subroutine wf_gen_report ();//s_Error	lstr_Error
//
//lstr_Error.se_app_name  = "orp"
//lstr_Error.se_window_name = "w_PA-Summary"
//lstr_Error.se_function_name = "wf_Retriev"
//lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")
//
////li_Rtn = iu_orp003.nf_orpo38ar(lstr_Error, lc_ProcessOption, &
////										lc_AvailableDate, ls_page_descr, &
////										ls_PlantData, ls_DetailData, ls_descr_out)
end subroutine

on w_load_availability_by_order.create
int iCurrent
call super::create
this.dw_ship_date=create dw_ship_date
this.sle_1=create sle_1
this.st_1=create st_1
this.st_2=create st_2
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ship_date
this.Control[iCurrent+2]=this.sle_1
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.cb_1
end on

on w_load_availability_by_order.destroy
call super::destroy
destroy(this.dw_ship_date)
destroy(this.sle_1)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.cb_1)
end on

event ue_postopen;call super::ue_postopen;dw_ship_date.SetItem(1, "ship_date", Today())
sle_1.SetFocus()
iu_ws_orp4 = Create u_ws_orp4 
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')
iw_frame.im_menu.mf_disable('m_inquire')
iw_frame.im_menu.mf_disable('m_print')
end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')
iw_frame.im_menu.mf_enable('m_inquire')
iw_frame.im_menu.mf_disable('m_print')
end event

event open;call super::open;iu_orp204 = Create u_orp204
end event

event close;call super::close;Destroy( iu_orp204)
DESTROY iu_ws_orp4
end event

type dw_ship_date from u_ship_date within w_load_availability_by_order
integer x = 1033
integer y = 132
integer width = 663
integer height = 96
integer taborder = 20
boolean bringtotop = true
end type

type sle_1 from u_base_singlelineedit_ext within w_load_availability_by_order
integer x = 288
integer y = 124
integer width = 215
integer height = 88
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
fontcharset fontcharset = ansi!
string facename = "MS Sans Serif"
long backcolor = 1090519039
textcase textcase = upper!
integer limit = 5
end type

type st_1 from statictext within w_load_availability_by_order
integer x = 82
integer y = 136
integer width = 197
integer height = 76
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Order:"
boolean focusrectangle = false
end type

type st_2 from statictext within w_load_availability_by_order
integer x = 731
integer y = 148
integer width = 325
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Move Order to "
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_load_availability_by_order
integer x = 1952
integer y = 112
integer width = 251
integer height = 108
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&OK"
boolean default = true
end type

event clicked;Parent.TriggerEvent ("ue_ok")
end event

