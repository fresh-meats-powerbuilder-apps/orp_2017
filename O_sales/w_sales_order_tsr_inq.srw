HA$PBExportHeader$w_sales_order_tsr_inq.srw
forward
global type w_sales_order_tsr_inq from w_base_response_ext
end type
type dw_sales_open from u_base_dw_ext within w_sales_order_tsr_inq
end type
end forward

global type w_sales_order_tsr_inq from w_base_response_ext
int X=55
int Y=552
int Width=2249
int Height=400
boolean TitleBar=true
string Title="Sales Orders by TSR Inquire"
long BackColor=12632256
dw_sales_open dw_sales_open
end type
global w_sales_order_tsr_inq w_sales_order_tsr_inq

type variables
String		is_openstring

w_sales_order_tsr 	iw_Parent
end variables

event ue_postopen;call super::ue_postopen;Date					ldt_ship_date

String				ls_ship_date, &
						ls_customer, &
						ls_div_code, &
						ls_psqi, &
						ls_tsr

u_string_functions	lu_string_functions

dw_Sales_open.InsertRow(0)

If Not lu_string_functions.nf_IsEmpty(is_openstring) Then
	ls_tsr = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_customer = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_div_code = lu_string_functions.nf_gettoken(is_openstring, '~t')
	ls_ship_date = lu_string_functions.nf_gettoken(is_openstring, '~t')	
	ldt_ship_date = Date(ls_ship_date)
	ls_psqi = is_openstring
	dw_Sales_open.SetItem(1, "tsr_id", ls_tsr)
	dw_Sales_open.SetItem(1, "ship_date", ldt_ship_date)
	dw_Sales_open.SetItem(1, "cust_id", ls_customer)
	dw_Sales_open.SetItem(1, "div_code", ls_div_code)
	dw_Sales_open.SetItem(1, "short_queue", ls_psqi)
ELSE
	dw_Sales_open.SetItem(1, "tsr_id", "SEM")
	dw_Sales_open.SetItem(1, "ship_date", Today())
	dw_Sales_open.SetItem(1, "cust_id", "")
	dw_Sales_open.SetItem(1, "div_code", "  ")
END IF

dw_sales_open.SetColumn( "cust_id")
dw_Sales_open.SelectText( 1,10)
end event

event open;call super::open;DataWindowChild	ldwc_customer    

is_openstring = message.stringparm

dw_Sales_open.GetChild( "cust_id", ldwc_Customer)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_customer)
ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'customer_id', 'All')
ldwc_customer.SetItem(1, 'short_name', 'All Customers')

dw_sales_open.getchild( "tsr_id", ldwc_customer)
iw_frame.iu_project_functions.nf_gettsrs(ldwc_customer, message.is_smanlocation)

dw_sales_open.getchild( "div_code", ldwc_customer)
iw_frame.iu_project_functions.nf_gettutltype(ldwc_customer, "divcode")

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', 'PK')
ldwc_customer.SetItem(1, 'type_desc', 'ALL PORK DIVISIONS')

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', 'BF')
ldwc_customer.SetItem(1, 'type_desc', 'ALL BEEF DIVISIONS')

ldwc_customer.InsertRow(1)
ldwc_customer.SetItem(1, 'type_code', '  ')
ldwc_customer.SetItem(1, 'type_desc', 'ALL DIVISIONS')



end event

on w_sales_order_tsr_inq.create
int iCurrent
call super::create
this.dw_sales_open=create dw_sales_open
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_open
end on

on w_sales_order_tsr_inq.destroy
call super::destroy
destroy(this.dw_sales_open)
end on

event ue_base_ok;call super::ue_base_ok;DataWindowChild	ldwc_Temp
String			ls_value

u_string_functions	lu_string_functions

If dw_sales_open.AcceptText() = -1 Then Return

ls_value = dw_sales_open.GetItemString(1, 'tsr_id')
If lu_string_functions.nf_isempty(ls_value) Then
	iw_frame.SetMicroHelp('TSR is a required field.')
	Return
End If	

//dw_sales_open.GetChild("tsr_id",ldwc_Temp)
//IF ldwc_Temp.Find("smancode = '"+ls_value+"'",1, ldwc_Temp.RowCount()) < 1 Then
//	iw_frame.SetMicrohelp('TSR is invalid')
//	dw_sales_open.SetColumn('tsr_id')
//	Return 
//END IF
CloseWithReturn( This, Upper(dw_sales_open.Describe("DataWindow.Data")))
end event

event ue_base_cancel;call super::ue_base_cancel;iw_frame.setmicrohelp('Ready')
CloseWithReturn(This, "Abort")
end event

event close;call super::close;DataWindowChild	ldwc_customer    


dw_Sales_open.GetChild( "cust_id", ldwc_Customer)

ldwc_customer.DeleteRow(1)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_sales_order_tsr_inq
int X=1815
int Y=396
int TabOrder=0
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_sales_order_tsr_inq
int X=1947
int Y=136
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_sales_order_tsr_inq
int X=1947
int Y=20
end type

type cb_browse from w_base_response_ext`cb_browse within w_sales_order_tsr_inq
int TabOrder=0
end type

type dw_sales_open from u_base_dw_ext within w_sales_order_tsr_inq
event ue_postitemchanged ( )
int X=27
int Y=8
int Width=1829
int Height=268
int TabOrder=10
string DataObject="d_sales_order_tsr_inq"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event ue_postitemchanged;call super::ue_postitemchanged;This.SetItem(1, 'div_code', '  ')

end event

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_Temp

u_string_functions	lu_string_functions

CHOOSE CASE This.GetColumnName()
CASE "customer_id"
	If lu_string_functions.nf_isempty(data) Then
	iw_frame.SetMicroHelp('Customer Id is a required field.')
		Return 1
	End If	
Case 'div_code'
	If data = 'ALL DIVISIONS' Then
		This.PostEvent('ue_postitemchanged')
	End IF
	dw_sales_open.GetChild("div_code",ldwc_Temp)
	IF ldwc_Temp.Find("type_code = '"+data+"'",1, ldwc_Temp.RowCount()) < 1 then
		iw_frame.SetMicrohelp('Division code is invalid')
		This.SetItem(1, 'div_code', '  ')
		This.SetColumn('div_code')
		Return 1
	END IF
//Case 'tsr_id'
//	dw_sales_open.GetChild("tsr_id",ldwc_Temp)
//	IF ldwc_Temp.Find("smancode = '"+data+"'",1, ldwc_Temp.RowCount()) < 1 then
//		iw_frame.SetMicrohelp('TSR is invalid')
//		This.SetColumn('tsr_id')
//		Return 1
//	END IF
END CHOOSE
iw_frame.setmicrohelp('Ready')
end event

event itemerror;call super::itemerror;Choose Case This.GetColumnName()
	Case 'div_code'
		iw_frame.SetMicrohelp('Division code is invalid - Select a valid Division')
		Return 1
	Case Else
		Return 1
End Choose

Return 1
end event

