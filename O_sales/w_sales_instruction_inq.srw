HA$PBExportHeader$w_sales_instruction_inq.srw
$PBExportComments$popup used to inq fro sales order instructions
forward
global type w_sales_instruction_inq from w_base_response_ext
end type
type st_1 from statictext within w_sales_instruction_inq
end type
type em_sales_id from editmask within w_sales_instruction_inq
end type
type dw_instruction from u_base_dw_ext within w_sales_instruction_inq
end type
type st_2 from statictext within w_sales_instruction_inq
end type
end forward

type wstr_instruction_type from structure
    boolean show_Instruction
    string Instruction_List
end type

global type w_sales_instruction_inq from w_base_response_ext
int X=55
int Y=553
int Width=1518
int Height=417
boolean TitleBar=true
string Title="Sales Order Inquiry"
long BackColor=12632256
st_1 st_1
em_sales_id em_sales_id
dw_instruction dw_instruction
st_2 st_2
end type
global w_sales_instruction_inq w_sales_instruction_inq

type variables
String	is_passedstring, &
	is_instruction_type

w_sales_order_detail iw_Parent



end variables

event ue_postopen;call super::ue_postopen;DataWindowChild    lddwc_instruction

String 	ls_Instruction_Type, &
			ls_boolean
			
boolean	lb_incomplete_order			
		 
u_conversion_functions	lu_conversion

ls_boolean	=	iw_frame.iu_string.nf_gettoken(is_passedstring, '~t')
lb_Incomplete_Order = lu_conversion.nf_boolean( ls_boolean)
em_sales_id.text	=	iw_frame.iu_string.nf_gettoken(is_passedstring, '~t')
IF lb_Incomplete_Order Then
	em_sales_id.Enabled = FALSE
	em_sales_ID.backcolor = 12632256
ELSE
	em_sales_id.SetFocus()
	em_sales_id.SelectText( 1, Len(em_sales_id.Text))
END IF
is_instruction_type = is_passedstring
dw_instruction.InsertRow(0)
dw_instruction.GetChild( "instruction", lddwc_instruction)
lddwc_instruction.SetTransObject(SQLCA)
lddwc_instruction.Retrieve()
dw_instruction.SetItem( 1, "instruction", is_instruction_type)


end event

event open;call super::open;is_passedstring  = Message.StringParm   



end event

on w_sales_instruction_inq.create
int iCurrent
call w_base_response_ext::create
this.st_1=create st_1
this.em_sales_id=create em_sales_id
this.dw_instruction=create dw_instruction
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=em_sales_id
this.Control[iCurrent+3]=dw_instruction
this.Control[iCurrent+4]=st_2
end on

on w_sales_instruction_inq.destroy
call w_base_response_ext::destroy
destroy(this.st_1)
destroy(this.em_sales_id)
destroy(this.dw_instruction)
destroy(this.st_2)
end on

event ue_base_ok;call super::ue_base_ok;String ls_Return_String 

IF LEN(TRIM(em_sales_id.Text))  = 0 Then
	Messagebox("Sales Order Inquire", "Please enter a sales order number to inquire on")
	Return
END IF

ls_Return_String = Upper(em_sales_id.Text) + '~t' + is_instruction_type

CloseWithReturn(This, ls_Return_String)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,'Abort')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_sales_instruction_inq
int X=124
int Y=181
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_sales_instruction_inq
int X=1180
int Y=169
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_sales_instruction_inq
int X=1180
int Y=41
end type

type st_1 from statictext within w_sales_instruction_inq
int X=60
int Y=69
int Width=444
int Height=73
boolean Enabled=false
string Text="&Sales Order ID"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_sales_id from editmask within w_sales_instruction_inq
int X=513
int Y=69
int Width=503
int Height=89
int TabOrder=50
BorderStyle BorderStyle=StyleLowered!
string Mask="!####"
MaskDataType MaskDataType=StringMask!
int Accelerator=83
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on modified;//IF (NOT(IsNumber(Left(ls_value,1))) OR Not(f_IsAlpha(left(ls_value,1),1,1))) AND Not(IsNumber(Mid(ls_value,2,4)))  THEN
//	MessageBox("Invalid Order Number - "+ls_value,"All order Ids must start with one letter or number and end with 4 numbers")
//	SetActionCode(1)
//END IF
//
end on

type dw_instruction from u_base_dw_ext within w_sales_instruction_inq
int X=513
int Y=189
int Width=503
int Height=89
int TabOrder=40
string DataObject="d_instruction_type"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event itemchanged;call super::itemchanged;IF dwo.Name	=	'instruction' THEN
	is_instruction_type	=	data
END IF
end event

type st_2 from statictext within w_sales_instruction_inq
int X=51
int Y=193
int Width=435
int Height=89
boolean Enabled=false
boolean BringToTop=true
string Text="Instruction Type"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-10
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

