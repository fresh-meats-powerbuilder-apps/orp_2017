HA$PBExportHeader$w_salesordersbycustomernotepad.srw
$PBExportComments$Prompts the user for products ages and quantities
forward
global type w_salesordersbycustomernotepad from w_base_response_ext
end type
type dw_header from u_base_dw_ext within w_salesordersbycustomernotepad
end type
type st_1 from statictext within w_salesordersbycustomernotepad
end type
type st_2 from statictext within w_salesordersbycustomernotepad
end type
type st_3 from statictext within w_salesordersbycustomernotepad
end type
end forward

global type w_salesordersbycustomernotepad from w_base_response_ext
int Width=1203
int Height=1229
boolean TitleBar=true
string Title="Inquire "
long BackColor=12632256
dw_header dw_header
st_1 st_1
st_2 st_2
st_3 st_3
end type
global w_salesordersbycustomernotepad w_salesordersbycustomernotepad

event open;call super::open;//Int li_rows
//String ls_products
//DataWindowChild	ldw_dddwchild
//
//ls_products = Message.StringParm
// // Populate the ordered age DDDW
//dw_header.GetChild("age", ldw_DDDWChild)
//Message.iw_UtlData.wf_GetTypeDesc("AGECODE", ldw_DDDWChild)
//
//dw_Header.Ib_NewRowOnChange = True
//
//li_rows  = dw_Header.ImportString( Trim(ls_Products))
//IF li_rows > 0 Then cb_ok.Enabled = TRUE
//
//dw_header.InsertRow(0)
end event

on w_salesordersbycustomernotepad.create
int iCurrent
call w_base_response_ext::create
this.dw_header=create dw_header
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_header
this.Control[iCurrent+2]=st_1
this.Control[iCurrent+3]=st_2
this.Control[iCurrent+4]=st_3
end on

on w_salesordersbycustomernotepad.destroy
call w_base_response_ext::destroy
destroy(this.dw_header)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
end on

event ue_base_ok;call super::ue_base_ok;String	ls_Return_String
dw_Header.SetRedraw( False)
dw_Header.DeleteRow( dw_Header.RowCount())
dw_Header.AcceptText()
ls_Return_String = dw_Header.Describe("DataWindow.Data")
CloseWithReturn( This, ls_Return_String)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn( This, "Abort")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_salesordersbycustomernotepad
int X=874
int Y=365
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_salesordersbycustomernotepad
int X=865
int Y=229
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_salesordersbycustomernotepad
int X=865
int Y=105
boolean Default=false
end type

type dw_header from u_base_dw_ext within w_salesordersbycustomernotepad
int X=23
int Y=101
int Width=755
int Height=1021
int TabOrder=20
string DataObject="d_browsebyproduct_header"
end type

event itemchanged;call super::itemchanged;IF cb_base_ok.Enabled = False Then
	cb_base_ok.Enabled = TRUE
END IF

end event

type st_1 from statictext within w_salesordersbycustomernotepad
int X=289
int Y=33
int Width=279
int Height=61
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Product"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_salesordersbycustomernotepad
int X=46
int Y=33
int Width=234
int Height=61
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Quantity"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_salesordersbycustomernotepad
int X=581
int Y=33
int Width=106
int Height=61
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="Age"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

