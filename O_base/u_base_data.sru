HA$PBExportHeader$u_base_data.sru
forward
global type u_base_data from nonvisualobject
end type
end forward

global type u_base_data from nonvisualobject
event type integer ue_set_settings ( datawindow adw_microhelp )
event type integer ue_get_sheetsettings ( ref integer ai_save_onexit )
event type integer ue_get_scrollsettings ( ref integer ai_hscrollbars,  ref integer ai_vscrollbars )
event type integer ue_get_exitsettings ( ref integer ai_prompt_onexit,  ref integer ai_prompt_onclose )
event type integer ue_get_dwsettings ( ref integer ai_save_columnlayouts )
event type integer ue_reset_settings ( integer ai_userid,  integer ai_status,  integer ai_datetime,  long al_message_color,  long al_userid_color,  long al_status_color,  long al_datetime_color )
event type integer ue_updatemicrohelp ( string as_type,  integer ai_value )
event type integer ue_reset_winsettings ( integer ai_prompt_on_exit,  integer ai_prompt_save_onexit,  integer ai_save_dwlayouts,  integer ai_display_hscrollbars,  integer ai_display_vscrollbars,  integer ai_prompt_on_close,  string as_always_openas )
end type
global u_base_data u_base_data

type variables
integer	ii_userid, &
	ii_status, &
	ii_datetime

long	il_microhelp_color, &
	il_userid_color, &
	il_status_color, &
	il_datetime_color

integer	ii_prompt_on_exit, &
	ii_prompt_to_save_onexit,&
	ii_save_dw_layouts, &
	ii_display_hscrollbars, &
	ii_display_vscrollbars, &
	ii_prompt_on_close

string	is_always_openas


end variables

forward prototypes
public subroutine nf_alwaysopenas (string as_type)
end prototypes

event ue_set_settings;adw_microhelp.SetRedraw(False)
adw_microhelp.Object.User.Visible 	= ii_userid
adw_microhelp.Object.Status.Visible = ii_status
adw_microhelp.Object.Time.Visible	= ii_datetime
adw_microhelp.Object.user.color 		= il_userid_color
adw_microhelp.Object.status.color 	= il_status_color
adw_microhelp.Object.time.color 		= il_datetime_color
adw_microhelp.Object.message.color 	= il_microhelp_color
adw_microhelp.SetRedraw(True)
return 1
end event

event ue_get_sheetsettings;ai_save_onexit	=	ii_prompt_to_save_onexit
Return 1
end event

event ue_get_scrollsettings;ai_hscrollbars	=	ii_display_hscrollbars
ai_vscrollbars	=	ii_display_vscrollbars
return 1
end event

event ue_get_exitsettings;ai_prompt_onexit	=	ii_prompt_on_exit
ai_prompt_onclose	=	ii_prompt_on_close
Return 1
end event

event ue_get_dwsettings;ai_save_columnlayouts	=	ii_save_dw_layouts
Return 1
end event

event ue_reset_settings;ii_userid			=	ai_userid
ii_status			=	ai_status
ii_datetime			=	ai_datetime
il_userid_color	=	al_userid_color
il_datetime_color	=	al_datetime_color
il_microhelp_color=	al_message_color
il_status_color	=	al_status_color
gw_base_frame.Trigger Event ue_reset_settings()
Return 1
end event

event ue_updatemicrohelp;CHOOSE CASE	as_type
	CASE	'userid'
		ii_userid		=	ai_value
	CASE	'microhelp_status'
		ii_status		=	ai_value
	CASE	'microhelp_datetime'
		ii_datetime		=	ai_value
END CHOOSE
RETURN 1
end event

event ue_reset_winsettings;ii_prompt_on_exit				=	ai_prompt_on_exit
ii_prompt_to_save_onexit	=	ai_prompt_save_onexit
ii_save_dw_layouts			=	ai_save_dwlayouts
ii_display_hscrollbars		=	ai_display_hscrollbars
ii_display_vscrollbars		=	ai_display_vscrollbars
ii_prompt_on_close			=	ai_prompt_on_close
is_always_openas				=	as_always_openas

nf_alwaysopenas(is_always_openas)

Return 1
end event

public subroutine nf_alwaysopenas (string as_type);arrangeopen	lao_arrangeopen

CHOOSE CASE	as_type
	CASE	'original'
		lao_arrangeopen	=	Original!
	CASE	'layered'
		lao_arrangeopen	=	layered!
	CASE	'cascaded'
		lao_arrangeopen	=	cascaded!
	CASE Else
		lao_arrangeopen	=	Original!
END CHOOSE
gw_base_frame.im_base_menu.iao_arrangeopen	=	lao_arrangeopen
end subroutine

on u_base_data.create
TriggerEvent( this, "constructor" )
end on

on u_base_data.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;//gets the values for the settings of the application from ini file
ii_prompt_on_exit				= ProfileInt(gw_base_frame.is_userini, "settings", &
													"Prompt on Exit", 1)
ii_prompt_on_close			= ProfileInt(gw_base_frame.is_userini, "settings", &
													"Prompt on Close", 0)
ii_prompt_to_save_onexit	= ProfileInt(gw_base_frame.is_userini, "settings", &
													"Prompt Save on Exit", 1)
ii_save_dw_layouts 			= ProfileInt(gw_base_frame.is_userini, "settings", &
													"Save Datawindow Layout", 0)
ii_display_hscrollbars 		= ProfileInt(gw_base_frame.is_userini, "settings", &
													"hscrollbars", 0)
ii_display_vscrollbars 		= ProfileInt(gw_base_frame.is_userini, "settings", &
													"vscrollbars", 0)
is_always_openas				= ProfileString(gw_base_frame.is_userini, "settings", &
													"arrangeopen", "original")
ii_userid				 		= ProfileInt(gw_base_frame.is_userini, "settings", &
													"userid", 1)
ii_status				 		= ProfileInt(gw_base_frame.is_userini, "settings", &
													"status", 0)
ii_datetime				 		= ProfileInt(gw_base_frame.is_userini, "settings", &
													"datetime", 1)
il_datetime_color				= Long(ProfileString(gw_base_frame.is_userini, "settings", &
													"datetime color", "0"))
il_microhelp_color			= Long(ProfileString(gw_base_frame.is_userini, "settings", &
													"microhelp color", "0"))													
il_status_color				= Long(ProfileString(gw_base_frame.is_userini, "settings", &
													"status color", "0"))													
il_userid_color				= Long(ProfileString(gw_base_frame.is_userini, "settings", &
													"userid color", "0"))
nf_alwaysopenas(is_always_openas)													
													

end event

event destructor;//writes the values for the settings of the application to ini file
SetProfileString(gw_base_frame.is_userini, "settings", &
					"Prompt on Exit", String(ii_prompt_on_exit))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"Prompt on Close", String(ii_prompt_on_close))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"Prompt Save on Exit", String(ii_prompt_to_save_onexit))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"Save Datawindow Layout", String(ii_save_dw_layouts))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"hscrollbars", String(ii_display_hscrollbars))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"vscrollbars", String(ii_display_vscrollbars))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"arrangeopen", is_always_openas)
SetProfileString(gw_base_frame.is_userini, "settings", &
					"userid", String(ii_userid))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"status", String(ii_status))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"datetime", String(ii_datetime))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"datetime color", String(il_datetime_color))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"microhelp color", String(il_microhelp_color))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"status color", String(il_status_color))
SetProfileString(gw_base_frame.is_userini, "settings", &
					"userid color", String(il_userid_color))
end event

