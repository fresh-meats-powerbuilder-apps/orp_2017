HA$PBExportHeader$graph_app.sra
forward
global u_base_transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global u_base_message message
end forward

global type graph_app from application
 end type
global graph_app graph_app

on graph_app.create
appname = "graph_app"
message = create u_base_message
sqlca = create u_base_transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on graph_app.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

