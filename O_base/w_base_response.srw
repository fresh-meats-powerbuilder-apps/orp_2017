HA$PBExportHeader$w_base_response.srw
$PBExportComments$Base Response Window
forward
global type w_base_response from window
end type
type cb_base_help from u_base_commandbutton within w_base_response
end type
type cb_base_cancel from u_base_commandbutton within w_base_response
end type
type cb_base_ok from u_base_commandbutton within w_base_response
end type
end forward

global type w_base_response from window
integer x = 1074
integer y = 484
integer width = 2501
integer height = 1492
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
windowtype windowtype = response!
event ue_postopen ( unsignedlong wparam,  long lparam )
event ue_help ( unsignedlong wparam,  long lparam )
event ue_base_ok ( )
event ue_base_cancel ( )
event type string ue_get_data ( string as_value )
event ue_set_data ( string as_data_item,  string as_value )
cb_base_help cb_base_help
cb_base_cancel cb_base_cancel
cb_base_ok cb_base_ok
end type
global w_base_response w_base_response

event ue_help;gw_base_frame.wf_context(this.title)

end event

event open;Long	frameX, &
		frameY, &
		responseX, &
		responseY, &
		parent_windowX, &
		parent_windowY
		
responseX = This.X
responseY = This.Y

frameX = gw_netwise_frame.X
frameY = gw_netwise_frame.Y

window	lw_parentwindow

If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		lw_parentwindow = message.powerobjectparm
		parent_windowX = lw_parentwindow.X
		parent_windowY = lw_parentwindow.Y
	Else	
		parent_windowX = 0
		parent_windowY = 0
	End If
Else
	parent_windowX = 0
	parent_windowY = 0
End If

If this.Title = 'Inquire Product Cross Reference Customer' Then
// no parent window causes problems	
	This.Move (400, 600)
Else	

	If responseX < frameX Then
	// added the next if for move rows to a new order, 2nd response windows in a row opening way off the monitor	
		If (parent_windowX > gw_netwise_frame.x) Then
			This.Move (frameX + 400, frameY + frameY + 600)	
		else
			This.Move (frameX + parent_windowX + 400, frameY + parent_windowY + 600)
		end If
	Else	
	// added the next if for move rows to a new order, 2nd response windows in a row opening way off the monitor	
		If (parent_windowX > gw_netwise_frame.x) Then
			This.Move (frameX + 400, frameY + frameY + 600)	
		else
			This.Move (frameX + parent_windowX + 400, frameY + parent_windowY + 600)
		end If	
	End If
End If

// Post an event that handles post-opening processing
PostEvent("ue_postopen")
end event

on w_base_response.create
this.cb_base_help=create cb_base_help
this.cb_base_cancel=create cb_base_cancel
this.cb_base_ok=create cb_base_ok
this.Control[]={this.cb_base_help,&
this.cb_base_cancel,&
this.cb_base_ok}
end on

on w_base_response.destroy
destroy(this.cb_base_help)
destroy(this.cb_base_cancel)
destroy(this.cb_base_ok)
end on

type cb_base_help from u_base_commandbutton within w_base_response
integer x = 334
integer y = 340
integer width = 270
integer height = 108
integer taborder = 3
integer textsize = -9
integer weight = 400
string facename = "MS Sans Serif"
string text = "&Help"
end type

event clicked;call super::clicked;Parent.TriggerEvent ("ue_help")
end event

type cb_base_cancel from u_base_commandbutton within w_base_response
integer x = 334
integer y = 216
integer width = 270
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 400
string facename = "MS Sans Serif"
string text = "&Cancel"
boolean cancel = true
end type

event clicked;call super::clicked;Parent.TriggerEvent ("ue_base_cancel")
end event

type cb_base_ok from u_base_commandbutton within w_base_response
integer x = 334
integer y = 92
integer width = 270
integer height = 108
integer taborder = 10
integer textsize = -9
integer weight = 400
string facename = "MS Sans Serif"
string text = "&OK"
boolean default = true
end type

event clicked;call super::clicked;Parent.TriggerEvent ("ue_base_ok")
end event

