HA$PBExportHeader$w_productpopup.srw
forward
global type w_productpopup from window
end type
type dw_productcodeshort from datawindow within w_productpopup
end type
end forward

global type w_productpopup from window
string tag = "Product"
integer x = 251
integer y = 208
integer width = 1079
integer height = 1208
boolean titlebar = true
boolean controlmenu = true
boolean minbox = true
windowtype windowtype = popup!
long backcolor = 79741120
event ue_postopen ( )
event ue_losefocus pbm_dwnkillfocus
dw_productcodeshort dw_productcodeshort
end type
global w_productpopup w_productpopup

type variables
s_error		istr_error_info
u_ws_orp2       iu_ws_orp2
u_cfm001		iu_cfm001
String		 is_group_id
end variables

forward prototypes
public subroutine wf_listproducts ()
end prototypes

event ue_postopen();
is_group_id = Message.StringParm
iu_ws_orp2 = Create u_ws_orp2
iu_Cfm001 = Create u_cfm001 
If Message.ReturnValue = -1 Then
	Close(This)
	return
End if

wf_listproducts()

end event

event ue_losefocus;//close(this)
end event

public subroutine wf_listproducts ();Boolean	lb_ret

String	ls_input, &
			ls_output, &
			ls_short_name, &
			ls_product

long	ll_rowcount, ll_count
			
datawindowchild ldwc_Temp
			
u_string_functions		lu_string_functions
u_project_functions		lu_project_functions

istr_error_info.se_event_name = "wf_listproducts of w_productpopup"

ls_input = is_group_id + '~r~n'
ls_output = " "

SetPointer(HourGlass!)

dw_productcodeshort.Reset()

//lb_ret = iu_cfm001.nf_cfmc31br_skugrp_inq(istr_error_info, &
//											ls_input, &
//											ls_output)

lb_ret = iu_ws_orp2.nf_cfmc31fr(ls_input, &
											ls_output, &
											istr_error_info)

lb_ret= TRUE
//ls_output = "D0101DM" + '~r~n' + &
//					"D0101CE" + '~r~n' 

If lb_ret Then
	If lu_string_functions.nf_isempty(ls_output) Then
		Messagebox("ERROR","No data found.")
		return
	else
		dw_productcodeshort.SetRedraw(False)
		dw_productcodeshort.ImportString(ls_output)
		ll_rowcount = dw_productcodeshort.rowcount()
		for ll_count = 1 to ll_rowcount
			ls_product = dw_productcodeshort.getitemstring(ll_count,"product_code") + &
			space(10 - len(dw_productcodeshort.getitemstring(ll_count,"product_code")))

			SELECT sku_products.short_description
				INTO :ls_short_name
				FROM sku_products
				WHERE sku_products.sku_product_code = :ls_product
				USING SQLCA ;
				
			
			if isnull(ls_short_name) then
				dw_productcodeshort.setitem( ll_count, 'product_description', " ")
			else
				dw_productcodeshort.setitem( ll_count, 'product_description', ls_short_name)
			end if
			
		next
		dw_productcodeshort.SetRedraw(True)
	end if
End if

	

//dw_productcodeshort.ImportString(ls_label)
end subroutine

on w_productpopup.create
this.dw_productcodeshort=create dw_productcodeshort
this.Control[]={this.dw_productcodeshort}
end on

on w_productpopup.destroy
destroy(this.dw_productcodeshort)
end on

event close;Destroy u_ws_orp2
Close(This)

end event

event open;
This.PostEvent('ue_postopen')
this.title = "Product Group Product List"
//dw_productcodeshort.object.product_code.protect = 1
//dw_productcodeshort.Modify ( "product_code.Background.Color = 12632256" )


end event

type dw_productcodeshort from datawindow within w_productpopup
integer x = 14
integer y = 16
integer width = 1038
integer height = 1088
integer taborder = 10
string dataobject = "d_productcodeshort"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event losefocus;//close(parent)
end event

