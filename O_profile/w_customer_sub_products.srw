HA$PBExportHeader$w_customer_sub_products.srw
forward
global type w_customer_sub_products from w_base_response_ext
end type
type dw_sub_product from u_base_dw_ext within w_customer_sub_products
end type
end forward

global type w_customer_sub_products from w_base_response_ext
int Width=1678
int Height=1225
boolean TitleBar=true
string Title="Substitution Products"
long BackColor=12632256
dw_sub_product dw_sub_product
end type
global w_customer_sub_products w_customer_sub_products

event open;call super::open;String						ls_string, &
								ls_product_code, &
								ls_product_descr,&
								ls_modstring

ls_string = message.StringParm

ls_product_code = iw_frame.iu_string.nf_GetToken(ls_string, '~t')
ls_product_descr = iw_frame.iu_string.nf_GetToken(ls_string, '~r')
ls_string = Right(ls_string, Len(ls_string) - 1)

ls_modstring = "product_code_t.Text = '" + ls_product_code  + &
							"' product_descr_t.Text = '" + ls_product_descr + "'"

dw_sub_product.Modify(ls_modstring)
dw_sub_product.ImportString(ls_string)

end event

on ue_postopen;call w_base_response_ext::ue_postopen;dw_sub_product.SelectRow(0, False)
end on

on w_customer_sub_products.create
int iCurrent
call w_base_response_ext::create
this.dw_sub_product=create dw_sub_product
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_sub_product
end on

on w_customer_sub_products.destroy
call w_base_response_ext::destroy
destroy(this.dw_sub_product)
end on

event ue_base_ok;call super::ue_base_ok;Integer			li_count
String			ls_output


For li_count = 1 to dw_sub_product.RowCount()
	If dw_sub_product.IsSelected(li_count) Then
		ls_output += dw_sub_product.GetItemString(li_count, "sub_product_code") + &
				+ "~t" + &
				dw_sub_product.GetItemString(li_count, "sub_product_descr") + "~r"
	End IF
Next

CloseWithReturn(This, ls_output)
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Abort")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_customer_sub_products
int X=1331
int Y=349
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_customer_sub_products
int X=1331
int Y=225
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_customer_sub_products
int X=1331
int Y=101
end type

type dw_sub_product from u_base_dw_ext within w_customer_sub_products
int X=46
int Y=25
int Width=1189
int Height=1097
int TabOrder=20
string DataObject="d_sub_product"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
end type

on constructor;call u_base_dw_ext::constructor;is_selection = '4'
ib_updateable = TRUE

end on

event clicked;// Verify if a valid row was clicked on
If row < 1 Then
	Return
ELSE
	Call Super::Clicked
End If


end event

