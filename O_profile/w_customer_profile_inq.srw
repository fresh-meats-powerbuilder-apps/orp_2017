HA$PBExportHeader$w_customer_profile_inq.srw
forward
global type w_customer_profile_inq from w_base_response_ext
end type
type dw_customer from u_base_dw_ext within w_customer_profile_inq
end type
end forward

global type w_customer_profile_inq from w_base_response_ext
integer x = 219
integer y = 208
integer width = 2222
integer height = 540
string title = "Customer Profile Inquire"
long backcolor = 12632256
dw_customer dw_customer
end type
global w_customer_profile_inq w_customer_profile_inq

type variables
Private:
Boolean		ib_OK_To_Close

window		iw_Parent

u_cfm002		iu_cfm002

boolean 		ib_happened
end variables

forward prototypes
public function boolean nf_populate_dddws ()
public subroutine wf_filldddw (ref datawindowchild ldw_childdw)
end prototypes

public function boolean nf_populate_dddws ();//DATAWINDOWCHILD	ldw_DDDWChild
//
//dw_customer.GetChild("customer_id", ldw_DDDWChild)
//w_Project_Data.wf_GetCustomerData(ldw_DDDWChild)
//
RETURN True
end function

public subroutine wf_filldddw (ref datawindowchild ldw_childdw);IF ldw_childdw.RowCount()	> 0 THEN RETURN
ldw_childdw.SetTransObject(SQLCA)
ldw_childdw.Retrieve()
end subroutine

event open;call super::open;datawindowchild			ldwc_temp

								
String						ls_data
								

ls_data = Message.StringParm

dw_customer.GetChild('customer_id', ldwc_temp)
iw_frame.iu_project_functions.ids_customers.ShareData(ldwc_temp)



dw_customer.GetChild('billto_id', ldwc_temp)
iw_frame.iu_project_functions.ids_billtos.ShareData(ldwc_temp)

dw_customer.GetChild('corp_id', ldwc_temp)
iw_frame.iu_project_functions.ids_corps.ShareData(ldwc_temp)

If dw_customer.ImportString(ls_data) < 1 Then 
	dw_customer.InsertRow(0)
End If

end event

on close;call w_base_response_ext::close;If Not ib_OK_To_Close Then
	Message.StringParm = ""
End if

If IsValid(iu_cfm002) Then destroy iu_cfm002
end on

on w_customer_profile_inq.create
int iCurrent
call super::create
this.dw_customer=create dw_customer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_customer
end on

on w_customer_profile_inq.destroy
call super::destroy
destroy(this.dw_customer)
end on

event ue_base_ok;String	ls_Data, ls_findexpression
long		ll_rowfound
datawindowchild	ldw_childdw

if dw_customer.AcceptText() < 0 then
	dw_customer.SetColumn('customer_id')
	dw_customer.setfocus()
	return
end if

If iw_frame.iu_string.nf_isempty(dw_customer.GetItemString(1, 'customer_type')) Then
	iw_frame.SetMicroHelp("Customer type is a required field")
	dw_customer.SetColumn('customer_type')
	dw_customer.SetFocus()
	return
End if

If iw_frame.iu_string.nf_isempty(dw_customer.GetItemString(1, 'customer_id')) Then
	iw_frame.SetMicroHelp("Customer is a required field")
	dw_customer.SetColumn('customer_id')
	dw_customer.SetFocus()
	return
End if

If not iw_frame.iu_string.nf_isempty(dw_customer.GetItemString(1, 'division')) Then
	dw_customer.GetChild('division', ldw_ChildDW)
	ll_rowfound	=	ldw_ChildDW.RowCount()
	ls_FindExpression = "type_code = '" + dw_customer.GetItemString(1, 'division') + "'"
	ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
	If  ll_RowFound <= 0 Then 
		iw_frame.SetMicroHelp(dw_customer.GetItemString(1, 'division') + ' is an invalid division')
		dw_customer.SetColumn('division')
		dw_customer.SetFocus()
		dw_customer.SelectText(1, Len(dw_customer.GetItemString(1, 'division')))
		return
	END IF
end if

dw_customer.SetItem(1, 'division',Trim(dw_customer.GetItemString(1, 'division')))
ls_Data = dw_customer.Describe("DataWindow.Data") + '~t' + &
			dw_customer.Object.customer_id_name.Text + '~t' + &
			dw_customer.Object.customer_id_city.Text + '~t' + &
			dw_customer.Object.t_state.Text + '~t' + &
			dw_customer.Object.t_zip.Text

iw_frame.SetMicroHelp("")			
			
ib_OK_To_Close = True
CloseWithReturn(This, ls_Data)
end event

event ue_base_cancel;CloseWithReturn(This, "ABORT")
end event

event ue_postopen;String	ls_CustomerName, &
			ls_CustomerCity, &
			ls_state, &
			ls_zip, &
			ls_customer

datawindowchild	ldw_DDDWChild
			
dw_customer.SetColumn('customer_id')

dw_customer.GetChild('division', ldw_DDDWChild)
wf_filldddw(ldw_DDDWChild)

dw_customer.GetChild('contact_type', ldw_DDDWChild)
wf_filldddw(ldw_DDDWChild)

if dw_customer.GetItemString(1,"customer_id") > '0' then
	IF iw_frame.iu_project_functions.nf_ValidateCustomerState( dw_customer.GetItemString(1,"customer_id"), &
																			dw_customer.GetItemString(1,"customer_type"), &
																			ls_CustomerName,ls_CustomerCity,ls_state) < 1 Then
		dw_customer.Object.customer_id_name.Text = ''
		dw_customer.Object.customer_id_city.Text = ''
		dw_customer.Object.t_state.Text = ''
	ELSE
		if isnull(ls_CustomerName) then
			dw_customer.Object.customer_id_name.Text = ''
		else
			dw_customer.Object.customer_id_name.Text = ls_CustomerName
		end if
		if isnull(ls_CustomerCity) then
			dw_customer.Object.customer_id_city.Text = ''
		else
			dw_customer.Object.customer_id_city.Text = ls_CustomerCity
		end if
		if isnull(ls_state) then
			dw_customer.Object.t_state.Text = ''
		else
			dw_customer.Object.t_state.Text = ls_state
		end if
		ls_customer = dw_customer.GetItemString(1,"customer_id")
		SELECT customers.zip_code
			INTO :ls_zip
			FROM customers
			WHERE customers.customer_id = :ls_customer
			USING SQLCA ;
		IF isnull(ls_zip) THEN
			dw_customer.Object.t_zip.Text = ''
		else
			dw_customer.Object.t_zip.Text = ls_zip
		END IF
	END IF
END IF
	
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_customer_profile_inq
integer x = 1906
integer y = 308
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_customer_profile_inq
integer x = 1627
integer y = 308
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_customer_profile_inq
integer x = 1349
integer y = 308
integer taborder = 30
end type

type cb_browse from w_base_response_ext`cb_browse within w_customer_profile_inq
end type

type dw_customer from u_base_dw_ext within w_customer_profile_inq
integer x = 9
integer y = 12
integer width = 2167
integer height = 336
integer taborder = 20
string dataobject = "d_customer_profile_inq"
boolean border = false
end type

event itemchanged;call super::itemchanged;String				ls_CustomerName,&
						ls_CustomerCity, &
						ls_state, &
						ls_zip, &
						ls_FindExpression , &
						ls_customer
Long					ll_RowFound

datawindowchild	ldw_childdw		
S_Error		lstr_Error_Info

Choose Case This.GetColumnName()
	Case 'customer_type'
		CHOOSE Case data 
			Case  'B' 
				This.SetItem( 1, "customer_id", "")
			
			Case  'C'
				This.SetItem( 1, "customer_id", "")
			Case  'S'
				This.SetItem( 1, "customer_id", "")
		END  CHOOSE
		this.setitem(1, "Division", "")
		this.setitem(1, "contact_type", "")
		This.Object.customer_id_name.Text = ''
		This.Object.customer_id_city.Text = ''
		This.Object.t_state.Text = ''
		this.object.t_zip.text = ''
	Case 'corp_id',"billto_id","customer_id"
		IF iw_frame.iu_project_functions.nf_ValidateCustomerState( Data, This.GetItemString(1,"customer_type"),&
																				ls_CustomerName,ls_CustomerCity,ls_state) < 1 Then
																				
			IF lstr_error_info.se_message = "" then iw_frame.setmicrohelp("INVALID/INACTIVE CUSTOMER ID")
			This.Object.customer_id_name.Text = ''
			This.Object.customer_id_city.Text = ''
			This.Object.t_state.Text = ''
			this.setitem(1,'customer_id',"")
			This.SelectText(1, Len(data))
			This.SetColumn('customer_id')
													
			Return 1
		ELSE
			if isnull(ls_CustomerName) THEN
				This.Object.customer_id_name.Text = ''
			else
				This.Object.customer_id_name.Text = ls_CustomerName
			end if
			if isnull(ls_CustomerCity) THEN
				This.Object.customer_id_city.Text = ''
			else
				This.Object.customer_id_city.Text = ls_CustomerCity
			end if
			if isnull(ls_state) THEN
				This.Object.t_state.Text = ''
			else
				This.Object.t_state.text = ls_state
			end if
			this.SetTransObject(SQLCA)
			SELECT customers.zip_code
				INTO :ls_zip
				FROM customers
				WHERE customers.customer_id = :data
				USING SQLCA ;
			IF not isnull(ls_zip) THEN
				dw_customer.Object.t_zip.Text = ls_zip
			END IF 
		END IF
	case 'division'
		This.GetChild('division', ldw_ChildDW)
		ll_rowfound	=	ldw_ChildDW.RowCount()
		ls_FindExpression = "type_code = '" + Trim(data) + "'"
		ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
		If  ll_RowFound <= 0 Then 
			iw_frame.SetMicroHelp(data + ' is an invalid division')
//			This.SelectText(1, Len(data))
		END IF	
//	case 'contact_type'
//		If data = "A" then
//			This.SetItem( 1, "contact_type", "ASSISTBUY")
//		END IF
End Choose




end event

event itemerror;This.SelectText(1, Len(data))
RETURN 1
end event

event itemfocuschanged;call super::itemfocuschanged;this.accepttext()
end event

