HA$PBExportHeader$w_checkcustomers.srw
forward
global type w_checkcustomers from w_base_sheet_ext
end type
type tab_1 from tab within w_checkcustomers
end type
type tabpage_1 from userobject within tab_1
end type
type dw_shipto_customers from u_base_dw_ext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_shipto_customers dw_shipto_customers
end type
type tabpage_2 from userobject within tab_1
end type
type dw_billtos from u_base_dw_ext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_billtos dw_billtos
end type
type tabpage_3 from userobject within tab_1
end type
type dw_corps from u_base_dw_ext within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_corps dw_corps
end type
type tabpage_4 from userobject within tab_1
end type
type dw_defaults from u_base_dw_ext within tabpage_4
end type
type tabpage_4 from userobject within tab_1
dw_defaults dw_defaults
end type
type tab_1 from tab within w_checkcustomers
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
end type
type cbx_1 from checkbox within w_checkcustomers
end type
end forward

global type w_checkcustomers from w_base_sheet_ext
integer width = 2889
integer height = 1484
string title = "View Customer Data"
tab_1 tab_1
cbx_1 cbx_1
end type
global w_checkcustomers w_checkcustomers

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();Int li_rtn
String	ls_ReturnValue

Open(w_checkcustomers_inq)
ls_ReturnValue = Message.StringParm



Choose Case ls_ReturnValue
	Case "Cancel"
		Return FALSE
	Case "Checked"
		tab_1.tabpage_1.dw_shipto_customers.ShareDataOff()
		tab_1.tabpage_2.dw_billtos.ShareDataOff()
		tab_1.tabpage_3.dw_corps.ShareDataOff()
		
		li_rtn = iw_Frame.iu_project_functions.ids_customers.ShareData(tab_1.tabpage_1.dw_shipto_customers)
		li_rtn = iw_Frame.iu_project_functions.ids_Billtos.ShareData(tab_1.tabpage_2.dw_billtos)
		li_rtn = iw_Frame.iu_project_functions.ids_Corps.ShareData(tab_1.tabpage_3.dw_corps)
		cbx_1.Checked = True
	Case Else
		tab_1.tabpage_1.dw_shipto_customers.ShareDataOff()
		tab_1.tabpage_2.dw_billtos.ShareDataOff()
		tab_1.tabpage_3.dw_corps.ShareDataOff()
		
		tab_1.tabpage_1.dw_shipto_customers.Reset()
		tab_1.tabpage_2.dw_billtos.Reset()
		tab_1.tabpage_3.dw_corps.Reset()

		tab_1.tabpage_1.dw_shipto_customers.SetTransObject( SQLCA)
		tab_1.tabpage_2.dw_billtos.SetTransObject( SQLCA)
		tab_1.tabpage_3.dw_corps.SetTransObject( SQLCA)

		tab_1.tabpage_1.dw_shipto_customers.Retrieve("   ", "ZZZ")
		tab_1.tabpage_2.dw_billtos.Retrieve()
		tab_1.tabpage_3.dw_corps.Retrieve()
		cbx_1.Checked = False
		
END CHOOSE
Return True
end function

on w_checkcustomers.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.cbx_1=create cbx_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.cbx_1
end on

on w_checkcustomers.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.cbx_1)
end on

event ue_postopen;call super::ue_postopen;iw_Frame.im_Menu.mf_Disable("m_save")
wf_retrieve()
tab_1.tabpage_4.dw_defaults.SetTransObject( SQLCA)
tab_1.tabpage_4.dw_defaults.Retrieve( )
end event

event activate;call super::activate;iw_Frame.im_Menu.mf_Disable("m_save")
end event

event deactivate;call super::deactivate;iw_Frame.im_Menu.mf_Enable("m_save")
end event

type tab_1 from tab within w_checkcustomers
integer x = 46
integer y = 104
integer width = 2766
integer height = 1232
integer taborder = 1
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2729
integer height = 1116
long backcolor = 12632256
string text = "Shipto~'s"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_shipto_customers dw_shipto_customers
end type

on tabpage_1.create
this.dw_shipto_customers=create dw_shipto_customers
this.Control[]={this.dw_shipto_customers}
end on

on tabpage_1.destroy
destroy(this.dw_shipto_customers)
end on

type dw_shipto_customers from u_base_dw_ext within tabpage_1
integer width = 2670
integer height = 1076
integer taborder = 2
string dataobject = "d_customer_data"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2729
integer height = 1116
long backcolor = 12632256
string text = "Billto~'s"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_billtos dw_billtos
end type

on tabpage_2.create
this.dw_billtos=create dw_billtos
this.Control[]={this.dw_billtos}
end on

on tabpage_2.destroy
destroy(this.dw_billtos)
end on

type dw_billtos from u_base_dw_ext within tabpage_2
integer width = 2706
integer height = 1100
integer taborder = 11
string dataobject = "d_billto_customer_data"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2729
integer height = 1116
long backcolor = 12632256
string text = "Corporate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_corps dw_corps
end type

on tabpage_3.create
this.dw_corps=create dw_corps
this.Control[]={this.dw_corps}
end on

on tabpage_3.destroy
destroy(this.dw_corps)
end on

type dw_corps from u_base_dw_ext within tabpage_3
integer width = 2683
integer height = 1108
integer taborder = 11
string dataobject = "d_corp_customer_data"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type tabpage_4 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2729
integer height = 1116
long backcolor = 12632256
string text = "Shipto Defaults"
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_defaults dw_defaults
end type

on tabpage_4.create
this.dw_defaults=create dw_defaults
this.Control[]={this.dw_defaults}
end on

on tabpage_4.destroy
destroy(this.dw_defaults)
end on

type dw_defaults from u_base_dw_ext within tabpage_4
integer width = 2738
integer height = 1124
integer taborder = 11
string dataobject = "d_customer_defaults"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type cbx_1 from checkbox within w_checkcustomers
integer x = 55
integer y = 16
integer width = 498
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Location Specific"
boolean lefttext = true
end type

