HA$PBExportHeader$utlu01sr_programinterfaceutlu01ci.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_ProgramInterfaceUtlu01ci from nonvisualobject
    end type
end forward

global type utlu01sr_ProgramInterfaceUtlu01ci from nonvisualobject
end type

type variables
    utlu01sr_ProgramInterfaceUtlu01ciUtl000sr_cics_container utl000sr_cics_container
    boolean structuredContainer
end variables

on utlu01sr_ProgramInterfaceUtlu01ci.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_ProgramInterfaceUtlu01ci.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

