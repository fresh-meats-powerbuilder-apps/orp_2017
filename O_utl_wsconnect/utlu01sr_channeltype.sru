HA$PBExportHeader$utlu01sr_channeltype.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type utlu01sr_channelType from nonvisualobject
    end type
end forward

global type utlu01sr_channelType from nonvisualobject
end type

type variables
    boolean channel
end variables

on utlu01sr_channelType.create
call super::create
TriggerEvent( this, "constructor" )
end on

on utlu01sr_channelType.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

