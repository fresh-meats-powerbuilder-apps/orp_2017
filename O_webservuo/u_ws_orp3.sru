HA$PBExportHeader$u_ws_orp3.sru
forward
global type u_ws_orp3 from u_orp_webservice
end type
end forward

global type u_ws_orp3 from u_orp_webservice
end type
global u_ws_orp3 u_ws_orp3

forward prototypes
public function integer uf_orpo52fr (ref s_error astr_error_info, character ac_mode_indicator, ref string as_parameter_info)
public function integer uf_orpo53fr (ref s_error astr_error_info, string product_string_in)
public function integer uf_orpo55fr (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer uf_orpo56fr (ref s_error astr_error_info, ref string as_sales_info)
public function integer uf_orpo62fr (ref s_error astr_error_info, ref string as_detail_info, character ac_action_ind)
public function integer uf_orpo63fr_cancel_sales_order (s_error astr_error_info, string as_order_id)
public function integer uf_orpo66fr_getsubs (ref s_error astr_error_info, string as_product_code, ref string as_output)
public function boolean uf_orpo70fr_inq_sku_prod (ref s_error astr_error_info, string as_input, ref string as_output)
public function boolean uf_orpo71fr_sales_order_variance (ref s_error astr_error_info, string as_header_in, ref string as_detail_out)
public function boolean uf_orpo72fr_inq_exch_rate (ref s_error astr_error_info, string as_input, ref string as_output)
public function boolean uf_orpo73fr_inq_monthly_exch_rates (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer uf_orpo75fr_validate_products (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
public function integer uf_orpo51fr (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, string ac_mode_indicator)
public function integer uf_orpo54fr_product_browse_update (ref s_error astr_error_info, ref string as_customer_info_in, ref string as_main_info_in_1, ref string as_main_info_in_2, ref string as_main_info_in_3, ref string as_main_info_out_1, ref string as_main_info_out_2, ref string as_main_info_out_3, ref string as_dup_orders, string as_dup_product_string)
public function boolean uf_orpo69fr_pricing_review_inq (ref s_error astr_error_info, string as_header_in, string as_detail_in, ref string as_detail_out, ref string as_header_out)
public function integer uf_orpo60fr_resutil_resproduct (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info)
public function integer uf_orpo61fr_sc_resutl_weekres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_week_out)
public function integer uf_orpo64fr_sc_resutl_scres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out)
public function integer uf_orpo67fr_sc_resutl_tsrres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out)
public function integer uf_orpo59fr_resutil_custres (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info)
public function integer uf_orpo58fr_resutil_tsrcust (ref s_error astr_error_info, string as_input_info, ref string as_output_info)
public function integer uf_orpo57fr (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_sales_info, string as_roll_info)
public function integer uf_orpo74fr_customer_order_print (ref s_error astr_error_info, string as_input)
public function integer uf_orpo02gr_inq_reservatiion_item (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_orpo03gr_upd_reservation_item (ref s_error astr_error_info, string as_input_string)
public function integer uf_orpo13gr_linked_orders_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_orpo14gr_linked_orders_upd (ref s_error astr_error_info, string as_input_string)
public function integer uf_orpo11gr_edi_trans_by_cust_inq (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string)
public function integer uf_orpo12gr_edi_trans_by_cust_upd (ref s_error astr_error_info, string as_header_string, string as_detail_string)
public function integer uf_orpo10gr_product_to_global_upd (ref s_error astr_error_info, string as_input_string)
public function integer uf_orpo99gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_orpo98gr (string as_order_id, ref datawindowchild ad_header_dw, ref datawindowchild ad_detail_dw, ref datawindowchild ad_pallet_dw, ref datawindowchild ad_tare_dw, ref datawindowchild ad_hides_dw, ref s_error astr_error_info)
end prototypes

public function integer uf_orpo52fr (ref s_error astr_error_info, character ac_mode_indicator, ref string as_parameter_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO52FR'
ls_tran_id = 'O52F'


ls_input_string = ac_mode_indicator +'~t' +  as_parameter_info


li_rval = uf_rpccall(ls_input_string,as_parameter_info, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo53fr (ref s_error astr_error_info, string product_string_in);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO53FR'
ls_tran_id = 'O53F'


ls_input_string = product_string_in


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo55fr (ref s_error astr_error_info, string as_input, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO55FR'
ls_tran_id = 'O55F'


ls_input_string = as_input


li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo56fr (ref s_error astr_error_info, ref string as_sales_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO56FR'
ls_tran_id = 'O56F'


ls_input_string = as_sales_info


li_rval = uf_rpccall(ls_input_string, as_sales_info, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo62fr (ref s_error astr_error_info, ref string as_detail_info, character ac_action_ind);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO62FR'
ls_tran_id = 'O62F'


ls_input_string = ac_action_ind +'~t' +  as_detail_info


li_rval = uf_rpccall(ls_input_string, as_detail_info, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer uf_orpo63fr_cancel_sales_order (s_error astr_error_info, string as_order_id);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO63FR'
ls_tran_id = 'O63F'


ls_input_string = as_order_id


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo66fr_getsubs (ref s_error astr_error_info, string as_product_code, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO66FR'
ls_tran_id = 'O66F'


ls_input_string = as_product_code


li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean uf_orpo70fr_inq_sku_prod (ref s_error astr_error_info, string as_input, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO70FR'
ls_tran_id = 'O70F'


ls_input_string = as_input


li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then 	return true

return false
end function

public function boolean uf_orpo71fr_sales_order_variance (ref s_error astr_error_info, string as_header_in, ref string as_detail_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO71FR'
ls_tran_id = 'O71F'


ls_input_string = as_header_in


li_rval = uf_rpccall(ls_input_string, as_detail_out, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then 	return true

return false
end function

public function boolean uf_orpo72fr_inq_exch_rate (ref s_error astr_error_info, string as_input, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO72FR'
ls_tran_id = 'O72F'


ls_input_string = as_input


li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then 	return true

return false
end function

public function boolean uf_orpo73fr_inq_monthly_exch_rates (ref s_error astr_error_info, string as_input, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO73FR'
ls_tran_id = 'O73F'


ls_input_string = as_input


li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then 	return true

return false
end function

public function integer uf_orpo75fr_validate_products (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO75FR'
ls_tran_id = 'O75F'


ls_input_string = as_header_string + '~t' + as_detail_string


li_rval = uf_rpccall(ls_input_string, as_detail_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo51fr (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, string ac_mode_indicator);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO51FR'
ls_tran_id = 'O51F'


ls_input_string = ac_mode_indicator + '~h7F' + as_header_in + '~h7F' + as_detail_in

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
as_header_in = ''
as_detail_in = ''

//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_in += ls_output
			Case 'D'
				as_detail_in += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval
end function

public function integer uf_orpo54fr_product_browse_update (ref s_error astr_error_info, ref string as_customer_info_in, ref string as_main_info_in_1, ref string as_main_info_in_2, ref string as_main_info_in_3, ref string as_main_info_out_1, ref string as_main_info_out_2, ref string as_main_info_out_3, ref string as_dup_orders, string as_dup_product_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO54FR'
ls_tran_id = 'O54F'


ls_input_string = as_customer_info_in + '~h7F' + as_main_info_in_1 +  + '~h7F' + as_main_info_in_2 + '~h7F' + as_main_info_in_3 + '~h7F' + as_dup_orders + '~h7F' + as_dup_product_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

//as_customer_info_in = ''
//as_main_info_in_1 = '' 
//as_main_info_in_2 = ''
//as_main_info_in_3 = ''
as_main_info_out_1 = '' 
as_main_info_out_2 = ''
as_main_info_out_3 = ''
as_dup_orders = '' 
//as_dup_product_string = ''
ls_stringsep = Left(ls_outputstring, 1)
//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'A'
				as_main_info_out_1 += ls_output
			Case 'B'
				as_main_info_out_2 += ls_output
			Case 'C'
				as_main_info_out_3 += ls_output
			Case 'D'
				as_dup_orders += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval
end function

public function boolean uf_orpo69fr_pricing_review_inq (ref s_error astr_error_info, string as_header_in, string as_detail_in, ref string as_detail_out, ref string as_header_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO69FR'
ls_tran_id = 'O69F'


ls_input_string = as_header_in + '~h7F' + as_detail_in

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
as_detail_out = ''
as_header_out = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_out += ls_output
			Case 'D'
				as_detail_out += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
	
	return true
end if	

return false

end function

public function integer uf_orpo60fr_resutil_resproduct (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header_in, ls_detail_in, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO60FR'
ls_tran_id = 'O60F'


ls_input_string = as_input_info + '~h7F' + as_customer_id

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
ls_header_in = ''
ls_detail_in = ''
as_output_info = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header_in = ls_output
			Case 'D'
				ls_detail_in += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
	as_output_info = ls_header_in + ls_detail_in
end if	
return li_rval
end function

public function integer uf_orpo61fr_sc_resutl_weekres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_week_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO61FR'
ls_tran_id = 'O61F'


ls_input_string = as_input_info

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
as_week_out = ''
as_output_info = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_week_out += ls_output
			Case 'D'
				as_output_info += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval
end function

public function integer uf_orpo64fr_sc_resutl_scres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO64FR'
ls_tran_id = 'O64F'


ls_input_string = as_input_info

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
as_header_out = ''
as_output_info = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_out += ls_output
			Case 'D'
				as_output_info += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval
end function

public function integer uf_orpo67fr_sc_resutl_tsrres (ref s_error astr_error_info, string as_input_info, ref string as_output_info, ref string as_header_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO67FR'
ls_tran_id = 'O67F'


ls_input_string = as_input_info

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
as_header_out = ''
as_output_info = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_out += ls_output
			Case 'D'
				as_output_info += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval
end function

public function integer uf_orpo59fr_resutil_custres (ref s_error astr_error_info, string as_input_info, ref string as_customer_id, ref string as_output_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header_in, ls_detail_in, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO59FR'
ls_tran_id = 'O59F'


ls_input_string = as_input_info + '~h7F' + as_customer_id

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
ls_header_in = ''
ls_detail_in = ''
as_output_info = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header_in = ls_output
			Case 'D'
				ls_detail_in += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
	as_output_info = ls_header_in + ls_detail_in
end if	
return li_rval
end function

public function integer uf_orpo58fr_resutil_tsrcust (ref s_error astr_error_info, string as_input_info, ref string as_output_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header_in, ls_detail_in, ls_stingsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO58FR'
ls_tran_id = 'O58F'


ls_input_string = as_input_info

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stingsep = Left(ls_outputstring, 1)
ls_header_in = ''
ls_detail_in = ''
as_output_info = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stingsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_header_in = ls_output
			Case 'D'
				ls_detail_in += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
	as_output_info = ls_header_in + ls_detail_in
end if	
return li_rval
end function

public function integer uf_orpo57fr (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_sales_info, string as_roll_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header_in, ls_detail_in, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO57FR'
ls_tran_id = 'O57F'


ls_input_string = as_header_info + '~h7F' + as_detail_info + '~h7F' + as_roll_info

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
//if  li_rval = 0 then
	as_header_info = ''
	as_detail_info = ''
	as_sales_info = ''
	
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_info = Trim(ls_output)
			Case 'D'
				as_detail_info += Trim(ls_output)
			Case 'S'
				as_sales_info += Trim(ls_output)

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval
end function

public function integer uf_orpo74fr_customer_order_print (ref s_error astr_error_info, string as_input);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO74FR'
ls_tran_id = 'O74F'


ls_input_string = as_input


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo02gr_inq_reservatiion_item (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO02GR'
ls_tran_id = 'O02G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo03gr_upd_reservation_item (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO03GR'
ls_tran_id = 'O03G'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo13gr_linked_orders_inq (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO13GR'
ls_tran_id = 'O13G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo14gr_linked_orders_upd (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO14GR'
ls_tran_id = 'O14G'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo11gr_edi_trans_by_cust_inq (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header_in, ls_detail_in, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO11GR'
ls_tran_id = 'O11G'


ls_input_string = as_header_string_in

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
ls_stringsep = Left(ls_outputstring, 1)
as_header_string_out = ''
as_output_string = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_string_out = ls_output
			Case 'D'
				as_output_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval
end function

public function integer uf_orpo12gr_edi_trans_by_cust_upd (ref s_error astr_error_info, string as_header_string, string as_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO12GR'
ls_tran_id = 'O12G'


ls_input_string = as_header_string + '~h7F' + as_detail_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo10gr_product_to_global_upd (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO10GR'
ls_tran_id = 'O10G'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_orpo99gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name,ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'ORPO99GR'
ls_tran_id = 'O99G'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer uf_orpo98gr (string as_order_id, ref datawindowchild ad_header_dw, ref datawindowchild ad_detail_dw, ref datawindowchild ad_pallet_dw, ref datawindowchild ad_tare_dw, ref datawindowchild ad_hides_dw, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, &
	ls_Header,&
	ls_Detail,&
	ls_Pallet,&
	ls_Tare,&	
	ls_Hides,&
	ls_stringsep
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'ORPO98GR'
ls_tran_id = 'O98G'

ls_input_string = as_order_id
li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_outputstring,1)
ls_Header = ''
ls_Detail = ''
ls_Pallet = ''
ls_Tare = ''
ls_Hides = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_Header += ls_output
			Case 'D'
				ls_Detail += ls_output
			Case 'P'
				ls_Pallet += ls_output
			Case 'T'
				ls_Tare += ls_output	
			Case 'X'
				ls_Hides += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop

	ad_header_dw.Reset()
	ad_header_dw.ImportString( ls_Header)
	
	ad_detail_dw.Reset()
	ad_detail_dw.ImportString( ls_Detail)

	ad_pallet_dw.Reset()
	ad_pallet_dw.ImportString( ls_Pallet)
	
	ad_tare_dw.Reset()
	ad_tare_dw.ImportString( ls_Tare)
	
	ad_hides_dw.Reset()
	ad_hides_dw.ImportString( ls_Hides)
end if	

return li_rval
end function

on u_ws_orp3.create
call super::create
end on

on u_ws_orp3.destroy
call super::destroy
end on

