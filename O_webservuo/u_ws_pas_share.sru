HA$PBExportHeader$u_ws_pas_share.sru
forward
global type u_ws_pas_share from u_pas_webservice
end type
end forward

global type u_ws_pas_share from u_pas_webservice
end type
global u_ws_pas_share u_ws_pas_share

forward prototypes
public function boolean nf_pasp00hr (ref s_error astr_error_info, string as_division, ref string as_last_update, ref string as_plant, ref string as_week_invent)
public function boolean nf_pasp43fr (ref s_error astr_error_info, string as_input, ref string as_marketing_data)
public function boolean nf_pasp44fr (ref s_error astr_error_info, string as_division_code, string as_update)
public function boolean nf_pasp45fr (ref s_error astr_error_info, string as_product_code, ref string as_sku_product)
public function boolean nf_pasp49fr (ref s_error astr_error_info, string as_input_string, ref string as_timestamp, ref datawindow adw_freezer_inv)
public function integer nf_pasp58fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp65fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp73fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp76fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp76gr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string, string as_include_exclude_string)
public function integer nf_pasp77fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp77gr (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
public function boolean nf_pasp40fr (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref string as_daily_summary)
public function integer nf_pasp01hr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp04hr (ref s_error astr_error_info, string as_division, ref string as_last_update, ref string as_plant, ref string as_week_invent)
public function boolean nf_pasp39fr (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref datastore adw_sales_inv, ref string output_string)
public function boolean nf_pasp72gr (string as_input_string, s_error astr_error_info)
end prototypes

public function boolean nf_pasp00hr (ref s_error astr_error_info, string as_division, ref string as_last_update, ref string as_plant, ref string as_week_invent);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'PASP00HR'
ls_tran_id = 'P00H'


ls_input_string = as_division +'~h7F' + as_last_update+'~h7F' + as_plant

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'L'
				as_last_update += ls_output
			Case 'P'
				as_plant += ls_output
				
			Case 'E'
				as_week_invent += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_last_update = TRIM(as_last_update)








if li_rval = 0 then
	return true
else 
	return false
end if

end function

public function boolean nf_pasp43fr (ref s_error astr_error_info, string as_input, ref string as_marketing_data);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP43FR'
ls_tran_id = 'P43F'


ls_input_string = as_input

li_rval = uf_rpccall(ls_input_string,as_marketing_data, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if


end function

public function boolean nf_pasp44fr (ref s_error astr_error_info, string as_division_code, string as_update);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'PASP44FR'
ls_tran_id = 'P44F'


ls_input_string = as_division_code +'~h7F' + as_update

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
	
else
	
	return false
	
end if

end function

public function boolean nf_pasp45fr (ref s_error astr_error_info, string as_product_code, ref string as_sku_product);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP45FR'
ls_tran_id = 'P45F'


ls_input_string = as_product_code

li_rval = uf_rpccall(ls_input_string,as_sku_product, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function boolean nf_pasp49fr (ref s_error astr_error_info, string as_input_string, ref string as_timestamp, ref datawindow adw_freezer_inv);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_freezer_inv
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'PASP49FR'
ls_tran_id = 'P49F'


ls_input_string = as_input_string +'~h7F' + as_timestamp
li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'L'
				as_timestamp += ls_output
			Case 'F'
				ls_freezer_inv += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
		adw_freezer_inv.ImportString(ls_freezer_inv)
	
end if	

if li_rval = 0 then

   return true
else
	return false
	
end if
end function

public function integer nf_pasp58fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP58FR'
ls_tran_id = 'P58F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp65fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP65FR'
ls_tran_id = 'P65F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp73fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP73FR'
ls_tran_id = 'P73F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp76fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP76FR'
ls_tran_id = 'P76F'


ls_input_string =as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp76gr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string, string as_include_exclude_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header
integer li_rval, li_version
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'PASP76GR'
ls_tran_id = 'P76G'


ls_input_string = as_header_string

li_version = 1


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id,li_version)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	//as_header_string = ''
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'D'
				as_detail_string += ls_output
			Case 'I'
				as_include_exclude_string += ls_output
			case 'H'
					ls_header += ls_output
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
as_header_string = ls_header
return li_rval

end function

public function integer nf_pasp77fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval, li_version_number


u_String_Functions  lu_string

ls_program_name= 'PASP77FR'
ls_tran_id = 'P77F'

li_version_number = 1

ls_input_string =as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id, li_version_number)

return li_rval

end function

public function integer nf_pasp77gr (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP77GR'
ls_tran_id = 'P77G'


ls_input_string = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpccall(ls_input_string,as_detail_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp40fr (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref string as_daily_summary);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_sales_inv
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'PASP40FR'
ls_tran_id = 'P40F'


ls_input_string = as_input +'~h7F' + as_timestamp
li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'L'
				as_timestamp += ls_output
			Case 'D'
				as_daily_summary += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
		
end if	

if li_rval = 0 then

   return true
else
	return false
	
end if
end function

public function integer nf_pasp01hr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP01HR'
ls_tran_id = 'P01H'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp04hr (ref s_error astr_error_info, string as_division, ref string as_last_update, ref string as_plant, ref string as_week_invent);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval
string ls_stringsep

u_String_Functions  lu_string


ls_program_name= 'PASP04HR'
ls_tran_id = 'P04H'


ls_input_string = as_division +'~h7F' + as_last_update+'~h7F' + as_plant

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'L'
				as_last_update += ls_output
			Case 'P'
				as_plant += ls_output
				
			Case 'E'
				as_week_invent += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_last_update = TRIM(as_last_update)








if li_rval = 0 then
	return true
else 
	return false
end if

end function

public function boolean nf_pasp39fr (ref s_error astr_error_info, string as_input, ref string as_timestamp, ref datastore adw_sales_inv, ref string output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_sales_inv
integer li_rval
string ls_stringsep

u_String_Functions  lu_string

ls_program_name= 'PASP39FR'
ls_tran_id = 'P39F'


ls_input_string = as_input +'~h7F' + as_timestamp
li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'L'
				as_timestamp += ls_output
			Case 'D'
				ls_sales_inv += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
		adw_sales_inv.ImportString(ls_sales_inv)
	output_string = ls_sales_inv
end if	

if li_rval = 0 then

   return true
else
	return false
	
end if
end function

public function boolean nf_pasp72gr (string as_input_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP72GR'
ls_tran_id ='P72G'

li_rval = uf_rpcupd(as_input_string,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end If

end function

on u_ws_pas_share.create
call super::create
end on

on u_ws_pas_share.destroy
call super::destroy
end on

