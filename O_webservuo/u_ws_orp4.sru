HA$PBExportHeader$u_ws_orp4.sru
forward
global type u_ws_orp4 from u_orp_webservice
end type
end forward

global type u_ws_orp4 from u_orp_webservice
end type
global u_ws_orp4 u_ws_orp4

forward prototypes
public function integer nf_orpo99fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo98fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo94fr (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_orpo90fr (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_orpo91fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo92fr (ref s_error astr_error_info, string as_input_string, string as_group_input, string as_tsr_input, string as_from_date, string as_to_date)
public function integer nf_orpo93fr (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo87fr (ref s_error astr_error_info, character ac_process_option, string as_order_id, string as_customer, string ac_customer_type, string as_input, ref string as_output)
public function integer nf_orpo85fr (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_dates_info)
public function integer nf_orpo89fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo80fr (ref s_error astr_error_info, string ac_process_option_ind, string as_input_info_in, string as_customer_email, ref string as_input_info_out, string ac_process_tla_option_ind)
public function integer nf_orpo80fr (ref s_error astr_error_info, string ac_process_option_ind, string as_input_info_in, ref string as_input_info_out, string as_customer_email)
public function integer nf_orpo88fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo84fr (s_error astr_error_info, string as_header_info, string as_product_info, ref string as_detail_info, string ac_indicator)
public function integer nf_orpo86fr (s_error astr_error_info, string as_input_string)
public function integer nf_orpo77fr (s_error astr_error_info, string as_header_string, ref string as_detail_string, ref string as_default_string)
public function integer nf_orpo83fr (ref s_error astr_error_info, string as_header_info, ref string as_detail_info)
public function integer nf_orpo78fr (string as_primary_order, ref datawindowchild ad_stops_dw, ref datawindowchild ad_header_dw, ref datawindowchild ad_detail_dw, ref datawindowchild ad_instruction_dw, ref s_error astr_error_info)
public function integer nf_orpo79fr (ref s_error astr_error_info, string as_customer_info, string as_main_info_in1, string as_main_info_in2, string as_main_info_in3, string as_additional_data, ref string as_main_info_out1, ref string as_main_info_out2, ref string as_main_info_out3, ref string as_so_info, ref string multi_return_code)
public function integer nf_orpo82fr (ref s_error astr_error_info, string as_process_option, string as_order_id, string as_detail_data_in, ref string as_detail_data_out, ref character ac_send_to_sched_ind)
public function integer nf_orpo08gr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_output_string)
public function integer nf_orpo07gr (ref s_error astr_error_info, string as_input_string)
public function integer nf_orpo06gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo05gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo04gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_orpo09gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_detail_string)
public function integer nf_orpo97fr (ref s_error astr_error_info, string as_group_type, ref string as_output)
public function integer nf_orpo81fr (ref s_error astr_error_info, string ac_process_option, string ac_availability_date, string as_page_descr_in, ref string as_file_descr_out, ref string as_plant_data, ref string as_detail_data, string as_inq_userid)
end prototypes

public function integer nf_orpo99fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO99FR'
ls_tran_id = 'O99F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo98fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO98FR'
ls_tran_id = 'O98F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo94fr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO94FR'
ls_tran_id = 'O94F'


ls_input_string = ''

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo90fr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO90FR'
ls_tran_id = 'O90F'


ls_input_string = " "

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo91fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO91FR'
ls_tran_id = 'O91F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo92fr (ref s_error astr_error_info, string as_input_string, string as_group_input, string as_tsr_input, string as_from_date, string as_to_date);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO92FR'
ls_tran_id = 'O92F'


ls_input_string = as_input_string +'~t' +  as_group_input +'~t' + as_tsr_input+'~t' + as_from_date+'~t' + as_to_date


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo93fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO93FR'
ls_tran_id = 'O93F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo87fr (ref s_error astr_error_info, character ac_process_option, string as_order_id, string as_customer, string ac_customer_type, string as_input, ref string as_output);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO87FR'
ls_tran_id = 'O87F'


ls_input_string = ac_process_option +'~h7F' + as_order_id+'~h7F' + as_customer +'~h7F' + ac_customer_type +'~h7F' + as_input 

li_rval = uf_rpccall(ls_input_string,as_output, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo85fr (ref s_error astr_error_info, ref string as_header_info, ref string as_detail_info, ref string as_dates_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_stringsep
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'ORPO85FR'
ls_tran_id = 'O85F'

as_Header_Info = Trim(as_Header_Info)
ls_input_string = as_header_info + '~h7F'+as_detail_info
li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_header_info = ''
as_detail_info = ''
as_dates_info = ''
ls_stringsep = Left(ls_outputstring,1)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_info += ls_output
			Case 'D'
				as_detail_info += ls_output
			Case 'I'
				as_dates_info += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

return li_rval

end function

public function integer nf_orpo89fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO89FR'
ls_tran_id = 'O89F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo80fr (ref s_error astr_error_info, string ac_process_option_ind, string as_input_info_in, string as_customer_email, ref string as_input_info_out, string ac_process_tla_option_ind);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'ORPO80FR'
ls_tran_id = 'O80F'

ls_input_string = ac_process_option_ind + '>'+as_input_info_in+ '>'+ as_customer_email + '>'+ ac_process_tla_option_ind
li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo80fr (ref s_error astr_error_info, string ac_process_option_ind, string as_input_info_in, ref string as_input_info_out, string as_customer_email);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'ORPO80FR'
ls_tran_id = 'O80F'

ls_input_string = ac_process_option_ind + '~h7F'+as_input_info_in+ '~h7F'+ as_customer_email
li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo88fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO88FR'
ls_tran_id = 'O88F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo84fr (s_error astr_error_info, string as_header_info, string as_product_info, ref string as_detail_info, string ac_indicator);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'ORPO84FR'
ls_tran_id = 'O84F'

as_header_info = Trim(as_header_info)
ls_input_string =ac_indicator+ '~h7F'+ as_header_info + '~h7F'+as_product_info+ '~h7F'+as_detail_info

li_rval = uf_rpccall(ls_input_string, as_detail_info, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo86fr (s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO86FR'
ls_tran_id = 'O86F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo77fr (s_error astr_error_info, string as_header_string, ref string as_detail_string, ref string as_default_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringInd, ls_output, ls_stringsep
integer li_rval

u_String_Functions  lu_string
 

ls_program_name= 'ORPO77FR'
ls_tran_id = 'O77F'


ls_input_string = as_header_string 
li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_detail_string = ''
as_default_string = ''
ls_stringsep = Left(ls_outputstring,1)

if  li_rval >= 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	ls_stringsep = Left(ls_outputstring,1)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'D'
				as_detail_string += ls_output
			Case 'F'
				as_default_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

RETURN li_rval
end function

public function integer nf_orpo83fr (ref s_error astr_error_info, string as_header_info, ref string as_detail_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO83FR'
ls_tran_id = 'O83F'


ls_input_string = as_header_info 

li_rval = uf_rpccall(ls_input_string,as_detail_info, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo78fr (string as_primary_order, ref datawindowchild ad_stops_dw, ref datawindowchild ad_header_dw, ref datawindowchild ad_detail_dw, ref datawindowchild ad_instruction_dw, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, &
	ls_Stops,&
	ls_Header,&
	ls_Detail,&
	ls_Instruction, ls_stringsep
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'ORPO78FR'
ls_tran_id = 'O78F'

ls_input_string = as_primary_order
li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_outputstring,1)
ls_Header = ''
ls_Detail = ''
ls_Instruction = ''
ls_Stops = ''

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'H'
				ls_Header += ls_output
			Case 'D'
				ls_Detail += ls_output
			Case 'I'
				ls_Instruction += ls_output
			Case 'S'
				ls_Stops += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop

	ad_stops_dw.Reset()
	ad_stops_dw.ImportString( ls_Stops)
	
	ad_header_dw.Reset()
	ad_header_dw.ImportString( ls_Header)
	
	ad_Detail_dw.Reset()
	ad_Detail_dw.ImportString( ls_Detail)

	ad_Instruction_dw.Reset()
	ad_Instruction_dw.ImportString( ls_instruction)
end if	

return li_rval
end function

public function integer nf_orpo79fr (ref s_error astr_error_info, string as_customer_info, string as_main_info_in1, string as_main_info_in2, string as_main_info_in3, string as_additional_data, ref string as_main_info_out1, ref string as_main_info_out2, ref string as_main_info_out3, ref string as_so_info, ref string multi_return_code);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind,ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO79FR'
ls_tran_id = 'O79F'


ls_input_string = as_customer_info + '~h7F' + as_main_info_in1 +  + '~h7F' + as_main_info_in2 + '~h7F' + as_main_info_in3 + '~h7F' + as_additional_data

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_main_info_out1 = '' 
as_main_info_out2 = ''
as_main_info_out3 = ''
as_so_info = '' 
multi_return_code = ''
ls_stringsep = Left(ls_outputstring,1)
//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'A'
				as_main_info_out1 += ls_output
			Case 'B'
				as_main_info_out2 += ls_output
			Case 'C'
				as_main_info_out3 += ls_output
			Case 'D'
				as_so_info += ls_output
			Case 'E'
				multi_return_code += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval
end function

public function integer nf_orpo82fr (ref s_error astr_error_info, string as_process_option, string as_order_id, string as_detail_data_in, ref string as_detail_data_out, ref character ac_send_to_sched_ind);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringInd, ls_output,ls_stringsep
integer li_rval

u_String_Functions  lu_string
 

ls_program_name= 'ORPO82FR'
ls_tran_id = 'O82F'


ls_input_string = as_process_option + '~h7F' + as_order_id + '~h7F' + as_detail_data_in
li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_detail_data_out = ''
ac_send_to_sched_ind = ''
ls_stringsep =Left(ls_outputstring,1)

//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'D'
				as_detail_data_out += ls_output
			Case 'I'
				ac_send_to_sched_ind = ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	

RETURN li_rval
end function

public function integer nf_orpo08gr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO08GR'
ls_tran_id = 'O08G'


ls_input_string = as_header_string + '~h7F'  + as_input_string


li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo07gr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO07GR'
ls_tran_id = 'O07G'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo06gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);
string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO06GR'
ls_tran_id = 'O06G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo05gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO05GR'
ls_tran_id = 'O05G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo04gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO04GR'
ls_tran_id = 'O04G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_orpo09gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header_in, ls_detail_in, ls_stringsep
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO09GR'
ls_tran_id = 'O09G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_output_string = ''
as_detail_string = ''

//if  li_rval = 0 then
	ls_stringsep = Left(ls_outputstring,1)
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
	
		Choose Case ls_StringInd
			Case 'P'
				as_output_string = ls_output
			Case 'D'
				as_detail_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval
end function

public function integer nf_orpo97fr (ref s_error astr_error_info, string as_group_type, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO97FR'
ls_tran_id = 'O97F'


ls_input_string =as_group_type

li_rval = uf_rpccall(ls_input_string,as_output, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_orpo81fr (ref s_error astr_error_info, string ac_process_option, string ac_availability_date, string as_page_descr_in, ref string as_file_descr_out, ref string as_plant_data, ref string as_detail_data, string as_inq_userid);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_app_name
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'ORPO81FR'
ls_tran_id = 'O81F'
ls_app_name  = Message.nf_Get_App_ID()

ls_input_string =ls_app_name + '~h7F' +  ac_process_option + '~h7F' + ac_availability_date + '~h7F' +  as_page_descr_in + '~h7F' +as_plant_data + '~h7F' + as_detail_data + '~h7F' + as_inq_userid

li_rval = uf_rpccall(ls_input_string,as_file_descr_out, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

on u_ws_orp4.create
call super::create
end on

on u_ws_orp4.destroy
call super::destroy
end on

