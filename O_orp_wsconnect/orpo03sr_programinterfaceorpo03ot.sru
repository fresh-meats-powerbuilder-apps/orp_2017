HA$PBExportHeader$orpo03sr_programinterfaceorpo03ot.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03ot from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03ot from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on orpo03sr_ProgramInterfaceOrpo03ot.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03ot.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

