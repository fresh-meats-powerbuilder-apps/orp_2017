HA$PBExportHeader$orpo03sr_programinterfaceorpo03pgorp000sr_program_container.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03pgOrp000sr_program_container from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03pgOrp000sr_program_container from nonvisualobject
end type

type variables
    int orp000sr_rval
    string orp000sr_message
    ulong orp000sr_task_num
    ulong orp000sr_max_record_num
    ulong orp000sr_last_record_num
    uint orp000sr_version_number
end variables

on orpo03sr_ProgramInterfaceOrpo03pgOrp000sr_program_container.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03pgOrp000sr_program_container.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

