HA$PBExportHeader$orpo03sr_programinterfaceorpo03pg1.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03pg1 from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03pg1 from nonvisualobject
end type

type variables
    orpo03sr_ProgramInterfaceOrpo03pgOrp000sr_program_container1 orp000sr_program_container
    boolean structuredContainer
end variables

on orpo03sr_ProgramInterfaceOrpo03pg1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03pg1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

