HA$PBExportHeader$orpo03sr_channeltype.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_channelType from nonvisualobject
    end type
end forward

global type orpo03sr_channelType from nonvisualobject
end type

type variables
    boolean channel
end variables

on orpo03sr_channelType.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_channelType.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

