HA$PBExportHeader$orpo03sr_charcontainertype.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_charContainerType from nonvisualobject
    end type
end forward

global type orpo03sr_charContainerType from nonvisualobject
end type

type variables
    string Value
end variables

on orpo03sr_charContainerType.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_charContainerType.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

