HA$PBExportHeader$orpo03sr_programinterfaceorpo03in.sru
$PBExportComments$Proxy imported from Web service using Web Service Proxy Generator.
forward
    global type orpo03sr_ProgramInterfaceOrpo03in from nonvisualobject
    end type
end forward

global type orpo03sr_ProgramInterfaceOrpo03in from nonvisualobject
end type

type variables
    boolean charContainer
    string Value
end variables

on orpo03sr_ProgramInterfaceOrpo03in.create
call super::create
TriggerEvent( this, "constructor" )
end on

on orpo03sr_ProgramInterfaceOrpo03in.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

