HA$PBExportHeader$w_214_exception_filter.srw
forward
global type w_214_exception_filter from w_base_response_ext
end type
type dw_carrier from u_base_dw_ext within w_214_exception_filter
end type
type gb_2 from groupbox within w_214_exception_filter
end type
type gb_1 from groupbox within w_214_exception_filter
end type
type dw_from_to_dates from u_base_dw_ext within w_214_exception_filter
end type
type dw_status from u_base_dw_ext within w_214_exception_filter
end type
type dw_exception from u_base_dw_ext within w_214_exception_filter
end type
end forward

global type w_214_exception_filter from w_base_response_ext
int Width=1719
int Height=1160
boolean TitleBar=true
string Title="Shipment Status Exception Filter"
long BackColor=79741120
dw_carrier dw_carrier
gb_2 gb_2
gb_1 gb_1
dw_from_to_dates dw_from_to_dates
dw_status dw_status
dw_exception dw_exception
end type
global w_214_exception_filter w_214_exception_filter

type variables
integer		ii_rc

DataWindowChild	idwc_carrier, &
		idwc_exception

u_214_exception	iu_214_exception
end variables

on w_214_exception_filter.create
int iCurrent
call super::create
this.dw_carrier=create dw_carrier
this.gb_2=create gb_2
this.gb_1=create gb_1
this.dw_from_to_dates=create dw_from_to_dates
this.dw_status=create dw_status
this.dw_exception=create dw_exception
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carrier
this.Control[iCurrent+2]=this.gb_2
this.Control[iCurrent+3]=this.gb_1
this.Control[iCurrent+4]=this.dw_from_to_dates
this.Control[iCurrent+5]=this.dw_status
this.Control[iCurrent+6]=this.dw_exception
end on

on w_214_exception_filter.destroy
call super::destroy
destroy(this.dw_carrier)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.dw_from_to_dates)
destroy(this.dw_status)
destroy(this.dw_exception)
end on

event open;call super::open;// load any info into the correct fields
date		ld_from_date, &
			ld_to_date, &
			current

string	ls_from_carrier, &
			ls_to_carrier, &
			ls_load_key, &
			ls_status_code 
						
int		li_rtn,&
			li_pos, &
			li_string_len

long		ll_current_row

iu_214_exception = Message.PowerObjectParm

current = today()

IF iu_214_exception.to_carrier <> '' THEN 
    If iu_214_exception.to_carrier = iu_214_exception.from_carrier  THEN
        dw_carrier.object.end_carrier_code[1] = "    "
    End if  
END IF

dw_carrier.SetFocus()
dw_carrier.SetColumn("start_carrier_code")
dw_carrier.SelectText(1, 4)

 
IF iu_214_exception.from_date <> '' THEN
	ld_from_date = Date(iu_214_exception.from_date)
	dw_from_to_dates.SetItem( 1, "from_date", ld_from_date)
	IF iu_214_exception.to_date <> '' THEN 
		ld_to_date = Date(iu_214_exception.to_date)
		dw_from_to_dates.SetItem( 1, "to_date", ld_to_date)
	Else
		dw_from_to_dates.SetItem( 1, "to_date", ld_from_date)
	END IF
Else
	dw_from_to_dates.SetItem( 1, "from_date", RelativeDate(Today(), -1))
	IF iu_214_exception.to_date <> '' THEN 
		ld_to_date = Date(iu_214_exception.to_date)
		dw_from_to_dates.SetItem( 1, "to_date", ld_to_date)
		dw_from_to_dates.SetItem( 1, "from_date", ld_to_date)
	Else
		dw_from_to_dates.SetItem( 1, "to_date", current)
	END IF
END IF
 

dw_carrier.SetItem( 1, "end_carrier_code", iu_214_exception.to_carrier )
dw_carrier.SetItem( 1, "start_carrier_code", iu_214_exception.from_carrier )
dw_exception.SetItem( 1, "exception_code", iu_214_exception.exception_code )
dw_status.SetItem( 1, "status_code", iu_214_exception.status_code )

dw_carrier.SetFocus()

dw_carrier.SelectText(1, 4)

end event

event ue_base_cancel;iu_214_exception.cancel = true
CloseWithReturn( this, iu_214_exception )

end event

event ue_base_ok;long		ll_foundrow

string	ls_exception_code, &
			ls_from_carrier, &
			ls_to_carrier, &
			ls_status_code, &
			ls_from_date, &
			ls_to_date, &
			ls_from_carr, &
			ls_to_carr
				
If dw_carrier.AcceptText() = 1 then
	ls_from_carrier	= " "
   ls_to_carrier  	= " "
     
   ls_from_carrier	= trim( dw_carrier.object.start_carrier_code[1])
   ls_to_carrier   	= trim( dw_carrier.object.end_carrier_code[1])
	
	If Trim(ls_from_carrier) = "" then
		ls_from_carrier = " "
	End if
   If Trim(ls_to_carrier) = "" Then
      ls_to_carrier = ls_from_carrier
   End if
Else
	ls_from_carrier = " "
	ls_to_carrier   = " "
End if
	

	
If dw_from_to_dates.AcceptText() = 1 Then
   ls_from_date  = string( dw_from_to_dates.GetItemDate( 1, "from_date" ), "yyyy-mm-dd" )
	ls_to_date    = string( dw_from_to_dates.GetItemDate( 1, "to_date" ), "yyyy-mm-dd" )
End If

If dw_status.Accepttext() = 1 Then
	ls_status_code         = dw_status.GetItemString( 1, "status_code" )
End If

If dw_exception.Accepttext() = 1 Then
	ls_exception_code         = dw_exception.GetItemString( 1, "exception_code" )
End If

		
iu_214_exception.exception_code    = ls_exception_code
iu_214_exception.from_carrier	     = ls_from_carrier
iu_214_exception.to_carrier	     = ls_to_carrier
iu_214_exception.status_code       = ls_status_code 
iu_214_exception.from_date         = ls_from_date
iu_214_exception.to_date           = ls_to_date
iu_214_exception.cancel		        = false

Message.PowerObjectParm = iu_214_exception

CloseWithReturn( This, iu_214_exception )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_214_exception_filter
int X=1042
int Y=936
int TabOrder=70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_214_exception_filter
int X=718
int Y=936
int TabOrder=60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_214_exception_filter
int X=393
int Y=936
int TabOrder=50
end type

type dw_carrier from u_base_dw_ext within w_214_exception_filter
int X=398
int Y=80
int Width=933
int Height=296
int TabOrder=10
boolean BringToTop=true
string DataObject="d_dddw_start_end_carriers"
end type

event constructor;call super::constructor;InsertRow(0)

ii_rc = GetChild( 'start_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTransObject(SQLCA)
idwc_carrier.Retrieve()

ii_rc = GetChild( 'end_carrier_code', idwc_carrier )
IF ii_rc = -1 THEN MessageBox("DataWindowChild Error", "Unable to get Child Handle for carrier_code.")

idwc_carrier.SetTransObject(SQLCA)
idwc_carrier.Retrieve()


dw_carrier.SetFocus()
SelectText(1,4)
end event

event itemchanged;call super::itemchanged;//long ll_FoundRow
//
//IF Trim(data) = "" THEN Return
//
//IF dwo.name = "start_carrier_code" Then
//   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
//   IF ll_FoundRow < 1 THEN
//	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
////	  dw_carrier.SetItem(1,"start_carrier_code", "")
//	  dw_carrier.SetFocus()  
//     dw_carrier.SetColumn("start_carrier_code")
//     Return 1
//   END IF		
//End if
//
//IF dwo.name = "end_carrier_code" Then
//   ll_FoundRow = idwc_carrier.Find ( "carrier_code='"+data+"'", 1, idwc_carrier.RowCount() )
//   IF ll_FoundRow < 1 THEN
//  	  MessageBox( "Carrier Error", data + " is Not a Valid Carrier Code." )
////     dw_carrier.SetItem(1,"end_carrier_code", "")
//     dw_carrier.SetFocus()  
//     dw_carrier.SetColumn("end_carrier_code")
//	  Return 1
//   END IF		
//End if
//
end event

event itemerror;call super::itemerror;Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;integer	li_len
string	ls_temp

ls_temp = this.GetText()

li_len = len(ls_temp)

this.SelectText(1, li_len)
end event

event getfocus;SelectText(1,6)
end event

type gb_2 from groupbox within w_214_exception_filter
int X=91
int Y=456
int Width=919
int Height=440
string Text="Date Range"
long TextColor=33554432
long BackColor=67108864
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_214_exception_filter
int X=325
int Y=4
int Width=1074
int Height=412
string Text="Carrier"
long TextColor=33554432
long BackColor=67108864
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_from_to_dates from u_base_dw_ext within w_214_exception_filter
int X=119
int Y=552
int Width=823
int Height=312
int TabOrder=20
boolean BringToTop=true
string DataObject="d_from_and_to_dates"
end type

event constructor;call super::constructor;InsertRow(0)

//date current
//current = today()
//

 
dw_from_to_dates.SetItem(1, "from_date", RelativeDate(Today(), -1))
dw_from_to_dates.SetItem(1, "to_date", RelativeDate(Today(), 0))
dw_from_to_dates.SetFocus()
SelectText(1,10)
end event

event itemchanged;call super::itemchanged;integer   li_len
string    ls_temp

ls_temp = this.gettext()

li_len  = len(ls_temp)

this.selecttext(1, li_len)

Date		ld_from, &
			ld_to


If dwo.name = "to_date" Then
	ld_from = dw_from_to_dates.GetItemDate(1,"from_date")
	ld_to = Date(data)
	If ld_from > ld_to Then
		MessageBox("Date error", "To Date can not be less than the From Date, please reenter dates")
		SelectText(1,10)
		Return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;SelectText(1,10)
end event

type dw_status from u_base_dw_ext within w_214_exception_filter
int X=1088
int Y=484
int Width=521
int Height=172
int TabOrder=30
boolean BringToTop=true
string DataObject="d_status_code"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event constructor;call super::constructor;insertRow(0)
end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;THIS.SelectText (1,99)
end event

type dw_exception from u_base_dw_ext within w_214_exception_filter
int X=1097
int Y=704
int Width=494
int Height=176
int TabOrder=40
boolean BringToTop=true
string DataObject="d_exception_code"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event constructor;call super::constructor;insertRow(0)
ii_rc = GetChild( 'exception_code', idwc_exception )
IF ii_rc = -1 THEN MessageBox("DataWindwoChild Error", "Unable to get Child Handle for type code/record type of EDI214EX.")

idwc_exception.SetTransObject(SQLCA)
idwc_exception.Retrieve('EDI214EX')
end event

event itemerror;call super::itemerror;Return 1
end event

event itemchanged;call super::itemchanged;long ll_FoundRow
   
If Trim(data) = "" Then Return

ll_FoundRow = idwc_exception.Find ( "exception_code='"+data+"'", 1, idwc_exception.RowCount() )
IF ll_FoundRow < 1 THEN Return 1

end event

event getfocus;THIS.SelectText (1,99)
end event

