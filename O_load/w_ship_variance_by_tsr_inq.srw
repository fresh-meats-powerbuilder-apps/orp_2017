HA$PBExportHeader$w_ship_variance_by_tsr_inq.srw
forward
global type w_ship_variance_by_tsr_inq from w_base_response_ext
end type
type dw_variance_inq from u_base_dw_ext within w_ship_variance_by_tsr_inq
end type
end forward

global type w_ship_variance_by_tsr_inq from w_base_response_ext
integer width = 1819
integer height = 632
string title = "Ship Variance by TSR Inquire"
long backcolor = 134217728
dw_variance_inq dw_variance_inq
end type
global w_ship_variance_by_tsr_inq w_ship_variance_by_tsr_inq

type variables

string is_openstring
integer rval_error = 0 

end variables

on w_ship_variance_by_tsr_inq.create
int iCurrent
call super::create
this.dw_variance_inq=create dw_variance_inq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_variance_inq
end on

on w_ship_variance_by_tsr_inq.destroy
call super::destroy
destroy(this.dw_variance_inq)
end on

event open;call super::open;is_openstring = Message.StringParm

end event

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_temp_storage

String 		ls_location,ls_sman
Date ls_ship_date
								
u_string_functions		lu_string_functions	


IF lu_string_functions.nf_IsEmpty(is_openstring) THEN
	dw_variance_inq.Reset()
	dw_variance_inq.InsertRow(1)
	//obtener datos
	ls_ship_date = Today()
	//message.triggerEvent("ue_getlocation")
	//ls_location = message.stringparm
//	message.triggerEvent("ue_getsmancode")
//	ls_sman = message.stringparm

	//dw_variance_inq.SetItem(1,'location',ls_location)
	dw_variance_inq.SetItem(1,'sched_ship_date',ls_ship_date)
Else
	dw_variance_inq.InsertRow(1)
	dw_variance_inq.Reset()
	dw_variance_inq.ImportString(is_openstring)
END IF

This.SetRedraw(True)





end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,'CANCEL')
end event

event ue_base_ok;call super::ue_base_ok;u_string_functions		lu_string_functions
String ls_location, ls_userid,ls_tsr, ls_string_param, ls_db_location,ls_location_user
date ls_ship_Date

dw_variance_inq.AcceptText()

ls_ship_date =dw_variance_inq.GetItemDate(1,'sched_ship_date')
ls_location = dw_variance_inq.GetItemString(1,'location')
ls_tsr = dw_variance_inq.GetItemString(1,'tsr')
ls_userid = trim(UPPER(Message.nf_getuserid())) 

IF lu_string_functions.nf_IsEmpty(ls_location) THEN
	iw_frame.SetMicroHelp("Locations is a required field")
	Return
End If

//Validate that the location match the sales location for the TSR
//IF dw_variance_inq.GetItemString(1,'tsr_all') = 'N' THEN
	IF lu_string_functions.nf_IsEmpty(ls_tsr) THEN
		iw_frame.SetMicroHelp("Please select a TSR")
		Return
	ELSE
		//select to retrieve location from tsr	
//		message.triggerEvent("ue_getlocation")
//		ls_location_user = message.stringparm\
		SELECT salesman.smanloc
		   INTO  :ls_location_user
		   FROM salesman
	 	WHERE  salesman.userid = :ls_userid;
				
		IF ls_location <> ls_location_user THEN
			MessageBox("Location Error","Assigned Sales Location must match the Sales Location for the TSR")
			Return
		END IF
	END IF
//ELSE
//	ls_tsr = '   '
//END IF

If rval_error = 0 Then
	ls_string_param = String(ls_ship_date) +  '~t'  + ls_location +  '~t'  + ls_tsr + '~t' 
	CloseWithReturn(This,ls_string_param )
END IF




	
	
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable("m_save")
iw_frame.im_menu.mf_disable("m_delete")
iw_frame.im_menu.mf_disable("m_deleterow")
iw_frame.im_Menu.mf_disable("m_copyrows")
iw_Frame.im_Menu.mf_disable("m_complete")
iw_Frame.im_Menu.mf_disable("m_generatesales")
iw_Frame.im_Menu.mf_disable("m_print")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_ship_variance_by_tsr_inq
boolean visible = false
integer x = 1403
integer y = 292
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_ship_variance_by_tsr_inq
integer x = 1403
integer y = 168
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_ship_variance_by_tsr_inq
integer x = 1403
integer y = 48
end type

type cb_browse from w_base_response_ext`cb_browse within w_ship_variance_by_tsr_inq
end type

type dw_variance_inq from u_base_dw_ext within w_ship_variance_by_tsr_inq
integer x = 64
integer y = 40
integer width = 1298
integer height = 460
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_ship_variance_by_tsr_inq"
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_temp_storage
 
u_project_functions	lu_project_functions

dw_variance_inq.GetChild("tsr",ldwc_temp_storage)
lu_project_functions.nf_gettsrs(ldwc_temp_storage,'')


end event

event itemchanged;call super::itemchanged;
string ls_db_tsr_loc
CHOOSE CASE This.GetColumnName()
	CASE 'sched_ship_date'
		IF Date(data) > Today() Then
			iw_frame.SetMicroHelp("Ship Date must be less than or equal to current date")
			rval_error=1
			Return
		ELSE 
			rval_error = 0
		END IF
	CASE  'tsr'
		SELECT SALESMAN.SMANLOC
		   INTO  :ls_db_tsr_loc
		 FROM   SALESMAN 
		WHERE SALESMAN.SMANCODE = :data
		     and SALESMAN.SMANTYPE = 'S';
		This.SetItem(row,'location',ls_db_tsr_loc)
	CASE 'tsr_all'
		message.triggerEvent("ue_getlocation")
		ls_db_tsr_loc = message.stringparm
		This.SetItem(row,'location',ls_db_tsr_loc)
END CHOOSE
end event

