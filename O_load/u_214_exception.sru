HA$PBExportHeader$u_214_exception.sru
forward
global type u_214_exception from nonvisualobject
end type
end forward

global type u_214_exception from nonvisualobject autoinstantiate
end type

type variables
string	exception_code, &
	from_carrier, &
	to_carrier, &
	status_code, &
	from_date, &
	to_date 

boolean	cancel
end variables

on u_214_exception.create
TriggerEvent( this, "constructor" )
end on

on u_214_exception.destroy
TriggerEvent( this, "destructor" )
end on

