HA$PBExportHeader$w_report_list_new_inq.srw
forward
global type w_report_list_new_inq from w_netwise_response
end type
type dw_report_list from u_netwise_dw within w_report_list_new_inq
end type
type cb_view from commandbutton within w_report_list_new_inq
end type
type cb_print from commandbutton within w_report_list_new_inq
end type
type cb_delete from commandbutton within w_report_list_new_inq
end type
end forward

global type w_report_list_new_inq from w_netwise_response
integer width = 2437
string title = "Report List Inquire"
event ue_print ( )
dw_report_list dw_report_list
cb_view cb_view
cb_print cb_print
cb_delete cb_delete
end type
global w_report_list_new_inq w_report_list_new_inq

type variables
u_ws_utl		iu_ws_utl

s_error		istr_error_info

string			is_userid

w_report_list_new		iw_parentwindow
end variables

forward prototypes
public function integer wf_retrieve ()
end prototypes

event ue_print();string		ls_report_id, &
			ls_report_date, &
			ls_report_time, &
			ls_report_pages, &
			ls_report_list, &
			ls_report_width
			
long		ll_rowcount, &
			ll_sub

ll_rowcount = dw_report_list.RowCount()

for ll_sub = 1 to ll_rowcount
	
	if dw_report_list.IsSelected(ll_sub) then
//		ls_option = 'Print'
		ls_report_id	= dw_report_list.getitemstring(ll_sub,'report_id')
		ls_report_date	= dw_report_list.getitemstring(ll_sub,'report_date')
		ls_report_time	= dw_report_list.getitemstring(ll_sub,'report_time')
		ls_report_pages	= dw_report_list.getitemstring(ll_sub,'report_pages')
		ls_report_list	= dw_report_list.getitemstring(ll_sub,'report_list')
		ls_report_width	 = dw_report_list.getitemstring(ll_sub,'report_width')
		
		//11/29/2016
		ls_report_date = mid(ls_report_date,7,4) + mid(ls_report_date,1,2) + mid(ls_report_date,4,2)
		
		//14:51:08
		ls_report_time = mid(ls_report_time,1,2) + mid(ls_report_time,4,2) + mid(ls_report_time,7,2)
		
//		iw_parentwindow.Event ue_set_data('option', ls_option)
		iw_parentwindow.Event ue_set_data('report_id', ls_report_id)
		iw_parentwindow.Event ue_set_data('report_date', ls_report_date)
		iw_parentwindow.Event ue_set_data('report_time', ls_report_time)
		iw_parentwindow.Event ue_set_data('report_pages', ls_report_pages)
		iw_parentwindow.Event ue_set_data('report_list', ls_report_list)
		iw_parentwindow.Event ue_set_data('report_width', ls_report_width)
		
		iw_parentwindow.wf_print_new()
		
	end if
	
next


end event

public function integer wf_retrieve ();integer 	li_rtn
string		ls_output, ls_input

u_string_functions		lu_string_functions

ls_input = is_userid

dw_report_list.reset()

li_rtn = iu_ws_utl.nf_utlu29er(istr_error_info, "L", ls_input, ls_output)

If li_rtn = 0 Then
	If lu_string_functions.nf_isempty(ls_output) Then
		SetMicroHelp("No reports to show.")
	else
		dw_report_list.SetRedraw(False)
		dw_report_list.importstring(ls_output)
		dw_report_list.SetRedraw(True)
		dw_report_list.SelectRow(1, True)
	end if
end if

return li_rtn
end function

on w_report_list_new_inq.create
int iCurrent
call super::create
this.dw_report_list=create dw_report_list
this.cb_view=create cb_view
this.cb_print=create cb_print
this.cb_delete=create cb_delete
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_list
this.Control[iCurrent+2]=this.cb_view
this.Control[iCurrent+3]=this.cb_print
this.Control[iCurrent+4]=this.cb_delete
end on

on w_report_list_new_inq.destroy
call super::destroy
destroy(this.dw_report_list)
destroy(this.cb_view)
destroy(this.cb_print)
destroy(this.cb_delete)
end on

event ue_postopen;call super::ue_postopen;iw_parentwindow = message.powerobjectparm

is_userid = iw_parentwindow.is_userid

//is_userid = Message.StringParm 

wf_retrieve()


//integer 	li_rtn
//string		ls_output, ls_input
//
//u_string_functions		lu_string_functions
//
//iw_parentwindow = message.powerobjectparm
//
//is_userid = iw_parentwindow.is_userid
//
//ls_input = is_userid
//
//dw_report_list.reset()
//
//li_rtn = iu_ws_utl.nf_utlu29er(istr_error_info, "L", ls_input, ls_output)
//
//If li_rtn = 0 Then
//	If lu_string_functions.nf_isempty(ls_output) Then
//		SetMicroHelp("No reports to show.")
//	else
//		dw_report_list.SetRedraw(False)
//		dw_report_list.importstring(ls_output)
//		dw_report_list.SetRedraw(True)
//		dw_report_list.SelectRow(1, True)
//	end if
//end if
//
//
end event

event open;call super::open;iu_ws_utl = Create u_ws_utl
end event

event close;call super::close;Destroy (iu_ws_utl)
end event

event ue_base_ok;call super::ue_base_ok;string		ls_report_id, &
			ls_report_date, &
			ls_report_time, &
			ls_report_pages, &
			ls_report_list, &
			ls_option

ls_option = 'View'
ls_report_id	= dw_report_list.getitemstring(dw_report_list.getrow(),'report_id')
ls_report_date	= dw_report_list.getitemstring(dw_report_list.getrow(),'report_date')
ls_report_time	= dw_report_list.getitemstring(dw_report_list.getrow(),'report_time')
ls_report_pages	= dw_report_list.getitemstring(dw_report_list.getrow(),'report_pages')
ls_report_list	= dw_report_list.getitemstring(dw_report_list.getrow(),'report_list')

ls_report_date = mid(ls_report_date,7,4) + mid(ls_report_date,1,2) + mid(ls_report_date,4,2)

ls_report_time = mid(ls_report_time,1,2) + mid(ls_report_time,4,2) + mid(ls_report_time,7,2)

iw_parentwindow.Event ue_set_data('option', ls_option)
iw_parentwindow.Event ue_set_data('report_id', ls_report_id)
iw_parentwindow.Event ue_set_data('report_date', ls_report_date)
iw_parentwindow.Event ue_set_data('report_time', ls_report_time)
iw_parentwindow.Event ue_set_data('report_pages', ls_report_pages)
iw_parentwindow.Event ue_set_data('report_list', ls_report_list)

Close(This)
end event

event ue_base_cancel;call super::ue_base_cancel;iw_parentwindow.Event ue_set_data('report_id', '')
iw_parentwindow.Event ue_set_data('report_date', '')
iw_parentwindow.Event ue_set_data('report_time', '')
iw_parentwindow.Event ue_set_data('report_pages', '')

Close(This)
end event

type cb_base_help from w_netwise_response`cb_base_help within w_report_list_new_inq
boolean visible = false
integer x = 754
integer y = 1252
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_report_list_new_inq
integer x = 2094
integer y = 1252
string text = "&Close"
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_report_list_new_inq
boolean visible = false
integer x = 430
integer y = 1252
end type

type dw_report_list from u_netwise_dw within w_report_list_new_inq
integer x = 27
integer y = 36
integer width = 2322
integer height = 1152
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_report_view_print"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;//This.SelectRow(0, FALSE)
//This.SelectRow( row, Not(This.IsSelected( row)))
end event

event constructor;call super::constructor;is_selection = '3'
end event

type cb_view from commandbutton within w_report_list_new_inq
integer x = 1106
integer y = 1252
integer width = 270
integer height = 108
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&View"
end type

event clicked;Long		ll_sub, &
			ll_rowcount

Integer	li_selected_count

li_selected_count = 0
ll_rowcount = dw_report_list.RowCount()

//  if more than 1 selected row, then return
//  matches functionality of old VB control

for ll_sub = 1 to ll_rowcount
	if dw_report_list.IsSelected(ll_sub) Then
		li_selected_count ++
	End If
Next

if li_selected_count >= 2 Then Return

Parent.TriggerEvent ("ue_base_ok")



end event

type cb_print from commandbutton within w_report_list_new_inq
integer x = 1435
integer y = 1252
integer width = 270
integer height = 108
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Print"
end type

event clicked;Parent.TriggerEvent ("ue_print")
end event

type cb_delete from commandbutton within w_report_list_new_inq
integer x = 1765
integer y = 1252
integer width = 270
integer height = 108
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Delete"
end type

event clicked;integer	li_rtn

string		ls_report_id, &
			ls_report_date, &
			ls_report_time, &
			ls_report_pages, &
			ls_input, &
			ls_output
			
Long		ll_rowcount, &
			ll_sub

			
If MessageBox("Delete Rows", "Are you sure you want to delete the selected report(s)?", Question!, YesNo!, 2) = 2 Then return 1

ll_rowcount = dw_report_list.RowCount()

for ll_sub = 1 to ll_rowcount
	
	if dw_report_list.IsSelected(ll_sub) then

		ls_report_id	= dw_report_list.getitemstring(ll_sub,'report_id')
		ls_report_date	= dw_report_list.getitemstring(ll_sub,'report_date')
		ls_report_time	= dw_report_list.getitemstring(ll_sub,'report_time')
		ls_report_pages	= dw_report_list.getitemstring(ll_sub,'report_pages')
		
		//11/29/2016
		ls_report_date = mid(ls_report_date,7,4) + mid(ls_report_date,1,2) + mid(ls_report_date,4,2)
		
		//14:51:08
		ls_report_time = mid(ls_report_time,1,2) + mid(ls_report_time,4,2) + mid(ls_report_time,7,2)
		
		ls_input = is_userid + '~t' + ls_report_id + '~t' + ls_report_date + '~t' + ls_report_time + '~t' + ls_report_pages
			
		li_rtn = iu_ws_utl.nf_utlu30er(istr_error_info, "D", ls_input, ls_output)
		
//		If li_rtn = 0 Then
//			Choose Case ls_output
//				Case "1"
//					SetMicroHelp("Succesful delete route.")
//				Case "2"
//					SetMicroHelp("Delete print not allowed.")
//				Case "3"
//					SetMicroHelp("Unsuccesful delete route.")
//			End Choose
//		end if
		
	end if

Next
	

wf_retrieve()
	
end event

