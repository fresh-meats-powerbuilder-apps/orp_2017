HA$PBExportHeader$w_report_list_new.srw
forward
global type w_report_list_new from w_netwise_sheet
end type
type sle_count from singlelineedit within w_report_list_new
end type
type st_1 from statictext within w_report_list_new
end type
type cb_delete from commandbutton within w_report_list_new
end type
type cb_view from commandbutton within w_report_list_new
end type
type dw_report_dd from u_netwise_dw within w_report_list_new
end type
type dw_report_detail from u_netwise_dw within w_report_list_new
end type
end forward

global type w_report_list_new from w_netwise_sheet
integer x = 27
integer y = 64
integer width = 2926
integer height = 1400
string title = "Report List "
long backcolor = 12632256
event ue_view ( )
event ue_morbius ( )
event ue_pps ( )
event ue_print ( )
event ue_delete ( )
event ue_reinquire ( )
sle_count sle_count
st_1 st_1
cb_delete cb_delete
cb_view cb_view
dw_report_dd dw_report_dd
dw_report_detail dw_report_detail
end type
global w_report_list_new w_report_list_new

type variables
u_utl001				iu_utl001
u_dwselect			iu_dwselect

boolean				ib_OLE_Error 
boolean				ib_list, &
						ib_portrait,&
						ib_first_time
						
string 				is_report_id, &
						is_userid, &
						is_password,&	
						is_option, &
						is_report_date, &
						is_report_time, &
						is_report_pages, &
						is_report_list, &
						is_report_width
						
m_report_options	im_popmenu
u_olecom				iu_oleReportList
oleobject			iu_oleReport
m_netwise_menu		im_menu
//w_netwise_frame	iw_frame

u_ws_utl		iu_ws_utl

s_error		istr_error_info
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_retrieve ()
public subroutine wf_print ()
public subroutine wf_view ()
public subroutine wf_report_list ()
public subroutine wf_print_new ()
end prototypes

event ue_view;//s_Error					ls_Error
//
//oleobject				lu_oleReport
//u_string_functions	lu_string_functions
//integer					li_rtn, li_count
//long						ll_row
//string 					ls_output, ls_reportid
//
//if not ib_list then return
//
//
//ll_row = dw_1.getselectedrow(0)
//if ll_row = 0 then
//	iw_frame.setmicrohelp("A report has to be selected to view.")
//	return
//end if
//iw_frame.setmicrohelp("Inquiring to Mainframe for report information.")
//SetPointer(HourGlass!)
//this.SetRedraw(False)
//ls_reportid = dw_1.getitemstring(ll_row, "report_id")
//
//lu_oleReport = Create u_olecom
//// This is the correct way of call fo rthe report handle
////lu_oleReport = iu_oleReportList.item(ll_row)
//
////This is the way it works(but not the correct way to do things)
////iu_oleReportList.SetReport(ll_row)
////lu_oleReport = iu_oleReportList.NeededReport
//// This is a better way to do it incorrectlly
//lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//
//If IsNull(lu_oleReport) Then
//	// Didn't get an object, but no error happened.  This is weird.  Better exit just in case.
//	iw_frame.setmicrohelp("No connection made to the Com Object. - ue_view of w_report_list - ") 
//	SetPointer(Arrow!)
//	this.SetRedraw(True)
//	Return
//End If
//
//ls_output = lu_oleReport.rawdata
////ls_output = "12345678901234567890123456789012345678901234567890123456789012345678901234567890" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"         1         2         3      Report       5         6         7         8" + "~r~n" + & 
////				"         1         2         3       View        5         6         7         8" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"~f" + "~r~n" + & 
////				"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*(){}[]::;;<>,.?/-=_+|\            X" + "~r~n"
//
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"p","~f",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"a","|",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"$$HEX1$$ff00$$ENDHEX$$"," ",1)
//
//
//
//if len(ls_output) = 0 then
//	iw_frame.setmicrohelp("The Report is not available.")
//	SetPointer(Arrow!)
//	this.SetRedraw(True)
//	return
//end if
//
//if dw_1.getitemstring(ll_row,"report_width") = "W" then
//	ib_portrait = false
//else
//	ib_portrait = true
//end if
//
//iu_oleReport = lu_oleReport
//dw_1.DataObject = 'd_report_detail'
//dw_1.reset()
//
//if ib_portrait then
//	dw_1.object.detail.width = 2750
//else
//	dw_1.object.detail.width = 4306
//end if
//
//dw_1.importstring(ls_output)
//im_popmenu.m_popup.m_view.disable()
//
//ib_list = false
//
//SetPointer(Arrow!)
//this.SetRedraw(True)
//iw_frame.setmicrohelp("Ready")
end event

event ue_morbius;//s_Error		ls_Error
//oleobject		lu_oleReport
//integer		li_rtn, li_count
//long			ll_row
//string 		ls_output, ls_input
//
//ls_Error.se_User_ID = SQLCA.userid
//dw_1.SetRedraw(False)
//ll_row = dw_1.getselectedrow(0)
//if ll_row = 0 then
//	iw_frame.setmicrohelp("A report has to be selected to delete.")
//	return
//end if
//
//lu_oleReport = Create u_olecom
//
//if ib_list then
//	ll_row = dw_1.getselectedrow(0)
////	This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////	
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly 
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//
//lu_oleReport.Route("M")
//
//if ib_list then 
//	wf_retrieve()
//end if
//
//Messagebox("Process Flow","This is mobius!  " + ls_output)
//
//dw_1.SetRedraw(true)
end event

event ue_pps;//s_Error		ls_Error
//oleobject		lu_oleReport
//integer		li_rtn, li_count
//long			ll_row
//string 		ls_output, ls_input
//
//ls_Error.se_User_ID = SQLCA.userid
//
//lu_oleReport = Create u_olecom
//
//if ib_list then
//	ll_row = dw_1.getselectedrow(0)
//	if ll_row = 0 then
//		iw_frame.setmicrohelp("A report has to be selected to route to the PPS System.")
//		return
//	end if
//// This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//iw_frame.setmicrohelp("Inputting information to Mainframe for report.")
//SetPointer(HourGlass!)
//dw_1.SetRedraw(False)
//
//lu_oleReport.Route("P")
//
//ls_output = lu_oleReport.routeoutput
//choose case ls_output
//	case "1"
//		iw_frame.setmicrohelp("Route successful.")
//	case "2"
//		iw_frame.setmicrohelp("Route unsuccessful. The report has been reset..")
//		SetPointer(Arrow!)
//		dw_1.SetRedraw(true)
//		return
//	case "3"
//		iw_frame.setmicrohelp("Route unsuccessful.")
//		SetPointer(Arrow!)
//		dw_1.SetRedraw(true)
//		return
//	case else
//		iw_frame.setmicrohelp("Unknown return code.")
//		SetPointer(Arrow!)
//		dw_1.SetRedraw(true)
//		return
//end choose
//
//if ib_list then 
//	wf_retrieve()
//end if
//
//SetPointer(Arrow!)
//dw_1.SetRedraw(true)
//iw_frame.setmicrohelp("Ready")
end event

event ue_print();//oleobject				lu_oleReport
//datastore				lds_print_report, lds_report
//u_string_functions	lu_string_functions
//integer					li_rtn, li_count
//long						ll_row, ll_temp, ll_report_rowcount, ll_count, ll_remainder, ll_blank_count, ll_count2, ll_line_count
//string 					ls_output, ls_input, ls_string, ls_print_output
//boolean					lb_detail
//
//ll_row = dw_1.getselectedrow(0)
//if ll_row = 0  and ib_list then
//	iw_frame.setmicrohelp("A report has to be selected to print.")
//	return
//end if
//
//iw_frame.setmicrohelp("Inquiring to Mainframe for report information.")
//SetPointer(HourGlass!)
//dw_1.SetRedraw(False)
//
//if ib_list then
//	if dw_1.getitemstring(ll_row,"report_width") = "W" then
//		ib_portrait = false
//	else
//		ib_portrait = true
//	end if
//end if
//
//lu_oleReport = Create u_olecom
//
//if ib_list then 
////	This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////	
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly 
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//
//ls_output = lu_oleReport.RawData
//
////ls_output = "12345678901234567890123456789012345678901234567890123456789012345678901234567890" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"         1         2         3      Report       5         6         7         8" + "~r~n" + & 
////				"         1         2         3       View        5         6         7         8" + "~r~n" + & 
////				"         1         2         3         4         5         6         7         8" + "~r~n" + & 
////				"~p" + "~r~n" + & 
////				"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*(){}[]::;;<>,.?/-=_+|\            x" + "~r~n"
////
////Integer i
////For i = 1 to lu_oleReportView.Count
////	lu_oleReport = lu_oleReportView.Item(i)
////	
////	// Do something with lu_oleReport, for example
////	//ls_description = lu_oleReport.Description	
////	
////	
////Next
//
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"p","~f",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"a","|",1)
//ls_output = lu_string_functions.nf_globalreplace(ls_output,"$$HEX1$$ff00$$ENDHEX$$"," ",1)
//
//if len(ls_output) = 0 then
//	iw_frame.setmicrohelp("The Report is not available.")
//	SetPointer(Arrow!)
//	dw_1.SetRedraw(true)
//	return
//end if
//
//lds_report = create datastore
//lds_print_report = create datastore
//lds_report.dataobject = "d_report_detail"
//lds_print_report.dataobject = "d_report_print"
//lds_report.reset()
//lds_print_report.reset()
//
//if ib_portrait then
//	lds_print_report.Object.detail.Font.Height = 60
//else
//	lds_print_report.Object.detail.Font.Height = 40
//end if
//
//ll_report_rowcount = lds_report.importstring(ls_output)
//ll_count = 1
//ll_line_count = 1
//ls_print_output = ""
//lb_detail = false
//do until ll_count > ll_report_rowcount
//	choose case lds_report.getitemstring(ll_count, "detail_ind")
//		case "H"
//			if lb_detail then
//				ll_remainder = mod(ll_line_count, 68)
//				ll_blank_count = 69 - ll_remainder
//				if ll_blank_count > 0 then
//					for ll_count2 = 1 to ll_blank_count
//						ls_print_output += "                               " + "~r~n"
//						ll_line_count ++
//					next
//				end if
//				ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~r~n"
////				ll_line_count ++
////				ll_count ++
//			else
//				ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~r~n"
////				ll_line_count ++
////				ll_count ++
//			end if
//		case "D"
//			ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~r~n"
////			ll_line_count ++
////			ll_count ++
//			lb_detail = true
//	end choose
//	ll_line_count ++
//	ll_count ++
//loop
//
//lds_print_report.importstring(ls_print_output)
//
//if ib_portrait then
//	lds_report.Object.detail.Font.Height = 60
//else
//	lds_report.Object.detail.Font.Height = 40
//end if
// 
//lds_print_report.Print()
//SetPointer(Arrow!)
//dw_1.SetRedraw(true)
//iw_frame.setmicrohelp("Ready")
//
end event

event ue_delete;//s_Error		ls_Error
//oleobject		lu_oleReport
//integer		li_rtn, li_count
//long			ll_row
//string 		ls_output, ls_input
//
//ls_Error.se_User_ID = SQLCA.userid
//
//lu_oleReport = Create u_olecom
//
//if ib_list then
//	ll_row = dw_1.getselectedrow(0)	
//	if ll_row = 0 then
//		iw_frame.setmicrohelp("A report has to be selected to delete")
//		return
//	end if
////	This is the correct way of call fo rthe report handle
////	lu_oleReport = iu_oleReportList.item(ll_row)
////	
////	This is the way it works(but not the correct way to do things)
////	iu_oleReportList.SetReport(ll_row)
////	lu_oleReport = iu_oleReportList.NeededReport
////	This is a better way to do it incorrectlly 
//	lu_oleReport = iu_oleReportList.NeededReport(ll_row)
//else
//   lu_oleReport = iu_oleReport
//end if
//iw_frame.setmicrohelp("Deleting the report")
//SetPointer(HourGlass!)
//
//lu_oleReport.Delete
//
//ls_output = lu_oleReport.DeleteOutput
//choose case ls_output
//	case "1"
//		This.SetRedraw(True)
//		iw_frame.setmicrohelp("Delete successful")
//		wf_retrieve()
//		Return
//	case "2"
//		if ib_list then
//			dw_1.setitem(ll_row,"report_description", "** RETAINED **")
//		end if
//		iw_frame.setmicrohelp("Report is protected. Delete unsuccessful.")
//		SetPointer(Arrow!)
//		this.SetRedraw(True)
//		return
//	case "3"
//		iw_frame.setmicrohelp("Delete unsuccessful")
//		SetPointer(Arrow!)
//		this.SetRedraw(True)
//		return
//	case else
//		iw_frame.setmicrohelp("Unknown return code")
//		SetPointer(Arrow!)
//		this.SetRedraw(True)
//		return
//end choose
//
end event

event ue_reinquire;//
//dw_1.DataObject = 'd_report_view_print'
//dw_1.reset()
//
//im_popmenu.m_popup.m_view.enable()
//ib_list = true
end event

public subroutine wf_delete ();/*
//if ib_OLE_Error then return 
//
//ole_report.object.DeleteReport()
*/

end subroutine

public function boolean wf_retrieve ();OpenwithParm(w_report_list_new_inq, this)

wf_report_list()

dw_report_detail.reset()

If is_option = 'View' then
	wf_view()
End If

return true


end function

public subroutine wf_print ();datastore				lds_print_report, lds_report
//u_string_functions	lu_string_functions
//integer					li_rtn, li_count
long						ll_row, ll_temp, ll_report_rowcount, ll_count, ll_page_num
string 					ls_output, ls_input, ls_string, ls_print_output
boolean					lb_detail, lb_end
integer					li_rtn

lds_report = create datastore
lds_print_report = create datastore
lds_report.dataobject = "d_report_detail"
lds_print_report.dataobject = "d_report_print"
lds_report.reset()
lds_print_report.reset()

//if ib_portrait then
//	lds_print_report.Object.detail.Font.Height = 60
//else
//	lds_print_report.Object.detail.Height = 52
//end if

//--------------------
//ls_input = is_userid + '~t' + is_report_id + '~t' + is_report_date + '~t' + is_report_time + '~t' + is_report_pages

//li_rtn = iu_ws_utl.nf_utlu29er(istr_error_info, "V", ls_input, ls_output)
//-----------------

ll_report_rowcount = 7	//lds_report.importstring(ls_output from UTLU29ER call)

ls_print_output = ""
lb_detail = false
lb_end = false
ll_page_num = 1
ll_count = 1

do until lb_end
	choose case dw_report_detail.getitemstring(ll_count, "detail_ind")
			
		case "H"
			if lb_detail then
				ll_page_num ++
				ls_print_output += dw_report_detail.getitemstring(ll_count, "detail") + "~t" + string(ll_page_num) + "~r~n"
			else
				ls_print_output += dw_report_detail.getitemstring(ll_count, "detail") +  "~t" + string(ll_page_num) + "~r~n"
			end if
			
		case "D"
			ls_print_output += dw_report_detail.getitemstring(ll_count, "detail")  +  "~t" + string(ll_page_num) + "~r~n"
			lb_detail = true
		
		case else
			lb_end = true
			
	end choose
	
	ll_count ++
loop

lds_print_report.importstring(ls_print_output)
lds_print_report.GroupCalc()

//lds_print_report.Modify("DataWindow.print.preview=YES") 
lds_print_report.Modify("DataWindow.Print.Page.Range='1-2'")
lds_print_report.Print(true,true)
SetPointer(Arrow!)
//dw_1.SetRedraw(true)
iw_frame.setmicrohelp("Ready")


end subroutine

public subroutine wf_view ();integer	li_rtn, li_rows

string		ls_report_id, &
			ls_report_date, &
			ls_report_time, &
			ls_report_pages, &
			ls_report_list, &
			ls_option, &
			ls_input, &
			ls_output
			
u_string_functions		lu_string_functions

dw_report_detail.reset()

If not lu_string_functions.nf_isempty(is_report_id) Then
	
		ls_option = is_option
		
		ls_report_id = is_report_id
		ls_report_date = is_report_date
		ls_report_time = is_report_time
		ls_report_pages = is_report_pages
		ls_report_list = is_report_list
		
		dw_report_dd.SetItem(1, "report", ls_report_list) 
		
		ls_input = is_userid + '~t' + ls_report_id + '~t' + ls_report_date + '~t' + ls_report_time + '~t' + ls_report_pages
		
		li_rtn = iu_ws_utl.nf_utlu29er(istr_error_info, "V", ls_input, ls_output)
		
		If li_rtn = 0 Then
			If lu_string_functions.nf_isempty(ls_output) Then
				SetMicroHelp("Report not found.")
			else
				dw_report_detail.SetRedraw(False)
				dw_report_detail.importstring(ls_output)
				dw_report_detail.SetRedraw(True)
				
				If ls_option = 'Print' Then
					wf_print()
//					dw_report_detail.reset()
				End If
				
//				dw_report_detail.SetRedraw(True)
				
				SetMicroHelp("Ready")
			end if
		end if

else
	SetMicroHelp("Ready")
end if

//return true
end subroutine

public subroutine wf_report_list ();integer	li_rtn, li_rows

long		ll_scroll

string		ls_report_id, &
			ls_report_date, &
			ls_report_time, &
			ls_report_pages, &
			ls_option, &
			ls_input, &
			ls_output, &
			ls_report
			
u_string_functions		lu_string_functions

DataWindowChild	ldwc_temp

ll_scroll = dw_report_dd.insertrow(0)
//dw_report_dd.ScrollToRow(ll_scroll)



ls_input = is_userid

li_rtn = iu_ws_utl.nf_utlu29er(istr_error_info, "L", ls_input, ls_output)

If li_rtn = 0 Then
	If lu_string_functions.nf_isempty(ls_output) Then
		dw_report_dd.getchild("report",ldwc_temp)
		ldwc_temp.reset()
		SetMicroHelp("No reports to show.")
	else
		dw_report_dd.setredraw(false)
		dw_report_dd.getchild("report",ldwc_temp)
		ldwc_temp.reset()
		ldwc_temp.importstring(ls_output)
		li_rows = ldwc_temp.rowcount()
		
		//ldwc_temp.SelectRow(1,true)
		//ldwc_temp.ScrollToRow (1)
		//li_scroll = dw_report_dd.ScrollToRow (2)
		
		dw_report_dd.setredraw(true)
	end if
end if

//ls_report = dw_report_dd.GetItemString(1, "report")
//ls_report = "ORP109XE - COMBINED ORDERS BY P (001 Pages) 12/14/2016 15:48:02"
//if isnull(dw_report_dd.GetItemString(1, "report")) then 
//	dw_report_dd.SetItem(1, "report", ls_report) 
//end if 

dw_report_dd.SetItem(1, "report", "") 

//	dw_report_dd.SetFocus()
//	dw_report_dd.ScrollToRow(1)

//dw_report_dd.SETROW(1)
//dw_report_dd.SelectRow(1,true)
//dw_report_dd.ScrollToRow (1)


sle_count.text = string(li_rows)

dw_report_detail.reset()
end subroutine

public subroutine wf_print_new ();datastore				lds_print_report, lds_report
//u_string_functions	lu_string_functions
//integer					li_rtn, li_count
long						ll_row, ll_temp, ll_count, ll_page_num
string 					ls_output, ls_input, ls_string, ls_print_output, &
							ls_option, ls_report_id, ls_report_date, ls_report_time, &
							ls_report_pages, ls_report_list, ls_report_width		
							
boolean					lb_detail, lb_end
integer					li_rtn

u_string_functions		lu_string_functions

ls_option = is_option
ls_report_id = is_report_id
ls_report_date = is_report_date
ls_report_time = is_report_time
ls_report_pages = is_report_pages
ls_report_list = is_report_list
ls_report_width = is_report_width

lds_report = create datastore
lds_print_report = create datastore
lds_report.dataobject = "d_report_detail"

if ls_report_width = 'N' then
	lds_print_report.dataobject = "d_report_print"
else
	lds_print_report.dataobject = "d_report_print_wide"
	lds_print_report.object.detail.Font.Height = 40
end if

lds_report.reset()
lds_print_report.reset()

ls_input = is_userid + '~t' + ls_report_id + '~t' + ls_report_date + '~t' + ls_report_time + '~t' + ls_report_pages

li_rtn = iu_ws_utl.nf_utlu29er(istr_error_info, "V", ls_input, ls_output)

If li_rtn = 0 Then
	If lu_string_functions.nf_isempty(ls_output) Then
		SetMicroHelp("Report not found.")
	else
		lds_report.importstring(ls_output)
	end if
end if

ls_print_output = ""
lb_detail = false
lb_end = false
ll_page_num = 1
ll_count = 1

do until lb_end
	
//	ls_ind = 	lds_report.getitemstring(ll_count, "detail_ind")
//	ls_detail =  lds_report.getitemstring(ll_count, "detail")
	
	if not isnull(lds_report.getitemstring(ll_count, "detail")) then
	
		choose case lds_report.getitemstring(ll_count, "detail_ind")
				
			case "H"
				if lb_detail then
					ll_page_num ++
					ls_print_output += lds_report.getitemstring(ll_count, "detail") + "~t" + string(ll_page_num) + "~r~n"
				else
					ls_print_output += lds_report.getitemstring(ll_count, "detail") +  "~t" + string(ll_page_num) + "~r~n"
				end if
				
			case "D"
				ls_print_output += lds_report.getitemstring(ll_count, "detail")  +  "~t" + string(ll_page_num) + "~r~n"
				lb_detail = true
			
			case else
				lb_end = true
				
		end choose
		
	end if
		
	ll_count ++
loop

lds_print_report.importstring(ls_print_output)
lds_print_report.GroupCalc()

//lds_print_report.Modify("DataWindow.print.preview=YES") 
lds_print_report.Modify("DataWindow.Print.Page.Range='1-2'")
lds_print_report.Print(true,true)
//SetPointer(Arrow!)
//dw_1.SetRedraw(true)
//iw_frame.setmicrohelp("Ready")


end subroutine

on w_report_list_new.create
int iCurrent
call super::create
this.sle_count=create sle_count
this.st_1=create st_1
this.cb_delete=create cb_delete
this.cb_view=create cb_view
this.dw_report_dd=create dw_report_dd
this.dw_report_detail=create dw_report_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_count
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_delete
this.Control[iCurrent+4]=this.cb_view
this.Control[iCurrent+5]=this.dw_report_dd
this.Control[iCurrent+6]=this.dw_report_detail
end on

on w_report_list_new.destroy
call super::destroy
destroy(this.sle_count)
destroy(this.st_1)
destroy(this.cb_delete)
destroy(this.cb_view)
destroy(this.dw_report_dd)
destroy(this.dw_report_detail)
end on

event ue_postopen;call super::ue_postopen;iu_ws_utl = Create u_ws_utl

wf_retrieve()


/*
//String	ls_server_suffix
//
//if ib_OLE_Error then return 
//
//ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 
//
//If IsNull(ls_server_suffix) Then  ls_server_suffix = " "
//ole_report.object.SignonSystem = ls_server_suffix
//
//IF IsNull(is_userid) Then is_userid = " "
//ole_report.object.UserName = is_userid  
//
//IF IsNull(is_password) Then is_password = " "
//ole_report.object.Password = is_password 
//
//wf_retrieve()
//
//if this.width  > 10 then
//	ole_report.Width = this.width - 50
//end if
//
//if this.height > 10 then
//	ole_report.Height = this.height - 150
//end if
*/
//
//
//
//
//
//
//
//
//
//
//
//
//
//integer		li_rtn
//
//im_popmenu = Create m_report_options
//is_userid = SQLCA.UserID
//is_password = SQLCA.dbpass
//
//ib_list = true
//
//
//
//iu_oleReportList = Create u_olecom
//li_rtn = iu_oleReportList.ConnectToNewObject("ReportView.ReportList")
//
//If li_rtn < 0 Then
//	// Problem happened, handle this error and forget about retrieving the report list
//	messagebox("Error",string("Error has occurred. Return code from Com Object is " + &
//														string(li_rtn) + ". Please call help desk 3133")) 
//	Return 
//End If
//
//If IsNull(iu_oleReportList) Then
//	// Didn't get an object, but no error happened.  This is weird.  Better exit just in case.
//	iw_frame.setmicrohelp("No connection made to the Com Object. - wf_retrieve of w_report_list - ") 	
//	Return 
//End If
//
//ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 
//
//
//
//wf_retrieve()





end event

event activate;call super::activate;string	ls_option,ls_temp

ls_option = Message.StringParm

// ibdkdld 07/02 fix to work from last used windows option _
// and disable m_new
If ls_option <> "S" and ls_option <> "U" then ls_option = "U"

iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_generatesales')
iw_frame.im_menu.mf_Disable('m_complete')		
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_disable('m_new')

//ls_temp = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")

CHOOSE CASE ls_option
	//The User has requested the Scheduling System Reports	
	CASE "S"
		is_userid = "DCSCXXX"
		//password must have something in it
		is_password = "XXX"
		//User's are not allowed to delete system reports
		iw_frame.im_menu.mf_disable('m_delete')
	//The User has requested their own Reports	
	CASE "U"
		is_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
		is_password = SQLCA.dbpass
		iw_frame.im_menu.mf_enable('m_delete')
END CHOOSE

end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_generatesales')
iw_frame.im_menu.mf_enable('m_complete')		
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
// ibdkdld 07/02 fix to work from last used windows option _
// and disable m_new
iw_frame.im_menu.mf_enable('m_new')

//iw_frame.im_menu.mf_disable('m_print')

If is_userid = 'DCSCXXX' Then
	iw_frame.im_menu.mf_enable('m_delete')
End If
	
end event

event resize;call super::resize;/*
//if newwidth  > 100 then
//	ole_report.Width = newwidth 
//end if
//
//if newheight > 200 then
//	ole_report.Height = newheight 
//end if
*/
//integer li_x		= 14
//integer li_y		= 14

//.width	= width - (50 + li_x)
//dw_1.height	= height - (125 + li_y)

end event

event ue_fileprint;//wf_print()
//this.triggerevent ("ue_print")

DataWindowChild	ldwc_temp
dw_report_dd.getchild("report",ldwc_temp)

string		ls_report
ls_report = dw_report_dd.GetItemString(1, "report")

if ls_report <> "" Then
	
	is_option = 'Print'
	is_report_id	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_id')
	is_report_date	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_date')
	is_report_time	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_time')
	is_report_pages	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_pages')
	is_report_list	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_list')
	is_report_width	 = ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_width')
	
	is_report_date = mid(is_report_date,7,4) + mid(is_report_date,1,2) + mid(is_report_date,4,2)
	
	is_report_time = mid(is_report_time,1,2) + mid(is_report_time,4,2) + mid(is_report_time,7,2)
	
	//wf_view()
	wf_print_new()

End if
end event

event clicked;This.SetFocus()
This.BringtoTop = True

end event

event ue_printwithsetup;This.TriggerEvent("ue_FilePrint")
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'option'
		is_option = as_value
	Case 'report_id'
		is_report_id = as_value
	Case 'report_date'
		is_report_date = as_value
	Case 'report_time'
		is_report_time = as_value
	Case 'report_pages'
		is_report_pages = as_value			
	Case 'report_list'
		is_report_list = as_value		
	Case 'report_width'
		is_report_width = as_value			
End Choose
end event

event close;call super::close;Destroy (iu_ws_utl)
end event

type sle_count from singlelineedit within w_report_list_new
integer x = 2693
integer y = 24
integer width = 165
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean enabled = false
string text = "0"
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_report_list_new
integer x = 2501
integer y = 48
integer width = 169
integer height = 52
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Count"
boolean focusrectangle = false
end type

type cb_delete from commandbutton within w_report_list_new
integer x = 2190
integer y = 20
integer width = 270
integer height = 108
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Delete"
end type

event clicked;integer	li_rtn

string		ls_input, &
			ls_output
			
string		ls_report
ls_report = dw_report_dd.GetItemString(1, "report")

if ls_report <> "" Then

	If MessageBox("Delete Rows", "Are you sure you want to delete the report?", Question!, YesNo!, 2) = 2 Then return 1
	
	DataWindowChild	ldwc_temp
	dw_report_dd.getchild("report",ldwc_temp)
	
	is_option = 'View'
	is_report_id	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_id')
	is_report_date	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_date')
	is_report_time	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_time')
	is_report_pages	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_pages')
	
	is_report_date = mid(is_report_date,7,4) + mid(is_report_date,1,2) + mid(is_report_date,4,2)
	
	is_report_time = mid(is_report_time,1,2) + mid(is_report_time,4,2) + mid(is_report_time,7,2)
	
	ls_input = is_userid + '~t' + is_report_id + '~t' + is_report_date + '~t' + is_report_time + '~t' + is_report_pages
		
	li_rtn = iu_ws_utl.nf_utlu30er(istr_error_info, "D", ls_input, ls_output)
	
	If li_rtn = 0 Then
		Choose Case ls_output
			Case "1"
				SetMicroHelp("Succesful delete route.")
			Case "2"
				SetMicroHelp("Delete print not allowed.")
			Case "3"
				SetMicroHelp("Unsuccesful delete route.")
		End Choose
	end if
	
	wf_report_list()
	
End If
	
end event

type cb_view from commandbutton within w_report_list_new
integer x = 1902
integer y = 20
integer width = 270
integer height = 108
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&View"
end type

event clicked;DataWindowChild	ldwc_temp
dw_report_dd.getchild("report",ldwc_temp)

string		ls_report
ls_report = dw_report_dd.GetItemString(1, "report")

if ls_report <> "" Then
	
	is_option = 'View'
	is_report_id	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_id')
	is_report_date	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_date')
	is_report_time	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_time')
	is_report_pages	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_pages')
	is_report_list	= ldwc_temp.getitemstring(ldwc_temp.getrow(),'report_list')
	
	is_report_date = mid(is_report_date,7,4) + mid(is_report_date,1,2) + mid(is_report_date,4,2)
	
	is_report_time = mid(is_report_time,1,2) + mid(is_report_time,4,2) + mid(is_report_time,7,2)
	
	wf_view()

End if
end event

type dw_report_dd from u_netwise_dw within w_report_list_new
integer x = 32
integer y = 32
integer width = 1847
integer height = 88
integer taborder = 20
string dataobject = "d_report_dddw"
borderstyle borderstyle = stylelowered!
boolean ib_scrollsetscurrentrow = true
end type

type dw_report_detail from u_netwise_dw within w_report_list_new
integer y = 148
integer width = 2880
integer height = 1156
integer taborder = 10
string dataobject = "d_report_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

