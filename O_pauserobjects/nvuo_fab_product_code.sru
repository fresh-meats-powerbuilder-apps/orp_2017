HA$PBExportHeader$nvuo_fab_product_code.sru
forward
global type nvuo_fab_product_code from nonvisualobject
end type
end forward

global type nvuo_fab_product_code from nonvisualobject autoinstantiate
end type

type variables
Public:
Boolean		ib_error_occurred = FALSE

Private:
s_error		istr_error_info
u_pas201		iu_pas201
u_ws_pas1		iu_ws_pas1

String 		is_product_code, &	
				is_product_data, &
				is_product_description, &
				is_valid_product_states, &
				is_valid_product_statuses, &
				is_return_message

Boolean		ib_required = TRUE



end variables

forward prototypes
public function string uf_get_product_description ()
public function string uf_get_return_message ()
public function string uf_get_product_states ()
public function string uf_get_product_data ()
public function boolean uf_check_product (string as_product_code)
public function boolean uf_check_product_state (string as_product_code, string as_product_state)
public function boolean uf_check_product_state_rmt (string as_product_code, string as_product_state)
public function boolean uf_check_product_status (string as_product_code, string as_product_status)
public function boolean uf_check_product_status_rmt (string as_product_code, string as_product_status)
public function string uf_get_product_statuses ()
public function boolean uf_check_product (string as_product_code, string as_product_state)
end prototypes

public function string uf_get_product_description ();return is_product_description
end function

public function string uf_get_return_message ();return is_return_message
end function

public function string uf_get_product_states ();return is_valid_product_states
end function

public function string uf_get_product_data ();return is_product_data
end function

public function boolean uf_check_product (string as_product_code);return uf_check_product(as_product_code,'1')
end function

public function boolean uf_check_product_state (string as_product_code, string as_product_state);Long		ll_rtn_count

If Upper(Trim(is_product_code)) <> Upper(Trim(as_product_code)) Then
		if not uf_check_product(as_product_code) then return false
End If

ll_rtn_count = iw_frame.iu_string.nf_countoccurrences(is_valid_product_states, as_product_state)
If IsNull(ll_rtn_count) Then
	Return False 
End if

Return (ll_rtn_count > 0)
end function

public function boolean uf_check_product_state_rmt (string as_product_code, string as_product_state);Long		ll_rtn_count

String	ls_valid_product_states


If Upper(Trim(is_product_code)) <> Upper(Trim(as_product_code)) Then
		if not uf_check_product(as_product_code) then return false
End If

CONNECT USING SQLCA;	
SELECT tutltypes.type_code  
INTO :ls_valid_product_states
FROM tutltypes  
WHERE ( tutltypes.record_type = 'PRDSTATE' ) AND  
      ( tutltypes.type_code = :as_product_state )   ;


If IsNull(ls_valid_product_states) or ls_valid_product_states = "" Then
	Return False
End If
//Return (ll_rtn_count > 0)
Return True
end function

public function boolean uf_check_product_status (string as_product_code, string as_product_status);Long		ll_rtn_count

If Upper(Trim(is_product_code)) <> Upper(Trim(as_product_code)) Then
		if not uf_check_product(as_product_code) then return false
End If

ll_rtn_count = iw_frame.iu_string.nf_countoccurrences(is_valid_product_statuses, as_product_status)
If IsNull(ll_rtn_count) Then
	Return False 
End if

Return (ll_rtn_count > 0)
end function

public function boolean uf_check_product_status_rmt (string as_product_code, string as_product_status);Long		ll_rtn_count

String	ls_valid_product_statuses


If Upper(Trim(is_product_code)) <> Upper(Trim(as_product_code)) Then
		if not uf_check_product(as_product_code) then return false
End If

CONNECT USING SQLCA;	
SELECT tutltypes.type_code  
INTO :ls_valid_product_statuses
FROM tutltypes  
WHERE ( tutltypes.record_type = 'PRODSTAT' ) AND  
      ( tutltypes.type_code = :as_product_status )   ;


If IsNull(ls_valid_product_statuses) or ls_valid_product_statuses = "" Then
	Return False
End If
//Return (ll_rtn_count > 0)
Return True
end function

public function string uf_get_product_statuses ();return is_valid_product_statuses
end function

public function boolean uf_check_product (string as_product_code, string as_product_state);Boolean			lb_return
String			ls_product_data, &
					ls_valid_product_states, &
					ls_return_message, &
					ls_prod_statuses, &
					ls_status
				
is_product_code = ''
is_product_data = ''
is_product_description = ''
is_valid_product_states = ''
is_return_message = ''
ib_error_occurred = False
		
iw_frame.SetMicroHelp("Wait.. Inquiring Database")

istr_error_info.se_event_name			= "uf_check_product"
istr_error_info.se_procedure_name	= "nf_pasp03br"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

iu_ws_pas1 = Create u_ws_pas1


//lb_return	= iu_pas201.nf_pasp03br( istr_Error_Info, as_product_code,as_product_state, ls_product_data, ls_valid_product_states)
lb_return	= iu_ws_pas1.nf_pasp03fr( istr_Error_Info, as_product_code,as_product_state, ls_product_data, ls_valid_product_states)
 
IF Not lb_return THEN
	is_return_message = istr_error_info.se_message
	ib_error_occurred = TRUE
	
	If Left(Upper(istr_error_info.se_message),6) = "PAS002" Then
		// Allow us to display "So-and-so is an invalid Product Code" instead of
		// displaying "PAS002 FAB PRODUCT CODE SO-AND-SO NOT FOUND" which is ugly

		// ** IBDKEEM ** 10/31/2002 ** 
		is_product_code = ""
		is_product_data = ""
		is_product_description = ""
		is_valid_product_states = ""
		
		ib_error_occurred = FALSE
		Return False
	End If
End If

is_product_code = as_product_code
is_product_data = ls_product_data
is_product_description = iw_frame.iu_string.nf_gettoken(ls_product_data,'~t')
is_valid_product_states = ls_valid_product_states


// Declare the status_cur. 
ls_prod_statuses = ''
DECLARE status_cur CURSOR FOR
SELECT tutltypes.type_code
FROM tutltypes
Where 	(tutltypes.record_type = 'PRODSTAT');
			

// Open The Cursor.
OPEN status_cur;
// Fetch the first row from the result set.
FETCH status_cur INTO :ls_status;
// Loop through result set until exhausted.
DO WHILE sqlca.sqlcode = 0
	// Build the return string.
	ls_prod_statuses += trim(ls_status)
	// Fetch the next row from the result set.
	FETCH status_cur INTO :ls_status;
LOOP
// All done, so close the cursor.
CLOSE status_cur;	

is_valid_product_statuses = ls_prod_statuses
Destroy iu_ws_pas1
return true
end function

on nvuo_fab_product_code.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvuo_fab_product_code.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

