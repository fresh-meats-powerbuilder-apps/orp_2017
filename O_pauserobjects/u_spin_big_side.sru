HA$PBExportHeader$u_spin_big_side.sru
$PBExportComments$Inherited from u_spin_big, but buttons are side by side
forward
global type u_spin_big_side from u_spin_big
end type
end forward

global type u_spin_big_side from u_spin_big
int Width=298
int Height=113
end type
global u_spin_big_side u_spin_big_side

on u_spin_big_side.create
call u_spin_big::create
end on

on u_spin_big_side.destroy
call u_spin_big::destroy
end on

type p_down from u_spin_big`p_down within u_spin_big_side
int Y=5
end type

type p_up from u_spin_big`p_up within u_spin_big_side
int X=151
end type

