HA$PBExportHeader$u_spin_up_down.sru
$PBExportComments$This is the same as u_spin but is to be used for moving items up and down
forward
global type u_spin_up_down from UserObject
end type
type p_down from picture within u_spin_up_down
end type
type p_up from picture within u_spin_up_down
end type
end forward

global type u_spin_up_down from UserObject
int Width=151
int Height=212
long BackColor=12632256
long PictureMaskColor=25166016
long TabTextColor=33554432
long TabBackColor=67108864
event resize pbm_custom01
p_down p_down
p_up p_up
end type
global u_spin_up_down u_spin_up_down

type variables

end variables

on u_spin_up_down.create
this.p_down=create p_down
this.p_up=create p_up
this.Control[]={this.p_down,&
this.p_up}
end on

on u_spin_up_down.destroy
destroy(this.p_down)
destroy(this.p_up)
end on

type p_down from picture within u_spin_up_down
event lbuttondown pbm_lbuttondown
event lbuttonup pbm_lbuttonup
int X=5
int Width=142
int Height=100
string PictureName="spin1.bmp"
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
boolean FocusRectangle=false
end type

on lbuttondown;This.BorderStyle = StyleLowered!

end on

on lbuttonup;This.BorderStyle = StyleRaised!

end on

event clicked;If gw_base_frame.im_base_menu.m_Edit.m_Previous.Enabled And &
		gw_base_frame.im_base_menu.m_Edit.m_Previous.Visible Then
	gw_base_frame.im_base_menu.m_Edit.m_Previous.PostEvent(Clicked!)
End if

end event

type p_up from picture within u_spin_up_down
event lbuttondown pbm_lbuttondown
event lbuttonup pbm_lbuttonup
int X=5
int Y=108
int Width=142
int Height=100
string PictureName="spin2.bmp"
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
boolean FocusRectangle=false
end type

on lbuttondown;This.BorderStyle = StyleLowered!

end on

on lbuttonup;This.BorderStyle = StyleRaised!

end on

event clicked;If gw_base_frame.im_base_menu.m_Edit.m_Next.Enabled And &
		gw_base_frame.im_base_menu.m_Edit.m_Next.Visible Then
	gw_base_frame.im_base_menu.m_Edit.m_Next.PostEvent(Clicked!)
End if

end event

