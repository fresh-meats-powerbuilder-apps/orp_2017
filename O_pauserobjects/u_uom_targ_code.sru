HA$PBExportHeader$u_uom_targ_code.sru
forward
global type u_uom_targ_code from datawindow
end type
end forward

global type u_uom_targ_code from datawindow
integer width = 1166
integer height = 132
string title = "none"
string dataobject = "d_uom_targ_code"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type
global u_uom_targ_code u_uom_targ_code

forward prototypes
public function string uf_get_uom_code ()
public subroutine uf_set_uom_code (string as_uom_code)
public subroutine uf_enable (boolean ab_enable)
public function string uf_get_description ()
end prototypes

public function string uf_get_uom_code ();return Trim(This.GetItemString(1, "uom_code"))
end function

public subroutine uf_set_uom_code (string as_uom_code);DataWindowChild	ldwc_child

Long	ll_row, &
		ll_row_count

String	ls_text, &
			ls_description




This.SetItem(1, "uom_code", as_uom_code)

If Len(trim(as_uom_code)) = 0 Then Return

This.GetChild("uom_code",ldwc_child)

ls_text = 'Trim(type_code) = "' + as_uom_code + '"'
ll_row_count = ldwc_child.RowCount()
ll_row = ldwc_child.Find(ls_text, 1, ll_row_count)
If ll_row <= 0 Then
	gw_netwise_Frame.SetMicroHelp(as_uom_code + " is an Invalid UOM Code")
	SetFocus()
	This.SelectText(1, Len(ls_text))
Else
	SetFocus()
	This.SelectText(1, Len(ls_text) + 10) 
	gw_netwise_frame.SetMicroHelp("Ready")
End if


end subroutine

public subroutine uf_enable (boolean ab_enable);if ab_enable then
	This.Modify("uom_code.Background.Mode = 0 uom_code.Protect = 0" + &
					"uom_code.Pointer = 'Beam!'")
else
	This.Modify("uom_code.Background.Mode = 1 uom_code.Protect = 1 " + &
				"uom_code.Pointer = 'Arrow!'")
end if
				
end subroutine

public function string uf_get_description ();DataWindowChild	ldwc_temp
String				ls_desc
Long					ll_row

this.getchild("uom_code", ldwc_temp)
ll_row = ldwc_temp.find("type_code = '" + this.getitemstring(1, "uom_code") + "'", 1, ldwc_temp.Rowcount() + 1)

if ll_row > 0 Then
	ls_desc = ldwc_temp.getitemstring(ll_row, "type_desc")
else
	ls_desc = ""
end if

Return ls_desc

end function

on u_uom_targ_code.create
end on

on u_uom_targ_code.destroy
end on

event constructor;DataWindowChild	ldwc_child
String				ls_uom_code


This.GetChild("uom_code", ldwc_child)

ldwc_child.SetTransObject(SQLCA)
ldwc_child.Retrieve()

If This.Describe("uom_code.Protect") <> '1' Then
	ls_uom_code = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastBoxInventoryTargUOMCode", "  ")
	This.Reset()
	This.InsertRow(0)
	This.uf_set_uom_code(ls_uom_code)
End if

end event

event destructor;SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastBoxInventoryTargUOMCode", &
			This.GetItemString(1, 'uom_code'))

end event

event getfocus;This.SelectText(1, Len(This.GetText()))
end event

event itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end event

