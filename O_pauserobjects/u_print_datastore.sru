HA$PBExportHeader$u_print_datastore.sru
forward
global type u_print_datastore from datastore
end type
end forward

global type u_print_datastore from datastore
end type
global u_print_datastore u_print_datastore

type variables
w_printing		iw_printing

integer			ii_pagemax
end variables

on u_print_datastore.create
call datastore::create
TriggerEvent( this, "constructor" )
end on

on u_print_datastore.destroy
call datastore::destroy
TriggerEvent( this, "destructor" )
end on

event printstart;Open(iw_printing)

iw_printing.Event ue_setdata('pagesmax', String(pagesmax))

ii_pagemax = pagesmax
end event

event printend;Close(iw_printing)
end event

event printpage;String				ls_cancel, &
						ls_modify


iw_printing.Event ue_setdata('pagenumber', String(pagenumber))
iw_printing.Event ue_getdata('cancel', ls_cancel)
If ls_cancel = 'yes' Then 
	This.PrintCancel()
	Return 1
End If


// I hid the Computed Filed and made it a text field until you can go to 6.01
//ls_modify = "xofx.Text = 'Page " + String(pageNumber) + " of " + String(ii_pagemax) + "'"
//This.modify(ls_modify)

Return
end event

