HA$PBExportHeader$u_product_productgroup.sru
forward
global type u_product_productgroup from datawindow
end type
end forward

global type u_product_productgroup from datawindow
int Width=539
int Height=324
int TabOrder=10
string DataObject="d_product_productgroup"
boolean Border=false
boolean LiveScroll=true
end type
global u_product_productgroup u_product_productgroup

forward prototypes
public function string uf_get_product_productgroup ()
public subroutine uf_set_product_productgroup (string as_product_productgroup)
public subroutine uf_protect ()
end prototypes

public function string uf_get_product_productgroup ();return This.GetItemString(1, "product_productgroup")
end function

public subroutine uf_set_product_productgroup (string as_product_productgroup);This.SetItem(1,"product_productgroup",as_product_productgroup)
end subroutine

public subroutine uf_protect ();This.object.product_productgroup.Background.Color = 12632256
This.object.product_productgroup.Protect = 1
end subroutine

event constructor;This.InsertRow(0)
this.setitem(1,1, 'P')

end event

