HA$PBExportHeader$u_classfactory.sru
forward
global type u_classfactory from u_abstractclassfactory
end type
end forward

global type u_classfactory from u_abstractclassfactory
end type
global u_classfactory u_classfactory

type variables
private:
  u_abstractobjectlist	iu_ObjectList
  u_abstractrunningobjectlist	iu_RunningObjectTable
  connection		icon_connection
  ArrangeOpen		ienum_ArrangeOpen = Original!
end variables

forward prototypes
public function boolean uf_initialize (u_abstractobjectlist ao_theobjectlist, connection acon_theconnection)
public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject)
public subroutine uf_getresponsewindow (string as_theclassname, ref u_abstractparameterstack ao_theparamstack)
public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject, ref u_abstracterrorcontext ao_theerrorcontext)
private subroutine uf_appenderrormessage (string as_themessage, ref u_abstracterrorcontext ao_theerrorcontext)
private subroutine uf_seterrorreturnvalue (long al_thevalue, ref u_abstracterrorcontext ao_theerrorcontext)
public function boolean uf_setconnection (ref connection au_connection)
public function boolean uf_setarrangeopen (arrangeopen aen_arrangeopen)
public function boolean uf_getarrangeopen (ref arrangeopen aen_arrangeopen)
end prototypes

public function boolean uf_initialize (u_abstractobjectlist ao_theobjectlist, connection acon_theconnection);iu_objectlist = ao_theobjectlist
icon_connection = acon_theconnection

iu_runningobjecttable = Create Using 'u_runningobjectlist'
iu_runningobjecttable.uf_Initialize()
Return True
end function

public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject);u_abstracterrorcontext	lu_ErrorContext

SetNull(lu_ErrorContext)
Return This.uf_GetObject(as_theclassname, ao_theobject, lu_ErrorContext)

end function

public subroutine uf_getresponsewindow (string as_theclassname, ref u_abstractparameterstack ao_theparamstack);String		ls_class
Window		lw_response

ls_class = iu_ObjectList.uf_LookupClass(as_theClassName)
If Len(Trim(ls_class)) < 1 or IsNull(ls_class) Then
	ls_class = as_theClassName
End if
OpenWithParm(lw_response, ao_theParamStack, ls_class)
ao_theParamStack = Message.PowerObjectParm

Return

end subroutine

public function boolean uf_getobject (string as_theclassname, ref powerobject ao_theobject, ref u_abstracterrorcontext ao_theerrorcontext);Boolean		lb_IsWindow

String		ls_class, &
				ls_where, &
				ls_duration, &
				ls_windowtype
				
Long			ll_return

ErrorReturn	ler_return

Window		lw_temp, &
				lw_Frame
				

ls_class = iu_ObjectList.uf_LookupClass(as_theClassName)
If Len(Trim(ls_class)) < 1 Or IsNull(ls_Class) Then
	ls_where = "local"
	ls_duration = "short"
	ls_class = as_theClassName
	If Left(Lower(as_theclassname), 1) = 'w' Then
		lb_IsWindow = True
		ls_windowtype = 'sheet'
	End If		
Else
	ls_where = iu_ObjectList.uf_LookupWhere(as_theClassName)
	ls_duration = iu_ObjectList.uf_LookupDuration(as_theClassName)
	ls_windowtype = Lower(iu_ObjectList.uf_LookupWindowType(as_theClassName))
	If Len(Trim(ls_WindowType)) > 0 Then
		lb_IsWindow = True
	End if
End if

Choose Case ls_duration
	Case "long"
		// Reuse existing instance if valid
		If Lower(Left(ls_windowtype,5)) = "frame" Then
			If iu_RunningObjectTable.uf_GetFrame(lw_frame) Then
				If IsValid(lw_frame) Then
					ao_theObject = lw_frame
					Return True
				End If
			End If
		End If
		ao_theObject = iu_RunningObjectTable.uf_QueryInterface(as_theClassName, ls_where)
		If IsValid(ao_theObject) Then
			Return True
		End If
	Case "short"
		// Create a new instance (below)
	Case Else
		this.uf_AppendErrorMessage("Unknown duration: " + ls_duration, ao_theErrorContext)
		this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
		Return False
End Choose

If Not IsValid(icon_connection) Then ls_where = "local"

Choose Case ls_where
	Case "local"
		If Not IsValid(FindClassDefinition(ls_class)) Or IsNull(FindClassDefinition(ls_class)) Then
			this.uf_AppendErrorMessage( "Unable to find class definition for " + ls_class, ao_theErrorContext)
			this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
			Return False
		End If
		If Not lb_IsWindow Then
			ao_theObject = CREATE USING ls_class
			If IsNull(ao_theObject) Then
				this.uf_AppendErrorMessage( "Unable to create local object " + ls_class, ao_theErrorContext)
				this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
				Return False
			End If
		Else
			// This object is a window
			If ls_WindowType = 'sheet' Or IsNull(ls_WindowType) Or Len(Trim(ls_WindowType)) = 0 Then
				If iu_runningobjecttable.uf_GetFrame(lw_frame) Then
					OpenSheet(lw_temp, as_theclassname, lw_frame,0,ienum_arrangeopen)
					ao_theobject = lw_temp
				Else
					this.uf_AppendErrorMessage( "Unable to open sheet " + ls_class + ". No MDI Frame defined ", ao_theErrorContext)
					this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
					Return False
				End if
					
			Else	
				Open(lw_temp, ls_class)
				ao_theobject = lw_temp
			End if
		End if
				
	Case "server"
		ll_return = icon_connection.CreateInstance(ao_theObject, ls_class)
		If ll_return <> 0 Then
			this.uf_AppendErrorMessage("Unable to create remote object " + ls_class, ao_theErrorContext)
		End If
		Choose Case ll_return
			Case 0  // Success
			Case 50 // Distributed service error
				this.uf_AppendErrorMessage("Distributed service error", ao_theErrorContext)
			Case 52 // Distributed communications error
				this.uf_AppendErrorMessage("Distributed communications error", ao_theErrorContext)
			Case 53 // Requested server not active
				this.uf_AppendErrorMessage("Requested server not active", ao_theErrorContext)
			Case 54 // Server not accepting requests
				this.uf_AppendErrorMessage("Server not accepting requests", ao_theErrorContext)
			Case 55 // Request terminated abnormally
				this.uf_AppendErrorMessage("Request terminated abnormally", ao_theErrorContext)
			Case 56 // Response to request incomplete
				this.uf_AppendErrorMessage("Response to request incomplete", ao_theErrorContext)
			Case 62 // Server busy
				this.uf_AppendErrorMessage("Server busy", ao_theErrorContext)
			Case Else
				this.uf_AppendErrorMessage("Unknown error", ao_theErrorContext)
		End Choose
		this.uf_SetErrorReturnValue(ll_return, ao_theErrorContext)
		If ll_return <> 0 Then
			Return False
		End If
	Case "shared"
		Choose Case ls_duration
			Case "long"
				ler_return = SharedObjectGet(as_theClassName,ao_theObject)
				Choose Case ler_return
					Case Success!
					Case FeatureNotSupportedError!
						this.uf_AppendErrorMessage("SharedObjectGet is not supported on Windows 3.x", &
							ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectCreateInstanceError!
						this.uf_AppendErrorMessage("The local reference to the shared " + &
							"object " + ls_class + " could not be created", ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectNotExistsError!
						// Need to register and get the shared object
						ler_return = SharedObjectRegister(ls_class, as_theClassName)
						Choose Case ler_return
							Case Success!
							Case FeatureNotSupportedError!
								this.uf_AppendErrorMessage("SharedObjectRegister is not " + &
									"supported on Windows 3.x", ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
							Case SharedObjectExistsError!
								this.uf_AppendErrorMessage("SharedObjectRegister: " + &
									as_theClassName + " already registered", ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
							Case SharedObjectCreateInstanceError!
								this.uf_AppendErrorMessage("The local reference to the " + &
									"shared object " + ls_class + &
									" could not be created", ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
							Case SharedObjectCreatePBSessionError!
								this.uf_AppendErrorMessage("The shared object session " + &
									"could not be started", ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
						End Choose				
						ler_return = SharedObjectGet(as_theClassName,ao_theObject)
						Choose Case ler_return
							Case Success!
							Case FeatureNotSupportedError!
								this.uf_AppendErrorMessage("SharedObjectGet is not " + &
									"supported on Windows 3.x", ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
							Case SharedObjectCreateInstanceError!
								this.uf_AppendErrorMessage("The local reference to the " + &
									"shared object " + ls_class + &
									" could not be created", ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
							Case SharedObjectNotExistsError!
								this.uf_AppendErrorMessage("The instance name for the " + &
									"shared object " + ls_class + &
									" could not be registered", &
									ao_theErrorContext)
								this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
								Return False
						End Choose
				End Choose
			Case "short"
				// Need to register and get the shared object
				ler_return = SharedObjectRegister(ls_class, as_theClassName)
				Choose Case ler_return
					Case Success!
					Case FeatureNotSupportedError!
						this.uf_AppendErrorMessage("SharedObjectRegister is not " + & 
					"supported on Windows 3.x", ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectExistsError!
						this.uf_AppendErrorMessage("SharedObjectRegister: " + as_theClassName + &
					" already registered", ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectCreateInstanceError!
						this.uf_AppendErrorMessage("The local reference to the shared " + &
							"object " + ls_class + " could not be created", ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectCreatePBSessionError!
						this.uf_AppendErrorMessage("The shared object session could not be started", &
					ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
				End Choose				
				ler_return = SharedObjectGet(as_theClassName,ao_theObject)
				Choose Case ler_return
					Case Success!
					Case FeatureNotSupportedError!
						this.uf_AppendErrorMessage("SharedObjectGet is not supported on Windows 3.x", &
					ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectCreateInstanceError!
						this.uf_AppendErrorMessage("The local reference to the shared " + &
							"object " + ls_class + " could not be created", ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
					Case SharedObjectNotExistsError!
						this.uf_AppendErrorMessage("The instance name for the shared " + &
							"object " + ls_class + " has not been registered", ao_theErrorContext)
						this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
						Return False
				End Choose
			Case Else
				// Something must have modified ls_duration in this very script!
		End Choose
	Case Else
		SetNull(ao_theObject)
		this.uf_AppendErrorMessage("Unknown location for object " + ls_class, ao_theErrorContext)
		this.uf_SetErrorReturnValue(-1, ao_theErrorContext)
		Return False
End Choose

Choose Case ls_duration
	Case "short"
		// Object is not initialized
		Return False		
	Case "long"
		// Insert into running object table
		iu_RunningObjectTable.uf_AddRef(as_theClassName, ls_class, ao_theObject, ls_where, ls_windowtype)
		Return False
	Case Else
		// Something must have modified ls_duration in this very script!
End Choose

Return False

end function

private subroutine uf_appenderrormessage (string as_themessage, ref u_abstracterrorcontext ao_theerrorcontext);If IsValid(ao_theerrorcontext) Then
	ao_theerrorcontext.uf_AppendText(as_themessage)
End If
Return
end subroutine

private subroutine uf_seterrorreturnvalue (long al_thevalue, ref u_abstracterrorcontext ao_theerrorcontext);If IsValid(ao_theerrorcontext) Then
	ao_theerrorcontext.uf_SetReturnCode(al_thevalue)
End If
end subroutine

public function boolean uf_setconnection (ref connection au_connection);icon_connection = au_connection

Return IsValid(icon_connection)
end function

public function boolean uf_setarrangeopen (arrangeopen aen_arrangeopen);/* --------------------------------------------------------
uf_SetArrangeOpen()

<DESC>	Sets how windows will open
</DESC>

<USAGE>	Call this function to tell the ClassFactory how windows should open.
			Valid Arguments are: Cascaded!, Layered!, Original!
</USAGE>
-------------------------------------------------------- */

ienum_arrangeopen  = aen_arrangeopen
Return True
end function

public function boolean uf_getarrangeopen (ref arrangeopen aen_arrangeopen);/* --------------------------------------------------------
uf_GetArrangeOpen()

<DESC>	Retrieves how windows will open
</DESC>

<USAGE>	Call this function to get how windows should
			open from the class factory.
			Valid Arguments are:Cascaded!,Layered!,Original!
</USAGE>
-------------------------------------------------------- */

aen_arrangeopen = ienum_arrangeopen
Return True
end function

on u_classfactory.create
TriggerEvent( this, "constructor" )
end on

on u_classfactory.destroy
TriggerEvent( this, "destructor" )
end on

