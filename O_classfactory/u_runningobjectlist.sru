HA$PBExportHeader$u_runningobjectlist.sru
forward
global type u_runningobjectlist from u_abstractrunningobjectlist
end type
end forward

global type u_runningobjectlist from u_abstractrunningobjectlist
end type
global u_runningobjectlist u_runningobjectlist

type variables
Private:
DataStore	ids_list
PowerObject	ipo_running[]
end variables

forward prototypes
public function boolean uf_initialize ()
public function powerobject uf_queryinterface (string as_classname, string as_where)
public function boolean uf_getframe (ref window aw_frame)
public subroutine uf_addref (string as_theclassname, string as_class, powerobject ao_theobject, string as_where, string as_windowtype)
end prototypes

public function boolean uf_initialize ();ids_list = Create DataStore
ids_list.DataObject = 'd_objectlist'
// We want to use the same data object as the objectlist, 
// but it will only contain running objects
ids_list.Reset()

Return True



end function

public function powerobject uf_queryinterface (string as_classname, string as_where);Long		ll_row, &
			ll_count
PowerObject	lpo_Null
String	ls_return = ""

ll_row = ids_list.Find("class = '" + as_classname + "'",1,ids_list.RowCount() + 1)

If ll_row > 0 Then 
	Return ipo_running[ll_row]
End If

SetNull(lpo_Null)
Return lpo_Null

end function

public function boolean uf_getframe (ref window aw_frame);Long		ll_row
String	ls_return = ""

ll_row = ids_list.Find("Lower(window_type) = 'frame'", 1,ids_list.RowCount() + 1)

If ll_row > 0 Then
	aw_frame = ipo_running[ll_row]
End If 

Return True

end function

public subroutine uf_addref (string as_theclassname, string as_class, powerobject ao_theobject, string as_where, string as_windowtype);Long		ll_row, &
			ll_count
String	ls_return = ""

ll_row = ids_list.Find("class = '" + as_theclassname + "'",1,ids_list.RowCount() + 1)

If ll_row > 0 Then 
	ll_count = ids_list.GetItemNumber(ll_row, "count")
	ids_list.SetItem(ll_row, "count", ll_count + 1)
	Return 
End If

ll_row = ids_list.InsertRow(0)
ids_list.SetItem(ll_row, "class", as_theclassname)
ids_list.SetItem(ll_row, "classname", as_class)
ids_list.SetItem(ll_row, "where", as_where)
ids_list.SetItem(ll_row, "window_type", as_windowtype)
ids_list.SetItem(ll_row, "duration", "long")
ids_list.SetItem(ll_row, "count", 1)
ipo_running[ll_row] = ao_theobject

Return

end subroutine

on u_runningobjectlist.create
TriggerEvent( this, "constructor" )
end on

on u_runningobjectlist.destroy
TriggerEvent( this, "destructor" )
end on

